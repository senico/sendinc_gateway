/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.sms;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.UserPreferences;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.sms.SMSTransportException;
import mitm.common.sms.transport.clickatell.ClickatellParameterProvider;
import mitm.common.sms.transport.clickatell.ClickatellParameters;
import mitm.common.sms.transport.clickatell.PropertiesClickatellParameters;
import mitm.common.sms.transport.clickatell.StaticClickatellParameters;
import mitm.common.util.Check;

/**
 * A ClickatellParameterProvider that returns the parameter values from the global user preferences (which in most cases
 * is a database backed)
 * 
 * @author Martijn Brinkers
 *
 */
public class DynamicClickatellParameterProvider implements ClickatellParameterProvider
{
    /*
     * Used for getting the global properties
     */
	private final GlobalPreferencesManager globalPreferencesManager;
	
	public DynamicClickatellParameterProvider(GlobalPreferencesManager globalPreferencesManager)
	{
		Check.notNull(globalPreferencesManager, "globalPreferencesManager");
		
		this.globalPreferencesManager = globalPreferencesManager;
	}

	@Override
    @StartTransaction
	public ClickatellParameters getParameters() 
	throws SMSTransportException
	{
		try {
			HierarchicalProperties globalPropeties = getGlobalPropeties();
			
			
			ClickatellParameters dynamic = new PropertiesClickatellParameters(globalPropeties);
			
	         /*
             * We need to copy the valued from the dynamic to the static because we want to 
             * able to use the values outside of a database transaction
             */
			ClickatellParameters result = new StaticClickatellParameters();
			
			dynamic.copyTo(result);
			
			return result;
		}
		catch(HierarchicalPropertiesException e) {
			throw new SMSTransportException("Error getting Clickatell parameters.", e);
		}
	}
	
	private HierarchicalProperties getGlobalPropeties() 
	throws HierarchicalPropertiesException 
	{
    	UserPreferences globalPreferences = globalPreferencesManager.getGlobalUserPreferences();
    	
    	Check.notNull(globalPreferences, "globalPreferences");
    	
    	HierarchicalProperties properties = globalPreferences.getProperties();

    	Check.notNull(properties, "properties");
    	
    	return properties;
	}
}
