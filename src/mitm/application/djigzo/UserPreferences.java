/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.common.properties.NamedBlob;
import mitm.common.security.KeyAndCertificate;

/**
 * UserPreferences contains preferences for a specific user. UserPreferences is not just used by the User object,
 * although the name seems to imply this, but also for a domain.
 * 
 * Note: a UserPreferences is uniquely identified by the combination of name and category. The implementor should therefore
 * override equals is such a way that an object is equal if and only if name and category is equal.
 * 
 * @author Martijn Brinkers
 *
 */
public interface UserPreferences
{
    /**
     * Name of this preference. This name should be unique.
     */
    public String getName();
    
    /**
     * The category of this UserPreferences.
     */
    public String getCategory();
    
    /**
     * A mutable Set of X509Certificates provided by this UserPreferences.
     *  
     * Note: There is no guarantee that returned set will always be the same instance 
     * (depends on implementation). The returned Set should be a 'live' set ie. 
     * changes made to the set are persisted.
     */
    public Set<X509Certificate> getCertificates();
    
    /**
     * Returns an immutable Set of certificates that are inherited from the inherited UserPreferences. The returned
     * set is unordered.
     */
    public Set<X509Certificate> getInheritedCertificates();

    /**
     * Returns a mutable set of named certificates. The named certificates set is a general purpose 
     * collection of certificates. The rationale for the named certificates collection is that it 
     * allows you to associate certificates with the user preference without having to change the
     * database. This cannot be solved by using the UserProperties because it does not contain
     * a foreign key to a certificate.
     *   
     * Note: There is no guarantee that returned set will always be the same instance 
     * (depends on implementation). The returned Set should be a 'live' set ie. changes 
     * made to the set are persisted.
     */
    public Set<NamedCertificate> getNamedCertificates();

    /**
     * Returns an immutable Set of nameCertificates that are inherited from the inherited UserPreferences. The returned
     * set is unordered.
     */
    public Set<NamedCertificate> getInheritedNamedCertificates();
    
    /**
     * Returns the KeyCertificatePair for this UserPreferences.
     */
    public KeyAndCertificate getKeyAndCertificate()
    throws CertStoreException, KeyStoreException;

    /**
     * Sets the KeyAndCertificate for this UserPreferences.
     */
    public void setKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException;

    /**  
     * Returns an immutable Set of KeyAndCertificates that are inherited from the inherited UserPreferences. The
     * returned set is unordered.
     */
    public Set<KeyAndCertificate> getInheritedKeyAndCertificates()
    throws CertStoreException, KeyStoreException;
    
    /**
     * The properties. The returned HierarchicalProperties object is the combination of the properties of this
     * UserPreferences and the properties of the inherited UserPreferences.
     */
    public UserProperties getProperties();
    
    /**
     * Returns the named blobs referenced by this UserPreferences object. The Set of NamesBlob's that's returned are
     * only the named blobs referenced by this UserPreferences. To get the inherited named blobs use 
     * getInheritedUserPreferences#getNamedBlobs.
     * 
     * Note: There is no guarantee that returned set will always be the same instance (depends on implementation). 
     * The returned Set should be a 'live' set ie. changes made to the set are persisted.
     */
    public Set<NamedBlob> getNamedBlobs();

    /**
     * Returns a mutable ordered set of inherited UserPreferences. The last element in the ordered set is the most
     * significant UserPreferences and the first element the least significant. 
     * 
     * Note: There is no guarantee that returned set will always be the same instance 
     * (depends on implementation). The returned Set should be a 'live' set ie. changes 
     * made to the set are persisted.
     */
    public Set<UserPreferences> getInheritedUserPreferences();
}
