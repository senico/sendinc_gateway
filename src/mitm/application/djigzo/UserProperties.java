/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.util.Date;

import mitm.application.djigzo.relay.RelayBounceMode;
import mitm.common.properties.DefaultPropertyProvider;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.smime.SMIMEEncryptionAlgorithm;
import mitm.common.security.smime.SMIMESigningAlgorithm;

/**
 * UserProperties contains higher level functionality (as opposed to the basic HierarchicalProperties methods) 
 * for accessing Djigzo specific settings.
 * 
 * @author Martijn Brinkers
 *
 */
public interface UserProperties extends ResolvableHierarchicalProperties, DefaultPropertyProvider
{
	/*
	 * User property names
	 */
    public final static String LOCALITY                            = "locality";
    public final static String ENCRYPT_MODE                        = "encryptMode";
    public final static String SMIME_ENABLED                       = "sMIMEEnabled";
    public final static String SMIME_STRICT                        = "sMIMEEStrict";
    public final static String SMIME_SKIP_CALENDAR                 = "sMIMESkipCalendar";
    public final static String SMIME_SKIP_SIGNING_CALENDAR         = "sMIMESkipSigningCalendar";
    public final static String SMIME_ADD_USER                      = "sMIMEAddUser";
    public final static String SMIME_PROTECT_HEADERS               = "sMIMEProtectHeaders";
    public final static String SMIME_ENCRYPTION_ALGORITHM          = "sMIMEEncryptionAlgorithm";
    public final static String SMIME_SIGNING_ALGORITHM             = "sMIMESigningAlgorithm";
    public final static String AUTO_SELECT_ENCRYPTION_CERTS        = "autoSelectEncryptionCerts";
    public final static String ALWAYS_USE_FRESHEST_SIGNING_CERT    = "alwaysUseFreshestSigningCert";
    public final static String ONLY_SIGN_WHEN_ENCRYPT              = "onlySignWhenEncrypt";
    public final static String ADD_ADDITIONAL_CERTS                = "addAdditionalCerts";
    public final static String FORCE_SIGNING_HEADER_TRIGGER        = "forceSigningHeaderTrigger";
    public final static String FORCE_SIGNING_ALLOWED               = "forceSigningAllowed";
    public final static String FORCE_ENCRYPT_HEADER_TRIGGER        = "forceEncryptHeaderTrigger";
    public final static String FORCE_ENCRYPT_ALLOWED               = "forceEncryptAllowed";
    public final static String MAX_SIZE_SMIME                      = "maxSizeSMIME";
    public final static String REMOVE_SMIME_SIGNATURE              = "removeSMIMESignature";
    public final static String SUBJECT_TRIGGER                     = "subjectTrigger";
    public final static String SUBJECT_TRIGGER_ENABLED             = "subjectTriggerEnabled";
    public final static String SUBJECT_TRIGGER_IS_REGEXPR          = "subjectTriggerIsRegExpr";
    public final static String SUBJECT_TRIGGER_REMOVE_PATTERN      = "subjectTriggerRemovePattern";
    public final static String SEND_ENCRYPTION_NOTIFICATION        = "sendEncryptionNotification";
    public final static String SMS_PHONE_NUMBER                    = "smsPhoneNumber";
    public final static String SMS_SEND_ALLOWED                    = "smsSendAllowed";
    public final static String SMS_RECEIVE_ALLOWED                 = "smsReceiveAllowed";
    public final static String SMS_PHONE_NUMBER_SET_ALLOWED        = "smsPhoneNumberSetAllowed";
    public final static String PHONE_DEFAULT_COUNTRY_CODE          = "phoneDefaultCountryCode";
    public final static String PASSWORD                            = "password";
    public final static String PASSWORD_LENGTH                     = "passwordLength";
    public final static String PASSWORD_ID                         = "passwordID";
    public final static String DATE_PASSWORD_SET                   = "datePasswordSet";
    public final static String PASSWORD_VALIDITY_INTERVAL          = "passwordValidityInterval";
    public final static String PASSWORDS_SEND_TO_ORIGINATOR        = "passwordsSendToOriginator";
    public final static String SERVER_SECRET                       = "serverSecret";
    public final static String CLIENT_SECRET                       = "clientSecret";
    public final static String BLACKBERRY_RECIPIENT                = "blackberryRecipient";
    public final static String STRIP_UNSUPPORTED_FORMATS           = "stripUnsupportedFormats";
    public final static String RELAY_ALLOWED                       = "relayAllowed";
    public final static String RELAY_EMAIL                         = "relayEmail";
    public final static String RELAY_VALIDITY_INTERVAL             = "relayValidityInterval";
    public final static String RELAY_BOUNCE_MODE                   = "relayBounceMode";
    public final static String PFX_PASSWORD                        = "pfxPassword";
    public final static String AUTO_REQUEST_CERTIFICATE            = "autoRequestCertificate";
    public final static String OTP_ENABLED                         = "otpEnabled";
    public final static String OTP_URL                             = "otpURL";
    public final static String AUTO_CREATE_CLIENT_SECRET           = "autoCreateClientSecret";
    public final static String ADD_SECURITY_INFO                   = "securityInfo.add";
    public final static String SECURITY_INFO_DECRYPTED_TAG         = "securityInfo.decryptedTag";
    public final static String SECURITY_INFO_SIGNED_VALID_TAG      = "securityInfo.signedValidTag";
    public final static String SECURITY_INFO_SIGNED_BY_VALID_TAG   = "securityInfo.signedByValidTag";
    public final static String SECURITY_INFO_SIGNED_INVALID_TAG    = "securityInfo.signedInvalidTag";
    public final static String PDF_ENCRYPTION_ALLOWED              = "pdf.encryptionAllowed";
    public final static String PDF_MAX_SIZE                        = "pdf.maxSize";
    public final static String PDF_REPLY_ALLOWED                   = "pdf.replyAllowed";
    public final static String PDF_REPLY_VALIDITY_INTERVAL         = "pdf.replyValidityInterval";
    public final static String PDF_REPLY_URL                       = "pdf.replyURL";
    public final static String PDF_REPLY_SENDER                    = "pdf.replySender";
    public final static String PDF_ONLY_ENCRYPT_IF_MANDATORY       = "pdf.onlyEncryptIfMandatory";
    public final static String PDF_SIGN_EMAIL                      = "pdf.signEmail";
    public final static String SUBJECT_FILTER_ENABLED              = "subjectFilter.enabled";
    public final static String SUBJECT_FILTER_REGEX                = "subjectFilter.regEx";
    
    /** 
     * The locality of the user determines whether an incoming message should be encrypted
     * or decrypted.
     */
    public void setUserLocality(UserLocality locality) 
    throws HierarchicalPropertiesException;
    
    public UserLocality getUserLocality() 
    throws HierarchicalPropertiesException;   

    /**
     * If the user supports encryption
     */
    public EncryptMode getEncryptMode() 
    throws HierarchicalPropertiesException;
    
    public void setEncryptMode(EncryptMode encryptMode) 
    throws HierarchicalPropertiesException;

    /**
     * If S/MIME is enabled for the user
     */
    public void setSMIMEEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMEEnabled()
    throws HierarchicalPropertiesException;

    /**
     * Trie if S/MIME strict is enabled for the user
     */
    public void setSMIMEStrict(Boolean strict)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMEStrict()
    throws HierarchicalPropertiesException;
    
    /**
     * If true calendar emails are not S/MIME'd
     * (Outlook cannot handle S/MIME calandar items)
     */
    public void setSMIMESkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMESkipCalendar()
    throws HierarchicalPropertiesException;

    /**
     * If true calendar emails are not digitally signed
     * (Outlook cannot handle S/MIME calandar items)
     */
    public void setSMIMESkipSigningCalendar(Boolean skip)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMESkipSigningCalendar()
    throws HierarchicalPropertiesException;

    /**
     * If true and a valid certificate is available for the recipient, a user will be created
     * for the recipient
     */
    public void setSMIMEAddUser(Boolean addUser)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMEAddUser()
    throws HierarchicalPropertiesException;

    /**
     * If true certain headers will be added to the encrypted CMS blob 
     */
    public void setSMIMEProtectHeaders(Boolean protect)
    throws HierarchicalPropertiesException;
    
    public boolean isSMIMEProtectHeaders()
    throws HierarchicalPropertiesException;

    /**
     * The S/MIME encryption algorithm to use (see SMIMEEncryptionAlgorithm for supported algorithms)
     */
    public void setSMIMEEncryptionAlgorithm(SMIMEEncryptionAlgorithm algorithm)
    throws HierarchicalPropertiesException;
    
    public SMIMEEncryptionAlgorithm getSMIMEEncryptionAlgorithm()
    throws HierarchicalPropertiesException;

    /**
     * The S/MIME signing algorithm to use (see SMIMESigningAlgorithm for supported algorithms)
     */
    public void setSMIMESigningAlgorithm(SMIMESigningAlgorithm algorithm)
    throws HierarchicalPropertiesException;
    
    public SMIMESigningAlgorithm getSMIMESigningAlgorithm()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, auto selection of encryption certificates is enabled
     */
    public void setAutoSelectEncryptionCerts(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isAutoSelectEncryptionCerts()
    throws HierarchicalPropertiesException;

    /**
     * If true, the most freshest (i.e., with the most latest valid from) signing certificate will always be used
     */
    public void setAlwaysUseFreshestSigningCert(Boolean useFreshestSigningCert)
    throws HierarchicalPropertiesException;
    
    public boolean isAlwaysUseFreshestSigningCert()
    throws HierarchicalPropertiesException;
    
    /**
     * If true messages will only be signed when encrypted
     */
    public void setOnlySignWhenEncrypt(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isOnlySignWhenEncrypt()
    throws HierarchicalPropertiesException;

    /**
     * If true and a message is S/MIME encrypted, the additional certificates will be added
     */
    public void setAddAdditionalCertificates(Boolean add)
    throws HierarchicalPropertiesException;
    
    public boolean isAddAdditionalCertificates()
    throws HierarchicalPropertiesException;

    /**
     * The trigger that can trigger signing the message when a header
     * matches the trigger
     */
    public void setForceSigningHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException;
    
    public String getForceSigningHeaderTrigger()
    throws HierarchicalPropertiesException;
    
    /**
     * If true a header can force signing of a message (see ForceSigningHeaderTrigger)
     */
    public void setForceSigningAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isForceSigningAllowed()
    throws HierarchicalPropertiesException;
    
    /**
     * The trigger that can trigger encrypting the message when a header
     * matches the trigger
     */
    public void setForceEncryptHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException;
    
    public String getForceEncryptHeaderTrigger()
    throws HierarchicalPropertiesException;
    
    /**
     * If true a header can force encypt of a message (see ForceEncryptHeaderTrigger)
     */
    public void setForceEncryptAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isForceEncryptAllowed()
    throws HierarchicalPropertiesException;
    
    /**
     * The maximum size of a message that will be S/MIME'd
     */
    public void setMaxSizeSMIME(Long size)
    throws HierarchicalPropertiesException;
    
    public Long getMaxSizeSMIME()
    throws HierarchicalPropertiesException;

    /**
     * If true the signature from S/MIME message will be removed
     */
    public void setRemoveSMIMESignature(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isRemoveSMIMESignature()
    throws HierarchicalPropertiesException;
    
    /**
     * The subject trigger that triggers encryption
     */
    public void setSubjectTrigger(String trigger)
    throws HierarchicalPropertiesException;
    
    public String getSubjectTrigger()
    throws HierarchicalPropertiesException;

    /**
     * If the subject trigger is enabled
     */
    public void setSubjectTriggerEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSubjectTriggerEnabled()
    throws HierarchicalPropertiesException;
    
    /**
     * If the subject trigger should be treated as a regular expression
     */
    public void setSubjectTriggerIsRegExpr(Boolean isRegExpr)
    throws HierarchicalPropertiesException;
    
    public boolean isSubjectTriggerRegExpr()
    throws HierarchicalPropertiesException;

    /**
     * If the matching pattern should be removed from the subject
     */
    public void setSubjectTriggerRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException;
    
    public boolean isSubjectTriggerRemovePattern()
    throws HierarchicalPropertiesException;    

    /**
     * If true a notification will be sent to the sender when the message is encrypted
     */
    public void setSendEncryptionNotification(Boolean sendNotification)
    throws HierarchicalPropertiesException;
    
    public boolean isSendEncryptionNotification()
    throws HierarchicalPropertiesException;    

    /**
     * The telephone number used to when sending SMS messages.
     * @throws HierarchicalPropertiesException 
     */
    public void setSMSPhoneNumber(String phoneNumber) 
    throws HierarchicalPropertiesException;
    
    public String getSMSPhoneNumber() 
    throws HierarchicalPropertiesException;

    /**
     * If the user allows an SMS to be sent
     */
    public void setSMSSendAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isSMSSendAllowed()
    throws HierarchicalPropertiesException;    

    /**
     * If the user allows an SMS to be received
     */
    public void setSMSReceiveAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isSMSReceiveAllowed()
    throws HierarchicalPropertiesException;    

    /**
     * If the user is allowed to set the SMS phone number
     */
    public void setSMSPhoneNumberSetAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isSMSPhoneNumberSetAllowed()
    throws HierarchicalPropertiesException;    
    
    /**
     * The default country code used when phone number starts with 0
     */
    public void setPhoneDefaultCountryCode(String defaultCountryCode)
    throws HierarchicalPropertiesException;
    
    public String getPhoneDefaultCountryCode()
    throws HierarchicalPropertiesException;    
    
    /**
     * The password used for pdf encryption.
     */
    public void setPassword(String password)
    throws HierarchicalPropertiesException;
    
    public String getPassword()
    throws HierarchicalPropertiesException;

    /**
     * The length of the generated password in bytes
     */
    public void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException;
    
    public int getPasswordLength()
    throws HierarchicalPropertiesException;
    
    /**
     * Can be used to identify which password was used to encrypt the pdf. 
     * @throws HierarchicalPropertiesException 
     */
    public void setPasswordID(String passwordID) 
    throws HierarchicalPropertiesException;
    
    public String getPasswordID() 
    throws HierarchicalPropertiesException;
    
    /**
     * The date at which the password was set.
     * @throws HierarchicalPropertiesException 
     */
    public void setDatePasswordSet(Date datePasswordSet) 
    throws HierarchicalPropertiesException;
    
    public Date getDatePasswordSet() 
    throws HierarchicalPropertiesException;
    
    /**
     * The time the password is valid (in milliseconds)
     * @throws HierarchicalPropertiesException 
     */
    public void setPasswordValidityInterval(Long interval) 
    throws HierarchicalPropertiesException;
    
    public long getPasswordValidityInterval()
    throws HierarchicalPropertiesException;

    /**
     * If true generated passwords will be sent to the message originator
     * @throws HierarchicalPropertiesException 
     */
    public void setPasswordsSendToOriginator(Boolean value) 
    throws HierarchicalPropertiesException;
    
    public boolean isPasswordsSendToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * Sets the server secret which is used for generating URLs etc. (using HMAC)
     */
    public void setServerSecret(String secret)
    throws HierarchicalPropertiesException;
    
    public String getServerSecret()
    throws HierarchicalPropertiesException;

    /**
     * The secret shared between gateway and client used for one time passwords (OTP) etc
     */
    public void setClientSecret(String secret)
    throws HierarchicalPropertiesException;
    
    public String getClientSecret()
    throws HierarchicalPropertiesException;
    
    /**
     * If true the recipient is a Blackberry recipient
     */
    public void setBlackBerryRecipient(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isBlackBerryRecipient()
    throws HierarchicalPropertiesException;

    /**
     * If true unsupported message formats are stripped
     */
    public void setStripUnsupportedFormats(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isStripUnsupportedFormats()
    throws HierarchicalPropertiesException;

    /**
     * If true the sender is allowed to relay mail from for example a Blackberry
     */
    public void setRelayAllowed(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isRelayAllowed()
    throws HierarchicalPropertiesException;

    /**
     * The email address to which a relay email  should be sent for 
     * relaying through Djigzo.
     */
    public void setRelayEmail(String email)
    throws HierarchicalPropertiesException;
    
    public String getRelayEmail()
    throws HierarchicalPropertiesException;

    /**
     * The number of milliseconds a relay email is valid
     */
    public void setRelayValidityInterval(Long interval)
    throws HierarchicalPropertiesException;
    
    public long getRelayValidityInterval()
    throws HierarchicalPropertiesException;

    /**
     * The bounce mode for a relay message
     */
    public void setRelayBounceMode(RelayBounceMode mode)
    throws HierarchicalPropertiesException;
    
    public RelayBounceMode getRelayBounceMode()
    throws HierarchicalPropertiesException;
    
    /**
     * The password used for the PFX file.
     */
    public void setPFXPassword(String password)
    throws HierarchicalPropertiesException;
    
    public String getPFXPassword()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a certificate will be requested if the sender does not yet have a certificate
     */
    public void setAutoRequestCertificate(Boolean value)
    throws HierarchicalPropertiesException;
    
    public boolean isAutoRequestCertificate()
    throws HierarchicalPropertiesException;

    /**
     * If true, One Time Passwords will be enabled
     */
    public void setOTPEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isOTPEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The OTP generator URL
     */
    public void setOTPURL(String url)
    throws HierarchicalPropertiesException;
    
    public String getOTPURL()
    throws HierarchicalPropertiesException;

    /**
     * If true, and the user does not have a client secret but needs one, a client 
     * secret will be created
     */
    public void setAutoCreateClientSecret(Boolean autoCreate)
    throws HierarchicalPropertiesException;
    
    public boolean isAutoCreateClientSecret()
    throws HierarchicalPropertiesException;

    /**
     * If true, security info will be added to the subject for received messages (see the securityInfoDecryptedTag,
     * securityInfoSignatureValidTag and securityInfoSignatureInvalidTag properties)
     */
    public void setAddSecurityInfo(Boolean add)
    throws HierarchicalPropertiesException;
    
    public boolean isAddSecurityInfo()
    throws HierarchicalPropertiesException;
    
    /**
     * Tag that will be added to the subject if the messages was decrypted (only if addSecurityInfo is true)
     */
    public void setSecurityInfoDecryptedTag(String tag)
    throws HierarchicalPropertiesException;

    public String getSecurityInfoDecryptedTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true)
     */
    public void setSecurityInfoSignedValidTag(String tag)
    throws HierarchicalPropertiesException;

    public String getSecurityInfoSignedValidTagTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true).
     * Additionally, the email address of the signer will be added because the from is different from the signer.
     */
    public void setSecurityInfoSignedByValidTag(String tag)
    throws HierarchicalPropertiesException;

    public String getSecurityInfoSignedByValidTagTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed but invalid (only if addSecurityInfo is true)
     */
    public void setSecurityInfoSignedInvalidTag(String tag)
    throws HierarchicalPropertiesException;

    public String getSecurityInfoSignedInvalidTagTag()
    throws HierarchicalPropertiesException;
       
    /**
     * If true PDF encryption is allowed for this user
     */
    public void setPdfEncryptionAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;
    
    public boolean isPdfEncryptionAllowed()
    throws HierarchicalPropertiesException;
    
    /**
     * The maximum size of a message that will be PDF'd
     */
    public void setPdfMaxSize(Long size)
    throws HierarchicalPropertiesException;
    
    public Long getPdfMaxSize()
    throws HierarchicalPropertiesException;
    
    /**
     * Determines whether a PDF reply is allowed. For sender of a PDF encrypted email this means that
     * a reply URL is added to the encrypted PDF. For the receiver, if false the receiver cannot reply
     * to a PDF. 
     */
    public void setPdfReplyAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;

    public boolean isPdfReplyAllowed()
    throws HierarchicalPropertiesException;
    
    /**
     * The time in milliseconds a reply is valid from the time the PDF was created.
     */
    public void setPdfReplyValidityInterval(Long interval)
    throws HierarchicalPropertiesException;
    
    public Long getPdfReplyValidityInterval()
    throws HierarchicalPropertiesException;

    /**
     * The base URL for PDF reply
     */
    public void setPdfReplyURL(String url)
    throws HierarchicalPropertiesException;
    
    public String getPdfReplyURL()
    throws HierarchicalPropertiesException;

    /**
     * The email sender to use for a PDF reply
     */
    public void setPdfReplySender(String email)
    throws HierarchicalPropertiesException;
    
    public String getPdfReplySender()
    throws HierarchicalPropertiesException;
    
    /**
     * Only PDF encrypt the message if encryption is required
     */
    public void setPdfOnlyEncryptIfMandatory(Boolean encryptIfMandatory)
    throws HierarchicalPropertiesException;
    
    public boolean isPdfOnlyEncryptIfMandatory()
    throws HierarchicalPropertiesException;
    
    /**
     * S/MIME sign the PDF encrypted email 
     */
    public void setPdfSignEmail(Boolean sign)
    throws HierarchicalPropertiesException;
    
    public boolean isPdfSignEmail()
    throws HierarchicalPropertiesException;

    /**
     * If true, the subject filter will be enabled 
     */
    public void setSubjectFilterEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSubjectFilterEnabled()
    throws HierarchicalPropertiesException;
    
    /**
     * The subject filter reg ex. 
     */
    public void setSubjectFilterRegEx(String regex)
    throws HierarchicalPropertiesException;
    
    public String getSubjectFilterRegEx()
    throws HierarchicalPropertiesException;
    
    /**
     * Template specific properties for this user.
     */
    public TemplateProperties getTemplateProperties();
    
    /**
     * DLP specific properties for this user.
     */
    public DLPProperties getDLPProperties();
    
    /**
     * DKIM specific properties for this user.
     */
    public DKIMProperties getDKIMProperties();

    /**
     * Portal specific properties for this user.
     */
    public PortalProperties getPortalProperties();
}
