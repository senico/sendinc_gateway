/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.phonebook;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A PhoneBook implementation that uses user properties as the PhoneBook store to get the SMS phone number of users.
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPropertiesSMSPhoneBook implements PhoneBook
{
    private final static Logger logger = LoggerFactory.getLogger(UserPropertiesSMSPhoneBook.class);
    
    private final UserWorkflow userWorkflow;
    
    public UserPropertiesSMSPhoneBook(UserWorkflow userWorkflow)
    {
    	Check.notNull(userWorkflow, "userWorkflow");
    	
        this.userWorkflow = userWorkflow;
    }
    
    @Override
    public String getPhoneNumber(String email)
    throws PhoneBookException
    {
        User user = null;
        
        try {
            email = EmailAddressUtils.canonicalizeAndValidate(email, false);
        	
            user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
        }
        catch (AddressException e) {
            logger.error("Error converting email to InternetAddress.", e);
        } 
        catch (HierarchicalPropertiesException e) {
        	throw new PhoneBookException(e);
		}
        
        String phoneNumber = null;
        
        if (user != null) 
        {
            try {
				phoneNumber = user.getUserPreferences().getProperties().getSMSPhoneNumber();
			} 
            catch (HierarchicalPropertiesException e) {
            	throw new PhoneBookException(e);
			}
        }
        
        return phoneNumber;
    }
}
