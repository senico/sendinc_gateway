/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.service;

import mitm.application.djigzo.DjigzoFactoryProperties;
import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.EncryptionRecipientSelector;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.admin.DefaultAdmins;
import mitm.application.djigzo.dlp.PolicyPatternManager;
import mitm.application.djigzo.dlp.UserPreferencesPolicyPatternManager;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.mail.repository.MailStorer;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.cache.KeyCache;
import mitm.common.cache.PatternCache;
import mitm.common.dlp.MimeMessageTextExtractor;
import mitm.common.dlp.PolicyChecker;
import mitm.common.dlp.TextNormalizer;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.pdf.FontProvider;
import mitm.common.properties.NamedBlobManager;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.ca.CA;
import mitm.common.security.ca.CASettingsProvider;
import mitm.common.security.ca.CertificateRequestStore;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.crl.ThreadedCRLStoreUpdater;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.security.ctl.CTLManager;
import mitm.common.security.keystore.KeyStoreProvider;
import mitm.common.security.otp.OTPGenerator;
import mitm.common.security.password.PasswordGenerator;
import mitm.common.sms.SMSGateway;
import mitm.common.template.TemplateBuilder;
import mitm.common.util.URLBuilder;

import org.apache.avalon.framework.service.ServiceManager;

public class SystemServices
{
	private static String SERVICE_NOT_FOUND = "Service %s cannot be found.";

	/*
	 * The service Id's of the Spring beans that are being used by non Spring beans. These service id's are hardwired.
	 * If they should be configurable a translation from the external names to these internal service names can be
	 * added to James assembly.xml.
	 */
    private static String SYSTEM_ENCRYPTOR                        = "systemEncryptor";
    private static String SESSION_MANAGER                         = "sessionManager";
    private static String HIBERNATE_SESSION_SOURCE                = "hibernateSessionSource";
    private static String USER_WORKFLOW                           = "userWorkflow";
    private static String USER_PREFERENCES_WORKFLOW               = "userPreferencesWorkflow";
    private static String PASSWORD_GENERATOR                      = "passwordGenerator";
    private static String MESSAGE_ORIGINATOR_IDENTIFIER           = "messageOriginatorIdentifier";
    private static String PKI_SECURITY_SERVICES                   = "pkiSecurityServices";
    private static String KEY_AND_CERTIFICATE_WORKFLOW            = "keyAndCertificateWorkflow";
    private static String KEY_AND_ROOT_CERTIFICATE_WORKFLOW       = "keyAndRootCertificateWorkflow";
    private static String SMS_GATEWAY                             = "smsGateway";
    private static String ENCRYPTION_RECIPIENT_SELECTOR           = "encryptionRecipientSelector";
    private static String GLOBAL_PREFERENCES_MANAGER              = "globalPreferencesManager";
    private static String DJIGZO_FACTORY_PROPERTIES               = "djigzoFactoryProperties";
    private static String THREADED_CRL_STORE_UPDATER              = "threadedCRLStoreUpdater";
    private static String DOMAIN_MANAGER                          = "domainManager";
    private static String KEY_STORE_PROVIDER                      = "keyStoreProvider";
    private static String ADMIN_MANAGER                           = "adminManager";
    private static String AUTHORITY_MANAGER                       = "authorityManager";
    private static String DEFAULT_ADMINS                          = "defaultAdmins";
    private static String KEY_AND_CERT_STORE                      = "keyAndCertStore";
    private static String KEY_AND_ROOT_CERT_STORE                 = "keyAndRootCertStore";
    private static String AVALON_SERVICE_MANAGER                  = "avalonServiceManager";
    private static String CERTIFICATE_PATH_BUILDER_FACTORY        = "certificatePathBuilderFactory";
    private static String URL_BUILDER                             = "urlBuilder";
    private static String CTL_MANAGER                             = "ctlManager";
    private static String KEY_CACHE                               = "keyCache";
    private static String CA                                      = "ca";
    private static String CERTIFICATE_REQUEST_STORE               = "certificateRequestStore";
    private static String CA_SETTINGS_PROVIDER                    = "caSettingsProvider";
    private static String TEXT_NORMALIZER                         = "textNormalizer";
    private static String POLICY_CHECKER_PIPELINE                 = "policyCheckerPipeline";
    private static String MIME_MESSAGE_TEXT_EXTRACTOR             = "mimeMessageTextExtractor";
    private static String POLICY_PATTERN_MANAGER                  = "policyPatternManager";    
    private static String USER_PREFERENCES_POLICY_PATTERN_MANAGER = "userPreferencesPolicyPatternManager";
    private static String TEMPLATE_BUILDER                        = "templateBuilder";
    private static String QUARANTINE_MAIL_STORER                  = "quarantineMailStorer";
    private static String RANDOM_GENERATOR                        = "randomGenerator";
    private static String NAMED_BLOB_MANAGER                      = "namedBlobManager";
    private static String FONT_PROVIDER                           = "fontProvider";
    private static String OTP_GENERATOR                           = "otpGenerator";
    private static String PATTERN_CACHE                           = "patternCache";
        
    public static <T> T getService(String serviceId, Class<T> clazz)
    {
    	ServiceRegistry registry = DjigzoServiceRegistry.getRegistry();
    	
        T service = registry.getService(serviceId, clazz);

        if (service == null) {
    		throw new DjigzoServiceNotFoundRuntimeException(String.format(
    				SERVICE_NOT_FOUND, serviceId));
    	}
        
        return service;
    }
    
    /**
     * Returns the Encryptor service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static Encryptor getSystemEncryptor() {
        return getService(SystemServices.SYSTEM_ENCRYPTOR, Encryptor.class);
    }

    /**
     * Returns the SessionManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static SessionManager getSessionManager() {
        return getService(SystemServices.SESSION_MANAGER, SessionManager.class);
    }

    /**
     * Returns the HibernateSessionSource service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static HibernateSessionSource getHibernateSessionSource() {
        return getService(SystemServices.HIBERNATE_SESSION_SOURCE, HibernateSessionSource.class);
    }
    
    /**
     * Returns the UserWorkflow service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static UserWorkflow getUserWorkflow() {
        return getService(SystemServices.USER_WORKFLOW, UserWorkflow.class);
    }

    /**
     * Returns the UserPreferencesWorkflow service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static UserPreferencesWorkflow getUserPreferencesWorkflow() {
        return getService(SystemServices.USER_PREFERENCES_WORKFLOW, UserPreferencesWorkflow.class);
    }
    
    /**
     * Returns the PasswordGenerator service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static PasswordGenerator getPasswordGenerator() {
        return getService(SystemServices.PASSWORD_GENERATOR, PasswordGenerator.class);
    }

    /**
     * Returns the MessageOriginatorIdentifier service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
        return getService(SystemServices.MESSAGE_ORIGINATOR_IDENTIFIER, MessageOriginatorIdentifier.class);
    }

    /**
     * Returns the PKISecurityServices service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static PKISecurityServices getPKISecurityServices() {
        return getService(SystemServices.PKI_SECURITY_SERVICES, PKISecurityServices.class);
    }

    /**
     * Returns the KeyAndCertificateWorkflow service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyAndCertificateWorkflow getKeyAndCertificateWorkflow() {
        return getService(SystemServices.KEY_AND_CERTIFICATE_WORKFLOW, KeyAndCertificateWorkflow.class);
    }

    /**
     * Returns the KeyAndRootCertificateWorkflow service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyAndCertificateWorkflow getKeyAndRootCertificateWorkflow() {
        return getService(SystemServices.KEY_AND_ROOT_CERTIFICATE_WORKFLOW, KeyAndCertificateWorkflow.class);
    }

    /**
     * Returns the SMSGateway service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static SMSGateway getSMSGateway() {
        return getService(SystemServices.SMS_GATEWAY, SMSGateway.class);
    }

    /**
     * Returns the EncryptionRecipientSelector service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static EncryptionRecipientSelector getEncryptionRecipientSelector() {
        return getService(SystemServices.ENCRYPTION_RECIPIENT_SELECTOR, EncryptionRecipientSelector.class);
    }
    
    /**
     * Returns the GlobalPreferencesManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static GlobalPreferencesManager getGlobalPreferencesManager() {
        return getService(SystemServices.GLOBAL_PREFERENCES_MANAGER, GlobalPreferencesManager.class);
    }

    /**
     * Returns the DjigzoFactoryProperties service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static DjigzoFactoryProperties getDjigzoFactoryProperties() {
        return getService(SystemServices.DJIGZO_FACTORY_PROPERTIES, DjigzoFactoryProperties.class);
    }
    
    /**
     * Returns the CRLStoreUpdater service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static ThreadedCRLStoreUpdater getThreadedCRLStoreUpdater() {
        return getService(SystemServices.THREADED_CRL_STORE_UPDATER, ThreadedCRLStoreUpdater.class);
    }

    /**
     * Returns the DomainManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static DomainManager getDomainManager() {
        return getService(SystemServices.DOMAIN_MANAGER, DomainManager.class);
    }

    /**
     * Returns the KeyStoreService service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyStoreProvider getKeyStore() {
        return getService(SystemServices.KEY_STORE_PROVIDER, KeyStoreProvider.class);
    }

    /**
     * Returns the AdminManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static AdminManager getAdminManager() {
        return getService(SystemServices.ADMIN_MANAGER, AdminManager.class);
    }

    /**
     * Returns the AuthorityManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static AuthorityManager getAuthorityManager() {
        return getService(SystemServices.AUTHORITY_MANAGER, AuthorityManager.class);
    }

    /**
     * Returns the DefaultAdmins service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static DefaultAdmins getDefaultAdmins() {
        return getService(SystemServices.DEFAULT_ADMINS, DefaultAdmins.class);
    }

    /**
     * Returns the KeyAndCertStore service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyAndCertStore getKeyAndCertStore() {
        return getService(SystemServices.KEY_AND_CERT_STORE, KeyAndCertStore.class);
    }

    /**
     * Returns the KeyAndRootCertStore service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyAndCertStore getKeyAndRootCertStore() {
        return getService(SystemServices.KEY_AND_ROOT_CERT_STORE, KeyAndCertStore.class);
    }
    
    /**
     * Returns the Avalon ServiceManager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static ServiceManager getAvalonServiceManager() {
        return getService(AVALON_SERVICE_MANAGER, ServiceManager.class);
    }

    /**
     * Returns CertificatePathBuilderFactory service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static CertificatePathBuilderFactory getCertificatePathBuilderFactory() {
        return getService(CERTIFICATE_PATH_BUILDER_FACTORY, CertificatePathBuilderFactory.class);
    }

    /**
     * Returns URL builder service (this bean has the prototype scope).
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static URLBuilder getURLBuilder() {
        return getService(URL_BUILDER, URLBuilder.class);
    }

    /**
     * Returns CTL Manager service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static CTLManager getCTLManager() {
        return getService(CTL_MANAGER, CTLManager.class);
    }

    /**
     * Returns KeyCache service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static KeyCache getKeyCache() {
        return getService(KEY_CACHE, KeyCache.class);
    }

    /**
     * Returns CA service.
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static CA getCA() {
        return getService(CA, CA.class);
    }
    
    /**
     * Returns CertificateRequestStore service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static CertificateRequestStore getCertificateRequestStore() {
        return getService(CERTIFICATE_REQUEST_STORE, CertificateRequestStore.class);
    }

    /**
     * Returns CASettingsProvider service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static CASettingsProvider getCASettingsProvider() {
        return getService(CA_SETTINGS_PROVIDER, CASettingsProvider.class);
    }
    
    /**
     * Returns TextNormalizer service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static TextNormalizer getTextNormalizer() {
        return getService(TEXT_NORMALIZER, TextNormalizer.class);
    }

    /**
     * Returns PolicyChecker pipeline service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static PolicyChecker getPolicyCheckerPipeline() {
        return getService(POLICY_CHECKER_PIPELINE, PolicyChecker.class);
    }
    
    /**
     * Returns PolicyChecker pipeline service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static MimeMessageTextExtractor getMimeMessageTextExtractor() {
        return getService(MIME_MESSAGE_TEXT_EXTRACTOR, MimeMessageTextExtractor.class);
    }

    /**
     * Returns the PolicyPatternManager service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static PolicyPatternManager getPolicyPatternManager() {
        return getService(POLICY_PATTERN_MANAGER, PolicyPatternManager.class);
    }
    
    /**
     * Returns the UserPreferencesPolicyPatternManager pipeline service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static UserPreferencesPolicyPatternManager getUserPreferencesPolicyPatternManager() {
        return getService(USER_PREFERENCES_POLICY_PATTERN_MANAGER, UserPreferencesPolicyPatternManager.class);
    }
    
    /**
     * Returns the TemplateBuilder service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static TemplateBuilder getTemplateBuilder() {
        return getService(TEMPLATE_BUILDER, TemplateBuilder.class);
    }

    /**
     * Returns the Quarantine Mail Storer service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static MailStorer getQuarantineMailStorer() {
        return getService(QUARANTINE_MAIL_STORER, MailStorer.class);
    }
    
    /**
     * Returns the RandomGenerator service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static RandomGenerator getRandomGenerator() {
        return getService(RANDOM_GENERATOR, RandomGenerator.class);
    }
    
    /**
     * Returns the NamedBlobManager service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static NamedBlobManager getNamedBlobManager() {
        return getService(NAMED_BLOB_MANAGER, NamedBlobManager.class);
    }
    
    /**
     * Returns the FontProvider service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static FontProvider getFontProvider() {
        return getService(FONT_PROVIDER, FontProvider.class);
    }    

    /**
     * Returns the OTPGenerator service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static OTPGenerator getOTPGenerator() {
        return getService(OTP_GENERATOR, OTPGenerator.class);
    }
    
    /**
     * Returns the PatternCache service
     * @throws DjigzoServiceNotFoundRuntimeException when service is not found.
     */
    public static PatternCache getPatternCache() {
        return getService(PATTERN_CACHE, PatternCache.class);
    }        
}
