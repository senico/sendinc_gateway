/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.service;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import mitm.application.djigzo.DjigzoFactoryProperties;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.net.ProxyInjector;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.DefaultPKISecurityServicesFactory;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.PKISecurityServicesFactory;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import mitm.common.security.certpath.CertStoreTrustAnchorBuilder;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.X509StoreEventListener;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crl.CRLDownloadParameters;
import mitm.common.security.crl.CRLDownloader;
import mitm.common.security.crl.CRLDownloaderImpl;
import mitm.common.security.crl.CRLPathBuilderFactory;
import mitm.common.security.crl.CRLStoreMaintainer;
import mitm.common.security.crl.CRLStoreUpdater;
import mitm.common.security.crl.CRLStoreUpdaterImpl;
import mitm.common.security.crl.CRLStoreUpdaterParameters;
import mitm.common.security.crl.DefaultCRLStoreUpdaterParametersBuilder;
import mitm.common.security.crl.PKIXRevocationChecker;
import mitm.common.security.crl.RevocationChecker;
import mitm.common.security.crl.ThreadedCRLStoreUpdater;
import mitm.common.security.crl.ThreadedCRLStoreUpdaterImpl;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.keystore.KeyStoreProvider;
import mitm.common.security.keystore.KeyStoreProviderKeyStoreWrapper;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.password.PasswordGenerator;
import mitm.common.security.password.PasswordGeneratorImpl;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.Check;

public class SecurityServices
{
	/**
	 * Event listener that will force an update of the TrustAnchorBuilder cache when a certificate
	 * is added and/or removed to/from the root store.
	 */
	private static class RootStoreEventListener implements X509StoreEventListener
	{
		private final TrustAnchorBuilder trustAnchorBuilder;
		
		public RootStoreEventListener(TrustAnchorBuilder trustAnchorBuilder) 
		{
			Check.notNull(trustAnchorBuilder, "trustAnchorBuilder");
			
			this.trustAnchorBuilder = trustAnchorBuilder;
		}
		
		@Override
        public void onChange() {
			trustAnchorBuilder.refresh();
		}
	}
	
    /**
     * Builds the HibernateSessionSource service.
     * 
     * @param hibernateConfig the path to the hibernate configuration file.
     * @return the HibernateSessionSource
     */
    public static HibernateSessionSource buildHibernateSessionSource(File hibernateConfig)
    {
        /*
         * The implementation of HibernateSessionSource must be thread safe because the service 
         * will be a singleton.
         */          
        return new StandardHibernateSessionSourceImpl(hibernateConfig);
    }    

    /**
     * Builds the SessionManager service 
     */
    public static SessionManager buildSessionManager(HibernateSessionSource hibernateSessionSource)
    {
    	SessionManager sessionManager = new SessionManagerImpl(hibernateSessionSource);
    	
    	return sessionManager;
    }
    
    /**
     * Builds the CertStore service.
     */
    public static X509CertStoreExt buildCertStore(String storeName, SessionManager sessionManager)
    {
        /*
         * The implementation of X509CertStoreExt must be thread safe because the service 
         * will be a singleton.
         */  
        X509CertStoreExt certStore = new X509CertStoreExtHibernate(storeName, sessionManager);
        
        return certStore;
    }

    /**
     * Builds the RootStore service. 
     */
    public static X509CertStoreExt buildRootStore(String storeName, SessionManager sessionManager)
    {
        /*
         * The implementation of X509CertStoreExt must be thread safe because the service 
         * will be a singleton.
         */  
        X509CertStoreExt rootStore = new X509CertStoreExtHibernate(storeName, sessionManager);
        
        return rootStore;
    }

    /**
     * Builds the CRLStore service.
     */
    public static X509CRLStoreExt buildCRLStore(String storeName, SessionManager sessionManager)
    {
        /*
         * The implementation of X509CRLStoreExt must be thread safe because the service 
         * will be a singleton.
         */  
        X509CRLStoreExt crlStore = new X509CRLStoreExtHibernate(storeName, sessionManager);
        
        return crlStore;
    }

    /**
     * Builds the KeyStoreProvider service. 
     */
    public static KeyStoreProvider buildKeyStoreProvider(String storeName, SessionManager sessionManager) 
    throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException
    {
        /* 
         * because we are going to use the MITM database KeyStore we have to sure that the MITM provider
         * is registered.
         */
        MITMProvider.initialize(sessionManager);
        
        /*
         * The implementation of KeyStore must be thread safe because the service 
         * will be a singleton.
         */  
        KeyStore keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, MITMProvider.PROVIDER);
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter(storeName, sessionManager));
        
        return new KeyStoreProviderKeyStoreWrapper(keyStore);
    }
    
    /**
     * Builds the TrustAnchorBuilder service.
     * 
     * @param rootStore RootStore service
     * @param updateCheckInterval the number of seconds between updates of the internal trust anchor cache.
     * @return the TrustAnchorBuilder service
     * @throws HierarchicalPropertiesException 
     */
    public static TrustAnchorBuilder buildTrustAnchorBuilder(X509CertStoreExt rootStore, DjigzoFactoryProperties factoryProperties) 
    throws HierarchicalPropertiesException
    {
        /*
         * The implementation of TrustAnchorBuilder must be thread safe because the service 
         * will be a singleton.
         */  
    	TrustAnchorBuilder trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStore, 
    			factoryProperties.getTrustAnchorBuilderUpdateCheckInterval());
    	
    	/*
    	 * register the listener for changes to the root store (add/remove of certificates) so we can
    	 * force an update of the trustAnchorBuilder.
    	 */
    	rootStore.setStoreEventListener(new RootStoreEventListener(trustAnchorBuilder));
    	
    	return trustAnchorBuilder;
    }
    
    /**
     * Builds the RevocationChecker service.
     */
    public static RevocationChecker buildRevocationChecker(X509CRLStoreExt crlStore)
    {
        /*
         * The implementation of RevocationChecker must be thread safe because the service 
         * will be a singleton.
         */  
        return new PKIXRevocationChecker(crlStore);
    }
    
    /**
     * Builds the KeyAndCertStore service.
     */
    public static KeyAndCertStore buildKeyAndCertStore(X509CertStoreExt certStore, KeyStoreProvider keyStoreService, 
            String keyEntryPassword)
    throws KeyStoreException
    {
        return new KeyAndCertStoreImpl(certStore, keyStoreService.getKeyStore(), keyEntryPassword);
    }
    
    /**
     * Builds the PKISecurityServicesFactory service.
     */
    public static PKISecurityServicesFactory buildPKISecurityServicesFactory(KeyAndCertStore keyAndCertStore, 
            X509CertStoreExt rootStore, X509CRLStoreExt crlStore,TrustAnchorBuilder trustAnchorBuilder, 
            RevocationChecker revocationChecker, Encryptor encryptor, 
            CertificatePathBuilderFactory certificatePathBuilderFactory,
            CRLPathBuilderFactory crlPathBuilderFactory,
            PKITrustCheckCertificateValidatorFactory certificateValidatorFactory)
    {
        /*
         * The implementation of PKISecurityServicesFactory must be thread safe because the service 
         * will be a singleton.
         */  
        return new DefaultPKISecurityServicesFactory(keyAndCertStore, rootStore, crlStore, trustAnchorBuilder, 
                revocationChecker, encryptor, certificatePathBuilderFactory, crlPathBuilderFactory,
                certificateValidatorFactory);
    }
    
    /**
     * Builds the PKISecurityServices service.
     */
    public static PKISecurityServices buildPKISecurityServices(PKISecurityServicesFactory factory) 
    {
        /*
         * The implementation of PKISecurityServices must be thread safe because the service 
         * will be a singleton.
         */  
        return factory.createPKISecurityServices();
    }
    
    /**
     * Builds the CRLDownloader service
     */
    public static CRLDownloader buildCRLDownloader(DjigzoFactoryProperties factoryProperties, ProxyInjector proxyInjector) 
    throws HierarchicalPropertiesException
    {
        CRLDownloadParameters downloadParameters = new CRLDownloadParameters(
                factoryProperties.getCRLDownloadParametersTotalTimeout(),  
                factoryProperties.getCRLDownloadParametersConnectTimeout(),
                factoryProperties.getCRLDownloadParametersReadTimeout(),
                factoryProperties.getCRLDownloadParametersMaxBytes());

        CRLDownloader crlDownloader = new CRLDownloaderImpl(downloadParameters, proxyInjector);
        
        return crlDownloader;
    }
    
    /**
     * Builds the CRLStoreUpdaterParameters service
     */
    public static CRLStoreUpdaterParameters buildCRLStoreUpdaterParameters(PKISecurityServices pKISecurityServices,
            DjigzoFactoryProperties factoryProperties, CRLDownloader crlDownloader,
            CRLStoreMaintainer crlStoreMaintainer) 
    throws CRLStoreException, HierarchicalPropertiesException
    {
        DefaultCRLStoreUpdaterParametersBuilder parametersBuilder = new DefaultCRLStoreUpdaterParametersBuilder(
        		pKISecurityServices, crlDownloader, crlStoreMaintainer);
        
        parametersBuilder.setCheckTrust(factoryProperties.isCRLDownloadParametersCheckTrust());
        
        /*
         * The implementation of CRLStoreUpdaterParameters must be thread safe because the service 
         * will be a singleton.
         */          
        return parametersBuilder.createCRLStoreUpdaterParameters();
    }
    
    /**
     * Builds the CRLStoreUpdater service.
     */
    public static CRLStoreUpdater buildCRLStoreUpdater(CRLStoreUpdaterParameters cRLStoreUpdaterParameters)
    {
        /*
         * The implementation of CRLStoreUpdater must be thread safe because the service 
         * will be a singleton.
         */          
        return new CRLStoreUpdaterImpl(cRLStoreUpdaterParameters);
    }

    public static ThreadedCRLStoreUpdater buildThreadedCRLStoreUpdater(CRLStoreUpdater crlStoreUpdater, 
    		SessionManager sessionManager, DjigzoFactoryProperties factoryProperties) 
    throws HierarchicalPropertiesException
    {
    	long updateInterval = factoryProperties.getCRLStoreUpdateInterval();
    	
        /*
         * The implementation of CRLStoreUpdater must be thread safe because the service 
         * will be a singleton.
         */          
        return new ThreadedCRLStoreUpdaterImpl(crlStoreUpdater, sessionManager, updateInterval);
    }
    
    /**
     * Builds the Password generator service.
     */
    public static PasswordGenerator buildPasswordGenerator() 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException 
    {
        return new PasswordGeneratorImpl();
    }
}
