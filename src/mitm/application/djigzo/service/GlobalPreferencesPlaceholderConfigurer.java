/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.service;

import java.util.Properties;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.util.Check;

import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Extension of PropertyPlaceholderConfigurer that resolves it's properties from the global preferences.
 * If the global preferences cannot resolve the property the falls back to the property files
 * from the "locations".
 * 
 * @author Martijn Brinkers
 *
 */
public class GlobalPreferencesPlaceholderConfigurer extends PropertyPlaceholderConfigurer
{
    /*
     * Used to create database sessions
     */
    private final SessionManager sessionManager;
    
    /*
     * Use to get the global preferences
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Helper class that will start transactions when required.
     * 
     * Note: We would have liked to use @StartTransaction on resolvePlaceholder but this bean is 
     * created outside the reach of the Spring AOP intercepter.
     */
    private static class AutoTransactDelegator
    {
        @SuppressWarnings("unused")
        public AutoTransactDelegator() {
            /* required by AutoCommitProxyFactory */
        }
        
        @StartTransaction
        public String getProperty(GlobalPreferencesManager globalPreferencesManager, String placeholder, 
                Properties properties) 
        throws HierarchicalPropertiesException 
        {
            HierarchicalProperties hierarchicalProperties = globalPreferencesManager.
                    getGlobalUserPreferences().getProperties();
    
            return hierarchicalProperties.getProperty(placeholder, false);
        }
        
        public static AutoTransactDelegator createProxy(SessionManager sessionManager)
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory
                    <AutoTransactDelegator>(AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
    }
    
    public GlobalPreferencesPlaceholderConfigurer(SessionManager sessionManager, 
            GlobalPreferencesManager globalPreferencesManager)
    {
        Check.notNull(sessionManager, "sessionManager");
        Check.notNull(globalPreferencesManager, "globalPreferencesManager");
        
        this.sessionManager = sessionManager;
        this.globalPreferencesManager = globalPreferencesManager;
    }
    
    @Override
    protected String resolvePlaceholder(String placeholder, Properties properties)
    {
        String result = null;
        
        try {
            result = AutoTransactDelegator.createProxy(sessionManager).getProperty(globalPreferencesManager, 
                    placeholder, properties);
            
            if (result == null) {
                result = super.resolvePlaceholder(placeholder, properties);
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new FatalBeanException("Error resolving placeholder", e);
        }
        catch (ProxyFactoryException e) {
            throw new FatalBeanException("Error resolving placeholder", e);
        }
        catch (NoSuchMethodException e) {
            throw new FatalBeanException("Error resolving placeholder", e);
        }
                
        return result;
    }
}
