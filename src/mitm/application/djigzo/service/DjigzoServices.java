/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import mitm.application.djigzo.DjigzoFactoryProperties;
import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.EncryptionRecipientSelector;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.UserManager;
import mitm.application.djigzo.UserPreferenceMerger;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesSelector;
import mitm.application.djigzo.impl.DefaultDomainManager;
import mitm.application.djigzo.impl.DefaultEncryptionRecipientSelector;
import mitm.application.djigzo.impl.DefaultGlobalPreferencesManager;
import mitm.application.djigzo.impl.DefaultUserPreferenceMerger;
import mitm.application.djigzo.impl.DefaultUserPreferencesSelector;
import mitm.application.djigzo.impl.DjigzoFactoryPropertiesImpl;
import mitm.application.djigzo.impl.hibernate.UserManagerHibernate;
import mitm.application.djigzo.impl.hibernate.UserPreferencesCategoryManagerHibernate;
import mitm.application.djigzo.james.DefaultMessageOriginatorIdentifier;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.phonebook.PhoneBook;
import mitm.application.djigzo.phonebook.UserPropertiesSMSPhoneBook;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.impl.KeyAndCertificateWorkflowImpl;
import mitm.application.djigzo.workflow.impl.UserWorkflowImpl;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.PropertiesResolver;
import mitm.common.properties.StandardHierarchicalProperties;
import mitm.common.properties.PropertiesResolver.PropertiesResolverException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.smime.selector.CertificateSelector;
import mitm.common.security.smime.selector.EncryptionCertificateSelector;
import mitm.common.sms.SMSGateway;
import mitm.common.sms.SMSTransport;
import mitm.common.sms.hibernate.SMSGatewayImpl;
import mitm.common.util.ResourceLocator;

public class DjigzoServices
{    
    /** 
     * Builds the UserPreferencesCategoryManager service.
     */
    public static UserPreferencesCategoryManager buildUserPreferencesCategoryManager(PKISecurityServices pKISecurityServices,
            SessionManager sessionManager)
    {
        /*
         * The implementation of UserPreferencesCategoryManager must be thread safe because the service 
         * will be a singleton.
         */          
        UserPreferencesCategoryManager categoryManager = new UserPreferencesCategoryManagerHibernate(
                pKISecurityServices.getKeyAndCertStore(), pKISecurityServices.getEncryptor(),
                sessionManager);
        
        return categoryManager;
    }
    
    /** 
     * Builds the FactoryProperties service.
     */
    public static DjigzoFactoryProperties buildDjigzoFactoryProperties(String factoryPropertiesResource,
    		Encryptor encryptor) 
    throws IOException, PropertiesResolverException
    {
        Properties properties = new Properties();

        ResourceLocator resourceLocator = new ResourceLocator();
        
        /* the factory defaults file can be overridden by a system property */
        resourceLocator.setSystemProperty("djigzoFactoryProperties");
        
        InputStream propertyStream = resourceLocator.getResourceAsStream(factoryPropertiesResource);
        
        if (propertyStream != null) 
        {
            properties.load(propertyStream);
            
            /*
             * The loaded factory properties can contain special properties with a binding (like file:) which
             * need to be resolved
             */
            PropertiesResolver propertiesResolver = new PropertiesResolver();
            
            propertiesResolver.resolve(properties);
        }
        
        /*
         * The implementation of HierarchicalProperties must be thread safe because the service 
         * will be a singleton.
         */          
        HierarchicalProperties factoryProperties = new StandardHierarchicalProperties(
                UserPreferencesCategory.GLOBAL.getName(), null, properties, encryptor);

        return new DjigzoFactoryPropertiesImpl(factoryProperties); 
    }
    
    /** 
     * Builds the GlobalPreferencesManager service.
     */
    public static GlobalPreferencesManager buildGlobalPreferencesManager(UserPreferencesCategoryManager userPreferencesCategoryManager,
            DjigzoFactoryProperties djigzoFactoryProperties)
    {
        /*
         * The implementation of GlobalPreferencesManager must be thread safe because the service 
         * will be a singleton.
         */          
        return new DefaultGlobalPreferencesManager(userPreferencesCategoryManager, djigzoFactoryProperties);
    }
    
    /**
     * Builds the UserPreferenceMerger service.
     */
    public static UserPreferenceMerger buildUserPreferenceMerger() 
    {
        /*
         * The implementation of UserPreferenceMerger must be thread safe because the service 
         * will be a singleton.
         */          
        return new DefaultUserPreferenceMerger();
    }
    
    /**
     * Builds the DomainManager service.
     */
    public static DomainManager buildDomainManager(UserPreferencesCategoryManager userPreferencesCategoryManager,
    		GlobalPreferencesManager globalPreferencesManager) 
    {
        /*
         * The implementation of DomainManager must be thread safe because the service 
         * will be a singleton.
         */          
        return new DefaultDomainManager(userPreferencesCategoryManager, globalPreferencesManager);
    }
    
    /**
     * Builds the EncryptionRecipientSelector service.
     */
    public static EncryptionRecipientSelector buildEncryptionRecipientSelector(PKISecurityServices pKISecurityServices,
            DjigzoFactoryProperties factoryProperties) 
    throws HierarchicalPropertiesException
    {
        boolean checkValidity = factoryProperties.isEncryptionRecipientSelectorCheckValidity();
        
        /*
         * The implementation of EncryptionRecipientSelector must be thread safe because the service 
         * will be a singleton.
         */          
        return new DefaultEncryptionRecipientSelector(pKISecurityServices, checkValidity);
    }
    
    /**
     * Builds the UserPreferencesSelector service.
     */
    public static UserPreferencesSelector buildUserPreferencesSelector(DomainManager domainManager) 
    {
        /*
         * The implementation of UserPreferencesSelector must be thread safe because the service 
         * will be a singleton.
         */          
        return new DefaultUserPreferencesSelector(domainManager);
    }
    
    /**
     * Builds the EncryptionCertificateSelector service.
     */
    public static CertificateSelector buildEncryptionCertificateSelector(PKISecurityServices pKISecurityServices) {
        return new EncryptionCertificateSelector(pKISecurityServices);
    }
    
    /**
     * Builds the UserManager service.
     */
    public static UserManager buildUserManager(PKISecurityServices pKISecurityServices, CertificateSelector encryptionCertificateSelector,
            SessionManager sessionManager)
    {
        UserManager userManager = new UserManagerHibernate(pKISecurityServices, encryptionCertificateSelector,
        		sessionManager);
        
        return userManager;
    }
    
    /**
     * Builds the UserWorkflow service.
     */
    public static UserWorkflow buildUserWorkflow(UserManager userManager, GlobalPreferencesManager globalPreferencesManager, 
            UserPreferencesSelector userPreferencesSelector, UserPreferenceMerger userPreferenceMerger)
    {
        return new UserWorkflowImpl(userManager, globalPreferencesManager, userPreferencesSelector, userPreferenceMerger);
    }
    
    /**
     * Builds the CertStoreCertificateWorkflow service.
     */
    public static KeyAndCertificateWorkflow buildKeyAndCertificateWorkflow(KeyAndCertStore keyAndCertStore,
            CertificatePathBuilderFactory pathBuilderFactory, SessionManager sessionManager)
    {
        return new KeyAndCertificateWorkflowImpl(keyAndCertStore, pathBuilderFactory, 
        		sessionManager);
    }

    /**
     * Builds the MessageOriginatorIdentifier service.
     */
    public static MessageOriginatorIdentifier buildMessageOriginatorIdentifier() {
        return new DefaultMessageOriginatorIdentifier();
    }

    /**
     * Builds the PhoneBook service.
     */
    public static PhoneBook buildUserPropertiesSMSPhoneBook(UserWorkflow userWorkflow) {
        return new UserPropertiesSMSPhoneBook(userWorkflow);
    }
    
    /**
     * Builds the SMSGateway service.
     */
    public static SMSGateway buildSMSGateway(SMSTransport transport, SessionManager sessionManager,
            DjigzoFactoryProperties factoryProperties, long expirationTime) 
    {
        SMSGatewayImpl gateway = new SMSGatewayImpl(transport, null, sessionManager);
        
        gateway.setExpirationTime(expirationTime);
        
        return gateway;
    }
}
