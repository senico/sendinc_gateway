/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.xml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mitm.application.djigzo.xml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _XmlStore_QNAME = new QName("", "djigzo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mitm.application.djigzo.xml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link XMLUser }
     * 
     */
    public XMLUser createXMLUser() {
        return new XMLUser();
    }

    /**
     * Create an instance of {@link XMLProperty }
     * 
     */
    public XMLProperty createXMLProperty() {
        return new XMLProperty();
    }

    /**
     * Create an instance of {@link XMLStore }
     * 
     */
    public XMLStore createXMLStore() {
        return new XMLStore();
    }

    /**
     * Create an instance of {@link XMLDomain }
     * 
     */
    public XMLDomain createXMLDomain() {
        return new XMLDomain();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLStore }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "djigzo")
    public JAXBElement<XMLStore> createXmlStore(XMLStore value) {
        return new JAXBElement<XMLStore>(_XmlStore_QNAME, XMLStore.class, null, value);
    }
}
