/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp.xml;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

/**
 * XMLPolicyPatternFactory is used for marshalling/unmarshalling XMLPolicyPatternStore to/from xml 
 * using JAXB. 
 * 
 * @author Martijn Brinkers
 *
 */
public class XMLPolicyPatternFactory 
{
	public static XMLPolicyPatternStore unmarshall(InputStream input) 
	throws JAXBException
	{
		XMLPolicyPatternStore xmlStore = null;
		
		/*
		 * Create an instance of JAXBContext and make sure it can find all the classes it needs
		 * for building the object model from the XML file.
		 */
		JAXBContext jcontext = JAXBContext.newInstance(XMLPolicyPatternStore.class.getPackage().getName());
		
		Unmarshaller unmarshaller = jcontext.createUnmarshaller();
		
		Object object = unmarshaller.unmarshal(input);
		
		if (object instanceof JAXBElement) 
		{
			JAXBElement<?> element = (JAXBElement<?>) object;
			
			if (element.getValue() instanceof XMLPolicyPatternStore) 
			{
				xmlStore = (XMLPolicyPatternStore) element.getValue();
			}
		}
		
		return xmlStore;
	}
	
	public static void marshall(XMLPolicyPatternStore xmlStore, OutputStream output) 
	throws JAXBException
	{
		/*
		 * Create an instance of JAXBContext and make sure it can find all the classes it needs
		 * for building the object model from the XML file.
		 */
		JAXBContext jcontext = JAXBContext.newInstance(XMLPolicyPatternStore.class.getPackage().getName());
	
		Marshaller marshaller = jcontext.createMarshaller();

        /*
         * Pretty print the XML
         */
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		/*
		 * marshal without requiring a @XmlRootElement
		 * 
		 * See: http://weblogs.java.net/blog/kohsuke/archive/2006/03/why_does_jaxb_p.html
		 */
		marshaller.marshal(new JAXBElement<XMLPolicyPatternStore>(
				  new QName(null, "policyPatterns"), XMLPolicyPatternStore.class, xmlStore), output);
	}
}