/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp.xml;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * XMLPolicyPatternGroup is to marshall and unmarshall to/from XML using JAXB. 
 * 
 * @author Martijn Brinkers
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(name = XMLPolicyPatternGroup.XML_TYPE_NAME)
public class XMLPolicyPatternGroup
{
    protected final static String XML_TYPE_NAME = "PolicyPatternGroup";
    
    /*
     * Name of the group
     */
    private String name;
    
    /*
     * The names of the child patterns
     */
    private Set<String> childNames = new HashSet<String>();

    public XMLPolicyPatternGroup() {
        /*
         * JAXB requires a default constructor
         */
    }
    
    public XMLPolicyPatternGroup(String name) {
        this.name = name;
    }
        
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name ;
    }
    
    @XmlElement(name = "child")
    public Set<String> getChildNames() {
        return childNames;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof XMLPolicyPatternGroup)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        XMLPolicyPatternGroup rhs = (XMLPolicyPatternGroup) obj;
        
        return new EqualsBuilder()
            .append(name, rhs.name)
            .isEquals();
    }
    
    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().
            append(name).
            toHashCode();
    }
}
