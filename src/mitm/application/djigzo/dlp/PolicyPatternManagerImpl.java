/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;

import mitm.application.djigzo.NamedBlobCategories;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.dlp.xml.XMLPolicyPatternFactory;
import mitm.application.djigzo.dlp.xml.XMLPolicyPatternGroup;
import mitm.application.djigzo.dlp.xml.XMLPolicyPatternStore;
import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyPatternMarshaller;
import mitm.common.dlp.impl.PolicyPatternImpl;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.NamedBlobManager;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link PolicyPatternManager}.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternManagerImpl implements PolicyPatternManager
{
    private final static Logger logger = LoggerFactory.getLogger(PolicyPatternManagerImpl.class);
    
    /*
     * Sane maximum which should with normal usage never be reached
     */
    private final static int MAX_IN_USE_INFO_ITEMS = 100;
    
    /*
     * The named blob category to use.
     */
    private final static String CATEGORY = NamedBlobCategories.DLP_POLICY_PATTERN_CATEGORY; 
    /*
     * Is used to persistently store the PolicyPatternNode's.
     */
    private final NamedBlobManager namedBlobManager;

    /*
     * Used for marshalling/unmarshalling PolicyPattern's to/from xml.
     */
    private final PolicyPatternMarshaller policyPatternMarshaller;

    /*
     * Used for checking whether a pattern is used by a UserPreferences.
     */
    private final UserPreferencesWorkflow userPreferencesWorkflow;
    
    public PolicyPatternManagerImpl(NamedBlobManager namedBlobManager, PolicyPatternMarshaller policyPatternMarshaller,
            UserPreferencesWorkflow userPreferencesWorkflow)
    {
        Check.notNull(namedBlobManager, "namedBlobManager");
        Check.notNull(policyPatternMarshaller, "policyPatternMarshaller");
        Check.notNull(userPreferencesWorkflow, "userPreferencesWorkflow");
        
        this.namedBlobManager = namedBlobManager;
        this.policyPatternMarshaller = policyPatternMarshaller;
        this.userPreferencesWorkflow = userPreferencesWorkflow;
    }
    
    @Override
    public PolicyPatternNode createPattern(String name)
    {
        return new PolicyPatternNodeImpl(namedBlobManager.createNamedBlob(CATEGORY, name), policyPatternMarshaller);
    }

    @Override
    public void deletePattern(String name) {
        namedBlobManager.deleteNamedBlob(CATEGORY, name);
    }

    @Override
    public PolicyPatternNode getPattern(String name)
    {
        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);
        
        return namedBlob != null ? new PolicyPatternNodeImpl(namedBlob, policyPatternMarshaller) : null;
    }

    @Override
    public List<PolicyPatternNode> getPatterns(Integer firstResult, Integer maxResults)
    {
        List<PolicyPatternNode> patterns = new LinkedList<PolicyPatternNode>();
        
        List<? extends NamedBlob> namedBlobs = namedBlobManager.getByCategory(CATEGORY, firstResult, maxResults);
 
        if (CollectionUtils.getSize(namedBlobs) > 0)
        {
            for (NamedBlob namedBlob : namedBlobs) {
                patterns.add(new PolicyPatternNodeImpl(namedBlob, policyPatternMarshaller));
            }
        }
        
        return patterns;
    }
    
    @Override
    public int getNumberOfPatterns() {
        return namedBlobManager.getByCategoryCount(CATEGORY);
    }    

    @Override
    public boolean isInUse(String name)
    {
        long refs = 0;
        
        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);

        if (namedBlob != null)
        {
            /*
             * Check if used by a UserPreference 
             */
            refs = userPreferencesWorkflow.getReferencingFromNamedBlobsCount(namedBlob);
            
            if (refs == 0) {
                /*
                 * Check if the pattern is used by a group
                 */
                refs = namedBlobManager.getReferencedByCount(namedBlob);
            }
        }
        
        return refs > 0;
    }

    @Override
    public List<String> getInUseInfo(String name)
    {
        List<String> result = new LinkedList<String>();
        
        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);

        if (namedBlob != null)
        {
            /*
             * Check if used by a UserPreference
             * 
             * Note: use a sane maximum which should with normal usage never be reached 
             */
            List<UserPreferences> referencing = userPreferencesWorkflow.getReferencingFromNamedBlobs(
                    namedBlob, null, MAX_IN_USE_INFO_ITEMS);
            
            if (CollectionUtils.isNotEmpty(referencing))
            {
                for (UserPreferences userPreferences :referencing)
                {
                    result.add("user," + userPreferences.getCategory() + 
                            "," + userPreferences.getName());
                }
            }
            
            /*
             * Check if the pattern is used by a group
             * 
             * Note: use a sane maximum which should with normal usage never be reached 
             */
            List<? extends NamedBlob> blobs = namedBlobManager.getReferencedBy(namedBlob, 
                    null, MAX_IN_USE_INFO_ITEMS);

            if (CollectionUtils.isNotEmpty(blobs))
            {
                for (NamedBlob blob : blobs) {
                    result.add("pattern,group," + blob.getName());
                }
            }
        }
        
        return result;
    }
        
    private void exportToXML(XMLPolicyPatternStore store, PolicyPatternNode node, OutputStream output)
    throws IOException
    {
        PolicyPattern pattern = node.getPolicyPattern(); 
        
        if (pattern != null)
        {
            if (!(pattern instanceof PolicyPatternImpl)) {
                throw new IOException("PolicyPattern is not a PolicyPatternImpl.");
            }
            
            store.getPatterns().add((PolicyPatternImpl) node.getPolicyPattern());
        }
        else {
            XMLPolicyPatternGroup group = new XMLPolicyPatternGroup(node.getName());

            for (PolicyPatternNode child : node.getChilds())
            {
                exportToXML(store, child, output);

                group.getChildNames().add(child.getName());
            }

            store.getGroups().add(group);
        }
    }
    
    @Override
    public void exportToXML(Collection<String> names, OutputStream output)
    throws IOException
    {
        if (CollectionUtils.isEmpty(names)) {
            throw new IOException("names is empty.");
        }
        
        XMLPolicyPatternStore store = new XMLPolicyPatternStore();
        
        for (String name : names)
        {
            PolicyPatternNode node = getPattern(name);
            
            if (node == null) {
                throw new IOException("Pattern with name " + name + " does not exist.");
            }
            
            exportToXML(store, node, output);
        }
        
        try {
            XMLPolicyPatternFactory.marshall(store, output);
        }
        catch (JAXBException e) {
            throw new IOException(e);
        }
    }
    
    @Override
    public void importFromXML(InputStream xmlStream, boolean skipExisting)
    throws IOException
    {
        try
        {
            XMLPolicyPatternStore store = XMLPolicyPatternFactory.unmarshall(xmlStream);
            
            for (PolicyPatternImpl pattern : store.getPatterns())
            {
                String name = pattern.getName();
                
                if (StringUtils.isEmpty(name)) {
                    throw new IOException("Pattern name is missing.");
                }
                
                if (getPattern(name) != null)
                {
                    if (skipExisting)
                    {
                        logger.info("Pattern with name " + name + " skipped because it already exists.");
                        
                        continue;
                    }
                    
                    throw new IOException("Pattern with name " + name + " already exists.");
                }
                
                PolicyPatternNode node = createPattern(name);
                
                node.setPolicyPattern(pattern);
            }
            
            for (XMLPolicyPatternGroup group : store.getGroups())
            {
                String name = group.getName();
                
                if (StringUtils.isEmpty(name)) {
                    throw new IOException("Group name is missing.");
                }
                
                if (getPattern(name) != null)
                {
                    if (skipExisting)
                    {
                        logger.info("Pattern with name " + name + " skipped because it already exists.");
                        
                        continue;
                    }
                    
                    throw new IOException("Pattern with name " + name + " already exists.");
                }
                
                PolicyPatternNode node = createPattern(name);
                
                for (String childName : group.getChildNames())
                {
                    if (StringUtils.isEmpty(childName)) {
                        throw new IOException("Child name is missing.");
                    }
                    
                    /*
                     * Lookup the node and add it as a child
                     */
                    PolicyPatternNode child = getPattern(childName);
                    
                    if (child == null) {
                        throw new IOException("Pattern with name " + childName + " does not exists.");
                    }
                    
                    node.getChilds().add(child);
                }
            }
        }
        catch (JAXBException e) {
            throw new IOException(e);
        }
    }
}
