/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;

import mitm.common.dlp.impl.RegExpPolicyChecker;

/**
 * Used for managing a store of {@link PolicyPatternNode} used by {@link RegExpPolicyChecker}
 * 
 * @author Martijn Brinkers
 *
 */
public interface PolicyPatternManager
{
    /**
     * Creates and adds a new pattern. The name of the pattern should be unique. 
     */
    public PolicyPatternNode createPattern(String name);

    /**
     * Returns the pattern with the given name.
     */
    public PolicyPatternNode getPattern(String name);

    /**
     * Returns all patterns starting at firstResult and max results maxResults.
     */
    public List<PolicyPatternNode> getPatterns(Integer firstResult, Integer maxResults);

    /**
     * Returns the total number of available patterns
     */
    public int getNumberOfPatterns();
    
    /**
     * Deletes the pattern with the given name.
     */
    public void deletePattern(String name);
    
    /**
     * Returns true if the pattern is in use
     */
    public boolean isInUse(String name);

    /**
     * Returns info about the usage of this pattern. Returns an empty list if the 
     * pattern is not in use (i.e., isInUse is false). The returned list is a 
     * list of comma separated strings.
     * 
     * Note: the returned info should only be used for displaying purposes.
     */
    public List<String> getInUseInfo(String name);
    
    /**
     * Exports all the PolicyPatternNode's to xml
     */
    public void exportToXML(Collection<String> names, OutputStream output)
    throws IOException;
    
    /**
     * Exports all the PolicyPatternNode's from xml
     */
    public void importFromXML(InputStream xmlStream, boolean skipExisting)
    throws IOException;
}
