/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mitm.common.dlp.PolicyPattern;
import mitm.common.util.CollectionUtils;

/**
 * Helper methods
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternNodeUtils
{
    /**
     * Returns all the PolicyPattern's from the collection of PolicyPatternNode's. Returns null if
     * policyPatternNodes is null. All petterns are returned including ALL child patterns. 
     */
    public static Set<PolicyPattern> getPolicyPatterns(Collection<PolicyPatternNode> policyPatternNodes) {
        return internalGetPolicyPatterns(policyPatternNodes, new HashSet<PolicyPatternNode>());
    }
    
    /**
     * Returns all the PolicyPattern's from the collection of PolicyPatternNode's. Returns null if
     * policyPatternNodes is null. All petterns are returned including ALL child patterns. 
     */
    private static Set<PolicyPattern> internalGetPolicyPatterns(Collection<PolicyPatternNode> policyPatternNodes,
            Set<PolicyPatternNode> seen)
    {
        Set<PolicyPattern> patterns = null;
        
        if (policyPatternNodes != null)
        {
            patterns = new HashSet<PolicyPattern>(CollectionUtils.getSize(policyPatternNodes));
            
            for (PolicyPatternNode node : policyPatternNodes) 
            {
                /*
                 * If we already saw the node, don't handle again to prevent
                 * indefinite recursion when there is a loop.
                 */
                if (node == null || seen.contains(node)) {
                    continue;
                }
                
                seen.add(node);
                
                PolicyPattern pattern = node.getPolicyPattern();
                
                if (pattern != null) {
                    patterns.add(node.getPolicyPattern());
                }
                
                Set<PolicyPatternNode> childs = node.getChilds();

                if (CollectionUtils.isNotEmpty(childs)) {
                    /*
                     * Recursively get all childs
                     */
                    Set<PolicyPattern> childPatterns = internalGetPolicyPatterns(childs, seen);
                    
                    if (childPatterns != null) {
                        patterns.addAll(childPatterns);
                    }
                }
            }
        }
        
        return patterns;
    }
}
