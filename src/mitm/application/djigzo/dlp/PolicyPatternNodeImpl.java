/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.Set;

import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyPatternMarshaller;
import mitm.common.properties.NamedBlob;
import mitm.common.util.Check;
import mitm.common.util.MarshallException;
import mitm.common.util.ObservableSet;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of PolicyPatternNode that reads/writes the data to a wrapped NamedBlob.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternNodeImpl implements PolicyPatternNode
{
    private final static Logger logger = LoggerFactory.getLogger(PolicyPatternNodeImpl.class);
    
    private final NamedBlob namedBlob;
    
    private final PolicyPatternMarshaller policyPatternMarshaller;
    
    public PolicyPatternNodeImpl(NamedBlob namedBlob, PolicyPatternMarshaller policyPatternMarshaller)
    {
        Check.notNull(namedBlob, "namedBlob");
        Check.notNull(policyPatternMarshaller, "policyPatternMarshaller");
        
        this.namedBlob = namedBlob;
        this.policyPatternMarshaller = policyPatternMarshaller;
    }
    
    private boolean onAddChild(PolicyPatternNode pattern)
    {
        if (!(pattern instanceof PolicyPatternNodeImpl)) {
            throw new IllegalArgumentException("Pattern must be a PolicyPatternNodeImpl.");
        }
        
        PolicyPatternNodeImpl patternImpl = (PolicyPatternNodeImpl) pattern;
        
        return namedBlob.getNamedBlobs().add(patternImpl.namedBlob);
    }

    private boolean onRemoveChild(Object pattern)
    {
        if (!(pattern instanceof PolicyPatternNodeImpl)) {
            throw new IllegalArgumentException("Pattern must be a PolicyPatternNodeImpl.");
        }

        PolicyPatternNodeImpl patternImpl = (PolicyPatternNodeImpl) pattern;
        
        return namedBlob.getNamedBlobs().remove(patternImpl.namedBlob);
    }

    @Override
    public void setName(String name) {
        namedBlob.setName(name);
    }
    
    @Override
    public String getName() {
        return namedBlob.getName();
    }
    
    @Override
    public Set<PolicyPatternNode> getChilds()
    {
        Set<NamedBlob> childBlobs = namedBlob.getNamedBlobs();

        Set<PolicyPatternNode> patterns = new HashSet<PolicyPatternNode>();

        if (childBlobs != null)
        {
            for (NamedBlob childBlob : childBlobs) {
                patterns.add(new PolicyPatternNodeImpl(childBlob, policyPatternMarshaller));
            }
        }
        
        /* create a Set instance that wraps-up the PolicyPatternNode set so we can intercept changes 
         * (like add and remove) made to the set. We need this to reflect these 
         * changes to the database
         */
        ObservableSet<PolicyPatternNode> observer = new ObservableSet<PolicyPatternNode>(patterns);
        
        observer.setAddEvent(new ObservableSet.Event<PolicyPatternNode>()
        {
            @Override
            public boolean event(PolicyPatternNode pattern) {
                return onAddChild(pattern);
            }
        });

        observer.setRemoveEvent(new ObservableSet.Event<Object>()
        {
            @Override
            public boolean event(Object pattern) {
                return onRemoveChild(pattern);
            }
        });
        
        return observer;
    }

    @Override
    public PolicyPattern getPolicyPattern()
    {
        PolicyPattern pattern = null;
        
        byte[] xml = namedBlob.getBlob();
        
        if (xml != null)
        {
            try {
                pattern = policyPatternMarshaller.unmarshall(new ByteArrayInputStream(xml));
            }
            catch (MarshallException e) {
                logger.error("Unable to unmarshall pattern.", e);
            }
        }
        
        return pattern;
    }
    
    @Override
    public void setPolicyPattern(PolicyPattern pattern)
    {
        byte[] xml = null;
        
        if (pattern != null)
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            try {
                policyPatternMarshaller.marshall(pattern, bos);
                
                xml = bos.toByteArray();
            }
            catch (MarshallException e) {
                logger.error("Unable to marshall pattern.", e);
            }
        }
        
        namedBlob.setBlob(xml);
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternNode)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        PolicyPatternNode rhs = (PolicyPatternNode) obj;
        
        return new EqualsBuilder()
            .append(getName(), rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getName())
            .toHashCode();    
    }

    public NamedBlob getNamedBlob() {
        return namedBlob;
    }
}
