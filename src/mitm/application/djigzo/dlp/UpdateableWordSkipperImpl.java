/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashSet;
import java.util.Set;

import mitm.application.djigzo.NamedBlobCategories;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.NamedBlobManager;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;
import mitm.common.util.UnicodeReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;

/**
 * UpdateableWordSkipper implementation that reads and writes the list of words to skip to/from a {@link NamedBlob}.
 * 
 * @author Martijn Brinkers
 *
 */
public class UpdateableWordSkipperImpl implements UpdateableWordSkipper 
{
    /*
     * The named blob category to use.
     */
    private final static String NAMED_BLOB_CATEGORY = NamedBlobCategories.DLP_WORD_SKIPPER_CATEGORY;

    /*
     * The named blob name under which the skip list is stored.
     */
    private final static String NAMED_BLOB_KEY = "skipList";
    
    /*
     * Is used to persistently store the word list.
     */
    private final NamedBlobManager namedBlobManager;
    
    /*
     * Handles database sessions.
     */
    private final SessionManager sessionManager;
    
    /*
     * The set of words to skip. The words will be stored with lowercase.
     */
    private Set<String> skipWords;
    
    /*
     * helper class proxy to start a database transaction when @StartTransaction is detected.
     */
    public static class AutoTransactProxy
    {
        /*
         * SessionManagedAutoCommitProxyFactory requires that AutoTransactProxy is static. We therefore
         * need to store a reference to the outer class.
         */
        private UpdateableWordSkipperImpl that;
                
        @StartTransaction
        public void loadSkipList(File file)
        throws IOException
        {
            /*
             * First see whether the list can be loaded from the Named Blobs. If not try to load it from
             * the file and store the contents from the read file in the named blobs
             */
            NamedBlob namedBlob = that.namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);
            
            if (namedBlob != null) {
                loadList(namedBlob);
            }
            else {
                loadFromFile(file);
            }
        }

        private void loadList(NamedBlob namedBlob)
        throws IOException
        {
            that.skipWords = that.createSkipWords();

            byte[] blob = namedBlob.getBlob();
            
            if (blob != null)
            {
                /*
                 * The list is stored as a UTF-8 encoded string
                 */
                try {
                    initializeSkipList(new StringReader(new String(blob, CharEncoding.UTF_8)));
                }
                catch (UnsupportedEncodingException e) {
                    throw new IOException(e);
                }
            }
        }

        private void storeList()
        {
            NamedBlob namedBlob = that.namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);
            
            if (namedBlob == null) {
                namedBlob = that.namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);
            }
            
            namedBlob.setBlob(MiscStringUtils.toUTF8Bytes(that.getSkipList()));
        }
        
        private void loadFromFile(File file)
        throws IOException
        {
            if (!file.exists()) {
                throw new IOException("File " + file.getAbsolutePath() + " does not exist.");
            }
            
            BufferedInputStream bufferdInput = new BufferedInputStream(new FileInputStream(file));
            
            try {
                /*
                 * Read the file using a Unicode aware reader
                 */
                initializeSkipList(new UnicodeReader(bufferdInput, CharEncoding.UTF_8));
            }
            finally {
                IOUtils.closeQuietly(bufferdInput);
            }
        }
        
        @StartTransaction
        public void initializeSkipList(Reader reader)
        {
            that.skipWords = that.createSkipWords();
            
            LineIterator lineIterator = new LineIterator(reader);
                    
            while (lineIterator.hasNext())
            {
                String line = StringUtils.trimToNull(lineIterator.nextLine());
                
                if (line == null || line.startsWith("#")) {
                    continue;
                }
                
                String[] words = StringUtils.split(line);
                
                for (String word : words) {
                    that.skipWords.add(word.toLowerCase());
                }
            }
            
            storeList();
        }
    }
    
    public UpdateableWordSkipperImpl(NamedBlobManager namedBlobManager, SessionManager sessionManager, File skipList)
    throws IOException
    {
        Check.notNull(namedBlobManager, "namedBlobManager");
        Check.notNull(sessionManager, "sessionManager");
        Check.notNull(skipList, "skipList");
        
        this.namedBlobManager = namedBlobManager;
        this.sessionManager = sessionManager;

        try {
            createProxy().loadSkipList(skipList);
        }
        catch (ProxyFactoryException e) {
            throw new IOException(e);
        }
        catch (NoSuchMethodException e) {
            throw new IOException(e);
        }
    }

    private Set<String> createSkipWords()
    {
        /*
         * Create a LinkedHashSet to keep the order of the words
         */
        return new LinkedHashSet<String>();
    }
    
    private AutoTransactProxy createProxy()
    throws ProxyFactoryException, NoSuchMethodException
    {
        AutoTransactProxy autoTransactProxy = new SessionManagedAutoCommitProxyFactory<AutoTransactProxy>(
                AutoTransactProxy.class, sessionManager).createProxy();
        
        autoTransactProxy.that = this;
        
        return autoTransactProxy;
    }

    @Override
    public synchronized boolean isSkip(String word)
    {
        if (word == null) {
            return true;
        }
        
        return skipWords != null && skipWords.contains(word.toLowerCase());
    }

    @Override
    public synchronized String getSkipList() {
        return skipWords != null ? StringUtils.join(skipWords, ' ') : "";
    }

    @Override
    public synchronized void setSkipList(String list)
    throws IOException
    {
        try {
            createProxy().initializeSkipList(new StringReader(list));
        }
        catch (ProxyFactoryException e) {
            throw new IOException(e);
        }
        catch (NoSuchMethodException e) {
            throw new IOException(e);
        }
    }
}
