/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import java.util.List;

import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SortDirection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * DAO for AdminEntity {@link AdminEntity}
 * 
 * @author Martijn Brinkers
 *
 */
public class AdminEntityDAO extends GenericHibernateDAO<AdminEntity, Long>
{
    private String entityName = AdminEntity.ENTITY_NAME;
        
    public AdminEntityDAO(SessionAdapter session) {
        super(session);
    }

    public AdminEntity getAdmin(String username)
    {
        Criteria criteria = createCriteria(entityName);

        criteria.add(Restrictions.eq(AdminEntity.USERNAME_COLUMN, username));

        return (AdminEntity) criteria.uniqueResult();
    }

    protected AdminEntity addAdmin(String username, boolean builtIn)
    {
    	AdminEntity admin = new AdminEntity(username, builtIn);
    	
    	admin = makePersistent(admin);
    	
    	return admin;
    }
    
    public boolean deleteAdmin(String username)
    {
    	boolean deleted = false;
    	
    	AdminEntity admin = getAdmin(username);
    	
    	if (admin != null) 
    	{
    		delete(admin);
    		
    		deleted = true;
    	}
    	
    	return deleted;
    }
    
    public void deleteAll() 
    {
    	List<AdminEntity> admins = getAdmins(null, null, null);
    	
    	for (AdminEntity admin : admins)
    	{
    		delete(admin);
    	}
    }

	@SuppressWarnings("unchecked")
	public List<AdminEntity> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection)
	{
        Criteria criteria = createCriteria(entityName);
        
        if (firstResult != null) {
        	criteria.setFirstResult(firstResult);
        }

        if (maxResults != null) {
        	criteria.setMaxResults(maxResults);
        }
        
        criteria.addOrder(sortDirection == SortDirection.DESC ? 
        		Order.desc(AdminEntity.USERNAME_COLUMN) : Order.asc(AdminEntity.USERNAME_COLUMN));
        
        return criteria.list();
	}
    
	public int getAdminCount() 
	{
        Criteria criteria = createCriteria(entityName);

        criteria.setProjection(Projections.rowCount());
        
        return (Integer) criteria.uniqueResult();        
	}
}
