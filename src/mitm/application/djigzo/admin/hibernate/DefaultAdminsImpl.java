/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.admin.DefaultAdmins;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;

/**
 * Utility class that adds default admin's and authority's.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultAdminsImpl implements DefaultAdmins
{	
	private final AdminManager adminManager;
	
	private final AuthorityManager authorityManager;
	
	private final String defaultPassword;
	
	public DefaultAdminsImpl(AdminManager adminManager, AuthorityManager authorityManager,
			String defaultPassword)
	{
		Check.notNull(adminManager, "adminManager");
		Check.notNull(authorityManager, "authorityManager");
		Check.notNull(defaultPassword, "defaultPassword");
		
		this.adminManager = adminManager;
		this.authorityManager = authorityManager;
		this.defaultPassword = defaultPassword;
	}
	
	private Authority addAuthority(String role)
	{
		Authority authority = authorityManager.getAuthority(role);
		
		if (authority == null) {
			authority = authorityManager.addAuthority(role);
		}
		
		return authority;
	}
	
	private Admin addAdmin(String username, String password)
	{
		Admin admin = adminManager.getAdmin(SYSTEM_ADMIN);
		
		if (admin == null) 
		{
			admin = adminManager.addAdmin(username, true /* built-in */);
			
			/*
			 * The password will be stored without any encoding (ie plain text). The default
			 * password should be changed. 
			 */
			admin.setPassword(password);
			admin.setPasswordEncoding(PasswordEncoding.NONE);
			admin.setEnabled(true);
		}

		return admin;
	}
	
	@Override
    @StartTransaction
	public void addDefaultAdminsAndAuthorities()
	{
		Authority adminAuthority = addAuthority(FactoryRoles.ROLE_LOGIN);
		Authority superAdminAuthority = addAuthority(FactoryRoles.ROLE_ADMIN);

		addAuthority(FactoryRoles.ROLE_USER_MANAGER);
		addAuthority(FactoryRoles.ROLE_DOMAIN_MANAGER);
		addAuthority(FactoryRoles.ROLE_GLOBAL_MANAGER);
		addAuthority(FactoryRoles.ROLE_TEMPLATE_MANAGER);
		addAuthority(FactoryRoles.ROLE_PKI_MANAGER);
		addAuthority(FactoryRoles.ROLE_SMS_MANAGER);
		addAuthority(FactoryRoles.ROLE_QUEUE_MANAGER);
        addAuthority(FactoryRoles.ROLE_LOG_MANAGER);
        addAuthority(FactoryRoles.ROLE_DLP_MANAGER);
        addAuthority(FactoryRoles.ROLE_QUARANTINE_MANAGER);
        addAuthority(FactoryRoles.ROLE_MOBILE_MANAGER);
        addAuthority(FactoryRoles.ROLE_PORTAL_MANAGER);
		
		Admin admin = addAdmin(DefaultAdmins.SYSTEM_ADMIN, defaultPassword);
		
		admin.getAuthorities().add(adminAuthority);
		admin.getAuthorities().add(superAdminAuthority);
	}
}
