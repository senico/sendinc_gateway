/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import java.util.List;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.Check;

/**
 * Admin manager implementation which uses Hibernate to store Admin's.
 * 
 * @author Martijn Brinkers
 *
 */
public class AdminManagerHibernate implements AdminManager 
{
    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
        	
    public AdminManagerHibernate(SessionManager sessionManager)
    {
    	Check.notNull(sessionManager,"sessionManager");
    	
    	this.sessionManager = sessionManager;
    }
    
	@Override
    public Admin addAdmin(String username, boolean builtIn)
	{
		Admin admin = getDAO().addAdmin(username, builtIn);
		
		return admin;
	}

	@Override
    public Admin getAdmin(String username) 
	{
    	return getDAO().getAdmin(username);
	}

    @Override
	public boolean deleteAdmin(String username) {
		return getDAO().deleteAdmin(username);
	}

    @Override
	public void deleteAll() {
		getDAO().deleteAll();
	}

    @Override
	public int getAdminCount() {
		return getDAO().getAdminCount();
	}

	@Override
    public List<? extends Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection) 
	{
		return getDAO().getAdmins(firstResult, maxResults, sortDirection);
	}
    
    private AdminEntityDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new AdminEntityDAO(session);
    }
}
