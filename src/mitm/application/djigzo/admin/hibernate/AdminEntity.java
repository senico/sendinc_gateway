/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Hibernate entity for the Admin (@See Admin)
 * 
 * @author Martijn Brinkers
 *
 */
@Entity(name = AdminEntity.ENTITY_NAME)
public class AdminEntity implements Admin
{
    public static final String ENTITY_NAME = "Admin";

    protected static final String USERNAME_COLUMN = "username";
    
	private static final int MAX_USERNAME_LENGTH = 255;
    private static final int MAX_PASSWORD_LENGTH = 255;
    private static final int MAX_SALT_LENGTH = 255;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = USERNAME_COLUMN, unique = true, nullable = false, length = MAX_USERNAME_LENGTH)
    private String username;

    @Column(nullable = true, length = MAX_PASSWORD_LENGTH)
    private String password;

    @Column(nullable = true)
    private PasswordEncoding passwordEncoding;

    @Column(nullable = true, length = MAX_SALT_LENGTH)
    private String salt;

    @Column
    private boolean builtIn;
    
    @Column
    private boolean enabled = false;
    
    @ManyToMany
    @Cascade(CascadeType.SAVE_UPDATE)
    private Set<AuthorityEntity> authorities = new HashSet<AuthorityEntity>();
    
    protected AdminEntity() {
    	/* Required by Hibernate */
    }

    public AdminEntity(String username, boolean builtIn) 
    {
    	Check.notNull(username,"username");

    	/* 
    	 * only accept usernames that are words
    	 */
		if (!username.matches("\\w*")) {
			throw new IllegalArgumentException("Username should be a word.");
		}
		
    	this.username = username;
    	this.builtIn = builtIn;
    }
    
    public Long getId() {
    	return id;
    }
    
    @Override
    public String getUsername() {
    	return username;
    }

    @Override
    public void setPassword(String password) {
    	this.password = password;
    }
    
    @Override
    public String getPassword() {
    	return password;
    }
    
	@Override
    public PasswordEncoding getPasswordEncoding() {
		return passwordEncoding;
	}

	@Override
    public void setPasswordEncoding(PasswordEncoding passwordEncoding) {
		this.passwordEncoding = passwordEncoding;
	}
    
	@Override
    public void setSalt(String salt) {
		this.salt = salt;
	}
	
	@Override
    public String getSalt() {
		return salt;
	}

    @Override
    public boolean isBuiltIn() {
    	return builtIn;
    }

    @Override
    public void setEnabled(boolean enabled) {
    	this.enabled = enabled;
    }
    
    @Override
    public boolean isEnabled() {
    	return enabled;
    }
    
	@Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<Authority> getAuthorities() 
	{
		/*
		 * For this implementation We only allow AuthorityEntity to the set but we want to keep the
		 * interface to be as clean as possible and not depend on anything Hibernate related.
		 */
    	return  (Set)Collections.checkedSet(authorities, AuthorityEntity.class);
    }
    
    @Override
    public String toString() {
        return getUsername();
    }
    
    /**
     * Admin is equals iff username is equal.
     */    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof AdminEntity)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        AdminEntity rhs = (AdminEntity) obj;
        
        return new EqualsBuilder()
            .append(username, rhs.username)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(username)
            .toHashCode();    
    }
}
