/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import mitm.application.djigzo.admin.Authority;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Hibernate entity implementation of {@link Authority}
 * 
 * @author Martijn Brinkers
 *
 */
@Entity(name = AuthorityEntity.ENTITY_NAME)
public class AuthorityEntity implements Authority
{
	protected static final String ROLE_COLUMN = "role";
	
    public static final String ENTITY_NAME = "Authority";
	
    private static final int MAX_ROLE_LENGTH = 255;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (name = ROLE_COLUMN, unique = true, nullable = false, length = MAX_ROLE_LENGTH)
    private String role;

    protected AuthorityEntity() {
    	/* Required by Hibernate */
    }

    public AuthorityEntity(String role)
    {
    	Check.notNull(role, "role");
    	
    	this.role = role;
    }
    
    public Long getId() {
    	return id;
    }
    
    @Override
    public String getRole() {
    	return role;
    }
    
    @Override
    public String toString() {
        return getRole();
    }
    
    /**
     * Authority is equals iff role is equal.
     */    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof AuthorityEntity)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        AuthorityEntity rhs = (AuthorityEntity) obj;
        
        return new EqualsBuilder()
            .append(role, rhs.role)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(role)
            .toHashCode();    
    }
}
