/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin;

/**
 * These roles are used by the built-in admin account.
 * 
 * @author Martijn Brinkers
 *
 */
public interface FactoryRoles 
{
	/**
	 * The general role every admin must have.
	 */
	public static String ROLE_LOGIN = "ROLE_LOGIN";

	/**
	 * This role allows all actions.
	 */
	public static String ROLE_ADMIN = "ROLE_ADMIN";
	
	/**
	 * Role that allows the admin to manage users (add, remove, edit etc.)
	 */
	public static String ROLE_USER_MANAGER = "ROLE_USER_MANAGER";
	
	/**
	 * Role that allows the admin to manage domains (add, remove, edit etc.)
	 */
	public static String ROLE_DOMAIN_MANAGER = "ROLE_DOMAIN_MANAGER";
	
	/**
	 * Role that allows the admin to manage the global settings
	 */
	public static String ROLE_GLOBAL_MANAGER = "ROLE_GLOBAL_MANAGER";

	/**
	 * Role that allows the admin to edit templates
	 */
	public static String ROLE_TEMPLATE_MANAGER = "ROLE_TEMPLATE_MANAGER";
	
	/**
	 * Role that allows the admin to manage PKI (add, remove, edit certificates/crls etc.)
	 */
	public static String ROLE_PKI_MANAGER = "ROLE_PKI_MANAGER";

	/**
	 * Role that allows the admin to manage SMS queue.
	 */
	public static String ROLE_SMS_MANAGER = "ROLE_SMS_MANAGER";
	
	/**
	 * Role that allows the admin to manage mail queues.
	 */
	public static String ROLE_QUEUE_MANAGER = "ROLE_QUEUE_MANAGER";
	
    /**
     * Role that allows the admin to view the log files
     */
    public static String ROLE_LOG_MANAGER = "ROLE_LOG_MANAGER";

    /**
     * Role that allows the admin to manage DLP
     */
    public static String ROLE_DLP_MANAGER = "ROLE_DLP_MANAGER";
    
    /**
     * Role that allows the admin to manage the quarantine
     */
    public static String ROLE_QUARANTINE_MANAGER = "ROLE_QUARANTINE_MANAGER";
    
    /**
     * Role that allows the admin to manage BlackBerry and other mobile related settings
     */
    public static String ROLE_MOBILE_MANAGER = "ROLE_MOBILE_MANAGER";    

    /**
     * Role that allows the admin to manage Portal settings
     */
    public static String ROLE_PORTAL_MANAGER = "ROLE_PORTAL_MANAGER";    

    /**
     * Role that allows the user to login to the portal
     */
    public static String ROLE_PORTAL_LOGIN = "ROLE_PORTAL_LOGIN";    

    /**
     * Role for anonymous access
     */
    public static String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";    
}
