/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin;

import java.util.Set;

/**
 * An Admin is allowed to login to the system to manage the server.
 * 
 * @author Martijn Brinkers
 *
 */
public interface Admin 
{
	/**
	 * Returns the login username
	 */
	public String getUsername();
	
	/**
	 * Sets the password. The caller is responsible for encoding the password (like hashing etc.)
	 */
	public void setPassword(String password);
	
	/**
	 * Returns the (possibly hashed) password
	 */
	public String getPassword();
	
	/**
	 * Must be set to match the way the password is encoded
	 */
	public void setPasswordEncoding(PasswordEncoding passwordEncoding);
	
	/**
	 * Returns the encoding used to encode the password.
	 */
	public PasswordEncoding getPasswordEncoding();
	
	/** 
	 * Sets the salt used to encode the password
	 */
	public void setSalt(String salt);
	
	/**
	 * Returns the salt for this admin.
	 */
	public String getSalt();
	
	/**
	 * Returns the list of granted authorities of this user
	 */
	public Set<Authority> getAuthorities();
	
	/**
	 * If the Admin is built-in the admin should not be deleted by the end user
	 */
	public boolean isBuiltIn();

	/**
	 * Enables or disables the admin
	 */
	public void setEnabled(boolean enabled);
	
	/**
	 * True if this account is enabled
	 */
	public boolean isEnabled();
}
