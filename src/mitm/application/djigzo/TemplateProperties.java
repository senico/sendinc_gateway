/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import mitm.common.properties.DefaultPropertyProvider;
import mitm.common.properties.HierarchicalPropertiesException;

/**
 * Java interface wrapper around hierarchical properties for the template properties
 * 
 * @author Martijn Brinkers
 *
 */
public interface TemplateProperties extends ResolvableHierarchicalProperties, DefaultPropertyProvider
{
	public final static String ENCRYPTED_PDF                  = "encryptedPdf";
    public final static String ENCRYPTED_PDF_SMS              = "encryptedPdfSms";
    public final static String ENCRYPTED_PDF_OTP              = "encryptedPdfOTP";
    public final static String ENCRYPTED_PDF_OTP_INVITE       = "encryptedPdfOTPInvite";
	public final static String ENCRYPTION_FAILED_NOTIFICATION = "encryptionFailedNotification";
    public final static String ENCRYPTION_NOTIFICATION        = "encryptionNotification";
    public final static String PASSWORDS_NOTIFICATION         = "passwordsNotification";
	public final static String SMS_PASSWORD                   = "smsPassword";
    public final static String BB_SMIME_ADAPTER               = "blackberrySMIMEAdapter";
    public final static String SMS_PFX_PASSWORD               = "smsPFXPassword";
    public final static String MAIL_PFX                       = "mailPFX";
    public final static String DLP_WARNING                    = "dlp.warning";
    public final static String DLP_QUARANTINE                 = "dlp.quarantine";
    public final static String DLP_BLOCK                      = "dlp.block";
    public final static String DLP_ERROR                      = "dlp.error";
    public final static String DLP_RELEASE_NOTIFICATION       = "dlp.release.notification";
    public final static String DLP_DELETE_NOTIFICATION        = "dlp.delete.notification";
    public final static String DLP_EXPIRE_NOTIFICATION        = "dlp.expire.notification";
    
    /**
     * The template (the source of the template) used for an encrypted PDF.
     */
    public void setEncryptedPdfTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptedPdfTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) used for an encrypted PDF for which the password is sent via SMS.
     */
    public void setEncryptedPdfSmsTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptedPdfSmsTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) used for an encrypted PDF for which the password is created with
     * the One Time Password (OTP) service.
     */
    public void setEncryptedPdfOTPTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptedPdfOTPTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) used for an encrypted PDF for which the password is created with
     * the One Time Password (OTP) service and inviting the user.
     */
    public void setEncryptedPdfOTPInviteTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptedPdfOTPInviteTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the notification when encryption failed
     */
    public void setEncryptionFailedNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptionFailedNotificationTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the notification when the message was encrypted
     */
    public void setEncryptionNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getEncryptionNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the notification with the generated passwords
     */
    public void setPasswordsNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getPasswordsNotificationTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the SMS with the password
     */
    public void setSMSPasswordTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getSMSPasswordTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the BB adapater message
     */
    public void setBBSMIMEAdapterTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getBBSMIMEAdapterTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the SMS sent with the PFX password
     */
    public void setSMSPFXPasswordTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getSMSPFXPasswordTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for mail with the PFX
     */
    public void setMailPFXTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getMailPFXTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the DLP warning message
     */
    public void setDLPWarningTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPWarningTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP quarantine warning message
     */
    public void setDLPQuarantineTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPQuarantineTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP block warning message
     */
    public void setDLPBlockTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPBlockTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the DLP error warning message
     */
    public void setDLPErrorTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPErrorTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the DLP release notification
     */
    public void setDLPReleaseNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPReleaseNotificationTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the DLP delete notification
     */
    public void setDLPDeleteNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPDeleteNotificationTemplate()
    throws HierarchicalPropertiesException;
    
    /**
     * The template (the source of the template) for the DLP expire notification
     */
    public void setDLPExpireNotificationTemplate(String template)
    throws HierarchicalPropertiesException;
    
    public String getDLPExpireNotificationTemplate()
    throws HierarchicalPropertiesException;
}
