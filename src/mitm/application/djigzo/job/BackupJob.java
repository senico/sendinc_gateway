/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.job;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.backup.DjigzoBackupPreferences;
import mitm.application.djigzo.backup.PropertiesDjigzoBackupPreferences;
import mitm.common.backup.BackupDriver;
import mitm.common.backup.BackupDriverFactory;
import mitm.common.backup.BackupException;
import mitm.common.backup.BackupMaker;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.util.Check;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Quartz job that will start a system backup
 * 
 * @author Martijn Brinkers
 *
 */
public class BackupJob extends QuartzJobBean
{
    private final static Logger logger = LoggerFactory.getLogger(BackupJob.class);
    
    /*
     * Used for creating a BackupDriver 
     */
    private BackupDriverFactory backupDriverFactory;

    /*
     * BackupMaker is responsible for doing the actual backup. It uses a BackupDriver to
     * write the backup to a device.
     */
    private BackupMaker backupMaker;
            
    /*
     * Used to create database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Use to get the global preferences
     */
    private GlobalPreferencesManager globalPreferencesManager;

    /*
     * Helper class that will start transactions when required.
     * 
     * Note: We would have liked to use @StartTransaction on executeInternal but this bean is created 
     * outside the reach of the Spring AOP intercepter.
     */
    private static class AutoTransactDelegator
    {
        @SuppressWarnings("unused")
        public AutoTransactDelegator() {
            /* required by AutoCommitProxyFactory */
        }
        
        @StartTransaction
        public void backup(BackupMaker backupMaker, BackupDriverFactory backupDriverFactory, 
                GlobalPreferencesManager globalPreferencesManager)
        throws BackupException 
        {
            try {
                DjigzoBackupPreferences backupPreferences = new PropertiesDjigzoBackupPreferences(
                                globalPreferencesManager.getGlobalUserPreferences().getProperties());
                
                if (backupPreferences.isAutomaticBackupEnabled())
                {
                    BackupDriver backupDriver = backupDriverFactory.createBackupDriver();
                    
                    String password = backupPreferences.getPassword();
                    
                    String backup = backupMaker.backup(backupDriver, password);
                                        
                    logger.info("Backup '" + backup + "' created.");                    
                }
                else {
                    logger.info("Automatic backup is not enabled.");
                }
            }
            catch(HierarchicalPropertiesException e) {
                throw new BackupException(e);
            }
        }
        
        public static AutoTransactDelegator createProxy(SessionManager sessionManager)
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory
                    <AutoTransactDelegator>(AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
    }
    
    public void setBackupDriverFactory(BackupDriverFactory backupDriverFactory) {
        this.backupDriverFactory = backupDriverFactory;
    }

    public void setBackupMaker(BackupMaker backupMaker) {
        this.backupMaker = backupMaker;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setGlobalPreferencesManager(GlobalPreferencesManager globalPreferencesManager) {
        this.globalPreferencesManager = globalPreferencesManager;
    }
    
    @Override
    protected void executeInternal(JobExecutionContext context)
    throws JobExecutionException
    {
        logger.info("BackupJob started.");

        Check.notNull(backupDriverFactory, "backupDriverFactory");
        Check.notNull(backupMaker, "backupMaker");
        Check.notNull(sessionManager, "sessionManager");
        Check.notNull(globalPreferencesManager, "globalPreferencesManager");
        
        try {
            AutoTransactDelegator.createProxy(sessionManager).backup(backupMaker, 
                    backupDriverFactory, globalPreferencesManager);
        }
        catch (Exception e)
        {
            logger.error("backup failed.", e);

            throw new JobExecutionException(e);
        }
    }    
}
