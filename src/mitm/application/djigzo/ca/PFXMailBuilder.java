/*
 * Copyright (c) 2009-2010, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ca;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.util.ByteArrayDataSource;

import mitm.application.djigzo.DjigzoHeader;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.mail.MailUtils;
import mitm.common.template.TemplateBuilder;
import mitm.common.template.TemplateBuilderException;
import mitm.common.util.Check;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.SimpleHash;

/**
 * PFXMailBuilder builds a new message from the provided template and PFX file.
 * 
 * PFXMailBuilder is not thread safe
 * 
 * @author Martijn Brinkers
 *
 */
public class PFXMailBuilder
{
    private final static Logger logger = LoggerFactory.getLogger(PFXMailBuilder.class);
    
    /*
     * The Freemarker template
     */
    private final String template;
    
    /*
     * for building and processing Freemarker templates.
     */
    private final TemplateBuilder templateBuilder;
    
    /*
     * The pfx to attach
     */
    private byte[] pfx;
    
    /*
     * The from of the final message
     */
    private InternetAddress from;

    /*
     * The recipient of the final message
     */
    private InternetAddress recipient;

    /*
     * Extra template properties
     */
    private Map<String, Object> properties;
    
    public PFXMailBuilder(String template, TemplateBuilder templateBuilder)
    {
        Check.notNull(template, "template");
        Check.notNull(templateBuilder, "templateBuilder");
        
        this.template = template;
        this.templateBuilder = templateBuilder;
    }

    /**
     * Sets the PFX to be added to the message. The PFX should be a valid PFX file (is not checked)
     */
    public void setPFX(byte[] pfx)
    {
        Check.notNull(pfx, "pfx");
        
        this.pfx = pfx;
    }
    
    /**
     * Sets the from of the email
     */
    public void setFrom(InternetAddress from)
    {
        Check.notNull(from, "from");
        
        this.from = from;
    }

    /**
     * Sets the to of the email
     */
    public void setRecipient(InternetAddress recipient)
    {
        Check.notNull(recipient, "recipient");
        
        this.recipient = recipient;
    }
    
    /**
     * Sets extra template properties to be used by the template 
     */
    public void addProperty(String name, Object value)
    {
        if (this.properties == null) {
            this.properties = new HashMap<String, Object>();
        }
        
        this.properties.put(name, value);
    }
    
    private void checkState()
    throws TemplateBuilderException
    {
        if (pfx == null) {
            throw new TemplateBuilderException("pfx must be set");
        }

        if (from == null) {
            throw new TemplateBuilderException("from must be set");
        }
    }
    
    protected String createBoundary()
    {
        String boundary = "=-" + UUID.randomUUID().toString();
        
        return boundary;
    }
    
    private void replacePFX(MimeMessage message)
    throws MessagingException
    {
        Multipart mp;

        try {
            mp = (Multipart) message.getContent();
        }
        catch (IOException e) {
            throw new MessagingException("Error getting message content.", e);
        }

        BodyPart pfxPart = null;

        /*
         * Fallback in case the template does not contain a DjigzoHeader.MARKER
         */
        BodyPart octetStreamPart = null;
        
        /*
         * Try to find the first attachment with X-Djigzo-Marker attachment header which should be the attachment
         * we should replace (we should replace the content and keep the headers)
         */        
        for (int i=0; i < mp.getCount(); i++)
        {
            BodyPart part = mp.getBodyPart(i);

            if (ArrayUtils.contains(part.getHeader(DjigzoHeader.MARKER), DjigzoHeader.ATTACHMENT_MARKER_VALUE))
            {
                pfxPart = part;

                break;
            }
            
            /*
             * Fallback scanning for octet-stream in case the template does not contain a DjigzoHeader.MARKER
             */
            if (part.isMimeType("application/octet-stream")) {
                octetStreamPart = part;
            }
        }

        if (pfxPart == null)
        {
            if (octetStreamPart != null)
            {
                logger.info("Marker not found. Using ocet-stream instead.");
                
                /*
                 * Use the octet-stream part
                 */
                pfxPart = octetStreamPart;
            }
            else {
                throw new MessagingException("Unable to find the attachment part in the template.");
            }
        }

        pfxPart.setDataHandler(new DataHandler(new ByteArrayDataSource(pfx, "application/octet-stream")));
    }
    
    public MimeMessage createMessage()
    throws TemplateBuilderException
    {
        try {
            checkState();
            
            SimpleHash root = new SimpleHash();
    
            root.put("from",           from);
            root.put("recipient",      recipient);
            root.put("recipientLocal", EmailAddressUtils.getLocalPart(recipient));
            root.put("boundary",       createBoundary());

            if (properties != null) {
                root.putAll(properties);
            }
            
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
    
            templateBuilder.buildTemplate(template, root, bos);
    
            MimeMessage templatedMessage = MailUtils.byteArrayToMessage(bos.toByteArray());
            
            templatedMessage.setFrom(from);
            
            if (recipient != null) {
                templatedMessage.setRecipient(RecipientType.TO, recipient);
            }
            
            replacePFX(templatedMessage);
            
            /*
             * Make sure the message has a message-ID
             */
            templatedMessage.saveChanges();
                        
            return templatedMessage;
        }
        catch (MessagingException e) {
            throw new TemplateBuilderException(e);
        }
    }
}
