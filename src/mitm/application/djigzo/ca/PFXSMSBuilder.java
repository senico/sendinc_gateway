/*
 * Copyright (c) 2009-2010, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ca;

import java.util.HashMap;
import java.util.Map;

import mitm.common.template.TemplateBuilder;
import mitm.common.template.TemplateBuilderException;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;

import freemarker.template.SimpleHash;

/**
 * PFXSMSBuilder builds a SMS message from the provided template.
 * 
 * PFXSMSBuilder is not thread safe
 * 
 * @author Martijn Brinkers
 *
 */
public class PFXSMSBuilder
{
    /*
     * The Freemarker template
     */
    private final String template;

    /*
     * for building and processing Freemarker templates.
     */
    private final TemplateBuilder templateBuilder;
    
    /*
     * The PFX password
     */
    private String password;
    
    /*
     * Extra template properties
     */
    private Map<String, Object> properties;
    
    public PFXSMSBuilder(String template, TemplateBuilder templateBuilder)
    {
        Check.notNull(template, "template");
        Check.notNull(templateBuilder, "templateBuilder");
        
        this.template = template;
        this.templateBuilder = templateBuilder;
    }

    public void setPassword(String password)
    {
        Check.notNull(password, "password");
        
        this.password = password;
    }
    
    /**
     * Sets extra template properties to be used by the template 
     */
    public void addProperty(String name, Object value)
    {
        if (this.properties == null) {
            this.properties = new HashMap<String, Object>();
        }
        
        this.properties.put(name, value);
    }
    
    private void checkState()
    throws TemplateBuilderException
    {
        if (StringUtils.isBlank(password)) {
            throw new TemplateBuilderException("Password must be non-blank");
        }
    }
    
    public String createSMS()
    throws TemplateBuilderException
    {
        checkState();
        
        SimpleHash root = new SimpleHash();

        root.put("password", password);

        if (properties != null) {
            root.putAll(properties);
        }
        
        return templateBuilder.buildTemplate(template, root);
    }
}
