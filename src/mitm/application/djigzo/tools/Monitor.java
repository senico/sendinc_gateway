/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.tools;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.xml.ws.WebServiceException;

import mitm.application.djigzo.james.JamesRepository;
import mitm.application.djigzo.ws.CertificateRequestStoreWS;
import mitm.application.djigzo.ws.DjigzoWSDefaults;
import mitm.application.djigzo.ws.JamesRepositoryManagerWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.application.djigzo.ws.UsersWS;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.application.djigzo.ws.impl.factory.CertificateRequestStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.JamesRepositoryManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.KeyAndCertStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PostfixSpoolManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.SMSGatewayWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.SystemManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UsersWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.X509CRLStoreWSProxyFactory;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.util.LogUtils;
import mitm.common.ws.AbstractWSProxyFactory;
import mitm.common.ws.Credential;
import mitm.common.ws.SOAPPasswordMode;
import mitm.common.ws.WSProxyFactoryException;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.BasicConfigurator;

/**
 * Monitor can be used to monitor the MPA queue and CRL store (can be used with remote
 * monitoring tools like Cacti, Munin etc.)
 * 
 * @author Martijn Brinkers
 *
 */
public class Monitor
{
    private static final String COMMAND_NAME = Monitor.class.getName();
    
    private String user;

    private String password;
    
    private String repository;

    private Option userOption;
    
    private Option passwordOption;
    
    private Option passwordPromptOption;
    
    private Option helpOption;
    
    private Option mpaSizeOption;
    
    private Option crlStoreSizeOption;
    
    private Option runningOption;
    
    private Option mtaSizeOption;

    private Option certStoreSizeOption;

    private Option certRequestStoreSizeOption;

    private Option smsSizeOption;

    private Option userSizeOption;

    private Option loggingOption;
    
    private Option hostOption;

    private Option portOption;
    
    @SuppressWarnings("static-access")
    private Options createCommandLineOptions()
    {
        Options options = new Options();

        userOption = OptionBuilder.withArgName("user")
            .hasArg().withDescription("user")
            .create("user");    
        userOption.setRequired(false);
        options.addOption(userOption);
        
        passwordOption = OptionBuilder.withArgName("password")
            .hasArg().withDescription("password")
            .create("password");    
        passwordOption.setRequired(false);
        options.addOption(passwordOption);

        passwordPromptOption = OptionBuilder.withDescription("ask for password")
            .create("pwd");    
        passwordPromptOption.setRequired(false);
        options.addOption(passwordPromptOption);
        
        helpOption = OptionBuilder.withDescription("Show help")
            .create("help");
        helpOption.setRequired(false);
        options.addOption(helpOption);

        mpaSizeOption = OptionBuilder.withArgName("repository").hasArg()
            .withDescription("Returns the MPA mail queue size of [error, outgoing, spool, respool]")
            .create("mpasize");    
        mpaSizeOption.setRequired(false);
        options.addOption(mpaSizeOption);

        crlStoreSizeOption = OptionBuilder.withDescription("Returns the size of the CRL store")
            .create("crlsize");    
        crlStoreSizeOption.setRequired(false);
        options.addOption(crlStoreSizeOption);

        runningOption = OptionBuilder.withDescription("Returns true if the back-end is running.")
            .create("running");    
        options.addOption(runningOption);

        mtaSizeOption = OptionBuilder.withDescription("Returns the MTA mail queue size")
            .create("mtasize");    
        options.addOption(mtaSizeOption);

        certStoreSizeOption = OptionBuilder.withDescription("Returns the certificate store size")
            .create("certsize");    
        options.addOption(certStoreSizeOption);

        certRequestStoreSizeOption = OptionBuilder.withDescription("Returns the certificate request store size")
            .create("certrequestsize");    
        options.addOption(certRequestStoreSizeOption);

        smsSizeOption = OptionBuilder.withDescription("Returns the SMS queue size")
            .create("smssize");    
        options.addOption(smsSizeOption);

        userSizeOption = OptionBuilder.withDescription("Returns the number of users")
            .create("usersize");    
        options.addOption(userSizeOption);

        loggingOption = OptionBuilder.withDescription("If set, debug logging will be enabled")
            .create("logging");    
        options.addOption(loggingOption);
        
        hostOption = OptionBuilder.withArgName("host").hasArg()
    		.withDescription("The host to connect to (def: 127.0.0.1)")
    		.create("host");    
        options.addOption(hostOption);

        portOption = OptionBuilder.withArgName("port").hasArg()
    		.withDescription("The port to use (def: " + DjigzoWSDefaults.PORT + ")")
    		.create("port");    
        options.addOption(portOption);
        
        return options;
    }
    
    private Credential getCredentials()
    {
        String localUser = user;
        String localPassword = password;
        
        if (StringUtils.isBlank(localUser)) {
            localUser = "admin";
        }

        if (StringUtils.isBlank(localPassword)) {
            localPassword = "password";
        }   
        
        return new Credential(localUser, localPassword);
    }
    
    private String getSOAPProtocol() {
        return "http";
    }

    private String getSOAPHost()
    {
    	String host = StringUtils.trimToNull(hostOption.getValue());
    	
    	if (host == null) {
    		host = "127.0.0.1"; 
    	}
    	
        return host;
    }

    private int getSOAPPort() {
    	return NumberUtils.toInt(portOption.getValue(), DjigzoWSDefaults.PORT);
    }

    private void setPasswordMode(AbstractWSProxyFactory<?> factory)
    {
        /*
         * We use TEXT mode because DIGEST is way too slow for command line util. I think the slowness
         * is caused by initializing the secure random generator
         */
        factory.setPasswordMode(SOAPPasswordMode.TEXT);
    }
    
    private JamesRepositoryManagerWS createJamesRepositoryManagerWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.JAMES_REPOSITORY_MANAGER_WSDL);

        JamesRepositoryManagerWSProxyFactory factory = new JamesRepositoryManagerWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.JAMES_REPOSITORY_MANAGER_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private X509CRLStoreWS createX509CRLStoreWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.CRL_STORE_WSDL);

        X509CRLStoreWSProxyFactory factory = new X509CRLStoreWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CRL_STORE_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private SystemManagerWS createSystemManagerWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.SYSTEM_MANAGER_WSDL);

        SystemManagerWSProxyFactory factory = new SystemManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.SYSTEM_MANAGER_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    private JamesRepository getJamesRepository()
    {
        if (repository == null) {
            throw new CLIRuntimeException("repository is missing.");
        }
        
        JamesRepository jamesRepository = JamesRepository.fromString(repository);
        
        if (jamesRepository == null)
        {
            List<String> allowed = new LinkedList<String>();
            
            for (JamesRepository value : JamesRepository.values()) {
                allowed.add(value.getName());
            }
            
            throw new CLIRuntimeException("Illegal repository. Allowed: " + allowed);
        }
        
        return jamesRepository;
    }

    private PostfixSpoolManagerWS createPostfixSpoolManagerWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.POSTFIX_SPOOL_MANAGER_WSDL);

        PostfixSpoolManagerWSProxyFactory factory = new PostfixSpoolManagerWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.POSTFIX_SPOOL_MANAGER_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private SMSGatewayWS createSMSGatewayWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.SMS_GATEWAY_WSDL);

        SMSGatewayWSProxyFactory factory = new SMSGatewayWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.SMS_GATEWAY_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private KeyAndCertStoreWS createKeyAndCertStoreWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.KEY_AND_CERTSTORE_WSDL);

        KeyAndCertStoreWSProxyFactory factory = new KeyAndCertStoreWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.KEY_AND_CERTSTORE_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private CertificateRequestStoreWS createCertificateRequestStoreWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.CERTIFICATE_REQUEST_STORE_WSDL);

        CertificateRequestStoreWSProxyFactory factory = new CertificateRequestStoreWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.CERTIFICATE_REQUEST_STORE_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private UsersWS createUsersWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.USERS_WSDL);

        UsersWSProxyFactory factory = new UsersWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.USERS_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    private void retrieveMPAQueueSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        JamesRepositoryManagerWS ws = createJamesRepositoryManagerWS();
        
        System.out.println(ws.getRepositorySize(getJamesRepository()));
    }
    
    private void retrieveCRLStoreSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        X509CRLStoreWS ws = createX509CRLStoreWS();
        
        long size = ws.size();
        
        System.out.println(size);
    }

    private void checkRunning()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createSystemManagerWS().isRunning());
    }
    
    private void retrieveMTAQueueSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createPostfixSpoolManagerWS().getQueueLength(null));
    }
    
    private void retrieveCertStoreSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createKeyAndCertStoreWS().size(Expired.ALLOWED, MissingKeyAlias.ALLOWED));
    }

    private void retrieveCertRequestStoreSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createCertificateRequestStoreWS().getSize());
    }
    
    private void retrieveSMSSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createSMSGatewayWS().getCount());
    }

    private void retrieveUsersSize()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException
    {
        System.out.println(createUsersWS().getUserCount());
    }
    
    private void handleCommandline(String[] args)
    throws IOException, ParseException, WSProxyFactoryException, WebServiceCheckedException
    {        
        CommandLineParser parser = new BasicParser();
        
        Options options = createCommandLineOptions();
        
        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;
        
        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        initLogging(commandLine.hasOption(loggingOption.getOpt()));
        
        user = userOption.getValue();
        
        password = passwordOption.getValue();

        repository = mpaSizeOption.getValue();
        
        if (commandLine.getOptions().length == 0) 
        {
            formatter.printHelp(COMMAND_NAME, options, true);
            
            System.exit(1);
            
            return;
        }

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(helpOption.getOpt())) 
        {
            formatter.printHelp(COMMAND_NAME, options, true);
            
            return;
        }
        
        if (commandLine.hasOption(passwordPromptOption.getOpt())) 
        {
            System.err.println("Please enter password: ");
            password = new jline.ConsoleReader().readLine(new Character('*'));
        }
        
        if (commandLine.hasOption(mpaSizeOption.getOpt())) {
            retrieveMPAQueueSize();
        }

        if (commandLine.hasOption(crlStoreSizeOption.getOpt())) {
            retrieveCRLStoreSize();
        }

        if (commandLine.hasOption(runningOption.getOpt())) {
            checkRunning();
        }

        if (commandLine.hasOption(mtaSizeOption.getOpt())) {
            retrieveMTAQueueSize();
        }
        
        if (commandLine.hasOption(certStoreSizeOption.getOpt())) {
            retrieveCertStoreSize();
        }

        if (commandLine.hasOption(certRequestStoreSizeOption.getOpt())) {
            retrieveCertRequestStoreSize();
        }

        if (commandLine.hasOption(smsSizeOption.getOpt())) {
            retrieveSMSSize();
        }

        if (commandLine.hasOption(userSizeOption.getOpt())) {
            retrieveUsersSize();
        }
    }
        
    private static void initLogging(boolean enable)
    {
        if (!enable) {
            /*
             * We will disable all logging because we do not want to clutter the output
             * and std-err with logging info
             */
            LogUtils.disableLogging();
        }
        else {
            BasicConfigurator.configure();
        }
    }
    
    public static void main(String[] args)
    throws ParseException, IOException
    {
        Monitor monitor = new Monitor();
        
        try {
            monitor.handleCommandline(args);
        }
        catch(CLIRuntimeException e)
        {
            System.err.println(e.getMessage());

            System.exit(2);
        }
        catch (MissingArgumentException e)
        {
            System.err.println("Not all required parameters are specified. " + e);

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e);

            System.exit(4);
        }        
        catch (WebServiceException e)
        {
            Throwable cause = ExceptionUtils.getRootCause(e);

            if (cause instanceof ConnectException) {
                System.err.println("Unable to connect to backend. Cause: " + cause.getMessage());
            }
            else {
                e.printStackTrace();
            }
            System.exit(5);
        }        
        catch (WSProxyFactoryException e)
        {
            e.printStackTrace();

            System.exit(6);
        }
        catch (WebServiceCheckedException e)
        {
            e.printStackTrace();

            System.exit(7);
        }
    }
}
