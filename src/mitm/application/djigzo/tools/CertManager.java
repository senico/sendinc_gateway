/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.tools;

import java.io.ByteArrayOutputStream;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.xml.ws.WebServiceException;

import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow.MissingKey;
import mitm.application.djigzo.ws.DjigzoWSDefaults;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.KeyAndCertificateWorkflowWS;
import mitm.application.djigzo.ws.impl.factory.KeyAndCertStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.KeyAndCertificateWorkflowWSProxyFactory;
import mitm.common.security.SyncMode;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.util.LogUtils;
import mitm.common.ws.AbstractWSProxyFactory;
import mitm.common.ws.Credential;
import mitm.common.ws.SOAPPasswordMode;
import mitm.common.ws.WSProxyFactoryException;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.log4j.BasicConfigurator;

/**
 * Tool which can be used to import a certificate
 * 
 * @author Martijn Brinkers
 *
 */
public class CertManager
{
    private static final String COMMAND_NAME = CertManager.class.getName();
    
    private String soapUser;

    private String soapPassword;

    private String keyStorePassword;

    private Option soapUserOption;
    
    private Option keyStorePasswordOption;

    private Option soapPasswordOption;

    private Option importOption;

    private Option cerOption;

    private Option pfxOption;

    private Option syncOption;

    private Option helpOption;
    
    private Option loggingOption;

    private Option hostOption;

    private Option portOption;

    @SuppressWarnings("static-access")
    private Options createCommandLineOptions()
    {
        Options options = new Options();

        soapUserOption = OptionBuilder.withArgName("soapUser")
            .hasArg().withDescription("soap user")
            .create("soapUser");    
        soapUserOption.setRequired(false);
        options.addOption(soapUserOption);
        
        soapPasswordOption = OptionBuilder.withArgName("soapPassword")
            .hasArg().withDescription("soap password")
            .create("soapPassword");    
        soapPasswordOption.setRequired(false);
        options.addOption(soapPasswordOption);

        importOption = OptionBuilder.withDescription("If set, certificate(s) or a pfx will be read from stdin and imported")
            .create("import");    
        options.addOption(importOption);

        cerOption = OptionBuilder.withDescription("If set, certificate(s) will be read from stdin and imported")
            .create("cer");    
        options.addOption(cerOption);

        pfxOption = OptionBuilder.withDescription("If set, certificate(s) and key(s) will be read from stdin and imported")
            .create("pfx");    
        options.addOption(pfxOption);
        
        keyStorePasswordOption = OptionBuilder.withArgName("keyStorePassword")
            .hasArg().withDescription("Password for the keystore when importing a pfx file")
            .create("keyStorePassword");    
        keyStorePasswordOption.setRequired(false);
        options.addOption(keyStorePasswordOption);

        syncOption = OptionBuilder.withArgName("syncmode").hasArg().
            withDescription("If set, the certificate and keys will be synced")
            .create("sync");    
        options.addOption(syncOption);
        
        loggingOption = OptionBuilder.withDescription("If set, debug logging will be enabled")
            .create("logging");    
        options.addOption(loggingOption);
        
        helpOption = OptionBuilder.withDescription("Show help")
            .create("help");
        helpOption.setRequired(false);
        options.addOption(helpOption);

        hostOption = OptionBuilder.withArgName("host").hasArg()
        	.withDescription("The host to connect to (127.0.0.1)")
        	.create("host");    
        options.addOption(hostOption);

        portOption = OptionBuilder.withArgName("port").hasArg()
        	.withDescription("The port to use (" + DjigzoWSDefaults.PORT + ")")
    		.create("port");    
        options.addOption(portOption);

        return options;
    }
    
    private Credential getCredentials()
    {
        String localUser = soapUser;
        String localPassword = soapPassword;
        
        if (StringUtils.isBlank(localUser)) {
            localUser = "admin";
        }

        if (StringUtils.isBlank(localPassword)) {
            localPassword = "password";
        }   
        
        return new Credential(localUser, localPassword);
    }
    
    private String getSOAPProtocol() {
        return "http";
    }

    private String getSOAPHost()
    {
    	String host = StringUtils.trimToNull(hostOption.getValue());
    	
    	if (host == null) {
    		host = "127.0.0.1"; 
    	}
    	
        return host;
    }

    private int getSOAPPort() {
    	return NumberUtils.toInt(portOption.getValue(), DjigzoWSDefaults.PORT);
    }

    private void setPasswordMode(AbstractWSProxyFactory<?> factory)
    {
        /*
         * We use TEXT mode because DIGEST is way too slow for command line util. I think the slowness
         * is caused by initializing the secure random generator
         */
        factory.setPasswordMode(SOAPPasswordMode.TEXT);
    }
    
    private KeyAndCertificateWorkflowWS createKeyAndCertificateWorkflowWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.KEY_AND_CERTIFICATE_WORKFLOW_WSDL);

        KeyAndCertificateWorkflowWSProxyFactory factory = new KeyAndCertificateWorkflowWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private KeyAndCertStoreWS createKeyAndCertStoreWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), 
                DjigzoWSDefaults.KEY_AND_CERTSTORE_WSDL);

        KeyAndCertStoreWSProxyFactory factory = new KeyAndCertStoreWSProxyFactory(wsdlURL, 
                DjigzoWSDefaults.NAMESPACE, DjigzoWSDefaults.KEY_AND_CERTSTORE_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    private void importKeyStore() 
    throws Exception
    {
        createKeyAndCertificateWorkflowWS().addPFX(IOUtils.toByteArray(System.in), keyStorePassword, 
                MissingKey.ADD_CERTIFICATE);
    }
    
    private void importCertificates() 
    throws Exception
    {
        Collection<X509Certificate> certificates = CertificateUtils.readX509Certificates(System.in);
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        CertificateUtils.writeCertificates(certificates, bos);
        
        createKeyAndCertStoreWS().addCertificates(bos.toByteArray());
    }    

    private void syncKeyAndCertStore() 
    throws Exception
    {
        SyncMode syncMode = SyncMode.fromName(syncOption.getValue());
        
        if (syncMode == null)
        {
            StrBuilder sb = new StrBuilder();
            
            for (SyncMode mode : SyncMode.values())
            {
                sb.appendSeparator(',');
                sb.append(mode.getName());
            }
            
            throw new MissingArgumentException("Syncmode is not a valid syncmode. Supported syncmodes: " + 
                    sb.toString());
        }
        
        createKeyAndCertStoreWS().sync(syncMode);
    }

    private void handleCommandline(String[] args)
    throws Exception
    {        
        CommandLineParser parser = new BasicParser();
        
        Options options = createCommandLineOptions();
        
        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;
        
        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        initLogging(commandLine.hasOption(loggingOption.getOpt()));
        
        soapUser = soapUserOption.getValue();
        
        soapPassword = soapPasswordOption.getValue();

        keyStorePassword = keyStorePasswordOption.getValue();

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(helpOption.getOpt())) 
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            System.exit(1);
            
            return;
        }

        if (commandLine.hasOption(importOption.getOpt()))
        {
            if (commandLine.hasOption(cerOption.getOpt())) {
                importCertificates();
            } 
            else if (commandLine.hasOption(pfxOption.getOpt())) {
                importKeyStore();
            }
        }
        else if (commandLine.hasOption(syncOption.getOpt())) {
            syncKeyAndCertStore();
        }
    }
        
    private static void initLogging(boolean enable)
    {
        if (!enable) {
            /*
             * We will disable all logging because we do not want to clutter the output
             * and std-err with logging info
             */
            LogUtils.disableLogging();
        }
        else {
            BasicConfigurator.configure();
        }
    }
    
    public static void main(String[] args)
    throws Exception
    {
        CertManager monitor = new CertManager();
        
        try {
            monitor.handleCommandline(args);
        }
        catch(CLIRuntimeException e)
        {
            System.err.println(e.getMessage());

            System.exit(2);
        }
        catch (MissingArgumentException e)
        {
            System.err.println("Not all required parameters are specified. " + e);

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e);

            System.exit(4);
        }        
        catch (WebServiceException e)
        {
            Throwable cause = ExceptionUtils.getRootCause(e);

            if (cause instanceof ConnectException) {
                System.err.println("Unable to connect to backend. Cause: " + cause.getMessage());
            }
            else {
                e.printStackTrace();
            }
            System.exit(5);
        }        
        catch (WSProxyFactoryException e)
        {
            e.printStackTrace();

            System.exit(6);
        }
        catch (WebServiceCheckedException e)
        {
            e.printStackTrace();

            System.exit(7);
        }
        catch (Exception e)
        {
            e.printStackTrace();

            System.exit(8);
        }
    }
}
