/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.tools;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import mitm.application.djigzo.ws.CTLEntryDTO;
import mitm.application.djigzo.ws.CTLWS;
import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.DjigzoWSDefaults;
import mitm.application.djigzo.ws.UserDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.UserWS;
import mitm.application.djigzo.ws.UsersWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.application.djigzo.ws.impl.factory.CTLWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CertificateValidatorWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserPreferencesWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UsersWSProxyFactory;
import mitm.common.hibernate.SortDirection;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.common.security.ctl.CTLManager;
import mitm.common.util.LogUtils;
import mitm.common.ws.AbstractWSProxyFactory;
import mitm.common.ws.Credential;
import mitm.common.ws.SOAPPasswordMode;
import mitm.common.ws.WSProxyFactoryException;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Upgrade tool which can be used to upgrade to newer versions of Djigzo.
 * 
 * @author Martijn Brinkers
 *
 */
public class Upgrade
{
    private static final String COMMAND_NAME = Upgrade.class.getName();

    private String user;

    private String password;
    
    private Option hostOption;

    private Option portOption;
    
    @SuppressWarnings("static-access")
    private Options createCommandLineOptions()
    {
        Options options = new Options();

        Option helpOption = OptionBuilder.withDescription("Show help")
            .create("help");
        helpOption.setRequired(false);
        options.addOption(helpOption);

        Option versionOption = OptionBuilder.withArgName("version")
            .hasArg().withDescription("version")
            .create("version");    
        versionOption.setRequired(true);
        options.addOption(versionOption);

        Option userOption = OptionBuilder.withArgName("user")
            .hasArg().withDescription("user")
            .create("user");    
        userOption.setRequired(false);
        options.addOption(userOption);
    
        Option passwordOption = OptionBuilder.withArgName("password")
            .hasArg().withDescription("password")
            .create("password");    
        passwordOption.setRequired(false);
        options.addOption(passwordOption);
        
        hostOption = OptionBuilder.withArgName("host").hasArg()
    		.withDescription("The host to connect to (def: 127.0.0.1)")
    		.create("host");    
        options.addOption(hostOption);

        portOption = OptionBuilder.withArgName("port").hasArg()
    		.withDescription("The port to use (def: " + DjigzoWSDefaults.PORT + ")")
    		.create("port");    
        options.addOption(portOption);
        
        return options;
    }
    
    private Credential getCredentials()
    {
        String localUser = user;
        String localPassword = password;
        
        if (StringUtils.isBlank(localUser)) {
            localUser = "admin";
        }

        if (StringUtils.isBlank(localPassword)) {
            localPassword = "password";
        }   
        
        return new Credential(localUser, localPassword);
    }
    
    private String getSOAPProtocol() {
        return "http";
    }

    private String getSOAPHost()
    {
    	String host = StringUtils.trimToNull(hostOption.getValue());
    	
    	if (host == null) {
    		host = "127.0.0.1"; 
    	}
    	
        return host;
    }

    private int getSOAPPort() {
    	return NumberUtils.toInt(portOption.getValue(), DjigzoWSDefaults.PORT);
    }

    private void setPasswordMode(AbstractWSProxyFactory<?> factory)
    {
        /*
         * We use TEXT mode because DIGEST is way too slow for command line util. I think the slowness
         * is caused by initializing the secure random generator
         */
        factory.setPasswordMode(SOAPPasswordMode.TEXT);
    }
    
    private UserWS createUserWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.USER_WSDL);

        UserWSProxyFactory factory = new UserWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.USER_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private UsersWS createUsersWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.USERS_WSDL);

        UsersWSProxyFactory factory = new UsersWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.USERS_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    private UserPreferencesWS createUserPreferencesWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.USER_PREFERENCES_WSDL);

        UserPreferencesWSProxyFactory factory = new UserPreferencesWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.USER_PREFERENCES_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private CertificateValidatorWS createCertificateValidatorWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.CERTIFICATE_VALIDATOR_WSDL);

        CertificateValidatorWSProxyFactory factory = new CertificateValidatorWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CERTIFICATE_VALIDATOR_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private CTLWS createCTLWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.CTL_WSDL);

        CTLWSProxyFactory factory = new CTLWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CTL_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    /*
     * Certificates that are explicitly selected but not valid for encryption will be added to the
     * CTL and white listed. This is an upgrade to 1.3 because 1.3 introduced the CTL. Before 1.3 certificates that
     * were explicitly added were also trusted.
     */
    private void upgradeTo13()
    throws WebServiceCheckedException, MalformedURLException, WSProxyFactoryException
    {
        UsersWS usersWS = createUsersWS();
        UserWS userWS = createUserWS();
        UserPreferencesWS userPreferencesWS = createUserPreferencesWS();
        CertificateValidatorWS certificateValidatorWS = createCertificateValidatorWS();
        CTLWS cTLWS = createCTLWS();
        
        int userCount = usersWS.getUserCount();
        
        for (int i = 0; i < userCount; i++)
        {
            System.out.println("Handling user " + i);
            
            List<UserDTO> users = usersWS.getUsers(i, 1, SortDirection.ASC);
            
            if (users != null && users.size() > 0)
            {
                String email = users.get(0).getEmail();
                
                UserPreferencesDTO userPreferences = userWS.getUserPreferences(email);
                
                if (userPreferences != null)
                {
                    List<X509CertificateDTO> certificates = userPreferencesWS.getCertificates(userPreferences);
                    
                    if (certificates != null && certificates.size() > 0)
                    {
                        for (X509CertificateDTO certificate : certificates)
                        {
                            CertificateValidatorResult result = certificateValidatorWS.checkValidityForEncryption(
                                    CertificateStore.CERTIFICATES, 
                                    certificate.getThumbprint());
                            
                            if (!result.isValid() && !result.isRevoked() && !result.isWhiteListed() && !result.isBlackListed())
                            {
                                CTLEntryDTO ctlEntry = cTLWS.getEntry(CTLManager.DEFAULT_CTL, certificate.getThumbprint());
                                
                                if (ctlEntry == null)
                                {
                                    System.out.println("White listing certificate with thumbprint " + certificate.getThumbprint());
                                    
                                    ctlEntry = new CTLEntryDTO(certificate.getThumbprint(), CTLEntryStatus.WHITELISTED);
                                    
                                    ctlEntry.setAllowExpired(certificate.isExpired());
                                    
                                    cTLWS.addEntry(CTLManager.DEFAULT_CTL, ctlEntry);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void handleCommandline(String[] args)
    throws IOException, ParseException, WSProxyFactoryException, WebServiceCheckedException
    {        
        CommandLineParser parser = new BasicParser();
        
        Options options = createCommandLineOptions();
        
        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;
        
        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        user = commandLine.getOptionValue("user");
        
        password = commandLine.getOptionValue("password");

        String version = commandLine.getOptionValue("version");
        
        if (commandLine.getOptions().length == 0 || commandLine.hasOption("help")) 
        {
            formatter.printHelp(COMMAND_NAME, options, true);
            
            return;
        }
        
        if (StringUtils.equalsIgnoreCase(version, "1.3")) {
            upgradeTo13();
        }
        else {
            throw new IllegalArgumentException("Unsupported version.");
        }
    }
        
    private static void initLogging()
    {
        /*
         * We will disable logging because we do not want to clutter the output
         * and std-err with logging info
         */
        LogUtils.disableLogging();
    }
    
    public static void main(String[] args)
    throws ParseException, IOException
    {
        initLogging();
        
        Upgrade upgrade = new Upgrade();
        
        try {
            upgrade.handleCommandline(args);
        }
        catch(CLIRuntimeException e) {
            System.err.println(e.getMessage());
        }
        catch (MissingArgumentException e) {
            System.err.println("Not all required parameters are specified. " + e);
        }
        catch (ParseException e) {
            System.err.println("Command line parsing error. " + e);
        }        
        catch (WSProxyFactoryException e) {
            e.printStackTrace();
        }
        catch (WebServiceCheckedException e) {
            e.printStackTrace();
        }
    }
}
