/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.tools;

import java.net.MalformedURLException;
import java.net.URL;

import mitm.application.djigzo.ws.DjigzoWSDefaults;
import mitm.application.djigzo.ws.GlobalPreferencesManagerWS;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.impl.SaveableHierarchicalPropertiesImpl;
import mitm.application.djigzo.ws.impl.factory.GlobalPreferencesManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.HierarchicalPropertiesWSProxyFactory;
import mitm.common.net.PropertiesProxySettings;
import mitm.common.net.ProxySettings;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.LogUtils;
import mitm.common.ws.AbstractWSProxyFactory;
import mitm.common.ws.Credential;
import mitm.common.ws.SOAPPasswordMode;
import mitm.common.ws.WSProxyFactoryException;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;

/**
 * Command line tool to manage the proxy settings
 * 
 * @author Martijn Brinkers
 *
 */
public class ProxyManager
{
    private static final String COMMAND_NAME = ProxyManager.class.getName();
    
    private String user;

    private String password;
    
    private Option hostOption;

    private Option portOption;
    
    @SuppressWarnings("static-access")
    private Options createCommandLineOptions()
    {
        Options options = new Options();

        Option getOption = OptionBuilder.create("get");    
        getOption.setRequired(false);
        getOption.setDescription("Returns the proxy settings");
        options.addOption(getOption);
        
        Option userOption = OptionBuilder.withArgName("user")
            .hasArg().withDescription("user")
            .create("user");    
        userOption.setRequired(false);
        options.addOption(userOption);

        Option passwordOption = OptionBuilder.withArgName("password")
            .hasArg().withDescription("password")
            .create("password");    
        passwordOption.setRequired(false);
        options.addOption(passwordOption);

        Option passwordPromptOption = OptionBuilder.withDescription("ask for password")
            .create("pwd");    
        passwordPromptOption.setRequired(false);
        options.addOption(passwordPromptOption);
        
        Option helpOption = OptionBuilder.withDescription("Show help")
            .create("help");
        helpOption.setRequired(false);
        options.addOption(helpOption);
        
        hostOption = OptionBuilder.withArgName("host").hasArg()
    		.withDescription("The host to connect to (127.0.0.1)")
    		.create("host");    
        options.addOption(hostOption);

        portOption = OptionBuilder.withArgName("port").hasArg()
    		.withDescription("The port to use (" + DjigzoWSDefaults.PORT + ")")
    		.create("port");    
        options.addOption(portOption);
        
        return options;
    }
    
    private HierarchicalPropertiesWS createHierarchicalPropertiesWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.HIERARCHICAL_PROPERTIES_WSDL);

        HierarchicalPropertiesWSProxyFactory factory = new HierarchicalPropertiesWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.HIERARCHICAL_PROPERTIES_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }

    private GlobalPreferencesManagerWS createGlobalPreferencesManagerWS()
    throws MalformedURLException, WSProxyFactoryException
    {
        URL wsdlURL = new URL(getSOAPProtocol(), getSOAPHost(), getSOAPPort(), DjigzoWSDefaults.GLOBAL_PREFERENCES_MANAGER_WSDL);

        GlobalPreferencesManagerWSProxyFactory factory = new GlobalPreferencesManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.GLOBAL_PREFERENCES_MANAGER_SERVICE_NAME);

        setPasswordMode(factory);
        
        return factory.createProxy(getCredentials());
    }
    
    private void handleGet()
    throws MalformedURLException, WSProxyFactoryException, WebServiceCheckedException, HierarchicalPropertiesException
    {
        GlobalPreferencesManagerWS globalPreferencesManager= createGlobalPreferencesManagerWS();
        HierarchicalPropertiesWS hierarchicalProperties= createHierarchicalPropertiesWS();

        UserPreferencesDTO globalPreferences = globalPreferencesManager.getGlobalUserPreferences();
        
        HierarchicalProperties globalProperties = new SaveableHierarchicalPropertiesImpl(hierarchicalProperties, 
                globalPreferences);
        
        ProxySettings proxySettings = new PropertiesProxySettings(globalProperties);
        
        StrBuilder sb = new StrBuilder();
        
        if (proxySettings.isEnabled()) 
        {
            String proxyUsername = proxySettings.getUsername();
            String proxyPassword = proxySettings.getPassword();
            
            sb.append("http://");

            if (StringUtils.isNotBlank(proxyUsername))
            {
                sb.append(proxyUsername);
                sb.append(":");
                sb.append(StringUtils.defaultString(proxyPassword)).append("@");
            }
            
            sb.append(proxySettings.getHost());
            sb.append(":");
            sb.append(proxySettings.getPort());
        }
        
        
        System.out.println(sb.toString());
    }
    
    private Credential getCredentials()
    {
        String localUser = user;
        String localPassword = password;
        
        if (StringUtils.isBlank(localUser)) {
            localUser = "admin";
        }

        if (StringUtils.isBlank(localPassword)) {
            localPassword = "password";
        }   
        
        return new Credential(localUser, localPassword);
    }
    
    private String getSOAPProtocol() {
        return "http";
    }

    private String getSOAPHost()
    {
    	String host = StringUtils.trimToNull(hostOption.getValue());
    	
    	if (host == null) {
    		host = "127.0.0.1"; 
    	}
    	
        return host;
    }

    private int getSOAPPort() {
    	return NumberUtils.toInt(portOption.getValue(), DjigzoWSDefaults.PORT);
    }

    private void setPasswordMode(AbstractWSProxyFactory<?> factory)
    {
        /*
         * We use TEXT mode because DIGEST is way too slow for command line util. I think the slowness
         * is caused by initializing the secure random generator
         */
        factory.setPasswordMode(SOAPPasswordMode.TEXT);
    }
    
    private void handleCommandline(String[] args)
    throws Exception
    {        
        CommandLineParser parser = new BasicParser();
        
        Options options = createCommandLineOptions();
        
        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;
        
        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        if (commandLine.getOptions().length == 0 || commandLine.hasOption("help")) 
        {
            formatter.printHelp(COMMAND_NAME, options, true);
            
            System.exit(2);
        }
        
        user = commandLine.getOptionValue("user");
        
        password = commandLine.getOptionValue("password");

        if (commandLine.hasOption("pwd")) 
        {
            System.err.println("Please enter password: ");
            password = new jline.ConsoleReader().readLine(new Character('*'));
        }
        
        if (commandLine.hasOption("get")) {
            handleGet();
        }
    }
    
    private static void initLogging()
    {
        /*
         * We will disable logging because we do not want to clutter the output
         * and std-err with logging info
         */
        LogUtils.disableLogging();
    }

    public static void main(String[] args)
    {
        initLogging();
        
        ProxyManager proxy = new ProxyManager();
        
        try {
            proxy.handleCommandline(args);
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            
            System.exit(1);
        }
    }
}
