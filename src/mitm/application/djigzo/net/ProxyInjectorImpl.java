/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.net;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.common.net.PropertiesProxySettings;
import mitm.common.net.ProxyException;
import mitm.common.net.ProxyInjector;
import mitm.common.net.ProxySettings;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of ProxyInjector
 * 
 * @author Martijn Brinkers
 *
 */
public class ProxyInjectorImpl implements ProxyInjector
{
    private final static Logger logger = LoggerFactory.getLogger(ProxyInjectorImpl.class);
    /*
     * Used for getting the global properties
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    public ProxyInjectorImpl(GlobalPreferencesManager globalPreferencesManager)
    {
        Check.notNull(globalPreferencesManager, "globalPreferencesManager");
        
        this.globalPreferencesManager = globalPreferencesManager;
    }
    
    @Override
    public void setProxy(HttpClient httpClient)
    throws ProxyException
    {
        try {
            if (httpClient == null) {
                return;
            }
            
            ProxySettings proxySettings = new PropertiesProxySettings(globalPreferencesManager.
                    getGlobalUserPreferences().getProperties());
            
            if (proxySettings.isEnabled())
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Setting proxy. Host: " + proxySettings.getHost() + ", Port: " + proxySettings.getPort() +
                            ", Username: " + proxySettings.getUsername());
                }
                
                httpClient.getHostConfiguration().setProxy(proxySettings.getHost(), proxySettings.getPort());

                Credentials credentials = new NTCredentials(
                            StringUtils.defaultString(proxySettings.getUsername()), 
                            proxySettings.getPassword(), 
                            StringUtils.defaultString(proxySettings.getDomain()), 
                            StringUtils.defaultString(proxySettings.getHost()));
                
                httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
            }
            else {
                httpClient.getHostConfiguration().setProxyHost(null);
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new ProxyException(e);
        }
    }
}
