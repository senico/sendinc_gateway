/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow;

import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;

public interface KeyAndCertificateWorkflow extends CertificateWorkflow
{
	/**
	 * Required action when the KeyStore to import contains an entry with a certificate but the 
	 * entry does not contain a private key.
	 */
	public enum MissingKey {SKIP_CERTIFICATE, ADD_CERTIFICATE}
	
    /** 
     * Imports all the keys and associated certificates from the key store. Returns the number
     * of imported entries
     */
    public int importKeyStore(KeyStore keyStore, MissingKey missingKey) 
    throws KeyStoreException;
    
    /**
     * Returns an encoded PFX containing the certificates with and their associated keys (if present).
     * The PFX will be encrypted with the given password. The password will be cleared (ie all 
     * elements of the password will be set to 0). The resulting PFX will be written to the pfx
     * OutputStream.
     */
    public void getPFX(Collection<X509Certificate> certificates, char[] password, OutputStream pfx)
    throws KeyStoreException;

    public void getPFX(Collection<X509Certificate> certificates, char[] password, boolean includeRoot,
            OutputStream pfx)
    throws KeyStoreException;
}
