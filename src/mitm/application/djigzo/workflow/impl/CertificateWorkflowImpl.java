/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;

import mitm.application.djigzo.impl.hibernate.UserPreferencesDAO;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.certstore.CertificateAlreadyExistsException;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.util.Check;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CertificateWorkflowImpl implements CertificateWorkflow
{
    private final static Logger logger = LoggerFactory.getLogger(CertificateWorkflowImpl.class);
    
    private final X509CertStoreExt certStore;
    
    /*
     * Used to execute database actions within it's own transaction
     */
    private final DatabaseActionExecutor actionExecutor;
    
    /* 
     * because we need to temporarily override the current session for some functions we need to know
     * which session is in use. We therefore need access to SessionManager.
     */
    private final SessionManager sessionManager;
        
    public CertificateWorkflowImpl(X509CertStoreExt certStore, SessionManager sessionManager)
    {
    	Check.notNull(certStore, "certStore");
    	Check.notNull(sessionManager, "sessionManager");
    	
        this.certStore = certStore;
        this.sessionManager = sessionManager;
        
        this.actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);
    }

    protected SessionManager getSessionManager() {
        return sessionManager;
    }
    
    protected DatabaseActionExecutor getActionExecutor() {
        return actionExecutor;
    }
    
    @Override
    public boolean addCertificateTransacted(final X509Certificate certificate)
    throws CertStoreException
    {
        boolean added = false;
        
        /*
         * we want to add the certificate in it's own session because we want to be sure that 
         * the certificate is always added even when the 'main' session need to be rollback.
         * We need to save the existing session before setting our own session because we 
         * need to restore the existing session after use.
         */
        Session previousSession = sessionManager.getSession();
        
        try {
            added = actionExecutor.executeTransaction(
                new DatabaseAction<Boolean>()
                {
                    @Override
                    public Boolean doAction(Session session)
                    throws DatabaseException
                    {
                        return addCertificateAction(certificate, session);
                    }
                });
        }
        catch(DatabaseException e) 
        {
            Throwable cause = e.getCause();
            
            if (cause == null) {
                cause = e;
            }
            
            if (cause instanceof CertStoreException) {
                throw (CertStoreException) cause;
            }
            
            throw new CertStoreException(cause);
        }
        catch(ConstraintViolationException e) {
            logger.warn("ConstraintViolationException. The certificate was probably already in the certStore. Message: " + 
                    e.getMessage());
        }
        finally {
            /* restore the session */
        	sessionManager.setSession(previousSession);
        }
        
        return added;
    }    
      
    private boolean addCertificateAction(X509Certificate certificate, Session session)
    throws DatabaseException
    {
        boolean added = false;
        
        /* make sure all associated listeners will use the new session */
        sessionManager.setSession(session);

        try {
            certStore.addCertificate(certificate);
            
            added = true;
        }
        catch(CertificateAlreadyExistsException e) {
            /* 
             * gets thrown when the certificate already exists. Although I prefer not to check the existence of elements
             * instead of relying on exceptions here I will use this approach. X509CertStore already checks for the 
             * existence of the certificate and doing it again is just wasting time.
             */
            if (logger.isDebugEnabled()) {
                logger.info("Certificate already in CertStore.");
            }
        }
        catch (CertStoreException e) {
            throw new DatabaseException(e);
        }
        
        return added;
    }
    
    @Override
    public boolean isInUse(X509Certificate certificate) 
    throws CertStoreException
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
    	
    	UserPreferencesDAO userPreferencesDAO = new UserPreferencesDAO(session);
    	
    	X509CertStoreEntry certificateEntry = certStore.getByCertificate(certificate);
    	
    	boolean inUse = false;
    	
    	/*
    	 * We can only check if the X509CertStoreEntry is a Hibernate entity. 
    	 */
    	if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate)) {
    		inUse = userPreferencesDAO.isReferencing((X509CertStoreEntryHibernate) certificateEntry);
    	}
    	
    	return inUse;
    }
}
