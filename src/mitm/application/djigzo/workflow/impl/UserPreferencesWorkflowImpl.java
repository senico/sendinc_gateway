/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.impl.hibernate.UserPreferencesDAO;
import mitm.application.djigzo.impl.hibernate.UserPreferencesEntity;
import mitm.application.djigzo.impl.hibernate.UserPreferencesHibernate;
import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.hibernate.NamedBlobEntity;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;

public class UserPreferencesWorkflowImpl implements UserPreferencesWorkflow 
{
    /*
     * The store with certificates and keys
     */
    private final KeyAndCertStore keyAndCertStore;
    
    /* 
     * Handles database sessions
     */
    private final SessionManager sessionManager;
    
    /*
     * The system encryptor
     */
    private final Encryptor encryptor;
    
    public UserPreferencesWorkflowImpl(KeyAndCertStore keyAndCertStore, SessionManager sessionManager, 
            Encryptor encryptor)
    {
        Check.notNull(keyAndCertStore, "keyAndCertStore");
        Check.notNull(sessionManager, "sessionManager");
        Check.notNull(encryptor, "encryptor");
        
        this.keyAndCertStore = keyAndCertStore;
        this.sessionManager = sessionManager;
        this.encryptor = encryptor;
    }
    
    /*
     * Creates a UserPreferences from a UserPreferencesEntity.
     */
    private UserPreferences createUserPreferences(UserPreferencesEntity entity)
    {
        return new UserPreferencesHibernate(entity, keyAndCertStore, 
                encryptor, sessionManager.getSession());        
    }
    
    @Override
    public List<UserPreferences> getReferencingFromCertificates(X509Certificate certificate, Integer firstResult,
            Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<UserPreferences>();
        
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromCertificates(
                    (X509CertStoreEntryHibernate) certificateEntry, firstResult, maxResults);
            
            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }
        
        return result;
    }

    @Override
    public long getReferencingFromCertificatesCount(X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;
        
        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            result = createDAO().referencingFromCertificatesCount(
                    (X509CertStoreEntryHibernate) certificateEntry);
        }
        
        return result;
    }
    
    @Override
    public List<UserPreferences> getReferencingFromNamedCertificates(X509Certificate certificate, Integer firstResult,
            Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<UserPreferences>();
        
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromNamedCertificates(
                    (X509CertStoreEntryHibernate) certificateEntry, firstResult, maxResults);
            
            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }
        
        return result;
    }

    @Override
    public long getReferencingFromNamedCertificatesCount(X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;
        
        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            result = createDAO().referencingFromNamedCertificatesCount(
                    (X509CertStoreEntryHibernate) certificateEntry);
        }
        
        return result;
    }
        
    @Override
    public List<UserPreferences> getReferencingFromKeyAndCertificate(X509Certificate certificate, Integer firstResult,
            Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<UserPreferences>();

        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromKeyAndCertificate(
                    (X509CertStoreEntryHibernate) certificateEntry, firstResult, maxResults);

            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }

        return result;
    }
    
    @Override
    public long getReferencingFromKeyAndCertificateCount(X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;
        
        /*
         * We can only check if the X509CertStoreEntry is a Hibernate entity. 
         */
        if (certificateEntry != null && (certificateEntry instanceof X509CertStoreEntryHibernate))
        {
            result = createDAO().referencingFromKeyAndCertificateCount(
                    (X509CertStoreEntryHibernate) certificateEntry);
        }
        
        return result;
    }    
    
    private UserPreferencesDAO createDAO() {
        return new UserPreferencesDAO(SessionAdapterFactory.create(sessionManager.getSession()));
    }
    
    @Override
    public long getReferencingFromNamedBlobsCount(NamedBlob namedBlob)
    {
        /*
         * We only support NamedBlobEntity's
         */
        if (!(namedBlob instanceof NamedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }
        
        return createDAO().referencingFromNamedBlobsCount((NamedBlobEntity) namedBlob);
    }
    
    @Override
    public List<UserPreferences> getReferencingFromNamedBlobs(NamedBlob namedBlob, 
            Integer firstResult, Integer maxResults)
    {
        List<UserPreferences> result = new LinkedList<UserPreferences>();
        
        /*
         * We only support NamedBlobEntity's
         */
        if (!(namedBlob instanceof NamedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }
        
        List<UserPreferencesEntity> entities = createDAO().getReferencingFromNamedBlobs(
                (NamedBlobEntity) namedBlob, firstResult, maxResults);
        
        if (entities != null)
        {
            for (UserPreferencesEntity entity : entities) {
                result.add(createUserPreferences(entity));
            }
        }
        
        return result;
    }    
}
