/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserManager;
import mitm.application.djigzo.UserPreferenceMerger;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesSelector;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.SortDirection;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;

public class UserWorkflowImpl implements UserWorkflow
{
    private final UserManager userManager;
    private final GlobalPreferencesManager globalPreferencesManager;
    private final UserPreferencesSelector userPreferencesSelector;
    private final UserPreferenceMerger userPreferenceMerger;
    
    public UserWorkflowImpl(UserManager userManager, GlobalPreferencesManager globalPreferencesManager, 
            UserPreferencesSelector userPreferencesSelector, UserPreferenceMerger userPreferenceMerger)
    {
    	Check.notNull(userManager, "userManager");
    	Check.notNull(globalPreferencesManager, "globalPreferencesManager");
    	Check.notNull(userPreferencesSelector, "userPreferencesSelector");
    	Check.notNull(userPreferenceMerger, "userPreferenceMerger");
        
        this.userManager = userManager;
        this.globalPreferencesManager = globalPreferencesManager;
        this.userPreferencesSelector = userPreferencesSelector;
        this.userPreferenceMerger = userPreferenceMerger;
    }
    
    @Override
    public User getUser(String email, GetUserMode userMode)
    throws AddressException, HierarchicalPropertiesException    
    {
        User user = userManager.getUser(email);
        
        if (user == null && userMode == GetUserMode.CREATE_IF_NOT_EXIST) {
            user = addUser(email);
        }
        
        if (user != null) 
        {
            /* 
             * update the user preferences of this user. The strategy used depends on the actual 
             * userPreferencesSelector implementation.
             */
            Set<UserPreferences> selectedUserPreferences = userPreferencesSelector.select(user);
            
            Set<UserPreferences> inheritedPrefereces = user.getUserPreferences().getInheritedUserPreferences();            
            /* 
             * merge the selectedUserPreferences with the existing user preferences.
             */
            Set<UserPreferences> merged = userPreferenceMerger.merge(inheritedPrefereces, selectedUserPreferences);
            
            /*
             * Replace the inherited user preferences by the new merged preferences
             */
            if (merged != null) 
            {
                inheritedPrefereces.clear();
                inheritedPrefereces.addAll(merged);
            }
        }
        
        return user;
    }
    
    @Override
    public Set<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection) 
    throws AddressException, CloseableIteratorException, HierarchicalPropertiesException
    {
        Set<User> users = new LinkedHashSet<User>();
        
        CloseableIterator<String> emailIterator = userManager.getEmailIterator(firstResult, 
        		maxResults, sortDirection);
        
        try {
            while (emailIterator.hasNext())
            {
                String email = emailIterator.next();
                
                email = EmailAddressUtils.canonicalizeAndValidate(email, false);
                
                User user = this.getUser(email, GetUserMode.CREATE_IF_NOT_EXIST);
                
                users.add(user);
            }
        }
        finally {
            emailIterator.close();
        }
        
        return users;
    }
    
    @Override
    public int getUserCount()
    {
        return userManager.getUserCount();
    }

    @Override
    public User addUser(String email) 
    throws AddressException, HierarchicalPropertiesException    
    {
        User user = userManager.addUser(email, UserManager.AddMode.NON_PERSISTENT);
        
        /* every user inherits the global preferences */
        user.getUserPreferences().getInheritedUserPreferences().add(
                globalPreferencesManager.getGlobalUserPreferences());
        
        /*
         * Note: When a user is added, the user only inherits from the global preferences. When getting a user
         * it is checked whether the user should inherit from other preferences as well (see getUser).
         */
        return user;
    }
    
    @Override
    public boolean deleteUser(User user) {
        return userManager.deleteUser(user);
    }
    
    @Override
    public void makePersistent(User user) {
        userManager.makePersistent(user);
    }
    
    @Override
    public boolean isPersistent(User user) {
        return userManager.isPersistent(user);
    }   
    
    @Override
    public CloseableIterator<String> getEmailIterator() {
    	return userManager.getEmailIterator();
    }

    @Override
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection) 
    {
    	return userManager.getEmailIterator(firstResult, maxResults, sortDirection);
    }
    
    @Override
    public CloseableIterator<String> searchEmail(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
	{
    	return userManager.searchEmail(search, firstResult, maxResults, sortDirection);
	}

    @Override
    public int getSearchEmailCount(String search) {
    	return userManager.getSearchEmailCount(search);
    }
}
