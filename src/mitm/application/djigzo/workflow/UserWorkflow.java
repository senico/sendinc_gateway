/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow;

import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.User;
import mitm.common.hibernate.SortDirection;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;

public interface UserWorkflow
{
    enum GetUserMode {NULL_IF_NOT_EXIST, CREATE_IF_NOT_EXIST};
    
    /**
     * Returns the user. Return value if a user does not exists depends on the userMode
     * parameter. If CREATE_IF_NOT_EXIST addUser is called.
     */
    public User getUser(String email, GetUserMode userMode)
    throws AddressException, HierarchicalPropertiesException;    
    
    /**
     * Returns a bounded set of users. 
     */
    public Set<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws AddressException, CloseableIteratorException, HierarchicalPropertiesException;

    /**
     * Returns the number of users.
     */
    public int getUserCount();
    
    /**
     * Adds a user with the given email. The user is not yet persisted. Use makePersistent 
     * to persist the user.
     */
    public User addUser(String email)
    throws AddressException, HierarchicalPropertiesException;
    
    /**
     * Deletes the user. Returns true if deleted.
     */
    public boolean deleteUser(User user);
    
    /**
     * Makes the user persistent ie. write the user to the DB.
     */
    public void makePersistent(User user);

    /**
     * Returns true if the user is persistent.
     */
    public boolean isPersistent(User user);
    
    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    public CloseableIterator<String> getEmailIterator();

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection);
    
    /**
     * Returns an iterator that returns all the email addresses of all the users that match the
     * search string (using LIKE). This iterator must be manually closed.
     */
    public CloseableIterator<String> searchEmail(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection);

    /**
     * Returns the number of email address that will be returned by a call to searchEmail when 
     * when maxResults is MAXINT.
     */
    public int getSearchEmailCount(String search);
}
