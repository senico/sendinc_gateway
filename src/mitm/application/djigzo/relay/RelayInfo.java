/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.relay;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * RelayInfo is used for unmarshalling the xml sent by the Djigzo BlackBerry plugin and other
 * services that need to relay.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Relay")
public class RelayInfo
{
    @XmlAttribute
    private String version;

    @XmlAttribute
    protected Long timestamp;
    
    /*
     * Note: the nonce should be unique for a sender. It was better to give it a different name but we leave it like
     * this because the released version already expect this name. Combined with timestamp it should be globally
     * unique
     */
    @XmlAttribute
    protected String nonce;

    @XmlElement
    protected String from;
    
    @XmlElement(name = "recipient")
    protected List<RelayRecipient> recipients;

    public String getVersion() {
        return version;
    }

    public Long getTimestamp() {
        return timestamp;
    }
        
    public String getNonce() {
        return nonce;
    }
    
    public String getFrom() {
        return from;
    }
    
    /**
     * Gets the value of the recipients property.
     */
    public List<RelayRecipient> getRecipients()
    {
        if (recipients == null) {
            recipients = new ArrayList<RelayRecipient>();
        }
        return this.recipients;
    }
}
