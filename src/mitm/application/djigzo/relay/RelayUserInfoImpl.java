/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.relay;

import java.security.cert.X509Certificate;
import java.util.Collection;

import mitm.common.util.Check;

public class RelayUserInfoImpl implements RelayUserInfo
{
    /*
     * The email address of the user
     */
    private final String email;
    
    /*
     * The allowed signing certificates for relaying the message
     */
    private final Collection<X509Certificate> relayCertificates;
    
    /*
     * true if Relaying is allowed
     */
    private final boolean relayAllowed;
    
    /*
     * The time in milliseconds a relay message is valid (the relay message
     * is timestamped). 
     */
    private final long validityInterval;
    
    /*
     * Bounce mode determines whether a message is bounced or not
     */
    private final RelayBounceMode bounceMode;
    
    public RelayUserInfoImpl(String email, Collection<X509Certificate> relayCertificates, 
            boolean relayAllowed, long validityInterval, RelayBounceMode bounceMode)
    {
        Check.notNull(relayAllowed, "relayAllowed");
        Check.notNull(bounceMode, "bounceMode");
     
        this.email = email;
        this.relayCertificates = relayCertificates;
        this.relayAllowed = relayAllowed;
        this.validityInterval = validityInterval;
        this.bounceMode = bounceMode;
    }

    @Override
    public String getEmail() {
        return email;
    }
    
    @Override
    public Collection<X509Certificate> getRelayCertificates() {
        return relayCertificates;
    }

    @Override
    public boolean isRelayAllowed() {
        return relayAllowed;
    }

    @Override
    public long getValidityInterval() {
        return validityInterval;
    }
    
    @Override
    public RelayBounceMode getBounceMode() {
        return bounceMode;
    }    
}
