package mitm.application.djigzo.relay;

public interface RelayValidator
{
    /**
     * Validates whether a relay is allowed. Should throw a RelayException if relay is not allowed.
     */
    void validate(RelayInfo relayInfo)
    throws RelayException;
}
