/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.mail.repository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DLPProperties;
import mitm.application.djigzo.TemplateProperties;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.JamesStoreManager;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MailetUtils;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.mail.MailUtils;
import mitm.common.mail.repository.MailRepositoryEventListener;
import mitm.common.mail.repository.MailRepositoryItem;
import mitm.common.template.TemplateBuilder;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.apache.avalon.framework.service.ServiceException;
import org.apache.avalon.framework.service.ServiceManager;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Default implementation of MailRepositoryEventListener.
 * 
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryEventListenerImpl implements MailRepositoryEventListener
{
    private final static Logger logger = LoggerFactory.getLogger(MailRepositoryEventListenerImpl.class);
    
    /*
     * Internally used enum to know which event was fired 
     */
    private static enum Notification {DELETE, RELEASE, EXPIRE};
    
    /*
     * For getting the user objects from an email address
     */
    private final UserWorkflow userWorkflow;
    
    /*
     * ServiceManager is used for injecting the released mail into the James spool.
     */
    private final ServiceManager serviceManager;

    /*
     * Service used to build Freemarker templates
     */
    private final TemplateBuilder templateBuilder;
    
    /*
     * The processor for newly injected messages
     */
    private final String newMailProcessor;
        
    public MailRepositoryEventListenerImpl(UserWorkflow userWorkflow, ServiceManager serviceManager, 
            TemplateBuilder templateBuilder, String newMailProcessor)
    {
        Check.notNull(userWorkflow, "userWorkflow");
        Check.notNull(serviceManager, "serviceManager");
        Check.notNull(templateBuilder, "templateBuilder");

        this.userWorkflow = userWorkflow;
        this.serviceManager = serviceManager;
        this.templateBuilder = templateBuilder;
        this.newMailProcessor = newMailProcessor;
    }

    private void sendNotification(Notification notification, MailRepositoryItem item, Set<String> recipients,
            String templateSource)
    {
        SimpleHash root = new SimpleHash();
        SimpleHash org = new SimpleHash();

        org.put("subject", StringUtils.defaultString(item.getSubject()));
        org.put("id", StringUtils.defaultString(item.getID()));
        org.put("from", StringUtils.defaultString(item.getFromHeader()));
        org.put("messageID", StringUtils.defaultString(item.getMessageID()));

        root.put("org", org);
        root.put("recipients", recipients);
        
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(bos);

            Template template = templateBuilder.createTemplate(new StringReader(templateSource));
            
            template.process(root, writer);
            
            MimeMessage message = MailUtils.byteArrayToMessage(bos.toByteArray());
            
            message.saveChanges();
            
            MailUtils.validateMessage(message);
            
            MailImpl mail = new MailImpl(MailetUtils.createUniqueMailName(), null, 
                    MailAddressUtils.toMailAddressList(recipients), message);
            
            mail.setState(newMailProcessor);
            
            JamesStoreManager storemanager = new JamesStoreManager(serviceManager);
            
            try {
                storemanager.store(mail);
            }
            finally {
                mail.dispose();
            }
        } 
        catch (IOException e) {
            logger.error("Error creating the notification template.", e);
        } 
        catch (TemplateException e) {
            logger.error("The template is not a valid Freemarker template or variables are missing.", e);
        } 
        catch (MessagingException e) {
            logger.error("The resulting mime message is not valid.", e);
        } 
        catch (ServiceException e) {
            logger.error("Error getting the JamesStoreManager.", e);
        }
    }
    
    /*
     * We assume that the caller already started a database transaction.
     */
    private void notifyUsers(Notification notification, MailRepositoryItem item)
    {
        try {
            /*
             * Get the user. If the from is invalid, use the default user
             */
            String filteredOriginator = EmailAddressUtils.canonicalizeAndValidate(
                    EmailAddressUtils.getEmailAddress(item.getOriginator()), 
                    false /* INVALID_EMAIL if invalid or null */);
            
            User user = userWorkflow.getUser(filteredOriginator, GetUserMode.CREATE_IF_NOT_EXIST);
            
            UserProperties properties = user.getUserPreferences().getProperties();
            
            DLPProperties dlpProperties = properties.getDLPProperties();
            TemplateProperties templateProperties = properties.getTemplateProperties();
            
            boolean notifyOriginator;
            boolean notifyDLPManagers;
            
            String templateSource;
            
            switch(notification)
            {
            case DELETE: 
                notifyOriginator = dlpProperties.isSendDeleteNotifyToOriginator();
                notifyDLPManagers = dlpProperties.isSendDeleteNotifyToDLPManagers();
                templateSource = templateProperties.getDLPDeleteNotificationTemplate();
            break;
            case RELEASE: 
                notifyOriginator = dlpProperties.isSendReleaseNotifyToOriginator();
                notifyDLPManagers = dlpProperties.isSendReleaseNotifyToDLPManagers();
                templateSource = templateProperties.getDLPReleaseNotificationTemplate();
            break;
            case EXPIRE: 
                notifyOriginator = dlpProperties.isSendExpireNotifyToOriginator();
                notifyDLPManagers = dlpProperties.isSendExpireNotifyToDLPManagers();
                templateSource = templateProperties.getDLPExpireNotificationTemplate();
            break;
            
            default:
                throw new IllegalArgumentException("Unknown Notification: " + notification);
            }
                                                
            Set<String> recipients = new HashSet<String>();
            
            /*
             * It can be that the originator address stored in the MailRepositoryItem is the invalid dummy
             * address (invalid@invalid.tld). We do not want to send email to the dummy user.
             */
            if (EmailAddressUtils.isInvalidDummyAddress(filteredOriginator)) {
                notifyOriginator = false;
            }
            
            if (notifyOriginator && filteredOriginator != null) {
                recipients.add(filteredOriginator);
            }

            Set<String> managers = EmailAddressUtils.canonicalizeAndValidate(
                    dlpProperties.getDLPManagers(), true /* skipIfInvalid */);
            
            if (CollectionUtils.isEmpty(managers)) {
                notifyDLPManagers = false;
            }
            
            if (notifyDLPManagers && managers != null) {
                recipients.addAll(managers);
            }
            
            if (recipients.size() > 0)
            {
                if (StringUtils.isNotBlank(templateSource)) {
                    sendNotification(notification, item, recipients, templateSource);
                }
                else {
                    logger.debug("The notification template is empty.");
                }
            }
            else {
                logger.debug("There are no recipient to notify.");
            }
        }
        catch (Exception e) {
            logger.error("Error while notifying users.", e);
        }
    }
    
    private String createMessage(MailRepositoryItem item, String action, String repository)
    {
        return "The message with id " + item.getID() + " from " + item.getFromHeader() + " with message-id " +
                item.getMessageID() + " was " + action + " from " + repository;
    }
    
    @Override
    public void onDeleted(String repository, MailRepositoryItem item)
    {
        logger.info(createMessage(item, "deleted", repository));
        
        notifyUsers(Notification.DELETE, item);
    }

    @Override
    public void onReleased(String repository, MailRepositoryItem item)
    {
        logger.info(createMessage(item, "released", repository));
        
        notifyUsers(Notification.RELEASE, item);
    }

    @Override
    public void onExpired(String repository, MailRepositoryItem item)
    {
        logger.info(createMessage(item, "deleted because it expired", repository));
        
        notifyUsers(Notification.EXPIRE, item);
    }
}
