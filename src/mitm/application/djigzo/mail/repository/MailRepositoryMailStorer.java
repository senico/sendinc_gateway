/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.mail.repository;

import java.io.IOException;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.mail.MailSerializer;
import mitm.common.mail.repository.MailRepository;
import mitm.common.mail.repository.MailRepositoryItem;
import mitm.common.util.Check;

import org.apache.mailet.Mail;

/**
 * MailStorer implementation that stores the mail in a MailRepository. 
 * 
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryMailStorer implements MailStorer
{
    /*
     * The mail repository to store the mail into
     */
    private final MailRepository repository;
    
    /*
     * Used to identify the 'sender'. The default MessageOriginatorIdentifier uses the from as 
     * the identifier.
     */
    private final MessageOriginatorIdentifier originatorIdentifier;
    
    public MailRepositoryMailStorer(MailRepository repository, MessageOriginatorIdentifier originatorIdentifier)
    {
        Check.notNull(repository, "repository");
        Check.notNull(originatorIdentifier, "originatorIdentifier");
        
        this.repository = repository;
        this.originatorIdentifier = originatorIdentifier;
    }
    
    @Override
    public void store(Mail mail)
    throws MessagingException, IOException
    {
        Check.notNull(mail, "mail");
        
        MailRepositoryItem item = repository.createItem(mail.getMessage());
    
        item.setRecipients(MailAddressUtils.toInternetAddressList(MailAddressUtils.getRecipients(mail)));
        item.setRemoteAddress(mail.getRemoteAddr());
        item.setSender(MailAddressUtils.toInternetAddress(mail.getSender()));
        item.setOriginator(originatorIdentifier.getOriginator(mail));
        
        /*
         * Store the Mail object in the additional data blob so the Mail item can be 'resurrected'
         * when it is released from the store (for example the message is release from quarantine) 
         */
        item.setAdditionalData(new MailSerializer().serialize(mail));
        
        repository.addItem(item);
        
        /*
         * Store the ID of the MailRepositoryItem in the Mail object (this is required for example when
         * a link to the stored email should be generated).
         */
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        attributes.setMailRepositoryID(item.getID());
    }
}
