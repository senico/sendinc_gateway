/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import mitm.application.djigzo.PropertyRegistry;
import mitm.application.djigzo.TemplateProperties;
import mitm.common.properties.DelegatedHierarchicalProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;

public class TemplatePropertiesImpl extends DelegatedHierarchicalProperties implements TemplateProperties
{
    /* 
     * The base name of the EmailRoleProperties. The actual name of a property will be based upon
     * the role, base name and the name of the property.
     */
    private final static String BASE_NAME = "user.template.";
            
    public TemplatePropertiesImpl(HierarchicalProperties properties) {
        super(properties, null);
    }
    
    /*
     * Returns the full property name which is a combination of base name
     * and property name.
     */
    @Override
    public String getFullPropertyName(String property) {
        return staticGetFullPropertyName(property);
    }

    private static String staticGetFullPropertyName(String property) {
        return BASE_NAME + property;
    }
    
    /*
     * Register all the known properties using a static initializer
     * 
     * Note: when adding new properties always add them to the property registry 
     */
    static 
    {
        PropertyRegistry registry = PropertyRegistry.getInstance();

        registry.registerProperty(staticGetFullPropertyName(ENCRYPTED_PDF), false);
        registry.registerProperty(staticGetFullPropertyName(ENCRYPTED_PDF_SMS), false);
        registry.registerProperty(staticGetFullPropertyName(ENCRYPTED_PDF_OTP), false);
        registry.registerProperty(staticGetFullPropertyName(ENCRYPTED_PDF_OTP_INVITE), false);
    	registry.registerProperty(staticGetFullPropertyName(ENCRYPTION_FAILED_NOTIFICATION), false);
        registry.registerProperty(staticGetFullPropertyName(ENCRYPTION_NOTIFICATION), false);
        registry.registerProperty(staticGetFullPropertyName(PASSWORDS_NOTIFICATION), false);
        registry.registerProperty(staticGetFullPropertyName(SMS_PASSWORD), false);
        registry.registerProperty(staticGetFullPropertyName(SMS_PFX_PASSWORD), false);
        registry.registerProperty(staticGetFullPropertyName(MAIL_PFX), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_WARNING), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_QUARANTINE), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_BLOCK), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_ERROR), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_RELEASE_NOTIFICATION), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_DELETE_NOTIFICATION), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_EXPIRE_NOTIFICATION), false);
    }
    
    @Override
    public void setEncryptedPdfTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTED_PDF), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF)));
    }
    
    @Override
    public String getEncryptedPdfTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTED_PDF), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF)));
    }
    
    @Override
    public void setEncryptedPdfSmsTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTED_PDF_SMS), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF_SMS)));
    }
    
    @Override
    public String getEncryptedPdfSmsTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTED_PDF_SMS), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF_SMS)));
    }

    @Override
    public void setEncryptedPdfOTPTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTED_PDF_OTP), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF_OTP)));
    }
    
    @Override
    public String getEncryptedPdfOTPTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTED_PDF_OTP), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(ENCRYPTED_PDF_OTP)));
    }    

    @Override
    public void setEncryptedPdfOTPInviteTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTED_PDF_OTP_INVITE), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTED_PDF_OTP_INVITE)));
    }
    
    @Override
    public String getEncryptedPdfOTPInviteTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTED_PDF_OTP_INVITE), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTED_PDF_OTP_INVITE)));
    }    

    @Override
    public void setEncryptionFailedNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTION_FAILED_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTION_FAILED_NOTIFICATION)));
    }
    
    @Override
    public String getEncryptionFailedNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTION_FAILED_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTION_FAILED_NOTIFICATION)));
    }
    
    @Override
    public void setEncryptionNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(ENCRYPTION_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTION_NOTIFICATION)));
    }
    
    @Override
    public String getEncryptionNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(ENCRYPTION_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPTION_NOTIFICATION)));
    }

    @Override
    public void setPasswordsNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(PASSWORDS_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(PASSWORDS_NOTIFICATION)));
    }
    
    @Override
    public String getPasswordsNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(PASSWORDS_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(PASSWORDS_NOTIFICATION)));
    }
    
    @Override
    public void setSMSPasswordTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SMS_PASSWORD), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PASSWORD)));
    }
    
    @Override
    public String getSMSPasswordTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SMS_PASSWORD), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PASSWORD)));
    }
    
    @Override
    public void setBBSMIMEAdapterTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(BB_SMIME_ADAPTER), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(BB_SMIME_ADAPTER)));
    }
    
    @Override
    public String getBBSMIMEAdapterTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(BB_SMIME_ADAPTER), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(BB_SMIME_ADAPTER)));
    }
    
    @Override
    public void setSMSPFXPasswordTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SMS_PFX_PASSWORD), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PFX_PASSWORD)));
    }
    
    @Override
    public String getSMSPFXPasswordTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SMS_PFX_PASSWORD), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PFX_PASSWORD)));
    }
    
    @Override
    public void setMailPFXTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(MAIL_PFX), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(MAIL_PFX)));
    }
    
    @Override
    public String getMailPFXTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(MAIL_PFX), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(MAIL_PFX)));
    }
    
    @Override
    public void setDLPWarningTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_WARNING), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_WARNING)));
    }
    
    @Override
    public String getDLPWarningTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_WARNING), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_WARNING)));
    }

    @Override
    public void setDLPQuarantineTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_QUARANTINE), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_QUARANTINE)));
    }
    
    @Override
    public String getDLPQuarantineTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_QUARANTINE), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_QUARANTINE)));
    }

    @Override
    public void setDLPBlockTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_BLOCK), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_BLOCK)));
    }
    
    @Override
    public String getDLPBlockTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_BLOCK), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_BLOCK)));
    }
    
    @Override
    public void setDLPErrorTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_ERROR), template, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_ERROR)));
    }
    
    @Override
    public String getDLPErrorTemplate()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_ERROR), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(DLP_ERROR)));
    }
    
    @Override
    public void setDLPReleaseNotificationTemplate(String template) 
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_RELEASE_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_RELEASE_NOTIFICATION)));
    }

    @Override
    public String getDLPReleaseNotificationTemplate() 
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_RELEASE_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_RELEASE_NOTIFICATION)));
    }

    @Override
    public void setDLPDeleteNotificationTemplate(String template) 
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_DELETE_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_DELETE_NOTIFICATION)));
    }

    @Override
    public String getDLPDeleteNotificationTemplate() 
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_DELETE_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_DELETE_NOTIFICATION)));
    }

    @Override
    public void setDLPExpireNotificationTemplate(String template) 
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(DLP_EXPIRE_NOTIFICATION), template, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_EXPIRE_NOTIFICATION)));
    }

    @Override
    public String getDLPExpireNotificationTemplate() 
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(DLP_EXPIRE_NOTIFICATION), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(DLP_EXPIRE_NOTIFICATION)));
    }
    
    @Override
    public String getDefaultValue(String property)
    throws HierarchicalPropertiesException
    {
        /*
         * Not defaults provided
         */
        return null;
    }
}
