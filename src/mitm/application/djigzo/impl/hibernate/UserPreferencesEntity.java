/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mitm.common.properties.hibernate.NamedBlobEntity;
import mitm.common.properties.hibernate.PropertyEntity;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Index;

@Entity(name = UserPreferencesEntity.ENTITY_NAME)
@Table(name = "userpreferences",
        uniqueConstraints = {@UniqueConstraint(columnNames={"category", "name"})}
)
@org.hibernate.annotations.Table(appliesTo = UserPreferencesEntity.ENTITY_NAME, indexes = {
        @Index(name = "userpreferences_name_index",     columnNames = {"name"}),
        @Index(name = "userpreferences_category_index", columnNames = {"category"})
        }
)
public class UserPreferencesEntity implements Serializable
{
    public static final String ENTITY_NAME = "UserPreferences";
    
    public static final String INHERITED_PREFERENCES_NAME = "userpreferences_inheritedpreferences";

    public static final String NAMED_CERTIFICATES_NAME = "userpreferences_named_certificates";
    
    private static final long serialVersionUID = 9125242459498547056L;

    /*
     * Use maximum length of an email address as max length
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_NAME_LENGTH = 64 + 1 + 255; 

    /*
     * Use maximum length of an email address as max length
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_CATEGORY_LENGTH = 64 + 1 + 255;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The name of the UserPreferences
     */
    @Column (name = "name", unique = false, nullable = false, length = MAX_NAME_LENGTH)
    @Cascade(CascadeType.SAVE_UPDATE)
    private String name;

    /**
     * The category of the group.
     */
    @Column (name = "category", unique = false, nullable = false, length = MAX_CATEGORY_LENGTH)
    @Cascade(CascadeType.SAVE_UPDATE)
    private String category;
    
    @ManyToMany
    @Cascade(CascadeType.SAVE_UPDATE)
    private Set<X509CertStoreEntryHibernate> certificates = new HashSet<X509CertStoreEntryHibernate>();

    @org.hibernate.annotations.CollectionOfElements
    @JoinTable(name = NAMED_CERTIFICATES_NAME)
    private Set<NamedCertificateHibernate> namedCertificates = new HashSet<NamedCertificateHibernate>();
    
    @OneToOne
    private X509CertStoreEntryHibernate keyAndCertificateEntry;
    
    @OneToOne
    @Cascade(CascadeType.ALL)
    private PropertyEntity propertyEntity;

    @ManyToMany
    private Set<NamedBlobEntity> namedBlobs = new HashSet<NamedBlobEntity>();
    
    @org.hibernate.annotations.CollectionOfElements
    @JoinTable(name = INHERITED_PREFERENCES_NAME)
    @OrderBy("index")
    private Set<InheritedUserPreferences> inheritedPreferences = new LinkedHashSet<InheritedUserPreferences>();
    
    protected UserPreferencesEntity() {
        // Hibernate requires default constructor
    }

    public UserPreferencesEntity(String category, String name)
    {
    	Check.notNull(category, "category");
    	Check.notNull(name, "name");
        
        this.category = category;
        this.name = name;
        
        /*
         * #getPropertyEntityKey must be called after setting category and name
         */
        this.propertyEntity = new PropertyEntity(getPropertyEntityKey());        
    }
    
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }
    
    public Set<X509CertStoreEntryHibernate> getCertificates() {
        return certificates;
    }
    
    public Set<InheritedUserPreferences> getInheritedPreferences() {
        return inheritedPreferences;
    }

    public Set<NamedCertificateHibernate> getNamedCertificates() {
        return namedCertificates;
    }
    
    public X509CertStoreEntryHibernate getKeyAndCertificateEntry() {
        return keyAndCertificateEntry;
    }

    public void setKeyAndCertificate(X509CertStoreEntryHibernate keyAndCertificate) {
        this.keyAndCertificateEntry = keyAndCertificate;
    }
    
    public PropertyEntity getProperyEntity() {
        return propertyEntity;
    }

    public Set<NamedBlobEntity> getNamedBlobs() {
        return namedBlobs;
    }
    
    private String getPropertyEntityKey() {
        return getCategory() + ":" + getName();
    }
    
    @Override
    public String toString() {
        return getPropertyEntityKey();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferencesEntity)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        UserPreferencesEntity rhs = (UserPreferencesEntity) obj;
        
        return new EqualsBuilder()
            .append(name, rhs.name)
            .append(category, rhs.category)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(name)
            .append(category)
            .toHashCode();    
    }    
}