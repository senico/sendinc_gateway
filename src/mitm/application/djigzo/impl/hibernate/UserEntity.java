/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import mitm.application.djigzo.UserPreferencesCategory;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity(name = UserEntity.ENTITY_NAME)
@Table(name = "users")
public class UserEntity implements Serializable
{
    public static final String ENTITY_NAME = "User";

    private static final long serialVersionUID = 426933903512665468L;

    /*
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255; 
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (name = "email", unique = true, nullable = false, length = MAX_EMAIL_LENGTH)
    private String email;
        
    @OneToOne
    @Cascade(CascadeType.ALL)    
    private UserPreferencesEntity userPreferencesEntity;

    protected UserEntity() {
        /* required by Hibernate */
    }
    
    public UserEntity(String email) 
    {
    	Check.notNull(email, "email");
        
        /* we only want users with valid email addresses */
        email = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (email == null) {
            throw new IllegalArgumentException("email address is not a valid email address.");
        }
        
        this.email = email;
        
        /* the name of the associated preferences object will be prefixed */
        this.userPreferencesEntity = new UserPreferencesEntity(UserPreferencesCategory.USER.getName(), email);
    }
    
    public Long getId() {
        return id;
    }
    
    public String getEmail() {
        return email;
    }
    
    public UserPreferencesEntity getUserPreferencesEntity() {
        return userPreferencesEntity;
    }
    
    @Override
    public String toString() {
        return getEmail();
    }
    /**
     * User is equals iff email address is equal.
     */    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserEntity)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        UserEntity rhs = (UserEntity) obj;
        
        return new EqualsBuilder()
            .append(email, rhs.email)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(email)
            .toHashCode();    
    }    
}
