/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import mitm.common.hibernate.AbstractScrollableResultsIterator;
import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.CloseableIterator;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class UserDAO extends GenericHibernateDAO<UserEntity, Long>
{
    private String entityName = UserEntity.ENTITY_NAME;
    
    public UserDAO(SessionAdapter session) 
    {
        super(session);
    }

    public UserEntity getUser(String email)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq("email", email));
                
        return (UserEntity) criteria.uniqueResult();
    }
    
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection) 
    {
    	if (sortDirection == null) {
    		sortDirection = SortDirection.ASC;
    	}

    	/*
    	 * We will use HQL because I do not (yet?) know how to return just a field instead of 
    	 * an object when using the Criteria API.
    	 */
        final String hql = "select n.email from " + entityName + " n order by n.email " + 
        		sortDirection;
        
        Query query = createQuery(hql);
     
        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }
        
        ScrollableResults scrollableResults = query.scroll(ScrollMode.FORWARD_ONLY);
        
        return new EmailIterator(scrollableResults);
    }

    public CloseableIterator<String> searchEmail(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection) 
    {
    	if (sortDirection == null) {
    		sortDirection = SortDirection.ASC;
    	}

    	/*
    	 * We will use HQL because I do not (yet?) know how to return just a field instead of 
    	 * an object when using the Criteria API.
    	 */
        final String hql = "select n.email from " + entityName + " n  where n.email LIKE :search order by n.email " + 
        		sortDirection;
        
        Query query = createQuery(hql);
     
        query.setParameter("search", search);
        
        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }
        
        ScrollableResults scrollableResults = query.scroll(ScrollMode.FORWARD_ONLY);
        
        return new EmailIterator(scrollableResults);
    }

    public int getSearchEmailCount(String search) 
    {
        Criteria criteria = createCriteria(entityName);
        
        criteria.setProjection(Projections.rowCount());
        
        criteria.add(Restrictions.ilike("email", search));
                
        return (Integer) criteria.uniqueResult();
    }
    
    public UserEntity addUser(String email, boolean makePersistent)
    {
        UserEntity userEntity = new UserEntity(email);
        
        if (makePersistent) {
            userEntity = makePersistent(userEntity);
        }
        
        return userEntity;
    }
    
    public void deleteUser(UserEntity userEntity) {
        delete(userEntity);
    }
    
    public int getUserCount()
    {
        Criteria criteria = createCriteria(entityName);
        
        criteria.setProjection(Projections.rowCount());
        
        return (Integer) criteria.uniqueResult();        
    }
    
    private static class EmailIterator extends AbstractScrollableResultsIterator<String>
    {
        public EmailIterator(ScrollableResults results) {
            super(results);
        }
        
        @Override
        protected boolean hasMatch(String element) {
            return element != null;
        }
    }
}
