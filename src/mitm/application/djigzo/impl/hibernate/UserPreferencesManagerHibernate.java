/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.common.hibernate.CommitOnCloseIterator;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;

import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A UserPreferencesManagerHibernate manages UserPreferences for a specific category (for example user category,
 * global category etc.).
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesManagerHibernate implements UserPreferencesManager
{
    private final static Logger logger = LoggerFactory.getLogger(UserPreferencesManagerHibernate.class);
    
    private final static String INCORRECT_IMPLEMENTATION = 
    	"UserPreferences cannot be deleted because user is not a UserPreferencesHibernate.";
    
    /*
     * The category of UserPreferences managed by this instance.
     */
    private final String category;
    
    /*
     * The store containing private keys and X509Certificates.
     */
    private final KeyAndCertStore keyAndCertStore;
    
    /*
     * The system encryptor
     */
    private final Encryptor encryptor;
    
    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
    
    protected UserPreferencesManagerHibernate(String category, KeyAndCertStore keyAndCertStore,
    		Encryptor encryptor, SessionManager sessionManager) 
    { 
    	Check.notNull(category, "category");
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(encryptor, "encryptor");
    	Check.notNull(sessionManager, "sessionManager");
        
        this.category = category;
        this.keyAndCertStore = keyAndCertStore;
        this.encryptor = encryptor;
        this.sessionManager = sessionManager;
    }
    
    @Override
    public UserPreferences addUserPreferences(String name)
    {
        UserPreferencesEntity entity = getDAO().addPreferences(category, name);
        
        return getUserPreferences(entity);
    }

    @Override
    public boolean deleteUserPreferences(UserPreferences userPreferences) 
    {
        boolean deleted = false;

        if (userPreferences instanceof UserPreferencesHibernate) 
        {
            UserPreferencesHibernate userPreferencesHibernate = (UserPreferencesHibernate) userPreferences;

            getDAO().deletePreferences(userPreferencesHibernate.getUserPreferencesEntity());

            deleted = true;
        }
        else {
            logger.warn(INCORRECT_IMPLEMENTATION);
        }

        return deleted;
    }
    
    @Override
    public boolean isInUse(UserPreferences userPreferences) 
    {
        boolean inUse = false;
        
        if (userPreferences instanceof UserPreferencesHibernate) 
        {
            UserPreferencesHibernate userPreferencesHibernate = (UserPreferencesHibernate) userPreferences;
            
            inUse = getDAO().isInherited(userPreferencesHibernate.getUserPreferencesEntity());
        }
        else {
            logger.warn(INCORRECT_IMPLEMENTATION);
        }
        
        return inUse;
    }
    
    @Override
    public UserPreferences getUserPreferences(String name) 
    {
        UserPreferences userPreferences = null;
        
        UserPreferencesEntity entity = getDAO().getPreferences(category, name);
        
        if (entity != null) {
            userPreferences = getUserPreferences(entity);
        }
        
        return userPreferences;
    }
    
    @Override
    public int getUserPreferencesCount() {
    	return getDAO().getUserPreferencesCount(category);
    }
    
    @Override
    public CloseableIterator<String> getNameIterator()
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        CloseableIterator<String> iterator = getStatelessDAO(statelessSession).getPreferencesIterator(category);
        
        CloseableIterator<String> commitOnCloseIterator = new CommitOnCloseIterator<String>(iterator, 
                SessionAdapterFactory.create(statelessSession), tx);
        
        return commitOnCloseIterator;
    }

    @Override
    public CloseableIterator<String> getStatefulNameIterator() {
        return getDAO().getPreferencesIterator(category);
    }
    
    @Override
    public CloseableIterator<String> getNameIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        CloseableIterator<String> iterator = getStatelessDAO(statelessSession).getPreferencesIterator(category,
        		firstResult, maxResults, sortDirection);
        
        CloseableIterator<String> commitOnCloseIterator = new CommitOnCloseIterator<String>(iterator, 
                SessionAdapterFactory.create(statelessSession), tx);
        
        return commitOnCloseIterator;
    }

    @Override
    public CloseableIterator<String> getStatefulNameIterator(Integer firstResult, Integer maxResults, 
            SortDirection sortDirection)
    {
        return getDAO().getPreferencesIterator(category, firstResult, maxResults, sortDirection);
    }
    
    private UserPreferences getUserPreferences(UserPreferencesEntity entity)
    {
        return new UserPreferencesHibernate(entity, keyAndCertStore, 
                encryptor, sessionManager.getSession());        
    }
    
    private UserPreferencesDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new UserPreferencesDAO(session);
    }

    private UserPreferencesDAO getStatelessDAO(StatelessSession statelessSession)
    {
        SessionAdapter session = SessionAdapterFactory.create(statelessSession);
        
        return new UserPreferencesDAO(session);
    }
}
