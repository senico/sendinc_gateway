/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.common.hibernate.CommitOnCloseIterator;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;

import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class UserPreferencesCategoryManagerHibernate implements UserPreferencesCategoryManager
{        
    /*
     * The store containing private keys and X509Certificates.
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * The system encryptor
     */
    private final Encryptor encryptor;
        
    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
   
    public UserPreferencesCategoryManagerHibernate(KeyAndCertStore keyAndCertStore,	
    		Encryptor encryptor, SessionManager sessionManager)
    {
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(encryptor, "encryptor");
    	Check.notNull(sessionManager, "sessionManager");
                
        this.keyAndCertStore = keyAndCertStore;
        this.encryptor = encryptor;
        this.sessionManager = sessionManager;
    }
        
    @Override
    public UserPreferencesManager getUserPreferencesManager(String category)
    {
        UserPreferencesManager manager = new UserPreferencesManagerHibernate(category, keyAndCertStore,
        		encryptor, sessionManager);

        return manager;
    }
    
    @Override
    public CloseableIterator<String> getCategoryIterator()
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        CloseableIterator<String> iterator = getStatelessDAO(statelessSession).getCategoryIterator();
        
        CloseableIterator<String> commitOnCloseIterator = new CommitOnCloseIterator<String>(iterator, 
                SessionAdapterFactory.create(statelessSession), tx);
        
        return commitOnCloseIterator;        
    }
        
    private UserPreferencesDAO getStatelessDAO(StatelessSession statelessSession)
    {
        SessionAdapter session = SessionAdapterFactory.create(statelessSession);
        
        return new UserPreferencesDAO(session);
    }    
}
