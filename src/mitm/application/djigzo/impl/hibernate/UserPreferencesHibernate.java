/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.impl.NamedCertificateImpl;
import mitm.application.djigzo.impl.UserPropertiesImpl;
import mitm.common.properties.DelegatedHierarchicalProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.hibernate.HierarchicalPropertiesPropertyEntity;
import mitm.common.properties.hibernate.NamedBlobEntity;
import mitm.common.properties.hibernate.PropertyEntity;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;
import mitm.common.util.LoopDetector;
import mitm.common.util.ObservableRuntimeException;
import mitm.common.util.ObservableSet;
import mitm.common.util.SetBridge;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UserPreferencesHibernate is not thread safe. Life time should not be any longer than the lifetime of the
 * associated session because the instance cannot be used any longer after the session has been closed.
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesHibernate implements UserPreferences
{
    private final static Logger logger = LoggerFactory.getLogger(UserPreferencesHibernate.class) ;

    /*
     * Because UserPreferencesHibernate can contain references to other UserPreferencesHibernate there is
     * always the chance of a loop. The LoopDetector will be used to detect such loops.
     */
    private final LoopDetector loopDetector;

    /* 
     * Tokens used by the loop detector. Exact value does not matter as long as they are unique.
     */
    private final static String GET_PROPERTIES_TOKEN = "1";
    private final static String GET_INH_CERTS_TOKEN  = "2";
    private final static String GET_INH_KEY_AND_CERTS_TOKEN = "3";
    private final static String GET_INH_NAMED_CERTS_TOKEN  = "4";
    
    private final UserPreferencesEntity userPreferencesEntity;
    private final KeyAndCertStore keyAndCertStore;
    private final Encryptor encryptor;
    private final Session session;
    
    /**
     * Create a UserPreferences instance with a database back-end.
     * 
     * Note: the actual implementation of KeyAndCertStore should return {@link X509CertStoreEntryHibernate} instances ie. for
     * UserPreferencesHibernate we will rely on a specific implementation of the X509CertStoreExt. We can however not easily
     * check in advance if the X509CertStoreExt is a specific implementation because the KeyAndCertStore can use delegation
     * to delegate the X509CertStoreExt interface to another X509CertStoreExt. See getCertStoreEntry for reasons why we 
     * need a specific implementation.
     */
    public UserPreferencesHibernate(UserPreferencesEntity userPreferencesEntity, KeyAndCertStore keyAndCertStore, 
    		Encryptor encryptor, Session session)
    {
        this(userPreferencesEntity, keyAndCertStore, encryptor, session, new LoopDetector());
    }

    private UserPreferencesHibernate(UserPreferencesEntity userPreferencesEntity, KeyAndCertStore keyAndCertStore, 
    		Encryptor encryptor, Session session, LoopDetector loopDetector)
    {
    	Check.notNull(userPreferencesEntity, "userPreferencesEntity");
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(encryptor, "encryptor");
    	Check.notNull(session, "session");
    	
        this.userPreferencesEntity = userPreferencesEntity;
        this.keyAndCertStore = keyAndCertStore;
        this.encryptor = encryptor;
        this.session = session;
        this.loopDetector = loopDetector;
    }
    
    @Override
    public String getName() {
        return userPreferencesEntity.getName();
    }
    
    @Override
    public String getCategory() {
        return userPreferencesEntity.getCategory();
    }

    protected UserPreferencesEntity getUserPreferencesEntity() {
        return userPreferencesEntity;
    }
    
    @Override
    public Set<X509Certificate> getCertificates()
    {
        Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        Set<X509CertStoreEntryHibernate> storeEntries = userPreferencesEntity.getCertificates();
        
        for (X509CertStoreEntryHibernate entry : storeEntries) {
            certificates.add(entry.getCertificate());
        }
        
        /* create a Set instance that wraps-up the certificates set so we can intercept changes 
         * (like add and remove) made to the certificates set. We need this to reflect these 
         * changes to the database
         */
        ObservableSet<X509Certificate> certificateObserver = new ObservableSet<X509Certificate>(certificates);
        
        certificateObserver.setAddEvent(new ObservableSet.Event<X509Certificate>()
        {
            @Override
            public boolean event(X509Certificate certificate) {
                return onAddCertificate(certificate);
            }
        });

        certificateObserver.setRemoveEvent(new ObservableSet.Event<Object>()
        {
            @Override
            public boolean event(Object certificate) {
                return onRemoveCertificate(certificate);
            }
        });
        
        return certificateObserver;
    }
    
    @Override
    public Set<NamedCertificate> getNamedCertificates()
    {
        Set<NamedCertificate> namedCertificates = new HashSet<NamedCertificate>();
        
        Set<NamedCertificateHibernate> entries = userPreferencesEntity.getNamedCertificates();
        
        for (NamedCertificateHibernate entry : entries)
        {
            namedCertificates.add(new NamedCertificateImpl(entry.getName(), 
                    entry.getCertificateEntry().getCertificate()));
        }
        
        /* create a Set instance that wraps-up the certificates set so we can intercept changes 
         * (like add and remove) made to the certificates set. We need this to reflect these 
         * changes to the database
         */
        ObservableSet<NamedCertificate> certificateObserver = new ObservableSet<NamedCertificate>(namedCertificates);
        
        certificateObserver.setAddEvent(new ObservableSet.Event<NamedCertificate>()
        {
            @Override
            public boolean event(NamedCertificate namedCertificate) {
                return onAddNamedCertificate(namedCertificate);
            }
        });

        certificateObserver.setRemoveEvent(new ObservableSet.Event<Object>()
        {
            @Override
            public boolean event(Object namedCertificate) {
                return onRemoveNamedCertificate(namedCertificate);
            }
        });
        
        return certificateObserver;
    }
    
    @Override
    public Set<X509Certificate> getInheritedCertificates()
    {
        Set<X509Certificate> inheritedCertificates = new HashSet<X509Certificate>();

        /* 
         * because user preferences are hierarchical there is a change of a loop which we need to 
         * detect and handle.
         */
        String token = createToken(GET_INH_CERTS_TOKEN);
        
        if (!loopDetector.hasToken(token)) 
        {
            loopDetector.addToken(token);
            
            try {                
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();
        
                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    inheritedCertificates.addAll(userPreferences.getCertificates());
                    inheritedCertificates.addAll(userPreferences.getInheritedCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected with token: " + token);
        }        
                
        return Collections.unmodifiableSet(inheritedCertificates);
    }
    
    @Override
    public Set<NamedCertificate> getInheritedNamedCertificates()
    {
        Set<NamedCertificate> inheritedNamedCertificates = new HashSet<NamedCertificate>();

        /* 
         * because user preferences are hierarchical there is a change of a loop which we need to 
         * detect and handle.
         */
        String token = createToken(GET_INH_NAMED_CERTS_TOKEN);
        
        if (!loopDetector.hasToken(token)) 
        {
            loopDetector.addToken(token);
            
            try {                
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();
        
                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    inheritedNamedCertificates.addAll(userPreferences.getNamedCertificates());
                    inheritedNamedCertificates.addAll(userPreferences.getInheritedNamedCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected with token: " + token);
        }        
                
        return Collections.unmodifiableSet(inheritedNamedCertificates);
    }
    
    /*
     * Returns a Set of all the inherited UserPreferences.
     */
    private Set<UserPreferences> getInheritedUserPreferencesInternal()
    {
        Set<UserPreferences> inheritedUserPreferences = new LinkedHashSet<UserPreferences>();

        // get a set of join table entries that associates this UserPreferences with the inherited UserPreferences.
        Set<InheritedUserPreferences> joinEntities = userPreferencesEntity.getInheritedPreferences();
        
        for (InheritedUserPreferences joinEntity : joinEntities)
        {
            // get the inherited entity
            UserPreferencesEntity inheritedUserPreferencesEntity = joinEntity.getInheritedPreferences();
            
            if (inheritedUserPreferencesEntity != null) 
            {
                /*
                 * because we create a new instance of UserPreferencesHibernate we need to share the
                 * loop detector.
                 */  
                inheritedUserPreferences.add(new UserPreferencesHibernate(inheritedUserPreferencesEntity, 
                        keyAndCertStore, encryptor, session, loopDetector));
            }
        }

        return inheritedUserPreferences;
    }
    
    @Override
    public Set<UserPreferences> getInheritedUserPreferences() 
    {
        Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

        /*
         * Create a Set that wraps-up inheritedUserPreferences so we can keep track of changes to the Set and
         * reflect these changes in the database.
         */
        ObservableSet<UserPreferences> userPreferencesObserver = new ObservableSet<UserPreferences>(inheritedUserPreferences);
        
        userPreferencesObserver.setAddEvent(new ObservableSet.Event<UserPreferences>()
        {
            @Override
            public boolean event(UserPreferences userPreferences) {
                return onAddUserPreferences(userPreferences);
            }
        });

        userPreferencesObserver.setRemoveEvent(new ObservableSet.Event<Object>()
        {
            @Override
            public boolean event(Object userPreferences) {
                return onRemoveUserPreferences(userPreferences);
            }
        });
        
        return userPreferencesObserver;
    }
    
    @Override
    public UserProperties getProperties()
    {
        HierarchicalProperties parent = null;

        /* 
         * because user preferences are hierarchical there is a change of a loop which we need to 
         * detect and handle.
         */
        String token = createToken(GET_PROPERTIES_TOKEN);
        
        if (!loopDetector.hasToken(token)) 
        {
            loopDetector.addToken(token);
            
            try {
                /*
                 * we need to build a tree of properties each inheriting from the previous. 
                 */
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();
        
                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    if (userPreferences != null) 
                    {
                        HierarchicalProperties prefs = userPreferences.getProperties();
                        
                        if (prefs != null) {
                            parent = new DelegatedHierarchicalProperties(prefs, parent);
                        }
                    }
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected with token: " + token);
        }        
        
        PropertyEntity thisPropertyEntity = userPreferencesEntity.getProperyEntity();
        
        parent = new HierarchicalPropertiesPropertyEntity(thisPropertyEntity, parent, encryptor);
        
        return new UserPropertiesImpl(parent);
    }
    
    @Override
    public Set<NamedBlob> getNamedBlobs() {
        return new SetBridge<NamedBlob, NamedBlobEntity>(userPreferencesEntity.getNamedBlobs(), NamedBlobEntity.class);
    }
    
    @Override
    public KeyAndCertificate getKeyAndCertificate() 
    throws CertStoreException, KeyStoreException 
    {
        KeyAndCertificate keyAndCertificate = null;

        X509CertStoreEntryHibernate certStoreEntry = userPreferencesEntity.getKeyAndCertificateEntry();
        
        if (certStoreEntry != null)
        {
            keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);
        }
        
        return keyAndCertificate;
    }

    @Override
    public void setKeyAndCertificate(KeyAndCertificate keyAndCertificate) 
    throws CertStoreException, KeyStoreException
    {
    	if (keyAndCertificate != null)
    	{
    		/*
    		 * Note: We assume that the keyAndCertificate has already been added to the KeyAndCertStore.
    		 * We previously always added the keyAndCertificate to the KeyAndCertStore but this was problematic
    		 * when the PKCS11 provider is used since it will remove the chain from the KeyStore when setting
    		 * a key for an existing alias. 
    		 */
	        X509CertStoreEntryHibernate certStoreEntry = getCertStoreEntry(keyAndCertificate.getCertificate());
	        
	        if (certStoreEntry != null) {
	            userPreferencesEntity.setKeyAndCertificate(certStoreEntry);
	        }
	        else {
	            // we have just added it but it can be removed in another session
	            logger.warn("certStoreEntry not found.");
	        }
    	}
    	else {
            userPreferencesEntity.setKeyAndCertificate(null);
    	}
    }

    @Override
    public Set<KeyAndCertificate> getInheritedKeyAndCertificates()
    throws CertStoreException, KeyStoreException
    {
        Set<KeyAndCertificate> inheritedKeyAndCertificates = new HashSet<KeyAndCertificate>();

        /* 
         * because user preferences are hierarchical there is a change of a loop which we need to 
         * detect and handle.
         */
        String token = createToken(GET_INH_KEY_AND_CERTS_TOKEN);
        
        if (!loopDetector.hasToken(token)) 
        {
            loopDetector.addToken(token);
            
            try {                
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();
        
                for (UserPreferences userPreferences : inheritedUserPreferences) 
                {
                    if (userPreferences.getKeyAndCertificate() != null) {
                        inheritedKeyAndCertificates.add(userPreferences.getKeyAndCertificate());
                    }
        
                    inheritedKeyAndCertificates.addAll(userPreferences.getInheritedKeyAndCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected with token: " + token);
        }        
        
        return Collections.unmodifiableSet(inheritedKeyAndCertificates);
    }
    
    /*
     * Returns the X509CertStoreEntryHibernate with the given certificate. Returns null if there
     * is no entry with the certificate.
     */
    private X509CertStoreEntryHibernate getCertStoreEntry(X509Certificate certificate) 
    throws CertStoreException 
    {
        X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(certificate);
        
        /*
         * This is a kind of 'hack' because we are now relying on a specific implementation of
         * the returned X509CertStoreEntry. The reason for this is that I would like to shield
         * Hibernate as much as possible from the API but I would like still be able to use
         * the database build-in referential checks. I therefore must use a Hibernate entity
         * but I do not want to make the API to depend on Hibernate.
         */
        if (certStoreEntry != null && !(certStoreEntry instanceof X509CertStoreEntryHibernate)) {
            throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntryHibernate.");
        }
        
        return (X509CertStoreEntryHibernate) certStoreEntry;
    }
    
    /*
     * Gets called when a certificate is added to the collection of certificates (see getCertificates()).
     */
    private boolean onAddCertificate(X509Certificate certificate)
    throws ObservableRuntimeException
    {
        X509CertStoreEntryHibernate entry;
        
        try {
            /* 
             * we try to get the entry from the X509CertStore.
             */
            entry = getCertStoreEntry(certificate);
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }
        
        if (entry == null) 
        {
            logger.warn("Certificate entry not found. Adding it the the certStore.");
            
            X509CertStoreEntry newEntry;
            
            try {
                newEntry = keyAndCertStore.addCertificate(certificate);
            }
            catch (CertStoreException e) {
                throw new ObservableRuntimeException(e);
            }

            /*
             * see getCertStoreEntry why we need on a specific implementation.
             */ 
            if (!(newEntry instanceof X509CertStoreEntryHibernate)) {
                throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntryHibernate.");
            }
            
            entry = (X509CertStoreEntryHibernate) newEntry;
        }
        
        userPreferencesEntity.getCertificates().add(entry);
        
        /*
         * We will always return true. It can happen that the certificate was already added in a different thread. 
         * We therefore would like to always add it to the source of the ObservableCollection.
         */
        return true;
    }

    /*
     * Gets called when a certificate is removed from the collection of certificates (see getCertificates()).
     */
    private boolean onRemoveCertificate(Object object)
    throws ObservableRuntimeException
    {
        if (!(object instanceof X509Certificate)) {
            throw new IllegalArgumentException("Object must be a X509Certificate.");
        }
        
        X509Certificate certificate = (X509Certificate) object;
        
        X509CertStoreEntryHibernate entry;
        
        try {
            entry = getCertStoreEntry(certificate);
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }
        
        if (entry != null) {
            userPreferencesEntity.getCertificates().remove(entry);
        }
        else {
            /*
             * the entry was probably removed.
             */   
            logger.warn("certStore entry not found.");            
        }
        
        /*
         * We will always return true even when it's not removed. It can happen that the certificate
         * is already removed in a different thread. We therefore would like to always remove it from to the
         * source of the ObservableCollection.
         */
        return true;
    }
        
    /*
     * Gets called when a namedCertificate is added to the collection of namedCertificates
     */
    private boolean onAddNamedCertificate(NamedCertificate namedCertificate)
    throws ObservableRuntimeException
    {
        Check.notNull(namedCertificate.getCertificate(), "certificate");
        Check.notNull(namedCertificate.getName(), "name");
        
        X509CertStoreEntryHibernate entry;
        
        try {
            /* 
             * we try to get the entry from the X509CertStore.
             */
            entry = getCertStoreEntry(namedCertificate.getCertificate());
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }
        
        if (entry == null) 
        {
            logger.warn("Certificate entry not found. Adding it the the certStore.");
            
            X509CertStoreEntry newEntry;
            
            try {
                newEntry = keyAndCertStore.addCertificate(namedCertificate.getCertificate());
            }
            catch (CertStoreException e) {
                throw new ObservableRuntimeException(e);
            }

            /*
             * see getCertStoreEntry why we need on a specific implementation.
             */ 
            if (!(newEntry instanceof X509CertStoreEntryHibernate)) {
                throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntryHibernate.");
            }
            
            entry = (X509CertStoreEntryHibernate) newEntry;
        }
        
        NamedCertificateHibernate embeddable = new NamedCertificateHibernate(
                namedCertificate.getName(), entry);
        
        userPreferencesEntity.getNamedCertificates().add(embeddable);
        
        /*
         * We will always return true. It can happen that the certificate was already added in a different thread. 
         * We therefore would like to always add it to the source of the ObservableCollection.
         */
        return true;
    }
    
    /*
     * Gets called when a certificate is removed from the collection of certificates (see getCertificates()).
     */
    private boolean onRemoveNamedCertificate(Object object)
    throws ObservableRuntimeException
    {
        if (!(object instanceof NamedCertificate)) {
            throw new IllegalArgumentException("Object must be a NamedCertificate.");
        }
        
        NamedCertificate namedCertificate = (NamedCertificate) object;
        
        Check.notNull(namedCertificate.getCertificate(), "certificate");
        Check.notNull(namedCertificate.getName(), "name");
        
        X509CertStoreEntryHibernate entry;
        
        try {
            entry = getCertStoreEntry(namedCertificate.getCertificate());
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }
        
        if (entry != null)
        {
            NamedCertificateHibernate embeddable = new NamedCertificateHibernate(
                    namedCertificate.getName(), entry);
            
            userPreferencesEntity.getNamedCertificates().remove(embeddable);
        }
        else {
            /*
             * the entry was probably removed.
             */   
            logger.warn("certStore entry not found.");            
        }
        /*
         * We will always return true even when it's not removed. It can happen that the certificate
         * is already removed in a different thread. We therefore would like to always remove it from to the
         * source of the ObservableCollection.
         */
        return true;
    }
    
    /*
     * Gets called when a new UserPreferences object is added to the collection of UserPreferences.
     * This implementation only supports UserPreferences that are implemented by a UserPreferencesHibernate.
     * A IllegalArgumentException is thrown when the UserPreferences is not a UserPreferencesHibernate.
     */
    private boolean onAddUserPreferences(UserPreferences newUserPreferences)
    throws ObservableRuntimeException
    {
    	Check.notNull(newUserPreferences, "newUserPreferences");
    	
        if (!(newUserPreferences instanceof UserPreferencesHibernate)) {
            throw new IllegalArgumentException("newUserPreferences is not a UserPreferencesHibernate");
        }
        
        UserPreferencesHibernate newUserPreferencesHibernate = (UserPreferencesHibernate) newUserPreferences;
        
        /* 
         * Get the associated Hibernate entity of the UserPreferences we need to add. 
         */
        UserPreferencesEntity newUserPreferencesEntity = newUserPreferencesHibernate.userPreferencesEntity;

        long newIndex = userPreferencesEntity.getInheritedPreferences().size() + 1;
        
        InheritedUserPreferences newInheritedUserPreferences = new InheritedUserPreferences(
                  newUserPreferencesEntity, newIndex);
        
        boolean added = false;
        
        if (!userPreferencesEntity.getInheritedPreferences().contains(newInheritedUserPreferences))
        {
            userPreferencesEntity.getInheritedPreferences().add(newInheritedUserPreferences);
            
            added = true;
        }
        
        return added;
    }

    /*
     * Gets called when a new UserPreferences object is removed from the collection of UserPreferences.
     * This implementation only supports UserPreferences that are implemented by a UserPreferencesHibernate.
     * A IllegalArgumentException is thrown when the UserPreferences is not a UserPreferencesHibernate.
     */
    private boolean onRemoveUserPreferences(Object object)
    throws ObservableRuntimeException
    {
        if (!(object instanceof UserPreferencesHibernate)) {
            throw new IllegalArgumentException("object is not a UserPreferencesHibernate");
        }
        
        UserPreferencesHibernate toRemoveUserPreferencesHibernate = (UserPreferencesHibernate) object;
        
        /* get the associated Hibernate entity */
        UserPreferencesEntity toRemoveUserPreferencesEntity = toRemoveUserPreferencesHibernate.userPreferencesEntity;
        
        InheritedUserPreferences inheritedUserPreferences = new InheritedUserPreferences(
                toRemoveUserPreferencesEntity);
        
        boolean removed = userPreferencesEntity.getInheritedPreferences().remove(inheritedUserPreferences);
        
        if (!removed) {
            logger.warn("UserPreferences not removed.");
        }
        
        return removed;
    }

    private String getBusinessKey() {
        return getCategory() + ":" + getName();
    }    
    
    /*
     * Creates a token that is a unique identifier for a resource used by this class.
     * This token is used for loop detection. We cannot just compare memory locations 
     * because a lot of delegation classes are used.
     */
    private String createToken(String prefix) {
        return prefix + getBusinessKey();
    }
    
    @Override
    public String toString() {
        return getBusinessKey();
    }

    /*
     * a UserPreferencesHibernate is identified by it's unique name so equals compares the name.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferences)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        UserPreferences rhs = (UserPreferences) obj;
        
        return new EqualsBuilder()
            .append(getCategory(), rhs.getCategory())
            .append(getName(), rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getCategory())
            .append(getName())
            .toHashCode();    
    }    
}
