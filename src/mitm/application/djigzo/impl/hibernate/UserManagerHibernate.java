/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserManager;
import mitm.common.hibernate.CommitOnCloseIterator;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.smime.selector.CertificateSelector;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;

import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManagerHibernate implements UserManager
{
    private final static Logger logger = LoggerFactory.getLogger(UserManagerHibernate.class);
    

    private final PKISecurityServices pKISecurityServices;
    
    /*
     * Selector that selects encryption certificates for the user.
     */
    private final CertificateSelector encryptionCertificateSelector;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
    public UserManagerHibernate(PKISecurityServices pKISecurityServices, CertificateSelector encryptionCertificateSelector,
    		SessionManager sessionManager)
    {
    	Check.notNull(pKISecurityServices, "pKISecurityServices");
    	Check.notNull(encryptionCertificateSelector, "encryptionCertificateSelector");
    	Check.notNull(sessionManager, "sessionManager");
        
        this.pKISecurityServices = pKISecurityServices;
        this.encryptionCertificateSelector = encryptionCertificateSelector;
        this.sessionManager = sessionManager;
    }
    
    @Override
    public User addUser(String email, AddMode addMode) 
    throws AddressException
    {
        /*
         * We only accept valid email addresses. Email addresses need to be normalized as well. The caller is responsible to 
         * normalize and validate the email address so this check is more or less a sanity check.
         */
        String validatedUser = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (validatedUser == null) {
            throw new AddressException(email + " is not a valid email address.");
        }
        
        UserEntity entity = getDAO().addUser(validatedUser, addMode == AddMode.PERSISTENT);
        
        UserHibernate user = new UserHibernate(entity, pKISecurityServices, encryptionCertificateSelector, 
        		sessionManager.getSession());
        
        return user;        
    }
    
    @Override
    public boolean deleteUser(User user)
    {
        boolean deleted = false;

        Check.notNull(user, "user");
        
        if (user instanceof UserHibernate) 
        {
            UserHibernate userHibernate = (UserHibernate) user;
            
            getDAO().delete(userHibernate.getUserEntity());
            
            deleted = true;
        }
        else {
            logger.warn("User cannot be deleted because user is not a UserHibernate.");
        }
        
        return deleted;
    }
    
    @Override
    public User getUser(String email)
    throws AddressException    
    {
        /*
         * We only accept valid email addresses. Email addresses need to be normalized as well. The caller is responsible to 
         * normalize and validate the email address so this check is more or less a sanity check.
         */
        String validatedUser = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (validatedUser == null) {
            throw new AddressException(email + " is not a valid email address.");
        }
        
        User user = null;
        
        UserEntity entity = getDAO().getUser(validatedUser);
        
        if (entity != null)
        {
            user = new UserHibernate(entity, pKISecurityServices, encryptionCertificateSelector,
            		sessionManager.getSession());
        }
        
        return user;        
    }
        
    @Override
    public int getUserCount() {
        return getDAO().getUserCount();
    }
        
    @Override
    public boolean isPersistent(User user) 
    {
        boolean persisted = false;

        Check.notNull(user, "user");
        
        if (user instanceof UserHibernate) 
        {
            UserHibernate userHibernate = (UserHibernate) user;
            
            persisted = (userHibernate.getUserEntity().getId() != null);
        }
        else {
            logger.warn("User cannot be persisted because user is not a UserHibernate.");
        }
        
        return persisted;
    }
    
    @Override
    public void makePersistent(User user) 
    {
    	Check.notNull(user, "user");
        
        if (user instanceof UserHibernate) 
        {
            UserHibernate userHibernate = (UserHibernate) user;
            
            getDAO().makePersistent(userHibernate.getUserEntity());
        }
        else {
            logger.warn("User cannot be persisted because user is not a UserHibernate.");
        }
    }    
    
    @Override
    public CloseableIterator<String> getEmailIterator()
    {
        return getEmailIterator(null, null, SortDirection.ASC);
    }

    @Override
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        CloseableIterator<String> iterator = getStatelessDAO(statelessSession).getEmailIterator(firstResult, 
        		maxResults, sortDirection);
        
        CloseableIterator<String> commitOnCloseIterator = new CommitOnCloseIterator<String>(iterator, 
                SessionAdapterFactory.create(statelessSession), tx);
        
        return commitOnCloseIterator;        
    }
    
    @Override
    public CloseableIterator<String> searchEmail(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
	{
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        CloseableIterator<String> iterator = getStatelessDAO(statelessSession).searchEmail(search, 
        		firstResult, maxResults, sortDirection);
        
        CloseableIterator<String> commitOnCloseIterator = new CommitOnCloseIterator<String>(iterator, 
                SessionAdapterFactory.create(statelessSession), tx);
        
        return commitOnCloseIterator;        
	}
    
    @Override
    public int getSearchEmailCount(String search) 
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        return getStatelessDAO(statelessSession).getSearchEmailCount(search);
    }
    
    private UserDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new UserDAO(session);
    }

    private UserDAO getStatelessDAO(StatelessSession statelessSession)
    {
        SessionAdapter session = SessionAdapterFactory.create(statelessSession);
        
        return new UserDAO(session);
    }    
}
