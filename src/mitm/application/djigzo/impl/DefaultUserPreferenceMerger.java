/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.UserPreferenceMerger;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Merges the preferences in order of specificity based on category ie. global, domain, user
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultUserPreferenceMerger implements UserPreferenceMerger
{
    private final static Logger logger = LoggerFactory.getLogger(DefaultUserPreferenceMerger.class);
    
    /**
     * Merges the newPreferences with the targetPreferences. It is assumed that if a category is used for a 
     * UserPreferences in newPreferences that all UserPreferences for that category are available in
     * newPreferences and that they are ordered from least specific to most specific. All items in 
     * targetPreferences with the same category in newPreferences will be removed and replaced by the 
     * items in newPreferences.
     */
    @Override
    public Set<UserPreferences> merge(Set<UserPreferences> targetPreferences, Set<UserPreferences> newPreferences)
    {
        Set<UserPreferences> merged = null; 
            
        /*
         * This procedure requires some explanation. UserPreferences are ordered from least specific (for
         * example global) to more specific (for example user preference). newPreferences can contain
         * multiple UserPreferences objects. Those UserPreferences objects can have the same category or
         * there can be object from different categories. Within one category the objects need to be 
         * ordered because there is, currently, no way for this method to know how to oder objects 
         * within one category. What this method does is it removes all items from the target which 
         * have a matching category with newPreferences and add these newPreferences to the 
         * new targetPreferences. The targetPreferences is then ordered according to the ordering
         * of the categories. We must take care that the initial ordering of the UserPreferences
         * within the same category is not changed. We will use a wrapper class to help us getting
         * the correct order.
         */
        
        Set<String> categoriesToReplace = new HashSet<String>(); 
        
        /* first find out if new preferences are really new */
        for (UserPreferences userPreferences : newPreferences)
        {
            if (!targetPreferences.contains(userPreferences)) 
            {
                categoriesToReplace.add(userPreferences.getCategory());
            }
        }
        
        if (categoriesToReplace.size() > 0) 
        {
            if (logger.isDebugEnabled()) {
                logger.debug("New preferences for categories: " + categoriesToReplace);
            }
            
            /* 
             * create a copy of targetPreferences but with the categoriesToReplace removed. We will use
             * a list to keep the order intact. 
             */
             List<UserPreferencesWrapper> sortingPreferences = new LinkedList<UserPreferencesWrapper>(); 
                 
             for (UserPreferences userPreferences : targetPreferences)
             {
                 if (!categoriesToReplace.contains(userPreferences.getCategory()))
                 {
                     int index = sortingPreferences.size();
                     
                     UserPreferencesWrapper wrapper = new UserPreferencesWrapper(index, userPreferences);
                     
                     sortingPreferences.add(wrapper);
                 }
             }
             
             /* now add the newPreferences */
             for (UserPreferences userPreferences : newPreferences)
             {
                 int index = sortingPreferences.size();
                 
                 UserPreferencesWrapper wrapper = new UserPreferencesWrapper(index, userPreferences);
                 
                 sortingPreferences.add(wrapper);
             }
             
             /* sort the list and replace the targetPreferences with the new targetPreferences */
             Collections.sort(sortingPreferences);
             
             merged = new LinkedHashSet<UserPreferences>();
             
             for (UserPreferencesWrapper wrapper : sortingPreferences)
             {
                 merged.add(wrapper.getUserPreferences());
             }
        }
        
        return merged;
    }
    
    /*
     * Wrapper class used for sorting UserPreferences objects.
     */
    private static class UserPreferencesWrapper implements Comparable<UserPreferencesWrapper>
    {
        private final int index;
        private final UserPreferences userPreferences;
        
        public UserPreferencesWrapper(int index, UserPreferences userPreferences)
        {
            this.index = index;
            this.userPreferences = userPreferences;
        }
        
        public UserPreferences getUserPreferences() {
            return userPreferences;
        }
        
        public int getIndex() {
            return index;
        }
        
        @Override
        public int compareTo(UserPreferencesWrapper that) 
        {
            /* 
             * first we will compare the category. If the category is the same we will 
             * compare the index 
             */
            
            UserPreferencesCategory thisCategory = UserPreferencesCategory.fromName(this.getUserPreferences().getCategory());
            UserPreferencesCategory thatCategory = UserPreferencesCategory.fromName(that.getUserPreferences().getCategory());
            
            int diff = thisCategory.getOrder() - thatCategory.getOrder();
            
            if (diff != 0) {
                return diff;
            }
            
            /* categories are equal. compare index. */
            return this.getIndex() - that.getIndex();
        }
    }
}
