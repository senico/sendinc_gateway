/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.security.cert.X509Certificate;

import mitm.application.djigzo.NamedCertificate;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class NamedCertificateImpl implements NamedCertificate
{
    private final String name;
    
    private final X509Certificate certificate;
    
    public NamedCertificateImpl(String name, X509Certificate certificate)
    {
        Check.notNull(name, "name");
        Check.notNull(certificate, "certificate");
        
        this.name = name;
        this.certificate = certificate;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public X509Certificate getCertificate() {
        return certificate;
    }
    
    @Override
    public String toString() {
        return "Certificate: " + certificate + "; name: " + name;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof NamedCertificate)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        NamedCertificate rhs = (NamedCertificate) obj;
        
        return new EqualsBuilder()
            .append(certificate, rhs.getCertificate())
            .append(name, rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(certificate)
            .append(name)
            .toHashCode();    
    }        
}
