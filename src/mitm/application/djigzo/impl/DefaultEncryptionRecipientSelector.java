/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.EncryptionRecipientSelector;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.certificate.validator.CertificateValidator;
import mitm.common.security.certificate.validator.IsValidForSMIMEEncryption;
import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidator;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default selector used for the selection of users that allow encryption.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultEncryptionRecipientSelector implements EncryptionRecipientSelector
{
    private final static Logger logger = LoggerFactory.getLogger(DefaultEncryptionRecipientSelector.class);
    
    private final PKISecurityServices pKISecurityServices;
    
    /*
     * If true, certificates that are not auto selected (ie certificates that are explicitly selected or
     * inherited) are checked is valid.
     */
    private final boolean checkValidity;
    
    public DefaultEncryptionRecipientSelector(PKISecurityServices pKISecurityServices, boolean checkValidity)
    {
    	Check.notNull(pKISecurityServices, "pKISecurityServices");
    	
        this.pKISecurityServices = pKISecurityServices;
        this.checkValidity = checkValidity;
    }
    
    /**
     * Returns a set of users which have at least one certificate (either automatically selected or inherited). 
     * The certificates found for all selected recipients will be added to the provided certificates set.
     */
    @Override
    public Set<User> select(Collection<User> users, Collection<X509Certificate> certificates) 
    throws HierarchicalPropertiesException
    {
        Set<User> selected = new HashSet<User>();
        
        if (users != null) 
        {
            for (User user : users)
            {
                UserPreferences userPreferences = user.getUserPreferences();
                
                Set<X509Certificate> userCertificates = new HashSet<X509Certificate>();

                addTrustedCertificates(userPreferences.getCertificates(), userCertificates);
                addTrustedCertificates(userPreferences.getInheritedCertificates(), userCertificates);

                /* 
                 * AutoSelectEncryptionCertificates are already checked for trust, validity etc. 
                 */
                userCertificates.addAll(user.getAutoSelectEncryptionCertificates());

                if (userCertificates.size() > 0)
                {
                	/* the user supports encryption and has public certificates so should be selected */
                	selected.add(user);

                	if (certificates != null) {
                		certificates.addAll(userCertificates);
                	}
                }
            }
        }
        
        return selected;
    }
    
    private void addTrustedCertificates(Set<X509Certificate> source, Set<X509Certificate> target)
    {
        PKITrustCheckCertificateValidator certificateValidator = pKISecurityServices.
        		getPKITrustCheckCertificateValidatorFactory().createValidator(null); 
        
        for (X509Certificate certificate : source)
        {
            boolean accept = true;
            
            if (checkValidity) 
            {
            	try {
	                accept = certificateValidator.isValid(certificate);
	                
	                if (accept)
	                {
	                    CertificateValidator validator = new IsValidForSMIMEEncryption();
	                    
	                    if (!validator.isValid(certificate))
	                    {
	                        logger.debug("Certificate is not valid for S/MIME encryption.");
	                        
	                        accept = false;
	                    }
	                }
            	}
            	catch(CertificateException e) {
            		/*
            		 * In case of a CertificateException we won't add the certificate
            		 */
            		logger.error("Error while validating the certificate.", e);
            		
            		accept =  false;
            	}
            }
            
            if (accept) {
                target.add(certificate);
            }
            else {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Certificate not accepted. Reason: " + certificateValidator.getFailureMessage() +
                            ". Certificate: "+ certificate);
                }
            }
        }
    }
}
