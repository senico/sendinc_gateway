/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.util.Date;

import mitm.application.djigzo.DKIMProperties;
import mitm.application.djigzo.DLPProperties;
import mitm.application.djigzo.EncryptMode;
import mitm.application.djigzo.PortalProperties;
import mitm.application.djigzo.PropertyRegistry;
import mitm.application.djigzo.TemplateProperties;
import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.relay.RelayBounceMode;
import mitm.common.properties.DefaultPropertyProvider;
import mitm.common.properties.DelegatedHierarchicalProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.security.smime.SMIMEEncryptionAlgorithm;
import mitm.common.security.smime.SMIMESigningAlgorithm;
import mitm.common.util.MiscStringUtils;
import mitm.common.util.PhoneNumberUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UserProperties implementation that reads/writes the user property values from/to a  HierarchicalProperties instance.
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPropertiesImpl extends DelegatedHierarchicalProperties implements UserProperties
{
    private final static Logger logger = LoggerFactory.getLogger(UserPropertiesImpl.class);
    
    /* 
     * The base name of the UserProperties. The actual name of a property will be based upon
     * the base name and the name of the property.
     */
    private final static String BASE_NAME = "user.";
        
    /*
     * The default relative URL for the OTP generator 
     */
    private final static String DEFAULT_RELATIVE_OTP_URL = "secure/otp";
    
    /*
     * The default relative URL for the PDF reply 
     */
    private final static String PDF_DEFAULT_RELATIVE_REPLY_URL = "pdf/reply";
    
    /*
     * The templates for the user
     */
    private final TemplateProperties templateProperties;
    
    /*
     * The data leak prevention properties for the user
     */
    private final DLPProperties dlpProperties;
    
    /*
     * The domain key properties for the user
     */
    private final DKIMProperties dkimProperties;
    
    /*
     * The portal properties for the user
     */
    private final PortalProperties portalProperties;
    
    /*
     * All the available providers of default values
     */
    private final DefaultPropertyProvider[] defaultProviders;
    
    public UserPropertiesImpl(HierarchicalProperties properties)
    {
        super(properties, null);
        
        this.portalProperties = new PortalPropertiesImpl(this);
        this.templateProperties = new TemplatePropertiesImpl(this);
        this.dlpProperties = new DLPPropertiesImpl(this, portalProperties);
        this.dkimProperties = new DKIMPropertiesImpl(this);
        
        /*
         * Register all DefaultPropertyProvider's that might provide default values for some properties
         */
        defaultProviders = new DefaultPropertyProvider[]{this, portalProperties,  templateProperties, 
                dlpProperties, dkimProperties}; 
    }
        
    /*
     * Returns the full property name which is a combination of base name
     * and property name.
     */
    @Override
    public String getFullPropertyName(String property) {
        return staticGetFullPropertyName(property);
    }

    private static String staticGetFullPropertyName(String property) {
        return BASE_NAME + property;
    }
    
    /*
     * Register all the known properties using a static initializer
     * 
     * Note: when adding new properties always add them to the property registry 
     */
    static 
    {
        PropertyRegistry registry = PropertyRegistry.getInstance();
        
    	registry.registerProperty(staticGetFullPropertyName(LOCALITY), false);
    	registry.registerProperty(staticGetFullPropertyName(ENCRYPT_MODE), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_ENABLED), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_STRICT), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_SKIP_CALENDAR), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_SKIP_SIGNING_CALENDAR), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_ADD_USER), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_PROTECT_HEADERS), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_ENCRYPTION_ALGORITHM), false);
        registry.registerProperty(staticGetFullPropertyName(SMIME_SIGNING_ALGORITHM), false);
        registry.registerProperty(staticGetFullPropertyName(AUTO_SELECT_ENCRYPTION_CERTS), false);
        registry.registerProperty(staticGetFullPropertyName(ALWAYS_USE_FRESHEST_SIGNING_CERT), false);
        registry.registerProperty(staticGetFullPropertyName(ONLY_SIGN_WHEN_ENCRYPT), false);
        registry.registerProperty(staticGetFullPropertyName(ADD_ADDITIONAL_CERTS), false);
        registry.registerProperty(staticGetFullPropertyName(FORCE_SIGNING_HEADER_TRIGGER), false);
        registry.registerProperty(staticGetFullPropertyName(FORCE_SIGNING_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(FORCE_ENCRYPT_HEADER_TRIGGER), false);
        registry.registerProperty(staticGetFullPropertyName(FORCE_ENCRYPT_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(MAX_SIZE_SMIME), false);
        registry.registerProperty(staticGetFullPropertyName(REMOVE_SMIME_SIGNATURE), false);
    	registry.registerProperty(staticGetFullPropertyName(SUBJECT_TRIGGER), false);
    	registry.registerProperty(staticGetFullPropertyName(SUBJECT_TRIGGER_ENABLED), false);
    	registry.registerProperty(staticGetFullPropertyName(SUBJECT_TRIGGER_IS_REGEXPR), false);
    	registry.registerProperty(staticGetFullPropertyName(SUBJECT_TRIGGER_REMOVE_PATTERN), false);
    	registry.registerProperty(staticGetFullPropertyName(SEND_ENCRYPTION_NOTIFICATION), false);
    	registry.registerProperty(staticGetFullPropertyName(SMS_PHONE_NUMBER), false);
    	registry.registerProperty(staticGetFullPropertyName(SMS_SEND_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(SMS_RECEIVE_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(SMS_PHONE_NUMBER_SET_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(PHONE_DEFAULT_COUNTRY_CODE), false);
        registry.registerProperty(staticGetFullPropertyName(PASSWORD), true /* encrypt */);
        registry.registerProperty(staticGetFullPropertyName(PASSWORD_LENGTH), false);
    	registry.registerProperty(staticGetFullPropertyName(PASSWORD_ID), false);
    	registry.registerProperty(staticGetFullPropertyName(DATE_PASSWORD_SET), false);
        registry.registerProperty(staticGetFullPropertyName(PASSWORD_VALIDITY_INTERVAL), false);
        registry.registerProperty(staticGetFullPropertyName(PASSWORDS_SEND_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SERVER_SECRET), true /* encrypt */);
        registry.registerProperty(staticGetFullPropertyName(CLIENT_SECRET), true /* encrypt */);
        registry.registerProperty(staticGetFullPropertyName(BLACKBERRY_RECIPIENT), false);
        registry.registerProperty(staticGetFullPropertyName(STRIP_UNSUPPORTED_FORMATS), false);
        registry.registerProperty(staticGetFullPropertyName(RELAY_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(RELAY_EMAIL), false);
        registry.registerProperty(staticGetFullPropertyName(RELAY_VALIDITY_INTERVAL), false);
        registry.registerProperty(staticGetFullPropertyName(RELAY_BOUNCE_MODE), false);
        registry.registerProperty(staticGetFullPropertyName(PFX_PASSWORD), true /* encrypt */);
        registry.registerProperty(staticGetFullPropertyName(AUTO_REQUEST_CERTIFICATE), false);
        registry.registerProperty(staticGetFullPropertyName(OTP_ENABLED), false);
        registry.registerProperty(staticGetFullPropertyName(OTP_URL), false);
        registry.registerProperty(staticGetFullPropertyName(AUTO_CREATE_CLIENT_SECRET), false);
        registry.registerProperty(staticGetFullPropertyName(ADD_SECURITY_INFO), false);
        registry.registerProperty(staticGetFullPropertyName(SECURITY_INFO_DECRYPTED_TAG), false);
        registry.registerProperty(staticGetFullPropertyName(SECURITY_INFO_SIGNED_VALID_TAG), false);
        registry.registerProperty(staticGetFullPropertyName(SECURITY_INFO_SIGNED_BY_VALID_TAG), false);
        registry.registerProperty(staticGetFullPropertyName(SECURITY_INFO_SIGNED_INVALID_TAG), false);        
        registry.registerProperty(staticGetFullPropertyName(PDF_ENCRYPTION_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_MAX_SIZE), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_REPLY_ALLOWED), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_REPLY_VALIDITY_INTERVAL), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_REPLY_URL), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_REPLY_SENDER), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_ONLY_ENCRYPT_IF_MANDATORY), false);
        registry.registerProperty(staticGetFullPropertyName(PDF_SIGN_EMAIL), false);        
        registry.registerProperty(staticGetFullPropertyName(SUBJECT_FILTER_ENABLED), false);        
        registry.registerProperty(staticGetFullPropertyName(SUBJECT_FILTER_REGEX), false);        
    }
    
    @Override
    public UserLocality getUserLocality() 
    throws HierarchicalPropertiesException 
    {
        String value = this.getProperty(getFullPropertyName(LOCALITY), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(LOCALITY)));
        
        UserLocality locality = UserLocality.fromString(value);
        
        if (locality == null) 
        {
            logger.debug("locality property not found. Using default EXTERNAL.");
            
            locality = UserLocality.EXTERNAL;
        }
        
        return locality;
    }
        
    @Override
    public void setUserLocality(UserLocality locality) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(LOCALITY), ObjectUtils.toString(locality, null),
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(LOCALITY)));
    }
    
    @Override
    public EncryptMode getEncryptMode()
    throws HierarchicalPropertiesException 
    {
        String value = this.getProperty(getFullPropertyName(ENCRYPT_MODE), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(ENCRYPT_MODE)));
        
        EncryptMode mode = EncryptMode.fromString(value);
        
        if (mode == null) 
        {
            logger.debug("EncryptMode property not found. Using default ALLOW.");
            
            mode = EncryptMode.ALLOW;
        }
        
        return mode;
    }
    
    @Override
    public void setEncryptMode(EncryptMode encryptMode) 
    throws HierarchicalPropertiesException 
    {
        setProperty(getFullPropertyName(ENCRYPT_MODE),  encryptMode != null ? encryptMode.toString() : null, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ENCRYPT_MODE)));
    }
    
    @Override
    public void setSMIMEEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_ENABLED),  
        		enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ENABLED)));
    }
    
    @Override
    public boolean isSMIMEEnabled()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_ENABLED), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ENABLED)));
    }

    @Override
    public void setSMIMEStrict(Boolean strict)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_STRICT),  
                strict, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_STRICT)));
    }
    
    @Override
    public boolean isSMIMEStrict()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_STRICT), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_STRICT)));
    }
    
    @Override
    public void setSMIMESkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_SKIP_CALENDAR),
                skip, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SKIP_CALENDAR)));
    }
    
    @Override
    public boolean isSMIMESkipCalendar()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_SKIP_CALENDAR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SKIP_CALENDAR)));
    }

    @Override
    public void setSMIMESkipSigningCalendar(Boolean skip)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_SKIP_SIGNING_CALENDAR),
                skip, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SKIP_SIGNING_CALENDAR)));
    }
    
    @Override
    public boolean isSMIMESkipSigningCalendar()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_SKIP_SIGNING_CALENDAR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SKIP_SIGNING_CALENDAR)));
    }

    @Override
    public void setSMIMEAddUser(Boolean addUser)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_ADD_USER),
                addUser, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ADD_USER)));
    }
    
    @Override
    public boolean isSMIMEAddUser()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_ADD_USER), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ADD_USER)));
    }
    

    @Override
    public void setSMIMEProtectHeaders(Boolean protect)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMIME_PROTECT_HEADERS),
                protect, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_PROTECT_HEADERS)));
    }

    @Override
    public boolean isSMIMEProtectHeaders()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMIME_PROTECT_HEADERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_PROTECT_HEADERS)));
    }
        
    @Override
    public void setSMIMEEncryptionAlgorithm(SMIMEEncryptionAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(SMIME_ENCRYPTION_ALGORITHM), algorithm != null ? algorithm.getName() : null, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ENCRYPTION_ALGORITHM)));
    }
    
    @Override
    public SMIMEEncryptionAlgorithm getSMIMEEncryptionAlgorithm()
    throws HierarchicalPropertiesException
    {
        return SMIMEEncryptionAlgorithm.fromName(getProperty(getFullPropertyName(SMIME_ENCRYPTION_ALGORITHM), 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_ENCRYPTION_ALGORITHM))));        
    }

    @Override
    public void setSMIMESigningAlgorithm(SMIMESigningAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(SMIME_SIGNING_ALGORITHM), algorithm != null ? algorithm.getName() : null, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SIGNING_ALGORITHM)));
    }
    
    @Override
    public SMIMESigningAlgorithm getSMIMESigningAlgorithm()
    throws HierarchicalPropertiesException
    {
        return SMIMESigningAlgorithm.fromName(getProperty(getFullPropertyName(SMIME_SIGNING_ALGORITHM), 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMIME_SIGNING_ALGORITHM))));        
    }
    
    @Override
    public void setAutoSelectEncryptionCerts(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(AUTO_SELECT_ENCRYPTION_CERTS),  
        		enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_SELECT_ENCRYPTION_CERTS)));
    }
    
    @Override
    public boolean isAutoSelectEncryptionCerts()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(AUTO_SELECT_ENCRYPTION_CERTS), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_SELECT_ENCRYPTION_CERTS)));
    }
    
    
    @Override
    public void setAlwaysUseFreshestSigningCert(Boolean useFreshestSigningCert)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALWAYS_USE_FRESHEST_SIGNING_CERT),  
                useFreshestSigningCert, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(
                        ALWAYS_USE_FRESHEST_SIGNING_CERT)));
    }

    @Override
    public boolean isAlwaysUseFreshestSigningCert()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALWAYS_USE_FRESHEST_SIGNING_CERT), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(
                        ALWAYS_USE_FRESHEST_SIGNING_CERT)));
    }   
    
    @Override
    public void setOnlySignWhenEncrypt(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ONLY_SIGN_WHEN_ENCRYPT),  
        		value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ONLY_SIGN_WHEN_ENCRYPT)));
    }
    
    @Override
    public boolean isOnlySignWhenEncrypt()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ONLY_SIGN_WHEN_ENCRYPT), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ONLY_SIGN_WHEN_ENCRYPT)));
    }
    
    @Override
    public void setAddAdditionalCertificates(Boolean add)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ADD_ADDITIONAL_CERTS),  
                add, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ADD_ADDITIONAL_CERTS)));
    }
    
    @Override
    public boolean isAddAdditionalCertificates()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ADD_ADDITIONAL_CERTS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ADD_ADDITIONAL_CERTS)));
    }

    @Override
    public void setForceSigningHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(FORCE_SIGNING_HEADER_TRIGGER), trigger, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(FORCE_SIGNING_HEADER_TRIGGER)));
    }
    
    @Override
    public String getForceSigningHeaderTrigger()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(FORCE_SIGNING_HEADER_TRIGGER), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(FORCE_SIGNING_HEADER_TRIGGER)));
    }
    
    @Override
    public void setForceSigningAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(FORCE_SIGNING_ALLOWED),  
                allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(FORCE_SIGNING_ALLOWED)));
    }
    
    @Override
    public boolean isForceSigningAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(FORCE_SIGNING_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(FORCE_SIGNING_ALLOWED)));
    }
        
    @Override
    public void setForceEncryptHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(FORCE_ENCRYPT_HEADER_TRIGGER), trigger, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(FORCE_ENCRYPT_HEADER_TRIGGER)));
    }
    
    @Override
    public String getForceEncryptHeaderTrigger()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(FORCE_ENCRYPT_HEADER_TRIGGER), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(FORCE_ENCRYPT_HEADER_TRIGGER)));
    }
    
    /**
     * If true a header can force encypt of a message (see ForceEncryptHeaderTrigger)
     */
    @Override
    public void setForceEncryptAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(FORCE_ENCRYPT_ALLOWED),  
                allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(FORCE_ENCRYPT_ALLOWED)));
    }
    
    @Override
    public boolean isForceEncryptAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(FORCE_ENCRYPT_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(FORCE_ENCRYPT_ALLOWED)));
    }

    @Override
    public void setMaxSizeSMIME(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(MAX_SIZE_SMIME),  
        		size, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(MAX_SIZE_SMIME)));
    }
    
    @Override
    public Long getMaxSizeSMIME()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(MAX_SIZE_SMIME), 
    			null, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(MAX_SIZE_SMIME)));
    }
    
    @Override
    public void setRemoveSMIMESignature(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(REMOVE_SMIME_SIGNATURE),  
                value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(REMOVE_SMIME_SIGNATURE)));
    }
    
    @Override
    public boolean isRemoveSMIMESignature()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(REMOVE_SMIME_SIGNATURE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(REMOVE_SMIME_SIGNATURE)));
    }
    
    @Override
    public void setSubjectTrigger(String trigger)
    throws HierarchicalPropertiesException
    {
    	setProperty(getFullPropertyName(SUBJECT_TRIGGER), trigger, PropertyRegistry.getInstance().
    	        isEncrypted(getFullPropertyName(SUBJECT_TRIGGER)));
    }
    
    @Override
    public String getSubjectTrigger()
    throws HierarchicalPropertiesException
    {
    	return getProperty(getFullPropertyName(SUBJECT_TRIGGER), PropertyRegistry.getInstance().
    	        isEncrypted(getFullPropertyName(SUBJECT_TRIGGER)));
    }
    
    @Override
    public void setSubjectTriggerEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_ENABLED),  
        		enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_TRIGGER_ENABLED)));
    }
    
    @Override
    public boolean isSubjectTriggerEnabled()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_ENABLED), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_TRIGGER_ENABLED)));
    }
    
    @Override
    public void setSubjectTriggerIsRegExpr(Boolean isRegExpr)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_IS_REGEXPR),  
        		isRegExpr, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_TRIGGER_IS_REGEXPR)));
    }
    
    @Override
    public boolean isSubjectTriggerRegExpr()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_IS_REGEXPR), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_TRIGGER_IS_REGEXPR)));
    }

    @Override
    public void setSubjectTriggerRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_REMOVE_PATTERN),  
        		removePattern, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(
        		        SUBJECT_TRIGGER_REMOVE_PATTERN)));
    }
    
    @Override
    public boolean isSubjectTriggerRemovePattern()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SUBJECT_TRIGGER_REMOVE_PATTERN), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_TRIGGER_REMOVE_PATTERN)));
    }
    
    @Override
    public void setSendEncryptionNotification(Boolean sendNotification)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_ENCRYPTION_NOTIFICATION),  
        		sendNotification, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(
        		        SEND_ENCRYPTION_NOTIFICATION)));
    }
    
    @Override
    public boolean isSendEncryptionNotification()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_ENCRYPTION_NOTIFICATION), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_ENCRYPTION_NOTIFICATION)));
    }
    
    @Override
    public void setSMSPhoneNumber(String phoneNumber) 
    throws HierarchicalPropertiesException 
    {
    	String filtered = PhoneNumberUtils.filterAndValidatePhoneNumber(phoneNumber);

    	if (phoneNumber != null && filtered == null) {
    		throw new HierarchicalPropertiesException("PhoneNumber " + phoneNumber + " is invalid.");
    	}
    	
        this.setProperty(getFullPropertyName(SMS_PHONE_NUMBER), filtered, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PHONE_NUMBER)));                
    }
    
    @Override
    public String getSMSPhoneNumber() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(SMS_PHONE_NUMBER), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SMS_PHONE_NUMBER)));
    }
    
    @Override
    public void setSMSSendAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMS_SEND_ALLOWED),  
        		allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_SEND_ALLOWED)));
    }
    
    @Override
    public boolean isSMSSendAllowed()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMS_SEND_ALLOWED), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_SEND_ALLOWED)));
    }

    @Override
    public void setSMSReceiveAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMS_RECEIVE_ALLOWED),  
        		allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_RECEIVE_ALLOWED)));
    }
    
    @Override
    public boolean isSMSReceiveAllowed()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMS_RECEIVE_ALLOWED), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_RECEIVE_ALLOWED)));
    }
    
    @Override
    public void setSMSPhoneNumberSetAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SMS_PHONE_NUMBER_SET_ALLOWED),  
                allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_PHONE_NUMBER_SET_ALLOWED)));
    }
    
    @Override
    public boolean isSMSPhoneNumberSetAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SMS_PHONE_NUMBER_SET_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SMS_PHONE_NUMBER_SET_ALLOWED)));
    }
    
    @Override
    public void setPhoneDefaultCountryCode(String defaultCountryCode)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(PHONE_DEFAULT_COUNTRY_CODE), defaultCountryCode, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PHONE_DEFAULT_COUNTRY_CODE)));
    }
    
    @Override
    public String getPhoneDefaultCountryCode()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(PHONE_DEFAULT_COUNTRY_CODE), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(PHONE_DEFAULT_COUNTRY_CODE)));
    }
    
    @Override
    public void setPassword(String password) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(PASSWORD), password, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PASSWORD)));
        
        setDatePasswordSet(new Date());
    }
    
    @Override
    public String getPassword() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(PASSWORD), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PASSWORD))); 
    }
    
    @Override
    public void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(this, getFullPropertyName(PASSWORD_LENGTH), 
                length, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORD_LENGTH)));
    }
    
    @Override
    public int getPasswordLength()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(this, getFullPropertyName(PASSWORD_LENGTH), 
                8, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORD_LENGTH)));
    }
    
    @Override
    public void setPasswordID(String passwordID) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(PASSWORD_ID), passwordID, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PASSWORD_ID)));
    }
    
    @Override
    public String getPasswordID() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(PASSWORD_ID), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PASSWORD_ID)));
    }
    
    @Override
    public void setDatePasswordSet(Date datePasswordSet) 
    throws HierarchicalPropertiesException 
    {
        /*
         * We will store the date as long
         */
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(DATE_PASSWORD_SET), 
                datePasswordSet != null ? datePasswordSet.getTime() : null, PropertyRegistry.getInstance().
                        isEncrypted(getFullPropertyName(DATE_PASSWORD_SET)));
    }
    
    @Override
    public Date getDatePasswordSet() 
    throws HierarchicalPropertiesException 
    {
        Date date = null;

        Long dateMilliseconds = HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(DATE_PASSWORD_SET),
        		null, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(DATE_PASSWORD_SET)));
        
        if (dateMilliseconds != null) {
            date = new Date(dateMilliseconds);
        }
        
        return date;
    }
    
    @Override
    public void setPasswordValidityInterval(Long interval) 
    throws HierarchicalPropertiesException 
    {
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(PASSWORD_VALIDITY_INTERVAL), 
        		interval, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORD_VALIDITY_INTERVAL)));
    }
    
    @Override
    public long getPasswordValidityInterval() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(PASSWORD_VALIDITY_INTERVAL), 
        		0L, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORD_VALIDITY_INTERVAL)));
    }
    
    @Override
    public void setPasswordsSendToOriginator(Boolean value) 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(PASSWORDS_SEND_TO_ORIGINATOR),  
                value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORDS_SEND_TO_ORIGINATOR)));
    }
    
    @Override
    public boolean isPasswordsSendToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(PASSWORDS_SEND_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PASSWORDS_SEND_TO_ORIGINATOR)));
    }
    
    @Override
    public void setServerSecret(String secret) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(SERVER_SECRET), secret, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SERVER_SECRET)));
    }
    
    @Override
    public String getServerSecret() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(SERVER_SECRET), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SERVER_SECRET))); 
    }

    @Override
    public void setClientSecret(String secret) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(CLIENT_SECRET), secret, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(CLIENT_SECRET)));
    }
    
    @Override
    public String getClientSecret() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(CLIENT_SECRET), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(CLIENT_SECRET))); 
    }
    
    @Override
    public void setBlackBerryRecipient(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(BLACKBERRY_RECIPIENT),  
        		value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(BLACKBERRY_RECIPIENT)));
    }
    
    @Override
    public boolean isBlackBerryRecipient()
    throws HierarchicalPropertiesException
    {
    	return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(BLACKBERRY_RECIPIENT), 
    			false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(BLACKBERRY_RECIPIENT)));
    }
    
    @Override
    public void setStripUnsupportedFormats(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(STRIP_UNSUPPORTED_FORMATS),  
                value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(STRIP_UNSUPPORTED_FORMATS)));
    }
    
    @Override
    public boolean isStripUnsupportedFormats()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(STRIP_UNSUPPORTED_FORMATS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(STRIP_UNSUPPORTED_FORMATS)));
    }
    
    @Override
    public void setRelayAllowed(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(RELAY_ALLOWED),  
                value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(RELAY_ALLOWED)));
    }
    
    @Override
    public boolean isRelayAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(RELAY_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(RELAY_ALLOWED)));
    }
    
    @Override
    public void setRelayEmail(String email)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(RELAY_EMAIL), email, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(RELAY_EMAIL)));        
    }
    
    @Override
    public String getRelayEmail()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(RELAY_EMAIL), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(RELAY_EMAIL)));
    }
    
    @Override
    public void setRelayValidityInterval(Long interval)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(RELAY_VALIDITY_INTERVAL), 
                interval, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(RELAY_VALIDITY_INTERVAL)));
    }
    
    @Override
    public long getRelayValidityInterval()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(RELAY_VALIDITY_INTERVAL), 
                (long) 5 * 24 * 60 * 60 * 1000 /* 5 days */ , PropertyRegistry.getInstance().isEncrypted(
                        getFullPropertyName(RELAY_VALIDITY_INTERVAL)));
    }
    
    @Override
    public void setRelayBounceMode(RelayBounceMode mode)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(RELAY_BOUNCE_MODE), ObjectUtils.toString(mode, null),
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(RELAY_BOUNCE_MODE)));
    }
    
    @Override
    public RelayBounceMode getRelayBounceMode()
    throws HierarchicalPropertiesException
    {
        String value = this.getProperty(getFullPropertyName(RELAY_BOUNCE_MODE), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(RELAY_BOUNCE_MODE)));
        
        RelayBounceMode mode = RelayBounceMode.fromString(value);
        
        if (mode == null) {
            mode = RelayBounceMode.NEVER;
        }
        
        return mode;
    }
    
    @Override
    public void setPFXPassword(String password) 
    throws HierarchicalPropertiesException 
    {
        this.setProperty(getFullPropertyName(PFX_PASSWORD), password, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PFX_PASSWORD)));
    }
    
    @Override
    public String getPFXPassword() 
    throws HierarchicalPropertiesException 
    {
        return this.getProperty(getFullPropertyName(PFX_PASSWORD), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PFX_PASSWORD))); 
    }
    
    @Override
    public void setAutoRequestCertificate(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(AUTO_REQUEST_CERTIFICATE),  
                value, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_REQUEST_CERTIFICATE)));
    }
    
    @Override
    public boolean isAutoRequestCertificate()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(AUTO_REQUEST_CERTIFICATE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_REQUEST_CERTIFICATE)));
    }
    
    @Override
    public void setOTPEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(OTP_ENABLED),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(OTP_ENABLED)));
    }
    
    @Override
    public boolean isOTPEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(OTP_ENABLED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(OTP_ENABLED)));
    }
        
    @Override
    public void setOTPURL(String url)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(OTP_URL), url, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(OTP_URL)));
    }
    
    @Override
    public String getOTPURL()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(OTP_URL), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(OTP_URL)));
    }
    
    private String getDefaultOTPURL()
    throws HierarchicalPropertiesException
    {
        String defaultURL = StringUtils.trimToNull(portalProperties.getBaseURL());
        
        if (defaultURL != null) {
            defaultURL = MiscStringUtils.ensureEndsWith(defaultURL, "/") + DEFAULT_RELATIVE_OTP_URL;
        }
        
        return defaultURL;
    }
    
    @Override
    public void setAutoCreateClientSecret(Boolean autoCreate)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(AUTO_CREATE_CLIENT_SECRET),  
                autoCreate, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_CREATE_CLIENT_SECRET)));
    }
    
    @Override
    public boolean isAutoCreateClientSecret()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(AUTO_CREATE_CLIENT_SECRET), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(AUTO_CREATE_CLIENT_SECRET)));
    }

    @Override
    public void setAddSecurityInfo(Boolean add)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ADD_SECURITY_INFO),  
                add, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ADD_SECURITY_INFO)));
    }

    @Override
    public boolean isAddSecurityInfo()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ADD_SECURITY_INFO), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ADD_SECURITY_INFO)));
    }

    @Override
    public void setSecurityInfoDecryptedTag(String tag)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SECURITY_INFO_DECRYPTED_TAG), tag, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_DECRYPTED_TAG)));
    }

    @Override
    public String getSecurityInfoDecryptedTag()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SECURITY_INFO_DECRYPTED_TAG), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_DECRYPTED_TAG)));
    }

    @Override
    public void setSecurityInfoSignedValidTag(String tag)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SECURITY_INFO_SIGNED_VALID_TAG), tag, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_VALID_TAG)));
    }

    @Override
    public String getSecurityInfoSignedValidTagTag()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SECURITY_INFO_SIGNED_VALID_TAG), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_VALID_TAG)));
    }

    @Override
    public void setSecurityInfoSignedByValidTag(String tag)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SECURITY_INFO_SIGNED_BY_VALID_TAG), tag, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_BY_VALID_TAG)));
    }

    @Override
    public String getSecurityInfoSignedByValidTagTag()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SECURITY_INFO_SIGNED_BY_VALID_TAG), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_BY_VALID_TAG)));
    }
    
    @Override
    public void setSecurityInfoSignedInvalidTag(String tag)
    throws HierarchicalPropertiesException
    {
        this.setProperty(getFullPropertyName(SECURITY_INFO_SIGNED_INVALID_TAG), tag, PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_INVALID_TAG)));
    }

    @Override
    public String getSecurityInfoSignedInvalidTagTag()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(getFullPropertyName(SECURITY_INFO_SIGNED_INVALID_TAG), PropertyRegistry.getInstance().
                isEncrypted(getFullPropertyName(SECURITY_INFO_SIGNED_INVALID_TAG)));
    }
    
    @Override
    public void setPdfEncryptionAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(PDF_ENCRYPTION_ALLOWED),  
                allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_ENCRYPTION_ALLOWED)));
    }

    @Override
    public boolean isPdfEncryptionAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(PDF_ENCRYPTION_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_ENCRYPTION_ALLOWED)));
    }
    
    @Override
    public void setPdfMaxSize(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(PDF_MAX_SIZE), 
                size, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_MAX_SIZE)));
    }
    
    @Override
    public Long getPdfMaxSize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(PDF_MAX_SIZE), 
                null, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_MAX_SIZE)));
    }
    
    @Override
    public void setPdfReplyAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(PDF_REPLY_ALLOWED),  
                allowed, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_REPLY_ALLOWED)));
    }

    @Override
    public boolean isPdfReplyAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(PDF_REPLY_ALLOWED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_REPLY_ALLOWED)));
    }
    
    @Override
    public void setPdfReplyValidityInterval(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, getFullPropertyName(PDF_REPLY_VALIDITY_INTERVAL), 
                size, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_REPLY_VALIDITY_INTERVAL)));
    }
    
    @Override
    public Long getPdfReplyValidityInterval()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, getFullPropertyName(PDF_REPLY_VALIDITY_INTERVAL), 
                null, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_REPLY_VALIDITY_INTERVAL)));
    }
    
    @Override
    public void setPdfReplyURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(PDF_REPLY_URL), url, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PDF_REPLY_URL)));
    }
    
    @Override
    public String getPdfReplyURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(PDF_REPLY_URL), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PDF_REPLY_URL)));
    }

    private String getDefaultPdfReplyURL()
    throws HierarchicalPropertiesException
    {
        String defaultReplyURL = StringUtils.trimToNull(portalProperties.getBaseURL());
        
        if (defaultReplyURL != null) {
            defaultReplyURL = MiscStringUtils.ensureEndsWith(defaultReplyURL, "/") + PDF_DEFAULT_RELATIVE_REPLY_URL;
        }
        
        return defaultReplyURL;
    }

    @Override
    public void setPdfReplySender(String email)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(PDF_REPLY_SENDER), email, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PDF_REPLY_SENDER)));
    }
    
    @Override
    public String getPdfReplySender()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(PDF_REPLY_SENDER), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(PDF_REPLY_SENDER)));
    }

    @Override
    public void setPdfOnlyEncryptIfMandatory(Boolean encryptIfMandatory)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(PDF_ONLY_ENCRYPT_IF_MANDATORY),  
                encryptIfMandatory, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_ONLY_ENCRYPT_IF_MANDATORY)));
    }

    @Override
    public boolean isPdfOnlyEncryptIfMandatory()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(PDF_ONLY_ENCRYPT_IF_MANDATORY), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_ONLY_ENCRYPT_IF_MANDATORY)));
    }

    @Override
    public void setPdfSignEmail(Boolean sign)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(PDF_SIGN_EMAIL), sign, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_SIGN_EMAIL)));
    }

    @Override
    public boolean isPdfSignEmail()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(PDF_SIGN_EMAIL), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(PDF_SIGN_EMAIL)));
    }

    @Override
    public void setSubjectFilterEnabled(Boolean sign)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SUBJECT_FILTER_ENABLED), sign, 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_FILTER_ENABLED)));
    }

    @Override
    public boolean isSubjectFilterEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SUBJECT_FILTER_ENABLED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SUBJECT_FILTER_ENABLED)));
    }
    
    
    @Override
    public void setSubjectFilterRegEx(String regEx)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(SUBJECT_FILTER_REGEX), regEx, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SUBJECT_FILTER_REGEX)));
    }
    
    @Override
    public String getSubjectFilterRegEx()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(SUBJECT_FILTER_REGEX), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(SUBJECT_FILTER_REGEX)));
    }
    
    @Override
    public TemplateProperties getTemplateProperties() {
    	return templateProperties;
    }
    
    @Override
    public DLPProperties getDLPProperties() {
        return dlpProperties; 
    }

    @Override
    public DKIMProperties getDKIMProperties() {
        return dkimProperties; 
    }
    
    @Override
    public PortalProperties getPortalProperties() {
        return portalProperties;
    }
    
    /*
     * Some values need to have a 'calculated' default value since the default value will be based on other 
     * property values. We therefore need to check whether the value is null and if so, get the default value.
     *  
     * @see mitm.common.properties.DelegatedHierarchicalProperties#getProperty(java.lang.String, boolean)
     */
    @Override
    public String getProperty(String propertyName, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        String value = super.getProperty(propertyName, decrypt);
       
        if (value == null)
        {
            /*
             * Check if we have a default for the property
             */
            for (DefaultPropertyProvider defaultProvider : defaultProviders)
            {
                value = defaultProvider.getDefaultValue(propertyName);
                
                if (value != null) {
                    break;
                }
            }
        }
        
        return value;
    }

    @Override
    public String getDefaultValue(String property)
    throws HierarchicalPropertiesException
    {
        /*
         * Note: This is not optimal if we need to support multiple default values. If more than 
         * a couple properties need calculated default values, we will probably need to use a set 
         * of registered properties or something. For now this will suffice.
         */
        if (getFullPropertyName(OTP_URL).equals(property)) {
            return getDefaultOTPURL();
        }
        else if (getFullPropertyName(PDF_REPLY_URL).equals(property)) {
            return getDefaultPdfReplyURL();
        }
        
        return null;
    }
}
