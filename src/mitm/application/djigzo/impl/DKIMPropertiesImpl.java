/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import mitm.application.djigzo.DKIMProperties;
import mitm.application.djigzo.PropertyRegistry;
import mitm.common.properties.DelegatedHierarchicalProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;

public class DKIMPropertiesImpl extends DelegatedHierarchicalProperties implements DKIMProperties
{
    /* 
     * The base name of the DKIMProperties. The actual name of a property will be based upon the
     * base name and the name of the property.
     */
    private final static String BASE_NAME = "user.dkim.";
            
    public DKIMPropertiesImpl(HierarchicalProperties properties) {
        super(properties, null);
    }
    
    /*
     * Returns the full property name which is a combination of base name
     * and property name.
     */
    @Override
    public String getFullPropertyName(String property) {
        return staticGetFullPropertyName(property);
    }

    private static String staticGetFullPropertyName(String property) {
        return BASE_NAME + property;
    }
    
    /*
     * Register all the known properties using a static initializer
     * 
     * Note: when adding new properties always add them to the property registry 
     */
    static 
    {
        PropertyRegistry registry = PropertyRegistry.getInstance();

    	registry.registerProperty(staticGetFullPropertyName(KEY_PAIR), false);
    }
        
    @Override
    public void setKeyPair(String keyPair)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(KEY_PAIR), keyPair, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(KEY_PAIR)));
    }
    
    @Override
    public String getKeyPair()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(KEY_PAIR), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(KEY_PAIR)));
    }
    
    @Override
    public String getDefaultValue(String property)
    throws HierarchicalPropertiesException
    {
        /*
         * Not defaults provided
         */
        return null;
    }
}
