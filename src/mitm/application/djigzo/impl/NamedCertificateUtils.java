/*
 * Copyright (c) 2009, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.NamedCertificate;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;


public class NamedCertificateUtils
{
    /**
     * Returns a set of Certificates from the provided namedCertificate with the given name.
     * If namedCertificates is null an empty set is returned.
     */
    public static Set<X509Certificate> getByName(String name, Collection<NamedCertificate> namedCertificates)
    {
        Set<X509Certificate> matching = new HashSet<X509Certificate>();
        
        if (namedCertificates != null)
        {
            for (NamedCertificate namedCertificate : namedCertificates)
            {
                if (StringUtils.equals(name, namedCertificate.getName()))
                {
                    X509Certificate certificate = namedCertificate.getCertificate(); 
                    
                    if (certificate != null) {
                        matching.add(certificate);
                    }
                }
            }
        }
        
        return matching;
    }
    
    /**
     * Replaces all the named certificates with the given name in the target set with the new certificates. The replacement is
     * done in-place (ie. target is modified)
     */
    public static void replaceNamedCertificates(Collection<NamedCertificate> target, String name, 
            Collection<X509Certificate> certificates)
    {
        Check.notNull(name, "name");

        if (target != null)
        {
            /*
             * We need to store the items we need to remove. We cannot change target
             * while looping.
             */
            List<NamedCertificate> toRemove = new LinkedList<NamedCertificate>();
            
            for (NamedCertificate namedCertificate : target)
            {
                if (namedCertificate != null && StringUtils.equals(name, namedCertificate.getName())) {
                    toRemove.add(namedCertificate);
                }
            }
            
            for (NamedCertificate namedCertificate : toRemove) {
                target.remove(namedCertificate);
            }

            if (certificates != null)
            {
                for (X509Certificate certificate : certificates) {
                    target.add(new NamedCertificateImpl(name, certificate));
                }
            }
        }
    }
}
