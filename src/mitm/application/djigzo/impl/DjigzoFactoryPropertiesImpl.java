/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.util.Set;

import mitm.application.djigzo.DjigzoFactoryProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

import org.apache.commons.lang.time.DateUtils;

public class DjigzoFactoryPropertiesImpl implements DjigzoFactoryProperties
{
    private final static String BASE_NAME = "system";

    private final HierarchicalProperties sourceProperties;
    
    public DjigzoFactoryPropertiesImpl(HierarchicalProperties sourceProperties)
    {
    	Check.notNull(sourceProperties, "sourceProperties");
        
        this.sourceProperties = sourceProperties; 
    }
    
    /*
     * Returns the full property name which is a combination base name and property name.
     */
    private String getPropertyName(String property) {
        return BASE_NAME + "." + property;
    }
    
    @Override
    public boolean isEncryptionRecipientSelectorCheckValidity() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getBoolean(sourceProperties, 
                getPropertyName("encryptionRecipientSelector.checkValidity"), true, false);
    }
    
    @Override
    public long getCRLDownloadParametersTotalTimeout() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("cRLDownloadParameters.totalTimeout"), 15 * DateUtils.MILLIS_PER_MINUTE,
                false);
    }
    
    @Override
    public long getCRLDownloadParametersConnectTimeout() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("cRLDownloadParameters.connectTimeout"), 2 * DateUtils.MILLIS_PER_MINUTE,
                false);
    }
    
    @Override
    public long getCRLDownloadParametersReadTimeout() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("cRLDownloadParameters.readTimeout"), 2 * DateUtils.MILLIS_PER_MINUTE,
                false);
    }
    
    @Override
    public long getCRLDownloadParametersMaxBytes() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("cRLDownloadParameters.maxBytes"), 25L * 1024 * 1024, false);
    }
    
    @Override
    public boolean isCRLDownloadParametersCheckTrust() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getBoolean(sourceProperties, 
                getPropertyName("cRLDownloadParameters.checkTrust"), true, false);
    }
    
    @Override
    public long getCRLStoreUpdateInterval() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("cRLUpdater.interval"), DateUtils.MILLIS_PER_HOUR, false);        
    }
    
    
    @Override
    public long getTrustAnchorBuilderUpdateCheckInterval() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("trustAnchorBuilder.updateCheckInterval"), 30 * DateUtils.MILLIS_PER_MINUTE,
                false);
    }
    
    @Override
    public long getSleepTime() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("sms.gateway.sleepTime"), 5 * DateUtils.MILLIS_PER_MINUTE, true);        
    }
    
    @Override
    public long getUpdateInterval() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("sms.gateway.updateInterval"), 5 * DateUtils.MILLIS_PER_MINUTE, false);        
    }
    
    @Override
    public long getExpirationTime() 
    throws HierarchicalPropertiesException 
    {
        return HierarchicalPropertiesUtils.getLong(sourceProperties, 
                getPropertyName("sms.gateway.expirationTime"), 24 * DateUtils.MILLIS_PER_HOUR, false);        
    }
    
    /*
     * Delegate all the HierarchicalProperties methods to the sourceProperties. 
     */    
    @Override
    public String getCategory() {
        return sourceProperties.getCategory();
    }
    
    @Override
    public String getProperty(String propertyName, boolean decrypt) 
    throws HierarchicalPropertiesException 
    {
        return sourceProperties.getProperty(propertyName, decrypt);
    }

    @Override
    public void setProperty(String propertyName, String value, boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        sourceProperties.setProperty(propertyName, value, encrypt);
    }
    
    @Override
    public void deleteProperty(String propertyName) 
    throws HierarchicalPropertiesException 
    {
        sourceProperties.deleteProperty(propertyName);
    }
    
    @Override
    public void deleteAll() 
    throws HierarchicalPropertiesException 
    {
        sourceProperties.deleteAll();
    }
    
    @Override
    public boolean isInherited(String propertyName) 
    throws HierarchicalPropertiesException 
    {
        return sourceProperties.isInherited(propertyName);
    }
    
    @Override
    public Set<String> getProperyNames(boolean recursive) 
    throws HierarchicalPropertiesException 
    {
        return sourceProperties.getProperyNames(recursive);
    }
}
