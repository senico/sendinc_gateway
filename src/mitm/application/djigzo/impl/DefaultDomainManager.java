/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.util.Set;

import org.apache.commons.lang.StringUtils;

import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;
import mitm.common.util.DomainUtils;

/**
 * There isn't really something like a domain. A domain is nothing more than a UserPreferences object 
 * with a specific naming strategy. This class is a thin wrapper around UserPreferencesCategoryManager.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultDomainManager implements DomainManager
{
    private final UserPreferencesManager domainPreferencesManager;
    
    private final GlobalPreferencesManager globalPreferencesManager;
    
    public DefaultDomainManager(UserPreferencesCategoryManager userPreferencesCategoryManager,
    		GlobalPreferencesManager globalPreferencesManager) 
    {
    	Check.notNull(userPreferencesCategoryManager, "userPreferencesCategoryManager");
    	Check.notNull(globalPreferencesManager, "globalPreferencesManager");
    	
        this.domainPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(
                UserPreferencesCategory.DOMAIN.getName());
        
        this.globalPreferencesManager = globalPreferencesManager;
    }
    
    private UserPreferencesManager getUserPreferencesManager()
    {
        return domainPreferencesManager;
    }
    
    @Override
    public UserPreferences addDomain(String domain) 
    throws HierarchicalPropertiesException, CloseableIteratorException
    {
        String canonicalDomain = DomainUtils.canonicalizeAndValidate(domain, DomainUtils.DomainType.WILD_CARD);
        
        if (domain == null) {
            throw new IllegalArgumentException("Domain is not valid.");
        }
        
        UserPreferences domainPreferences = getUserPreferencesManager().addUserPreferences(canonicalDomain);
        
        addInheritedPreferences(canonicalDomain, domainPreferences);
        
        return domainPreferences;
    }

    /*
     * wildcard domains inherit from the global preferences and non-wildcard domains inherit from wildcard 
     * preferences 
     */
    private void addInheritedPreferences(String domain, UserPreferences newDomainPreferences) 
    throws HierarchicalPropertiesException, CloseableIteratorException
    {
    	boolean isWildcardDomain = DomainUtils.isWildcardDomain(domain);
    	
    	if (isWildcardDomain)
    	{
    		/*
    		 * Wildcard domains will always inherit the global preferences
    		 */
    		newDomainPreferences.getInheritedUserPreferences().add(globalPreferencesManager.getGlobalUserPreferences());

    		/*
    		 * Since we are adding a wild card domain, we need to check whether there are already domains
    		 * that match the wild card domain because those domains will inherit from the wild card domain
    		 * 
    		 * Note: we MUST use the stateful version to get all domains to make sure that domains added
    		 * within the same transaction are used
    		 */
    		CloseableIterator<String> domainIterator = getUserPreferencesManager().getStatefulNameIterator();

    		try {
        		while (domainIterator.hasNext())
        		{
        		    String toCheckDomain = domainIterator.next();
    
        		    if (DomainUtils.isWildcardDomain(toCheckDomain)) {
        		        /*
        		         * Wilcard domains do not inherit other domains
        		         */
        		        continue;
        		    }
        		    
        		    if (DomainUtils.isWildcardMatchDomain(toCheckDomain, domain))
        		    {
        		        /*
        		         * It could be that there is already a higher prio wildcard domain. For example
        		         * sub.example.com should inherit from *.sub.example.com and not from *.example.com.
        		         * Only do this check if the domain to check is not the domain we are about to add
        		         */
        		        String toCheckWildcardDomain = "*." + toCheckDomain;
        		        
        		        if (!toCheckWildcardDomain.equals(domain) && 
        		                getDomainPreferences(toCheckWildcardDomain) != null)
        		        {
        		            /*
        		             * There is a higher prio domain
        		             */
        		            continue;
        		        }
        		        
        		        UserPreferences toCheckDomainPrefs = getDomainPreferences(toCheckDomain);
        		        
        		        if (toCheckDomainPrefs != null)
        		        {
        		            Set<UserPreferences> inherited = toCheckDomainPrefs.getInheritedUserPreferences();
    
        		            /*
        		             * The domain will inherit from the wildcard domain only so we will clear
        		             * the 'live' set of inherited preferences first
        		             */
        		            inherited.clear();
        		            /*
        		             * Inherit from the new wildcard domain
        		             */
        		            inherited.add(newDomainPreferences);
        		        }
        		    }
        		}
    		}
    		finally {
    		    CloseableIteratorUtils.closeQuietly(domainIterator);
    		}
    	}
    	else {
    		/*
    		 * The new domain is not a wildcard domain. We now need to check if there is already a wildcard 
    		 * domain because the new domain need to inherit the wildcard domain. 
    		 * 
    		 * First check if there is a wildcard domain with appending * and then check upper level.
    		 * 
    		 * Example:
    		 * 
    		 * if domain is sub.example.com, if *.sub.example.com exists, it's used. If not, it's checked
    		 * whether *.example.com exist
    		 */
    		UserPreferences wildcardDomain = getDomainPreferences("*." + domain);

            if (wildcardDomain == null)
            {
                /*
                 * Now check if there is a wildcard domain for the upper level domain 
                 */
                String upperDomain = DomainUtils.getUpperLevelDomain(domain);
                
                if (StringUtils.isNotEmpty(upperDomain)) {
                    wildcardDomain = getDomainPreferences("*." + upperDomain);
                }
            }
    		
    		if (wildcardDomain != null)
    		{
    			/*
    			 * Inherit from the new domain
    			 */
    			newDomainPreferences.getInheritedUserPreferences().add(wildcardDomain);
    		}
    		else {
    			/*
    			 * There was no wildcard domain so the new domain must inherit the global preferences
    			 */
        		newDomainPreferences.getInheritedUserPreferences().add(globalPreferencesManager.getGlobalUserPreferences());
    		}
    	}
    }
    
    @Override
    public boolean deleteDomainPreferences(UserPreferences domainPreferences)
    {
        return getUserPreferencesManager().deleteUserPreferences(domainPreferences);         
    }

    @Override
    public UserPreferences getDomainPreferences(String domain)
    {
        return getUserPreferencesManager().getUserPreferences(domain);
    }
    
    @Override
    public boolean isDomainInUse(String domain) 
    {
    	boolean inUse = false;

    	UserPreferences domainPreferences = getDomainPreferences(domain);
    	
    	if (domainPreferences != null) {
    		inUse = getUserPreferencesManager().isInUse(domainPreferences);
    	}

    	return inUse;
    }
    
    @Override
    public int getDomainCount() {
    	return getUserPreferencesManager().getUserPreferencesCount();
    }

    @Override
    public CloseableIterator<String> getDomainIterator()
    {
        return getUserPreferencesManager().getNameIterator();
    }
    
    @Override
    public CloseableIterator<String> getDomainIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
    {
    	return getUserPreferencesManager().getNameIterator(firstResult, maxResults, sortDirection);
    }  
}
