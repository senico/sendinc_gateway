/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import mitm.application.djigzo.DLPProperties;
import mitm.application.djigzo.PortalProperties;
import mitm.application.djigzo.PropertyRegistry;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.DelegatedHierarchicalProperties;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

/**
 * Implementation of DLPProperties.
 * 
 * @author Martijn Brinkers
 *
 */
public class DLPPropertiesImpl extends DelegatedHierarchicalProperties implements DLPProperties
{
    /* 
     * The base name of the DLPProperties. The actual name of a property will be based upon the
     * base name and the name of the property.
     */
    private final static String BASE_NAME = "user.dlp.";
        
    /*
     * The default relative URL for the Quarantine reply 
     */
    private final static String DEFAULT_RELATIVE_QUARANTINE_URL = "dlp/quarantineview";
    
    /*
     * We need the PortalProperties to get the default portal base URL
     */
    private final PortalProperties portalProperties;
    
    public DLPPropertiesImpl(HierarchicalProperties properties, PortalProperties portalProperties)
    {
        super(properties, null);
        
        Check.notNull(portalProperties, "portalProperties");
        
        this.portalProperties = portalProperties;
    }
    
    /*
     * Returns the full property name which is a combination of base name
     * and property name.
     */
    @Override
    public String getFullPropertyName(String property) {
        return staticGetFullPropertyName(property);
    }

    private static String staticGetFullPropertyName(String property) {
        return BASE_NAME + property;
    }
    
    /*
     * Register all the known properties using a static initializer
     * 
     * Note: when adding new properties always add them to the property registry 
     */
    static 
    {
        PropertyRegistry registry = PropertyRegistry.getInstance();

    	registry.registerProperty(staticGetFullPropertyName(ENABLED), false);
        registry.registerProperty(staticGetFullPropertyName(QUARANTINE_URL), false);
        registry.registerProperty(staticGetFullPropertyName(DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_WARNING_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_WARNING_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_QUARANTINE_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_QUARANTINE_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_BLOCK_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_BLOCK_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_ERROR_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_ERROR_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(ALLOW_DOWNLOAD_MESSAGE), false);
        registry.registerProperty(staticGetFullPropertyName(ALLOW_RELEASE_MESSAGE), false);
        registry.registerProperty(staticGetFullPropertyName(ALLOW_RELEASE_ENCRYPT_MESSAGE), false);
        registry.registerProperty(staticGetFullPropertyName(ALLOW_RELEASE_AS_IS_MESSAGE), false);
        registry.registerProperty(staticGetFullPropertyName(ALLOW_DELETE_MESSAGE), false);
        registry.registerProperty(staticGetFullPropertyName(QUARANTINE_ON_ERROR), false);
        registry.registerProperty(staticGetFullPropertyName(QUARANTINE_ON_FAILED_ENCRYPTION), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_RELEASE_NOTIFY_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_DELETE_NOTIFY_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_DELETE_NOTIFY_TO_DLP_MANAGERS), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_EXPIRE_NOTIFY_TO_ORIGINATOR), false);
        registry.registerProperty(staticGetFullPropertyName(SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS), false);
    }

    @Override
    public void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ENABLED),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ENABLED)));
    }

    @Override
    public boolean isEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ENABLED), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ENABLED)));
    }

    @Override
    public void setQuarantineURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(getFullPropertyName(QUARANTINE_URL), url, PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(QUARANTINE_URL)));
    }
    
    @Override
    public String getQuarantineURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(getFullPropertyName(QUARANTINE_URL), PropertyRegistry.getInstance().isEncrypted(
                getFullPropertyName(QUARANTINE_URL)));
    }
    
    private String getDefaultQuarantineURL()
    throws HierarchicalPropertiesException
    {
        String defaultURL = StringUtils.trimToNull(portalProperties.getBaseURL());
        
        if (defaultURL != null) {
            defaultURL = MiscStringUtils.ensureEndsWith(defaultURL, "/") + DEFAULT_RELATIVE_QUARANTINE_URL;
        }
        
        return defaultURL;
    }
    
    @Override
    public void setDLPManagers(String... emails)
    throws HierarchicalPropertiesException
    {
        StrBuilder sb = new StrBuilder();
        
        if (emails != null)
        {
            for (String email : emails)
            {
                email = StringUtils.trimToNull(email);
                
                if (email == null) {
                    continue;
                }
                
                /*
                 * Only accept valid email addresses
                 */
                if (!EmailAddressUtils.isValid(email)) {
                    throw new HierarchicalPropertiesException("Email address is not valid: " + email);
                }
                
                sb.appendSeparator(", ");
                sb.append(email);
            }
        }
        
        setProperty(getFullPropertyName(DLP_MANAGERS), StringUtils.trimToNull(sb.toString()), 
                PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(DLP_MANAGERS)));
    }
    
    @Override
    public String[] getDLPManagers()
    throws HierarchicalPropertiesException
    {
        String[] emails = null;
        
        String list = StringUtils.trimToNull(getProperty(getFullPropertyName(DLP_MANAGERS), PropertyRegistry.
                getInstance().isEncrypted(getFullPropertyName(DLP_MANAGERS))));
        
        if (list != null) {
            emails = StringUtils.split(list, ',');
        }
        
        return emails;
    }    
    
    @Override
    public void setSendWarningToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_WARNING_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_WARNING_TO_ORIGINATOR)));
    }
    
    @Override
    public boolean isSendWarningToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_WARNING_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_WARNING_TO_ORIGINATOR)));
    }
    
    @Override
    public void setSendWarningToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_WARNING_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_WARNING_TO_DLP_MANAGERS)));
    }
    
    @Override
    public boolean isSendWarningToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_WARNING_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_WARNING_TO_DLP_MANAGERS)));
    }
        
    @Override
    public void setSendQuarantineToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_QUARANTINE_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_QUARANTINE_TO_ORIGINATOR)));
    }
    
    @Override
    public boolean isSendQuarantineToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_QUARANTINE_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_QUARANTINE_TO_ORIGINATOR)));
    }
    
    @Override
    public void setSendQuarantineToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_QUARANTINE_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_QUARANTINE_TO_DLP_MANAGERS)));
    }
    
    @Override
    public boolean isSendQuarantineToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_QUARANTINE_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_QUARANTINE_TO_DLP_MANAGERS)));
    }

    @Override
    public void setSendBlockToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_BLOCK_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_BLOCK_TO_ORIGINATOR)));
    }
    
    @Override
    public boolean isSendBlockToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_BLOCK_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_BLOCK_TO_ORIGINATOR)));
    }
    
    @Override
    public void setSendBlockToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_BLOCK_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_BLOCK_TO_DLP_MANAGERS)));
    }
    
    @Override
    public boolean isSendBlockToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_BLOCK_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_BLOCK_TO_DLP_MANAGERS)));
    }
        
    @Override
    public void setSendErrorToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_ERROR_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_ERROR_TO_ORIGINATOR)));
    }
    
    @Override
    public boolean isSendErrorToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_ERROR_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_ERROR_TO_ORIGINATOR)));
    }
    
    @Override
    public void setSendErrorToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_ERROR_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_ERROR_TO_DLP_MANAGERS)));
    }
    
    @Override
    public boolean isSendErrorToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_ERROR_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_ERROR_TO_DLP_MANAGERS)));
    }

    @Override
    public void setAllowDownloadMessage(Boolean allow)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALLOW_DOWNLOAD_MESSAGE),  
                allow, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_DOWNLOAD_MESSAGE)));        
    }

    @Override
    public boolean isAllowDownloadMessage()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALLOW_DOWNLOAD_MESSAGE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_DOWNLOAD_MESSAGE)));
    }

    @Override
    public void setAllowReleaseMessage(Boolean allow)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALLOW_RELEASE_MESSAGE),  
                allow, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_MESSAGE)));        
    }

    @Override
    public boolean isAllowReleaseMessage()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALLOW_RELEASE_MESSAGE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_MESSAGE)));
    }

    @Override
    public void setAllowReleaseEncryptMessage(Boolean allow)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALLOW_RELEASE_ENCRYPT_MESSAGE),  
                allow, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_ENCRYPT_MESSAGE)));        
    }

    @Override
    public boolean isAllowReleaseEncryptMessage()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALLOW_RELEASE_ENCRYPT_MESSAGE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_ENCRYPT_MESSAGE)));
    }
    
    @Override
    public void setAllowReleaseAsIsMessage(Boolean allow)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALLOW_RELEASE_AS_IS_MESSAGE),  
                allow, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_AS_IS_MESSAGE)));        
    }
    
    @Override
    public boolean isAllowReleaseAsIsMessage()
    throws HierarchicalPropertiesException    
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALLOW_RELEASE_AS_IS_MESSAGE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_RELEASE_AS_IS_MESSAGE)));
    }
    
    @Override
    public void setAllowDeleteMessage(Boolean allow)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(ALLOW_DELETE_MESSAGE),  
                allow, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_DELETE_MESSAGE)));        
    }

    @Override
    public boolean isAllowDeleteMessage()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(ALLOW_DELETE_MESSAGE), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(ALLOW_DELETE_MESSAGE)));
    }
    
    @Override
    public void setQuarantineOnError(Boolean quarantine)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(QUARANTINE_ON_ERROR),  
                quarantine, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(QUARANTINE_ON_ERROR)));        
    }
    
    @Override
    public boolean isQuarantineOnError()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(QUARANTINE_ON_ERROR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(QUARANTINE_ON_ERROR)));
    }

    @Override
    public void setQuarantineOnFailedEncryption(Boolean quarantine)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(QUARANTINE_ON_FAILED_ENCRYPTION),  
                quarantine, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(QUARANTINE_ON_FAILED_ENCRYPTION)));        
    }
    
    @Override
    public boolean isQuarantineOnFailedEncryption()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(QUARANTINE_ON_FAILED_ENCRYPTION), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(QUARANTINE_ON_FAILED_ENCRYPTION)));
    }
    
    @Override
    public void setSendReleaseNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_RELEASE_NOTIFY_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_RELEASE_NOTIFY_TO_ORIGINATOR)));        
    }

    @Override
    public boolean isSendReleaseNotifyToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_RELEASE_NOTIFY_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_RELEASE_NOTIFY_TO_ORIGINATOR)));
    }

    @Override
    public void setSendReleaseNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS)));        
    }

    @Override
    public boolean isSendReleaseNotifyToDLPManagers() 
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS)));
    }

    @Override
    public void setSendDeleteNotifyToOriginator(Boolean enabled) 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_DELETE_NOTIFY_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_DELETE_NOTIFY_TO_ORIGINATOR)));        
    }

    @Override
    public boolean isSendDeleteNotifyToOriginator() 
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_DELETE_NOTIFY_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_DELETE_NOTIFY_TO_ORIGINATOR)));
    }

    @Override
    public void setSendDeleteNotifyToDLPManagers(Boolean enabled) 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_DELETE_NOTIFY_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_DELETE_NOTIFY_TO_DLP_MANAGERS)));        
    }

    @Override
    public boolean isSendDeleteNotifyToDLPManagers() 
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_DELETE_NOTIFY_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_DELETE_NOTIFY_TO_DLP_MANAGERS)));
    }

    @Override
    public void setSendExpireNotifyToOriginator(Boolean enabled) 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_ORIGINATOR),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_ORIGINATOR)));        
    }

    @Override
    public boolean isSendExpireNotifyToOriginator() 
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_ORIGINATOR), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_ORIGINATOR)));
    }

    @Override
    public void setSendExpireNotifyToDLPManagers(Boolean enabled) 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS),  
                enabled, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS)));        
    }

    @Override
    public boolean isSendExpireNotifyToDLPManagers() 
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS), 
                false, PropertyRegistry.getInstance().isEncrypted(getFullPropertyName(SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS)));
    }    
    
    @Override
    public String getDefaultValue(String property)
    throws HierarchicalPropertiesException
    {
        /*
         * Note: This is not optimal if we need to support multiple default values. If more than 
         * a couple properties need calculated default values, we will probably need to use a set 
         * of registered properties or something. For now this will suffice.
         */
        if (getFullPropertyName(QUARANTINE_URL).equals(property)) {
            return getDefaultQuarantineURL();
        }
        
        return null;
    }    
}
