/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import mitm.common.properties.DefaultPropertyProvider;
import mitm.common.properties.HierarchicalPropertiesException;

/**
 * Java interface wrapper around hierarchical properties for the DLP properties
 * 
 * @author Martijn Brinkers
 *
 */
public interface DLPProperties extends ResolvableHierarchicalProperties, DefaultPropertyProvider
{
    public final static String ENABLED                             = "enabled";
    public final static String QUARANTINE_URL                      = "quarantineURL";
    public final static String DLP_MANAGERS                        = "dlpManagers";
    public final static String SEND_WARNING_TO_ORIGINATOR          = "sendWarningToOriginator";
    public final static String SEND_WARNING_TO_DLP_MANAGERS        = "sendWarningToDLPManagers";
    public final static String SEND_QUARANTINE_TO_ORIGINATOR       = "sendQuarantineToOriginator";
    public final static String SEND_QUARANTINE_TO_DLP_MANAGERS     = "sendQuarantineToDLPManagers";
    public final static String SEND_BLOCK_TO_ORIGINATOR            = "sendBlockToOriginator";
    public final static String SEND_BLOCK_TO_DLP_MANAGERS          = "sendBlockToDLPManagers";
    public final static String SEND_ERROR_TO_ORIGINATOR            = "sendErrorToOriginator";
    public final static String SEND_ERROR_TO_DLP_MANAGERS          = "sendErrorToDLPManagers";
    public final static String ALLOW_DOWNLOAD_MESSAGE              = "allowDownloadMessage";
    public final static String ALLOW_RELEASE_MESSAGE               = "allowReleaseMessage";
    public final static String ALLOW_RELEASE_ENCRYPT_MESSAGE       = "allowReleaseEncryptMessage";
    public final static String ALLOW_RELEASE_AS_IS_MESSAGE         = "allowReleaseAsIsMessage";
    public final static String ALLOW_DELETE_MESSAGE                = "allowDeleteMessage";
    public final static String QUARANTINE_ON_ERROR                 = "quarantineOnError";
    public final static String QUARANTINE_ON_FAILED_ENCRYPTION     = "quarantineOnFailedEncryption";
    public final static String SEND_RELEASE_NOTIFY_TO_ORIGINATOR   = "sendReleaseNotifyToOriginator";
    public final static String SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS = "sendReleaseNotifyToDLPManagers";
    public final static String SEND_DELETE_NOTIFY_TO_ORIGINATOR    = "sendDeleteNotifyToOriginator";
    public final static String SEND_DELETE_NOTIFY_TO_DLP_MANAGERS  = "sendDeleteNotifyToDLPManagers";
    public final static String SEND_EXPIRE_NOTIFY_TO_ORIGINATOR    = "sendExpireNotifyToOriginator";
    public final static String SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS  = "sendExpireNotifyToDLPManagers";
    
    /**
     * If true, DLP is enabled for the user
     */
    public void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isEnabled()
    throws HierarchicalPropertiesException;
    
    /**
     * The base URL for the Quarantine
     */
    public void setQuarantineURL(String url)
    throws HierarchicalPropertiesException;
    
    public String getQuarantineURL()
    throws HierarchicalPropertiesException;
    
    /**
     * A comma separated list of email addresses to which DLP notifications 
     * (for example warnings) should be sent.
     * 
     * Note: the email address must be valid email addresses.
     */
    public void setDLPManagers(String... emails)
    throws HierarchicalPropertiesException;
    
    public String[] getDLPManagers()
    throws HierarchicalPropertiesException;   
    
    /**
     * If true, warnings, are send to the originator
     */
    public void setSendWarningToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendWarningToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, warnings, are send to the DLP managers
     */
    public void setSendWarningToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendWarningToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, Quarantine warnings, are send to the originator
     */
    public void setSendQuarantineToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendQuarantineToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, Quarantine warnings, are send to the DLP managers
     */
    public void setSendQuarantineToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendQuarantineToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, Block warnings, are send to the originator
     */
    public void setSendBlockToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendBlockToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, Block warnings, are send to the DLP managers
     */
    public void setSendBlockToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendBlockToDLPManagers()
    throws HierarchicalPropertiesException;
        
    /**
     * If true, Error warnings, are send to the originator
     */
    public void setSendErrorToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendErrorToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, Error warnings, are send to the DLP managers
     */
    public void setSendErrorToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendErrorToDLPManagers()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a user may download a message from the quarantine
     */
    public void setAllowDownloadMessage(Boolean allow)
    throws HierarchicalPropertiesException;
    
    public boolean isAllowDownloadMessage()
    throws HierarchicalPropertiesException;    
    
    /**
     * If true, a user may release a message from the quarantine
     */
    public void setAllowReleaseMessage(Boolean allow)
    throws HierarchicalPropertiesException;
    
    public boolean isAllowReleaseMessage()
    throws HierarchicalPropertiesException;    

    /**
     * If true, a user may release a message to be force encrypted from the quarantine
     */
    public void setAllowReleaseEncryptMessage(Boolean allow)
    throws HierarchicalPropertiesException;
    
    public boolean isAllowReleaseEncryptMessage()
    throws HierarchicalPropertiesException;    

    /**
     * If true, a user may release a message directly to outgoing transport
     */
    public void setAllowReleaseAsIsMessage(Boolean allow)
    throws HierarchicalPropertiesException;
    
    public boolean isAllowReleaseAsIsMessage()
    throws HierarchicalPropertiesException;    
    
    /**
     * If true, a user may delete a message from the quarantine
     */
    public void setAllowDeleteMessage(Boolean allow)
    throws HierarchicalPropertiesException;
    
    public boolean isAllowDeleteMessage()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, and an error occurs during DLP, the message will be put in quarantine
     */
    public void setQuarantineOnError(Boolean quarantine)
    throws HierarchicalPropertiesException;
    
    public boolean isQuarantineOnError()
    throws HierarchicalPropertiesException;

    /**
     * If true, and a message cannot be encrypted and encryption is mandatory, the message will be quarantined
     */
    public void setQuarantineOnFailedEncryption(Boolean quarantine)
    throws HierarchicalPropertiesException;
    
    public boolean isQuarantineOnFailedEncryption()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the originator when the email is released from quarantine.
     */
    public void setSendReleaseNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendReleaseNotifyToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the DLP managers when the email is released from quarantine.
     */
    public void setSendReleaseNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendReleaseNotifyToDLPManagers()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the originator when the email is deleted from quarantine.
     */
    public void setSendDeleteNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendDeleteNotifyToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the DLP managers when the email is deleted from quarantine.
     */
    public void setSendDeleteNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendDeleteNotifyToDLPManagers()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the originator when the email expires from quarantine.
     */
    public void setSendExpireNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendExpireNotifyToOriginator()
    throws HierarchicalPropertiesException;
    
    /**
     * If true, a notification is send to the DLP managers when the email expires from quarantine.
     */
    public void setSendExpireNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;
    
    public boolean isSendExpireNotifyToDLPManagers()
    throws HierarchicalPropertiesException;    
}
