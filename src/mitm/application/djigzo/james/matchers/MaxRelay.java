/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;
import java.util.Enumeration;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.MailAddressUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.RFC2822Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matches of the number of Received headers exceeds the maximum (default 100).
 * This is used as a protection against mail loops.
 * 
 * @author Martijn Brinkers
 *
 */
public class MaxRelay extends AbstractContextAwareDjigzoMatcher 
{
	private final static Logger logger = LoggerFactory.getLogger(MaxRelay.class);
	
	/*
	 * The maximum number of received headers the message may have. If exceeded the message will match.
	 */
	private int relayLimit = 100;
	
	@Override
	protected Logger getLogger() {
		return logger;
	}

	@Override
    public void init()
    {
    	getLogger().info("Initializing matcher: " + getMatcherName());

        String condition = getCondition();

        if (condition != null) 
        {
        	try {
        		relayLimit = Integer.parseInt(condition);
        	}
        	catch(NumberFormatException e) 
        	{
        		getLogger().error(condition + " is not an integer.");
        	}
        }
        
        getLogger().info("Relay limit " + relayLimit);
    }
    
	@Override
	public Collection<MailAddress> matchMail(Mail mail)
	throws MessagingException 
	{
        MimeMessage message = mail.getMessage();
        
        int count = 0;
        
        for (Enumeration<?> headerEnum = message.getAllHeaders(); headerEnum.hasMoreElements();) 
        {
            Header header = (Header)headerEnum.nextElement();
            
            if (header.getName().equals(RFC2822Headers.RECEIVED)) {
                count++;
            }
        }
        
        if (count >= relayLimit) 
        {
        	getLogger().warn("Relay limit reached.");
        	
            return MailAddressUtils.getRecipients(mail);
        }
        
        return null;
	}
}
