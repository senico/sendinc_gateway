/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.io.IOException;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.RegExprUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Matcher that matches the subject against an expression. The matching parameters (the pattern, if the pattern is a regular
 * expression and if the matching pattern should be removed from the subject) are  taken from the senders user properties. 
 * 
 * Usage:
 * 
 * SenderSubjectTrigger=matchOnError=false
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderSubjectTrigger extends SubjectTrigger 
{
	private final static Logger logger = LoggerFactory.getLogger(SenderSubjectTrigger.class);
	
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
	
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * The key under which we store the activation context
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "triggerDetails";
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    protected static class TriggerDetails
    {
    	private String subjectTrigger;
    	private boolean regExpr;
    	private boolean removePattern;
    	
		protected String getSubjectTrigger() {
			return subjectTrigger;
		}
		
		protected boolean isRegExpr() {
			return regExpr;
		}
		
		protected boolean isRemovePattern() {
			return removePattern;
		}
    }

	private TriggerDetails loadTriggerDetails(final Mail mail) 
	throws DatabaseException
	{
    	/*
    	 * loadTriggerDetailsAction will be executed in a transaction. Under 'normal' circumstances this should never
    	 * result in a ConstraintViolationException. But, when a new user is added at the same time this action is executed
    	 * it can result in a ConstraintViolationException even though no data is written in loadTriggerDetailsAction.
    	 * We therefore retry at max. COMMIT_RETRIES times when a ConstraintViolationException occurs.
    	 */		
		TriggerDetails triggerDetails = actionExecutor.executeTransaction(
                new DatabaseAction<TriggerDetails>()
                {
                    @Override
                    public TriggerDetails doAction(Session session)
                    throws DatabaseException
                    {
                        Session previousSession = sessionManager.getSession();
                    	
                    	sessionManager.setSession(session);
                        
                        try {
                            return loadTriggerDetailsAction(mail);
                        }
                        finally {
                        	sessionManager.setSession(previousSession);                            
                        }
                    }
                }, ACTION_RETRIES);
		
		return triggerDetails;
	}
	
	private TriggerDetails loadTriggerDetailsAction(Mail mail) 
	throws DatabaseException
	{
		TriggerDetails triggerDetails = null; 
		
		try {
			InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
	
	    	if (originator != null)
	    	{
				String userEmail = originator.getAddress();
				
                userEmail = EmailAddressUtils.canonicalizeAndValidate(userEmail, false);
	    		
	    		User user = userWorkflow.getUser(userEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
	    		
	    		UserProperties userProperties = user.getUserPreferences().getProperties();
	    		
	    		triggerDetails = new TriggerDetails();
	    		
	    		triggerDetails.subjectTrigger = userProperties.getSubjectTrigger();
	    		triggerDetails.regExpr = userProperties.isSubjectTriggerRegExpr();
	    		triggerDetails.removePattern = userProperties.isSubjectTriggerRemovePattern();
	    	}
	    	
	    	return triggerDetails;
		}
		catch(MessagingException e) {
			throw new DatabaseException(e);
		} 
		catch (HierarchicalPropertiesException e) {
			throw new DatabaseException(e);
		}
	}
    
    @Override
    public void init()
    {
    	getLogger().info("Initializing matcher: " + getMatcherName());

        /*
         * We must call getCondition even if we not not use the condition because if not matchOnError
         * will not be initialized.
         */
    	getCondition();
    	
        sessionManager = SystemServices.getSessionManager();
        
        userWorkflow = SystemServices.getUserWorkflow();
        
        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);

        assert(actionExecutor != null);
    }

    @Override
    protected Collection<MailAddress> onMatch(Mail mail, Matcher matcher) 
    throws MessagingException, IOException 
    {
    	TriggerDetails triggerDetails = getActivationContext().get(ACTIVATION_CONTEXT_KEY, TriggerDetails.class);
    	
    	Check.notNull(triggerDetails, "triggerDetails");
    	
    	if (triggerDetails.isRemovePattern())
    	{
	    	/*
	    	 * Remove the pattern from the subject.
	    	 */
	    	removePattern(mail, matcher);
    	}
        
        return MailAddressUtils.getRecipients(mail);
    }
    
    @Override
    protected Pattern getHeaderValuePattern(Mail mail)
    throws MessagingException
    {
    	try {
    		Pattern pattern = null;
    		
			TriggerDetails triggerDetails = loadTriggerDetails(mail);
			
			String trigger = null;
			
			if (triggerDetails != null) {
			    trigger = triggerDetails.getSubjectTrigger();
			}
			
			if (trigger != null && trigger.trim().length() > 0)
			{
				if (!triggerDetails.isRegExpr()) 
				{
					/*
					 * If the trigger is not a regular expression we need to escape all characters that are
					 * special for a regular expression
					 */
					trigger = "(?i)" + RegExprUtils.escape(trigger);
				}
				
				try {
					pattern = Pattern.compile(trigger);
				}
				catch(PatternSyntaxException e) {
					logger.warn("The trigger '" + trigger + "' is not a valid pattern. Matcher will not match.");
				}
			}
			
			/*
			 * Place the triggerDetails in the context so we can use it in onMatch
			 */
			getActivationContext().set(ACTIVATION_CONTEXT_KEY, triggerDetails);
			
			return pattern;
		} 
    	catch (DatabaseException e)	{
			throw new MessagingException("Error reading user properties.", e);
		}
    }
}
