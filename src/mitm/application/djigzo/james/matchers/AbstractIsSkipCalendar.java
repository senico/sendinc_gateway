/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;

import javax.mail.MessagingException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

/**
 * Abstract matcher that matches when a user (if sender or receiver depends on the class extending this class) has skip
 * calendar. The matcher condition determines which user property is checked:
 * 
 * - skipSMIME        : checks the isSMIMESkipCalendar user property
 * - skipSMIMESigning : checks the isSMIMESkipSigningCalendar user property
 * - skipSMIMEBoth    : checks the isSMIMESkipCalendar and/or isSMIMESkipSigningCalendar user property
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractIsSkipCalendar extends IsCalendar
{    
    private enum SkipParameter 
    {
        SKIP_SMIME          ("skipSMIME"),
        SKIP_SMIME_SIGNING  ("skipSMIMESigning"),
        SKIP_SMIME_BOTH     ("skipSMIMEBoth");
        
        private String name;
        
        private SkipParameter(String name) {
            this.name = name;
        }
        
        public static SkipParameter fromString(String name)
        {
            for (SkipParameter parameter : SkipParameter.values()) 
            {
                if (parameter.name.equals(name)) {
                    return parameter;
                }
            }
            
            return null;
        }
    };
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    private SkipParameter skipParameter;
    
    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: " + getMatcherName());

        sessionManager = SystemServices.getSessionManager();
                
        userWorkflow = SystemServices.getUserWorkflow();
        
        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
                sessionManager);

        assert(actionExecutor != null);
        
        String condition = getCondition();
        
        skipParameter = SkipParameter.fromString(condition);
        
        if (skipParameter == null) {
            throw new IllegalArgumentException(condition + " is not a valid SkipParameter.");
        }
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("Skip Parameter: ");
        sb.append(skipParameter);
        
        getLogger().info(sb.toString());        
    }
    
    protected Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = new MailAddressMatcher.HasMatchEventHandler()
        {
            @Override
            public boolean hasMatch(final User user)
            throws MessagingException 
            {
                return AbstractIsSkipCalendar.this.hasMatch(user);
            }
        };
        
        MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, 
                actionExecutor, hasMatchEventHandler, ACTION_RETRIES);
        
        return matcher.getMatchingMailAddresses(mailAddresses);
    }

    protected boolean hasMatch(User user) 
    throws MessagingException 
    {
        /*
         * Return true if user needs to skip a calendar
         */
        try {
            UserProperties properties = user.getUserPreferences().getProperties();
            
            switch(skipParameter)
            {
            case SKIP_SMIME         : return properties.isSMIMESkipCalendar();
            case SKIP_SMIME_SIGNING : return properties.isSMIMESkipSigningCalendar();
            case SKIP_SMIME_BOTH    : return properties.isSMIMESkipCalendar() || 
                                                properties.isSMIMESkipSigningCalendar();
            default:
                throw new IllegalArgumentException("Unknown skipParameter: " + skipParameter);
            }
        } 
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error in hasMatch.", e);
        }        
    }

    protected SessionManager getSessionManager() {
        return sessionManager;
    }

    protected UserWorkflow getUserWorkflow() {
        return userWorkflow;
    }

    protected MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
        return messageOriginatorIdentifier;
    }

    protected DatabaseActionExecutor getActionExecutor() {
        return actionExecutor;
    }
    
    @Override
    public abstract Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException;  
}
