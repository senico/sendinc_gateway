/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.ArrayList;

import org.apache.commons.lang.math.NumberUtils;

import net.sourceforge.jeval.ArgumentTokenizer;
import net.sourceforge.jeval.EvaluationConstants;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.Function;
import net.sourceforge.jeval.function.FunctionConstants;
import net.sourceforge.jeval.function.FunctionException;
import net.sourceforge.jeval.function.FunctionHelper;
import net.sourceforge.jeval.function.FunctionResult;

/**
 * JEval function that converts a string to a double. It requires two argument, a string value that will be converted to a double
 * and a double value that will be used  as the default value if the string cannot be converted to a double
 * 
 * @author Martijn Brinkers
 *
 */
public class ToFloatFunction implements Function
{
	@Override
    public String getName() {
		return "toFloat";
	}
	
	/*
	 * Taken from net.sourceforge.jeval.function.FunctionHelper and modified to make it return a double
	 */
	public static ArrayList<?> getOneStringAndOneDouble(final String arguments,
			final char delimiter) 
	throws FunctionException 
	{
		ArrayList<Object> returnValues = new ArrayList<Object>();

		try {
			final ArgumentTokenizer tokenizer = new ArgumentTokenizer(
					arguments, delimiter);

			int tokenCtr = 0;
			
			while (tokenizer.hasMoreTokens()) 
			{
				if (tokenCtr == 0) 
				{
					final String token = tokenizer.nextToken();
					
					returnValues.add(token);
				} 
				else if (tokenCtr == 1) 
				{
					final String token = tokenizer.nextToken().trim();
					returnValues.add(new Double(token));
				} 
				else {
					throw new FunctionException("Invalid values in string.");
				}

				tokenCtr++;
			}
		} catch (Exception e) {
			throw new FunctionException("Invalid values in string.", e);
		}

		return returnValues;
	}
	
	@Override
    public FunctionResult execute(Evaluator evaluator, String arguments)
	throws FunctionException 
	{
		ArrayList<?> values = getOneStringAndOneDouble(arguments, 
				EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);
		
		if (values.size() != 2) {
			throw new FunctionException("A String and a number is required.");
		}
		String stringValue = FunctionHelper.trimAndRemoveQuoteChars((String) values.get(0), 
				evaluator.getQuoteCharacter());
		
		Double value = NumberUtils.toDouble(stringValue, (Double) values.get(1));
		
		return new FunctionResult(value.toString(), FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
	}
}
