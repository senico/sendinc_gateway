/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;
import java.util.Collections;

import javax.mail.MessagingException;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.VariableResolver;
import net.sourceforge.jeval.function.FunctionException;

import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matcher that matches an expression against the mail attributes (@See {@link Mail#getAttribute(String)}.
 * 
 * Usage:
 * 
 * MailAttributeEvaluator=matchOnError=false,#{abc}=='123' | Matches when the mail contains an attribute 'abc' with value '123'
 * 
 * Note: If an attribute value is null or the attribute does not exist the string "null" will be returned. So in order to
 * check for a null or non existing attribute you need to check for null. 
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *  
 * @author Martijn Brinkers
 *
 */
public class MailAttributeEvaluator extends AbstractDjigzoMatcher
{
    private final static Logger logger = LoggerFactory.getLogger(MailAttributeEvaluator.class);

    /*
     * The property value used when the property value is null.
     */
    private static final String NULL_STRING = "null";
    
    /*
     * The expression to evaluate
     */
    private String expression;
    
    /*
     * Variable resolver that gets it's variable values from the mail attributes.
     */
    private static class MailAttributeResolver implements VariableResolver
    {
        private final Mail mail;
        
        public MailAttributeResolver(Mail mail) {
            this.mail = mail;
        }

        @Override
        public String resolveVariable(String variable)
        throws FunctionException 
        {
            Object attributevalue = mail.getAttribute(variable);
            
            /*
             * We cannot return null because it would result in a exception by the Evaluator.
             */
            String value = NULL_STRING;
            
            if (attributevalue != null) {
                value = attributevalue.toString();
            }
            
            /*
             * The result must be single quoted otherwise it is interpreted as a number.
             */
            return "'" + value + "'";
        }
    }
    
    protected VariableResolver getVariableResolver(Mail mail)
    throws MessagingException
    {
        return new MailAttributeResolver(mail);
    }
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public void init()
    {
        expression = getCondition();
                
        if (expression == null) {
            throw new IllegalArgumentException("Expression must be specified.");
        }
            
        StrBuilder sb = new StrBuilder();
        
        sb.append("Expression: ");
        sb.append(expression);
        
        getLogger().info(sb.toString());        
    }
     
    /*
     * Note: Evaluator is no thread safe. 
     */
    private Evaluator getEvaluator()
    {
        Evaluator evaluator = new Evaluator();
                
        return evaluator;
    }
    
    @Override
    public Collection<?> match(Mail mail)
    throws MessagingException 
    {
        boolean error = false;

        try {            
            Evaluator evaluator = getEvaluator();
            
            evaluator.setVariableResolver(getVariableResolver(mail));
            
            if (evaluator.getBooleanResult(expression) == true) {
                return mail.getRecipients();
            }
        }
        catch (MessagingException e) 
        {
        	getLogger().error("MessagingException in getMatchingMailAddresses().", e);

            error = true;
        }
        catch (EvaluationException e) 
        {
        	getLogger().error("expression cannot be evaluated.", e);

            error = true;
        }
        catch(RuntimeException e) 
        {
        	getLogger().error("Unhandled RuntimeException.", e);

            error = true;
        }
        catch(Error e)
        {
        	getLogger().error("Unhandled Error.", e);
            
            error = true;
        }

        Collection<?> matching = Collections.emptyList();

        if (error && isMatchOnError()) {
            matching = mail.getRecipients();
        }

        return matching;            
    }
}
