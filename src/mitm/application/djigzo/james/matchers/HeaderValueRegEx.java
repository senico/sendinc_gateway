/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.regex.Pattern;

import javax.mail.MessagingException;

import mitm.common.util.NameValueUtils;

import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A regular expression matcher that matches the value of a header to the provided pattern.
 * 
 * Usage:
 * 
 * HeaderValueRegEx=matchOnError=false,subject=(?i)test | Matches when the subject contains test (case insensitive)
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public class HeaderValueRegEx extends AbstractHeaderValueRegEx
{
    private final static Logger logger = LoggerFactory.getLogger(HeaderValueRegEx.class);
    
    private String header;
    private Pattern headerValuePattern;
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public void init() 
    {
        getLogger().info("Initializing matcher: " + getMatcherName());

        String condition = getCondition();
        
        if (condition == null || "".equals(condition)) {
            throw new IllegalArgumentException("Condition is missing.");
        }
        
        String[] pair = NameValueUtils.nameValueToPair(condition);
        
        if (pair.length != 2) {
            throw new IllegalArgumentException("Condition is invalid. It should be 'name=value'");            
        }
        
        header = pair[0];
        headerValuePattern = Pattern.compile(pair[1]);
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("Header: ");
        sb.append(header);
        sb.appendSeparator("; ");
        sb.append("pattern: ");
        sb.append(headerValuePattern);
        
        getLogger().info(sb.toString());                
    }

    @Override
    protected String getHeader(Mail mail)
    throws MessagingException
    {
        return header;
    }
    
    @Override
    protected Pattern getHeaderValuePattern(Mail mail)
    throws MessagingException
    {
        return headerValuePattern;
    }
}