/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matcher that matches the recipient against a property read from the sender user properties.
 * 
 * @author Martijn Brinkers
 *
 */
public class RecipientIsSenderProperty extends AbstractContextAwareDjigzoMatcher
{
    private final static Logger logger = LoggerFactory.getLogger(RecipientIsSenderProperty.class);
        
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
	
    /*
     * The key under which we store the activation context
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "BlackBerryRelayEmail";
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
    
    /*
     * The user property of the target recipient
     */
    private String userProperty;

    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: " + getMatcherName());

        sessionManager = SystemServices.getSessionManager();
        
        userWorkflow = SystemServices.getUserWorkflow();
        
        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);

        assert(sessionManager != null);
        assert(userWorkflow != null);
        assert(messageOriginatorIdentifier != null);
        assert(actionExecutor != null);
        
        userProperty = getCondition();

        if (StringUtils.isBlank(userProperty)) {
            throw new MessagingException("userProperty is missing.");
        }
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("userProperty: ");
        sb.append(userProperty);
        
        getLogger().info(sb.toString());        
    }
    
    protected String getBlackBerryRelayEmail(final Mail mail, final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = new MailAddressMatcher.HasMatchEventHandler()
        {
            @Override
            public boolean hasMatch(final User user)
            throws MessagingException 
            {
                String blackBerryRelayEmail;

                try {
                    blackBerryRelayEmail = user.getUserPreferences().getProperties().getProperty(userProperty, false);
                }
                catch (HierarchicalPropertiesException e) {
                    throw new MessagingException("Error getting blackBerryRelayEmail from property: " + userProperty, e);
                }
                
                getActivationContext().set(ACTIVATION_CONTEXT_KEY, blackBerryRelayEmail);
                
                return true;
            }
        };
        
        MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, 
                actionExecutor, hasMatchEventHandler, ACTION_RETRIES);
        
        matcher.getMatchingMailAddresses(mailAddresses);
        
        return getActivationContext().get(ACTIVATION_CONTEXT_KEY, String.class);        
    }
    
    @Override
    public Collection<MailAddress> matchMail(Mail mail) 
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        Collection<MailAddress> matching = new HashSet<MailAddress>();
        
        if (originator != null)
        {
            MailAddress sender = new MailAddress(originator);
            
            String blackBerryRelayEmail = EmailAddressUtils.canonicalize(
                    getBlackBerryRelayEmail(mail, Collections.singleton(sender)));
            
            if (blackBerryRelayEmail != null)
            {
                Collection<MailAddress> recipients = MailAddressUtils.getRecipients(mail);
                
                if (recipients != null)
                {
                    for (MailAddress recipient : recipients)
                    {
                        String email = EmailAddressUtils.canonicalize(recipient.toString());
                        
                        if (blackBerryRelayEmail.equals(email)) {
                            matching.add(recipient);
                        }
                    }
                }
            }
        }
        
        return matching;
    }    
}
