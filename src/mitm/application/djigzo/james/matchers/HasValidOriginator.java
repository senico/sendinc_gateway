/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.mail.EmailAddressUtils;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HasValidOriginator extends AbstractContextAwareDjigzoMatcher
{
	private final static Logger logger = LoggerFactory.getLogger(HasValidOriginator.class);
	
    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    private boolean mustMatch = true;
    
	@Override
	protected Logger getLogger() {
		return logger;
	}
    
	public MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
		return messageOriginatorIdentifier;
	}

    @Override
    public void init() 
    throws MessagingException
    {
    	messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
    	
        String condition = getCondition();

        if (StringUtils.isNotEmpty(condition)) {
        	mustMatch = BooleanUtils.toBoolean(condition);
        }
    }
    
    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
    	InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
    	
    	boolean match = false;
    	
    	/*
    	 * The sender has a valid originator if not null and not equal to invalid email address
    	 */
    	if (originator != null && !EmailAddressUtils.INVALID_EMAIL.equals(originator.getAddress())) {
    		match = true;
    	}

    	if (!mustMatch) {
    		match = !match;
    	}
    	
    	if (match) {
    		return MailAddressUtils.getRecipients(mail);
    	}
    	
    	return null;
    }
}
