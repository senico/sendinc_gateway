/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;

import javax.mail.MessagingException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.PhoneNumber;
import mitm.application.djigzo.james.PhoneNumbers;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseActionRetryEvent;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matcher that matches all recipients with a phone number. The phone numbers will be added to the mail attributes
 * as a map that maps the recipients email address to phone number
 * (@See {@link mitm.application.djigzo.james.DjigzoMailAttributes#setPhoneNumbers(PhoneNumbers)}
 *
 * Usage:
 * 
 * HasPhoneNumber=matchOnError=false | Matches when the recipient has a phone number
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public class HasPhoneNumber extends AbstractContextAwareDjigzoMatcher
{
    private final static Logger logger = LoggerFactory.getLogger(HasPhoneNumber.class);
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
    
    protected static final String ACTIVATION_CONTEXT_KEY = "hasPhoneNumber";
    
    /*
     * Keeps track of Passwords of matching users
     */
    protected static class HasPhoneNumberActivationContext 
    {
        private PhoneNumbers phoneNumbers = new PhoneNumbers();
        
        public PhoneNumbers getPhoneNumbers() {
            return phoneNumbers;
        }
        
        public void clear() {
        	phoneNumbers.clear();
        }
    }
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: " + getMatcherName());

        sessionManager = SystemServices.getSessionManager();
        
        userWorkflow = SystemServices.getUserWorkflow();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);

        assert(actionExecutor != null);
    }
    
    protected Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
    	MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = new MailAddressMatcher.HasMatchEventHandler()
    	{
			@Override
            public boolean hasMatch(final User user)
			throws MessagingException 
			{
				return HasPhoneNumber.this.hasMatch(user);
			}
    	};
    	
    	DatabaseActionRetryEvent retryEvent = new DatabaseActionRetryEvent()
    	{
			@Override
            public void onRetry() {
				handleRetry();
			}
    	};
    	
    	MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, 
    			actionExecutor, hasMatchEventHandler, ACTION_RETRIES, retryEvent);
    	
    	return matcher.getMatchingMailAddresses(mailAddresses);
    }
    
    /*
     * We must clear the HasPhoneNumberActivationContext on a retry
     */
    private void handleRetry()
    {
        HasPhoneNumberActivationContext hasPhoneNumberContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY, 
        		HasPhoneNumberActivationContext.class);
        
        hasPhoneNumberContext.clear();
    }
    
    protected boolean hasMatch(User user)
    throws MessagingException 
    {
        HasPhoneNumberActivationContext hasPhoneNumberContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY, 
        		HasPhoneNumberActivationContext.class);
        
        Check.notNull(hasPhoneNumberContext, "hasPhoneNumberContext");
        
        UserProperties userProperties = user.getUserPreferences().getProperties();
        
        boolean match = false;
        
        String smsPhoneNumber;
        
		try {
			smsPhoneNumber = userProperties.getSMSPhoneNumber();
		} 
		catch (HierarchicalPropertiesException e) {
			throw new MessagingException("Exception getting smsPhoneNumber.", e);
		}

        if (smsPhoneNumber != null)
        {
            PhoneNumber phoneNumber = new PhoneNumber(smsPhoneNumber); 
            
            hasPhoneNumberContext.getPhoneNumbers().put(user.getEmail(), phoneNumber);

            match = true;
        }
        else {
        	logger.debug("User " + user.getEmail() + " has no sms phoneNumber");
        }
        
        return match;
    }

    private void addPhoneNumbersAttribute(Mail mail, PhoneNumbers phoneNumbers)
    {
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        attributes.setPhoneNumbers(phoneNumbers);
    }
    
    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
    	HasPhoneNumberActivationContext context = new HasPhoneNumberActivationContext();

    	getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);
    	
    	Collection<MailAddress> matchingUsers = getMatchingMailAddresses(MailAddressUtils.getRecipients(mail));

    	addPhoneNumbersAttribute(mail, context.getPhoneNumbers());

    	return matchingUsers;
    }
}
