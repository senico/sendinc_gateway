package mitm.application.djigzo.james.matchers;

import java.io.File;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

import java.net.URL;
import java.net.HttpURLConnection;

import javax.mail.MessagingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.apache.james.util.Base64;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * this matcher checks the sender of a message against the configured Sendinc
 * corporate account to verify that it belongs to the list of allowed users
 *
 * @author jon shusta <jon.shusta@mxforce.com>
 */
public class SenderAuthorizedForCorporate extends AbstractDjigzoMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(SenderAuthorizedForCorporate.class);

    private static final File custom_processors_conf = new File(System.getProperty("phoenix.home"), "../conf/james/SAR-INF/custom_processors_config.xml");

    private static final String rest_api_url = "https://rest.sendinc.com/account.json?include_corporate_users=1";

    private static final Set users = new HashSet<String>();

    private static String rest_username = null;

    private static String rest_password = null;

    private static Long lastRefreshTime = 0L;


    /**
     * reads sendinc-transport mailet from custom_processors_config.xml
     * extracts the corporate account credentials for later use
     * overwrites the parent class members rest_username/rest_password, assuming that users is locked
     */
    public static class CustomProcessorsHandler extends DefaultHandler
    {
      private StringBuilder sb = null;
      @Override
      public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException
      {
      }
      @Override
      public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
      {
        sb = new StringBuilder();
      }
      @Override
      public void characters(char[] ch, int start, int length) throws SAXException
      {
        sb.append(ch, start, length);
      }
      @Override
      public void endElement(String uri, String localName, String qName) throws SAXException
      {
        if ("gatewayusername".equals(qName)) {
          rest_username = sb.toString().trim();
          logger.info("set username \"" + rest_username + "\"");
        }
        else if ("gatewayPassword".equals(qName)) {
          rest_password = sb.toString().trim();
          logger.info("set password \"" + rest_password + "\"");
        }
      }
    }


    /**
     */
    @Override
    protected Logger getLogger()
    {
      return logger;
    }


    /**
     */
    @Override
    public void init() throws MessagingException
    {
      super.init();
      if (refreshCache())
        lastRefreshTime = System.currentTimeMillis();
    }


    /**
     */
  @Override
  public Collection<?> match(Mail mail) throws MessagingException
  {
    boolean isAuthorized;

    synchronized (users) {
      final long now = System.currentTimeMillis();
      if (users.size() == 0 || (now - lastRefreshTime) > 300000) {
        if (refreshCache())
          lastRefreshTime = now;
      }
      isAuthorized = users.contains(mail.getSender().toString());
    }
    return isAuthorized
         ? mail.getRecipients()
         : null;
  }


  /**
   * no users cached or cache duration expired
   * pull credentials from conf if they are not already available
   * query REST API for account data
   * parse JSON account data, keep list of corporate users
   * refresh the users cache with the list
   */
  private boolean refreshCache()
  {
    if (retrieveSendincCorporateLogin()) {
      final String response = retrieveSendincCorporateAccountDetails();
      if (null != response) {
        final Set<String> newUsers = parseCorporateUsers(response);
        if (null != newUsers) {
          users.clear();
          users.addAll(newUsers);
          logger.info("Cached " + users.size() + " users for " + rest_username);
          return true;
        }
      }
    }
    logger.warn("Unable to refresh user cache.");
    return false;
  }



  /**
   */
  private Set<String> parseCorporateUsers(final String json)
  {
    try {
      final JSONObject obj = new JSONObject(json);
      final JSONArray list = obj.getJSONObject("account").getJSONArray("corporate_users");
      final Set<String> newUsers = new HashSet<String>();
      for (int i = 0; i < list.length(); i++)
        newUsers.add(list.getString(i));
      return newUsers;
    }
    catch (JSONException ex) {
      logger.error("Problem parsing account JSON: " + ex.getMessage());
    }
    return null;
  }


  /**
   */
  private boolean retrieveSendincCorporateLogin()
  {
    if (rest_username != null && rest_password != null)
      return true;

    try {
      SAXParserFactory.newInstance().newSAXParser().parse(custom_processors_conf, new CustomProcessorsHandler());
    }
    catch (ParserConfigurationException ex) {
      logger.error("Cannot configure SAX parser: " + ex.getMessage());
    }
    catch (SAXException ex) {
      logger.error("Problem parsing XML config: " + ex.getMessage());
    }
    catch (IOException ex) {
      logger.error("Problem parsing XML config: " + ex.getMessage());
    }
    return rest_username != null && rest_password != null;
  }


  /**
   */
  private String retrieveSendincCorporateAccountDetails()
  {
    try {
      final URL resource = new URL(rest_api_url);
      final HttpURLConnection conn = (HttpURLConnection)resource.openConnection();
      conn.setRequestProperty("Accept", "application/json");
      conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
      conn.setRequestProperty("Authorization", "Basic " + Base64.encodeAsString(rest_username + ":" + rest_password));
      conn.setConnectTimeout(5000);
      conn.setRequestMethod("GET");
      conn.connect();
      if (401 != conn.getResponseCode()) {
        final BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
        final byte[] buf = new byte[4096];
        for (;;) {
          final int rsz = bis.read(buf);
          if (rsz < 0) break;
          baos.write(buf, 0, rsz);
        }
        baos.flush();
        return baos.toString();
      }
    }
    catch (Throwable ex) {
    }
    return null;
  }
}
