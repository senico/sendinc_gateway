/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.application.djigzo.james.MailAddressUtils;
import mitm.common.mail.MimeTypes;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This matcher matches if an email is a calendar email (text/calendar). Sometimes a invite is a multipart/alternative
 * message with a text part containing the details of the invite and a text/calendar part. This is also detected as
 * a calendar mail by this matcher.
 * 
 * @author Martijn Brinkers
 *
 */
public class IsCalendar extends AbstractContextAwareDjigzoMatcher
{
    private final static Logger logger = LoggerFactory.getLogger(IsCalendar.class);
    
    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    protected boolean isCalendar(MimeMessage message)
    throws MessagingException
    {
        try {
            if (message == null) {
                return false;
            }

            boolean calendar = false;
            
            calendar = message.isMimeType(MimeTypes.CALENDAR);
            
            if (!calendar && message.isMimeType(MimeTypes.MULTIPART_ALTERNATIVE)) {
                /* 
                 * Check if one of the alternative parts is a calendar
                 */
                MimeMultipart mp = (MimeMultipart) message.getContent();
                
                for (int i = 0; i < mp.getCount(); i++)
                {
                    if (mp.getBodyPart(i).isMimeType(MimeTypes.CALENDAR))
                    {
                        calendar = true;
                        break;
                    }
                }
            }
            
            return calendar;
        }
        catch(IOException e) {
            throw new MessagingException("IOException reading email.", e);
        }
    }
    
    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        MimeMessage message = mail.getMessage();
        
        Collection<MailAddress> recipients;

        if (isCalendar(message)) {
            recipients = MailAddressUtils.getRecipients(mail);
        }
        else {
            recipients = Collections.emptyList();
        }

        return recipients;
    }
}