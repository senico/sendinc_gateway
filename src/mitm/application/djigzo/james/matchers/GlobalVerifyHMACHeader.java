/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import javax.mail.MessagingException;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extension of VerifyHMACHeader that reads the secret from the global preferences.
 * 
 * @author Martijn Brinkers
 *
 */
public class GlobalVerifyHMACHeader extends VerifyHMACHeader
{
    private final static Logger logger = LoggerFactory.getLogger(GlobalVerifyHMACHeader.class);
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    /*
     * Should be true if the value of the global property is encrypted and should be decrypted
     */
    private boolean decrypt;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for getting the secret from the global preferences
     */
    private GlobalPreferencesManager globalPreferencesManager;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
    
    @Override
    public void init()
    throws MessagingException
    {
        super.init();
        
        String[] parameters = getParameters(); 
        
        if (parameters.length > 3) {
            decrypt = Boolean.parseBoolean(StringUtils.trim(parameters[3]));
        }
        
        sessionManager = SystemServices.getSessionManager();

        globalPreferencesManager = SystemServices.getGlobalPreferencesManager();

        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
    }
        
    private byte[] getSecretTransacted()
    throws DatabaseException
    {
        try {
            return MiscStringUtils.toAsciiBytes(globalPreferencesManager.getGlobalUserPreferences().
                    getProperties().getProperty(getSecret(), decrypt));
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
        }
    }
    
    @Override
    protected byte[] getSecret(Mail mail)
    throws MessagingException
    {
        try {
            return actionExecutor.executeTransaction(
                new DatabaseAction<byte[]>()
                {
                    @Override
                    public byte[] doAction(Session session)
                    throws DatabaseException
                    {
                        Session previousSession = sessionManager.getSession();
                        
                        sessionManager.setSession(session);
                        
                        try {
                            return getSecretTransacted();
                        }
                        finally {
                            sessionManager.setSession(previousSession);                            
                        }
                    }
                }, ACTION_RETRIES);
        }
        catch(DatabaseException e) {
            throw new MessagingException("Error in handleRecipientsAction.", e);
        }
        catch(HibernateException e) {
            throw new MessagingException("HibernateException.", e);
        }
    }
}
