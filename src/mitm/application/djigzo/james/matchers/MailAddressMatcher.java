/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;
import java.util.HashSet;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionRetryEvent;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.mailet.MailAddress;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for matchers to make it easier to create matchers that use the User objects for determining
 * if a mail matches or not
 * 
 * @author Martijn Brinkers
 *
 */
public class MailAddressMatcher 
{
    private final static Logger logger = LoggerFactory.getLogger(MailAddressMatcher.class);
    
    /*
     * manages database sessions
     */
    private final SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private final UserWorkflow userWorkflow;
    
    /*
     * Used to execute database actions in a transaction
     */
    private final DatabaseActionExecutor actionExecutor;

    /*
     * The event instance to call to see if there is a match
     */
    private final HasMatchEventHandler hasMatchEventHandler;

    /*
     * The event to call when a retry is done
     */
    private final DatabaseActionRetryEvent retryEvent;
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs.
     */
    private final int actionRetries;
    
    /*
     * Event handler used to check for a match
     */
    public static interface HasMatchEventHandler
    {
        public boolean hasMatch(User user)
        throws MessagingException;
    }

    public MailAddressMatcher(SessionManager sessionManager, UserWorkflow userWorkflow, 
    		DatabaseActionExecutor actionExecutor, HasMatchEventHandler hasMatchEventHandler, int actionRetries)
    {
    	this(sessionManager, userWorkflow, actionExecutor, hasMatchEventHandler, actionRetries, null);
    }
    
    public MailAddressMatcher(SessionManager sessionManager, UserWorkflow userWorkflow, 
    		DatabaseActionExecutor actionExecutor, HasMatchEventHandler hasMatchEventHandler, int actionRetries,
    		DatabaseActionRetryEvent retryEvent)
    {
    	Check.notNull(sessionManager, "sessionManager");
    	Check.notNull(userWorkflow, "userWorkflow");
    	Check.notNull(actionExecutor, "actionExecutor");
    	Check.notNull(hasMatchEventHandler, "hasMatchEventHandler");
    	
    	this.sessionManager = sessionManager;
    	this.userWorkflow = userWorkflow;
    	this.actionExecutor = actionExecutor;
    	this.hasMatchEventHandler = hasMatchEventHandler;
    	this.actionRetries = actionRetries;
    	this.retryEvent = retryEvent;
    }
    
    public Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        Collection<MailAddress> matching = null;
        
        try {
        	/*
        	 * getMatchingMailAddressesAction will be executed in a transaction. Under 'normal' circumstances this should never
        	 * result in a ConstraintViolationException. But, when a new user is added at the same time this action is executed
        	 * it can result in a ConstraintViolationException even though no data is written in getMatchingMailAddressesAction.
        	 * We therefore retry at max. COMMIT_RETRIES times when a ConstraintViolationException occurs.
        	 */
            matching = actionExecutor.executeTransaction(
                new DatabaseAction<Collection<MailAddress>>()
                {
                    @Override
                    public Collection<MailAddress> doAction(Session session)
                    throws DatabaseException
                    {
                        Session previousSession = sessionManager.getSession();

                    	sessionManager.setSession(session);
                        
                        try {
                            return getMatchingMailAddressesAction(mailAddresses);
                        }
                        finally {
                        	sessionManager.setSession(previousSession);                            
                        }
                    }
                }, actionRetries, retryEvent);
        }
        catch(DatabaseException e) {
            throw new MessagingException("Error in getMatchingMailAddressesAction.", e);
        }
        catch(HibernateException e) {
            throw new MessagingException("HibernateException.", e);
        }
        
        return matching;
    }
    
    /**
     * Called for each recipient.
     */
    private boolean hasMatch(User user)
    throws MessagingException
    {
    	return hasMatchEventHandler.hasMatch(user);
    }
            
    private Collection<MailAddress> getMatchingMailAddressesAction(Collection<MailAddress> mailAddresses)
    throws DatabaseException
    {
        Collection<MailAddress> matching = new HashSet<MailAddress>();
        
        try {
            for (MailAddress mailAddress : mailAddresses)
            {
                if (mailAddress != null)
                {
                    InternetAddress emailAddress = mailAddress.toInternetAddress();
                    
                    /*
                     * We will only accept valid email addresses. If an email address is invalid it will be
                     * converted to EmailAddressUtils.INVALID_EMAIL
                     */
                    String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(emailAddress.getAddress(), false);
                    
                    if (validatedEmail != null)
                    {
                        User user;
                        
						try {
							user = userWorkflow.getUser(validatedEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
						} 
						catch (HierarchicalPropertiesException e) {
							throw new DatabaseException(e);
						}
                
                        if (hasMatch(user)) {
                            matching.add(mailAddress);
                        }
                    }
                    else {
                        logger.debug(emailAddress + " is not a valid email address.");
                    }
                }
            }
        }
        catch(MessagingException e) {
            throw new DatabaseException(e);
        } 
        
        return matching;
    }
}
