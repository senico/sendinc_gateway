/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.ArrayList;

import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;

import net.sourceforge.jeval.EvaluationConstants;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.Function;
import net.sourceforge.jeval.function.FunctionConstants;
import net.sourceforge.jeval.function.FunctionException;
import net.sourceforge.jeval.function.FunctionHelper;
import net.sourceforge.jeval.function.FunctionResult;

/**
 * JEval function that returns true if the input is a certain mime type.
 * 
 * usage:
 * 
 * isMIMEType('text/xml', 'text/*')
 * 
 * @author Martijn Brinkers
 *
 */
public class IsMIMETypeFunction implements Function
{
	@Override
    public String getName() {
		return "isMIMEType";
	}

	@Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
	throws FunctionException
	{
	    String result = EvaluationConstants.BOOLEAN_STRING_FALSE;

	    ArrayList<?> strings = FunctionHelper.getStrings(arguments, 
	            EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

	    if (strings.size() != 2) {
	        throw new FunctionException("Two string arguments are required.");
	    }

	    try {
	        String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
	                (String) strings.get(0), evaluator.getQuoteCharacter());
	        
	        String argumentTwo = FunctionHelper.trimAndRemoveQuoteChars(
	                (String) strings.get(1), evaluator.getQuoteCharacter());

	        ContentType contentType = new ContentType(argumentOne);
	        
	        if (contentType.match(argumentTwo)) { 
	            result = EvaluationConstants.BOOLEAN_STRING_TRUE;
	        }
	    } 
	    catch (FunctionException e) {
	        throw new FunctionException(e.getMessage(), e);
	    }
        catch (ParseException e) {
            throw new FunctionException(e.getMessage(), e);
        } 

	    return new FunctionResult(result, FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
	}
}