/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;

import mitm.application.djigzo.EncryptionRecipientSelector;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.Certificates;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.james.UserPersister;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseActionRetryEvent;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

public abstract class AbstractHasCertificates extends AbstractContextAwareDjigzoMatcher
{
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
	
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    /*
     * Used for the selection of encryption certificates for particular user(s).
     */
    private EncryptionRecipientSelector encryptionRecipientSelector;
    
    protected static final String ACTIVATION_CONTEXT_KEY = "certificates";
    
    /*
     * Keeps track of certificates and users that have matched.
     */
    protected static class HasCertificateActivationContext
    {
        /*
         * Collection of all the encryption certificates for all users 
         */
        private Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        /*
         * All the users with valid encryption certificate(s)
         */
        private Set<User> users = new HashSet<User>();

        /*
         * All the users for which the user should be persisted
         */
        private Set<User> usersToPersist = new HashSet<User>();

        public Set<X509Certificate> getCertificates() {
            return certificates;
        }
        
        public Set<User> getUsers() {
            return users;
        }

        public Set<User> getUsersToPersist() {
            return usersToPersist;
        }

        public void clear()
        {
        	certificates.clear();
        	users.clear();
        	usersToPersist.clear();
        }
    }
    
    @Override
    public void init() 
    {
    	getLogger().info("Initializing matcher: " + getMatcherName());

        sessionManager = SystemServices.getSessionManager();
        
        userWorkflow = SystemServices.getUserWorkflow();
        
        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);

        assert(actionExecutor != null);

        encryptionRecipientSelector = SystemServices.getEncryptionRecipientSelector();
    }
    
    protected Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
    	MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = new MailAddressMatcher.HasMatchEventHandler()
    	{
			@Override
            public boolean hasMatch(final User user)
			throws MessagingException 
			{
				return AbstractHasCertificates.this.hasMatch(user);
			}
    	};
    	
    	DatabaseActionRetryEvent retryEvent = new DatabaseActionRetryEvent()
    	{
			@Override
            public void onRetry() {
				handleRetry();
			}
    	};
    	
    	MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, 
    			actionExecutor, hasMatchEventHandler, ACTION_RETRIES, retryEvent);
    	
    	return matcher.getMatchingMailAddresses(mailAddresses);
    }

    /*
     * We must clear the HasCertificateActivationContext on a retry
     */
    private void handleRetry()
    {
        HasCertificateActivationContext context = getActivationContext().get(ACTIVATION_CONTEXT_KEY, 
        		HasCertificateActivationContext.class);
        
        context.clear();
    }
    
    protected boolean hasMatch(User user)
    throws MessagingException
    {
        HasCertificateActivationContext context = getActivationContext().get(ACTIVATION_CONTEXT_KEY, 
        		HasCertificateActivationContext.class);
        
        Check.notNull(context, "context");
        
        Collection<X509Certificate> certificates = context.getCertificates();
        
        Collection<User> encryptionUsers;
        
		try {
			encryptionUsers = encryptionRecipientSelector.select(Collections.singleton(user), 
					certificates);
		} 
		catch (HierarchicalPropertiesException e) {
			throw new MessagingException("Exception while selecting recipients.", e);
		}
        
		context.getUsers().addAll(encryptionUsers);
		
		for (User encryptionUser : encryptionUsers)
		{
		    try {
		        /*
		         * Persist the recipient if the properties of the user say so
		         */
                if (encryptionUser.getUserPreferences().getProperties().isSMIMEAddUser()) {
                    context.getUsersToPersist().add(encryptionUser);
                }
            } 
		    catch (HierarchicalPropertiesException e)
		    {
		        /*
		         * log and ignore
		         */
		        getLogger().error("Error getting user properties", e);
            }
		}
		
        return encryptionUsers.size() > 0;
    }

    @Override
    public final Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException 
    {
        HasCertificateActivationContext context = new HasCertificateActivationContext();

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);

        Collection<MailAddress> matching = getMatchingMailAddresses(getMailAddresses(mail));
                
        addCertificatesProperty(mail, context);

        makeNonPersistentUsersPersistent(context.getUsersToPersist());
        
        return matching;
    }

    /*
     * Adds certificates of all matching users to the mail attributes.
     */
    private void addCertificatesProperty(Mail mail, HasCertificateActivationContext context)
    {
        Set<X509Certificate> certificates = context.getCertificates();
        
        if (certificates.size() > 0) 
        {
            /*
             * Add the certificates as a serializable attribute to the mail.
             */
            Certificates serializableCertificates = new Certificates(certificates);
            
            DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
            
            mailAttributes.setCertificates(serializableCertificates);
        }
    }
    
    private void makeNonPersistentUsersPersistent(Collection<User> encryptionUsers)
    {        
        UserPersister userPersister = new UserPersister(getUserWorkflow(), getSessionManager(), 
                getActionExecutor());
        
        userPersister.tryToMakeNonPersistentUsersPersistent(encryptionUsers);
    }

    protected abstract Collection<MailAddress> getMailAddresses(Mail mail)
    throws MessagingException;
    
	protected SessionManager getSessionManager() {
		return sessionManager;
	}

	protected UserWorkflow getUserWorkflow() {
		return userWorkflow;
	}

	protected MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
		return messageOriginatorIdentifier;
	}

	protected DatabaseActionExecutor getActionExecutor() {
		return actionExecutor;
	}

	protected EncryptionRecipientSelector getEncryptionRecipientSelector() {
		return encryptionRecipientSelector;
	}
}
