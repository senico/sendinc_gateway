/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.MailAddressUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matcher that matches all recipients with valid public certificates. Recipients with valid public certificates 
 * a user instance is persisted to the database if the init parameter is true. The certificates of the matching user 
 * are added as a Mail attribute (See {@link DjigzoMailAttributes#setCertificates()}) to be used by other 
 * mailets (for example the SMIMEEncrypt mailet).
 * 
 * If an unhandled RuntimeException is thrown by the matcher this matcher does not match any recipient.
 * 
 * Usage:
 * 
 * RecipientHasCertificates=matchOnError=false | Matches when the recipient has valid certificates
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public class RecipientHasCertificates extends AbstractHasCertificates
{
    private final static Logger logger = LoggerFactory.getLogger(RecipientHasCertificates.class);
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public Collection<MailAddress> getMailAddresses(Mail mail)
    throws MessagingException 
    {
    	return MailAddressUtils.getRecipients(mail);
    }
}