/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.Collection;
import java.util.Collections;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.james.MailAddressUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract Matcher that matches an expression against the the senders users properties (@See {@link User#getUserPreferences()}.
 * 
 * Usage:
 * 
 * SenderEvaluateUserProperty=matchOnError=false,#{p1}=='v1' && #{p2}=='v2' | Matches when the sender has a property 'p1'
 * 		with value 'v1' and a property 'p2' with value 'v2'
 * 
 * Note: If an attribute value is null or the attribute does not exist the string "null" will be returned. So in order to
 * check for a null or non existing attribute you need to check for null. 
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderEvaluateUserProperty extends AbstractEvaluateUserProperty
{
    private final static Logger logger = LoggerFactory.getLogger(SenderEvaluateUserProperty.class);
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {       
    	InternetAddress originator = getMessageOriginatorIdentifier().getOriginator(mail);

    	if (originator != null)
    	{
    		MailAddress mailAddress = new MailAddress(originator);

    		Collection<MailAddress> matchingUsers = getMatchingMailAddresses(mail, Collections.singleton(mailAddress));

    		if (matchingUsers != null && matchingUsers.size() > 0) 
    		{
    			return MailAddressUtils.getRecipients(mail);
    		}
    	}

        return Collections.emptyList();
    }
}
