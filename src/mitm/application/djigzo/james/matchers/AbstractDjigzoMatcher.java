/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.mailet.GenericMatcher;
import org.slf4j.Logger;

/**
 * Extension of GenericMatcher that provides some general matching functionality 
 * for Djigzo matchers.
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractDjigzoMatcher extends GenericMatcher
{
    private final static String CONDITION_REG_EXPR = "(?i)^\\s*matchOnError\\s*=\\s*(true|false)\\s*(?:,(.*)|)"; 
    
    /*
     * Example patterns:
     * 1) matchOnError=True, other condition
     * 2) MatchOnError = true
     * 3) MatchOnError = faLse,
     */
    private final Pattern conditionPattern = Pattern.compile(CONDITION_REG_EXPR);
    
    /*
     * True if the matcher should match all recipients on error, false if nothing should match.
     */
    private boolean matchOnError;
    
    protected boolean isMatchOnError() {
        return matchOnError;
    }
    
    protected abstract Logger getLogger();
    
    @Override
    public final String getCondition()
    {
        String condition = super.getCondition();

        if (condition == null) {
            throw new IllegalArgumentException("Condition does not match: " + CONDITION_REG_EXPR);
        }
        /*
         * condition must be xml unescaped.
         */
        condition = StringEscapeUtils.unescapeXml(condition);

        /*
         * Remove the matchOnErrorPart
         */
        Matcher matcher = conditionPattern.matcher(condition);
        
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Condition does not match: " + CONDITION_REG_EXPR);
        }
        
        matchOnError = Boolean.parseBoolean(matcher.group(1));
        
        condition = matcher.group(2);
        
        if (condition == null) {
            condition = "";
        }
        
        return condition.trim();
    }
}
