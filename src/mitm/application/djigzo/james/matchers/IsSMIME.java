/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import javax.mail.MessagingException;

import mitm.common.security.smime.SMIMEType;
import mitm.common.security.smime.SMIMEUtils;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Matcher that matches when the message is a S/MIME message of the provided type.
 * 
 * Usage:
 * 
 * IsSMIME=matchOnError=false            | Matches on all S/MIME types
 * IsSMIME=matchOnError=false,none       | Matches when the message is not S/MIME
 * IsSMIME=matchOnError=false,signed     | Matches on S/MIME signed types
 * IsSMIME=matchOnError=false,encrypted  | Matches on S/MIME encrypted types
 * IsSMIME=matchOnError=false,compressed | Matches on S/MIME compressed types
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *  
 * @author Martijn Brinkers
 *
 */
public class IsSMIME extends AbstractDjigzoMatcher
{
    private final static Logger logger = LoggerFactory.getLogger(IsSMIME.class);
    
    /*
     * The S/MIME type (signed, encrypted etc.) the mail should be to match
     */
    private SMIMEType conditionType;

    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    @Override
    public void init() 
    {
        getLogger().info("Initializing matcher: " + getMatcherName());

        String condition = getCondition();
        
        if (!"".equals(condition)) 
        {
            conditionType = SMIMEType.fromName(condition);
            
            if (conditionType == null) {
                throw new IllegalArgumentException(condition + " is not a valid SMIME type.");
            }
        }
    }

    @Override
    public Collection<?> match(Mail mail)
    {   
        boolean error = false;
        
        try {
            SMIMEType messageType = SMIMEUtils.getSMIMEType(mail.getMessage());

            /*
             * check if conditionType matches the type of the message or of conditionType is null
             * that the message is a S/MIME message (does not matter what type of S/MIME message)
             */
            if ((conditionType != null && conditionType == messageType) || 
                (conditionType == null && messageType != SMIMEType.NONE))
            {
                return mail.getRecipients();
            }
        }
        catch (MessagingException e) 
        {
        	getLogger().error("Message cannot be read.", e);
            
            error = true;
        }
        catch (IOException e) 
        {
        	getLogger().error("Message cannot be read.", e);
            
            error = true;
        }
        catch(RuntimeException e) 
        {
        	getLogger().error("Unhandled RuntimeException.", e);
            
            error = true;
        }
        catch(Error e)
        {
        	getLogger().error("Unhandled Error.", e);
            
            error = true;
        }

        Collection<?> matching = Collections.emptyList();
        
        if (error && isMatchOnError()) {
            matching = mail.getRecipients();
        }
        
        return matching;
    }
}