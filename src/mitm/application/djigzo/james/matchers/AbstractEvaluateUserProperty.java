/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import javax.mail.MessagingException;

import mitm.application.djigzo.PropertyRegistry;
import mitm.application.djigzo.User;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import net.sourceforge.jeval.VariableResolver;
import net.sourceforge.jeval.function.FunctionException;

import org.apache.mailet.Mail;
import org.slf4j.Logger;

/**
 * Abstract Matcher that matches an expression against the the users properties (@See {@link User#getUserPreferences()}.
 * 
 * Note: If an attribute value is null or the attribute does not exist the string "null" will be returned. So in order to
 * check for a null or non existing attribute you need to check for null. 
 * 
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractEvaluateUserProperty extends AbstractEvaluateUser
{
    /*
     * The property value used when the property value is null.
     */
    private static final String NULL_STRING = "null";
    
    /*
     * Variable resolver that gets it's variable values from the user properties.
     */
    private static class UserPropertyResolver implements VariableResolver
    {
    	private final MailVariableResolver mailVariableResolver;
    	
    	private final User user;
        
        /*
         * if true it is checked whether the property is a registered property 
         */
        private final boolean checkForRegisteredProperty;
        private final Logger logger;
        
        public UserPropertyResolver(Mail mail, User user, boolean checkForRegisteredProperty, Logger logger) 
        {
        	Check.notNull(mail, "mail");
        	Check.notNull(user, "user");
        	Check.notNull(logger, "logger");
        	
            this.user = user;
            this.checkForRegisteredProperty = checkForRegisteredProperty;
            this.logger = logger;
            
            mailVariableResolver = new MailVariableResolver(mail); 
        }

        @Override
        public String resolveVariable(String variable)
        throws FunctionException 
        {
            /*
             * First try to see if it's a mail property
             */
            String value = mailVariableResolver.resolveVariable(variable);
            
            if (value == null)
            {
            	/*
            	 * It's not a mail property so see if it's a user property
            	 */
				try {
					/*
					 * check if the property is a known property and if not issue a warning 
					 */
					if (checkForRegisteredProperty) 
					{
						if (!PropertyRegistry.getInstance().isRegisteredProperty(variable)) {
							logger.warn("*** Unregistered property: " + variable + "***");
						}
					}				
					
					/*
					 * TODO: encrypted properties are not decrypted. We should add
					 * a decrypt method to JEval so users can decrypt the property if needed
					 */
					value = user.getUserPreferences().getProperties().getProperty(variable, 
							false /* no decrypt */);
				} 
				catch (HierarchicalPropertiesException e) {
					throw new FunctionException(e);
				}

				/*
	             * We need cannot return null because it would result in a exception by the Evaluator.
	             */
	            if (value == null) {
	                value = NULL_STRING;
	            }
	            
	            /*
	             * The result must be single quoted otherwise it is interpreted as a number.
	             */
	            return "'" + value + "'";
            }
            
            return value;
        }
    }
    
    @Override
    protected VariableResolver getVariableResolver(Mail mail, User user)
    throws MessagingException
    {
        return new UserPropertyResolver(mail, user, isCheckForRegisteredProperty(), getLogger());
    }
    
    protected boolean isCheckForRegisteredProperty() {
    	return true;
    }
}
