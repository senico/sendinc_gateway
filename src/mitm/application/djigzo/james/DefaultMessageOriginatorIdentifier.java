/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.EmailAddressUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

/**
 * This implementation of MessageOriginatorIdentifier first checks if there is a from header and if
 * the from header is a valid email address the from is returned. If there is more than one from header 
 * the first valid one is returned. If there is no from header or the from header is not valid the 
 * envelope sender is returned instead. If from and sender are invalid null is returned.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultMessageOriginatorIdentifier implements MessageOriginatorIdentifier
{
    @Override
    public InternetAddress getOriginator(final Mail mail)
    throws MessagingException
    {
        InternetAddress originator = getOriginator(mail.getMessage(), false);
        
        /*
         * If there is not a valid from we will use the enveloped sender.
         */
        if (originator == null) 
        {
            MailAddress sender = mail.getSender();
            
            if (sender != null) {
                originator = sender.toInternetAddress();
            }
            
            if (originator == null) {
                originator = new InternetAddress(EmailAddressUtils.INVALID_EMAIL);
            }
        }
                
        return originator;
    }
    
    @Override
    public InternetAddress getOriginator(MimeMessage message)
    throws MessagingException
    {
        return getOriginator(message, true /* return INVALID_EMAIL if null */);
    }
    
    private InternetAddress getOriginator(MimeMessage message, boolean dummyIfNull)
    throws MessagingException
    {
        InternetAddress originator = null;
        
        if (message != null)
        {
            String[] froms = message.getHeader("from");
            
            if (froms != null) 
            {
                /*
                 * If there are multiple from headers use the first valid one.
                 */
                for (String from : froms) 
                {
                    /*
                     * The from will be validated. If from is not a valid email address EmailAddressUtils.INVALID_EMAIL will
                     * be returned. This is required because we want to have a user with properties (from the global properties)
                     * even if the user email is not valid.
                     */
                    from = EmailAddressUtils.canonicalizeAndValidate(from, false);
                    
                    if (from != null)
                    {
                        originator = new InternetAddress(from);
                        
                        break;
                    }
                }
            }
        }
        
        if (dummyIfNull && originator == null) {
            return new InternetAddress(EmailAddressUtils.INVALID_EMAIL);
        }
        
        return originator;
    }   
}
