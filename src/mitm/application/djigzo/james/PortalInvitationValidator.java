/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.util.Base32;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.lang.StringUtils;

/**
 * This class is used to validate whether a portal account is allowed to be created or not. This is 
 * done by calculating the hmac of certain parameter values.
 * 
 * Note: This class is *not* thread safe
 * 
 * @author Martijn Brinkers
 *
 */
public class PortalInvitationValidator
{
    /*
     * Some unique string to make sure that the HMAC calculation never clashes
     * with some other validator
     */
    private final static byte[] UNIQUE_ID = MiscStringUtils.toUTF8Bytes("invite");
    
    /*
     * The hmac algorithm. 
     */
    private final static String algorithm = "HmacSHA1";
    
    /*
     * The email address of the account
     */
    private String email;
    
    /*
     * The timestamp (can be used to check whether the link is still valid) 
     */
    private Long timestamp;
    
    /**
     * Thrown when there is a problem calculating the MAC or if some parameters are missing
     */
    public static class ValidatorException extends Exception
    {
        private static final long serialVersionUID = -5918351258350255531L;

        public ValidatorException(String message) {
            super(message);
        }

        public ValidatorException(Throwable cause) {
            super(cause);
        }

        public ValidatorException(String message, Throwable cause) {
            super(message, cause);
        }
    }
    
    
    public void setEmail(String email) {
        this.email = EmailAddressUtils.canonicalize(email);
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
    private Mac createMAC(String key)
    throws ValidatorException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        try {
            Mac mac = securityFactory.createMAC(algorithm);
            
            SecretKeySpec keySpec = new SecretKeySpec(MiscStringUtils.toUTF8Bytes(key), "raw");
            
            mac.init(keySpec);
            
            return mac;
        } 
        catch (NoSuchAlgorithmException e) {
            throw new ValidatorException(e);
        } 
        catch (NoSuchProviderException e) {
            throw new ValidatorException(e);
        } 
        catch (InvalidKeyException e) {
            throw new ValidatorException(e);
        }
    }
    
    /**
     * returns the calculated MAC
     */
    public String calulateMAC(String key)
    throws ValidatorException
    {
        Check.notNull(key, "key");
        
        if (StringUtils.isEmpty(email)) {
            throw new ValidatorException("email is not set");
        }

        if (timestamp == null) {
            throw new ValidatorException("timestamp is not set");
        }
        
        Mac mac = createMAC(key);
        
        mac.update(UNIQUE_ID);
        mac.update(MiscStringUtils.toUTF8Bytes(email));
        mac.update(timestamp.byteValue());
    
        byte[] hmac = mac.doFinal();
        
        return Base32.encode(hmac);
    }
}
