/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.Collection;

import mitm.common.dlp.PolicyViolation;

/**
 * Type safe access to serializable Attributes stored in a James mail object.
 * 
 * @author Martijn Brinkers
 *
 */
public interface DjigzoMailAttributes
{
    /**
     * Collection of X509Certificates used by S/MIME encryption mailet.
     */
    public Certificates getCertificates();
    public void setCertificates(Certificates certificates);
    
    /**
     * Encrypted password used to encrypt a message when single password mode is used. The password should be 
     * decryptable by the system encryptor.
     */
    public byte[] getEncryptedPassword();
    public void setEncryptedPassword(byte[] encryptedPassword);

    /**
     * Can be used to identify which password was used to encrypt the message. 
     */
    public String getPasswordID();
    public void setPasswordID(String passwordID);
    
    /**
     * Passwords is a map from email address to password used by encryption mailet to encrypt pdf's etc. when multiple 
     * password mode is used.
     */
    public Passwords getPasswords();
    public void setPasswords(Passwords passwords);
    
    /**
     * Sets the email to phone number mapping.
     */
    public PhoneNumbers getPhoneNumbers();
    public void setPhoneNumbers(PhoneNumbers phoneNumbers);
    
    /**
     * A general String which can be used to store messages etc. in the mail item.
     */
    public String getMessage();
    public void setMessage(String message);
    
    /**
     * MailID is used as a unique identifier of the mail. This is just used for tracking purposes 
     * (tracking the mail flow through James) 
     */
    public String getMailID();
    public void setMailID(String id);
    
    /**
     * Some matchers/mailets (like the subject trigger) can modify the subject.  
     */
    public String getOriginalSubject();
    public void setOriginalSubject(String subject);
    
    /**
     * The policy violations that are detected by the policy checker
     */
    public Collection<PolicyViolation> getPolicyViolations();
    public void setPolicyViolations(Collection<PolicyViolation> violations);
    
    /**
     * The error message when the policy checkers fails because of an error
     */
    public String getPolicyViolationErrorMessage();
    public void setPolicyViolationErrorMessage(String message);

    /**
     * The ID under which the mail item was stored in the mail repository (for example in quarantine)
     */
    public String getMailRepositoryID();
    public void setMailRepositoryID(String id);
    
    /**
     * Map from email address to client secret
     */
    public ClientSecrets getClientSecrets();
    public void setClientSecrets(ClientSecrets clientSecrets);
    
    /**
     * Map from email address to Invitation details
     */
    public PortalInvitations getPortalInvitations();
    public void setPortalInvitations(PortalInvitations invitations);    
    
    /**
     * The headers that are protected by embedding them in the encrypted and/or signed CMS
     */
    public String[] getProtectedHeaders();
    public void setProtectedHeaders(String[] protectedHeaders);
    
    /**
     * Tags that are added to the message subject when a message is handled by the S/MIME handler.
     */
    public SecurityInfoTags getSecurityInfoTags();
    public void setSecurityInfoTags(SecurityInfoTags tags);
}
