/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.smtpserver;

import java.util.ArrayList;
import java.util.List;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.smtpserver.CommandHandler;
import org.apache.james.smtpserver.SMTPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XForwardEhloCmdHandler implements CommandHandler, Configurable
{
    private final static Logger logger = LoggerFactory.getLogger(XForwardEhloCmdHandler.class);
    
    /*
     * The name of the command handled by the command handler
     */
    private final static String COMMAND_NAME = "EHLO";

    /*
     * advertise XForward support.
     */
    private boolean xForward = false;

    /*
     * advertise XClient support.
     */
    private boolean xClient = false;

    /*
     * True if dsn is enabled
     */
    private boolean dsn;
    
    @Override
    public void configure(Configuration handlerConfiguration)
    throws ConfigurationException
    {
        logger.info("Configuring CommandHandler");
        
        Configuration config = handlerConfiguration.getChild("xForward", false);
        
        if(config != null) {
            xForward = config.getValueAsBoolean();
        }

        config = handlerConfiguration.getChild("xClient", false);
        
        if(config != null) {
            xClient = config.getValueAsBoolean();
        }

        config = handlerConfiguration.getChild("dsn", false);
        
        if(config != null) {
            dsn = config.getValueAsBoolean();
        }
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("xForward: ");
        sb.append(xForward);
        sb.appendSeparator("; ");
        sb.append("xClient: ");
        sb.append(xClient);
        sb.appendSeparator("; ");
        sb.append("dsn: ");
        sb.append(dsn);
        
        logger.info(sb.toString());
    }
    
    @Override
    public void onCommand(SMTPSession session) {
        doEHLO(session, session.getCommandArgument());
    }
    
    @SuppressWarnings("unchecked")
	private void doEHLO(SMTPSession session, String argument)
    {
        StringBuffer responseBuffer = session.getResponseBuffer();
    	
        session.resetState();
        
        session.getState().put(SMTPSession.CURRENT_HELO_MODE, COMMAND_NAME);

        List<String> extensions = new ArrayList<String>();

        extensions.add(new StringBuffer(session.getConfigurationData().getHelloName())
            .append(" Hello ")
            .append(argument)
            .append(" (")
            .append(session.getRemoteHost())
            .append(" [")
            .append(session.getRemoteIPAddress())
            .append("])").toString());

        // Extension defined in RFC 1870
        long maxMessageSize = session.getConfigurationData().getMaxMessageSize();
        
        if (maxMessageSize > 0) {
        	extensions.add("SIZE " + maxMessageSize);
        }

        if (session.isAuthRequired()) {
        	extensions.add("AUTH LOGIN PLAIN");
        	extensions.add("AUTH=LOGIN PLAIN");
        }

        extensions.add("PIPELINING");
        extensions.add("ENHANCEDSTATUSCODES");

        /*
         * We don't want 8-bit because we always want 7-bit when signing messages
         */
        /* extensions.add("8BITMIME"); */

        if (xForward) {
        	extensions.add("XFORWARD NAME ADDR");                
        }

        if (xClient) {
        	extensions.add("XCLIENT NAME ADDR");                
        }

        if (dsn) {
            extensions.add("DSN");                
        }
        
        for (int i = 0; i < extensions.size(); i++) 
        {
            if (i == extensions.size() - 1)
            {
                responseBuffer.append("250 ");
                responseBuffer.append((String) extensions.get(i));
                session.writeResponse(session.clearResponseBuffer());
            } 
            else {
                responseBuffer.append("250-");
                responseBuffer.append((String) extensions.get(i));
                session.writeResponse(session.clearResponseBuffer());
            }
        }
    }
}
