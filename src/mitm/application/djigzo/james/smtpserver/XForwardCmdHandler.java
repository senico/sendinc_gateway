/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.smtpserver;

import java.io.IOException;

import mitm.common.mail.XTextEncoder;
import mitm.common.util.NameValueIterator;

import org.apache.avalon.framework.logger.AbstractLogEnabled;
import org.apache.james.smtpserver.CommandHandler;
import org.apache.james.smtpserver.SMTPSession;

/**
 * This command handler handles Postfix XFORWARD extended SMTP commands. Currently only ADDR and HOST is supported.
 * 
 * See: http://www.postfix.org/XFORWARD_README.html
 * 
 * @author Martijn Brinkers
 *
 */
public class XForwardCmdHandler extends AbstractLogEnabled implements CommandHandler 
{

    @Override
    public void onCommand(SMTPSession session) 
    {
        try{
            doXFoward(session, session.getCommandArgument());
        } 
        catch (Exception e) {
            getLogger().error("Exception occured in XFoward handler.", e);
        }
        
        /*
         * Always return successful.
         */
        session.writeResponse("250 Ok");
    }

    private void handlePort(String argument, SMTPSession session)
    {
        // not implemented
    }

    private void handleProto(String argument, SMTPSession session)
    {
        // not implemented
    }

    private void handleHelo(String argument, SMTPSession session)
    {
        // not implemented
    }

    private void handleSource(String argument, SMTPSession session)
    {
        // not implemented
    }

    private void handleAddr(String ipAddress, SMTPSession session)
    {
        getLogger().debug("XForward handleAddr: " + ipAddress);
        
        session.setRemoteIPAddress(ipAddress);
    }
    
    private void handleName(String hostname, SMTPSession session)
    {
        getLogger().debug("XForward handleName: " + hostname);

        session.setRemoteHost(hostname);
    }
    
    private void doXFoward(SMTPSession session, String argument) 
    throws IOException
    {        
        getLogger().debug("XForward: " + argument);
        
        if (argument != null)
        {
            NameValueIterator iterator = new NameValueIterator(argument);
    
            while (iterator.hasNext())
            {
                NameValueIterator.Entry nameValue = iterator.next();
                
                XForwardAttribute attribute = XForwardAttribute.fromName(nameValue.getName());
                
                if (attribute == null) {
                    getLogger().warn("Unknown XForward attribute: " + nameValue.getName());                    
                }
                
                /*
                 * XFORWARD arguments are XText encoded so we should decode it.
                 */
                String value = XTextEncoder.decode(nameValue.getValue());
                
                if (value == null) 
                {
                    getLogger().debug("value is null. ");
                    
                    continue;
                }
                
                switch (attribute)
                {
                case NAME   : handleName(value, session); break; 
                case ADDR   : handleAddr(value, session); break; 
                case PORT   : handlePort(value, session); break;
                case PROTO  : handleProto(value, session); break;
                case HELO   : handleHelo(value, session); break;
                case SOURCE : handleSource(value, session); break;
                default:
                    getLogger().warn("Unknown XForward attribute: " + nameValue.getValue());
                }                
            }
        }        
    }
}
