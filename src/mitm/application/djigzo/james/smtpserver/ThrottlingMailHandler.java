/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.smtpserver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.JamesStoreManager;
import mitm.application.djigzo.service.SystemServices;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.service.ServiceException;
import org.apache.avalon.framework.service.ServiceManager;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.services.MailRepository;
import org.apache.james.smtpserver.CommandHandler;
import org.apache.james.smtpserver.SMTPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThrottlingMailHandler implements CommandHandler, Configurable
{
    private final static Logger logger = LoggerFactory.getLogger(ThrottlingMailHandler.class);
    
    private static class Throttling
    {
    	private final int limit;
    	private final long delay;

    	public Throttling(int limit, long delay)
    	{
    		this.limit = limit;
    		this.delay = delay;
    	}

		public int getLimit() {
			return limit;
		}

		public long getDelay() {
			return delay;
		}
		
		@Override
		public String toString() {
			return "Limit: " + limit + "; Delay: " + delay;
		}
    }
    
    /*
     * The repository URLs that will be monitored to check if they do not grow too big
     */
    private List<String> repositories; 
    
    /*
     * The number of ehlo's before a new check
     */
    private int checkRate = 25;
    
    /*
     * Used to track the last time a check was done
     */
    private int checkCounter = 0;

    /*
     * The current active delay
     */
    private long currentDelay = 0;
        
    /*
     * List of limits and delays
     */
    private final List<Throttling> throttlings = Collections.synchronizedList(new ArrayList<Throttling>());
    
    private void parseLimits(Configuration handlerConfiguration)
    throws ConfigurationException
    {
        Configuration[] limits = handlerConfiguration.getChildren("throttling");

        for (Configuration configuration : limits)
        {
        	Configuration limitConfig = configuration.getChild("limit");
        	Configuration delayConfig = configuration.getChild("delay");
        	
        	if (limitConfig == null) {
        		throw new ConfigurationException("limit is missing");
        	}

        	if (delayConfig == null) {
        		throw new ConfigurationException("delay is missing");
        	}
        	
        	int limit = limitConfig.getValueAsInteger();
        	long delay = delayConfig.getValueAsLong();
        	
        	throttlings.add(new Throttling(limit, delay));
        }    	
    }
    
    @Override
    public void configure(Configuration handlerConfiguration)
    throws ConfigurationException
    {
        logger.info("Configuring CommandHandler");
        
        Configuration[] repositoryConfigs = handlerConfiguration.getChild("repositories").getChildren("repository");
        
        repositories = new ArrayList<String>(repositoryConfigs.length);
        
        for (Configuration repositoryConfig : repositoryConfigs) {
            repositories.add(repositoryConfig.getValue());            
        }
        
        Configuration config = handlerConfiguration.getChild("checkRate", false);
        
        if (config != null) {
        	checkRate = config.getValueAsInteger(100);
        }
        
        parseLimits(handlerConfiguration);
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("Repositories: ");
        sb.appendWithSeparators(repositories, ", ");
        sb.appendSeparator("; ");
        sb.append("checkRate: ");
        sb.append(checkRate);
        sb.appendSeparator("; ");
        sb.appendAll(throttlings);
        
        logger.info(sb.toString());
    }
    
    @Override
    public void onCommand(SMTPSession session) {
    	delay();
    }
    
    private synchronized long getDelay()
    {
    	if (checkCounter > checkRate)
    	{
    		checkCounter = 0;
    		
        	currentDelay = 0;

        	ServiceManager serviceManager = SystemServices.getAvalonServiceManager();
        	
            for (String repositoryURL : repositories)
            {
                MailRepository mailRepository;
                
                try {
                    mailRepository = new JamesStoreManager(serviceManager).getMailRepository(repositoryURL);
                }
                catch (ServiceException e) 
                {
                    logger.error("Error getting mailRepository for URL: " + repositoryURL);
                    
                    continue;
                }
                
                Iterator<?> keyIterator;
                
                try {
                    keyIterator = mailRepository.list();
                }
                catch (MessagingException e) 
                {
                    logger.error("Error getting list for URL: " + repositoryURL);
                    
                    continue;
                }
                
                int size = 0;
                
                while (keyIterator.hasNext()) 
                {
                    keyIterator.next();
                    
                    size++;
                }
    
                logger.debug("size of repository " + repositoryURL + " is " + size);
                
                for (Throttling throttling : throttlings)
                {
                	if (size > throttling.getLimit())
                	{
                		if (throttling.getDelay() > currentDelay) {
                			currentDelay = throttling.getDelay();
                		}
                	}
                	else {
                		break;
                	}
                }
            }
    	}
    	
    	checkCounter++;
    	
    	return currentDelay;
    }
    
    private void delay() 
    {
        long delay = getDelay();
        
        logger.debug("Current delay: " + delay);
        
        if (delay > 0)
        {
	        try {
				Thread.sleep(delay);
			} 
	        catch (InterruptedException e) {
	        	// ignore
			}
        }
    }
}
