/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.smtpserver;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;

import org.apache.avalon.framework.configuration.Configurable;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.smtpserver.CommandHandler;
import org.apache.james.smtpserver.SMTPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckDatabaseBlockingRcptCmdHandler implements CommandHandler, Configurable
{
    private final static Logger logger = LoggerFactory.getLogger(CheckDatabaseBlockingRcptCmdHandler.class);
    
    /*
     * The number of RCPTs to receive after which a database check will be done
     */
    private int checkInterval;
    
    /*
     * The SMTP status to return when the database is inactive
     */
    private String rejectCode;
    
    /*
     * Used to track the number of times onCommand is called so we know if we need to check the database
     */
    private AtomicInteger rcptCounter = new AtomicInteger();
    
    /*
     * The SQL query we will use to test is the database is active
     */
    private String testQuery;
    
    /*
     * True if the database was not active last time we checked
     */
    private AtomicBoolean databaseNotActive = new AtomicBoolean();
    
    @Override
    public void configure(Configuration handlerConfiguration)
    throws ConfigurationException
    {
        logger.info("Configuring CommandHandler");
        
        Configuration config = handlerConfiguration.getChild("rejectCode", false);
        
        if (config != null) {
            rejectCode = config.getValue("452");
        }
        
        config = handlerConfiguration.getChild("checkInterval", false);
        
        if (config != null) {
        	checkInterval = config.getValueAsInteger(1);
        }

        config = handlerConfiguration.getChild("testQuery", false);
        
        if (config == null || config.getValue() == null) {
        	throw new ConfigurationException("testQuery is missing.");
        }
        
        testQuery = config.getValue();
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("rejectCode: ");
        sb.append(rejectCode);
        sb.appendSeparator("; ");
        sb.append("checkInterval: ");
        sb.append(checkInterval);
        sb.appendSeparator("; ");
        sb.append("testQuery: ");
        sb.append(testQuery);
        
        logger.info(sb.toString());
    }
    
    @Override
    public void onCommand(SMTPSession session)
    {
        try {                
            checkDatabase();
        }
        catch(DatabaseNotActiveException e) 
        {
            logger.warn(e.getMessage());
            
            String response = rejectCode + " " + e.getMessage();
            
            session.writeResponse(response);
        }
    }
    
    private void checkDatabase() 
    throws DatabaseNotActiveException
    {
        if (rcptCounter.incrementAndGet() >= checkInterval) 
        {
            rcptCounter.set(0);
            
            if (!HibernateUtils.isDatabaseActive(SystemServices.getSessionManager(), testQuery)) {
                throw new DatabaseNotActiveException("Database is not active.");
            }
        }
        else {
            if (databaseNotActive.get()) 
            {
                /*
                 * We did not currently check the database but the previous check (cached value) reported that it was not active
                 */
                throw new DatabaseNotActiveException("Database is not active (cached).");
            }
        }
        
        if (databaseNotActive.get()) {
            logger.info("Mail is accepted again because database is active.");
        }
        
        databaseNotActive.set(false);
    }
}
