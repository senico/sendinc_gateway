/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Set;

import mitm.common.security.NoSuchProviderRuntimeException;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.util.Check;

import org.apache.commons.lang.SerializationException;
import org.apache.mailet.Mail;

/**
 * This class is used for storing certificates into a James {@link Mail} object as a serialized mail 
 * attribute. We will do the serialization ourselves so we are in control which JCE provider is used 
 * to deserialize the certificate.
 * 
 * WARNING: this class is used for long term serialization. The class should there not be moved or renamed. Be careful
 * with any changes made to class members, changes might result in changes to deserialization.
 * 
 * @author Martijn Brinkers
 *
 */
public class Certificates implements Serializable
{
    private static final long serialVersionUID = -4834514498801331736L;

    private Set<X509Certificate> certificates;

    public Certificates() {
        this.certificates = new HashSet<X509Certificate>();
    }
    
    public Certificates(Set<X509Certificate> certificates) 
    {
    	Check.notNull(certificates, "certificates");
        
        this.certificates = certificates;
    }
        
    public Set<X509Certificate> getCertificates() {
        return certificates;
    }
    
    public void setCertificates(Set<X509Certificate> certificates) {
        this.certificates = certificates;
    }
    
    /*
     * Because Java deserializes Certificates using the default provider (and not using the provider that
     * created the certificates) we will use our own serialization mechanism. 
     */
    private void writeObject(ObjectOutputStream out) 
    throws IOException
    {
        try {
            out.writeLong(serialVersionUID);
            
            /*
             * Write the number of certificates so we know how many we have to read when deserializing.
             */
            out.writeInt(certificates.size());
            
            for (X509Certificate certificate : certificates)            
            {
                byte[] encoded = certificate.getEncoded();
                /* 
                 * write the size of the encoded certificate so we can restore it 
                 */
                out.writeInt(encoded.length);
                out.write(certificate.getEncoded());
            }
        }
        catch(CertificateEncodingException e) {
            throw new IOException(e);
        }
    }

    private void readObject(ObjectInputStream in) 
    throws IOException, ClassNotFoundException 
    {
        try {
            CertificateFactory certificateFactory = SecurityFactoryFactory.getSecurityFactory().
                    createCertificateFactory("X.509");
            
            certificates = new HashSet<X509Certificate>();
            
            long version = in.readLong();
            
            if (version != serialVersionUID) {
                throw new SerializationException("Version expected '" + serialVersionUID + "' but got '" + version);
            }
            
            /*
             * Read how many certificates we have to read.
             */
            int nrOfCertificates = in.readInt();
            
            for (int i = 0; i < nrOfCertificates; i++)
            {
                int encodedSize = in.readInt();
                
                byte[] encoded = new byte[encodedSize];
                
                in.readFully(encoded);
                    
                X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(
                        new ByteArrayInputStream(encoded));
                
                certificates.add(certificate);
            }
        }
        catch(NoSuchProviderException e) {
            throw new NoSuchProviderRuntimeException(e);
        }
        catch (CertificateException e) {
            throw new IOException(e);
        }
        catch (SecurityFactoryFactoryException e) {
            throw new IOException(e);
        }
    }
}
