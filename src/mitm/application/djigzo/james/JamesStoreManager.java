/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;

import mitm.common.util.Check;

import org.apache.avalon.cornerstone.services.store.Store;
import org.apache.avalon.framework.configuration.DefaultConfiguration;
import org.apache.avalon.framework.container.ContainerUtil;
import org.apache.avalon.framework.service.ServiceException;
import org.apache.avalon.framework.service.ServiceManager;
import org.apache.james.services.MailRepository;
import org.apache.james.services.SpoolRepository;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * helper class to manage James spools.
 * 
 * @author Martijn Brinkers
 *
 */
public class JamesStoreManager
{
    private final static Logger logger = LoggerFactory.getLogger(JamesStoreManager.class);
    
    private final Store mailStore;
    private final SpoolRepository mailSpool;
    
    public JamesStoreManager(ServiceManager serviceManager) 
    throws ServiceException
    {
        Check.notNull(serviceManager, "serviceManager");
        
        mailStore = (Store) serviceManager.lookup(Store.ROLE);
        
        mailSpool = (SpoolRepository) serviceManager.lookup(SpoolRepository.ROLE);
        
        Check.notNull(mailStore, "mailStore");
        Check.notNull(mailSpool, "mailSpool");
    }
    
    /**
     * Returns the mail repository with the URL
     */
    public MailRepository getMailRepository(String destinationURL) 
    throws ServiceException
    {
        DefaultConfiguration mailConf = new DefaultConfiguration("repository", "generated:JamesStoreUtils#getMailRepository");
        
        mailConf.setAttribute("destinationURL", destinationURL);
        mailConf.setAttribute("type", "MAIL");
        mailConf.setAttribute("CACHEKEYS", "false");
        MailRepository repository = (MailRepository) mailStore.select(mailConf);
        
        return repository;
    }

    /**
     * Returns the number of items in the given repository
     */
    public int getRepositorySize(MailRepository repository) 
    throws MessagingException
    {
        Check.notNull(repository, "repository");
        
        Iterator<?> nameIterator = repository.list();
        
        int size = 0;
        
        while (nameIterator.hasNext()) 
        {
            nameIterator.next();
            
            size++;
        }
        
        return size;            
    }
    
    /**
     * Returns the names (the key under which the mail is stored) of all the mail items.
     */
    public List<String> getMailNames(MailRepository repository, int startIndex, int maxItems) 
    throws MessagingException
    {
        Check.notNull(repository, "repository");

        Iterator<?> nameIterator = repository.list();
        
        List<String> names = new LinkedList<String>();
        
        int index = 0;
        
        while (nameIterator.hasNext()) 
        {
            Object obj = nameIterator.next();
            
            if (!(obj instanceof String)) 
            {
                logger.warn("Mail item is-not-a-String.");
                
                continue;
            }
            
            if (index >= startIndex) {
                names.add((String) obj);
            }
            
            index++;
            
            if (names.size() >= maxItems) {
                break;
            }
        }
        
        return names;
    }
    
    /**
     * Moves the mail with name from the source repository to the destination repository. If newSate is set (ie non null) the
     * mail item will have it's state set to the given state. If reset is true the error and last sent date is reset
     * 
     */
    public void moveMail(MailRepository sourceRepository, MailRepository destinationRepository, String mailName, String newState) 
    throws MessagingException
    {
        Check.notNull(sourceRepository, "sourceRepository");
        Check.notNull(destinationRepository, "destinationRepository");
        Check.notNull(mailName, "mailName");
        
        sourceRepository.lock(mailName);

        Mail mail = null;
        
        try {
            mail = sourceRepository.retrieve(mailName);
            
            if (newState != null) {
                mail.setState(newState);
            }
            
            destinationRepository.store(mail);
            sourceRepository.remove(mail);
        }
        finally 
        {
            try {
                sourceRepository.unlock(mailName);
            }
            catch(MessagingException e) {
                logger.warn("Unable to unlock mail " + mailName);
            }
            
            if (mail != null) {
                ContainerUtil.dispose(mail);
            }
        }
    }
    
    /**
     * Respools the message
     */
    public void respool(MailRepository repository, String mailName, String newState, boolean removeAttributes) 
    throws MessagingException
    {
        Check.notNull(repository, "repository");
        Check.notNull(mailName, "mailName");
        
        repository.lock(mailName);

        Mail mail = null;
        
        try {
            mail = repository.retrieve(mailName);

            if (removeAttributes) {
                mail.removeAllAttributes();
            }
            
            if (newState != null) {
                mail.setState(newState);
            }
            
            /*
             * Forces immediately delivery
             */
            mail.setLastUpdated(new Date(0));
            mail.setErrorMessage(null);

            /*
             * Add the message to the mail spool and remove it from the source repository
             */
            
            mailSpool.store(mail);
            
            repository.remove(mail);
        }
        finally 
        {
            try {
            	repository.unlock(mailName);
            }
            catch(MessagingException e) {
                logger.warn("Unable to unlock mail " + mailName);
            }
            
            if (mail != null) {
                ContainerUtil.dispose(mail);
            }
        }
    }
    
    /**
     * Stores the mail in the mail spool.
     */
    public void store(Mail mail)
    throws MessagingException
    {
        Check.notNull(mail, "mail");
        
        mailSpool.store(mail);
    }
}
