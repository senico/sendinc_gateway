/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import mitm.application.djigzo.service.SystemServices;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.SerializationException;
import org.apache.commons.lang.SerializationUtils;

/**
 * A serializable object which can be used to store serializable objects in encrypted form
 * 
 * @author Martijn Brinkers
 *
 */
public class EncryptedContainer<T extends Serializable> implements Serializable
{
    private static final long serialVersionUID = 3864556763893627979L;

    /*
     * The stored value
     */
    private T value;
    
    
    public EncryptedContainer() {
        /*
         * empty on purpose
         */
    }

    public EncryptedContainer(T value) {
        this.value = value;
    }

    public void set(T value) {
        this.value = value;
    }
    
    public T get() {
        return value;
    }

    /*
     * Note: We do not like that the encryptor is fixed like this. We can however no easilly
     * provide the encryptor in the constructor since this class is going to be serialized by
     * some internal systems. The encryptor therefore can not be easilly provided externally.
     */
    private Encryptor getEncryptor() {
        return SystemServices.getSystemEncryptor();
    }
    
    /*
     * Need to do the serialization manually since we need to encrypt the bytes
     */
    private void writeObject(ObjectOutputStream out)
    throws IOException, EncryptorException 
    {
        out.writeLong(serialVersionUID);
        
        byte[] encrypted = getEncryptor().encrypt(SerializationUtils.serialize(value)); 
        
        out.writeInt(ArrayUtils.getLength(encrypted));
        out.write(encrypted);
    }
    
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream in)
    throws IOException, EncryptorException 
    {
        long version = in.readLong();

        if (version != serialVersionUID) {
            throw new SerializationException("Version expected '" + serialVersionUID + "' but got '" + version);
        }
        
        byte[] encrypted = new byte[in.readInt()];
        
        in.read(encrypted);
        
        value = (T) SerializationUtils.deserialize(getEncryptor().decrypt(encrypted));
    }
}
