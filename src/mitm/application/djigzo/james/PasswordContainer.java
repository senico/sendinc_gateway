/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.io.Serializable;

import mitm.application.djigzo.service.SystemServices;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.util.MiscStringUtils;

/**
 * Password is used to temporarily store passwords in a mail object. The passwords are encrypted/decrypted using the
 * system encryptor.
 * 
 * @author Martijn Brinkers
 *
 */
public class PasswordContainer implements Serializable
{
    private static final long serialVersionUID = -565429239478217678L;

    /*
     * The encrypted password
     */
    private byte[] encryptedPassword;
    
    /*
     * The password identifier (can be used to tell the end-party which password was used when multiple passwords are used)
     */
    private String passwordID;
    
    /*
     * The password length (in bytes) of the generated password (can be null)
     */
    private Integer passwordLength;
    
    /*
     * General purpose object which can be used to add extra info the the PasswordContainer
     */
    private Serializable tag;
    
    /**
     * Construct a PasswordContainer with password and passwordID. The password will be encrypted using the system
     * encryptor.
     */
    public PasswordContainer(String password, String passwordID) 
    throws EncryptorException
    {
        this.encryptedPassword = encryptPassword(password);
        this.passwordID = passwordID;
    }

    /**
     * Construct a PasswordContainer with an encrypted password and passwordID. The password has to be decryptable by the
     * system encryptor.
     */
    public PasswordContainer(byte[] encryptedPassword, String passwordID)
    {
        this.encryptedPassword = encryptedPassword;
        this.passwordID = passwordID;
    }

    /*
     * Note: We do not like that the encryptor is fixed like this. We can however no easilly
     * provide the encryptor in the constructor since this class is going to be serialized by
     * some internal systems. The encryptor therefore can not be easilly provided externally.
     */
    private Encryptor getEncryptor() {
        return SystemServices.getSystemEncryptor();
    }
    
    public String getPassword() 
    throws EncryptorException 
    {
        return decryptPassword();
    }

    public String getPasswordID() {
        return passwordID;
    }
    
    private byte[] encryptPassword(String password) 
    throws EncryptorException 
    {
        return getEncryptor().encrypt(MiscStringUtils.toUTF8Bytes(password));
    }

    private String decryptPassword() 
    throws EncryptorException 
    {
        return MiscStringUtils.toUTF8String(getEncryptor().decrypt(encryptedPassword));
    }

    public Integer getPasswordLength() {
        return passwordLength;
    }

    public void setPasswordLength(Integer passwordLength) {
        this.passwordLength = passwordLength;
    }

    public Serializable getTag() {
        return tag;
    }

    public void setTag(Serializable tag) {
        this.tag = tag;
    }
}
