/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;

import mitm.common.mail.EmailAddressUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

public class MailAddressUtils
{
    /**
     * Returns a type safe collection of the mail recipients. It is assumed that mail.getRecipients()
     * only contain MailAddress instances.
     */
    @SuppressWarnings("unchecked")
    public static List<MailAddress> getRecipients(Mail mail)
    {
    	if (mail == null) {
    		return null;
    	}
    	
    	/*
    	 * We will create a clone to make sure that the original recipients are not modified. 
    	 */
        return new ArrayList<MailAddress>(mail.getRecipients());
    }

    /**
     * Converts the MailAddress to an InternetAddress. 
     */
    public static InternetAddress toInternetAddress(MailAddress addresses) {
        return addresses != null ? addresses.toInternetAddress() : null;
    }
    
    /**
     * Converts the collection of MailAddress's to a list of InternetAddress's. 
     */
    public static List<InternetAddress> toInternetAddressList(Collection<MailAddress> addresses) 
    {
        if (addresses == null) {
            return null;
        }
        
        List<InternetAddress> internetAddresses = new ArrayList<InternetAddress>(addresses.size());
        
        for (MailAddress address : addresses)
        {
            if (address != null) {
                internetAddresses.add(address.toInternetAddress());
            }
        }
        
        return internetAddresses;
    }
    
    /**
     * Converts the collection of strings to a list of MailAddress's.
     */
    public static List<MailAddress> toMailAddressList(Collection<String> addresses) 
    throws ParseException
    {
        if (addresses == null) {
            return null;
        }
        
        List<MailAddress> mailAddresses = new ArrayList<MailAddress>(addresses.size());
        
        for (String address : addresses)
        {
            if (address == null) {
                continue;
            }

            MailAddress mailAddress = new MailAddress(address);

            mailAddresses.add(mailAddress);
        }
        
        return mailAddresses;
    }
    
    /**
     * Converts the array of Address's to a list of MailAddress's.
     */
    public static List<MailAddress> fromAddressArrayToMailAddressList(Address... addresses) 
    throws ParseException
    {
        if (addresses == null) {
            return null;
        }
        
        List<MailAddress> mailAddresses = new ArrayList<MailAddress>(addresses.length);
        
        for (int i = 0; i < addresses.length; i++) 
        {
        	Address address = addresses[i];
        	
        	if (address == null) {
        		continue;
        	}
        	
            MailAddress mailAddress = new MailAddress(EmailAddressUtils.getEmailAddress(address));

            mailAddresses.add(mailAddress);
        }
        
        return mailAddresses;
    }
}
