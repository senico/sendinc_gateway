/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mitm.common.reflection.ReflectionUtils;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.BooleanUtils;
import org.apache.mailet.Mail;

public class MailAttributesUtils
{
    /**
     * Returns the attribute value with name attribute. If attribute does not exist or is null default
     * value is returned. The type T is checked to see if it is an instance of clazz.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getAttributeValue(Mail mail, String attribute, T defaultValue, Class<?> clazz)
    {
        if (defaultValue != null && !ReflectionUtils.isInstanceOf(defaultValue.getClass(), clazz)) {
            throw new IllegalArgumentException("defaultValue is not an instance of " + clazz.toString());            
        }
       
        Object rawValue = mail.getAttribute(attribute);
        
        T value = null;
        
        if (rawValue != null) 
        {
            if (!ReflectionUtils.isInstanceOf(rawValue.getClass(), clazz)) {
                throw new IllegalArgumentException("attribute is not an instance of " + clazz.toString());            
            }
            
            value = (T) rawValue;
        }
        
        if (value == null) {
            value = defaultValue;
        }
        
        return value;        
    }

    /**
     * Returns the attribute value as a List of clazz. Null if the attribute does not exist
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getAttributeAsList(Mail mail, String attribute, Class<?> clazz)
    {
        Object rawValue = mail.getAttribute(attribute);
        
        List<T> result = null;
        
        if (rawValue != null) 
        {
            if (rawValue instanceof Collection)
            {
                for (Object o : (Collection<?>) rawValue)
                {
                    if (o != null && !ReflectionUtils.isInstanceOf(o.getClass(), clazz)) {
                        throw new IllegalArgumentException("attribute is not an instance of " + clazz.toString());            
                    }
                }
                
                result = new ArrayList<T>((Collection<T>) rawValue);
            }
            else if (rawValue.getClass().isArray())
            {
                Object[] array = (Object[]) rawValue;
                
                result = new ArrayList<T>(array.length);
                
                for (Object o : array)
                {
                    if (o != null && !ReflectionUtils.isInstanceOf(o.getClass(), clazz)) {
                        throw new IllegalArgumentException("attribute is not an instance of " + clazz.toString());            
                    }
                    
                    result.add((T) o);
                }                
            }
            else 
            {
                if (!ReflectionUtils.isInstanceOf(rawValue.getClass(), clazz)) {
                    throw new IllegalArgumentException("attribute is not an instance of " + clazz.toString());            
                }

                result = new ArrayList<T>(1);
                
                result.add((T) rawValue);
            }
        }
        
        return result;        
    }
    
    /**
     * Returns the attribute value as an Array of clazz. Null if the attribute does not exist
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] getAttributeAsArray(Mail mail, String attribute, Class<?> clazz)
    {
        T[] result = null;
        
        List<T> list = getAttributeAsList(mail, attribute, clazz);
        
        if (list != null)
        {
            result = (T[]) Array.newInstance(clazz, list.size());
            
            /*
             * Note: we assume that the list returned from getAttributeAsList is an ArrayList
             */
            result = ((ArrayList<T>) list).toArray(result);
        }
        
        return result;
    }
        
    /**
     * Sets the attribute with the given name to the serialized value.
     */    
    public static void setAttributeValue(Mail mail, String attribute, Serializable value) {
        mail.setAttribute(attribute, value);
    }
    
    /**
     * Sets the attribute with the given name to the string representation of value using the toString()
     * method.
     */
    public static void setAttributeValueAsString(Mail mail, String attribute, Object value) {
        mail.setAttribute(attribute, value != null ? value.toString() : null);
    }

    /**
     * Returns the string attribute as a Boolean.
     */
    public static Boolean getBoolean(Mail mail, String attribute, Boolean defaultValue)
    {
        Boolean result = null;

        Object rawValue = mail.getAttribute(attribute);
        
        if (rawValue instanceof Boolean) {
            result = (Boolean) rawValue;
        }
        else if (rawValue instanceof String) {
            result = BooleanUtils.toBooleanObject((String) rawValue);
        }

        if (result == null) {
            result = defaultValue;
        }
        
        return result;        
    }    

    /**
     * Returns the string attribute as an Integer.
     */
    public static Integer getInteger(Mail mail, String attribute, Integer defaultValue)
    {
        Integer result = null;

        Object rawValue = mail.getAttribute(attribute);
        
        if (rawValue instanceof Integer) {
            result = (Integer) rawValue;
        }
        else if (rawValue instanceof String) {
            result = Integer.valueOf((String) rawValue);
        }

        if (result == null) {
            result = defaultValue;
        }
        
        return result;        
    }    

    /**
     * Returns the string attribute as a Long.
     */
    public static Long getLong(Mail mail, String attribute, Long defaultValue)
    {
        Long result = null;

        Object rawValue = mail.getAttribute(attribute);
        
        if (rawValue instanceof Long) {
            result = (Long) rawValue;
        }
        if (rawValue instanceof Integer) {
            result = (long) (Integer) rawValue;
        }
        else if (rawValue instanceof String) {
            result = Long.valueOf((String) rawValue);
        }

        if (result == null) {
            result = defaultValue;
        }
        
        return result;        
    }
    
    /**
     * Sets the binary attribute. The byte[] is base64 encoded.
     */
    public static void setBinary(Mail mail, String attribute, byte[] value)
    {
        String base64 = MiscStringUtils.toAsciiString(Base64.encodeBase64Chunked(value));
        
        mail.setAttribute(attribute, base64);
    }
    
    /**
     * Returns the binary attribute if it exists. The binary property should be base64 encoded 
     * String. Returns defaultValue if property does not exist.
     */
    public static byte[] getBinary(Mail mail, String attribute, byte[] defaultValue)
    {
        byte[] value = defaultValue;

        String attributeValue = (String) mail.getAttribute(attribute);

        if (attributeValue != null) {
            value = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(attributeValue));
        }
        
        return value;
    }    
}
