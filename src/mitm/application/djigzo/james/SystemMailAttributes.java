/*
 * Copyright (c) 2010, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.james.smtpserver.SMTPSession;
import org.apache.mailet.Mail;

/**
 * Helper class to work with mail attributes added by James
 * 
 * @author Martijn Brinkers
 *
 */
public class SystemMailAttributes
{
    /**
     * Removes the SMTP extensions (for example DSN extensions) from the mail attributes. 
     * This is for example needed when a Mail object is cloned but the SMTP extensions
     * should not be cloned.
     */
    public static void removeSMTPExtensions(Mail mail)
    {
        if (mail == null) {
            return;
        }
        
        Iterator<?> nameIterator =  mail.getAttributeNames();
        
        if (nameIterator != null)
        {
            /*
             * We cannot remove items while iterating (ConcurrentModificationException) so we need 
             * to keep track of which attributes to remove and remove them after the iteration. 
             */
            List<String> toRemove = new LinkedList<String>();
            
            while(nameIterator.hasNext())
            {
                Object next = nameIterator.next();
                
                if (!(next instanceof String)) {
                    continue;
                }
                
                String name = (String) next;
                
                if (name.startsWith(SMTPSession.SMTP_EXTENSIONS_BASE_ATTRIBUTE_NAME)) {
                    toRemove.add(name);
                }
            }
            
            for (String name : toRemove) {
                mail.removeAttribute(name);
            }
        }
    }
}
