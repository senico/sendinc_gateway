/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import mitm.common.dlp.PolicyViolation;
import mitm.common.util.Check;

import org.apache.mailet.Mail;

public class DjigzoMailAttributesImpl implements DjigzoMailAttributes
{
    private final static String BASE_ATTRIBUTE = "djigzo.";
    
    private final static String CERTIFICATES_ATTRIBUTE         = BASE_ATTRIBUTE + "certificates";
    private final static String ENCRYPTED_PASSWORD_ATTRIBUTE   = BASE_ATTRIBUTE + "encryptedPassword";
    private final static String PASSWORD_ID_ATTRIBUTE          = BASE_ATTRIBUTE + "passwordID";
    private final static String PASSWORDS_ATTRIBUTE            = BASE_ATTRIBUTE + "encryptedPasswords";
    private final static String PHONE_NUMBERS_ATTRIBUTE        = BASE_ATTRIBUTE + "phoneNumbers";
    private final static String MESSAGE_ATTRIBUTE              = BASE_ATTRIBUTE + "message";
    private final static String MAIL_ID                        = BASE_ATTRIBUTE + "mailID";
    private final static String ORIGINAL_SUBJECT               = BASE_ATTRIBUTE + "originalSubject";
    private final static String POLICY_VIOLATIONS              = BASE_ATTRIBUTE + "policyViolations";
    private final static String POLICY_VIOLATION_ERROR_MESSAGE = BASE_ATTRIBUTE + "policyViolationErrorMessage";
    private final static String MAIL_REPOSITORY_ID             = BASE_ATTRIBUTE + "mailRepositoryID";
    private final static String CLIENT_SECRETS                 = BASE_ATTRIBUTE + "clientSecrets";
    private final static String PORTAL_INVITATIONS             = BASE_ATTRIBUTE + "portalInvitations";
    private final static String PROTECTED_HEADERS              = BASE_ATTRIBUTE + "protectedHeaders";
    private final static String SECURITY_INFO_TAGS             = BASE_ATTRIBUTE + "securityInfoTags";
    
    /*
     * The Mail object from which the serialized will be read from and written to
     */
    private final Mail mail;
    
    public DjigzoMailAttributesImpl(Mail mail)
    {
    	Check.notNull(mail, "mail");
    	
        this.mail = mail;
    }

    public static DjigzoMailAttributes getInstance(Mail mail) {
        return new DjigzoMailAttributesImpl(mail);
    }
    
    @Override
    public Certificates getCertificates() {
        return MailAttributesUtils.getAttributeValue(mail, CERTIFICATES_ATTRIBUTE, null, Certificates.class);        
    }
    
    @Override
    public void setCertificates(Certificates certificates) {
        MailAttributesUtils.setAttributeValue(mail, CERTIFICATES_ATTRIBUTE, certificates);        
    }
    
    @Override
    public byte[] getEncryptedPassword() {
        return MailAttributesUtils.getBinary(mail, ENCRYPTED_PASSWORD_ATTRIBUTE, null);                
    }
    
    @Override
    public void setEncryptedPassword(byte[] encryptedPassword) {
        MailAttributesUtils.setBinary(mail, ENCRYPTED_PASSWORD_ATTRIBUTE, encryptedPassword);        
    }

    @Override
    public String getPasswordID() {
        return MailAttributesUtils.getAttributeValue(mail, PASSWORD_ID_ATTRIBUTE, null, String.class);                
    }
    
    @Override
    public void setPasswordID(String passwordID) {
        MailAttributesUtils.setAttributeValue(mail, PASSWORD_ID_ATTRIBUTE, passwordID);        
    }
    
    @Override
    public Passwords getPasswords() {
        return MailAttributesUtils.getAttributeValue(mail, PASSWORDS_ATTRIBUTE, null, Passwords.class);                        
    }
    
    @Override
    public void setPasswords(Passwords passwords) {
        MailAttributesUtils.setAttributeValue(mail, PASSWORDS_ATTRIBUTE, passwords);                
    }
    
    @Override
    public PhoneNumbers getPhoneNumbers() {
        return MailAttributesUtils.getAttributeValue(mail, PHONE_NUMBERS_ATTRIBUTE, null, PhoneNumbers.class);                        
    }
    
    @Override
    public void setPhoneNumbers(PhoneNumbers phoneNumbers) {
        MailAttributesUtils.setAttributeValue(mail, PHONE_NUMBERS_ATTRIBUTE, phoneNumbers);                
    }
    
    @Override
    public String getMessage() {
        return MailAttributesUtils.getAttributeValue(mail, MESSAGE_ATTRIBUTE, null, String.class);                
    }
    
    @Override
    public void setMessage(String message) {
        MailAttributesUtils.setAttributeValue(mail, MESSAGE_ATTRIBUTE, message);        
    }
    
    @Override
    public String getMailID() {
        return MailAttributesUtils.getAttributeValue(mail, MAIL_ID, null, String.class);                
    }

    @Override
    public void setMailID(String id) {
        MailAttributesUtils.setAttributeValue(mail, MAIL_ID, id);        
    }
    
    @Override
    public String getOriginalSubject() {
        return MailAttributesUtils.getAttributeValue(mail, ORIGINAL_SUBJECT, null, String.class);                
    }
    
    @Override
    public void setOriginalSubject(String subject) {
        MailAttributesUtils.setAttributeValue(mail, ORIGINAL_SUBJECT, subject);        
    }    
    
    @Override
    public List<PolicyViolation> getPolicyViolations() {
        return MailAttributesUtils.getAttributeValue(mail, POLICY_VIOLATIONS, null, List.class);                
    }
    
    @Override
    public void setPolicyViolations(Collection<PolicyViolation> violations)
    {
        /*
         * We must use a serialiable collection instance because Collection is not serialiable 
         */
        
        MailAttributesUtils.setAttributeValue(mail, POLICY_VIOLATIONS, new LinkedList<PolicyViolation>(violations));
    }
    
    @Override
    public String getPolicyViolationErrorMessage() {
        return MailAttributesUtils.getAttributeValue(mail, POLICY_VIOLATION_ERROR_MESSAGE, null, String.class);                
    }
    
    @Override
    public void setPolicyViolationErrorMessage(String message) {
        MailAttributesUtils.setAttributeValue(mail, POLICY_VIOLATION_ERROR_MESSAGE, message);
    }    
    
    @Override
    public String getMailRepositoryID() {
        return MailAttributesUtils.getAttributeValue(mail, MAIL_REPOSITORY_ID, null, String.class);                
    }

    @Override
    public void setMailRepositoryID(String id) {
        MailAttributesUtils.setAttributeValue(mail, MAIL_REPOSITORY_ID, id);
    }

    @Override
    public ClientSecrets getClientSecrets() {
        return MailAttributesUtils.getAttributeValue(mail, CLIENT_SECRETS, null, ClientSecrets.class);                        
    }

    @Override
    public void setClientSecrets(ClientSecrets clientSecrets) {
        MailAttributesUtils.setAttributeValue(mail, CLIENT_SECRETS, clientSecrets);                
    }

    @Override
    public PortalInvitations getPortalInvitations() {
        return MailAttributesUtils.getAttributeValue(mail, PORTAL_INVITATIONS, null, PortalInvitations.class);                        
    }

    @Override
    public void setPortalInvitations(PortalInvitations portalInvitations) {
        MailAttributesUtils.setAttributeValue(mail, PORTAL_INVITATIONS, portalInvitations);                
    }

    @Override
    public String[] getProtectedHeaders() {
        return MailAttributesUtils.getAttributeAsArray(mail, PROTECTED_HEADERS, String.class);                        
    }

    @Override
    public void setProtectedHeaders(String[] protectedHeaders) {
        MailAttributesUtils.setAttributeValue(mail, PROTECTED_HEADERS, protectedHeaders);                
    }

    @Override
    public SecurityInfoTags getSecurityInfoTags() {
        return MailAttributesUtils.getAttributeValue(mail, SECURITY_INFO_TAGS, null, SecurityInfoTags.class);                        
    }

    @Override
    public void setSecurityInfoTags(SecurityInfoTags tags) {
        MailAttributesUtils.setAttributeValue(mail, SECURITY_INFO_TAGS, tags);                
    }
}
