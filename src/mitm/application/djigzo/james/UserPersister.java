/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import java.util.Collection;
import java.util.HashSet;

import mitm.application.djigzo.User;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionManager;
import mitm.common.util.Check;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for making non persistent users persistent.
 * 
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPersister
{
    private final static Logger logger = LoggerFactory.getLogger(UserPersister.class);
    
    private final UserWorkflow userWorkflow;
    
    private final SessionManager sessionManager;
    
    private final DatabaseActionExecutor databaseActionExecutor;
    
    
    public UserPersister(UserWorkflow userWorkflow, SessionManager sessionManager, 
            DatabaseActionExecutor databaseActionExecutor)
    {
    	Check.notNull(userWorkflow, "userWorkflow");
    	Check.notNull(sessionManager, "sessionManager");
    	Check.notNull(databaseActionExecutor, "databaseActionExecutor");
    	
        this.userWorkflow = userWorkflow;
        this.sessionManager = sessionManager;
        this.databaseActionExecutor = databaseActionExecutor;
    }
    
    /**
     * Makes users that are not yet persistent persistent. Each users is made persistent in it's own
     * transaction to make sure that if one user cannot be persisted it does not influence other users. 
     * This method does not guarantee that users are persisted.
     */
    public void tryToMakeNonPersistentUsersPersistent(Collection<User> users)
    {
        Collection<User> nonPersistentUsers = getNonPersistentUsers(users);
        
        for (User nonPersistentUser : nonPersistentUsers)
        {
            try {
                final User finalNonPersistentUser = nonPersistentUser;
                
                databaseActionExecutor.executeTransaction(
                    new DatabaseVoidAction()
                    {
                        @Override
                        public void doAction(Session session)
                        throws DatabaseException
                        {
                            Session previousSession = sessionManager.getSession();

                        	sessionManager.setSession(session);
    
                            try {
                                makePersistentAction(finalNonPersistentUser);
                            }
                            finally {
                            	sessionManager.setSession(previousSession);                            
                            }
                        }
                    });
            }
            catch(DatabaseException e) {
                logger.error("Error persisting user.", e);
            }
            catch(ConstraintViolationException e) {
                logger.error("Error persisting the user. The user was probably already persisted. " + 
                        "Skipping this user.", e);
            }                
            catch(HibernateException e) {
                logger.error("Error persisting user.", e);
            }                
        }        
    }
    
    private void makePersistentAction(User user)
    {
        userWorkflow.makePersistent(user);
    }
    
    private Collection<User> getNonPersistentUsers(final Collection<User> users)
    {
        Collection<User> nonPersistentUsers = new HashSet<User>();
        
        if (users != null)
        {
            for (User user : users)
            {
                if (!userWorkflow.isPersistent(user)) {
                    nonPersistentUsers.add(user);
                }
            }
        }
        
        return nonPersistentUsers;
    }    
}
