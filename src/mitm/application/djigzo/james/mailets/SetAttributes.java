/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Similar to SetMailAttribute. The difference is that SetAttributes extends AbstractDjigzoMailet and
 * can therefore catch RunTimeExceptions and Errors and log some info. 
 * 
 * See {MailAttributes} for a more advanced version. 
 *
 * @author Martijn Brinkers
 *
 */
public class SetAttributes extends AbstractDjigzoMailet
{
	private final static Logger logger = LoggerFactory.getLogger(SetAttributes.class);

	/*
	 * The properties to set
	 */
    private Map<String, String> attributesToSet = Collections.synchronizedMap(new HashMap<String, String>());

    /*
     * The next processor (can be null in which case the processor is not changed)
     */
    private String nextProcessor;
    
	@Override
	protected Logger getLogger() {
		return logger;
	}

    @Override
    protected void initMailet()
    throws MessagingException    
    {
        Iterator<?> parameterIterator = getInitParameterNames();
        
        while (parameterIterator.hasNext()) 
        {
            String name = parameterIterator.next().toString();
            String value = getInitParameter(name);
            
            /*
             * processor should be handled differently
             */
            if ("processor".equalsIgnoreCase(name)) {
                nextProcessor = value;
            }
            else {
                attributesToSet.put(name,value);
            }
        }
    }
	
	@Override
	public void serviceMail(Mail mail) 
	{
		for (Map.Entry<String, String> entry : attributesToSet.entrySet())
		{
			mail.setAttribute(entry.getKey(), entry.getValue());
		}
		
		if (nextProcessor != null) {
		    mail.setState(nextProcessor);
		}
	}
}
