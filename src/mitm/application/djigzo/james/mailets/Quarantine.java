/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.io.IOException;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.mail.repository.MailStorer;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionManager;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Quarantine stores the email in the Quarantine repository.
 * 
 * @author Martijn Brinkers
 *
 */
public class Quarantine extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(Quarantine.class);

    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ERROR_PROCESSOR ("errorProcessor");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    };

    /*
     * The processor to for a relayed message
     */
    private String errorProcessor;

    /*
     * Used to store the email
     */
    private MailStorer quarantineMailStorer;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
        
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    private void initErrorProcessor()
    throws MessagingException
    {
        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
        
        if (StringUtils.isBlank(errorProcessor)) {
            throw new MessagingException("errorProcessor must be specified.");
        }
    }
    
    @Override
    final public void initMailet()
    throws MessagingException
    {
    	getLogger().info("Initializing mailet: " + getMailetName());

    	initErrorProcessor();
    	
        StrBuilder sb = new StrBuilder();

        sb.append("errorProcessor: ");
        sb.append(errorProcessor);
        
        getLogger().info(sb.toString());
    	
    	sessionManager = SystemServices.getSessionManager();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);

        quarantineMailStorer = SystemServices.getQuarantineMailStorer();
    }
    
    private void handleMessageAction(Session session, Mail mail)
    throws DatabaseException
    {
        try {
            quarantineMailStorer.store(mail);
            
            DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
            
            logger.info("Mail was quarantined. MailID: " + mailAttributes.getMailID() + ", Quarantine ID: " + 
                    mailAttributes.getMailRepositoryID());
        }
        catch (MessagingException e) {
            throw new DatabaseException(e);
        }
        catch (IOException e) {
            throw new DatabaseException(e);
        }
    }    
    
    @Override
    public void serviceMail(final Mail mail) 
    {
        try {
            actionExecutor.executeTransaction(
                    new DatabaseVoidAction()
                    {
                        @Override
                        public void doAction(Session session)
                        throws DatabaseException
                        {
                            Session previousSession = sessionManager.getSession();

                            sessionManager.setSession(session);

                            try {
                                handleMessageAction(session, mail);
                            }
                            finally {
                                sessionManager.setSession(previousSession);                            
                            }
                        }
            }, ACTION_RETRIES);
        }
        catch (Error e)
        {
            getLogger().error("Error handling the message.", e);
            
            mail.setState(errorProcessor);
        }
        catch (Exception e)
        {
        	getLogger().error("Exception handling the message.", e);
        	
        	mail.setState(errorProcessor);
        }
    }

    /*
     * For testing purposes
     */
    public void setQuarantineMailStorer(MailStorer quarantineMailStorer) {
        this.quarantineMailStorer = quarantineMailStorer;
    }
}
