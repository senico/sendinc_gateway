/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.MimeMessageWithID;
import mitm.common.mail.matcher.ContentHeaderNameMatcher;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.NotHeaderNameMatcher;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that takes the part(s) of the source message and wraps a multipart/mixed around the message
 * 
 * @author Martijn Brinkers
 *
 */
public class Attach extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(Attach.class);
    
    /*
     * If true the original Message-ID will be used for the signed message
     */
    private boolean retainMessageID = true;

    /*
     * If set, the attachment will have this filename
     */
    private String filename;
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        RETAIN_MESSAGE_ID ("retainMessageID"),
        FILE_NAME         ("filename");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
    
    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    final public void initMailet()
    {
        getLogger().info("Initializing mailet: " + getMailetName());
        
        String param = getInitParameter(Parameter.RETAIN_MESSAGE_ID.name);
        
        if (param != null) {
            retainMessageID = BooleanUtils.toBoolean(param);
        }

        filename = getInitParameter(Parameter.FILE_NAME.name, filename);
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("Retain Message-ID: ");
        sb.append(retainMessageID);
        sb.append("; Filename: ");
        sb.append(filename);
        
        getLogger().info(sb.toString());
    }
    
    @Override
    public void serviceMail(Mail mail)
    {
        try {
            MimeMessage sourceMessage = mail.getMessage();
            
            MimeMessage newMessage = retainMessageID ? new MimeMessageWithID(MailSession.getDefaultSession(), 
                    sourceMessage.getMessageID()) : new MimeMessage(MailSession.getDefaultSession());
            
            if (StringUtils.isNotEmpty(filename)) {
                newMessage.setFileName(filename);
            }
            
            Multipart mp = new MimeMultipart();
            
            HeaderMatcher contentMatcher = new ContentHeaderNameMatcher();
            
            mp.addBodyPart(BodyPartUtils.makeContentBodyPart(sourceMessage, contentMatcher));
            
            newMessage.setContent(mp);
        
            /* 
             * create a matcher that matches on everything expect content-* 
             */
            HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);
           
            /* 
             * copy all non-content headers from source message to the new message 
             */
            HeaderUtils.copyHeaders(sourceMessage, newMessage, nonContentMatcher);
                        
            newMessage.saveChanges();
            
            mail.setMessage(newMessage);
        }
        catch (MessagingException e) {
            getLogger().error("Error attaching the message.", e);
        } 
        catch (IOException e) {
            getLogger().error("Error attaching the message.", e);
        }        
    }
}
