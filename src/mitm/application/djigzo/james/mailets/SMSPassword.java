/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.Address;
import javax.mail.MessagingException;

import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.PasswordContainer;
import mitm.application.djigzo.james.Passwords;
import mitm.application.djigzo.james.PhoneNumber;
import mitm.application.djigzo.james.PhoneNumbers;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.sms.SMS;
import mitm.common.sms.SMSGateway;
import mitm.common.sms.SMSGatewayException;
import mitm.common.sms.hibernate.SMSImpl;
import mitm.common.template.TemplateBuilder;
import mitm.common.util.Check;

import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Mailet that will send passwords that are attached to the Mail (see {@link DjigzoMailAttributes#getPasswords()}) to the SMS
 * gateway. The phone numbers to use will be retrieved from the Mail attributes (see {@link DjigzoMailAttributes#getPhoneNumbers()}) 
 * 
 * Supported mailet parameters:
 * 
 * template                : The SMS template file (Freemarker template). The file must exist.
 * failureProcessor        : The next processor to use when the SMS cannot be sent
 * catchRuntimeExceptions  : If true all RunTimeExceptions are caught (default: true)
 * catchErrors             : If true all Errors are caught (default: true)
 * 
 * The following template parameters can be used:
 * 
 * from       : The sender of the SMS 
 * password   : The password
 * passwordID : The password ID
 * recipient  : The email address of the recipient
 * 
 * @author Martijn Brinkers
 *
 */
public class SMSPassword extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(SMSPassword.class);

    /*
     * The Freemarker template for the message.
     */
    private String templateFile;
    
    /*
     * The SMSGateway will send the SMS using the registered transport.
     */
    private SMSGateway smsGateway;

    /*
     * For building Freemarker templates
     */
    private TemplateBuilder templateBuilder;
    
    /*
     * The processor to use when a failure occurs (like missing phone number).
     */
    private String failureProcessor;
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        MAX_EMAIL_LENGTH  ("maxEmailLength"),
        TEMPLATE          ("template"),        
        FAILURE_PROCESSOR ("failureProcessor");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
    
    protected void loadTemplate(StrBuilder sb)
    {
        templateFile = getInitParameter(Parameter.TEMPLATE.name);
        
        if (templateFile == null) {
            throw new IllegalArgumentException("template must be specified.");
        }

        /*
    	 * Validate the template (if it can be found)
    	 */
        try {
        	templateBuilder.getTemplate(templateFile);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Error loading the template.", e);
        }
        
        sb.append("Template: ").append(templateFile);
    }
        
    private void initFailureProcessor(StrBuilder sb)
    {
        failureProcessor = getInitParameter(Parameter.FAILURE_PROCESSOR.name);        

        sb.append("; failureProcessor: ").append(failureProcessor);
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
    	getLogger().info("Initializing mailet: " + getMailetName());

        smsGateway = SystemServices.getSMSGateway();
        templateBuilder = SystemServices.getTemplateBuilder();
    	
        StrBuilder sb = new StrBuilder();

        loadTemplate(sb);
        
        initFailureProcessor(sb);
                
        getLogger().info(sb.toString());
    }

    private Passwords getPasswordsAttribute(Mail mail)
    {
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        return attributes.getPasswords();
    }

    private PhoneNumbers getPhoneNumbersAttribute(Mail mail)
    {
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        return attributes.getPhoneNumbers();
    }
    
    /*
     * Returns the recipients that have valid phone numbers and passwords.
     */
    private Set<String> getValidRecipients(Set<String> uniqueRecipients, Passwords passwords, PhoneNumbers phoneNumbers)
    {
        Set<String> validRecipients = new HashSet<String>();
        
        if (passwords == null) 
        {
        	getLogger().warn("Passwords attribute not found.");
            
            return validRecipients;
        }        
        
        if (phoneNumbers == null)
        {
        	getLogger().warn("Passwords attribute not found.");
        
            return validRecipients;
        }
        
        /*
         * Check that for every password there is a phone number and vice versa
         */        
        if (!passwords.keySet().equals(phoneNumbers.keySet())) 
        {
        	getLogger().warn("Passwords and PhoneNumbers do not match up.");
            
            return validRecipients;
        }

        /*
         * Check that for every recipient there is a password.
         */        
        for (String recipient : uniqueRecipients)
        {
            if (passwords.containsKey(recipient))
            {
                validRecipients.add(recipient);
            }
            else {
                /*
                 * Just report and continue processing.
                 */
            	getLogger().warn("Password for recipient " + recipient + " is missing.");
            }
        }
        
        return validRecipients;
    }

    protected Address getFrom(Mail mail)
    {
        Address from = null;
        
        try {
            Address[] froms = mail.getMessage().getFrom();
            
            from = (froms != null && froms.length > 0) ? froms[0] : null;
        }
        catch (MessagingException e) {
            logger.error("Error getting from.", e);
        }
        
        return from;
    }
    
    protected Template getTemplate(Mail mail)
    throws IOException, MessagingException 
    {
    	return templateBuilder.getTemplate(templateFile);
    }
    
    private String getSMSBody(Mail mail, Address from, PasswordContainer passwordContainer, String recipient) 
    throws TemplateException, IOException, EncryptorException, MessagingException
    {
        SimpleHash root = new SimpleHash();
        
        root.put("from",       from);
        root.put("password",   passwordContainer.getPassword());
        root.put("passwordID", passwordContainer.getPasswordID());
        root.put("recipient",  recipient);

        StringWriter writer = new StringWriter();

        getTemplate(mail).process(root, writer);

        return writer.toString();
    }
    
    private void sendPasswords(Mail mail, Set<String> uniqueRecipients, Passwords passwords, PhoneNumbers phoneNumbers) 
    throws TemplateException, IOException, MessagingException, SMSGatewayException, EncryptorException
    {
        Address from = getFrom(mail);
        
        for (String recipient : uniqueRecipients)
        {
            PasswordContainer passwordContainer = passwords.get(recipient);
            PhoneNumber phoneNumber = phoneNumbers.get(recipient);
                
            Check.notNull(passwordContainer, "passwordContainer");
            Check.notNull(phoneNumber, "phoneNumber");
            
            String smsBody = getSMSBody(mail, from, passwordContainer, recipient);
            
            SMS sms = new SMSImpl(phoneNumber.getNumber(), smsBody, null);
            
            smsGateway.sendSMS(sms);
        }
    }
    
    private Set<String> getUniqueRecipients(Mail mail)
    {
        Set<String> uniqueRecipients = new HashSet<String>();

        Collection<MailAddress> recipients = MailAddressUtils.getRecipients(mail);
        
        for (MailAddress recipient : recipients)
        {
            String email = recipient.toInternetAddress().getAddress();
            
            email = EmailAddressUtils.canonicalizeAndValidate(email, true);
            
            if (email != null) {
                uniqueRecipients.add(email);
            }
        }
        
        return uniqueRecipients;
    }
    
    @Override
    public void serviceMail(Mail mail) 
    {
        try {
            Passwords passwords = getPasswordsAttribute(mail);
            PhoneNumbers phoneNumbers = getPhoneNumbersAttribute(mail);
            
            Set<String> uniqueRecipients = getUniqueRecipients(mail);
            
            Set<String> validRecipients = getValidRecipients(uniqueRecipients, passwords, phoneNumbers);
            
            if (validRecipients.size() > 0)
            {
                sendPasswords(mail, validRecipients, passwords, phoneNumbers);
            }
            else {
                if (failureProcessor != null)
                {
                    mail.setState(failureProcessor);
                }
            }
        }
        catch (TemplateException e) {
        	getLogger().error("Error reading template.", e);
        }
        catch (IOException e) {
        	getLogger().error("IOException.", e);
        }
        catch (MessagingException e) {
        	getLogger().error("MessagingException.", e);
        }
        catch (SMSGatewayException e) {
        	getLogger().error("Error sending the SMS.", e);
        }
        catch (EncryptorException e) {
        	getLogger().error("Unable to decrypt the password.", e);
        }
    }

    protected TemplateBuilder getTemplateBuilder() {
        return templateBuilder;
    }
}
