/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.james.SecurityInfoTags;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that adds a SMIMEInfoHandlerTags instance to the Mail attributes with values from the sender's properties.
 * 
 * @author Martijn Brinkers
 *
 */
public class AddSMIMEInfoHandlerTags extends AbstractTransactedMailet
{
    private final static Logger logger = LoggerFactory.getLogger(AddSMIMEInfoHandlerTags.class);

    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailetTransacted()
    throws MessagingException
    {
        userWorkflow = SystemServices.getUserWorkflow();

        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
    }
    
    @Override
    public void serviceMailTransacted(Mail mail)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
        
        if (originator != null)
        {
            try {
                UserProperties properties = userWorkflow.getUser(originator.getAddress(), 
                        UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).getUserPreferences().getProperties(); 
                
                DjigzoMailAttributesImpl.getInstance(mail).setSecurityInfoTags(new SecurityInfoTags(
                        properties.getSecurityInfoDecryptedTag(), 
                        properties.getSecurityInfoSignedValidTagTag(), 
                        properties.getSecurityInfoSignedByValidTagTag(), 
                        properties.getSecurityInfoSignedInvalidTagTag()));
            } 
            catch (HierarchicalPropertiesException e) {
                throw new MessagingException("Error adding S/MIME info to attributes", e);
            }
        }
    }
}
