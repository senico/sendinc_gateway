/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.security.PublicKey;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.commons.lang.mutable.MutableObject;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extension of DKIMVerify that reads the public key for DKIM verification from the
 * sender properties.
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderDKIMVerify extends DKIMVerify
{
    private final static Logger logger = LoggerFactory.getLogger(SenderDKIMVerify.class);
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;

    /*
     * The user property from which the DKIM key should be read 
     */
    private String keyProperty;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();
        
        keyProperty = getInitParameter("keyProperty");
        
        if (keyProperty == null) {
            throw new MessagingException("keyProperty is missing.");
        }
        
        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();

        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
    }

    /*
     * Gets called by MailAddressHandler#handleMailAddresses to handle the user
     */
    protected void onHandleUserEvent(User user, MutableObject pemKeyPairHolder)
    throws MessagingException
    {
        try {
            pemKeyPairHolder.setValue(user.getUserPreferences().
                    getProperties().getProperty(keyProperty, false));
        } 
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error reading DKIM key pair.", e);
        }
    }
    
    @Override
    public PublicKey getPublicKey(Mail mail)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
        
        final MutableObject pemKeyPairHolder = new MutableObject();
        
        MailAddressHandler.HandleUserEventHandler eventHandler = new MailAddressHandler.HandleUserEventHandler()
        {
            @Override
            public void handleUser(User user) 
            throws MessagingException 
            {
                onHandleUserEvent(user, pemKeyPairHolder);
            }
        };

        MailAddressHandler mailAddressHandler = new MailAddressHandler(sessionManager, userWorkflow, 
                actionExecutor, eventHandler, ACTION_RETRIES);
        
        mailAddressHandler.handleMailAddresses(MailAddressUtils.fromAddressArrayToMailAddressList(originator));
        
        return parseKey((String) pemKeyPairHolder.getValue());
    }    
}
