/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.security.password.PasswordGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that generates random passwords for all recipients. 
 * 
 * Supported mailet parameters:
 * 
 * baseTime                : Date (format yyyy-MM-dd) that will be subtracted from the current time (default: 0)
 * uniqueID                : If multiple servers are used this is used to make sure the password ID is unique
 * defaultPasswordLength   : The default length of the generated passwords (in bytes, default: 8 bytes)
 * catchRuntimeExceptions  : If true all RunTimeExceptions are caught
 * catchErrors             : If true all Errors are caught
 * 
 * @author Martijn Brinkers
 *
 */
public class GeneratePassword extends AbstractGeneratePassword
{
    private final static Logger logger = LoggerFactory.getLogger(GeneratePassword.class);

    /*
     * Generator used for generating the passwords.
     */
    private PasswordGenerator passwordGenerator;
    
    @Override
    protected Logger getLogger() {
    	return logger;
    }
        
    @Override
    public void initMailet() 
    throws MessagingException 
    {
    	super.initMailet();
    	
        passwordGenerator = SystemServices.getPasswordGenerator();
    }
        
    @Override
    protected String generatePassword(User user, int passwordLength, String passwordID)
    throws MessagingException
    {
        return passwordGenerator.generatePassword(passwordLength);
    }
}
