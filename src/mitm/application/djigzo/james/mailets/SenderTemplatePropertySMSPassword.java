/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.io.IOException;
import java.io.StringReader;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Template;

/**
 * Extension of SMSPassword that makes it possible to read the template from the sender's properties
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderTemplatePropertySMSPassword extends SMSPassword
{
	private final static Logger logger = LoggerFactory.getLogger(SenderTemplatePropertySMSPassword.class);
	
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
	
    /*
     * The activation context keys
     */
    private final static String TEMPLATE_SOURCE_ACTIVATION_CONTEXT_KEY = "templateSource";
    private final static String TEMPLATE_ACTIVATION_CONTEXT_KEY = "template";
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        TEMPLATE_PROPERTY ("templateProperty");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
    
    /*
     * The name of the property from which the template is read
     */
    private String templateProperty;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
	
    @Override
    protected Logger getLogger() {
    	return logger;
    }
        
    @Override
    public void initMailet() 
    throws MessagingException 
    {
    	super.initMailet();
    
        templateProperty = getInitParameter(Parameter.TEMPLATE_PROPERTY.name);
        
        if (templateProperty == null) {
        	throw new IllegalArgumentException("templateProperty is missing.");
        }
        
        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();

        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        assert(actionExecutor != null);
        
        getLogger().info("templateProperty " + templateProperty);
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    protected void onHandleUserEvent(User user)
    throws MessagingException 
    {
    	String templateSource;
    	
		try {
			templateSource = user.getUserPreferences().getProperties().getProperty(templateProperty, false);
		} 
		catch (HierarchicalPropertiesException e) {
			throw new MessagingException("Error getting user property " + templateProperty, e);
		}
    	
    	getActivationContext().set(TEMPLATE_SOURCE_ACTIVATION_CONTEXT_KEY, templateSource);
    }
    
    @Override
    protected Template getTemplate(Mail mail)
    throws IOException, MessagingException 
    {
    	/*
    	 * Check if we already have a cached template for this thread. When there are multiple recipients getTemplate is called
    	 * multiple times. Because the template depends on the sender we only have to create the template once
    	 * for for every mailet activation
    	 */
    	Template template = getActivationContext().get(TEMPLATE_ACTIVATION_CONTEXT_KEY, Template.class);
    	
    	if (template != null)
    	{
    		/*
    		 * Return the cached template
    		 */
    		return template;
    	}
    	
    	InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
    	
    	if (templateProperty != null && originator != null)
    	{
        	MailAddressHandler.HandleUserEventHandler eventHandler = new MailAddressHandler.HandleUserEventHandler()
        	{
    			@Override
                public void handleUser(User user) 
    			throws MessagingException 
    			{
    				onHandleUserEvent(user);
    			}
        	};

        	MailAddressHandler mailAddressHandler = new MailAddressHandler(sessionManager, userWorkflow, 
        			actionExecutor, eventHandler, ACTION_RETRIES);
    		
    		mailAddressHandler.handleMailAddresses(MailAddressUtils.fromAddressArrayToMailAddressList(originator));
    		
    		String templateSource = getActivationContext().get(TEMPLATE_SOURCE_ACTIVATION_CONTEXT_KEY, String.class);
    		
    		if (templateSource != null)
    		{
	        	StringReader reader = new StringReader(templateSource);
	        	
	        	template = getTemplateBuilder().createTemplate(reader);
    		}
    	}
    	
    	if (template == null) 
    	{
    		getLogger().debug("No template found for " + originator);
    		/*
    		 * The sender was not a valid user or did not have a template. Use the default template
    		 */
    		template = super.getTemplate(mail);
    	}
    	
    	/*
    	 * Store the template so we can reuse it when there are multiple recipients
    	 */
    	getActivationContext().set(TEMPLATE_ACTIVATION_CONTEXT_KEY, template);
    	
    	return template;
    }

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public UserWorkflow getUserWorkflow() {
		return userWorkflow;
	}

	public MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
		return messageOriginatorIdentifier;
	}

	public DatabaseActionExecutor getActionExecutor() {
		return actionExecutor;
	}
}
