/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.RegExHeaderNameMatcher;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that removes headers (matched against the header name and not header value) that match a regular expression.
 * 
 * @author Martijn Brinkers
 *
 */
public class RemoveHeaders extends AbstractDjigzoMailet
{
	private final static Logger logger = LoggerFactory.getLogger(RemoveHeaders.class);
	
	private Pattern pattern;
	
	@Override
	protected Logger getLogger() {
		return logger;
	}

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        PATTERN ("pattern");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
	
    @Override
    final public void initMailet()
    throws MessagingException
    {
    	getLogger().info("Initializing mailet: " + getMailetName());
        
        String param = getInitParameter(Parameter.PATTERN.name);
        
        if (param == null) {
        	throw new MessagingException(Parameter.PATTERN.name + " must be specified.");
        }
        
        pattern = Pattern.compile(param);
    }
    
	@Override
	public void serviceMail(Mail mail)
	{
		
		try {
			HeaderMatcher matcher = new RegExHeaderNameMatcher(pattern);

			List<String> headersToRemove = new LinkedList<String>();
			
			MimeMessage message = mail.getMessage();
			
	        for (Enumeration<?> headerEnum = message.getAllHeaders(); headerEnum.hasMoreElements();) 
	        {
	            Header header = (Header)headerEnum.nextElement();
	            
	            if (matcher.isMatch(header)) {
	            	headersToRemove.add(header.getName());
	            }
	        }
	        
	        if (headersToRemove.size() > 0)
	        {
		        for (String header : headersToRemove) {
		        	message.removeHeader(header);
		        }
	        }
		} 
		catch (MessagingException e) {
        	getLogger().error("Error handling message.", e);
		} 
	}
}
