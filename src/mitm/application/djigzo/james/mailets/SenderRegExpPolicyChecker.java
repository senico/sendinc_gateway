/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.io.IOException;
import java.util.Collection;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.dlp.PolicyPatternNodeUtils;
import mitm.application.djigzo.dlp.UserPreferencesPolicyPatternManager;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.dlp.PolicyPattern;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;

import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that checks whether the message violates some regular expression policies. The policies are 
 * retrieved from the sending users preferences.
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderRegExpPolicyChecker extends AbstractRegExpPolicyChecker
{
    private final static Logger logger = LoggerFactory.getLogger(SenderRegExpPolicyChecker.class);
 
    /*
     * Activation context key
     */
    private final static String ACTIVATION_CONTEXT_KEY = "context";
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;

    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    /*
     * Used for getting the policy patterns from a user
     */
    private UserPreferencesPolicyPatternManager policyManager;
    
    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    {
        super.initMailet();
        
        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();

        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        policyManager = SystemServices.getUserPreferencesPolicyPatternManager();
    }
    
    /*
     * Gets called by MailAddressHandler#handleMailAddresses to handle the user
     */
    protected void onHandleUserEvent(User user)
    throws MessagingException 
    {
        Collection<PolicyPattern> policyPatterns = null;
        
        policyPatterns = PolicyPatternNodeUtils.getPolicyPatterns(policyManager.getPatterns(
                user.getUserPreferences()));
        
        getActivationContext().set(ACTIVATION_CONTEXT_KEY, policyPatterns);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    protected Collection<PolicyPattern> getPolicyPatterns(Mail mail)
    throws MessagingException, IOException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);
        
        MailAddressHandler.HandleUserEventHandler eventHandler = new MailAddressHandler.HandleUserEventHandler()
        {
            @Override
            public void handleUser(User user) 
            throws MessagingException 
            {
                onHandleUserEvent(user);
            }
        };

        MailAddressHandler mailAddressHandler = new MailAddressHandler(sessionManager, userWorkflow, 
                actionExecutor, eventHandler, ACTION_RETRIES);
        
        mailAddressHandler.handleMailAddresses(MailAddressUtils.fromAddressArrayToMailAddressList(originator));
        
        return getActivationContext().get(ACTIVATION_CONTEXT_KEY, Collection.class);
    }
}
