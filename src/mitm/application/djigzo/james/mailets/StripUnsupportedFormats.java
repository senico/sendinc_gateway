/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.filter.UnsupportedFormatStripper;
import mitm.common.mail.filter.UnsupportedFormatStripper.MatchingPair;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that can remove unwanted attachments. Typically this mailet is used in combination with the 
 * Djigzo BlackBerry addon for the removal of attachments that are not supported on the BlackBerry.
 * 
 * The following mailet parameters are supported:
 * 
 * keepSMIMEParts   : If true S/MIME parts won't be removed
 * maxMessageSize   : If new message is is larger than maxMessageSize new parts will be skipped
 * contentWildcard  : Content types that are not stripped  
 * filenameWildcard : Filename extenstions that are not stripped
 * 
 * THe following example shows how you can specify multiple attachment filters:
 * 
 * <contentWildcard.1>text/html</contentWildcard.1>
 * <contentWildcard.2>*</contentWildcard.2>
 * <filenameWildcard.2>*.html, *.txt</filenameWildcard.2>
 *
 * contentWildcard must be specified if filenameWildcard is specified (use * if all content types
 * are acceptable)
 * 
 * @author Martijn Brinkers
 *
 */
public class StripUnsupportedFormats extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(StripUnsupportedFormats.class);

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        KEEP_SMIME_PARTS  ("keepSMIMEParts"),
        MAX_MESSAGE_SIZE  ("maxMessageSize"),
        CONTENT_WILDCARD  ("contentWildcard"),
        FILENAME_WILDCARD ("filenameWildcard");

        private String name;

        private Parameter(String name) {
            this.name = name;
        }
        
        public String getName() {
            return name;
        }
    };

    /*
     * If true S/MIME parts won't be removed 
     */
    private boolean keepSMIMEParts = true;

    /*
     * If new message is is larger than maxMessageSize new parts will be skipped 
     */
    private int maxMessageSize = 1024 * 1024 * 32;
    
    /*
     * Collection of matchers that will match the attachments we will support
     */
    private final List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();
    
    /*
     * Scans the mail and removes all attachments that are not supported
     */
    private UnsupportedFormatStripper unsupportedFormatStripper;
    
    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    private void initAttachmentMatchers()
    {
        Iterator<?> it = getInitParameterNames();
        
        while (it.hasNext())
        {
            Object o = it.next();
            
            if (!(o instanceof String)) {
                continue;
            }
            
            String name = (String)o;
            
            if (StringUtils.startsWithIgnoreCase(name, Parameter.CONTENT_WILDCARD.getName()))
            {
                String contentWildcard = getInitParameter(name);
                
                if (StringUtils.isBlank(contentWildcard)) {
                    continue;
                }
                
                name = StringUtils.replaceOnce(name, Parameter.CONTENT_WILDCARD.getName(), 
                        Parameter.FILENAME_WILDCARD.getName());
                
                String filenameWildcard = getInitParameter(name);
                
                if (StringUtils.isBlank(filenameWildcard)) {
                    filenameWildcard = "*";
                }
                
                String[] wildcards = StringUtils.split(filenameWildcard, ',');

                for (String wildcard : wildcards)
                {
                    attachmentMatchers.add(new UnsupportedFormatStripper.MatchingPair(
                            contentWildcard.trim(), wildcard.trim()));
                }
            }
        }
    }
    
    @Override
    final public void initMailet()
    {
        getLogger().info("Initializing mailet: " + getMailetName());
        
        String param = getInitParameter(Parameter.KEEP_SMIME_PARTS.name);
        
        if (param != null) {
            keepSMIMEParts = BooleanUtils.toBoolean(param);
        }
        
        param = getInitParameter(Parameter.MAX_MESSAGE_SIZE.name);
        
        if (param != null) {
            maxMessageSize = NumberUtils.toInt(param, maxMessageSize);
        }
        
        initAttachmentMatchers();

        unsupportedFormatStripper = new UnsupportedFormatStripper(attachmentMatchers);

        unsupportedFormatStripper.setKeepSMIMEParts(keepSMIMEParts);
        unsupportedFormatStripper.setMaxMessageSize(maxMessageSize);
        
        StrBuilder sb = new StrBuilder();
        
        sb.append("keepSMIMEParts: ");
        sb.append(keepSMIMEParts);
        sb.appendSeparator("; ");
        sb.append("maxMessageSize: ");
        sb.append(maxMessageSize);
        sb.appendSeparator("; ");
        sb.append("Attachment matchers: ");
        sb.append(attachmentMatchers);
        
        getLogger().info(sb.toString());
    }
    
    @Override
    public void serviceMail(Mail mail) 
    {
        try {
            MimeMessage filtered = unsupportedFormatStripper.strip(mail.getMessage());
            
            if (filtered != null)
            {
                filtered.saveChanges();
                
                /*
                 * A new MimeMessage instance will be created. This makes ure that the 
                 * MimeMessage can be written by James (using writeTo()).
                 */
                mail.setMessage(new MimeMessage(filtered));
            }
        }
        catch (MessagingException e) {
            logger.error("Error stripping unsupported formats.", e);
        }
    }
}
