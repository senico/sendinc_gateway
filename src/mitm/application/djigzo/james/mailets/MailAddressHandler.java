/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.Collection;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.User;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionRetryEvent;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.mailet.MailAddress;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class used to get User instances from email addresses within a database transaction.
 * 
 * @author Martijn Brinkers
 *
 */
public class MailAddressHandler
{
	private final static Logger logger = LoggerFactory.getLogger(MailAddressHandler.class);
	
    private final SessionManager sessionManager;
    
    private final UserWorkflow userWorkflow;
    
    private final DatabaseActionExecutor actionExecutor;

    /*
     * The event handler instance to call when a User is found
     */
    private final HandleUserEventHandler eventHandler;
    
    /*
     * The event to call when a retry is done
     */
    private final DatabaseActionRetryEvent retryEvent;
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs. Retrying an action
     * cannot always be done because sometimes actions in the handler method "handleUser" are not transactional.
     */
    private final int actionRetries;
    
    /*
     * The event handler interface
     */
    public static interface HandleUserEventHandler
    {
        public void handleUser(User user)
        throws MessagingException;
    }
    
    public MailAddressHandler(SessionManager sessionManager, UserWorkflow userWorkflow, DatabaseActionExecutor actionExecutor,
    		HandleUserEventHandler eventHandler, int actionRetries)
    {
    	this(sessionManager, userWorkflow, actionExecutor, eventHandler, actionRetries, null);
    }
    
    public MailAddressHandler(SessionManager sessionManager, UserWorkflow userWorkflow, DatabaseActionExecutor actionExecutor,
    		HandleUserEventHandler eventHandler, int actionRetries, DatabaseActionRetryEvent retryEvent)
    {
    	Check.notNull(sessionManager, "sessionManager");
    	Check.notNull(userWorkflow, "userWorkflow");
    	Check.notNull(actionExecutor, "actionExecutor");
    	Check.notNull(eventHandler, "eventHandler");
    	
    	this.sessionManager = sessionManager;
    	this.userWorkflow = userWorkflow;
    	this.actionExecutor = actionExecutor;
    	this.eventHandler = eventHandler;
    	this.actionRetries = actionRetries;
    	this.retryEvent = retryEvent;
    }

    protected void handleMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        if (mailAddresses == null) {
            return;
        }
        
        try {
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        Session previousSession = sessionManager.getSession();
                    	
                        sessionManager.setSession(session);
                        
                        try {
                        	handleMailAddressesAction(mailAddresses);
                        }
                        finally {
                        	sessionManager.setSession(previousSession);                            
                        }
                    }
                }, actionRetries, retryEvent);
        }
        catch(DatabaseException e) {
            throw new MessagingException("Error in handleRecipientsAction.", e);
        }
        catch(HibernateException e) {
            throw new MessagingException("HibernateException.", e);
        }
    }
    
    private void handleMailAddressesAction(Collection<MailAddress> mailAddresses)
    throws DatabaseException
    {
        try {
            for (MailAddress mailAddress : mailAddresses)
            {
                if (mailAddress != null)
                {
                    InternetAddress emailAddress = mailAddress.toInternetAddress();
                    
                    /*
                     * We will only accept valid email addresses. If an email address is invalid it will be
                     * converted to EmailAddressUtils.INVALID_EMAIL
                     */
                    String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(emailAddress.getAddress(), false);
                    
                    if (validatedEmail != null)
                    {
                        User user;
                        
						try {
							user = userWorkflow.getUser(validatedEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
						} 
						catch (HierarchicalPropertiesException e) {
							throw new DatabaseException(e);
						}
    
						/*
						 * Call the event handler with the found user
						 */
                        eventHandler.handleUser(user);
                    }
                    else {
                        logger.debug(emailAddress + " is not a valid email address.");
                    }
                }
            }
        }
        catch(MessagingException e) {
            throw new DatabaseException(e);
        } 
    }    
}
