/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Names of special email addresses.
 * 
 * @author Martijn Brinkers
 *
 */
public enum SpecialAddress
{
    SAME_AS_RECIPIENTS ("sameAsRecipients"),
    SAME_AS_MESSAGE    ("sameAsMessage"),
    ORIGINATOR         ("originator"),
    REPLY_TO           ("replyTo"),
    SENDER             ("sender"),
    FROM               ("from"),
    NULL               ("null");
    
    private String name;
 
    private static final Pattern PATTERN = Pattern.compile("(?:\\$\\{)([^}]+)(?:\\})");
    
    private SpecialAddress(String name) {
        this.name = name;
    }
    
    public static SpecialAddress fromName(String name)
    {
        name = StringUtils.trimToNull(name);
        
        if (name != null)
        {
            Matcher matcher = PATTERN.matcher(name);
            
            if (matcher.matches())
            {
                name = StringUtils.trimToNull(matcher.group(1)); 
                    
                for (SpecialAddress specialAddress : SpecialAddress.values()) 
                {
                    if (specialAddress.name.equalsIgnoreCase(name)) {
                        return specialAddress;
                    }
                }
            }
        }
        
        return null;
    }
}
