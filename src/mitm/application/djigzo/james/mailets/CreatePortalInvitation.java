/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.PortalInvitationValidator.ValidatorException;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.PortalInvitationValidator;
import mitm.application.djigzo.james.PortalInvitationDetails;
import mitm.application.djigzo.james.PortalInvitations;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that creates an invitation detail for every recipient. 
 * 
 * Supported mailet parameters:
 * 
 * catchRuntimeExceptions : If true all RunTimeExceptions are caught
 * catchErrors            : If true all Errors are caught
 * 
 * @author Martijn Brinkers
 *
 */
public class CreatePortalInvitation extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(CreatePortalInvitation.class);
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;

    /*
     * The activation context key
     */
    protected final static String ACTIVATION_CONTEXT_KEY = "invites";
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;
    
    /*
     * Callback used to track user objects.
     */
    private static class ActivationContext
    {
        /*
         * The invitations
         */
        private final PortalInvitations invitations = new PortalInvitations();
        
        public PortalInvitations getInvitations() {
            return invitations;
        }
    }
    
    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    @Override
    public void initMailet() 
    throws MessagingException 
    {
        super.initMailet();
        
        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();

        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        assert(actionExecutor != null);
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    protected void onHandleUserEvent(User user)
    throws MessagingException 
    {
        try {
            ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY, 
                    ActivationContext.class);
            
            Check.notNull(activationContext, "activationContext");
            
            String serverSecret = user.getUserPreferences().getProperties().getServerSecret();
            
            if (StringUtils.isNotEmpty(serverSecret))
            {
                PortalInvitationValidator validator = new PortalInvitationValidator();
                
                long timestamp = System.currentTimeMillis();
                
                validator.setEmail(user.getEmail());
                validator.setTimestamp(timestamp);
                
                activationContext.getInvitations().put(user.getEmail(),  
                        new PortalInvitationDetails(timestamp, validator.calulateMAC(serverSecret)));
            }
            else {
                /*
                 * Should not happen because the server secret should always be set
                 */
                logger.warn("Server secret not set for user " + user.getEmail());
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error handling user " + user.getEmail(), e);
        } 
        catch (ValidatorException e) {
            throw new MessagingException("Error calculating MAC for user " + user.getEmail(), e);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            ActivationContext activationContext = new ActivationContext();
            
            /*
             * Place the ActivationContext in the context so we can use it in onHandleUserEvent
             */
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, activationContext);
            
            MailAddressHandler.HandleUserEventHandler eventHandler = new MailAddressHandler.HandleUserEventHandler()
            {
                @Override
                public void handleUser(User user) 
                throws MessagingException 
                {
                    onHandleUserEvent(user);
                }
            };

            MailAddressHandler mailAddressHandler = new MailAddressHandler(sessionManager, userWorkflow, 
                    actionExecutor, eventHandler, ACTION_RETRIES);
            
            mailAddressHandler.handleMailAddresses(MailAddressUtils.getRecipients(mail));
            
            DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);

            attributes.setPortalInvitations(activationContext.getInvitations());
        }
        catch(MessagingException e) {
            getLogger().error("Error creating invitions.", e);            
        }
    }
}
