/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.password.PasswordGenerator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that generates a client secret for all recipients that do not yet have a client secret
 * 
 * @author Martijn Brinkers
 *
 */
public class GenerateClientSecret extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(GenerateClientSecret.class);
    
    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    /*
     * Generator used for generating the client secrets.
     */
    private PasswordGenerator passwordGenerator;
    
    /*
     * manages database sessions
     */
    private SessionManager sessionManager;
    
    /*
     * Used for adding and retrieving users
     */
    private UserWorkflow userWorkflow;
    
    /*
     * Used to execute database actions in a transaction
     */
    private DatabaseActionExecutor actionExecutor;

    /*
     * The length in bytes of the generated client secret
     */
    private int clientSecretLength = 16;
    
    /*
     * Skip if the client secret was already set
     */
    private boolean skipIfClientSecretSet = true;
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        CLIENT_SECRET_LENGTH ("clientSecretLength"),
        SKIP_IF_SECRET_SET   ("skipIfClientSecretSet");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
    
    @Override
    protected Logger getLogger() {
        return logger;
    }
    
    @Override
    public void initMailet() 
    throws MessagingException 
    {
        super.initMailet();
        
        clientSecretLength = getIntegerInitParameter(Parameter.CLIENT_SECRET_LENGTH.name, clientSecretLength);
        skipIfClientSecretSet = getBooleanInitParameter(Parameter.SKIP_IF_SECRET_SET.name, skipIfClientSecretSet);
        
        passwordGenerator = SystemServices.getPasswordGenerator();
        
        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();

        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        assert(actionExecutor != null);
        
        StrBuilder sb = new StrBuilder();

        sb.append("clientSecretLength: ");
        sb.append(clientSecretLength);
        
        getLogger().info(sb.toString());
    }
    
    protected void onHandleUserEvent(User user)
    throws MessagingException 
    {
        UserProperties userProperties = user.getUserPreferences().getProperties();
        
        try {
            /*
             * Check if the user does not yet have a client secret. This is required to prevent 
             * a possible race condition. The check if the user has a secret is done with a matcher. 
             * After the check, it can happen that a client secret was already created within a different 
             * thread (hence the race condition).
             */
            if (StringUtils.isEmpty(userProperties.getClientSecret()) || !skipIfClientSecretSet)
            {
                userProperties.setClientSecret(passwordGenerator.generatePassword(clientSecretLength));
                
                /*
                 * Since we have created a client secret, we need to make the user persistent
                 */
                userWorkflow.makePersistent(user);
            }
        } 
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error creating client secret for user " + user.getEmail(), e);
        }
    }
    
    @Override
    public void serviceMail(Mail mail)
    {
        try {
            MailAddressHandler.HandleUserEventHandler eventHandler = new MailAddressHandler.HandleUserEventHandler()
            {
                @Override
                public void handleUser(User user) 
                throws MessagingException 
                {
                    onHandleUserEvent(user);
                }
            };

            MailAddressHandler mailAddressHandler = new MailAddressHandler(sessionManager, userWorkflow, 
                    actionExecutor, eventHandler, ACTION_RETRIES);
            
            mailAddressHandler.handleMailAddresses(MailAddressUtils.getRecipients(mail));            
        }
        catch(MessagingException e) {
            getLogger().error("Error generating client secrets.", e);            
        }
    }
}
