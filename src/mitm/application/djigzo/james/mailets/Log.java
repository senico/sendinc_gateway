/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.MessageOriginatorIdentifier;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.util.LogLevel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that logs comments and or info about a mail message to the logger.
 *
 * Supported mailet parameters:
 *
 * comment                 : Comment will be added to the log (is written first)
 * logLevel                : The log level of the logger (info, warn, error, debug). Default: info
 * logDetail               : The detail of logging (comment, basic, middle, full). Default: middle
 * catchRuntimeExceptions  : If true all RunTimeExceptions are caught (default: true)
 * catchErrors             : If true all Errors are caught (default: true)
 *
 * @author Martijn Brinkers
 *
 */
public class Log extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(Log.class);

    private enum LogDetail {COMMENT, BASIC, MIDDLE, FULL};

    /*
     * The comment that gets logged.
     */
    private String comment;

    /*
     * Default logDetail.
     */
    private LogDetail logDetail = LogDetail.BASIC;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    @Override
    protected Logger getLogger() {
    	return logger;
    }

    @Override
    final public void initMailet()
    throws MessagingException
    {
        comment = getInitParameter("comment");

        /*
         * We want to set the default loglevel to INFO if it's not specified
         */
        String parameter = getInitParameter("logLevel");

        if (parameter == null) {
            setLogLevel(LogLevel.INFO);
        }

        parameter = getInitParameter("logDetail");

        if (parameter != null) {
            logDetail = LogDetail.valueOf(parameter.trim().toUpperCase());
        }

        messageOriginatorIdentifier = SystemServices.getMessageOriginatorIdentifier();
    }

    /*
     * Returns the MailAddress as a String. If mailAddress is null <> will be returned
     */
    private String toString(MailAddress mailAddress)
    {
    	if (mailAddress == null) {
    		return "<>";
    	}

    	return mailAddress.toString();
    }

    /*
     * Returns the originator of the message (by default will return the from field)
     */
    private String getOriginator(Mail mail)
    {
        String originator = null;

        try {
            InternetAddress address = messageOriginatorIdentifier.getOriginator(mail);

            if (address != null)
            {
                originator = address.getAddress();

                /*
                 * We don't want the invalid@invalid.tld user in the logs
                 */
                if (EmailAddressUtils.isInvalidDummyAddress(originator)) {
                    originator = "<>";
                }
            }
        }
        catch (MessagingException e) {
            logger.debug("Error getting originator.", e);
        }

        if (StringUtils.isBlank(originator)) {
            originator = "<>";
        }

        return originator;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        StrBuilder strBuilder = new StrBuilder(256);

        strBuilder.setNewLineText("; ");

        strBuilder.append(comment);

        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        if (logDetail.compareTo(LogDetail.BASIC) >= 0)
        {
        	strBuilder.append(" | ");

        	String mailID = mailAttributes.getMailID();

        	strBuilder.append("MailID: ");
        	strBuilder.appendln(mailID);
            strBuilder.append("Originator: ");
            strBuilder.appendln(getOriginator(mail));
            strBuilder.append("Sender: ");
            strBuilder.appendln(toString(mail.getSender()));
        }

        if (logDetail.compareTo(LogDetail.MIDDLE) >= 0)
        {
        	strBuilder.append("Remote address: ");
        	strBuilder.appendln(mail.getRemoteAddr());
        	strBuilder.append("Recipients: ");
        	strBuilder.appendln(mail.getRecipients());

        	try {
        		if (mail.getMessage() != null)
        		{
        			String subject = mail.getMessage().getSubject();

        			strBuilder.append("Subject: ");
        			strBuilder.appendln(subject);
        		}
        	}
        	catch(MessagingException e) {
        		getLogger().error("Error getting subject.", e);
        	}

        	try {
        		if (mail.getMessage() != null)
        		{
        			String messageID = mail.getMessage().getMessageID();

        			strBuilder.append("Message-ID: ");
        			strBuilder.appendln(messageID);
        		}
        	}
        	catch(MessagingException e) {
        		getLogger().error("Error getting messageID.", e);
        	}
        }

        if (logDetail.compareTo(LogDetail.FULL) >= 0)
        {
        	strBuilder.append("Last updated: ");
        	strBuilder.appendln(mail.getLastUpdated());
        	strBuilder.append("Attribute names: ");
        	strBuilder.appendAll(mail.getAttributeNames());
        }

        logMessage(strBuilder.toString());
    }

    private void logMessage(String message)
    {
        switch(getLogLevel()) {
        case WARN  : getLogger().warn(message); break;
        case ERROR : getLogger().error(message); break;
        case DEBUG : getLogger().debug(message); break;
        case TRACE : getLogger().trace(message); break;
        default: break;
        }

        /*
         * default log level info
         */
        getLogger().info(message);
    }
}
