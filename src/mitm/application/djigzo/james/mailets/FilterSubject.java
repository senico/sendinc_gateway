/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.service.SystemServices;
import mitm.common.cache.PatternCache;
import mitm.common.util.RegExprUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mailet that filters the subject using a regular expression. The reg exp can either be static text or read 
 * from a Mail attribute.
 * 
 * @author Martijn Brinkers
 *
 */
public class FilterSubject extends AbstractDjigzoMailet
{
    private final static Logger logger = LoggerFactory.getLogger(FilterSubject.class);
    
    /*
     * The subject filter 
     */
    private String filter;
    
    /*
     * The name of the Mail attribute from which the filter will be read.
     */
    private String attribute;
    
    /*
     * Used for caching compiled Pattern's from a reg expr
     */
    private PatternCache patternCache;
    
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter 
    {
        FILTER    ("filter"),
        ATTRIBUTE ("attribute");
        
        private String name;
        
        private Parameter(String name) {
            this.name = name;
        }
    };
    
    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException    
    {
        filter = getInitParameter(Parameter.FILTER.name);
        
        attribute = getInitParameter(Parameter.ATTRIBUTE.name);
        
        patternCache = SystemServices.getPatternCache();
    }    
    
    private String[] getFilter(Mail mail)
    {
        /*
         * Use #toString on the attribute value 
         */
        String regExpValue = attribute != null ? ObjectUtils.toString(mail.getAttribute(attribute)) : filter;
        
        String[] result = RegExprUtils.splitRegExp(regExpValue);
        
        if (regExpValue != null && result == null) {
            logger.warn("Filter {} is not a valid filter", regExpValue);
        }
        
        return result;
    }
    
    @Override
    public void serviceMail(Mail mail)
    {
        try {
            MimeMessage message = mail.getMessage();

            String currentSubject = message.getSubject();

            if (currentSubject != null)
            {
                String[] filter = getFilter(mail);
                
                if (filter != null)
                {
                    Pattern pattern = patternCache.getPattern(StringUtils.defaultString(filter[0]));

                    if (pattern != null)
                    {
                        String newSubject = pattern.matcher(currentSubject).replaceAll(StringUtils.defaultString(
                                filter[1]));

                        logger.debug("Currrent subject: {}, new subject: {}", currentSubject, newSubject);

                        message.setSubject(newSubject);
                    }
                }
            }
            else {
                logger.debug("Subject is null");
            }
        }
        catch (MessagingException e) {
            logger.error("Error adding text to subject", e);
        }
    }
}
