/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import javax.mail.MessagingException;

import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionManager;

import org.apache.mailet.Mail;
import org.hibernate.Session;

/**
 * Abstract mailet that starts a database transaction when an email is handled.
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractTransactedMailet extends AbstractDjigzoMailet
{
    /*
     * manages database sessions
     */
    protected SessionManager sessionManager;
    
    /*
     * Used to execute database actions in a transaction
     */
    protected DatabaseActionExecutor actionExecutor;
        
    /**
     * Should be overridden to handle the mail. A database transaction has been started when this
     * method is called.
     */
    public abstract void serviceMailTransacted(Mail mail)
    throws MessagingException;

    public void initMailetTransacted()
    throws MessagingException
    {
        // empty on purpose
    }
    
    /**
     * Note: method is made final to make sure that extensions of this class do not 'remove' the 
     * required initialization. Override {@link #initMailetTransacted()} to add your own 
     * initialization code.
     */
    @Override
    public final void initMailet() 
    throws MessagingException 
    {
        super.initMailet();
    
        sessionManager = SystemServices.getSessionManager();

        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        initMailetTransacted();
    }
    
    /**
     * serviceMail is overridden to wrap the serviceMail call in a database transaction. 
     * 
     * Note: method is made final to make sure that extensions of this class do not 'remove' the 
     * transaction mechnism
     */
    @Override
    final public void serviceMail(final Mail mail)
    {
        try {
            actionExecutor.executeTransaction(
                    new DatabaseVoidAction()
                    {
                        @Override
                        public void doAction(Session session)
                        throws DatabaseException
                        {
                            Session previousSession = sessionManager.getSession();
    
                            sessionManager.setSession(session);
    
                            try {
                                /*
                                 * Call the super serviceMail method now wrapped in a transaction
                                 */
                                serviceMailTransacted(mail);
                            }
                            catch(MessagingException e) {
                                throw new DatabaseException(e);
                            }
                            finally {
                                sessionManager.setSession(previousSession);                            
                            }
                        }
            });
        }
        catch(DatabaseException e) {
            getLogger().error("Database error servicing email.", e);
        }
    }
}
