/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mitm.common.util.Check;

import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PropertyRegistry register all the known properies. This is used to make sure that properties accessed via the James 
 * config are known properties and to have some global structure that registers whether a property is an encrypted 
 * property. 
 * 
 * For example property names used in the James config cannot be statically checked by the compiler so to make sure 
 * that when property names are changed the James config will be updated as well we will do some checks when a 
 * property is used from a mailet.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertyRegistry 
{
	private final static Logger logger = LoggerFactory.getLogger(PropertyRegistry.class);
	
	/*
	 * Contains the names of all known properties and whether the property is encrypted
	 */
	private final Map<String, Boolean> registeredProperties = Collections.synchronizedMap(
	        new HashMap<String, Boolean>()); 

	public void registerProperty(String property, boolean encrypted)
	{
		Check.notNull(property, "");
	
		logger.info("Register property: " + property);
		
		registeredProperties.put(property, encrypted);
	}
	
	public boolean isRegisteredProperty(String property) {
		return registeredProperties.containsKey(property);
	}

    public boolean isEncrypted(String property)
    {
        Boolean encrypted = registeredProperties.get(property);
        
        if (encrypted == null) {
            logger.warn("Property " + property + " is not registered.");
        }
        
        return BooleanUtils.isTrue(encrypted);
    }
	
	private static class SingletonHolder { 
	     private final static PropertyRegistry INSTANCE = new PropertyRegistry();
	}
	 
	/**
	 * Returns the global (singleton) instance 
	 */
	public static PropertyRegistry getInstance() {
		return SingletonHolder.INSTANCE;
	}
}
