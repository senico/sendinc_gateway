/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mitm.application.djigzo.service.DjigzoServiceRegistry;
import mitm.common.security.JCEPolicyManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.DefaultFileComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class to configure Djigso system services.
 * 
 * @author Martijn Brinkers
 *
 */
public class DjigzoConfigurator
{
    private final static Logger logger = LoggerFactory.getLogger(DjigzoConfigurator.class);
 
    /*
     * Path to the spring config file.
     */
    private static String springConfig = "conf/spring/djigzo.xml";

    /*
     * Directory from which additional spring config files will be loaded
     */
    private static String additionalSpringConfigDir = "conf/spring/spring.d/";
    
    /*
     * Path to log4j properties. Default value is relative to james/bin
     */
    private static String log4jConfig = "../../conf/log4j.properties";
        
    private static void initializeLogging()
    {
        PropertyConfigurator.configure(log4jConfig);        
    }

    @SuppressWarnings("unchecked")
    private static void initializeRegistry()
    {
        /*
         * Load additional spring config files and sort them.
         */
        Collection<File> unsortedConfigFiles = FileUtils.listFiles(new File(additionalSpringConfigDir), 
                new String[]{"xml"}, true /* recursive */);
        
        ArrayList<File> sortedConfigFiles = new ArrayList<File>(unsortedConfigFiles); 
        
        Collections.sort(sortedConfigFiles, DefaultFileComparator.DEFAULT_COMPARATOR);
        
        String[] configFiles = new String[sortedConfigFiles.size() + 1];
        
        /*
         * The main config should be loaded first
         */
        configFiles[0] = springConfig;
        
        for (int i = 1; i < configFiles.length; i++) {
            configFiles[i] = sortedConfigFiles.get(i - 1).getPath();
        }
        
        logger.info("spring config files: {}", StringUtils.join(configFiles, ", "));
                
        DjigzoServiceRegistry.initialize(configFiles);        
    }
    
    private static void checkUnlimitedStrength()
    {
    	if (JCEPolicyManager.isUnlimitedStrength())
    	{
    		logger.info("The JCE unlimited strength policy is installed.");
    	}
    	else {
    		logger.warn("***** the JCE unlimited strength policy is NOT installed *****");
    	}
    }
    
    private static void printSystemProperties()
    {
        System.out.println();
        System.out.println("**** Begin system properties ****");
    	System.getProperties().list(System.out);
        System.out.println("**** End system properties ****");
        System.out.println();
    }

    private static void logAllPackages()
    {
    	List<Package> packages = Arrays.asList(Package.getPackages());
    	
    	Comparator<Package> packageComparator = new Comparator<Package>() 
    	{
			@Override
            public int compare(Package package1, Package package2) {
				return package1.getName().compareTo(package2.getName());
			}    		
    	};
    	
    	Collections.sort(packages, packageComparator);
    	
		StrBuilder sb = new StrBuilder(1024);

		sb.appendNewLine();

		for (Package pack : packages)
    	{
    		sb.append("Package: ").append(pack.getName()).appendSeparator("; ");
    		sb.append("Title: ").append(pack.getImplementationTitle()).appendSeparator("; ");
    		sb.append("Version: ").append(pack.getImplementationVersion());
    		sb.appendNewLine();
    	}

		logger.info(sb.toString());
    }
    
    public static void configure(String springConfig, String additionalSpringConfigDir, String log4jConfig)
    {
        System.out.println("Configuring Djigzo Service.");

        printSystemProperties();

        if (log4jConfig != null) {
            DjigzoConfigurator.log4jConfig = log4jConfig;
        }

        if (additionalSpringConfigDir != null) {
            DjigzoConfigurator.additionalSpringConfigDir = additionalSpringConfigDir;
        }

        if (springConfig != null) {
            DjigzoConfigurator.springConfig = springConfig;
        }                
        
        /* 
         * we will use println to make sure something is logged even when logger is not
         * properly initialized. 
         */
        System.out.println("spring config file: " + DjigzoConfigurator.springConfig);
        System.out.println("additional spring config dir: " + DjigzoConfigurator.additionalSpringConfigDir);
        System.out.println("log4j config file: " + DjigzoConfigurator.log4jConfig);
        System.out.println();
        
        initializeLogging();

        checkUnlimitedStrength();
        
        initializeRegistry();

        logAllPackages();
        
        logger.info("Djigzo Service Configured.");
    }
}
