/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import mitm.application.djigzo.UserLocality;
import mitm.common.util.Check;

/**
 * Represents a domain. 
 * 
 * Note: The DomainDTO pre-loads some user properties which are needed in the
 * domain overview. This is purely for optimization because otherwise an extra 
 * WS call need to be done for each domain which is really slow.  It would be 
 * better to refactor the web services. It want to specify which user properties
 * are pre-loaded. We do not want all properties to be pre-loaded (like the 
 * templates) because that would not scale. Currently all user properties 
 * are loaded on demand.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainDTO 
{
    @XmlElement
    private String domain;

    @XmlElement
    private UserLocality userLocality;

    @XmlElement
    private boolean inUse;
    
    protected DomainDTO() {
        /* JAXB requires a default constructor */        
    }
    
    public DomainDTO(String domain, UserLocality userLocality, boolean inUse)
    {
        Check.notNull(domain, "domain");
        Check.notNull(userLocality, "userLocality");
    	
    	this.domain = domain;
    	this.userLocality = userLocality;
    	this.inUse = inUse;
    }

    public String getDomain() {
        return domain;
    }

    public UserLocality getUserLocality() {
        return userLocality;
    }
    
    public boolean isInUse() {
        return inUse;
    }
}
