/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import mitm.common.security.digest.Digest;
import mitm.common.security.password.PasswordUtils;
import mitm.common.util.Check;

import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSPasswordCallback;

/**
 * ServerPasswordHandler is used to authenticate incoming SOAP calls.
 * 
 * @author Martijn Brinkers
 *
 */
public class ServerPasswordHandler implements CallbackHandler 
{
	private final String username;
	
	private final String password;
	
	public ServerPasswordHandler(String username, String password)
	{
		Check.notNull(username, "username");
		Check.notNull(password, "password");
		
		this.username = username;
		this.password = password;
	}
		
    @Override
    public void handle(Callback[] callbacks) 
    throws IOException, UnsupportedCallbackException 
    {
        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];

        if (pc == null) {
        	throw new SecurityException("Password callback is null.");
        }
        
        int usage = pc.getUsage();
        
        /*
         * Only clear or digest password is supported
         */
        if (usage != WSPasswordCallback.USERNAME_TOKEN && usage != WSPasswordCallback.USERNAME_TOKEN_UNKNOWN)
        {
        	throw new SecurityException("Only ClearText passwords are supported.");
        }
        
        if (WSConstants.PASSWORD_TEXT.equals(pc.getPasswordType())) 
        {
            /*
             * it's plain text password authentication
             */
            
            try {
            	/*
            	 * Always compare username AND password and compare the password after digesting them to prevent a 
            	 * timing attack.
            	 */
                boolean usernameGood = PasswordUtils.isEqualDigest(username, pc.getIdentifier(), Digest.SHA256);
                boolean passwordGood = PasswordUtils.isEqualDigest(password, pc.getPassword(), Digest.SHA256);

                if (!usernameGood || !passwordGood) {
                	throw new SecurityException("wrong username and or password.");
                }
            }
            catch(NoSuchProviderException e) {
                throw new IOException(e);
            }
            catch (NoSuchAlgorithmException e) {
                throw new IOException(e);
            }
        }
        else {
        	/*
        	 * probably digest passwords so let WSS handle it
        	 */
	        if (pc.getIdentifier().equals(username))
	        {
	            /* 
	             * set the password on the callback. This will be compared to the
	             * password which was sent from the client.
	             */
	            pc.setPassword(password);
	        }
        }
    }
}