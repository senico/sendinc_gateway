/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import mitm.common.util.Check;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SendCertificatesDTO
{
    @XmlElement
    private List<String> thumbprints;

    @XmlElement
    private String password;

    @XmlElement
    private String sender;
    
    @XmlElement
    private String recipient;
    
    @XmlElement
    private boolean sendSMS;
    
    @XmlElement
    private String phoneNumber;
    
    @XmlElement
    private boolean allowCA;
    
    @XmlElement
    private boolean allowEmailMismatch;
    
    protected SendCertificatesDTO() {
        /* JAXB requires a default constructor */        
    }

    public SendCertificatesDTO(List<String> thumbprints, String password, String sender, String recipient)
    {
        Check.notNull(thumbprints, "thumbprints");
        Check.notNull(password, "password");
        Check.notNull(sender, "sender");
        Check.notNull(recipient, "recipient");
        
        this.thumbprints = thumbprints;
        this.password = password;
        this.sender = sender;
        this.recipient = recipient;
    }

    public List<String> getThumbprints() {
        return thumbprints;
    }

    public void setThumbprints(List<String> thumbprints) {
        this.thumbprints = thumbprints;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public boolean isSendSMS() {
        return sendSMS;
    }

    public void setSendSMS(boolean sendSMS) {
        this.sendSMS = sendSMS;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAllowCA() {
        return allowCA;
    }

    public void setAllowCA(boolean allowCA) {
        this.allowCA = allowCA;
    }

    public boolean isAllowEmailMismatch() {
        return allowEmailMismatch;
    }

    public void setAllowEmailMismatch(boolean allowEmailMismatch) {
        this.allowEmailMismatch = allowEmailMismatch;
    }
}
