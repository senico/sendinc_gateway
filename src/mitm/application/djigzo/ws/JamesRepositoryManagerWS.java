/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.application.djigzo.james.JamesRepository;
import mitm.common.ws.WebServiceCheckedException;


@WebService
public interface JamesRepositoryManagerWS 
{
    /**
     * Returns the number of items in the repository.
     * 
     * @throws WebServiceCheckedException
     */
    public int getRepositorySize(@WebParam(name="repository") JamesRepository repository)
    throws WebServiceCheckedException;

    /**
     * Returns the keys for each mail item in the repository 
     * @throws WebServiceCheckedException
     */
    public List<String> getMailNames(@WebParam(name="repository") JamesRepository repository, 
            @WebParam(name="startIndex") int startIndex, @WebParam(name="maxItems") int maxItems)
    throws WebServiceCheckedException;
    
    /**
     * Returns the mails for each mail item in the repository 
     * @throws WebServiceCheckedException
     */
    public List<MailDTO> getMails(@WebParam(name="repository") JamesRepository repository,
            @WebParam(name="startIndex") int startIndex, @WebParam(name="maxItems") int maxItems)
    throws WebServiceCheckedException;

    /**
     * Removes the mail item with the given name
     * 
     * @param repository
     * @param mailName
     * @throws WebServiceCheckedException
     */
    public void removeMail(@WebParam(name="repository") JamesRepository repository, @WebParam(name="mailName") String mailName)
    throws WebServiceCheckedException;

    /**
     * Move the mail from one repository to the other. If newState is non null the mail items state will be set to
     * the newState. 
     * 
     */
    public void moveMail(@WebParam(name="sourceRepository") JamesRepository sourceRepository, 
            @WebParam(name="destinationRepository") JamesRepository destinationRepository, 
            @WebParam(name="mailName") String mailName, @WebParam(name="newState") String newState)
    throws WebServiceCheckedException;

    /**
     * Respools the mail 
     */
    public void respoolMail(@WebParam(name="repository") JamesRepository repository, 
            @WebParam(name="mailName") String mailName, @WebParam(name="newState") String newState,
            @WebParam(name="removeAttributes") boolean removeAttributes)
    throws WebServiceCheckedException;
    
    /**
     * returns the message source of the named message.
     * 
     * @param repository
     * @param mailName
     * @return
     * @throws WebServiceCheckedException
     */
    public byte[] getMessage(@WebParam(name="repository") JamesRepository repository, @WebParam(name="mailName") String mailName,
            @WebParam(name="maxLength") int maxLength)
    throws WebServiceCheckedException;    
}
