/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.ws.WebServiceCheckedException;

@WebService
public interface X509CertStoreWS
{
    public List<X509CertificateDTO> getByEmail(@WebParam(name="email") String email, 
            @WebParam(name="match") Match match, @WebParam(name="expired") Expired expired, 
            @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias,
            @WebParam(name="firstResult") Integer firstResult, @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;

    public long getByEmailCount(@WebParam(name="email") String email, 
            @WebParam(name="match") Match match, @WebParam(name="expired") Expired expired, 
            @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias)
    throws WebServiceCheckedException;
    
    public List<X509CertificateDTO> getCertificates(@WebParam(name="expired") Expired expired,
    		@WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias, 
            @WebParam(name="firstResult") Integer firstResult, @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;
    
    public byte[] getEncodedCertificate(@WebParam(name="thumbprint") String thumbprint,
    		@WebParam(name="encoding") ObjectEncoding encoding)
    throws WebServiceCheckedException;

    public byte[] getEncodedCertificates(@WebParam(name="thumbprints") List<String> thumbprints,
    		@WebParam(name="encoding") ObjectEncoding encoding)
    throws WebServiceCheckedException;
    
    public int addCertificates(@WebParam(name="encodedCertificates") byte[] encodedCertificates)
    throws WebServiceCheckedException;

    public void removeCertificate(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;
    
    public X509CertificateDTO getCertificate(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;
    
    public long size(@WebParam(name="expired") Expired expired, @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias)
    throws WebServiceCheckedException;
    
    public List<X509CertificateDTO> searchBySubject(@WebParam(name="subject") String subject, 
    		@WebParam(name="expired") Expired expired, @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias,
            @WebParam(name="firstResult") Integer firstResult, @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;

    public long getSearchBySubjectCount(@WebParam(name="subject") String subject, 
    		@WebParam(name="expired") Expired expired, @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias)
    throws WebServiceCheckedException;
    
    public List<X509CertificateDTO> searchByIssuer(@WebParam(name="issuer") String subject, 
    		@WebParam(name="expired") Expired expired, @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias,
            @WebParam(name="firstResult") Integer firstResult, @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;
    
    public long getSearchByIssuerCount(@WebParam(name="issuer") String issuer, 
    		@WebParam(name="expired") Expired expired, @WebParam(name="missingKeyAlias") MissingKeyAlias missingKeyAlias)
    throws WebServiceCheckedException;
}
