/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.io.IOException;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.certificate.X500PrincipalBuilder;
import mitm.common.security.certificate.X500PrincipalInspector;

public class X500PrincipalDTOUtils
{
    /**
     * Converts the X500PrincipalDTO to a X500Principal.
     */
    public static X500Principal toX500Principal(X500PrincipalDTO principal)
    throws IOException
    {
        if (principal == null) {
            return null;
        }
        
        X500PrincipalBuilder principalBuilder = new X500PrincipalBuilder();
        
        principalBuilder.setEmail(principal.getEmail());
        principalBuilder.setGivenName(principal.getGivenName());
        principalBuilder.setSurname(principal.getSurname());
        principalBuilder.setCommonName(principal.getCommonName());
        principalBuilder.setCountryCode(principal.getCountryCode());
        principalBuilder.setLocality(principal.getLocality());
        principalBuilder.setOrganisation(principal.getOrganisation());
        principalBuilder.setOrganisationalUnit(principal.getOrganisationalUnit());
        principalBuilder.setState(principal.getState());
        
        return principalBuilder.buildPrincipal();
    }
    
    /**
     * Converts the X500PrincipalDTO to a X500Principal.
     */
    public static X500PrincipalDTO toX500PrincipalDTO(X500Principal principal)
    throws IOException
    {
        if (principal == null) {
            return null;
        }
        
        X500PrincipalDTO principalDTO = new X500PrincipalDTO();
        
        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);
        
        principalDTO.setEmail(inspector.getEmail());
        principalDTO.setCommonName(inspector.getCommonName());
        principalDTO.setGivenName(inspector.getGivenName());
        principalDTO.setSurname(inspector.getSurname());
        principalDTO.setCountryCode(inspector.getCountryCode());
        principalDTO.setLocality(inspector.getLocality());
        principalDTO.setState(inspector.getState());
        principalDTO.setOrganisation(inspector.getOrganisation());
        principalDTO.setOrganisationalUnit(inspector.getOrganisationalUnit());
        
        return principalDTO;
    }
}
