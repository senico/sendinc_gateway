/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ComodoTierDetailsDTO
{
    /*
     * The level of validation performed on the company controlling the account (e.g. "Class 3")
     */
    @XmlElement
    private String verificationLevel;
    
    /*
     * The status of the account (e.g. "Not yet active", "Active", "Suspended", etc) 
     */
    @XmlElement
    private String accountStatus;

    /*
     * The status of the Reseller functionality of the account (e.g. "Awaiting Authorization", "Authorized", "Rejected")
     */
    @XmlElement
    private String resellerStatus;
    
    /*
     * The status of the Web Host Reseller functionality of the account (e.g. "Awaiting Authorization", 
     * "Authorized", "Rejected")
     */
    @XmlElement
    private String webHostResellerStatus;
    
    /*
     * The status of the E-PKI functionality of the account (e.g. "Awaiting Authorization", "Authorized", "Rejected")
     */
    @XmlElement
    private String epkiStatus;
    
    /*
     * The maximum number of concurrently "live" CCCs that this Tier 2 is permitted to have (Certain Tier 2 
     * Resellers only)
     */
    @XmlElement
    private String capLiveCCCs;

    /*
     * The highest number of concurrently "live" CCCs that this Tier 2 has ever had (Certain Tier 2 Resellers only)
     */
    @XmlElement
    private String peakLiveCCCs;
    
    /*
     * The number of "live" CCCs that this Tier 2 currently has (Certain Tier 2 Resellers only)
     */
    @XmlElement
    private String currentLiveCCCs;

    /*
     * A comma-separated list of domain names that have been authorized to be used in email address fields in CCCs order 
     * by this Partner. The special value "authorizedDomains=NONE" means that no domain names are authorized.
     * (If this parameter is omitted, all domain names are authorized).
     */
    @XmlElement
    private String authorizedDomains;    

    /*
     * True if an error occurred.
     */
    @XmlElement
    private boolean error;
    
    /*
     * The error message if error is true
     */
    @XmlElement
    private String errorMessage;
    
    public ComodoTierDetailsDTO() {
        /* JAXB requires a default constructor */        
    }

    public String getVerificationLevel() {
        return verificationLevel;
    }

    public void setVerificationLevel(String verificationLevel) {
        this.verificationLevel = verificationLevel;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getResellerStatus() {
        return resellerStatus;
    }

    public void setResellerStatus(String resellerStatus) {
        this.resellerStatus = resellerStatus;
    }

    public String getWebHostResellerStatus() {
        return webHostResellerStatus;
    }

    public void setWebHostResellerStatus(String webHostResellerStatus) {
        this.webHostResellerStatus = webHostResellerStatus;
    }

    public String getEpkiStatus() {
        return epkiStatus;
    }

    public void setEpkiStatus(String epkiStatus) {
        this.epkiStatus = epkiStatus;
    }

    public String getCapLiveCCCs() {
        return capLiveCCCs;
    }

    public void setCapLiveCCCs(String capLiveCCCs) {
        this.capLiveCCCs = capLiveCCCs;
    }

    public String getPeakLiveCCCs() {
        return peakLiveCCCs;
    }

    public void setPeakLiveCCCs(String peakLiveCCCs) {
        this.peakLiveCCCs = peakLiveCCCs;
    }

    public String getCurrentLiveCCCs() {
        return currentLiveCCCs;
    }

    public void setCurrentLiveCCCs(String currentLiveCCCs) {
        this.currentLiveCCCs = currentLiveCCCs;
    }

    public String getAuthorizedDomains() {
        return authorizedDomains;
    }

    public void setAuthorizedDomains(String authorizedDomains) {
        this.authorizedDomains = authorizedDomains;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
