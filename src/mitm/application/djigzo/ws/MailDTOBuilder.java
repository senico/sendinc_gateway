/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.MailAddressUtils;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This MailDTO extension is created to make sure that MailDTO does not depend on any Bouncycastle classes.
 * The implementation of MailDTO can be used by soap clients (if using JAXB). The non-default constructor however is not
 * used by the remove client so to decouple dependencies we do not need we created this build class. 
 * 
 * @author Martijn Brinkers
 *
 */
public class MailDTOBuilder extends MailDTO 
{
    private final static Logger logger = LoggerFactory.getLogger(MailDTOBuilder.class);
    
	
    public MailDTOBuilder(Mail mail)
    {
        Check.notNull(mail, "mail");
        
        this.name = mail.getName();
        this.recipients = CollectionUtils.toStringList(MailAddressUtils.getRecipients(mail), "");
        this.sender = ObjectUtils.toString(mail.getSender());
        this.state = mail.getState();
        this.remoteAddr = mail.getRemoteAddr();
        this.errorMessage = mail.getErrorMessage();
        this.lastUpdated = mail.getLastUpdated();
        
        try {
            this.messageSize = mail.getMessageSize();
        }
        catch (MessagingException e) 
        {
            logger.error("Unable to get the message size", e);
            
            this.messageSize = -1;
        }
    }
}
