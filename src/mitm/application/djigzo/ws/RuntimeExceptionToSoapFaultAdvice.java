/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import mitm.common.ws.WebServiceCheckedException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

/**
 * For SOAP exceptions we need to get the cause message of the Runtime exception because
 * SOAP does not support throwing regular Java exceptions. The soap service implementation 
 * class cannot catch exceptions thrown by StartTransactionAdvice (like commit exceptions) 
 * because some exceptions (like constraint violations) are thrown after the service method.
 * So we need to intercept all Runtime exceptions and convert them to a DjigzoSoapException
 * with the root cause as the message.
 * 
 * This advice has the lowest precedence because we always want this advice to be the first.
 * 
 * @author Martijn Brinkers
 *
 */

@Aspect
@Order(0)
public class RuntimeExceptionToSoapFaultAdvice 
{
    private final static Logger logger = LoggerFactory.getLogger(RuntimeExceptionToSoapFaultAdvice.class);
    
    @Around("@annotation(mitm.common.hibernate.annotations.StartTransaction) && " +
    		"within(mitm.application.djigzo.ws..*)")
    public Object startTransaction(ProceedingJoinPoint pjp) 
    throws Throwable
    {
    	try {
    		return pjp.proceed();
    	}
    	catch(RuntimeException e)
    	{
    	    logger.error("RuntimeException handling SOAP.", e);
    	    
    		throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
    	}
    }
}
