/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.ws.WebServiceCheckedException;

@WebService
public interface HierarchicalPropertiesWS
{
    /**
     * Returns a property value with the given name from the given userPreferences. If there is no property with the 
     * given name for this properties null is returned.
     */
    public String getProperty(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="propertyName") String propertyName, @WebParam(name="decrypt") boolean decrypt)
    throws WebServiceCheckedException;    

    /**
     * Returns the property values for the provided property selectors for the user. 
     */
    public List<PropertyDTO> getProperties(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences,
            @WebParam(name="propertySelectors") List<PropertySelectorDTO> propertySelectors)
    throws WebServiceCheckedException;    
    
    /**
     * Sets the property for the given userPreferences.
     */
    public void setProperty(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="propertyName") String propertyName, @WebParam(name="value") String value,
            @WebParam(name="encrypt") boolean encrypt)
    throws WebServiceCheckedException;    

    /**
     * Sets the properties for the given userPreferences. The properties will be set
     * and deleted in one transaction
     */
    public void setProperties(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="propertiesToSet") List<PropertyDTO> propertiesToSet,
            @WebParam(name="propertiesToDelete") List<String> propertiesToDelete)
    throws WebServiceCheckedException;    
    
    /**
     * Deletes the property with the given name. If the property does not exist on this object
     * nothing happens. The parent property is not removed.
     */
    public void deleteProperty(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="propertyName") String propertyName)
    throws WebServiceCheckedException;    
    
    /**
     * Deletes all properties of this property object. The parent properties are not deleted.
     */
    public void deleteAll(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException;    
    
    /**
     * Returns true if this properties does not have the property but the parent has.
     */
    public boolean isInherited(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="propertyName") String propertyName)
    throws WebServiceCheckedException;    
    
    /**
     * Returns an unmodifiable Set of all the property names. If recursive is true the property names
     * of the parent are returned as well.
     */
    public List<String> getProperyNames(@WebParam(name="userPreferences") UserPreferencesDTO userPreferences, 
            @WebParam(name="recursive") boolean recursive)
    throws WebServiceCheckedException;    
}
