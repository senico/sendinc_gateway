/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import mitm.application.djigzo.dlp.PolicyPatternNode;
import mitm.application.djigzo.dlp.UserPolicyPatternNode;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicyPatternNodeDTO
{
    /*
     * The name of this node
     */
    @XmlElement
    private String name;

    /*
     * The PolicyPattern of this node
     */
    @XmlElement
    private PolicyPatternDTO policyPattern;

    /*
     * True if this node has child nodes
     */
    @XmlElement
    private boolean hasChilds;
    
    /*
     * If true, the node is inherited 
     */
    @XmlElement
    private boolean inherited;
    
    public PolicyPatternNodeDTO() {
        /* JAXB requires a default constructor */        
    }

    public static PolicyPatternNodeDTO toDTO(PolicyPatternNode policyPattern)
    {
        if (policyPattern == null) {
            return null;
        }
        
        PolicyPatternNodeDTO dto = new PolicyPatternNodeDTO();
        
        dto.setName(policyPattern.getName());
        dto.setPolicyPattern(PolicyPatternDTO.toDTO(policyPattern.getPolicyPattern()));
        dto.setHasChilds(CollectionUtils.isNotEmpty(policyPattern.getChilds()));
        
        boolean inherited = false;
        
        if (policyPattern instanceof UserPolicyPatternNode) {
            inherited = ((UserPolicyPatternNode) policyPattern).isInherited();
        }
        
        dto.setInherited(inherited);
        
        return dto;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInherited() {
        return inherited;
    }

    public void setInherited(boolean inherited) {
        this.inherited = inherited;
    }

    public PolicyPatternDTO getPolicyPattern() {
        return policyPattern;
    }

    public void setPolicyPattern(PolicyPatternDTO policyPattern) {
        this.policyPattern = policyPattern;
    }

    public boolean isHasChilds() {
        return hasChilds;
    }

    public void setHasChilds(boolean hasChilds) {
        this.hasChilds = hasChilds;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternNodeDTO)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        PolicyPatternNodeDTO rhs = (PolicyPatternNodeDTO) obj;
        
        return new EqualsBuilder()
            .append(getName(), rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getName())
            .toHashCode();    
    }
}
