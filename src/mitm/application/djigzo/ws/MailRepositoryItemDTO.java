/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO class for a MailRepositoryItem.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MailRepositoryItemDTO
{
    /*
     * Unique Id of the item
     */
    @XmlElement
    private String id;
    
    /*
     * The repository this item belongs to
     */
    @XmlElement
    private String repository;    

    /*
     * The message-ID of the mime message of this item.
     */
    @XmlElement
    private String messageID;    
    
    /*
     * The subject of the message
     */
    @XmlElement
    private String subject;    
    
    /*
     * The from of the message.
     * 
     * Note: this should only be used for displaying purposes (use originator for sender identity)
     */
    @XmlElement
    private String fromHeader;    
    
    /*
     * The recipients of the message
     */
    @XmlElement
    private List<String> recipients;    
    
    /*
     * The sender of the message
     */
    @XmlElement
    private String sender;    

    /*
     * The originator of the message
     */
    @XmlElement
    private String originator;    
    
    /*
     * The IP address of the last hop smtp server
     */
    @XmlElement
    private String remoteAddress;    
    
    /*
     * The date this item was created
     */
    @XmlElement
    private Date created;    

    /*
     * Some general information
     */
    @XmlElement
    private String info;    
    
    /*
     * The violations.
     * 
     * Note: this makes this MailRepositoryItemDTO dependend on being a Quarantine. If we later need
     * to reuse this DTO we need to think of a more general way to solve this. 
     */
    @XmlElement
    private List<PolicyViolationDTO> violations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromHeader() {
        return fromHeader;
    }

    public void setFromHeader(String fromHeader) {
        this.fromHeader = fromHeader;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }
    
    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    public List<PolicyViolationDTO> getPolicyViolations() {
        return violations;
    }

    public void setPolicyViolations(List<PolicyViolationDTO> violations) {
        this.violations = violations;
    }
}
