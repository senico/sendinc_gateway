/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.ws.WebServiceCheckedException;

@WebService
public interface X509CRLStoreWS 
{
	/**
	 * Adds the encoded CRLs. Returns the number of CRLs added. 
	 */
    public int addCRLs(@WebParam(name="encodedCRLs") byte[] encodedCRLs)
    throws WebServiceCheckedException;

    /**
     * Returns the CRL with the given thumbprint, null if no CRL with thumbprint found
     */
    public X509CRLDTO getCRL(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;
    
    /**
     * Removes the CRL with the given thumbprint. Return true if removed, false is the CRL with
     * the thumbprint is not in the store (and can therefore not be removed)
     */
    public boolean remove(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;

    /**
     * Returns the CRL as a DER encoded byte array, null if there is no CRL with the given thumbprint
     */
    public byte[] getEncodedCRL(@WebParam(name="thumbprint") String thumbprint,
    		@WebParam(name="encoding") ObjectEncoding encoding)
    throws WebServiceCheckedException;

    /**
     * Returns the CRLs as an encoded byte array
     */
    public byte[] getEncodedCRLs(@WebParam(name="thumbprints") List<String> thumbprints,
    		@WebParam(name="encoding") ObjectEncoding encoding)
    throws WebServiceCheckedException;
    
    /**
     * Returns a list of CRLs
     */
    public List<X509CRLDTO> getCRLs(@WebParam(name="firstResult") Integer firstResult, 
    		@WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;
    
    /**
     * Returns the number of CRLs in the store
     */
    public long size()
    throws WebServiceCheckedException;

    /** 
     * Updates the store by forcing the download of CRLs from the CRL distibutionpoints
     */
    public void update()
    throws WebServiceCheckedException;

    /**
     * Checks the trust of the CRL (the CRL should exist, if not the CRL is not trusted)
     */
    public CRLTrustCheckResult checkTrust(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;

    /**
     * Returns a text representation of the CRL entries of the CRL with the given thumbprint
     */
    public String getCRLEntries(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException;
}
