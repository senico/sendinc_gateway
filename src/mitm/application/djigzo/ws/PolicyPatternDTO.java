/*
 * Copyright (c) 2010, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.dlp.impl.PolicyPatternImpl;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicyPatternDTO
{
    /*
     * The name of this policy pattern (aka the rule)
     */
    @XmlElement
    private String name;

    /*
     * The reg exp pattern of this rule 
     */
    @XmlElement
    private String regExp;
    
    /*
     * The name of the filter that will be used to filter any matching content.
     */
    @XmlElement
    private String matchFilter;
    
    /*
     * Explains what this pattern policy does.
     */
    @XmlElement
    private String description;

    /*
     * If the pattern matches the content more than the threshold, the policy is violated.
     */
    @XmlElement
    private int threshold;
    
    /*
     * The priority associated with this pattern.
     */
    @XmlElement
    private PolicyViolationPriority priority;
    
    public PolicyPatternDTO() {
        /* JAXB requires a default constructor */        
    }

    public static PolicyPattern fromDTO(PolicyPatternDTO dto)
    {
        if (dto == null) {
            return null;
        }
     
        PolicyPatternImpl policyPattern = new PolicyPatternImpl();
        
        policyPattern.setName(dto.getName());
        policyPattern.setDescription(dto.getDescription());
        policyPattern.setMatchFilterName(dto.getMatchFilter());
        policyPattern.setRegExp(dto.getRegExp());
        policyPattern.setThreshold(dto.getThreshold());
        policyPattern.setPriority(dto.getPriority());
        
        return policyPattern;
    }

    public static PolicyPatternDTO toDTO(PolicyPattern policyPattern)
    {
        if (policyPattern == null) {
            return null;
        }
        
        PolicyPatternDTO dto = new PolicyPatternDTO();
        
        dto.name = policyPattern.getName();
        dto.regExp = policyPattern.getPattern() != null ? policyPattern.getPattern().pattern() : null;
        dto.matchFilter = policyPattern.getMatchFilter() != null ? policyPattern.getMatchFilter().getName() : null;
        dto.description = policyPattern.getDescription();
        dto.threshold = policyPattern.getThreshold();
        dto.priority = policyPattern.getPriority();
        
        return dto;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public String getMatchFilter() {
        return matchFilter;
    }

    public void setMatchFilter(String matchFilter) {
        this.matchFilter = matchFilter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
    
    public PolicyViolationPriority getPriority() {
        return priority;
    }

    public void setPriority(PolicyViolationPriority priority) {
        this.priority = priority;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternDTO)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        PolicyPatternDTO rhs = (PolicyPatternDTO) obj;
        
        return new EqualsBuilder()
            .append(getName(), rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getName())
            .toHashCode();    
    }
}
