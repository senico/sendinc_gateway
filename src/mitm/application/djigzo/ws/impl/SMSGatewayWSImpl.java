/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.application.djigzo.ws.SMSDTO;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.SortDirection;
import mitm.common.sms.SMS;
import mitm.common.sms.SMSGateway;
import mitm.common.sms.SMSGatewayException;
import mitm.common.sms.SortColumn;
import mitm.common.sms.hibernate.SMSImpl;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

public class SMSGatewayWSImpl implements SMSGatewayWS 
{
	private static final Logger logger = LoggerFactory.getLogger(SMSGatewayWSImpl.class);
	
	private final SMSGateway smsGateway;
	
	public SMSGatewayWSImpl(SMSGateway smsGateway)
	{
		Check.notNull(smsGateway, "smsGateway");
		
		this.smsGateway = smsGateway;
	}
	
	@Override
    public boolean delete(long id) 
	throws WebServiceCheckedException 
	{
		try {
			return smsGateway.delete(id);
		} 
        catch(SMSGatewayException e)
        {
            logger.error("delete failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("delete failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public List<SMSDTO> getAll(Integer firstResult, Integer maxResults,
			SortColumn sortColumn, SortDirection sortDirection) 
	throws WebServiceCheckedException 
	{
		try {
			List<SMS> all = smsGateway.getAll(firstResult, maxResults, sortColumn, sortDirection);
			
			List<SMSDTO> allDTO = new ArrayList<SMSDTO>(all.size());
			
			for (SMS sms : all)	{
				allDTO.add(toDTO(sms));
			}
			
			return allDTO;
		} 
        catch(SMSGatewayException e)
        {
            logger.error("getAll failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getAll failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public int getCount() 
	throws WebServiceCheckedException 
	{
		try {
			return smsGateway.getCount();
		} 
        catch(SMSGatewayException e)
        {
            logger.error("getCount failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public void sendSMS(SMSDTO sms) 
	throws WebServiceCheckedException 
	{
		try {
			smsGateway.sendSMS(fromDTO(sms));
		} 
        catch(SMSGatewayException e)
        {
            logger.error("sendSMS failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("sendSMS failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	private SMSDTO toDTO(SMS sms)
	{
		return new SMSDTO(sms.getID(), sms.getPhoneNumber(), sms.getMessage(), sms.getCreated(),
				sms.getLastTry(), sms.getLastError());
	}

	private SMS fromDTO(SMSDTO smsDTO)
	{
		return new SMSImpl(smsDTO.getID(), smsDTO.getPhoneNumber(), smsDTO.getMessage(),
				null, smsDTO.getCreated(), smsDTO.getLastTry(), smsDTO.getLastError());
	}
}
