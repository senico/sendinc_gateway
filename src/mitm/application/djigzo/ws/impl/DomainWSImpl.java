/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.ws.DomainWS;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DomainWSImpl implements DomainWS
{
    private final static Logger logger = LoggerFactory.getLogger(DomainWSImpl.class);
    
    private final DomainManager domainManager;
    
    public DomainWSImpl(DomainManager domainManager) 
    {
    	Check.notNull(domainManager, "domainManager");
    	        
        this.domainManager = domainManager;
    }
    
    @Override
    @StartTransaction
    public UserPreferencesDTO getDomainPreferences(String domain)
    throws WebServiceCheckedException
    {
    	Check.notNull(domain, "domain");
    	
    	try {
            return getDomainPreferencesAction(domain);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getDomainPreferences failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getDomainPreferences failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    private UserPreferencesDTO getDomainPreferencesAction(String domain)
    throws WebServiceCheckedException
    {
        UserPreferencesDTO userPreferencesDTO = null;
        
        UserPreferences domainPreferences = domainManager.getDomainPreferences(domain);

        if (domainPreferences != null) 
        {
        	userPreferencesDTO = new UserPreferencesDTO(domainPreferences.getCategory(), 
        			domainPreferences.getName());
        }
        
        return userPreferencesDTO;
    }
}
