/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import mitm.application.djigzo.ws.BackupWS;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.backup.BackupDriver;
import mitm.common.backup.BackupDriverFactory;
import mitm.common.backup.BackupException;
import mitm.common.backup.BackupMaker;
import mitm.common.backup.impl.CIFSBackupDriver;
import mitm.common.backup.impl.OutputStreamBackupDriver;
import mitm.common.cifs.StaticSMBFileParameters;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackupWSImpl implements BackupWS
{
    private final static Logger logger = LoggerFactory.getLogger(BackupWSImpl.class);
    
    /*
     * BackupMaker is doing the actual backup
     */
    private final BackupMaker backupMaker;

    /*
     * Factory for a BackupDriver. The BackupDriver is used to actually store the backup
     */
    private final BackupDriverFactory defaultBackupDriverFactory;
    
    /*
     * The max size we will accept for a backup
     */
    private final int maxBackupSize;
    
    public BackupWSImpl(BackupMaker backupMaker, BackupDriverFactory defaultBackupDriverFactory, int maxBackupSize)
    {
        Check.notNull(backupMaker, "backupMaker");
        Check.notNull(defaultBackupDriverFactory, "defaultBackupDriverFactory");
        
        this.backupMaker = backupMaker;
        this.defaultBackupDriverFactory = defaultBackupDriverFactory;
        this.maxBackupSize = maxBackupSize;
    }
    
    @Override
    @StartTransaction
    public String backup(String password)
    throws WebServiceCheckedException
    {
        try {
            BackupDriver backupDriver = defaultBackupDriverFactory.createBackupDriver();
            
            return backupMaker.backup(backupDriver, password);
        }
        catch (BackupException e)
        {
            logger.error("backup failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("backup failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public BinaryDTO backupDirect(String password)
    throws WebServiceCheckedException
    {
        try {
            /*
             * The backup will be store in memory. If we store the backup in a file we need to create a 
             * temp file to support concurrent access. The problem with this is that we can only delete
             * the file when it has been received by the end party but then we already lost control.
             * Storing the backup in memory can take some memory so we will add a max backup size.
             * If we need to support much larger backup sizes we can always resort to a file cache
             * which will periodically delete stale files.
             */
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            OutputStream output = new SizeLimitedOutputStream(bos, maxBackupSize, true);

            BackupDriver backupDriver = new OutputStreamBackupDriver(output);
            
            String identifier = backupMaker.backup(backupDriver, password);

            DataSource source = new ByteArrayDataSource(bos.toByteArray(), "application/octet-stream");
            DataHandler dataHandler = new DataHandler(source);
            
            return new BinaryDTO(dataHandler, identifier);
        }
        catch (BackupException e)
        {
            logger.error("restore failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("restore failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public void restore(BinaryDTO backup, String password)
    throws WebServiceCheckedException
    {
        try {
            if (backup == null) {
                throw new WebServiceCheckedException("backup is null.");
            }
            
            DataHandler dataHandler = backup.getDataHandler();
            
            InputStream input = dataHandler.getInputStream();
            
            backupMaker.restore(input, password);
        }
        catch (IOException e)
        {
            logger.error("restore failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch (BackupException e)
        {
            logger.error("restore failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("restore failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public boolean testCIFSConnection(StaticSMBFileParameters sMBFileParameters)
    throws WebServiceCheckedException
    {
        try {
            CIFSBackupDriver backupDriver = new CIFSBackupDriver(sMBFileParameters);
            
            logger.info("ACEs " + StringUtils.join(backupDriver.getSecurity()));
            
            return backupDriver.isValid();
        }
        catch (BackupException e)
        {
            logger.error("testCIFSConnection failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("testCIFSConnection failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
}
