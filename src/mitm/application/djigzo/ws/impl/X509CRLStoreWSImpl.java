/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebParam;

import mitm.application.djigzo.ws.CRLTrustCheckResult;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.application.djigzo.ws.X509CRLDTO;
import mitm.application.djigzo.ws.X509CRLDTOBuilder;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.security.crl.CRLEncoder;
import mitm.common.security.crl.CRLPathBuilder;
import mitm.common.security.crl.CRLStoreUpdater;
import mitm.common.security.crl.CRLUtils;
import mitm.common.security.crl.OptimizedX509CRLObject;
import mitm.common.security.crl.X509CRLEntryInspector;
import mitm.common.security.crl.X509CRLInspector;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreEntry;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.MiscStringUtils;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class X509CRLStoreWSImpl implements X509CRLStoreWS
{
	private final static Logger logger = LoggerFactory.getLogger(X509CRLStoreWSImpl.class);

	private final static byte[] CR_LF = new byte[]{'\r', '\n'};

	private final PKISecurityServices pKISecurityServices;

	private final X509CRLStoreExt crlStore;

	private final CRLStoreUpdater crlStoreUpdater;

	private final int maxMemory;

	public X509CRLStoreWSImpl(PKISecurityServices pKISecurityServices,
			CRLStoreUpdater crlStoreUpdater, int maxMemory)
	{
		Check.notNull(pKISecurityServices, "pKISecurityServices");
		Check.notNull(crlStoreUpdater, "crlStoreUpdater");

		this.pKISecurityServices = pKISecurityServices;
		this.crlStoreUpdater = crlStoreUpdater;
		this.maxMemory = maxMemory;

		this.crlStore = pKISecurityServices.getCRLStore();

		Check.notNull(crlStore, "crlStore");
	}

	@Override
    @StartTransaction
	public int addCRLs(byte[] encodedCRL)
	throws WebServiceCheckedException
	{
		if (encodedCRL == null) {
			return 0;
		}

		Collection<X509CRL> crls;

		try {
			crls = CRLUtils.readX509CRLs(new ByteArrayInputStream(encodedCRL));
		}
		catch(CRLException e) {
			logger.error("addCRLs reading CRLs failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (CertificateException e) {
			logger.error("addCRLs reading CRLs failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (NoSuchProviderException e) {
			logger.error("addCRLs reading CRLs failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (SecurityFactoryFactoryException e) {
			logger.error("addCRLs reading CRLs failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}

		int added = 0;

		for (X509CRL crl : crls)
		{
			try {
				if (!crlStore.contains(crl))
				{
					crlStore.addCRL(crl);

					added++;
				}
			}
			catch(CRLStoreException e) {
				logger.error("Error adding CRL.", e);
			}
		}

		return added;
	}

	@Override
    @StartTransaction
    public X509CRLDTO getCRL(String thumbprint)
    throws WebServiceCheckedException
    {
		try {
			Check.notNull(thumbprint, "thumbprint");

			X509CRLDTO dto = null;

			X509CRL crl = crlStore.getCRL(thumbprint);

			if (crl != null) {
				dto = new X509CRLDTOBuilder(crl);
			}

			return dto;
		}
		catch (CRLStoreException e) {
			logger.error("Error getting CRL", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
    }

	@Override
    @StartTransaction
    public byte[] getEncodedCRL(String thumbprint, ObjectEncoding encoding)
    throws WebServiceCheckedException
    {
		try {
			Check.notNull(thumbprint, "thumbprint");

        	if (encoding == null) {
        		encoding = ObjectEncoding.PEM;
        	}

			byte[] encoded = null;

			X509CRL crl = crlStore.getCRL(thumbprint);

			if (crl != null) {
				encoded = CRLEncoder.encode(crl, encoding);
			}

			return encoded;
		}
		catch (CRLStoreException e) {
			logger.error("Error getting encoded CRL", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (CRLException e) {
			logger.error("Error getting encoded CRL", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (IOException e) {
			logger.error("Error getting encoded CRL", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
    }

	@Override
    @StartTransaction
    public byte[] getEncodedCRLs(List<String> thumbprints, ObjectEncoding encoding)
    throws WebServiceCheckedException
    {
		try {
			Check.notNull(thumbprints, "thumbprints");

			if (encoding == null) {
				encoding = ObjectEncoding.PEM;
			}

			List<X509CRL> crls = new LinkedList<X509CRL>();

			for (String thumbprint : thumbprints)
			{
				X509CRL crl = crlStore.getCRL(thumbprint);

				if (crl != null) {
					crls.add(crl);
				}
			}

			byte[] encoded = null;

			if (crls.size() > 0)
			{
				ByteArrayOutputStream bos = new ByteArrayOutputStream();

				CRLUtils.writeX509CRLs(crls, bos, encoding);

				encoded = bos.toByteArray();
			}

			return encoded;
		}
		catch (CRLStoreException e) {
			logger.error("Error getting encoded CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (CRLException e) {
			logger.error("Error getting encoded CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (CertificateEncodingException e) {
			logger.error("Error getting encoded CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (IOException e) {
			logger.error("Error getting encoded CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
    }

	@Override
    @StartTransaction
	public List<X509CRLDTO> getCRLs(Integer firstResult, Integer maxResults)
	throws WebServiceCheckedException
	{
		List<X509CRLDTO> crlDTOs = new LinkedList<X509CRLDTO>();

		try {
			CloseableIterator<? extends X509CRLStoreEntry> crlIterator = crlStore.getCRLStoreIterator(null,
					firstResult, maxResults);

			while(crlIterator.hasNext())
			{
				X509CRLStoreEntry crlEntry = crlIterator.next();

				if (crlEntry.getCRL() != null) {
					crlDTOs.add(new X509CRLDTOBuilder(crlEntry.getCRL()));
				}
			}
		}
		catch (CRLStoreException e) {
			logger.error("Error getting CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
		catch (CloseableIteratorException e) {
			logger.error("Error getting CRLs", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}

		return crlDTOs;
	}

	@Override
    @StartTransaction
	public long size()
	throws WebServiceCheckedException
	{
		return crlStore.size();
	}

	@Override
    @StartTransaction
	public boolean remove(String thumbprint)
	throws WebServiceCheckedException
	{
		boolean removed = false;

		try {
			X509CRL crl = crlStore.getCRL(thumbprint);

			if (crl != null)
			{
				crlStore.remove(crl);

				removed = true;
			}
		}
		catch (CRLStoreException e) {
			logger.error("Error remove CRL", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}

		return removed;
	}

	@Override
    @StartTransaction
    public void update()
    throws WebServiceCheckedException
    {
		try {
			crlStoreUpdater.update();
		}
		catch (CRLStoreException e) {
			logger.error("Error updating CRL store", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
    }

	@Override
    @StartTransaction
    public CRLTrustCheckResult checkTrust(String thumbprint)
    throws WebServiceCheckedException
    {
		try {
			CRLTrustCheckResult result;

			X509CRL crl = crlStore.getCRL(thumbprint);

			if (crl == null) {
				result = new CRLTrustCheckResult(false, "CRL not found.");
			}
			else {
				try {
					CRLPathBuilder pathBuilder = pKISecurityServices.getCRLPathBuilderFactory().
							createCRLPathBuilder();

					pathBuilder.buildPath(crl);

					result = new CRLTrustCheckResult(true, "");
				}
		        catch (CertPathBuilderException e) {
		            /*
		             * CertPathBuilderException is thrown for a lot of reasons so we will try to extract
		             * the reason.
		             */
		            Throwable rootCause = ExceptionUtils.getRootCause(e);

		            Throwable cause = (rootCause != null ? rootCause : e);

		            String errorMessage;

		            if (cause instanceof CertificateExpiredException) {
		                errorMessage = "Certificate in the CRL path is expired. CRL: " +
		                		X509CRLInspector.toString(crl) + ". Message: " + cause.getMessage();
		            }
		            else {
		                errorMessage = "Error while building path for CRL. CRL: " +
		                		X509CRLInspector.toString(crl);

		                if (logger.isDebugEnabled()) {
		                    logger.error(errorMessage, cause);
		                }
		                else {
		                    logger.error(errorMessage + ". Message: " + cause.getMessage());
		                }
		            }

		            result = new CRLTrustCheckResult(false, errorMessage);
		        }
			}

	        return result;
		}
		catch (CRLStoreException e) {
			logger.error("Error checking CRL trust.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
    }

    @Override
    @StartTransaction
    public String getCRLEntries(@WebParam(name="thumbprint") String thumbprint)
    throws WebServiceCheckedException
    {
        try {
            Check.notNull(thumbprint, "thumbprint");

            X509CRL crl = crlStore.getCRL(thumbprint);

            if (crl == null)
            {
                logger.warn("CRL with thumbprint " + thumbprint + " not found.");

                return null;
            }

            /*
             * We will only support OptimizedX509CRLObject's
             */
            if (!(crl instanceof OptimizedX509CRLObject)) {
                throw new WebServiceCheckedException("CRL is-not-a OptimizedX509CRLObject");
            }

            /*
             * The result will be store in memory. If we store it in a file we need to create a
             * temp file to support concurrent access. The problem with this is that we can only delete
             * the file when it has been received by the end party but then we already lost control.
             * Storing the backup in memory can take some memory so we will add a max size.
             */
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            OutputStream output = new SizeLimitedOutputStream(bos, maxMemory, true);

            Enumeration<? extends X509CRLEntry> revokedCertificatesEnum = ((OptimizedX509CRLObject) crl).
                    getRevokedCertificatesEnumeration();

            while (revokedCertificatesEnum.hasMoreElements())
            {
                X509CRLEntry crlEntry = revokedCertificatesEnum.nextElement();

                String line = X509CRLEntryInspector.toString(crlEntry);

                output.write(MiscStringUtils.toAsciiBytes(line));
                output.write(CR_LF);
            }

            output.close();
            return MiscStringUtils.toAsciiString(bos.toByteArray());
        }
        catch (Exception e) {
            logger.error("Error getting CRL entries", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
}
