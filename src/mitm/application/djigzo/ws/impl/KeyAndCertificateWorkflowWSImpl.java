/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.ws.KeyAndCertificateWorkflowWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeyAndCertificateWorkflowWSImpl implements KeyAndCertificateWorkflowWS
{
    private final static Logger logger = LoggerFactory.getLogger(UserWSImpl.class);
    
    private final KeyAndCertStore keyAndCertStore;
    private final KeyAndCertificateWorkflow keyAndCertificateWorkflow;
    
    public KeyAndCertificateWorkflowWSImpl(KeyAndCertStore keyAndCertStore, 
    		KeyAndCertificateWorkflow keyAndCertificateWorkflow) 
    {
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(keyAndCertificateWorkflow, "keyAndCertificateWorkflow");
        
    	this.keyAndCertStore = keyAndCertStore;
        this.keyAndCertificateWorkflow = keyAndCertificateWorkflow;
    }

    @Override
    @StartTransaction
    public int addPFX(byte[] encodedPFX, String password, KeyAndCertificateWorkflow.MissingKey missingKey)
    throws WebServiceCheckedException
    {
        try {
        	Check.notNull(encodedPFX, "encodedPFX");
        	
            return addKeysAction(encodedPFX, password, missingKey);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("addKeys failed.", e);
            
            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("addKeys failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public byte[] getPFX(List<String> thumbprints, char[] password)
    throws WebServiceCheckedException
    {
        try {
        	Check.notNull(thumbprints, "thumbprints");
        	Check.notNull(password, "password");
        	
            return getPFXAction(thumbprints, password);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getPFX failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getPFX failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public boolean isInUse(String thumbprint) 
    throws WebServiceCheckedException
    {
        try {
        	boolean inUse = false;
        	
        	if (thumbprint != null)
        	{
	        	X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);
	        	
	        	if (entry != null && entry.getCertificate() != null) {
	        		inUse = keyAndCertificateWorkflow.isInUse(entry.getCertificate());
	        	}
        	}
        	
            return inUse;
        }
        catch(CertStoreException e)
        {
            logger.error("isInUse failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isInUse failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    private int addKeysAction(byte[] encodedPFX, String password, KeyAndCertificateWorkflow.MissingKey missingKey) 
    throws WebServiceCheckedException
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(encodedPFX);
        
        KeyStore keyStore;
        
        try {
            SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

            keyStore = securityFactory.createKeyStore("PKCS12");

            keyStore.load(bis, password.toCharArray());
            
            if (keyStore.size() == 0) {
                throw new WebServiceCheckedException("No keys found in encodedPFX.");
            }
            
            return keyAndCertificateWorkflow.importKeyStore(keyStore, missingKey);
        }
        catch (CertificateException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (NoSuchProviderException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (IOException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    private byte[] getPFXAction(List<String> thumbprints, char[] password) 
    throws WebServiceCheckedException
    {
        try {
        	List<X509Certificate> certificates = new LinkedList<X509Certificate>();
        	
        	for (String thumbprint : thumbprints) 
        	{
        		X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);
        		
        		if (entry != null && entry.getCertificate() != null) {
        			certificates.add(entry.getCertificate());
        		}
        	}
        	
        	ByteArrayOutputStream bos = new ByteArrayOutputStream();
        	
            keyAndCertificateWorkflow.getPFX(certificates, password, bos);
            
            return bos.toByteArray();
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
		}
    }    
}
