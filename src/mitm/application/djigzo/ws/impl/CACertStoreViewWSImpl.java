/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.security.cert.CertStoreException;
import java.security.cert.X509CertSelector;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.ws.CACertStoreViewFilter;
import mitm.application.djigzo.ws.CACertStoreViewWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.application.djigzo.ws.X509CertificateDTOBuilder;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.certificate.KeyUsageType;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CACertStoreViewWSImpl implements CACertStoreViewWS
{
    private final static Logger logger = LoggerFactory.getLogger(HierarchicalPropertiesWSImpl.class);
    
    /*
     * The Certificate store
     */
    private final X509CertStoreExt certStore;
    
    /*
     * For building X509 Certificate DTOs
     */
    private final X509CertificateDTOBuilder certificateDTOBuilder;
    
    public CACertStoreViewWSImpl(X509CertStoreExt certStore, X509CertificateDTOBuilder certificateDTOBuilder)
    {
        Check.notNull(certStore, "certStore");
        Check.notNull(certificateDTOBuilder, "certificateDTOBuilder");
        
        this.certStore = certStore;
        this.certificateDTOBuilder = certificateDTOBuilder;
    }
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getCertificates(CACertStoreViewFilter filter, Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        try {
            List<X509CertificateDTO> certificates = new LinkedList<X509CertificateDTO>();

            CloseableIterator<? extends X509CertStoreEntry> iterator = getIterator(filter);
            
            try {
                /*
                 * Because we use a X509CertSelector we cannot use firstResult and maxResults with
                 * getCertStoreIterator because we do not know how many elements are filtered out.
                 * We therefore need to retrieve all elements and filter ourselves.
                 */
                int i = 0;
                
                while (iterator.hasNext())
                {
                    X509CertStoreEntry entry = iterator.next();
                
                    if (firstResult != null && i >= firstResult) {
                        certificates.add(certificateDTOBuilder.buildCertificateDTO(entry.getCertificate(), entry.getKeyAlias()));
                    }
                    
                    i++;
                    
                    if (maxResults != null && certificates.size() == maxResults) {
                        break;
                    }
                }
            }
            finally {
                iterator.close();
            }
            
            return certificates;
        }
        catch (Exception e)
        {
            logger.error("getCertificateCount failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }

    @Override
    @StartTransaction
    public long getCertificateCount(CACertStoreViewFilter filter)
    throws WebServiceCheckedException
    {
        try {
            return CloseableIteratorUtils.getCount(getIterator(filter));
        }
        catch (Exception e)
        {
            logger.error("getCertificateCount failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    private CloseableIterator<? extends X509CertStoreEntry> getIterator(CACertStoreViewFilter filter)
    throws CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();
        
        /*
         * Make sure we only get CA certificates
         */
        selector.setBasicConstraints(0);

        Set<KeyUsageType> keyUsage = new HashSet<KeyUsageType>(); 
        
        keyUsage.add(KeyUsageType.KEYCERTSIGN);
        
        if (filter == CACertStoreViewFilter.CRL_SIGN) {
            keyUsage.add(KeyUsageType.CRLSIGN);
        }
        
        selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsage));
        
        return certStore.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
    }
}
