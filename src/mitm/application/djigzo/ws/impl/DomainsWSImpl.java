/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.ws.DomainDTO;
import mitm.application.djigzo.ws.DomainsWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DomainsWSImpl implements DomainsWS 
{
    private final static Logger logger = LoggerFactory.getLogger(DomainsWSImpl.class);
    
    private final DomainManager domainManager;
    
    public DomainsWSImpl(DomainManager domainManager) 
    {
    	Check.notNull(domainManager, "domainManager");
        
        this.domainManager = domainManager;
    }

    @Override
    @StartTransaction
    public boolean isDomain(String domain) 
    throws WebServiceCheckedException
    {
        try {
            return containsDomainAction(domain);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("isDomain failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isDomain failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public boolean isDomainInUse(String domain)
    throws WebServiceCheckedException
    {
        try {
            return isDomainInUseAction(domain);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("isDomainInUse failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isDomainInUse failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    
    @Override
    @StartTransaction
    public void addDomain(String domain)
    throws WebServiceCheckedException
    {
        try {
            addDomainAction(domain);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("addDomain failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("addDomain failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public boolean deleteDomain(String domain)
    throws WebServiceCheckedException
    {
        try {
            if (domain == null) {
                throw new WebServiceCheckedException("domain is not specified.");
            }
            
            return deleteDomainAction(domain);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteDomain failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteDomain failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public List<DomainDTO> getDomains(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        try {
            return getDomainsAction(firstResult, maxResults, sortDirection);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getDomains failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getDomains failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }    
    }
    
    @Override
    @StartTransaction
    public int getDomainCount()
    throws WebServiceCheckedException    
    {
        try {
            return domainManager.getDomainCount();
        }
        catch(RuntimeException e) 
        {
            logger.error("getDomainCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    private boolean containsDomainAction(String domain)
    throws WebServiceCheckedException
    {
        String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);
        
        if (filteredDomain == null) {
            throw new WebServiceCheckedException("domain '" + domain + "' is invalid.");
        }
        
        return domainManager.getDomainPreferences(filteredDomain) != null;
    }

    private boolean isDomainInUseAction(String domain)
    throws WebServiceCheckedException
    {
        String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);
        
        if (filteredDomain == null) {
            throw new WebServiceCheckedException("domain '" + domain + "' is invalid.");
        }
        
        return domainManager.isDomainInUse(filteredDomain);
    }
    
    private void addDomainAction(String domain)
    throws WebServiceCheckedException
    {
    	String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);

    	if (filteredDomain == null) {
    		throw new WebServiceCheckedException("domain '" + domain + "' is invalid.");
    	}

    	try {
			domainManager.addDomain(filteredDomain);
		} 
    	catch (HierarchicalPropertiesException e) {
    		throw new WebServiceCheckedException(e);
		} 
    	catch (CloseableIteratorException e) {
            throw new WebServiceCheckedException(e);
        }
    }

    private boolean deleteDomainAction(String domain)
    throws WebServiceCheckedException
    {
        boolean deleted = false;
        
        String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);

        if (filteredDomain == null) {
        	throw new WebServiceCheckedException("domain '" + domain + "' is invalid.");
        }

        UserPreferences domainPreferences = domainManager.getDomainPreferences(filteredDomain);

        if (domainPreferences != null) {
        	deleted = domainManager.deleteDomainPreferences(domainPreferences);
        }
        
        return deleted;
    }
    
    private List<DomainDTO> getDomainsAction(Integer firstResult, Integer maxResults, 
    		SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        List<DomainDTO> domains = new LinkedList<DomainDTO>();
        
        try {
        	CloseableIterator<String> domainIterator = domainManager.getDomainIterator(firstResult, 
        			maxResults, sortDirection);
         
        	try {
                while (domainIterator.hasNext())
                {
                    String domainName = domainIterator.next();

                    UserPreferences preferences = domainManager.getDomainPreferences(domainName);
                    
                    DomainDTO domain = new DomainDTO(domainName, preferences.getProperties().getUserLocality(),
                            domainManager.isDomainInUse(domainName));
                    
                    domains.add(domain);
                }
        	}
        	finally {
        		domainIterator.close();
        	}
        	
            return domains;
        }
        catch (CloseableIteratorException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        } 
    }
}
