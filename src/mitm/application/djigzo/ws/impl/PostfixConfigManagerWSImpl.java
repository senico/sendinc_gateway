/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import mitm.application.djigzo.ws.PostfixConfigManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.locale.CharacterEncoding;
import mitm.common.postfix.Postfix;
import mitm.common.postfix.PostfixException;
import mitm.common.postfix.PostfixMainConfigWriter;
import mitm.common.postfix.SaslPassword;
import mitm.common.postfix.SaslPasswordManager;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostfixConfigManagerWSImpl implements PostfixConfigManagerWS
{
	private final static Logger logger = LoggerFactory.getLogger(PostfixConfigManagerWSImpl.class);
	
	/*
	 * The postfix main config file
	 */
	private final File mainConfigFile;

	/*
	 * The script that copies it's input to the main postfix config file
	 */
	private final File copyScript;
	
    /*
     * The script that is used to execute postfix commands
     */
    private final File postfixScript;

    /*
     * Used for setting/getting sasl passwords
     */
    private final SaslPasswordManager passwordManager;
    
	public PostfixConfigManagerWSImpl(File mainConfigFile, File copyScript, File postfixScript, File saslConfigScript)
	{
		Check.notNull(mainConfigFile, "mainConfigFile");
        Check.notNull(copyScript, "copyScript");
        Check.notNull(postfixScript, "postfixScript");
        Check.notNull(saslConfigScript, "saslConfigScript");
		
		this.mainConfigFile = mainConfigFile;
		this.copyScript = copyScript;
		this.postfixScript = postfixScript;
		
		passwordManager = new SaslPasswordManager(saslConfigScript);
	}
	
    @Override
    public String getMainConfig()
    throws WebServiceCheckedException
    {
    	try {
			return FileUtils.readFileToString(mainConfigFile, CharacterEncoding.US_ASCII);
		} 
        catch(IOException e) 
        {
            logger.error("Error reading postfix main.cf file.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getMainConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    public void setMainConfig(String config)
    throws WebServiceCheckedException
    {
    	try {
    		new PostfixMainConfigWriter(copyScript).writeConfig(config);
		} 
        catch(PostfixException e) 
        {
            logger.error("Error copying the new config.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("setMainConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    public List<SaslPassword> getSaslPasswords()
    throws WebServiceCheckedException
    {
        try {
            return passwordManager.getSaslPasswords();
        } 
        catch (IOException e)
        {
            logger.error("getSaslPasswords failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("getSaslPasswords failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public void setSaslPasswords(List<SaslPassword> passwords)
    throws WebServiceCheckedException
    {
        try {
            passwordManager.setSaslPasswords(passwords);
        } 
        catch (IOException e)
        {
            logger.error("setSaslPasswords failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("setSaslPasswords failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
    
    @Override
    public void reload()
    throws WebServiceCheckedException
    {
    	try {
    		new Postfix(postfixScript).reload();
		} 
        catch(PostfixException e) 
        {
            logger.error("Error reloading postfix.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("reload failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }	
}
