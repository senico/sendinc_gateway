/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.ws.AdminDTO;
import mitm.application.djigzo.ws.AdminManagerWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

public class AdminManagerWSImpl implements AdminManagerWS 
{
	private final AdminManager adminManager;
	
	public AdminManagerWSImpl(AdminManager adminManager)
	{
		Check.notNull(adminManager, "adminManager");
				
		this.adminManager = adminManager;
	}
	
	private AdminDTO toAdminDTO(Admin admin)
	{
		Set<Authority> authorities = admin.getAuthorities();
		
		List<String> roles = new LinkedList<String>();
		
		for (Authority authority : authorities)	{
			roles.add(authority.getRole());
		}
		
		return new AdminDTO(admin.getUsername(), admin.getPassword(), admin.getPasswordEncoding(),
				admin.getSalt(), admin.isBuiltIn(), admin.isEnabled(), roles);
	}
	
	@Override
    @StartTransaction
	public AdminDTO addAdmin(String username, boolean builtIn)
	throws WebServiceCheckedException 
	{
		Admin admin = adminManager.addAdmin(username, builtIn);
		
		return toAdminDTO(admin);
	}

	@Override
    @StartTransaction
	public boolean deleteAdmin(String username) 
	throws WebServiceCheckedException 
	{
		return adminManager.deleteAdmin(username);
	}

	@Override
    @StartTransaction
	public void deleteAll() 
	throws WebServiceCheckedException 
	{
		adminManager.deleteAll();
	}

	@Override
    @StartTransaction
	public AdminDTO getAdmin(String username) 
	throws WebServiceCheckedException 
	{
		AdminDTO adminDTO = null;

		Admin admin = adminManager.getAdmin(username);
		
		if (admin != null) {
			adminDTO = toAdminDTO(admin);
		}
		
		return adminDTO;
	}

	@Override
    @StartTransaction
	public int getAdminCount() 
	throws WebServiceCheckedException 
	{
		return adminManager.getAdminCount();
	}

	@Override
    @StartTransaction
	public List<AdminDTO> getAdmins(Integer firstResult, Integer maxResults,
			SortDirection sortDirection) 
	throws WebServiceCheckedException 
	{
		List<AdminDTO> adminDTOs = new LinkedList<AdminDTO>();
		
		List<? extends Admin> admins = adminManager.getAdmins(firstResult, maxResults, sortDirection);
		
		for (Admin admin : admins) {
			adminDTOs.add(toAdminDTO(admin));
		}
		
		return adminDTOs;
	}
}
