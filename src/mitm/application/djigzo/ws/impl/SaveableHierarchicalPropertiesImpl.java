/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.PropertyDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.SaveableHierarchicalProperties;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

/**
 * SaveableHierarchicalPropertiesImpl is an implementation of SaveableHierarchicalProperties that
 * does not immediately persist the properties when they are set. The properties are persisted 
 * when save is called. If two operations are incompatible, like setting and deleting the same
 * property without calling save between the actions the delete "wins".
 * 
 * Note: SaveableHierarchicalPropertiesImpl does not completely follow the HierarchicalProperties
 * contract. Properties that are deleted, or set only return the new values after save has
 * been called. In other words, until save is called the old values are returned.
 * 
 * @author Martijn Brinkers
 *
 */
public class SaveableHierarchicalPropertiesImpl implements SaveableHierarchicalProperties
{	
    /*
     * Used to temporarily store properties that are being set. These properties will be save
     * when save is called. We need to temporarily store these properties because we want
     * to save them in one transaction. Soap however does not 'support' transactions so 
     * we will save them all at once.
     */
	protected final Map<String, PropertyDTO> toSet = new HashMap<String, PropertyDTO>(); 

	/*
	 * Will store the properties we need to delete
	 */
	protected final Set<String> toDelete = new HashSet<String>(); 

    /*
     * If true all properties will be deleted.
     */
	protected boolean deleteAll;
    
    /*
     * The properties web service
     */
    protected final HierarchicalPropertiesWS hierarchicalPropertiesWS;

    /*
     * The user property we are working on
     */
    protected final UserPreferencesDTO userPreferencesDTO;
    
	public SaveableHierarchicalPropertiesImpl(HierarchicalPropertiesWS hierarchicalPropertiesWS,
			UserPreferencesDTO userPreferencesDTO)
	{
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");
		Check.notNull(userPreferencesDTO, "userPreferencesDTO");

		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
		this.userPreferencesDTO = userPreferencesDTO;
	}
		
	@Override
    public void deleteAll() 
	throws HierarchicalPropertiesException 
	{
	    deleteAll = true;
	}

    @Override
    public String getCategory()	{
		return userPreferencesDTO.getCategory();
	}
    
    @Override
    public String getProperty(String propertyName, boolean decrypt) 
	throws HierarchicalPropertiesException
	{
		try {
		    return hierarchicalPropertiesWS.getProperty(userPreferencesDTO, propertyName, decrypt);
		} 
		catch (WebServiceCheckedException e) {
			throw new HierarchicalPropertiesException(e);
		}
	}

    @Override
    public void setProperty(String propertyName, String value, boolean encrypt) 
	throws HierarchicalPropertiesException
	{
        toSet.put(propertyName, new PropertyDTO(propertyName, value, encrypt, value != null));
	}
    
    @Override
    public void deleteProperty(String propertyName) 
	throws HierarchicalPropertiesException
	{
        toDelete.add(propertyName);
	}
        
    @Override
    public boolean isInherited(String propertyName) 
	throws HierarchicalPropertiesException
	{
		try {
			return hierarchicalPropertiesWS.isInherited(userPreferencesDTO, propertyName);
		} 
		catch (WebServiceCheckedException e) {
			throw new HierarchicalPropertiesException(e);
		}
	}
    
    @Override
    public Set<String> getProperyNames(boolean recursive) 
    throws HierarchicalPropertiesException
	{
		try {
			/*
			 * CXF does not yet support a Set as return type so we need to convert it from
			 * List to Set. 
			 * 
			 * See: https://issues.apache.org/jira/browse/CXF-1558?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel
			 */
			return new HashSet<String>(hierarchicalPropertiesWS.getProperyNames(userPreferencesDTO, 
					recursive));
		} 
		catch (WebServiceCheckedException e) {
			throw new HierarchicalPropertiesException(e);
		}
	}
    
    private void resetAll() 
    {
        toSet.clear();
        toDelete.clear();
        deleteAll = false;
    }
    
    @Override
    public void save() 
    throws HierarchicalPropertiesException
    {
        try {
            if (deleteAll) {
                hierarchicalPropertiesWS.deleteAll(userPreferencesDTO);
            }
            else {
                for (String propertyName : toDelete) {
                    toSet.remove(propertyName);
                }
                
                List<PropertyDTO> save = new LinkedList<PropertyDTO>(toSet.values());
                List<String> delete = new LinkedList<String>(toDelete);
                
                hierarchicalPropertiesWS.setProperties(userPreferencesDTO, save, delete);
            }
            
            resetAll();
        }
        catch (WebServiceCheckedException e) {
            throw new HierarchicalPropertiesException(e);
        }
    }
}
