/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.LinkedList;
import java.util.List;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.UserDTO;
import mitm.application.djigzo.ws.UsersWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsersWSImpl implements UsersWS
{
    private final static Logger logger = LoggerFactory.getLogger(UsersWSImpl.class);
    
    private final UserWorkflow userWorkflow;
    
    public UsersWSImpl(UserWorkflow userWorkflow) 
    {
    	Check.notNull(userWorkflow, "userWorkflow");
    	
        this.userWorkflow = userWorkflow;
    }

    @Override
    @StartTransaction
    public boolean isUser(String email) 
    throws WebServiceCheckedException
    {
        try {
            return containsUserAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("isUser failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isUser failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public void addUser(String email)
    throws WebServiceCheckedException
    {
        try {
            addUserAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("addUser failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("addUser failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public boolean deleteUser(String email)
    throws WebServiceCheckedException
    {
        try {
            if (email == null) {
                throw new WebServiceCheckedException("email is not specified.");
            }
            
            return deleteUserAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteUser failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteUser failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public List<UserDTO> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        try {
            return getUsersAction(firstResult, maxResults, sortDirection);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getUsers failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getUsers failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }    
    }
    
    @Override
    @StartTransaction
    public List<UserDTO> searchUsers(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        try {
            return searchUsersAction(search, firstResult, maxResults, sortDirection);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("searchUsers failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("searchUsers failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }    
    }
    
    @Override
    @StartTransaction
    public int getSearchUsersCount(String search)
    throws WebServiceCheckedException
    {
        try {
            return userWorkflow.getSearchEmailCount(search);
        }
        catch(RuntimeException e) 
        {
            logger.error("getSearchUsersCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }    
    }
    
    @Override
    @StartTransaction
    public int getUserCount()
    throws WebServiceCheckedException    
    {
        try {
            return userWorkflow.getUserCount();
        }
        catch(RuntimeException e) 
        {
            logger.error("getUserCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    private boolean containsUserAction(String email)
    throws WebServiceCheckedException
    {
        try {
            String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
            
            if (filteredEmail == null) {
                throw new WebServiceCheckedException("email address '" + email + "' is invalid.");
            }
            
            User user = userWorkflow.getUser(filteredEmail, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);

            return user != null;
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
    }

    private void addUserAction(String email)
    throws WebServiceCheckedException
    {
        try {
            String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
            
            if (filteredEmail == null) {
                throw new WebServiceCheckedException("email address '" + email + "' is invalid.");
            }
            
            if (!isUser(filteredEmail))
            {
                User user = userWorkflow.addUser(filteredEmail);
                
                userWorkflow.makePersistent(user);
            }
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
    }

    private boolean deleteUserAction(String email)
    throws WebServiceCheckedException
    {
        boolean deleted = false;
        
        try {            
            String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
            
            if (filteredEmail == null) {
                throw new WebServiceCheckedException("email address '" + email + "' is invalid.");
            }
            
            User user = userWorkflow.getUser(filteredEmail, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
            
            if (user != null) {
                deleted = userWorkflow.deleteUser(user);
            }
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
        
        return deleted;
    }
    
    private UserDTO getUser(String email)
    throws AddressException, HierarchicalPropertiesException 
    {
        User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
        
        UserLocality userLocality = user.getUserPreferences().getProperties().getUserLocality(); 
        
        return new UserDTO(email, userLocality);
    }
    
    private List<UserDTO> getUsersAction(Integer firstResult, Integer maxResults, 
    		SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        List<UserDTO> users = new LinkedList<UserDTO>();
        
        try {
        	CloseableIterator<String> emailIterator = userWorkflow.getEmailIterator(firstResult, 
        			maxResults, sortDirection);
         
        	try {
                while (emailIterator.hasNext())
                {
                    String email = emailIterator.next();
                    
                    /*
                     * loading the UserLocality is an exception to all other properties because
                     * we need this in the user overview grid. Loading it on demand is too slow.
                     * See UserDTO for some more info.
                     */
                    users.add(getUser(email));
                }
        	}
        	finally {
        		emailIterator.close();
        	}
        	
            return users;
        }
        catch (CloseableIteratorException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        } 
    }

    private List<UserDTO> searchUsersAction(String search, Integer firstResult, Integer maxResults, 
            SortDirection sortDirection)
    throws WebServiceCheckedException
    {
        List<UserDTO> users = new LinkedList<UserDTO>();
        
        try {
            CloseableIterator<String> emailIterator = userWorkflow.searchEmail(search, firstResult, 
                    maxResults, sortDirection);
         
            try {
                while (emailIterator.hasNext())
                {
                    String email = emailIterator.next();
                    
                    /*
                     * loading the UserLocality is an exception to all other properties because
                     * we need this in the user overview grid. Loading it on demand is too slow.
                     * See UserDTO for some more info.
                     */
                    users.add(getUser(email));
                }
            }
            finally {
                emailIterator.close();
            }
            
            return users;
        }
        catch (CloseableIteratorException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        } 
    }
}
