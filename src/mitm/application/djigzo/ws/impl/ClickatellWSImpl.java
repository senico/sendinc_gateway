/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import mitm.application.djigzo.ws.ClickatellWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.net.ProxyInjector;
import mitm.common.sms.SMSTransportException;
import mitm.common.sms.transport.clickatell.ClickatellSMSTransport;
import mitm.common.sms.transport.clickatell.StaticClickatellParameterProvider;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClickatellWSImpl implements ClickatellWS
{
    private final static Logger logger = LoggerFactory.getLogger(ClickatellWSImpl.class);
        
    private final String clickatellURL;
    
    /*
     * Used for setting the proxy
     */
    private final ProxyInjector proxyInjector;
    
    public ClickatellWSImpl(String clickatellURL, ProxyInjector proxyInjector)
    {
        Check.notNull(clickatellURL, "clickatellURL");
        Check.notNull(proxyInjector, "proxyInjector");
        
        this.clickatellURL = clickatellURL;
        this.proxyInjector = proxyInjector;
    }
    
    @Override
    @StartTransaction
    public float getBalance(String apiID, String username, String password)
    throws WebServiceCheckedException
    {
        try {
            if (StringUtils.isEmpty(apiID)) {
                throw new WebServiceCheckedException("apiID is missing.");
            }
            
            if (StringUtils.isEmpty(username)) {
                throw new WebServiceCheckedException("username is missing.");
            }
            
            if (StringUtils.isEmpty(password)) {
                throw new WebServiceCheckedException("password is missing.");
            }
            
            /*
             * We are not using the registered Clickatell transport service because
             * it uses the credentials from the global settings. We would like to 
             * use the credentials provided by the web service client.
             * 
             * Note: the main disadvantage is that we not also need to provide the
             * Clickatell URL. Another option would be to refactor ClickatellSMSTransport
             * so we can also specify the parameter provider (this is currently not
             * possible because ClickatellSMSTransport is a singleton service).
             */
            StaticClickatellParameterProvider parameters = new StaticClickatellParameterProvider(apiID, 
                    username, password, "");
        
            ClickatellSMSTransport smsTransport = new ClickatellSMSTransport(clickatellURL, parameters, proxyInjector);
            
            try {
                return smsTransport.getBalance();
            } 
            catch (SMSTransportException e) {
                throw new WebServiceCheckedException("Error getting Clickatell balance.", e);
            }
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getBalance failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getBalance failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
}
