/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.PortalProperties;
import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.AdminDTO;
import mitm.application.djigzo.ws.LoginWS;
import mitm.application.djigzo.ws.PortalUserDTO;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginWSImpl implements LoginWS 
{
    private final static Logger logger = LoggerFactory.getLogger(LoginWSImpl.class);
    
    /*
     * For managing the admin users
     */
	private final AdminManager adminManager;
	
	/*
	 * For looking up the user
	 */
	private final UserWorkflow userWorkflow;
	
	/*
	 * The static list of roles assigned to a portal user
	 */
	private final static String[] PORTAL_ROLES = new String[]{FactoryRoles.ROLE_PORTAL_LOGIN};
	
	public LoginWSImpl(AdminManager adminManager, UserWorkflow userWorkflow)
	{
        Check.notNull(adminManager, "adminManager");
        Check.notNull(userWorkflow, "userWorkflow");
		
		this.adminManager = adminManager;
		this.userWorkflow = userWorkflow;
	}
	
	private AdminDTO toAdminDTO(Admin admin)
	{
		Set<Authority> authorities = admin.getAuthorities();
		
		List<String> roles = new LinkedList<String>();
		
		for (Authority authority : authorities)	{
			roles.add(authority.getRole());
		}
		
		return new AdminDTO(admin.getUsername(), admin.getPassword(), admin.getPasswordEncoding(),
				admin.getSalt(), admin.isBuiltIn(), admin.isEnabled(), roles);
	}
	
	@Override
    @StartTransaction
	public AdminDTO getAdmin(String username) 
	throws WebServiceCheckedException 
	{
		AdminDTO adminDTO = null;

		Admin admin = adminManager.getAdmin(username);
		
		if (admin != null) {
			adminDTO = toAdminDTO(admin);
		}
		
		return adminDTO;
	}

    @Override
    @StartTransaction
    public PortalUserDTO getPortalUser(String email)
    throws WebServiceCheckedException
    {
        try {
            String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
            
            if (validatedEmail == null)
            {
                logger.warn("Email address " + email + " is not a valid email address");
                
                return null;
            }
            
            PortalProperties portalProperties = userWorkflow.getUser(validatedEmail, 
                    UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).getUserPreferences().
                    getProperties().getPortalProperties();
                        
            boolean portalEnabled = portalProperties.isEnabled();
            
            String portalPassword = portalProperties.getPassword();
            
            /*
             * If there is no password set, make sure the user cannot login
             */
            if (StringUtils.isEmpty(portalPassword)) {
                portalEnabled = false;
            }
            
            return new PortalUserDTO(validatedEmail, portalPassword, portalEnabled, PORTAL_ROLES);
        }
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        }
    }
}
