/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.security.cert.CertStoreException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWorkflowWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;

public class UserPreferencesWorkflowWSImpl implements UserPreferencesWorkflowWS
{
    private final UserPreferencesWorkflow userPreferencesWorkflow;

    private final X509CertStoreExt certStore;
        
    public UserPreferencesWorkflowWSImpl(UserPreferencesWorkflow userPreferencesWorkflow, X509CertStoreExt certStore)
    {
        Check.notNull(userPreferencesWorkflow, "userPreferencesWorkflow");
        Check.notNull(certStore, "certStore");
        
        this.userPreferencesWorkflow = userPreferencesWorkflow;
        this.certStore = certStore;
    }

    @Override
    @StartTransaction
    public List<UserPreferencesDTO> getReferencingFromCertificates(String certificateThumbprint, 
            Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            List<UserPreferencesDTO> result = null;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null)
            {
                List<UserPreferences> preferences = userPreferencesWorkflow.getReferencingFromCertificates(
                        certificateEntry.getCertificate(), firstResult, maxResults);
                
                result = new ArrayList<UserPreferencesDTO>(preferences.size());
                
                for (UserPreferences preference : preferences) {
                    result.add(new UserPreferencesDTO(preference.getCategory(), preference.getName()));
                }
            }
            
            if (result == null) {
                result = Collections.emptyList();
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }

    @Override
    @StartTransaction
    public long getReferencingFromCertificatesCount(String certificateThumbprint)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            long result = 0;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null) {
                result = userPreferencesWorkflow.getReferencingFromCertificatesCount(certificateEntry.getCertificate());
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }

    @Override
    @StartTransaction
    public List<UserPreferencesDTO> getReferencingFromNamedCertificates(String certificateThumbprint, 
            Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            List<UserPreferencesDTO> result = null;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null)
            {
                List<UserPreferences> preferences = userPreferencesWorkflow.getReferencingFromNamedCertificates(
                        certificateEntry.getCertificate(), firstResult, maxResults);
                
                result = new ArrayList<UserPreferencesDTO>(preferences.size());
                
                for (UserPreferences preference : preferences) {
                    result.add(new UserPreferencesDTO(preference.getCategory(), preference.getName()));
                }
            }
            
            if (result == null) {
                result = Collections.emptyList();
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }

    @Override
    @StartTransaction
    public long getReferencingFromNamedCertificatesCount(String certificateThumbprint)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            long result = 0;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null) {
                result = userPreferencesWorkflow.getReferencingFromNamedCertificatesCount(certificateEntry.getCertificate());
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public List<UserPreferencesDTO> getReferencingFromKeyAndCertificate(String certificateThumbprint, 
            Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            List<UserPreferencesDTO> result = null;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null)
            {
                List<UserPreferences> preferences = userPreferencesWorkflow.getReferencingFromKeyAndCertificate(
                        certificateEntry.getCertificate(), firstResult, maxResults);
                
                result = new ArrayList<UserPreferencesDTO>(preferences.size());
                
                for (UserPreferences preference : preferences) {
                    result.add(new UserPreferencesDTO(preference.getCategory(), preference.getName()));
                }
            }
            
            if (result == null) {
                result = Collections.emptyList();
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public long getReferencingFromKeyAndCertificateCount(String certificateThumbprint)
    throws WebServiceCheckedException
    {
        if (StringUtils.isBlank(certificateThumbprint)) {
            throw new WebServiceCheckedException("certificateThumbprint must be given.");
        }
        
        try {
            X509CertStoreEntry certificateEntry = certStore.getByThumbprint(certificateThumbprint);

            long result = 0;
            
            if (certificateEntry != null && certificateEntry.getCertificate() != null) {
                result = userPreferencesWorkflow.getReferencingFromKeyAndCertificateCount(certificateEntry.getCertificate());
            }
            
            return result;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }
}
