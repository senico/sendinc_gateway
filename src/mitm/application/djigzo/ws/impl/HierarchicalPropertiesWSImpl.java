/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.LinkedList;
import java.util.List;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.PropertyDTO;
import mitm.application.djigzo.ws.PropertySelectorDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of HierarchicalPropertiesWS that provides a SOAP interface
 * 
 * @author Martijn Brinkers
 *
 */
public class HierarchicalPropertiesWSImpl implements HierarchicalPropertiesWS
{
    private final static Logger logger = LoggerFactory.getLogger(HierarchicalPropertiesWSImpl.class);
    
    /*
     * For getting UserPreference's for a given category (domain, user etc.)
     */
    private final UserPreferencesCategoryManager categoryManager;
    
    /*
     * For gettings user(s) from email address.
     */
    private final UserWorkflow userWorkflow;
    
    public HierarchicalPropertiesWSImpl(UserPreferencesCategoryManager categoryManager, UserWorkflow userWorkflow) 
    {
    	Check.notNull(categoryManager, "categoryManager");
    	Check.notNull(userWorkflow, "userWorkflow");
        
        this.categoryManager = categoryManager;
        this.userWorkflow = userWorkflow;
    }
    
    @Override
    @StartTransaction
    public String getProperty(UserPreferencesDTO userPreferences, String propertyName, boolean decrypt)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getPropertyAction(userPreferences, propertyName, decrypt);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getProperty failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getProperty failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public List<PropertyDTO> getProperties(UserPreferencesDTO userPreferences, List<PropertySelectorDTO> selectors)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
           
            List<PropertyDTO> properties = new LinkedList<PropertyDTO>();
            
            if (selectors != null)
            {
                for (PropertySelectorDTO selector : selectors)
                {
                    String value = getProperty(userPreferences, selector.getPropertyName(), selector.isEncrypt());
                    
                    boolean inherited = isInheritedAction(userPreferences, selector.getPropertyName());
                    
                    properties.add(new PropertyDTO(selector.getPropertyName(), value, selector.isEncrypt(), inherited));
                }
            }
            
            return properties;
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getProperties failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getProperties failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }    
    
    /**
     * Sets the property for the given userPreferences.
     */
    @Override
    @StartTransaction
    public void setProperty(UserPreferencesDTO userPreferences, String propertyName, String value,
    		boolean encrypt)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            setPropertyAction(userPreferences, propertyName, value, encrypt);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("setProperty failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setProperty failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    /**
     * Sets the properties for the given userPreferences.
     */
    @Override
    @StartTransaction
    public void setProperties(UserPreferencesDTO userPreferences, List<PropertyDTO> propertiesToSet,
            List<String> propertiesToDelete)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);

            HierarchicalProperties userProperties = getUserProperties(userPreferences);
            
            if (propertiesToSet != null)
            {

                for (PropertyDTO property : propertiesToSet)
                {
                    userProperties.setProperty(property.getPropertyName(), property.getValue(), 
                            property.isEncrypt());                    
                }
            }
            if (propertiesToDelete != null)
            {
                for (String propertyName : propertiesToDelete)
                {
                    userProperties.deleteProperty(propertyName);                    
                }
            }
        }
        catch (HierarchicalPropertiesException e) {
            logger.error("setProperties failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(WebServiceCheckedException e)
        {
            logger.error("setProperties failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setProperties failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }    
    
    /**
     * Deletes the property with the given name. If the property does not exist on this object
     * nothing happens. The parent property is not removed.
     */
    @Override
    @StartTransaction
    public void deleteProperty(UserPreferencesDTO userPreferences, String propertyName)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            deletePropertyAction(userPreferences, propertyName);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteProperty failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteProperty failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    /**
     * Deletes all properties of this property object. The parent properties are not deleted.
     */
    @Override
    @StartTransaction
    public void deleteAll(UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            deleteAllAction(userPreferences);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteAll failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteAll failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    /**
     * Returns true if this properties does not have the property but the parent has.
     */
    @Override
    @StartTransaction
    public boolean isInherited(UserPreferencesDTO userPreferences, String propertyName)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return isInheritedAction(userPreferences, propertyName);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("isInherited failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isInherited failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    /**
     * Returns an unmodifiable Set of all the property names. If recursive is true the property names
     * of the parent are returned as well.
     */
    @Override
    @StartTransaction
    public List<String> getProperyNames(UserPreferencesDTO userPreferences, boolean recursive)
    throws WebServiceCheckedException    
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getPropertyNamesAction(userPreferences, recursive);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getProperyNames failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getProperyNames failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    private String getPropertyAction(UserPreferencesDTO userPreferencesDTO, String propertyName,
    		boolean decrypt)
    throws WebServiceCheckedException
    {
        String propertyValue = null; 
        
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);

        try {
			propertyValue = properties.getProperty(propertyName, decrypt);
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
        
        return propertyValue;
    }    

    private void setPropertyAction(UserPreferencesDTO userPreferencesDTO, String propertyName, String value,
    		boolean encrypt)
    throws WebServiceCheckedException
    {
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);

        try {
			properties.setProperty(propertyName, value, encrypt);
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
    }    

    private void deletePropertyAction(UserPreferencesDTO userPreferencesDTO, String propertyName)
    throws WebServiceCheckedException
    {
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);

        try {
			properties.deleteProperty(propertyName);
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
    }    

    private void deleteAllAction(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);

        try {
			properties.deleteAll();
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
    }    

    private Boolean isInheritedAction(UserPreferencesDTO userPreferencesDTO, String propertyName)
    throws WebServiceCheckedException
    {
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);

        try {
			return properties.isInherited(propertyName);
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
    }    

    private List<String> getPropertyNamesAction(UserPreferencesDTO userPreferencesDTO, boolean recursive)
    throws WebServiceCheckedException
    {
        HierarchicalProperties properties = getUserProperties(userPreferencesDTO);
        
        try {
			return new LinkedList<String>(properties.getProperyNames(recursive));
		} 
        catch (HierarchicalPropertiesException e) {
        	throw new WebServiceCheckedException(e);
		}
    }    
    
    private HierarchicalProperties getUserProperties(UserPreferencesDTO userPreferencesDTO) 
    throws WebServiceCheckedException
    {
        UserPreferences userPreferences = null;
     
        /*
         * We will treat a user category differently. A user should not always exists yet the user does have
         * properties because any user can inherit from a domain or global settings. The UserPreferencesManager
         * only 'works' on existing UserPreferences. We therefore need to treat the user differently and use
         * userWorkflow to get the user.
         *  
         * Note: It would perhaps be nicer if we refactor the web services a bit so we don't need to this 'exception'.
         * Perhaps create another version of UserPreferencesWS specialized for only Users?
         */
        if (UserPreferencesCategory.fromName(userPreferencesDTO.getCategory()) == UserPreferencesCategory.USER)
        {
        	try {
        		userPreferences = userWorkflow.getUser(userPreferencesDTO.getName(), 
        				UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).getUserPreferences();				
			} 
        	catch (AddressException e) {
        		throw new WebServiceCheckedException(e);
			} 
        	catch (HierarchicalPropertiesException e) {
        		throw new WebServiceCheckedException(e);
			}
        }
        else 
        {
	        UserPreferencesManager preferencesManager = categoryManager.getUserPreferencesManager(
	                userPreferencesDTO.getCategory());
	        
	        if (preferencesManager != null) {
	            userPreferences = preferencesManager.getUserPreferences(userPreferencesDTO.getName());
	        }
        }

        if (userPreferences == null) {
            throw new WebServiceCheckedException("userPreferences " + userPreferencesDTO + " not found.");
        }
        
        HierarchicalProperties userProperties = userPreferences.getProperties();
        
        if (userProperties == null) {
            throw new WebServiceCheckedException("userProperties " + userPreferencesDTO + " not found.");            
        }
        
        return userProperties;
    }
    
    private void checkUserPreferencesParameter(UserPreferencesDTO userPreferences) 
    throws WebServiceCheckedException
    {
        if (userPreferences == null || userPreferences.getCategory() == null || userPreferences.getName() == null) {
            throw new WebServiceCheckedException("user is null or category or name is not specified.");
        }
    }
}
