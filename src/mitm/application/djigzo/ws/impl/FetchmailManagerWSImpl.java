/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import mitm.application.djigzo.ws.FetchmailManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.fetchmail.FetchmailConfig;
import mitm.common.fetchmail.FetchmailException;
import mitm.common.fetchmail.FetchmailManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FetchmailManagerWSImpl implements FetchmailManagerWS
{
    private final static Logger logger = LoggerFactory.getLogger(FetchmailManagerWSImpl.class);
    
    /*
     * Manager for Fetchmail
     */
    private final FetchmailManager fetchmailManager;

    public FetchmailManagerWSImpl(FetchmailManager fetchmailManager)
    {
        Check.notNull(fetchmailManager, "fetchmailManager");
        
        this.fetchmailManager = fetchmailManager;
    }
    
    @Override
    @StartTransaction
    public FetchmailConfig getConfig()
    throws WebServiceCheckedException
    {
        try {
            return fetchmailManager.getConfig();
        }
        catch (FetchmailException e) {
            logger.error("getConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
        catch(RuntimeException e) 
        {
            logger.error("getConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public void setConfig(FetchmailConfig config)
    throws WebServiceCheckedException
    {
        try {
            fetchmailManager.setConfig(config);
        }
        catch (FetchmailException e) {
            logger.error("setConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
        catch(RuntimeException e) 
        {
            logger.error("setConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public void applyConfig()
    throws WebServiceCheckedException
    {
        try {
            fetchmailManager.applyConfig();
        }
        catch (FetchmailException e) {
            logger.error("applyConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
        catch(RuntimeException e) 
        {
            logger.error("applyConfig failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
}
