/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.ArrayList;
import java.util.List;

import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.ws.AuthorityDTO;
import mitm.application.djigzo.ws.AuthorityManagerWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

public class AuthorityManagerWSImpl implements AuthorityManagerWS
{
	private final AuthorityManager authorityManager;
	
	public AuthorityManagerWSImpl(AuthorityManager authorityManager)
	{
		Check.notNull(authorityManager, "authorityManager");
		
		this.authorityManager = authorityManager;
	}
	
	private AuthorityDTO toDTO(Authority authority)	{
		return new AuthorityDTO(authority.getRole());
	}
	
	@Override
    @StartTransaction
	public AuthorityDTO addAuthority(String role)
	throws WebServiceCheckedException
	{
		Authority authority = authorityManager.addAuthority(role);
		
		return toDTO(authority);
	}

	@Override
    @StartTransaction
	public void deleteAuthority(String role) 
	throws WebServiceCheckedException 
	{
		authorityManager.deleteAuthority(role);
	}

	@Override
    @StartTransaction
	public List<AuthorityDTO> getAuthorities(Integer firstResult, Integer maxResults, SortDirection sortDirection)
	throws WebServiceCheckedException
	{
		List<? extends Authority> authorities = authorityManager.getAuthorities(firstResult, maxResults, sortDirection);
		
		List<AuthorityDTO> dtos = new ArrayList<AuthorityDTO>(authorities.size());
		
		for (Authority authority : authorities) {
			dtos.add(toDTO(authority));
		}
		
		return dtos;
	}

	@Override
    @StartTransaction
	public AuthorityDTO getAuthority(String role)
	{
		AuthorityDTO dto = null;

		Authority authority = authorityManager.getAuthority(role);
		
		if (authority != null) {
			dto = toDTO(authority);
		}
		
		return dto;
	}
}
