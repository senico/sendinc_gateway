/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.application.djigzo.dlp.PolicyPatternManager;
import mitm.application.djigzo.dlp.PolicyPatternNode;
import mitm.application.djigzo.dlp.UserPolicyPatternNode;
import mitm.application.djigzo.dlp.UserPreferencesPolicyPatternManager;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesPolicyPatternManagerWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link UserPreferencesPolicyPatternManagerWS}
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesPolicyPatternManagerWSImpl implements UserPreferencesPolicyPatternManagerWS
{
    private final static Logger logger = LoggerFactory.getLogger(UserPreferencesPolicyPatternManagerWSImpl.class);
    
    /*
     * Used to manage all the policy patterns
     */
    private final PolicyPatternManager policyPatternManager; 

    /*
     * Used to manage the selected policy patterns for a user, domain or global preference.
     */
    private final UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;
    
    /*
     * Used for getting the user, domain or global user preferences. 
     */
    private final UserPreferencesCategoryManager userPreferencesCategoryManager;
    
    public UserPreferencesPolicyPatternManagerWSImpl(PolicyPatternManager policyPatternManager,
            UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager,
            UserPreferencesCategoryManager userPreferencesCategoryManager)
    {
        Check.notNull(policyPatternManager, "policyPatternManager");
        Check.notNull(userPreferencesPolicyPatternManager, "userPreferencesPolicyPatternManager");
        Check.notNull(userPreferencesCategoryManager, "userPreferencesCategoryManager");
        
        this.policyPatternManager = policyPatternManager;
        this.userPreferencesPolicyPatternManager = userPreferencesPolicyPatternManager;
        this.userPreferencesCategoryManager = userPreferencesCategoryManager;
    }

    private UserPreferences getUserPreferences(UserPreferencesDTO userPreferencesDTO)
    {
        if (userPreferencesDTO == null) {
            return null;
        }
        
        UserPreferences userPreferences = null;
        
        UserPreferencesManager preferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(
                userPreferencesDTO.getCategory());
        
        if (preferencesManager != null) {
            userPreferences = preferencesManager.getUserPreferences(userPreferencesDTO.getName());
        }
        
        return userPreferences;
    }
    
    @Override
    @StartTransaction
    public List<PolicyPatternNodeDTO> getPatterns(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
        UserPreferences userPreferences = getUserPreferences(userPreferencesDTO);
        
        List<PolicyPatternNodeDTO> patternNodesDTO = null;
        
        if (userPreferences != null)
        {
            Set<PolicyPatternNode> patternNodes = userPreferencesPolicyPatternManager.getPatterns(userPreferences);
            
            if (patternNodes != null)
            {
                patternNodesDTO = new ArrayList<PolicyPatternNodeDTO>(patternNodes.size());
                
                for (PolicyPatternNode patternNode : patternNodes) {
                    patternNodesDTO.add(PolicyPatternNodeDTO.toDTO(patternNode));
                }
            }
        }
        
        return patternNodesDTO;
    }

    @Override
    @StartTransaction
    public void setPatterns(UserPreferencesDTO userPreferencesDTO, Collection<String> patternNodeNames)
    throws WebServiceCheckedException
    {
        UserPreferences userPreferences = getUserPreferences(userPreferencesDTO);
        
        if (userPreferences != null)
        {
            List<PolicyPatternNode> policyPatternNodes = new ArrayList<PolicyPatternNode>(
                    CollectionUtils.getSize(patternNodeNames));

            if (patternNodeNames != null)
            {
                for (String patternNodeName : patternNodeNames)
                {
                    PolicyPatternNode patternNode = policyPatternManager.getPattern(patternNodeName);
                    
                    if (patternNode != null) {
                        policyPatternNodes.add(patternNode);
                    }
                    else {
                        logger.warn("Pattern with name " + patternNodeName + " was not found.");
                    }
                }
            }
            
            userPreferencesPolicyPatternManager.setPatterns(userPreferences, policyPatternNodes);
        }
    }
    
    /*
     * Return all patterns from the patterns collection that are not inherited.
     */
    private Set<PolicyPatternNode> getNonInheritedPatterns(Collection<PolicyPatternNode> patterns)
    {
        Set<PolicyPatternNode> nonInheritedPatterns = new HashSet<PolicyPatternNode>();

        if (patterns != null)
        {
            /*
             * Only non inherited patterns will be used. Normally either all patterns
             * are inherited, or no pattern is inherited.
             */
            for (PolicyPatternNode patternNode : patterns)
            {
                boolean inherited = false;
                
                if (patternNode instanceof UserPolicyPatternNode) {
                    inherited = ((UserPolicyPatternNode) patternNode).isInherited();
                }
                
                if (!inherited) {
                    nonInheritedPatterns.add(patternNode);
                }
            }
        }
        
        return nonInheritedPatterns;
    }
    
    @Override
    @StartTransaction
    public void addPatterns(UserPreferencesDTO userPreferencesDTO, Collection<String> patternNodeNames)
    throws WebServiceCheckedException
    {
        
        UserPreferences userPreferences = getUserPreferences(userPreferencesDTO);
        
        if (userPreferences != null)
        {
            Set<PolicyPatternNode> nonInherited = getNonInheritedPatterns(
                    userPreferencesPolicyPatternManager.getPatterns(userPreferences));
            
            /*
             * Now add all patterns we need to add to the nonInherited
             */
            if (patternNodeNames != null)
            {
                for (String patternNodeName : patternNodeNames)
                {
                    PolicyPatternNode patternNode = policyPatternManager.getPattern(patternNodeName);
                    
                    if (patternNode != null) {
                        nonInherited.add(patternNode);
                    }
                    else {
                        logger.warn("Pattern with name " + patternNodeName + " was not found.");
                    }
                }
            }
            
            userPreferencesPolicyPatternManager.setPatterns(userPreferences, nonInherited);
        }
    }

    @Override
    @StartTransaction
    public void removePatterns(UserPreferencesDTO userPreferencesDTO, Collection<String> patternNodeNames)
    throws WebServiceCheckedException
    {
        UserPreferences userPreferences = getUserPreferences(userPreferencesDTO);
        
        if (userPreferences != null)
        {
            Set<PolicyPatternNode> nonInherited = getNonInheritedPatterns(
                    userPreferencesPolicyPatternManager.getPatterns(userPreferences));
            
            /*
             * Now remove all patterns from the nonInherited
             */
            if (patternNodeNames != null)
            {
                for (String patternNodeName : patternNodeNames)
                {
                    PolicyPatternNode patternNode = policyPatternManager.getPattern(patternNodeName);
                    
                    if (patternNode != null) {
                        nonInherited.remove(patternNode);
                    }
                    else {
                        logger.warn("Pattern with name " + patternNodeName + " was not found.");
                    }
                }
            }
            
            userPreferencesPolicyPatternManager.setPatterns(userPreferences, nonInherited);
        }
    }
}
