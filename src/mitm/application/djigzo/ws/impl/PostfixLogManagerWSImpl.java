/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import mitm.application.djigzo.ws.PostfixLogManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.postfix.PostfixLogItem;
import mitm.common.postfix.PostfixLogParser;
import mitm.common.util.Check;
import mitm.common.util.MetaReader;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostfixLogManagerWSImpl implements PostfixLogManagerWS
{
    private final static Logger logger = LoggerFactory.getLogger(PostfixLogManagerWSImpl.class);
    
    private final static int MAX_NUMBER_OF_LOG_LINES = 1000; 
    
    /*
     * Paths to the James log files
     */
    private final List<String> logFiles;
    
    public PostfixLogManagerWSImpl(List<String> logFiles)
    {
        Check.notNull(logFiles, "logFiles");
        
        if (logFiles.size() == 0) {
            throw new IllegalArgumentException("logFiles should at least contain one file.");
        }
        
        this.logFiles = new ArrayList<String>(logFiles);
        
        logger.info("Monitoring postfix log files: " + logFiles);
    }
        
    @Override
    public int getLogLineCount(String searchPattern)
    throws WebServiceCheckedException
    {
        int count = 0;
        
        try {
            Reader reader = getReader();
            
            if (reader != null)
            {
                try {
                    Pattern pattern = null;
                    
                    if (searchPattern != null && searchPattern.length() > 0) {
                        pattern = Pattern.compile(searchPattern);
                    }
                    
                    PostfixLogParser parser = new PostfixLogParser(pattern);
                    
                    count = parser.getLogCount(reader);
                }
                finally {
                    IOUtils.closeQuietly(reader);
                }
            }
        }
        catch(IOException e) {
            logger.error("Log file does not exist or could not be read", e);
        }
        catch(RuntimeException e) 
        {
            logger.error("getLogLineCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        
        return count;
    }
    
    @Override
    public List<PostfixLogItem> getLogLines(int startIndex, int nrOfItems, String searchPattern)
    throws WebServiceCheckedException    
    {
        List<PostfixLogItem> lines = null;
        
        try {
            /*
             * Sanity check
             */
            if (nrOfItems > MAX_NUMBER_OF_LOG_LINES) 
            {
                throw new WebServiceCheckedException("nrOfItems exceeds the maximum of " + 
                        MAX_NUMBER_OF_LOG_LINES);
            }
            
            Reader reader = getReader();
            
            if (reader != null)
            {
                try {
                    PostfixLogParser parser = new PostfixLogParser(searchPattern != null ? 
                            Pattern.compile(searchPattern) : null);
                    
                    lines = parser.getLogItems(reader, startIndex, nrOfItems);
                }
                finally {
                    IOUtils.closeQuietly(reader);
                }
            }
        }
        catch(IOException e) {
            logger.error("Log file does not exist or could not be read", e);
        }
        catch(RuntimeException e) 
        {
            logger.error("getLogLines failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        
        if (lines == null) {
            lines = Collections.emptyList();
        }
        
        return lines;
    }
    
    @Override
    public int getRawLogLineCount(String searchPattern)
    throws WebServiceCheckedException
    {
        int count = 0;
        
        try {
            Reader reader = getReader();
            
            if (reader != null)
            {
                try {
                    Pattern pattern = null;
                    
                    if (searchPattern != null && searchPattern.length() > 0) {
                        pattern = Pattern.compile(searchPattern);
                    }
                    
                    PostfixLogParser parser = new PostfixLogParser(pattern);
                    
                    count = parser.getRawLogCount(reader);
                }
                finally {
                    IOUtils.closeQuietly(reader);
                }
            }
        }
        catch(IOException e) {
            logger.error("Log file does not exist or could not be read", e);
        }
        catch(RuntimeException e) 
        {
            logger.error("getLogLineCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        
        return count;
    }
    
    @Override
    public List<String> getRawLogLines(int startIndex, int nrOfItems, String searchPattern)
    throws WebServiceCheckedException
    {
        List<String> lines = null;
        
        try {
            /*
             * Sanity check
             */
            if (nrOfItems > MAX_NUMBER_OF_LOG_LINES) 
            {
                throw new WebServiceCheckedException("nrOfItems exceeds the maximum of " + 
                        MAX_NUMBER_OF_LOG_LINES);
            }
            
            Reader reader = getReader();
            
            if (reader != null)
            {
                try {
                    PostfixLogParser parser = new PostfixLogParser(searchPattern != null ? 
                            Pattern.compile(searchPattern) : null);
                    
                    lines = parser.getRawLogItems(reader, startIndex, nrOfItems);
                }
                finally {
                    IOUtils.closeQuietly(reader);
                }
            }
        }
        catch(IOException e) {
            logger.error("Log file does not exist or could not be read", e);
        }
        catch(RuntimeException e) 
        {
            logger.error("getRawLogLines failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        
        if (lines == null) {
            lines = Collections.emptyList();
        }
        
        return lines;
    }
    
    private MetaReader getReader() 
    throws FileNotFoundException
    {
        Collection<Reader> readers = new ArrayList<Reader>(logFiles.size());
        
        for (String logFile : logFiles) 
        {
            File file = new File(logFile);
            
            if (file.exists() && file.canRead()) 
            {
                readers.add(new FileReader(file));
            }
        }
                
        MetaReader reader = null;
        
        if (readers.size() > 0) {
           reader = new MetaReader(readers);
        }
        
        return reader;
    }
}
