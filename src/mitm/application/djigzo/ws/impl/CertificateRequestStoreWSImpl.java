/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.CertificateRequestDTO;
import mitm.application.djigzo.ws.CertificateRequestStoreWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.ca.CertificateRequestStore;
import mitm.common.security.ca.Match;
import mitm.common.security.certificate.X500PrincipalInspector;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CertificateRequestStoreWSImpl implements CertificateRequestStoreWS 
{
    private final static Logger logger = LoggerFactory.getLogger(CertificateRequestStoreWSImpl.class);
    
    private final CertificateRequestStore store;
    
    public CertificateRequestStoreWSImpl(CertificateRequestStore store)
    {
        Check.notNull(store, "store");
        
        this.store = store;
    }
     
    private CertificateRequestDTO toCertificateRequestDTO(CertificateRequest request)
    {
        return request == null ? null : new CertificateRequestDTO(
                request.getID(),
                request.getCreated(),
                X500PrincipalInspector.getFriendly(request.getSubject()),
                request.getEmail(),
                request.getValidity(),
                request.getSignatureAlgorithm(),
                request.getKeyLength(),
                request.getCRLDistributionPoint(),
                request.getCertificateHandlerName(),
                request.getInfo(),
                request.getIteration(),
                request.getLastUpdated(),
                request.getNextUpdate(),
                request.getLastMessage());
    }
    
    @Override
    @StartTransaction
    public CertificateRequestDTO getRequest(long id)
    throws WebServiceCheckedException
    {
        try {
            return toCertificateRequestDTO(store.getRequest(id));
        }
        catch (Exception e)
        {
            logger.error("getRequest failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }

    @Override
    @StartTransaction
    public void deleteRequest(long id)
    throws WebServiceCheckedException
    {
        try {
            CertificateRequest request = store.getRequest(id);
            
            if (request != null)
            {
                store.deleteRequest(id);

                logger.info("Certificate request for email " + StringUtils.defaultString(request.getEmail()) + 
                        " was removed.");
            }
            else {
                logger.warn("Request with id " + id + " not found.");
            }
        }
        catch (Exception e)
        {
            logger.error("deleteRequest failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public List<CertificateRequestDTO> getRequestsByEmail(String email, Match match, Integer firstResult, 
            Integer maxResults)
    throws WebServiceCheckedException
    {
        try {
            List<CertificateRequestDTO> result = new LinkedList<CertificateRequestDTO>();
            
            List<? extends CertificateRequest> requests = store.getRequestsByEmail(email, match, 
                    firstResult, maxResults);
            
            if (requests != null)
            {
                for (CertificateRequest request : requests) {
                    result.add(toCertificateRequestDTO(request));
                }
            }
            
            return result;
        }
        catch (Exception e)
        {
            logger.error("getRequestsByEmail failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }

    @Override
    @StartTransaction
    public List<CertificateRequestDTO> getAllRequests(Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        try {
            List<CertificateRequestDTO> result = new LinkedList<CertificateRequestDTO>();
            
            List<? extends CertificateRequest> requests = store.getAllRequests(firstResult, maxResults);
            
            if (requests != null)
            {
                for (CertificateRequest request : requests) {
                    result.add(toCertificateRequestDTO(request));
                }
            }
            
            return result;
        }
        catch (Exception e)
        {
            logger.error("getAllRequests failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public int getSize()
    throws WebServiceCheckedException
    {
        try {
            return store.getSize();
        }
        catch (Exception e)
        {
            logger.error("getSize failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public int getSizeByEmail(String email, Match match)
    throws WebServiceCheckedException
    {
        try {
            return store.getSizeByEmail(email, match);
        }
        catch (Exception e)
        {
            logger.error("getSizeByEmail failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    @Override
    @StartTransaction
    public void rescheduleRequest(long id)
    throws WebServiceCheckedException
    {
        try {
            CertificateRequest request = store.getRequest(id);
            
            if (request != null)
            {
                request.setNextUpdate(new Date());

                logger.info("Certificate request for email " + StringUtils.defaultString(request.getEmail()) + 
                        " was rescheduled.");
            }
            else {
                logger.warn("Request with id " + id + " not found.");
            }
        }
        catch (Exception e)
        {
            logger.error("rescheduleRequest failed.", e);

            throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
        }
    }
}
