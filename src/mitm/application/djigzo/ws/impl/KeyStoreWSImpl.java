/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.ws.KeyStoreWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.security.keystore.KeyStoreProvider;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeyStoreWSImpl implements KeyStoreWS
{
    private final Logger logger = LoggerFactory.getLogger(KeyStoreWSImpl.class);
    
    private final KeyStore keyStore;
    
    public KeyStoreWSImpl(KeyStoreProvider keyStoreService)
    throws KeyStoreException
    {
    	Check.notNull(keyStoreService, "keyStoreService");
    	Check.notNull(keyStoreService.getKeyStore(), "keyStoreService.getKeyStore()");
        
        this.keyStore = keyStoreService.getKeyStore();
    }
    
    @Override
    public List<String> aliases()
    throws WebServiceCheckedException
    {
        try {
            return Collections.list(keyStore.aliases());
        }
        catch(KeyStoreException e)
        {
            logger.error("aliases failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("aliases failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public boolean containsAlias(String alias)
    throws WebServiceCheckedException
    {
        try {
            return keyStore.containsAlias(alias);
        }
        catch(KeyStoreException e)
        {
            logger.error("containsAlias failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("containsAlias failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public void deleteEntry(String alias)
    throws WebServiceCheckedException
    {
        try {
            keyStore.deleteEntry(alias);
        }
        catch(KeyStoreException e)
        {
            logger.error("deleteEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    public boolean isKeyEntry(String alias)
    throws WebServiceCheckedException
    {
        try {
            return keyStore.isKeyEntry(alias);
        }
        catch(KeyStoreException e)
        {
            logger.error("isKeyEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isKeyEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public boolean isCertificateEntry(String alias)
    throws WebServiceCheckedException
    {
        try {
            return keyStore.isCertificateEntry(alias);
        }
        catch(KeyStoreException e)
        {
            logger.error("isCertificateEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("isCertificateEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    public int size()
    throws WebServiceCheckedException
    {
        try {
            return keyStore.size();
        }
        catch(KeyStoreException e)
        {
            logger.error("size failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("size failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
}
