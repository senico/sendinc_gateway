/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.application.djigzo.ws.CTLEntryDTO;
import mitm.application.djigzo.ws.CTLWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.ctl.CTL;
import mitm.common.security.ctl.CTLEntry;
import mitm.common.security.ctl.CTLException;
import mitm.common.security.ctl.CTLManager;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

public class CTLWSImpl implements CTLWS
{
    private final static Logger logger = LoggerFactory.getLogger(CTLWSImpl.class);
    
    private final CTLManager ctlManager;
 
    public CTLWSImpl(CTLManager ctlManager)
    {
        Check.notNull(ctlManager, "ctlManager");
        
        this.ctlManager = ctlManager;
    }
    
    @Override
    @StartTransaction
    public List<CTLEntryDTO> getEntries(String ctlName, Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        try {
            return getEntriesInternal(ctlName, firstResult, maxResults);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getEntries failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getEntries failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public List<CTLEntryDTO> getEntriesInternal(String ctlName, Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            List<? extends CTLEntry> entries = ctl.getEntries(firstResult, maxResults);
            
            List<CTLEntryDTO> result = new ArrayList<CTLEntryDTO>(entries.size());
            
            for (CTLEntry entry : entries) {
                result.add(toDTO(entry));
            }
            
            return result;
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public int getSize(String ctlName)
    throws WebServiceCheckedException
    {
        try {
            return getSizeInternal(ctlName);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getSize failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getSize failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public int getSizeInternal(String ctlName)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            return ctl.size();
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public void deleteAll(String ctlName)
    throws WebServiceCheckedException
    {
        try {
            deleteAllInternal(ctlName);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteAll failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteAll failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public void deleteAllInternal(String ctlName)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            ctl.deleteAll();
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public CTLEntryDTO getEntry(String ctlName, String thumbprint)
    throws WebServiceCheckedException
    {
        try {
            if (StringUtils.isEmpty(thumbprint)) {
                throw new WebServiceCheckedException("Thumbprint is missing.");
            }
            
            return getEntryInternal(ctlName, thumbprint);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public CTLEntryDTO getEntryInternal(String ctlName, String thumbprint)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            return toDTO(ctl.getEntry(thumbprint));
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public void addEntry(String ctlName, CTLEntryDTO entry)
    throws WebServiceCheckedException
    {
        try {
            if (entry == null) {
                throw new WebServiceCheckedException("entry is missing.");
            }
            
            if (StringUtils.isEmpty(entry.getThumbprint())) {
                throw new WebServiceCheckedException("Thumbprint is missing.");
            }
            
            addEntryInternal(ctlName, entry);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("addEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("addEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public void addEntryInternal(String ctlName, CTLEntryDTO dtoEntry)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            CTLEntry entry = fromDTO(ctl, dtoEntry);
            
            ctl.addEntry(entry);
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    @Override
    @StartTransaction
    public void deleteEntry(String ctlName, String thumbprint)
    throws WebServiceCheckedException
    {
        try {
            if (StringUtils.isEmpty(thumbprint)) {
                throw new WebServiceCheckedException("Thumbprint is missing.");
            }
            
            deleteEntryInternal(ctlName, thumbprint);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("deleteEntry failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("deleteEntry failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    public void deleteEntryInternal(String ctlName, String thumbprint)
    throws WebServiceCheckedException
    {
        CTL ctl = getCTL(ctlName);
        
        try {
            CTLEntry entry = ctl.getEntry(thumbprint);
            
            if (entry != null) {
                ctl.deleteEntry(entry);
            }
        }
        catch (CTLException e) {
            throw new WebServiceCheckedException(e);
        }
    }
    
    private CTL getCTL(String ctlName)
    throws WebServiceCheckedException
    {
        if (StringUtils.isEmpty(ctlName)) {
            throw new WebServiceCheckedException("ctlName is missing.");
            
        }
        return ctlManager.getCTL(ctlName);
    }

    private CTLEntryDTO toDTO(CTLEntry entry)
    {
        if (entry == null) {
            return null;
        }
        
        CTLEntryDTO dto = new CTLEntryDTO(entry.getThumbprint(), entry.getStatus());
        
        dto.setAllowExpired(entry.isAllowExpired());
        
        return dto;
    }
    
    private CTLEntry fromDTO(CTL ctl, CTLEntryDTO dtoEntry)
    throws CTLException
    {
        if (dtoEntry == null) {
            return null;
        }
     
        CTLEntry entry = ctl.createEntry(dtoEntry.getThumbprint());
        
        entry.setStatus(dtoEntry.getStatus());
        entry.setAllowExpired(dtoEntry.isAllowExpired());
        
        return entry;
    }
}
