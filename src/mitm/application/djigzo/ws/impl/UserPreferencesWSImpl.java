/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.application.djigzo.impl.NamedCertificateUtils;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.application.djigzo.ws.X509CertificateDTOBuilder;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserPreferencesWSImpl implements UserPreferencesWS
{
    private final static Logger logger = LoggerFactory.getLogger(UserPreferencesWSImpl.class);
    
    /*
     * For getting the User preferences for a specific category
     */
    private final UserPreferencesCategoryManager categoryManager;
    
    /*
     * The store with the Keys and Certificates
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * For getting the user
     */
    private final UserWorkflow userWorkflow;
    
    /*
     * For building X509 Certificate DTOs
     */
    private final X509CertificateDTOBuilder certificateDTOBuilder;
    
    public UserPreferencesWSImpl(UserPreferencesCategoryManager categoryManager, KeyAndCertStore keyAndCertStore, 
    		UserWorkflow userWorkflow, X509CertificateDTOBuilder certificateDTOBuilder) 
    {
    	Check.notNull(categoryManager, "categoryManager");
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(userWorkflow, "userWorkflow");
    	Check.notNull(certificateDTOBuilder, "certificateDTOBuilder");
        
        this.categoryManager = categoryManager;
        this.keyAndCertStore = keyAndCertStore;
        this.userWorkflow = userWorkflow;
        this.certificateDTOBuilder = certificateDTOBuilder;
    }
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getCertificates(UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getCertificatesAction(userPreferences);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public void setCertificates(UserPreferencesDTO userPreferences, List<String> thumbprints)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            setCertificatesAction(userPreferences, thumbprints);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("setCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }    
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getInheritedCertificates(UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getInheritedCertificatesAction(userPreferences);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getInheritedCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getInheritedCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public List<X509CertificateDTO> getNamedCertificates(UserPreferencesDTO userPreferences, String name)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            checkNameParameter(name);
            
            return getNamedCertificatesAction(userPreferences, name);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getNamedCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getNamedCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public void setNamedCertificates(UserPreferencesDTO userPreferences, String name, List<String> thumbprints)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            checkNameParameter(name);
            
            setNamedCertificatesAction(userPreferences, name, thumbprints);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("setNamedCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setNamedCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }    
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getInheritedNamedCertificates(UserPreferencesDTO userPreferences, String name)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            checkNameParameter(name);
            
            return getInheritedNamedCertificatesAction(userPreferences, name);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getInheritedNamedCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getInheritedNamedCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public X509CertificateDTO getKeyAndCertificate(UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getKeyAndCertificateAction(userPreferences);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getKeyAndCertificate failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getKeyAndCertificate failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    @Override
    @StartTransaction
    public void setKeyAndCertificate(UserPreferencesDTO userPreferences, String thumbprint)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            setKeyAndCertificateAction(userPreferences, thumbprint);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("setKeyAndCertificate failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setKeyAndCertificate failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getInheritedKeyAndCertificates(UserPreferencesDTO userPreferences)
    throws WebServiceCheckedException
    {
        try {
            checkUserPreferencesParameter(userPreferences);
            
            return getInheritedKeyAndCertificateAction(userPreferences);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getInheritedKeyAndCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getInheritedKeyAndCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }

    /*
     * With X509Certificates returned from user we do not know what the key alias is because
     * it's not a CertStore object. We therefore will lookup the key alias.
     */
    private String getKeyAlias(X509Certificate certificate)
    throws WebServiceCheckedException
    {
        try {
            if (certificate == null) {
                return null;
            }
            
            X509CertStoreEntry entry = keyAndCertStore.getByCertificate(certificate);
            
            return entry != null ? entry.getKeyAlias() : null;
        }
        catch (CertStoreException e)
        {
            throw new WebServiceCheckedException(e);
        }
    }
    
    private List<X509CertificateDTO> getCertificatesAction(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> certificatesDTO = new LinkedList<X509CertificateDTO>(); 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<X509Certificate> certificates = usePreferences.getCertificates();
        
        for (X509Certificate certificate : certificates)
        {
            certificatesDTO.add(certificateDTOBuilder.buildCertificateDTO(certificate, getKeyAlias(certificate)));
        }
        
        return certificatesDTO;
    }    

    private void setCertificatesAction(UserPreferencesDTO userPreferencesDTO, Collection<String> thumbprints)
    throws WebServiceCheckedException
    {
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        try {
            if (thumbprints != null)
            {
                for (String thumbprint : thumbprints)
                {
                    if (thumbprint == null) {
                        throw new WebServiceCheckedException("Thumbprint is not specified.");
                    }
                    
                    X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(thumbprint);
                    
                    if (certStoreEntry == null) {
                        throw new WebServiceCheckedException("Certificate entry with thumbprint '" + thumbprint + "' not found.");                    
                    }
                    
                    certificates.add(certStoreEntry.getCertificate());
                }
            }
            
            /*
             * Replace the certificates with the new certificates
             */
            usePreferences.getCertificates().clear();
            usePreferences.getCertificates().addAll(certificates);
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }    
    
    private List<X509CertificateDTO> getInheritedCertificatesAction(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> certificatesDTO = new LinkedList<X509CertificateDTO>(); 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<X509Certificate> certificates = usePreferences.getInheritedCertificates();
        
        for (X509Certificate certificate : certificates)
        {
        	certificatesDTO.add(certificateDTOBuilder.buildCertificateDTO(certificate, getKeyAlias(certificate)));
        }

        return certificatesDTO;
    }    

    private List<X509CertificateDTO> getNamedCertificatesAction(UserPreferencesDTO userPreferencesDTO, String name)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> certificates = new LinkedList<X509CertificateDTO>(); 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<NamedCertificate> allNamedCertificates = usePreferences.getNamedCertificates();
        
        Set<X509Certificate> namedCertificates = NamedCertificateUtils.getByName(name, allNamedCertificates);
        
        for (X509Certificate certificate : namedCertificates) {
            certificates.add(certificateDTOBuilder.buildCertificateDTO(certificate, getKeyAlias(certificate)));
        }
        
        return certificates;
    }    
    
    private void setNamedCertificatesAction(UserPreferencesDTO userPreferencesDTO, String name, 
            Collection<String> thumbprints)
    throws WebServiceCheckedException
    {
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        try {
            if (thumbprints != null)
            {
                for (String thumbprint : thumbprints)
                {
                    if (StringUtils.isEmpty(thumbprint)) {
                        throw new WebServiceCheckedException("thumbprint is missing.");
                    }
                    
                    X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(thumbprint);
                    
                    if (certStoreEntry == null) {
                        throw new WebServiceCheckedException("Certificate entry with thumbprint '" + thumbprint + "' not found.");                    
                    }
                    
                    certificates.add(certStoreEntry.getCertificate());
                }
            }
            
            /*
             * Replace the certificates with the new certificates
             */
            NamedCertificateUtils.replaceNamedCertificates(usePreferences.getNamedCertificates(), name, certificates);             
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }    
    
    private List<X509CertificateDTO> getInheritedNamedCertificatesAction(UserPreferencesDTO userPreferencesDTO, String name)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> certificates = new LinkedList<X509CertificateDTO>(); 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        Set<NamedCertificate> allNamedCertificates = usePreferences.getInheritedNamedCertificates();
        
        Set<X509Certificate> namedCertificates = NamedCertificateUtils.getByName(name, allNamedCertificates);
        
        for (X509Certificate certificate : namedCertificates) {
            certificates.add(certificateDTOBuilder.buildCertificateDTO(certificate, getKeyAlias(certificate)));
        }
        
        return certificates;
    }    
    
    private X509CertificateDTO getKeyAndCertificateAction(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
    	X509CertificateDTO keyAndCertificateDTO = null; 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);

        try {
            KeyAndCertificate keyAndCertificate = usePreferences.getKeyAndCertificate();

            if (keyAndCertificate != null && keyAndCertificate.getCertificate() != null)
            {
                String keyAlias = getKeyAlias(keyAndCertificate.getCertificate());
                
                keyAndCertificateDTO = certificateDTOBuilder.buildKeyAndCertificateDTO(keyAndCertificate, keyAlias);
            }
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        
        return keyAndCertificateDTO;
    }    

    private void setKeyAndCertificateAction(UserPreferencesDTO userPreferencesDTO, String thumbprint)
    throws WebServiceCheckedException
    {
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);

        try {
        	KeyAndCertificate keyAndCertificate = null;
        	
        	if (thumbprint != null)
        	{
	            X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(thumbprint);
	            
	            if (certStoreEntry == null) {
	                throw new WebServiceCheckedException("Certificate entry with thumbprint '" + thumbprint + "' not found.");
	            }
	            
	            keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);
        	}
            
            usePreferences.setKeyAndCertificate(keyAndCertificate);
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }    
    
    private List<X509CertificateDTO> getInheritedKeyAndCertificateAction(UserPreferencesDTO userPreferencesDTO)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> keyAndCertificatesDTO = new LinkedList<X509CertificateDTO>(); 
        
        UserPreferences usePreferences = getUserPreferences(userPreferencesDTO);
        
        try {
            Set<KeyAndCertificate> keyAndCertificates = usePreferences.getInheritedKeyAndCertificates();
            
            for (KeyAndCertificate keyAndCertificate : keyAndCertificates)
            {
                if (keyAndCertificate != null && keyAndCertificate.getCertificate() != null)
                {
                    String keyAlias = getKeyAlias(keyAndCertificate.getCertificate());
                    
                    keyAndCertificatesDTO.add(certificateDTOBuilder.buildKeyAndCertificateDTO(keyAndCertificate, 
                    		keyAlias));
                }
            }
            
            return keyAndCertificatesDTO;
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        }
    }    
    
    private UserPreferences getUserPreferences(UserPreferencesDTO userPreferencesDTO) 
    throws WebServiceCheckedException
    {
        UserPreferences userPreferences = null;

        /*
         * We will treat a user category differently. A user should not always exists yet the user does have
         * properties because any user can inherit from a domain or global settings. The UserPreferencesManager
         * only 'works' on existing UserPreferences. We therefore need to treat the user differently and use
         * userWorkflow to get the user.
         *  
         * Note: It would perhaps be nicer if we refactor the web services a bit so we don't need to this 'exception'.
         * Perhaps create another version of UserPreferencesWS specialized for only Users?
         */
        if (UserPreferencesCategory.fromName(userPreferencesDTO.getCategory()) == UserPreferencesCategory.USER)
        {
        	try {
        		userPreferences = userWorkflow.getUser(userPreferencesDTO.getName(), 
        				UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).getUserPreferences();				
			} 
        	catch (AddressException e) {
        		throw new WebServiceCheckedException(e);
			} 
        	catch (HierarchicalPropertiesException e) {
        		throw new WebServiceCheckedException(e);
			}
        }
        else 
        {
	        UserPreferencesManager preferencesManager = categoryManager.getUserPreferencesManager(
	                userPreferencesDTO.getCategory());
	        
	        if (preferencesManager != null) {
	            userPreferences = preferencesManager.getUserPreferences(userPreferencesDTO.getName());
	        }
        }

        if (userPreferences == null) {
            throw new WebServiceCheckedException("userPreferences " + userPreferencesDTO + " not found.");
        }
        
        return userPreferences;
    }
    
    private void checkUserPreferencesParameter(UserPreferencesDTO userPreferences) 
    throws WebServiceCheckedException
    {
        if (userPreferences == null || userPreferences.getCategory() == null || userPreferences.getName() == null) {
            throw new WebServiceCheckedException("user is null or category or name is not specified.");
        }
    }    

    private void checkNameParameter(String name) 
    throws WebServiceCheckedException
    {
        if (StringUtils.isEmpty(name)) {
            throw new WebServiceCheckedException("name is not set.");
        }
    }    
}
