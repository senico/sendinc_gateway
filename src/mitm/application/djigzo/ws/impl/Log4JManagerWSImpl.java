/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.LoggerDTO;
import mitm.application.djigzo.ws.LoggerManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.util.Check;
import mitm.common.util.LogUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This implementation of LogManagerWS is specifically for log4j.
 * 
 * @author Martijn Brinkers
 *
 */
public class Log4JManagerWSImpl implements LoggerManagerWS 
{
	private final static Logger logger = LoggerFactory.getLogger(Log4JManagerWSImpl.class);
		
	@Override
    public void setLogLevel(LoggerDTO logDTO)
	throws WebServiceCheckedException 
	{
		try {
			Check.notNull(logDTO, "logDTO");		
			Check.notNull(logDTO.getName(), "name");		
			Check.notNull(logDTO.getLogLevel(), "logLevel");
			
			String name = logDTO.getName();
			
			org.apache.log4j.Logger log4jLogger = LogManager.exists(name);
			
			if (log4jLogger == null) {
	            throw new WebServiceCheckedException("Logger with name " + name + " does not exist.");
			}
			
			log4jLogger.setLevel(LogUtils.toLog4JLevel(logDTO.getLogLevel()));
			
			logger.info("Loglevel for " + name + " set to " + logDTO.getLogLevel());
		}
        catch(RuntimeException e) 
        {
            logger.error("setLogLevel failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	@Override
    public int getLoggersCount() 
	throws WebServiceCheckedException
	{
		try {
			return getLoggers(0, Integer.MAX_VALUE).size();
		}
        catch(RuntimeException e) 
        {
            logger.error("getLoggersCount failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	@Override
    public List<LoggerDTO> getLoggers(int startIndex, int maxItems)
    throws WebServiceCheckedException
    {
		try {
			List<LoggerDTO> allLoggers = new LinkedList<LoggerDTO>();
			
			Enumeration<?> loggerEnum = LogManager.getCurrentLoggers();
			
			while (loggerEnum.hasMoreElements())
			{
				Object obj = loggerEnum.nextElement();
				
				if (obj instanceof org.apache.log4j.Logger) 
				{
					org.apache.log4j.Logger logger = (org.apache.log4j.Logger) obj;
					
					if (logger == null || logger.getName() == null || logger.getName().length() == 0) {
						continue;
					}
					
					LoggerDTO logDTO = new LoggerDTO(logger.getName(), LogUtils.toLogLevel(logger.getEffectiveLevel()));
					
					allLoggers.add(logDTO);
				}
			}
			
			Collections.sort(allLoggers);

			List<LoggerDTO> result = new LinkedList<LoggerDTO>();
			
			int endIndex = startIndex + maxItems;
			
			int i = 0;
			
			for (LoggerDTO logger : allLoggers)
			{
				if (i >= startIndex) {
					result.add(logger);
				}

				i++;
				
				if (i >= endIndex) {
					break;
				}
			}
			
			return result;
		}
        catch(RuntimeException e) 
        {
            logger.error("getLoggers failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
}
