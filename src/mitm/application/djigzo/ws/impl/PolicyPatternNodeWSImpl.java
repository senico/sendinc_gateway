/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.dlp.PolicyPatternManager;
import mitm.application.djigzo.dlp.PolicyPatternNode;
import mitm.application.djigzo.ws.PolicyPatternDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

/**
 * Implementation of PolicyPatternNodeWS.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternNodeWSImpl implements PolicyPatternNodeWS
{
    private final PolicyPatternManager policyPatternManager; 

    public PolicyPatternNodeWSImpl(PolicyPatternManager policyPatternManager)
    {
        Check.notNull(policyPatternManager, "policyPatternManager");
        
        this.policyPatternManager = policyPatternManager;
    }
    
    private PolicyPatternNode getPolicyPatternNode(String name)
    throws WebServiceCheckedException
    {
        PolicyPatternNode node = policyPatternManager.getPattern(name);
        
        if (node == null) {
            throw new WebServiceCheckedException("PolicyPatternNode with name " + name + " not found.");
        }
        
        return node;
    }
    
    @Override
    @StartTransaction
    public void addChild(String parentName, String childName)
    throws WebServiceCheckedException
    {
        PolicyPatternNode parentNode = getPolicyPatternNode(parentName);
        PolicyPatternNode childNode = getPolicyPatternNode(childName);
        
        parentNode.getChilds().add(childNode);
    }

    @Override
    @StartTransaction
    public void removeChild(String parentName, String childName)
    throws WebServiceCheckedException
    {
        PolicyPatternNode parentNode = getPolicyPatternNode(parentName);
        PolicyPatternNode childNode = getPolicyPatternNode(childName);
        
        parentNode.getChilds().remove(childNode);
    }

    @Override
    @StartTransaction
    public void setChilds(String parentName, List<String> childs)
    throws WebServiceCheckedException
    {
        PolicyPatternNode parentNode = getPolicyPatternNode(parentName);

        Set<PolicyPatternNode> childNodes = parentNode.getChilds();
        
        childNodes.clear();
        
        if (childs != null)
        {
            for (String childName : childs) {
                childNodes.add(getPolicyPatternNode(childName));
            }
        }
    }
    
    @Override
    @StartTransaction
    public List<PolicyPatternNodeDTO> getChilds(String name)
    throws WebServiceCheckedException
    {
        Set<PolicyPatternNode> childs = getPolicyPatternNode(name).getChilds();
        
        List<PolicyPatternNodeDTO> dtos = new ArrayList<PolicyPatternNodeDTO>(childs.size());
        
        for (PolicyPatternNode child : childs) {
            dtos.add(PolicyPatternNodeDTO.toDTO(child));
        }
        
        return dtos;
    }

    @Override
    @StartTransaction
    public PolicyPatternDTO getPattern(String name)
    throws WebServiceCheckedException
    {
        return PolicyPatternDTO.toDTO(getPolicyPatternNode(name).getPolicyPattern());
    }

    @Override
    @StartTransaction
    public void setPattern(String name, PolicyPatternDTO pattern)
    throws WebServiceCheckedException
    {
        getPolicyPatternNode(name).setPolicyPattern(PolicyPatternDTO.fromDTO(pattern));
    }
}
