/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import mitm.application.djigzo.service.ServiceRestarter;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.security.JCEPolicyManager;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemManagerWSImpl implements SystemManagerWS
{
	private final static Logger logger = LoggerFactory.getLogger(SystemManagerWSImpl.class);
	
	/*
	 * The script that copies the JCE unlimited strength policy files to the correct Java JRE directory
	 */
	private final File copyJCEPolicyScript;

    /*
     * The restart script (script that will restart all the services)
     */
    private final File restartScript;
	
	public SystemManagerWSImpl(File copyJCEPolicyScript, File restartScript)
	{
        Check.notNull(copyJCEPolicyScript, "copyJCEPolicyScript");
        Check.notNull(restartScript, "restartScript");
		
		if (!copyJCEPolicyScript.exists()) {
			throw new IllegalArgumentException(copyJCEPolicyScript + " does not exist.");
		}

        if (!restartScript.exists()) {
            throw new IllegalArgumentException(restartScript + " does not exist.");
        }
		
		this.copyJCEPolicyScript = copyJCEPolicyScript;
		this.restartScript = restartScript;
	}
	
	@Override
    public boolean isUnlimitedStrength() 
	throws WebServiceCheckedException
    {
    	try {
    		return JCEPolicyManager.isUnlimitedStrength();
    	} 
    	catch(RuntimeException e) 
    	{
    		logger.error("Error executing isUnlimitedStrength.", e);

    		throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
    	}        
    }
	
    @Override
    public void installJCEPolicy(String encodedPolicy)
	throws WebServiceCheckedException
    {
    	try {
    		byte[] decodedPolicy = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(encodedPolicy));

    		new JCEPolicyManager(copyJCEPolicyScript).installJCEPolicy(new ByteArrayInputStream(decodedPolicy));
    	} 
    	catch (IOException e)
    	{
    		logger.error("Error executing installJCEPolicy.", e);

    		throw new WebServiceCheckedException(ExceptionUtils.getRootCauseMessage(e));
		}        
        catch(RuntimeException e) 
        {
            logger.error("Error executing installJCEPolicy.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    @Override
    public void restart()
    throws WebServiceCheckedException
    {
        try {
            new ServiceRestarter(restartScript).restart();
        } 
        catch (IOException e)
        {
            logger.error("Error executing restart.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("Error executing restart.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    @Override
    public boolean isRunning()
    throws WebServiceCheckedException
    {
        /*
         * Always return true. If back-end is not running calling the webservice will fail.
         */
        return true;
    }
}
