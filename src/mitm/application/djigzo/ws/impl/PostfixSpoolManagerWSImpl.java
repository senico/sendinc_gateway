/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.postfix.Postcat;
import mitm.common.postfix.PostfixException;
import mitm.common.postfix.PostfixQueueItem;
import mitm.common.postfix.PostfixQueueParser;
import mitm.common.postfix.Postqueue;
import mitm.common.postfix.Postsuper;
import mitm.common.util.Check;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostfixSpoolManagerWSImpl implements PostfixSpoolManagerWS
{
	private static final Logger logger = LoggerFactory.getLogger(PostfixSpoolManagerWSImpl.class);

	/*
	 * The script that is used to execute postfix commands
	 */
	private final File postfixScript;
	
	public PostfixSpoolManagerWSImpl(File postfixScript)
	{
	    Check.notNull(postfixScript, "postfixScript");
	    
	    this.postfixScript = postfixScript;
	}
	
	@Override
    public void flush()
	throws WebServiceCheckedException
	{
		try {
			getPostqueue().flush();
		} 
		catch (PostfixException e) 
		{
			logger.error("flush failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("flush failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	@Override
    public void deliver(String queueID)
	throws WebServiceCheckedException
	{
		try {
			getPostqueue().deliver(queueID);
		} 
		catch (PostfixException e) 
		{
			logger.error("deliver failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("deliver failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	@Override
    public List<PostfixQueueItem> getQueued(int startIndex, int maxItems, String searchPattern)
	throws WebServiceCheckedException
	{
		try {
			String listing = getPostqueue().list();
			
			return new PostfixQueueParser(searchPattern != null ? Pattern.compile(searchPattern) : null).
			        parseQueue(listing, startIndex, maxItems);
		} 
		catch (PostfixException e) 
		{
			logger.error("getQueued failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("getQueued failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	@Override
    public int getQueueLength(String searchPattern)
	throws WebServiceCheckedException
	{
		try {
			String listing = getPostqueue().list();

			return new PostfixQueueParser(searchPattern != null ? Pattern.compile(searchPattern) : null).
			        getQueueLength(listing);
		} 
		catch (PostfixException e) 
		{
			logger.error("getQueueLength failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("getQueueLength failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public void delete(String queueID)
	throws WebServiceCheckedException
	{
		try {
			getPostsuper().delete(queueID, null);
		} 
		catch (PostfixException e) 
		{
			logger.error("delete failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("delete failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public void hold(String queueID)
	throws WebServiceCheckedException
	{
		try {
			getPostsuper().hold(queueID, null);
		} 
		catch (PostfixException e) 
		{
			logger.error("hold failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("hold failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public void release(String queueID)
	throws WebServiceCheckedException
	{
		try {
			getPostsuper().release(queueID, null);
		} 
		catch (PostfixException e) 
		{
			logger.error("release failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("release failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public void requeue(String queueID)            
	throws WebServiceCheckedException
	{
		try {
			getPostsuper().requeue(queueID, null);
		} 
		catch (PostfixException e) 
		{
			logger.error("requeue failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("requeue failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}

	@Override
    public byte[] getMessage(String queueID, int maxLength)
	throws WebServiceCheckedException
	{
		try {
		    ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    
		    SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, maxLength, false /* bitsink mode */);
		    
		    getPostcat().getMessage(queueID, slos);

			return bos.toByteArray();
		} 
		catch (PostfixException e) 
		{
			logger.error("getMessage failed.", e);
			
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}
        catch(RuntimeException e) 
        {
            logger.error("getMessage failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	}
	
	private Postqueue getPostqueue() {
		return new Postqueue(postfixScript);
	}

	private Postsuper getPostsuper() {
		return new Postsuper(postfixScript);
	}

	private Postcat getPostcat() {
		return new Postcat(postfixScript);
	}
}
