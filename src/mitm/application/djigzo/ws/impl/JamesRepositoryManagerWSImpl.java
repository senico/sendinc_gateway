/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import mitm.application.djigzo.james.JamesRepository;
import mitm.application.djigzo.james.JamesStoreManager;
import mitm.application.djigzo.ws.JamesRepositoryManagerWS;
import mitm.application.djigzo.ws.MailDTO;
import mitm.application.djigzo.ws.MailDTOBuilder;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.mail.MailUtils;
import mitm.common.util.Check;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.avalon.framework.service.ServiceException;
import org.apache.avalon.framework.service.ServiceManager;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.james.services.MailRepository;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JamesRepositoryManagerWSImpl implements JamesRepositoryManagerWS 
{
	private static final Logger logger = LoggerFactory.getLogger(JamesRepositoryManagerWSImpl.class);
	
	/*
	 * Used for managing the James mail stores.
	 */
	private final ServiceManager serviceManager;

	/*
	 * Mapping from repository names to the actual repository URL
	 */
	private final Map<JamesRepository, String> repositoryURLMap;
	
	public JamesRepositoryManagerWSImpl(ServiceManager serviceManager, Map<JamesRepository, String> repositoryURLMap)
	{
		Check.notNull(serviceManager, "serviceManager");
        Check.notNull(repositoryURLMap, "repositoryURLMap");
		
		this.serviceManager = serviceManager;
		
		this.repositoryURLMap = Collections.synchronizedMap(new HashMap<JamesRepository, String>(repositoryURLMap)); 
	}

    @Override
    public int getRepositorySize(JamesRepository repository)
	throws WebServiceCheckedException
	{
		try {
		    MailRepository mailRepository = getMailRepository(repository);
		    
		    return new JamesStoreManager(serviceManager).getRepositorySize(mailRepository);		    
	    }
        catch (ServiceException e)
        {
            logger.error("getRepositorySize failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
	    catch (MessagingException e)
	    {
	        logger.error("getRepositorySize failed.", e);
	    	
	        throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
		}        
	    catch(RuntimeException e) 
	    {
	        logger.error("getRepositorySize failed.", e);
	
	        throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
	    } 
	}
    
    @Override
    public List<String> getMailNames(JamesRepository repository, int startIndex, int maxItems)
    throws WebServiceCheckedException
    {
        try {
            MailRepository mailRepository = getMailRepository(repository);
                        
            return new JamesStoreManager(serviceManager).getMailNames(mailRepository, startIndex, maxItems);
        }
        catch (ServiceException e)
        {
            logger.error("getMailNames failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch (MessagingException e)
        {
            logger.error("getMailNames failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e)
        {
            logger.error("getMailNames failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }

    @Override
    public List<MailDTO> getMails(JamesRepository repository, int startIndex, int maxItems)
    throws WebServiceCheckedException
    {
        try {
            List<String> names = getMailNames(repository, startIndex, maxItems);
            
            List<MailDTO> mails = new LinkedList<MailDTO>();
            
            MailRepository mailRepository = getMailRepository(repository);
            
            for (String name : names)
            {
                try {
                    Mail mail = mailRepository.retrieve(name);
                    
                    if (mail != null) 
                    {
                    	MailDTO mailDTO = new MailDTOBuilder(mail);
                    	
                        mails.add(mailDTO);
                    }
                    else {
                        logger.warn("mail with name " + name + " is null for respository " + repository);
                    }
                }
                catch (MessagingException e) {
                    logger.error("Error retrieving mail with name " + name + "for respository " + repository, e);
                }
            }
            
            return mails;
        }
        catch(RuntimeException e) 
        {
            logger.error("getMails failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }

    @Override
    public void removeMail(JamesRepository repository, String mailName)
    throws WebServiceCheckedException
    {
        try {
            MailRepository mailRepository = getMailRepository(repository);
            
            mailRepository.remove(mailName);
            
            logger.info("mail " + mailName + " from repository " + repository + " removed by user.");
        }
        catch (MessagingException e)
        {
            logger.error("removeMail failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("removeMail failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    @Override
    public void moveMail(JamesRepository sourceRepository, JamesRepository destinationRepository, String mailName,
            String newState)
    throws WebServiceCheckedException
    {
        try {
            MailRepository source = getMailRepository(sourceRepository);
            MailRepository destination = getMailRepository(destinationRepository);
            
            new JamesStoreManager(serviceManager).moveMail(source, destination, mailName, newState);
            
            logger.info("mail " + mailName + " moved from " + sourceRepository + "to " + destinationRepository + 
            		" by user.");
        }
        catch (ServiceException e)
        {
            logger.error("moveMail failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch (MessagingException e)
        {
            logger.error("moveMail failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("moveMail failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    @Override
    public void respoolMail(JamesRepository repository, String mailName, String newState, boolean removeAttributes)
    throws WebServiceCheckedException
    {
        try {
            MailRepository mailRepository = getMailRepository(repository);
            
            new JamesStoreManager(serviceManager).respool(mailRepository, mailName, newState, removeAttributes);
            
            logger.info("mail " + mailName + " from repository " + repository + " respooled by user.");
        }
        catch (ServiceException e)
        {
            logger.error("respoolMail failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch (MessagingException e)
        {
            logger.error("respoolMail failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("respoolMail failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    @Override
    public byte[] getMessage(JamesRepository repository, String mailName, int maxLength)
    throws WebServiceCheckedException
    {
        try {
            MailRepository mailRepository = getMailRepository(repository);
            
            byte[] message = null;
            
            Mail mail = mailRepository.retrieve(mailName);
            
            if (mail != null)
            {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, maxLength, false /* bitsink mode */);
                
                MailUtils.writeMessage(mail.getMessage(), slos);
                
                message = bos.toByteArray();
            }
            
            return message;
        }
        catch (IOException e)
        {
            logger.error("getMessage failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch (MessagingException e)
        {
            logger.error("getMessage failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("getMessage failed.", e);
    
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        } 
    }
    
    private MailRepository getMailRepository(JamesRepository repository) 
    throws WebServiceCheckedException
    {
    	if (repository == null) {
    		throw new WebServiceCheckedException("repository must be specified.");
    	}
    	
        String repositoryURL = repositoryURLMap.get(repository);
        
        if (repositoryURL == null) {
            throw new WebServiceCheckedException("Missing URL for " + repository);
        }
        
        MailRepository mailRepository;
        
        try {
            mailRepository = new JamesStoreManager(serviceManager).getMailRepository(repositoryURL);
        }
        catch (ServiceException e) {
            throw new WebServiceCheckedException(e);
        }
        
        return mailRepository;
    }
}
