/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import mitm.application.djigzo.ws.ComodoTierDetailsDTO;
import mitm.application.djigzo.ws.ComodoWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.ca.handlers.comodo.ComodoConnectionSettings;
import mitm.common.security.ca.handlers.comodo.ComodoSettings;
import mitm.common.security.ca.handlers.comodo.ComodoSettingsProvider;
import mitm.common.security.ca.handlers.comodo.CustomClientCertException;
import mitm.common.security.ca.handlers.comodo.Tier2PartnerDetails;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ComodoWSImpl implements ComodoWS
{
    private final static Logger logger = LoggerFactory.getLogger(ComodoWSImpl.class);
    
    /*
     * Is used for getting the Comodo account settings from the database.
     */
    private final ComodoSettingsProvider settingsProvider;

    /*
     * The HTTP connection settings
     */
    private final ComodoConnectionSettings connectionSettings;
    
    public ComodoWSImpl(ComodoSettingsProvider settingsProvider, ComodoConnectionSettings connectionSettings)
    {
        Check.notNull(settingsProvider, "settingsProvider");
        Check.notNull(connectionSettings, "connectionSettings");
        
        this.settingsProvider = settingsProvider;
        this.connectionSettings = connectionSettings;
    }

    @Override
    @StartTransaction
    public ComodoTierDetailsDTO getTierDetails()
    throws WebServiceCheckedException
    {
        try {
            Tier2PartnerDetails partnerDetails = new Tier2PartnerDetails(connectionSettings);
            
            ComodoSettings settings = settingsProvider.getSettings();
            
            partnerDetails.setLoginName(settings.getLoginName());
            partnerDetails.setLoginPassword(settings.getLoginPassword());
            
            partnerDetails.retrieveDetails();
            
            ComodoTierDetailsDTO dto = new ComodoTierDetailsDTO();
            
            dto.setVerificationLevel(partnerDetails.getVerificationLevel());
            dto.setAccountStatus(partnerDetails.getAccountStatus());
            dto.setResellerStatus(partnerDetails.getResellerStatus());
            dto.setWebHostResellerStatus(partnerDetails.getWebHostResellerStatus());
            dto.setEpkiStatus(partnerDetails.getEpkiStatus());
            dto.setCapLiveCCCs(partnerDetails.getCapLiveCCCs());
            dto.setPeakLiveCCCs(partnerDetails.getPeakLiveCCCs());
            dto.setCurrentLiveCCCs(partnerDetails.getCurrentLiveCCCs());
            dto.setAuthorizedDomains(partnerDetails.getAuthorizedDomains());
            dto.setError(partnerDetails.isError());
            dto.setErrorMessage(partnerDetails.getErrorMessage());
            
            return dto;
        }
        catch (CustomClientCertException e)
        {
            logger.error("getTierDetails failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch (HierarchicalPropertiesException e)
        {
            logger.error("getTierDetails failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
        catch(RuntimeException e) 
        {
            logger.error("getTierDetails failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
    }
}
