/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserWS;
import mitm.application.djigzo.ws.WSExceptionUtils;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.application.djigzo.ws.X509CertificateDTOBuilder;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserWSImpl implements UserWS
{
    private final static Logger logger = LoggerFactory.getLogger(UserWSImpl.class);
    
    /*
     * For getting the user
     */
    private final UserWorkflow userWorkflow;
    
    /*
     * The store with the Keys and Certificates
     */
    private final KeyAndCertStore keyAndCertStore;
    
    /*
     * For building X509 Certificate DTOs
     */
    private final X509CertificateDTOBuilder certificateDTOBuilder;
    
    public UserWSImpl(UserWorkflow userWorkflow, KeyAndCertStore keyAndCertStore, 
    		X509CertificateDTOBuilder certificateDTOBuilder) 
    {
    	Check.notNull(userWorkflow, "userWorkflow");
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(certificateDTOBuilder, "certificateDTOBuilder");
    	        
        this.userWorkflow = userWorkflow;
        this.keyAndCertStore = keyAndCertStore;
        this.certificateDTOBuilder = certificateDTOBuilder;
    }
    
    @Override
    @StartTransaction
    public UserPreferencesDTO getUserPreferences(String email)
    throws WebServiceCheckedException
    {
        try {
            checkEmailParameter(email);
            
            return getUserPreferencesAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getUserPreferences failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getUserPreferences failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public List<X509CertificateDTO> getAutoSelectEncryptionCertificates(String email)
    throws WebServiceCheckedException
    {
        try {
            checkEmailParameter(email);
            
            return getAutoSelectEncryptionCertificatesAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getAutoSelectEncryptionCertificates failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getAutoSelectEncryptionCertificates failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public X509CertificateDTO getSigningKeyAndCertificate(String email)
    throws WebServiceCheckedException
    {
        try {
            checkEmailParameter(email);
            
            return getSigningKeyAndCertificateAction(email);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("getSigningKeyAndCertificate failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("getSigningKeyAndCertificate failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    @Override
    @StartTransaction
    public void setSigningKeyAndCertificate(String email, String thumbprint)
    throws WebServiceCheckedException
    {
        try {
            checkEmailParameter(email);
            
            if (thumbprint == null) {
                throw new WebServiceCheckedException("thumbprint is not specified.");
            }
            
            setSigningKeyAndCertificateAction(email, thumbprint);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("setSigningKeyAndCertificate failed.", e);
            
            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }
        catch(RuntimeException e) 
        {
            logger.error("setSigningKeyAndCertificate failed.", e);

            throw new WebServiceCheckedException(WSExceptionUtils.getExceptionMessage(e));
        }        
    }
    
    private UserPreferencesDTO getUserPreferencesAction(String email)
    throws WebServiceCheckedException
    {
        UserPreferencesDTO userPreferencesDTO = null;
        
        try {
     
            User user = getUser(email);
            
            if (user != null) 
            {
                UserPreferences userPreferences = user.getUserPreferences();
                
                userPreferencesDTO = new UserPreferencesDTO(userPreferences.getCategory(), userPreferences.getName());
            }
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
        
        return userPreferencesDTO;
    }

    /*
     * With X509Certificates returned from user we do not know what the key alias is because
     * it's not a CertStore object. We therefore will lookup the key alias.
     */
    private String getKeyAlias(X509Certificate certificate)
    throws WebServiceCheckedException
    {
        try {
            if (certificate == null) {
                return null;
            }
            
            X509CertStoreEntry entry = keyAndCertStore.getByCertificate(certificate);
            
            return entry != null ? entry.getKeyAlias() : null;
        }
        catch (CertStoreException e)
        {
            throw new WebServiceCheckedException(e);
        }
    }
    
    private List<X509CertificateDTO> getAutoSelectEncryptionCertificatesAction(String email)
    throws WebServiceCheckedException
    {
        List<X509CertificateDTO> autoSelectEncryptionCertificates = new LinkedList<X509CertificateDTO>(); 
        
        try {
     
            User user = getUser(email);
            
            if (user != null) 
            {
                Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
                
                for (X509Certificate certificate : certificates)
                {
                    X509CertificateDTO certificateInfo = certificateDTOBuilder.buildCertificateDTO(certificate, 
                    		getKeyAlias(certificate));
                    
                    autoSelectEncryptionCertificates.add(certificateInfo);
                }
            }
            
            return autoSelectEncryptionCertificates;
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}        
    }

    private X509CertificateDTO getSigningKeyAndCertificateAction(String email)
    throws WebServiceCheckedException
    {
    	X509CertificateDTO keyAndCertificateDTO = null; 
        
        try {
     
            User user = getUser(email);
            
            if (user != null) 
            {
                KeyAndCertificate keyAndCertificate = user.getSigningKeyAndCertificate();

                /*
                 * We cannot return the private key itself because that would be insecure and not always possible 
                 * (like when a HSM is used). Preferably we would return the key alias but that binds this code 
                 * too much to implementation details. 
                 */
                if (keyAndCertificate != null && keyAndCertificate.getPrivateKey() != null)
                {
                    String keyAlias = getKeyAlias(keyAndCertificate.getCertificate());
                    
                    keyAndCertificateDTO = certificateDTOBuilder.buildKeyAndCertificateDTO(keyAndCertificate, keyAlias);
                }
            }
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
        
        return keyAndCertificateDTO;
    }

    private void setSigningKeyAndCertificateAction(String email, String thumbprint)
    throws WebServiceCheckedException
    {
        try {
     
            User user = getUser(email);
            
            if (user != null) 
            {
                X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(thumbprint);
                
                if (certStoreEntry == null) {
                    throw new WebServiceCheckedException("Certificate entry with thumbprint '" + thumbprint + "' not found.");
                }
                
                KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);

                user.setSigningKeyAndCertificate(keyAndCertificate);
            }
            else {
                throw new WebServiceCheckedException("User with email '" + email + " not found.");
            }
        }
        catch (AddressException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (CertStoreException e) {
            throw new WebServiceCheckedException(e);
        }
        catch (KeyStoreException e) {
            throw new WebServiceCheckedException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
		}
    }
        
    private User getUser(String email) 
    throws AddressException, HierarchicalPropertiesException
    {
        String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (filteredEmail == null) {
            throw new AddressException("email address '" + email + "' is invalid.");
        }
        
        User user = userWorkflow.getUser(filteredEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
        
        return user;
    }
    
    private void checkEmailParameter(String email) 
    throws WebServiceCheckedException
    {
        if (email == null) {
            throw new WebServiceCheckedException("email is not specified.");
        }
    }
}
