/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.util.List;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.application.djigzo.ws.AdminWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

public class AdminWSImpl implements AdminWS 
{
	private final AdminManager adminManager;
	private final AuthorityManager authorityManager;

	public AdminWSImpl(AdminManager adminManager, AuthorityManager authorityManager)
	{
		Check.notNull(adminManager, "adminManager");
		Check.notNull(authorityManager, "authorityManager");
		
		this.adminManager = adminManager;
		this.authorityManager = authorityManager;
	}
	
	private Admin getAdmin(String username)
	throws WebServiceCheckedException
	{
		Admin admin = adminManager.getAdmin(username);
		
		if (admin == null) {
			throw new WebServiceCheckedException("Admin with name " + username + " does not exist.");
		}
		
		return admin;
	}
	
	private Authority getAuthority(String role)
	throws WebServiceCheckedException
	{
		Authority authority = authorityManager.getAuthority(role);
		
		if (authority == null) {
			throw new WebServiceCheckedException("Authority with name " + role + " does not exist.");
		}
		
		return authority;
	}
	
	@Override
    @StartTransaction
	public void setEnabled(String username, boolean enabled) 
	throws WebServiceCheckedException
	{
		Admin admin = getAdmin(username);
		
		admin.setEnabled(enabled);
	}

	@Override
    @StartTransaction
	public void setPassword(String username, String password) 
	throws WebServiceCheckedException
	{
		Admin admin = getAdmin(username);
		
		admin.setPassword(password);
	}
	
	@Override
    @StartTransaction
	public void setPasswordEncoding(String username, PasswordEncoding passwordEncoding)
	throws WebServiceCheckedException
	{
		Admin admin = getAdmin(username);
		
		admin.setPasswordEncoding(passwordEncoding);
	}
	
	@Override
    @StartTransaction
	public void setSalt(String username, String salt)
	throws WebServiceCheckedException
	{
		Admin admin = getAdmin(username);
		
		admin.setSalt(salt);
	}
	
	@Override
    @StartTransaction
	public void setRoles(String username, List<String> roles)
	throws WebServiceCheckedException 
	{
		Admin admin = getAdmin(username);

		admin.getAuthorities().clear();
		
		for (String role : roles)
		{
			Authority authority = getAuthority(role);
			
			admin.getAuthorities().add(authority);
		}
	}
}
