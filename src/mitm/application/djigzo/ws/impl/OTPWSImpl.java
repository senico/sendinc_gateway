/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.ws.OTPWS;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.otp.OTPException;
import mitm.common.security.otp.OTPGenerator;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;

public class OTPWSImpl implements OTPWS
{
    /*
     * For finding the user
     */
    private final UserWorkflow userWorkflow;

    /*
     * The one time password generator
     */
    private final OTPGenerator otpGenerator;
    
    /*
     * The encoding used for the conversion of passwords to bytes
     */
    private final String encoding;
    
    public OTPWSImpl(UserWorkflow userWorkflow, OTPGenerator otpGenerator, String encoding)
    {
        Check.notNull(userWorkflow, "userWorkflow");
        Check.notNull(otpGenerator, "otpGenerator");
        Check.notNull(encoding, "encoding");
        
        this.userWorkflow = userWorkflow;
        this.otpGenerator = otpGenerator;
        this.encoding = encoding;
    }
    
    @Override
    @StartTransaction
    public String generate(String email, String counter, Integer byteLength)
    throws WebServiceCheckedException
    {
        email = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (email == null) {
            throw new WebServiceCheckedException("email is not a valid email address");
        }
        
        try {
            UserProperties properties = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).
                    getUserPreferences().getProperties();
            
            String clientSecret = properties.getClientSecret();
            
            if (StringUtils.isEmpty(clientSecret)) {
                throw new WebServiceCheckedException("client secret is not set");
            }
            
            if (byteLength == null) {
                byteLength = properties.getPasswordLength();
            }
            
            return otpGenerator.generate(clientSecret.getBytes(encoding), counter.getBytes(encoding), byteLength);
        } 
        catch (AddressException e) {
            throw new WebServiceCheckedException("Error getting user properties");
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException("Error getting user properties");
        } 
        catch (UnsupportedEncodingException e) {
            throw new WebServiceCheckedException(encoding + " is not a valid charset");
        } 
        catch (OTPException e) {
            throw new WebServiceCheckedException("Error generating OTP");
        }
    }

    @Override
    @StartTransaction
    public boolean hasClientSecret(String email)
    throws WebServiceCheckedException
    {
        email = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (email == null) {
            throw new WebServiceCheckedException("email is not a valid email address");
        }
        
        try {
            UserProperties properties = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST).
                    getUserPreferences().getProperties();
            
            return StringUtils.isNotEmpty(properties.getClientSecret());
        } 
        catch (AddressException e) {
            throw new WebServiceCheckedException("Error getting user properties");
        } 
        catch (HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException("Error getting user properties");
        } 
    }
}
