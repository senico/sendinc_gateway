/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import mitm.application.djigzo.dlp.PolicyPatternManager;
import mitm.application.djigzo.dlp.PolicyPatternNode;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.impl.PolicyPatternImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MimeTypes;
import mitm.common.util.Check;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.util.SizeUtils;
import mitm.common.ws.WebServiceCheckedException;

/**
 * Implementation of PolicyPatternManagerWS
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternManagerWSImpl implements PolicyPatternManagerWS
{
    /*
     * Some sane upper limit on the size of the XML exported.
     */
    private final static int MAX_XML_SIZE = SizeUtils.MB;
    
    /*
     * Manages the policy patterns
     */
    private final PolicyPatternManager policyPatternManager; 
    
    public PolicyPatternManagerWSImpl(PolicyPatternManager policyPatternManager)
    {
        Check.notNull(policyPatternManager, "policyPatternManager");
        
        this.policyPatternManager = policyPatternManager;
    }

    @Override
    @StartTransaction
    public PolicyPatternNodeDTO createPattern(String name)
    throws WebServiceCheckedException
    {
        Check.notNull(name, "name");
        
        return PolicyPatternNodeDTO.toDTO(policyPatternManager.createPattern(name));
    }

    @Override
    @StartTransaction
    public void deletePattern(String name)
    throws WebServiceCheckedException
    {
        Check.notNull(name, "name");
        
        policyPatternManager.deletePattern(name);
    }

    @Override
    @StartTransaction
    public PolicyPatternNodeDTO getPattern(String name)
    throws WebServiceCheckedException
    {
        Check.notNull(name, "name");
        
        PolicyPatternNode node = policyPatternManager.getPattern(name);

        PolicyPatternNodeDTO dto = null;
        
        if (node != null) {
            dto = PolicyPatternNodeDTO.toDTO(node);
        }
        
        return dto;
    }

    @Override
    @StartTransaction
    public void renamePattern(String currentName, String newName)
    throws WebServiceCheckedException
    {
        Check.notNull(currentName, "currentName");
        Check.notNull(newName, "newName");

        PolicyPatternNode node = policyPatternManager.getPattern(currentName);
        
        if (node != null)
        {
            node.setName(newName);
            
            /*
             * We must also rename the pattern not just the node. But only if the pattern is set
             */
            PolicyPattern pattern = node.getPolicyPattern();
            
            if (pattern != null) {
                node.setPolicyPattern(PolicyPatternImpl.clone(pattern, newName));
            }
        }
    }
    
    @Override
    @StartTransaction
    public List<PolicyPatternNodeDTO> getPatterns(Integer firstResult, Integer maxResults)
    throws WebServiceCheckedException
    {
        List<PolicyPatternNode> nodes = policyPatternManager.getPatterns(firstResult, maxResults);
        
        List<PolicyPatternNodeDTO> dtos = new ArrayList<PolicyPatternNodeDTO>(nodes.size());
        
        for (PolicyPatternNode node : nodes) {
            dtos.add(PolicyPatternNodeDTO.toDTO(node));
        }
        
        return dtos;
    }
    
    @Override         
    @StartTransaction
    public int getNumberOfPatterns()
    throws WebServiceCheckedException
    {
        return policyPatternManager.getNumberOfPatterns();
    }    
    
    @Override         
    @StartTransaction
    public boolean isInUse(String name)
    throws WebServiceCheckedException
    {
        return policyPatternManager.isInUse(name);
    }    

    @Override         
    @StartTransaction
    public List<String> getInUseInfo(String name)
    throws WebServiceCheckedException
    {
        return policyPatternManager.getInUseInfo(name);
    }    
    
    @Override         
    @StartTransaction
    public BinaryDTO exportToXML(Collection<String> names)
    throws WebServiceCheckedException
    {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            SizeLimitedOutputStream output = new SizeLimitedOutputStream(bos, MAX_XML_SIZE, true /* exception */);
            
            policyPatternManager.exportToXML(names, output);
            
            DataSource source = new ByteArrayDataSource(bos.toByteArray(), MimeTypes.OCTET_STREAM);
            DataHandler dataHandler = new DataHandler(source);
            
            return new BinaryDTO(dataHandler);
        }
        catch(IOException e) {
            throw new WebServiceCheckedException(e.getMessage());
        }
    }
    
    @Override         
    @StartTransaction
    public void importFromXML(BinaryDTO xml, boolean skipExisting)
    throws WebServiceCheckedException
    {
        try {
            if (xml == null) {
                throw new WebServiceCheckedException("xml is null.");
            }
            
            DataHandler dataHandler = xml.getDataHandler();
            
            policyPatternManager.importFromXML(dataHandler.getInputStream(), skipExisting);
        }
        catch(IOException e) {
            throw new WebServiceCheckedException(e.getMessage());
        }
    }
}
