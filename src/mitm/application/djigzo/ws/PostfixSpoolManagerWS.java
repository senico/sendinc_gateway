/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.postfix.PostfixQueueItem;
import mitm.common.ws.WebServiceCheckedException;

@WebService
public interface PostfixSpoolManagerWS 
{
	/**
	 * Flush the Postfix queue: attempt to deliver all queued mail.
	 */
	public void flush()
	throws WebServiceCheckedException;
	
	/**
	 * Schedule immediate delivery of deferred mail with the specified queue ID.
	 */
	public void deliver(@WebParam(name="queueID") String queueID)
	throws WebServiceCheckedException;
	
	/**
	 * Returns a list of the queued messages starting with index and maxItems number of items.
	 */
	public List<PostfixQueueItem> getQueued(@WebParam(name="startIndex") int startIndex, 
			@WebParam(name="maxItems") int maxItems, @WebParam(name="searchPattern") String searchPattern)
	throws WebServiceCheckedException;
	
	/**
	 * Returns the number of items that have been queued.
	 */
	public int getQueueLength(@WebParam(name="searchPattern") String searchPattern) 
	throws WebServiceCheckedException;
	
	/**
	 * Delete one message with the named queue ID from the named mail queue(s).
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
 	 */
	public void delete(@WebParam(name="queueID") String queueID)
	throws WebServiceCheckedException;

	/**
	 * Put mail "on hold" so that no attempt is made to deliver it.
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 * 
	 * mailQueue is the name of the mail queue from which a message should be deleted. 
	 * If mailQueue is null it will default to (hold, incoming, active and deferred).
	 */
	public void hold(@WebParam(name="queueID") String queueID)
	throws WebServiceCheckedException;

	/**
	 * Release  mail  that  was  put  "on  hold".
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 */
	public void release(@WebParam(name="queueID") String queueID)
	throws WebServiceCheckedException;

	/**
	 * Requeue  the  message  with  the named queue ID from the named mail queue(s).
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
 	 */ 
	public void requeue(@WebParam(name="queueID") String queueID)
	throws WebServiceCheckedException;

	/**
 	 * Returns the Postfix message content of the message with the given queueID.
	 */
	public byte[] getMessage(@WebParam(name="queueID") String queueID, 
			@WebParam(name="maxLength") int maxLength)
	throws WebServiceCheckedException;
}
