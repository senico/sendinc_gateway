/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.Collection;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.ws.WebServiceCheckedException;

/**
 * Web service for PolicyPatternManager.
 * 
 * @author Martijn Brinkers
 *
 */
@WebService
public interface PolicyPatternManagerWS
{
    /**
     * Creates and adds a new pattern. The name of the pattern should be unique. 
     */
    public PolicyPatternNodeDTO createPattern(@WebParam(name="name") String name)
    throws WebServiceCheckedException;

    /**
     * Returns the pattern with the given name.
     */
    public PolicyPatternNodeDTO getPattern(@WebParam(name="name") String name)
    throws WebServiceCheckedException;

    /**
     * Renames the pattern.
     */
    public void renamePattern(@WebParam(name="currentName") String currentName, 
            @WebParam(name="newName") String newName)
    throws WebServiceCheckedException;
    
    /**
     * Returns all patterns starting at firstResult and max results maxResults.
     */
    public List<PolicyPatternNodeDTO> getPatterns(@WebParam(name="firstResult") Integer firstResult, 
            @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;

    /**
     * Returns the total number of available patterns
     */
    public int getNumberOfPatterns()
    throws WebServiceCheckedException;
    
    /**
     * Deletes the pattern with the given name.
     */
    public void deletePattern(@WebParam(name="name") String name)
    throws WebServiceCheckedException;
    
    /**
     * Returns true if the pattern is in use
     */
    public boolean isInUse(@WebParam(name="name") String name)
    throws WebServiceCheckedException;

    /**
     * Returns info about the usage of this pattern. Returns an empty list if the 
     * pattern is not in use (i.e., isInUse is false). The returned list is a 
     * list of comma separated strings.
     * 
     * Note: the returned info should only be used for displaying purposes.
     */
    public List<String> getInUseInfo(@WebParam(name="name") String name)
    throws WebServiceCheckedException;
    
    /**
     * Exports all the PolicyPatternNode's to xml
     */
    public BinaryDTO exportToXML(@WebParam(name="name") Collection<String> names)
    throws WebServiceCheckedException;
    
    /**
     * Exports all the PolicyPatternNode's from xml
     */
    public void importFromXML(@WebParam(name="xml") BinaryDTO xml, 
            @WebParam(name="skipExisting") boolean skipExisting)
    throws WebServiceCheckedException;
}
