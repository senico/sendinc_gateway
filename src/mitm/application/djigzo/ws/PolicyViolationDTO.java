/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.text.StrBuilder;

import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.util.Check;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicyViolationDTO
{
    /*
     * The name of the policy that was violated
     */
    @XmlElement
    private String policy;
    
    /*
     * The name of the rule that was violated (a policy can contain multiple rules)
     */
    @XmlElement
    private String rule;
    
    /*
     * The part that matched the rule (i.e. the part that violated the policy)
     */
    @XmlElement
    private String match;
    
    /*
     * Returns the priority associated with this violation.
     */
    @XmlElement
    private PolicyViolationPriority priority;
    
    protected PolicyViolationDTO() {
        /* JAXB requires a default constructor */        
    }

    public PolicyViolationDTO(PolicyViolation violation)
    {
        Check.notNull(violation, "violation");
        
        this.policy = violation.getPolicy();
        this.rule = violation.getRule();
        this.match = violation.getMatch();
        this.priority = violation.getPriority();
    }
    
    /**
     * The name of the policy that was violated
     */
    public String getPolicy() {
        return policy;
    }
    
    /**
     * The name of the rule that was violated (a policy can contain multiple rules)
     */
    public String getRule() {
        return rule;
    }
    
    /**
     * The part that matched the rule (i.e. the part that violated the policy)
     */
    public String getMatch() {
        return match;
    }
    
    /**
     * Returns the priority associated with this violation.
     */
    public PolicyViolationPriority getPriority() {
        return priority;
    }    
    
    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder(256);
        
        sb.append("Policy: ").append(policy).
           append(", Rule: ").append(rule).
           append(", Priority: ").append(priority).
           append(", Match: ").append(match);
        
        return sb.toString();
    }
}
