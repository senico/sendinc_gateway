/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.common.ws.WebServiceCheckedException;

@WebService
public interface AdminWS 
{
	public void setRoles(@WebParam(name="username") String username, 
			@WebParam(name="roles") List<String> roles)
	throws WebServiceCheckedException;
	
	/**
	 * Sets the password for the admin. If the admin does not exist a DjigzoSoapException
	 * will be thrown. How the password will be stored (encoded and salted) depends on the 
	 * registered PasswordEncoder and SaltSource bean.
	 */
	public void setPassword(@WebParam(name="username") String username, 
			@WebParam(name="password") String password)
	throws WebServiceCheckedException;

	/**
	 * Sets the encoding which is used to encode the password with.
	 */
	public void setPasswordEncoding(@WebParam(name="username") String username, 
			@WebParam(name="passwordEncoding") PasswordEncoding passwordEncoding)
	throws WebServiceCheckedException;

	/**
	 * Sets the salt which was used for the password.
	 */
	public void setSalt(@WebParam(name="username") String username, 
			@WebParam(name="salt") String salt)
	throws WebServiceCheckedException;
	
	/**
	 * Enables the admin. A new admin is disabled and should be enabled before it can be used.
	 */
	public void setEnabled(@WebParam(name="username") String username, 
			@WebParam(name="enabled") boolean enabled)
	throws WebServiceCheckedException;
}
