/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import mitm.common.security.Inherited;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.PublicKeyInspector;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.crl.CRLDistributionPointsInspector;
import mitm.common.security.digest.Digest;
import mitm.common.security.keystore.KeyStoreProvider;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;
import mitm.common.util.StringReplaceUtils;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This X509CertificateDTO extension is created to make sure that X509CertificateDTO does not depend on any Bouncycastle classes.
 * The implementation of X509CertificateDTO can be used by soap clients (if using JAXB). The non-default constructor however is not
 * used by the remove client so to decouple dependencies we do not need we created this build class. 
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CertificateDTOBuilderImpl implements X509CertificateDTOBuilder
{
	private final static Logger logger = LoggerFactory.getLogger(X509CertificateDTOBuilderImpl.class);
	
	/*
	 * Return result when a parameter value results in an error
	 */
	final static String ERROR = "ERROR";
	
	/*
	 * For checking whether the key is accessible
	 */
	private final KeyStoreProvider keyStoreProvider;
	
	/*
	 * If true, a check will be done whether or not the key is accessible
	 */
	private final boolean checkPrivateKeyAccessible;
		
	public X509CertificateDTOBuilderImpl(KeyStoreProvider keyStoreProvider, boolean checkPrivateKeyAccessible)
	{
		this.keyStoreProvider = keyStoreProvider;
		this.checkPrivateKeyAccessible = checkPrivateKeyAccessible;
	}
	
	@Override
	public X509CertificateDTO buildCertificateDTO(X509Certificate certificate, String keyAlias)
	{
		X509CertificateDTO certificateDTO = new X509CertificateDTO();
		
		initializeDTO(certificateDTO, certificate, keyAlias);
		
		return certificateDTO;
	}
	
	@Override
	public X509CertificateDTO buildKeyAndCertificateDTO(KeyAndCertificate keyAndCertificate, String keyAlias)
	{
    	Check.notNull(keyAndCertificate, "keyAndCertificate");
    	Check.notNull(keyAndCertificate.getCertificate(), "keyAndCertificate#getCertificate()");

    	X509CertificateDTO certificateDTO = buildCertificateDTO(keyAndCertificate.getCertificate(), 
    			keyAlias);
    	
    	certificateDTO.inherited = keyAndCertificate instanceof Inherited;
    	
    	return certificateDTO;
	}
	
    /**
     * Initializes the DTO.   
     */
    protected void initializeDTO(X509CertificateDTO dto, X509Certificate certificate, String keyAlias)
    {
        try {
	        X509CertificateInspector certificateInspector = new X509CertificateInspector(certificate);

	        /*
	         * All the parameters will be retrieved within their own catch block to make sure that other values
	         * are retrieved when one parameter fails (for example the CRL dist. point can contain invalid ASN.1)
	         */
	        dto.thumbprint                = safeGetThumbprint(certificateInspector);
	        dto.thumbprintSHA1            = safeGetThumbprintSHA1(certificateInspector);
	        dto.keyAlias                  = keyAlias;
	        dto.serialNumberHex           = safeGetSerialNumberHex(certificateInspector);
	        dto.issuerFriendly            = safeGetIssuerFriendly(certificateInspector);
	        dto.subjectFriendly           = safeGetSubjectFriendly(certificateInspector);
	        dto.subjectKeyIdentifier      = safeGetSubjectKeyIdentifier(certificateInspector);
	        dto.notBefore                 = safeGetNotBefore(certificate);
	        dto.notAfter                  = safeGetNotAfter(certificate);
	        dto.expired                   = safeIsExpired(certificateInspector);
	        dto.emailFromAltNames         = safeGetEmailFromAltNames(certificateInspector);
	        dto.emailFromDN               = safeGetEmailFromDN(certificateInspector);
	        dto.keyUsage                  = safeGetkeyUsage(certificateInspector);
	        dto.extendedKeyUsage          = safeGetExtendedkeyUsage(certificateInspector);
	        dto.ca                        = safeIsCA(certificateInspector);
	        dto.pathLengthConstraint      = safeGetPathLengthConstraint(certificateInspector);
	        dto.publicKeyLength           = safeGetPublicKeyLength(certificate);
	        dto.publicKeyAlgorithm        = safeGetPublicKeyAlgorithm(certificate);
	        dto.signatureAlgorithm        = safeGetSignatureAlgorithm(certificate);
	        dto.uriDistributionPointNames = safeGetURIDistributionPointNames(certificateInspector);
	    	dto.privateKeyAvailable       = isPrivateKeyAvailable(keyAlias);
	    	dto.privateKeyAccessible      = isPrivateKeyAccessible(keyAlias);
        }
        catch(Exception e) {
            /*
             * Catch all exceptions to make sure that some of the settings are at least returned.
             */
        	logger.error("Error inspecting certificate.", e);
        } 
    }
    
    private boolean isPrivateKeyAvailable(String keyAlias) {
    	return keyAlias != null;
    }

    private boolean isPrivateKeyAccessible(String keyAlias)
    {
    	/*
    	 * Assume it's accessible if we do not check or if there is no key alias set.
    	 */
    	if (keyAlias == null || !checkPrivateKeyAccessible || keyStoreProvider == null) {
    		return true;
    	}
    	
    	boolean available = false;
    	
    	try {
			available = keyStoreProvider.getKeyStore().isKeyEntry(keyAlias);
		} 
    	catch (Exception e) {
    		logger.error("Error checking isPrivateKeyAccessible for alias: {}", keyAlias, e);
		}
    	
    	return available;
    }
    
    private String safeGetThumbprint(X509CertificateInspector inspector)
    {
        try {
            return inspector.getThumbprint();
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetThumbprintSHA1(X509CertificateInspector inspector)
    {
        try {
            return inspector.getThumbprint(Digest.SHA1);
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }
    
    private String safeGetSerialNumberHex(X509CertificateInspector inspector)
    {
        try {
            return inspector.getSerialNumberHex();
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }
    
    private String safeGetIssuerFriendly(X509CertificateInspector inspector)
    {
        try {
            return StringReplaceUtils.replaceNonXML(inspector.getIssuerFriendly(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetSubjectFriendly(X509CertificateInspector inspector)
    {
        try {
            return StringReplaceUtils.replaceNonXML(inspector.getSubjectFriendly(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }
    
    
    private String safeGetSubjectKeyIdentifier(X509CertificateInspector inspector)
    {
        try {
            return inspector.getSubjectKeyIdentifierHex();
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private Date safeGetNotBefore(X509Certificate certificate)
    {
        try {
            return certificate.getNotBefore();
        }
        catch(Exception e) {
            // ignore
        }
        return null;
    }
    
    private Date safeGetNotAfter(X509Certificate certificate)
    {
        try {
            return certificate.getNotAfter();
        }
        catch(Exception e) {
            // ignore
        }
        return null;
    }

    private boolean safeIsExpired(X509CertificateInspector inspector)
    {
        try {
            return inspector.isExpired();
        }
        catch(Exception e) {
            // ignore
        }
        return true;
    }

    private List<String> safeGetEmailFromAltNames(X509CertificateInspector inspector)
    {
        try {
            return StringReplaceUtils.replaceNonXML(inspector.getEmailFromAltNames(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return Collections.singletonList(ERROR);
    }

    private List<String> safeGetEmailFromDN(X509CertificateInspector inspector)
    {
        try {
            return StringReplaceUtils.replaceNonXML(inspector.getEmailFromDN(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return Collections.singletonList(ERROR);
    }
    
    private Set<String> safeGetkeyUsage(X509CertificateInspector inspector)
    {
        try {
            return CollectionUtils.toStringSet(inspector.getKeyUsage(), "");
        }
        catch(Exception e) {
            // ignore
        }
        return Collections.singleton(ERROR);
    }

    private Set<String> safeGetExtendedkeyUsage(X509CertificateInspector inspector)
    {
        try {
            return CollectionUtils.toStringSet(inspector.getExtendedKeyUsage(), "");
        }
        catch(Exception e) {
            // ignore
        }
        return Collections.singleton(ERROR);
    }

    private boolean safeIsCA(X509CertificateInspector inspector)
    {
        try {
            return inspector.isCA();
        }
        catch(Exception e) {
            // ignore
        }
        return false;
    }

    
    private BigInteger safeGetPathLengthConstraint(X509CertificateInspector inspector)
    {
        try {
            BasicConstraints bc = inspector.getBasicConstraints();
            
            return bc != null ? bc.getPathLenConstraint() : null;
        }
        catch(Exception e) {
            // ignore
        }
        return null;
    }

    private int safeGetPublicKeyLength(X509Certificate certificate)
    {
        try {
            return PublicKeyInspector.getKeyLength(certificate.getPublicKey());
        }
        catch(Exception e) {
            // ignore
        }
        return 0;
    }
    
    private String safeGetPublicKeyAlgorithm(X509Certificate certificate)
    {
        try {
            return PublicKeyInspector.getFriendlyAlgorithm(certificate.getPublicKey());
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetSignatureAlgorithm(X509Certificate certificate)
    {
        try {
            return certificate.getSigAlgName();
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private Set<String> safeGetURIDistributionPointNames(X509CertificateInspector inspector)
    {
        try {
            CRLDistributionPointsInspector dpi = new CRLDistributionPointsInspector(
                    inspector.getCRLDistibutionPoints());

            return StringReplaceUtils.replaceNonXML(dpi.getURIDistributionPointNames(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return Collections.singleton(ERROR);
    }
}
