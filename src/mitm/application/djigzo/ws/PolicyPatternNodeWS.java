/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.ws.WebServiceCheckedException;

/**
 * Web service for PolicyPatternNode.
 * 
 * @author Martijn Brinkers
 *
 */
@WebService
public interface PolicyPatternNodeWS
{
    /**
     * Returns the PolicyPattern of the PolicyPatternNode. The PolicyPatternNode's with name must exist.
     */
    public PolicyPatternDTO getPattern(@WebParam(name="name") String name)
    throws WebServiceCheckedException;

    /**
     * Sets the PolicyPattern of the PolicyPatternNode. The PolicyPatternNode's with name must exist.
     */
    public void setPattern(@WebParam(name="name") String name, @WebParam(name="pattern") PolicyPatternDTO pattern)
    throws WebServiceCheckedException;
    
    /**
     * Add the PolicyPatternNode with childName to the PolicyPatternNode with parentName. The PolicyPatternNode's
     * with parentName and childName must exist.
     */
    public void addChild(@WebParam(name="parentName") String parentName, 
            @WebParam(name="childName") String childName)
    throws WebServiceCheckedException;

    /**
     * Deletes the PolicyPatternNode with childName from the childs of the PolicyPatternNode with parentName. 
     * The PolicyPatternNode's with parentName and childName must exist.
     */
    public void removeChild(@WebParam(name="parentName") String parentName,
            @WebParam(name="childName") String childName)
    throws WebServiceCheckedException;

    /**
     * Sets all childs of the PolicyPatternNode with the given name.
     */
    public void setChilds(@WebParam(name="name") String parentName, @WebParam(name="childs") List<String> childs)
    throws WebServiceCheckedException;
    
    /**
     * Returns all child PolicyPatternNode's of the PolicyPatternNode with the given name.
     */
    public List<PolicyPatternNodeDTO> getChilds(@WebParam(name="name") String name)
    throws WebServiceCheckedException;
}
