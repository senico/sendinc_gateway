/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import mitm.common.mail.repository.MailRepositorySearchField;
import mitm.common.ws.WebServiceCheckedException;


/**
 * Webservice for accessing a MailRepository 
 * 
 * @author Martijn Brinkers
 *
 */
@WebService
public interface MailRepositoryWS
{
    /**
     * Gets the item with the given id.
     */
    public MailRepositoryItemDTO getItem(@WebParam(name="id") String id)
    throws WebServiceCheckedException;

    /**
     * Deletes the item with the given id.
     */
    public void deleteItem(@WebParam(name="id") String id)
    throws WebServiceCheckedException;

    /**
     * Returns the MIME message of the item with the given id.
     */
    public BinaryDTO getMimeMessage(@WebParam(name="id") String id)
    throws WebServiceCheckedException;

    /**
     * Returns maxResults items starting at firstResult sorted on creation date.
     */
    public List<MailRepositoryItemDTO> getItems(@WebParam(name="firstResult") Integer firstResult, 
            @WebParam(name="maxResults") Integer maxResults)
    throws WebServiceCheckedException;

    /**
     * Returns maxResults items starting at firstResult with the given search sorted on creation date. 
     */
    public List<MailRepositoryItemDTO> searchItems(@WebParam(name="searchField") MailRepositorySearchField searchField, 
            @WebParam(name="key") String key, @WebParam(name="firstResult") Integer firstResult, 
            @WebParam(name="maxResults") Integer maxResults)
            throws WebServiceCheckedException;

    /**
     * Returns the number of items in the repository
     */
    int getItemCount()
    throws WebServiceCheckedException;

    /**
     * Returns the number of items in the repository with the given search.
     */
    int getSearchCount(@WebParam(name="searchField") MailRepositorySearchField searchField, 
            @WebParam(name="key") String key)
    throws WebServiceCheckedException;
    
    /**
     * Releases the message with the given ID
     */
    public void releaseMessage(@WebParam(name="id") String id, @WebParam(name="processor") ReleaseProcessor processor)
    throws WebServiceCheckedException;    
}
