/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import mitm.common.security.ca.RequestParameters;
import mitm.common.security.ca.RequestParametersImpl;

public class RequestParametersDTOUtils
{
    /**
     * Converts the RequestParametersDTO to a RequestParameters
     */
    public static RequestParameters toRequestParameters(RequestParametersDTO requestDTO)
    throws IOException
    {
        if (requestDTO == null) {
            return null;
        }
        
        RequestParameters parameters = new RequestParametersImpl();
        
        parameters.setSubject(X500PrincipalDTOUtils.toX500Principal(requestDTO.getSubject()));
        parameters.setEmail(requestDTO.getEmail());
        parameters.setValidity(requestDTO.getValidity());
        parameters.setKeyLength(requestDTO.getKeyLength());
        parameters.setSignatureAlgorithm(requestDTO.getSignatureAlgorithm());
        parameters.setCRLDistributionPoint(requestDTO.getCRLDistributionPoint());
        parameters.setCertificateRequestHandler(requestDTO.getCertificateRequestHandler());
        
        return parameters;
    }

    /**
     * Converts the RequestParametersDTO to a RequestParameters
     */
    public static RequestParametersDTO toRequestParametersDTO(RequestParameters request)
    throws IOException
    {
        if (request == null) {
            return null;
        }
        
        return new RequestParametersDTO(X500PrincipalDTOUtils.toX500PrincipalDTO(request.getSubject()),
                request.getEmail(), 
                request.getValidity(), 
                request.getKeyLength(), 
                request.getSignatureAlgorithm(),
                request.getCRLDistributionPoint(), 
                request.getCertificateRequestHandler());
    }
    
    /**
     * Converts the collection of RequestParametersDTO's to a List of RequestParameters's
     * @throws IOException 
     */
    public static List<RequestParametersDTO> toRequestParametersDTOs(Collection<RequestParameters> requests)
    throws IOException
    {
        if (requests == null) {
            return Collections.emptyList();
        }
        
        List<RequestParametersDTO> result = new ArrayList<RequestParametersDTO>(requests.size());
        
        for (RequestParameters request : requests) {
            result.add(toRequestParametersDTO(request));
        }
        
        return result;
    }
}
