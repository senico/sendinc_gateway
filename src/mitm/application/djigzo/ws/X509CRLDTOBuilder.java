/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.security.cert.X509CRL;
import java.util.Date;

import mitm.common.security.AlgorithmUtils;
import mitm.common.security.crl.X509CRLInspector;
import mitm.common.security.digest.Digest;
import mitm.common.util.BigIntegerUtils;
import mitm.common.util.Check;
import mitm.common.util.StringReplaceUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This X509CRLDTO extension is created to make sure that X509CRLDTO does not depend on any Bouncycastle classes.
 * The implementation of X509CRLDTO can be used by soap clients (if using JAXB). The non-default constructor however is not
 * used by the remove client so to decouple dependencies we do not need we created this build class. 
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CRLDTOBuilder extends X509CRLDTO 
{
	private final static Logger logger = LoggerFactory.getLogger(X509CRLDTOBuilder.class);
	
    final static String ERROR = "ERROR";
	
    public X509CRLDTOBuilder(X509CRL crl)
    {
    	Check.notNull(crl, "crl");

    	try {
            X509CRLInspector inspector = new X509CRLInspector(crl);

    	    /*
             * All the parameters will be retrieved within their own catch block to make sure that other values
             * are retrieved when one parameter fails (for example the CRL dist. point can contain invalid ASN.1)
             */

    	    thumbprint         = safeGetThumbprint(inspector);
            thumbprintSHA1     = safeGetThumbprintSHA1(inspector);
            issuerFriendly     = safeGetIssuerFriendly(inspector);
            nextUpdate         = safeGetNextUpdate(crl);
            thisUpdate         = safeGetThisUpdate(crl);
            version            = safeGetVersion(crl);
            crlNumber          = safeGetCRLNumber(inspector);
            deltaIndicator     = safeGetDeltaIndicator(inspector);
            deltaCRL           = safeIsDeltaCRL(inspector);
            signatureAlgorithm = safeGetSignatureAlgorithm(crl);
    	}
    	catch(Exception e) {
            /*
             * Catch all exceptions to make sure that some of the settings are at least returned.
             */
        	logger.error("Error inspecting CRL.", e);
    	} 
    }
    
    private String safeGetThumbprint(X509CRLInspector inspector)
    {
        try {
            return inspector.getThumbprint();
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetThumbprintSHA1(X509CRLInspector inspector)
    {
        try {
            return inspector.getThumbprint(Digest.SHA1);
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetIssuerFriendly(X509CRLInspector inspector)
    {
        try {
            return StringReplaceUtils.replaceNonXML(inspector.getIssuerFriendly(), "?");
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private Date safeGetNextUpdate(X509CRL crl)
    {
        try {
            return crl.getNextUpdate();
        }
        catch(Exception e) {
            // ignore
        }
        return null;
    }

    private Date safeGetThisUpdate(X509CRL crl)
    {
        try {
            return crl.getThisUpdate();
        }
        catch(Exception e) {
            // ignore
        }
        return null;
    }

    private int safeGetVersion(X509CRL crl)
    {
        try {
            return crl.getVersion(); 
        }
        catch(Exception e) {
            // ignore
        }
        return 0;
    }

    private String safeGetCRLNumber(X509CRLInspector inspector)
    {
        try {
            return BigIntegerUtils.hexEncode(inspector.getCRLNumber());
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }

    private String safeGetDeltaIndicator(X509CRLInspector inspector)
    {
        try {
            return BigIntegerUtils.hexEncode(inspector.getDeltaIndicator());
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }
    
    private boolean safeIsDeltaCRL(X509CRLInspector inspector)
    {
        try {
            return inspector.isDeltaCRL(); 
        }
        catch(Exception e) {
            // ignore
        }
        return false;
    }
    
    private String safeGetSignatureAlgorithm(X509CRL crl)
    {
        try {
            return AlgorithmUtils.getAlgorithmName(crl.getSigAlgOID());
        }
        catch(Exception e) {
            // ignore
        }
        return ERROR;
    }    
}
