/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class X509CRLDTO 
{
    @XmlElement
    protected String issuerFriendly;
    
    @XmlElement
    protected Date nextUpdate;

    @XmlElement
    protected Date thisUpdate;

    @XmlElement
    protected int version;
    
    @XmlElement
    protected String crlNumber;
    
    @XmlElement
    protected String deltaIndicator;

    @XmlElement
    protected boolean deltaCRL;
    
    @XmlElement
    protected String thumbprint;
    
    @XmlElement
    protected String thumbprintSHA1;

    @XmlElement
    protected String signatureAlgorithm;
    
    protected X509CRLDTO() {
        /* JAXB requires a default constructor */        
	}
        
	public String getIssuerFriendly() {
		return issuerFriendly;
	}

	public Date getNextUpdate() {
		return nextUpdate;
	}

	public Date getThisUpdate() {
		return thisUpdate;
	}

	public int getVersion() {
		return version;
	}

	public String getCrlNumber() {
		return crlNumber;
	}

	public String getDeltaIndicator() {
		return deltaIndicator;
	}

	public boolean isDeltaCRL() {
		return deltaCRL;
	}

	public String getThumbprint() {
		return thumbprint;
	}

	public String getThumbprintSHA1() {
		return thumbprintSHA1;
	}

	public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }
}
