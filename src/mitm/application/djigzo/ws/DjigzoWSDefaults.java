/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.application.djigzo.ws;

/**
 * URLs and service names of all the Djigzo web services 
 * 
 * @author Martijn Brinkers
 */
public class DjigzoWSDefaults 
{
    /**
     * The default service port 
     */
    public static final int PORT = 9000;
    
    
	public static String NAMESPACE = "http://ws.djigzo.com";
	
    public final static String USERS_SERVICE_NAME = "Users";
    public final static String USERS_WSDL = "/usersws?wsdl";
    
    public final static String USER_SERVICE_NAME = "User";
    public final static String USER_WSDL = "/userws?wsdl";

    public final static String USER_PREFERENCES_SERVICE_NAME = "UserPreferences";
    public final static String USER_PREFERENCES_WSDL = "/userpreferencesws?wsdl";

    public final static String USER_PREFERENCES_WORKFLOW_SERVICE_NAME = "UserPreferencesWorkflow";
    public final static String USER_PREFERENCES_WORKFLOW_WSDL = "/userpreferencesworkflowws?wsdl";

    public final static String GLOBAL_PREFERENCES_MANAGER_SERVICE_NAME = "GlobalPreferencesManager";
    public final static String GLOBAL_PREFERENCES_MANAGER_WSDL = "/globalpreferencesmanagerws?wsdl";

    public final static String DOMAINS_SERVICE_NAME = "Domains";
    public final static String DOMAINS_WSDL = "/domainsws?wsdl";
    
    public final static String DOMAIN_SERVICE_NAME = "Domain";
    public final static String DOMAIN_WSDL = "/domainws?wsdl";
    
    public final static String HIERARCHICAL_PROPERTIES_SERVICE_NAME = "HierarchicalProperties";
    public final static String HIERARCHICAL_PROPERTIES_WSDL = "/hierarchicalpropertiesws?wsdl";

    public final static String KEY_AND_ROOT_CERTSTORE_SERVICE_NAME = "KeyAndRootCertStore";
    public final static String KEY_AND_ROOT_CERTSTORE_WSDL = "/keyandrootcertstorews?wsdl";    
    
    public final static String KEY_AND_CERTSTORE_SERVICE_NAME = "KeyAndCertStore";
    public final static String KEY_AND_CERTSTORE_WSDL = "/keyandcertstorews?wsdl";    
    
    public final static String CRL_STORE_SERVICE_NAME = "CRLStore";
    public final static String CRL_STORE_WSDL = "/crlstorews?wsdl";    

    public final static String KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME = "KeyAndCertificateWorkflow";
    public final static String KEY_AND_CERTIFICATE_WORKFLOW_WSDL = "/keyandcertificateworkflowws?wsdl";    
    
    public final static String CERTIFICATE_VALIDATOR_SERVICE_NAME = "CertificateValidator";
    public final static String CERTIFICATE_VALIDATOR_WSDL = "/certificatevalidatorws?wsdl";    
    
    public final static String ADMIN_MANAGER_SERVICE_NAME = "AdminManager";
    public final static String ADMIN_MANAGER_WSDL = "/adminmanagerws?wsdl";    

    public final static String ADMIN_SERVICE_NAME = "Admin";
    public final static String ADMIN_WSDL = "/adminws?wsdl";    

    public final static String AUTHORITY_MANAGER_SERVICE_NAME = "AuthorityManager";
    public final static String AUTHORITY_MANAGER_WSDL = "/authorityManagerws?wsdl";    
    
    public final static String LOGIN_SERVICE_NAME = "Login";
    public final static String LOGIN_WSDL = "/loginws?wsdl";    

    public final static String SMS_GATEWAY_SERVICE_NAME = "SMSGateway";
    public final static String SMS_GATEWAY_WSDL = "/smsgatewayws?wsdl";    
    
    public final static String CLICKATELL_SERVICE_NAME = "Clickatell";
    public final static String CLICKATELL_WSDL = "/clickatellws?wsdl";    

    public final static String JAMES_MANAGER_SERVICE_NAME = "JamesManager";
    public final static String JAMES_MANAGER_WSDL = "/jamesmanagerws?wsdl";    
    
    public final static String JAMES_REPOSITORY_MANAGER_SERVICE_NAME = "JamesRepositoryManager";
    public final static String JAMES_REPOSITORY_MANAGER_WSDL = "/jamesrepositorymanagerws?wsdl";    

    public final static String POSTFIX_SPOOL_MANAGER_SERVICE_NAME = "PostfixSpoolManager";
    public final static String POSTFIX_SPOOL_MANAGER_WSDL = "/postfixspoolmanagerws?wsdl";    
    
    public final static String POSTFIX_LOG_MANAGER_SERVICE_NAME = "PostfixLogManager";
    public final static String POSTFIX_LOG_MANAGER_WSDL = "/postfixlogmanagerws?wsdl";    

    public final static String POSTFIX_CONFIG_MANAGER_SERVICE_NAME = "PostfixConfigManager";
    public final static String POSTFIX_CONFIG_MANAGER_WSDL = "/postfixconfigmanagerws?wsdl";    

    public final static String LOGGER_MANAGER_SERVICE_NAME = "LoggerManager";
    public final static String LOGGER_MANAGER_WSDL = "/loggermanagerws?wsdl";    
    
    public final static String SYSTEM_MANAGER_SERVICE_NAME = "SystemManager";
    public final static String SYSTEM_MANAGER_WSDL = "/systemmanagerws?wsdl";    
    
    public final static String BACKUP_SERVICE_NAME = "Backup";
    public final static String BACKUP_WSDL = "/backupws?wsdl";    

    public final static String EVENT_LOGGER_SERVICE_NAME = "EventLogger";
    public final static String EVENT_LOGGER_WSDL = "/eventloggerws?wsdl";    
    
    public final static String CA_SERVICE_NAME = "CA";
    public final static String CA_WSDL = "/caws?wsdl";    
    
    public final static String CA_CERT_STORE_VIEW_SERVICE_NAME = "CACertStoreView";
    public final static String CA_CERT_STORE_VIEW_WSDL = "/cacertstoreviewws?wsdl";    

    public final static String CERTIFICATE_REQUEST_STORE_SERVICE_NAME = "CertificateRequestStore";
    public final static String CERTIFICATE_REQUEST_STORE_WSDL = "/certificaterequeststorews?wsdl";    
    
    public final static String CTL_SERVICE_NAME = "CTL";
    public final static String CTL_WSDL = "/ctlws?wsdl";    

    public final static String FETCHMAIL_MANAGER_SERVICE_NAME = "FetchmailManager";
    public final static String FETCHMAIL_MANAGER_WSDL = "/fetchmailmanagerws?wsdl";    

    public final static String POLICY_PATTERN_MANAGER_SERVICE_NAME = "PolicyPatternManager";
    public final static String POLICY_PATTERN_MANAGER_WSDL = "/policypatternmanagerws?wsdl";    

    public final static String POLICY_PATTERN_NODE_SERVICE_NAME = "PolicyPatternNode";
    public final static String POLICY_PATTERN_NODE_WSDL = "/policypatternnodews?wsdl";    

    public final static String MATCH_FILTER_REGISTRY_SERVICE_NAME = "MatchFilterRegistry";
    public final static String MATCH_FILTER_REGISTRY_WSDL = "/matchfilterregistryws?wsdl";    

    public final static String USER_PREFERENCES_POLICY_PATTERN_MANAGER_SERVICE_NAME = "UserPreferencesPolicyPatternManager";
    public final static String USER_PREFERENCES_POLICY_PATTERN_MANAGER_WSDL = "/userpreferencespolicypatternmanagerws?wsdl";    
    
    public final static String COMODO_SERVICE_NAME = "Comodo";
    public final static String COMODO_WSDL = "/comodows?wsdl";    

    public final static String WORD_SKIPPER_SERVICE_NAME = "WordSkipper";
    public final static String WORD_SKIPPER_WSDL = "/wordskipperws?wsdl";    

    public final static String QUARANTINE_MAIL_REPOSITORY_SERVICE_NAME = "QuarantineMailRepository";
    public final static String QUARANTINE_MAIL_REPOSITORY_WSDL = "/quarantinemailrepositoryws?wsdl";    

    public final static String OTP_SERVICE_NAME = "OTP";
    public final static String OTP_WSDL = "/otpws?wsdl";    

    public final static String PORTAL_USER_SERVICE_NAME = "PortalUser";
    public final static String PORTAL_USER_WSDL = "/portaluserws?wsdl";    
}
