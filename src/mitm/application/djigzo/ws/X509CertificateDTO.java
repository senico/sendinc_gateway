/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO class used by the SOAP web services.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class X509CertificateDTO
{	
	/*
	 * The alias of the associated key. Null if there is no key associated
	 * with the certificate
	 */
    @XmlElement
    protected String keyAlias;
    
    /*
     * Serial number of the certificate in HEX format
     */
    @XmlElement
    protected String serialNumberHex;
    
    /*
     * Subject Key identifier of the certificate
     */
    @XmlElement
    protected String subjectKeyIdentifier;
    
    /*
     * Human readable version of the certificate issuer
     */
    @XmlElement
    protected String issuerFriendly;
    
    /*
     * Human readable version of the certificate subject
     */
    @XmlElement
    protected String subjectFriendly;
    
    /*
     * Not before date of the certificate
     */
    @XmlElement
    protected Date notBefore;
    
    /*
     * Not after date of the certificate
     */
    @XmlElement
    protected Date notAfter;
    
    /*
     * True if the certificate expired
     */    
    @XmlElement
    protected boolean expired;
    
    /*
     * All the email addresses of the alternative name RFC822 field
     */
    @XmlElement
    protected List<String> emailFromAltNames;
    
    /*
     * All the email addresses of the DN field
     */
    @XmlElement
    protected List<String> emailFromDN;
    
    /*
     * The allowed key usages of the certificate
     */
    @XmlElement
    protected Set<String> keyUsage;
    
    /*
     * The allowed extended key usages of the certificate
     */
    @XmlElement
    protected Set<String> extendedKeyUsage;
    
    /*
     * The thumbprint of the certificate (by default sha512)
     */    
    @XmlElement
    protected String thumbprint;
    
    /*
     * The SHA1 thumbprint of the certificate
     */    
    @XmlElement
    protected String thumbprintSHA1;
    
    /*
     * All the CRL distribution points of the certificate
     */    
    @XmlElement
    protected Set<String> uriDistributionPointNames;
    
    /*
     * True if the CA field of the certificate is set
     */    
    @XmlElement
    protected boolean ca;

    /*
     * The path length constraint of the certificate
     */    
    @XmlElement
    protected BigInteger pathLengthConstraint;
    
    /*
     * The length of the public key (1024, 2048, 4096)
     */    
    @XmlElement
    protected int publicKeyLength;

    /*
     * The algorithm for the pub key
     */    
    @XmlElement
    protected String publicKeyAlgorithm;
    
    /*
     * The algorithm used for signing the certificate
     */    
    @XmlElement
    protected String signatureAlgorithm;

    /*
     * True if the certificate has a private key (actually, this is true if the
     * certificate has an associated key alias)
     */    
    @XmlElement
    protected boolean privateKeyAvailable;

    /*
     * True if the private key is available in the key store. Can be false for example
     * when an HSM is used and the HSM is not available.
     */    
    @XmlElement
    protected boolean privateKeyAccessible;
    
    /*
     * True if the certificate is inherited
     */    
    @XmlElement
    protected boolean inherited;
    
    /*
     * The certificate store this certificate belongs to (null if unknown)
     */
    @XmlElement
    protected CertificateStore certificateStore;

    protected X509CertificateDTO() {
        /* JAXB requires a default constructor */        
    }
    
    /**
     * The associated key alias. 
     */
    public String getKeyAlias() {
        return keyAlias;
    }
    
    public String getSerialNumberHex() {
        return serialNumberHex;
    }

    public String getIssuerFriendly() {
        return issuerFriendly;
    }

    public String getSubjectFriendly() {
        return subjectFriendly;
    }

    public String getSubjectKeyIdentifier() {
        return subjectKeyIdentifier;
    }

    public List<String> getEmailFromAltNames() {
        return emailFromAltNames;
    }

    public List<String> getEmailFromDN() {
        return emailFromDN;
    }
    
    public Set<String> getKeyUsage() {
        return keyUsage;
    }

    public Set<String> getExtendedKeyUsage() {
        return extendedKeyUsage;
    }

    public String getThumbprint() {
        return thumbprint;
    }

    public String getThumbprintSHA1() {
        return thumbprintSHA1;
    }
    
    public Set<String> getURIDistributionPointNames() {
        return uriDistributionPointNames;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public Date getNotAfter() {
        return notAfter;
    }
    
    public boolean isExpired() {
        return expired;
    }
    
    public boolean isCA() {
        return ca;
    }

    public BigInteger getPathLengthConstraint() {
        return pathLengthConstraint;
    }
    
    public int getPublicKeyLength() {
        return publicKeyLength;
    }

    public String getPublicKeyAlgorithm() {
        return publicKeyAlgorithm;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public boolean isPrivateKeyAvailable() {
        return privateKeyAvailable;
    }

    public boolean isPrivateKeyAccessible() {
        return privateKeyAccessible;
    }

    public boolean isInherited() {
    	return inherited;
    }
    
    public CertificateStore getCertificateStore() {
        return certificateStore;
    }

    public void setCertificateStore(CertificateStore certificateStore) {
        this.certificateStore = certificateStore;
    }
}