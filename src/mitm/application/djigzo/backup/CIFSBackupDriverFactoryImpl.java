/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.backup;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.UserPreferences;
import mitm.common.backup.BackupDriver;
import mitm.common.backup.BackupDriverFactory;
import mitm.common.backup.BackupException;
import mitm.common.backup.impl.CIFSBackupDriver;
import mitm.common.cifs.PropertiesSMBFileParameters;
import mitm.common.cifs.SMBFileParameters;
import mitm.common.cifs.StaticSMBFileParameters;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

/**
 * Factory for CIFSBackupDriver. The settings (SMBFileParameters) for the driver are retrieved from the
 * global properties.
 * 
 * @author Martijn Brinkers
 *
 */
public class CIFSBackupDriverFactoryImpl implements BackupDriverFactory
{
    private final GlobalPreferencesManager globalPreferencesManager;
    
    public CIFSBackupDriverFactoryImpl(GlobalPreferencesManager globalPreferencesManager)
    {
        Check.notNull(globalPreferencesManager, "globalPreferencesManager");
        
        this.globalPreferencesManager = globalPreferencesManager;
    }
        
    private HierarchicalProperties getGlobalPropeties() 
    throws HierarchicalPropertiesException 
    {
        UserPreferences globalPreferences = globalPreferencesManager.getGlobalUserPreferences();
        
        Check.notNull(globalPreferences, "globalPreferences");
        
        HierarchicalProperties properties = globalPreferences.getProperties();

        Check.notNull(properties, "properties");
        
        return properties;
    }    

    private SMBFileParameters getSMBFileParameters()
    throws HierarchicalPropertiesException
    {
        StaticSMBFileParameters parameters = new StaticSMBFileParameters();

        /*
         * We will create a snapshot from the SMBFileParameters read from the database properties. We do
         * this because we do not want the CIFSBackupDriver to use the dynamic implementation because
         * the file parameter will be changed and we do not want it to be written back to the peristent
         * properties.
         */
        SMBFileParameters fromProperties = new PropertiesSMBFileParameters(getGlobalPropeties());
        
        fromProperties.copyTo(parameters);
        
        return parameters;
    }
    
    @Override
    public BackupDriver createBackupDriver()
    throws BackupException
    {
        try {
            return new CIFSBackupDriver(getSMBFileParameters());
        }
        catch (HierarchicalPropertiesException e) {
            throw new BackupException(e);
        }
    }
}
