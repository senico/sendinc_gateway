/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.backup;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

/**
 * Implementation of DjigzoBackupPreferences that read it's settings from a HierarchicalProperties
 * instance.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesDjigzoBackupPreferences implements DjigzoBackupPreferences
{
    private final static String BASE_PROP = "backup.";
    private final static String AUTOMATIC_BACKUP_ENABLED = BASE_PROP + "automaticBackupEnabled";
    private final static String CRON_EXPRESSION = BASE_PROP + "cronExpression";
    private final static String PASSWORD = BASE_PROP + "password";
    
    private final HierarchicalProperties properties;
    
    public PropertiesDjigzoBackupPreferences(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }

    @Override
    public boolean isAutomaticBackupEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(properties, AUTOMATIC_BACKUP_ENABLED, false, false);
    }

    @Override
    public void setAutomaticBackupEnabled(boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, AUTOMATIC_BACKUP_ENABLED, enabled, false);
    }
    
    @Override
    public String getCronExpression()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(CRON_EXPRESSION, false);
    }
    
    @Override
    public void setCronExpression(String expression)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(CRON_EXPRESSION, expression, false);
    }
    
    @Override
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PASSWORD, true);
    }

    @Override
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PASSWORD, password, true);
    }
}
