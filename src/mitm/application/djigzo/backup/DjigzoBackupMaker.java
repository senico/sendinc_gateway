/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import mitm.common.backup.BackupBusyException;
import mitm.common.backup.BackupDriver;
import mitm.common.backup.BackupException;
import mitm.common.backup.BackupMaker;
import mitm.common.backup.FilenameStrategy;
import mitm.common.util.Check;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Implementation of BackupMaker that creates a backup of Djigzo by executing an external script. 
 * DjigzoBackupMaker requires a filename strategy and backup driver. 
 * 
 * @author Martijn Brinkers
 *
 */
public class DjigzoBackupMaker implements BackupMaker
{    
    /*
     * The script that will do the actual backup
     */
    private final File backupScript;
 
    /*
     * The backup name prefix
     */
    private final String prefix;

    /*
     * The extension for unencrypted backup
     */
    private final String extension;

    /*
     * The extension for encrypted backup
     */
    private final String extensionEncrypted;
    
    /*
     * The maximum time in milliseconds a command may run after which it is destroyed
     */
    private final int timeout;
    
    /*
     * The strategy for naming of backup files
     */
    private final FilenameStrategy filenameStrategy;
    
    /*
     * Lock object we will use to prevent concurrent access
     */
    private Lock lock = new ReentrantLock();
    
    public DjigzoBackupMaker(File backupScript, String prefix, String extension, 
            String extensionEncrypted, int timeout, FilenameStrategy filenameStrategy)
    {
        Check.notNull(backupScript, "backupScript");
        Check.notNull(prefix, "prefix");
        Check.notNull(extension, "extension");
        Check.notNull(extensionEncrypted, "extensionEncrypted");
        Check.notNull(filenameStrategy, "filenameStrategy");
        
        this.backupScript = backupScript;
        this.prefix = prefix;
        this.extension = extension;
        this.extensionEncrypted = extensionEncrypted;
        this.timeout = timeout;
        this.filenameStrategy = filenameStrategy;
    }
    
    private File createTempFile()
    throws IOException
    {
        return File.createTempFile("djigzo-backup", ".tmp");
    }
    
    @Override
    public String backup(BackupDriver backupDriver, String password)
    throws BackupException
    {
        if (!lock.tryLock()) {
            throw new BackupBusyException("Backup is busy.");
        }
        try {
            File tempBackup = createTempFile();
            
            try {
                DjigzoBackupRunner runner = new DjigzoBackupRunner(backupScript, timeout);
                
                runner.backup(tempBackup, password);
                
                String filename = filenameStrategy.getFilename(prefix);

                filename = filename + (StringUtils.isEmpty(password) ? 
                        extension : extensionEncrypted);

                InputStream backup = new FileInputStream(tempBackup);
                    
                try {
                    backupDriver.writeBackup(filename, backup);
                }
                finally {
                    IOUtils.closeQuietly(backup);
                }
                
                return filename;
            }
            finally {
                FileUtils.deleteQuietly(tempBackup);
            }
        }
        catch(IOException e) {
            throw new BackupException(e);
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void restore(InputStream backup, String password)
    throws BackupException
    {
        if (!lock.tryLock()) {
            throw new BackupBusyException("Backup is busy.");
        }
        try {
            File restore = createTempFile();

            try {                
                IOUtils.copy(backup, new FileOutputStream(restore));
                
                DjigzoBackupRunner runner = new DjigzoBackupRunner(backupScript, timeout);

                runner.restore(restore, password);
            }
            finally {
                FileUtils.deleteQuietly(restore);
            }
        }
        catch(IOException e) {
            throw new BackupException(e);
        }
        finally {
            lock.unlock();
        }
    }
}
