/*
 * Copyright (c) 2009, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.backup;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.common.util.Check;
import mitm.common.util.ProcessRunner;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.util.SizeUtils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;

/**
 * DjigzoBackupRunner is a helper class that runs the external scripts for backup and restore.
 * 
 * @author Martijn Brinkers
 *
 */
public class DjigzoBackupRunner
{
    /*
     * Maximal size of a command output (just to protect against a runaway command)
     */
    protected final static long MAX_OUTPUT_LENGTH = SizeUtils.KB * 10;
    
    /*
     * The backup script that will be executed
     */
    private final File backupScript;
 
    /*
     * The maximum time in milliseconds a command may run after which it is destroyed
     */
    private final int timeout;
    
    public DjigzoBackupRunner(File backupScript, int timeout)
    {
        Check.notNull(backupScript, "backupScript");
        
        this.backupScript = backupScript;
        this.timeout = timeout;
    }
 
    private String getFirstLine(ByteArrayOutputStream bos)
    throws IOException
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        
        LineIterator it = IOUtils.lineIterator(bis, "US-ASCII");
        
        String result = null;
        
        if (it.hasNext()) {
            result = it.nextLine();
        }
        
        return result;
    }
    
    /**
     * Starts the backup script. If password is not blank the backup will be encrypted with the password.
     */
    public void backup(File backupFile, String password)
    throws IOException
    {
        Check.notNull(backupFile, "backupFile");
        
        List<String> cmd = new LinkedList<String>();
        
        cmd.add(backupScript.getPath());
        cmd.add("-b");
        cmd.add("-f");
        cmd.add(backupFile.getAbsolutePath());
        
        if (StringUtils.isNotBlank(password))
        {
            cmd.add("-p");
            cmd.add(password);
        }
        
        ProcessRunner runner = new ProcessRunner();
        
        runner.setTimeout(timeout);
        /*
         * We do not want the password to be logged
         */
        runner.setLogArguments(false);    
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, MAX_OUTPUT_LENGTH, true);
        
        runner.setError(slos);
        
        try {
            runner.run(cmd);
        }
        catch(IOException e)
        {
            /*
             * We would like to report the error message from the external program
             */
            String error = getFirstLine(bos);
            
            if (StringUtils.isNotBlank(error)) {
                throw new IOException(error);
            }
            
            throw e;
        }
    }

    /**
     * Starts the restore script. If password is not blank the backup will be decrypted with the 
     * password before restore.
     */
    public void restore(File backupFile, String password)
    throws IOException
    {
        Check.notNull(backupFile, "backupFile");
        
        List<String> cmd = new LinkedList<String>();
        
        cmd.add(backupScript.getPath());
        cmd.add("-r");
        cmd.add("-f");
        cmd.add(backupFile.getAbsolutePath());
        
        if (StringUtils.isNotBlank(password))
        {
            cmd.add("-p");
            cmd.add(password);
        }
                
        ProcessRunner runner = new ProcessRunner();
        
        runner.setTimeout(timeout);
        /*
         * We do not want the password to be logged
         */
        runner.setLogArguments(false);    
                   
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, MAX_OUTPUT_LENGTH, true);
        
        runner.setError(slos);
        
        try {
            runner.run(cmd);
        }
        catch(IOException e)
        {
            /*
             * We would like to report the error message from the external program
             */
            String error = getFirstLine(bos);
            
            if (StringUtils.isNotBlank(error)) {
                throw new IOException(error);
            }
            
            throw e;
        }
    }
}
