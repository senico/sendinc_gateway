/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import javax.mail.internet.AddressException;

import mitm.common.hibernate.SortDirection;
import mitm.common.util.CloseableIterator;

/**
 * manager for users (users are the email addresses of receivers and senders)
 * 
 * @author Martijn Brinkers
 *
 */
public interface UserManager
{
    enum AddMode {PERSISTENT, NON_PERSISTENT};
    
    /**
     * Add a new user. If makePersistent is false the user is not persisted.
     */
    public User addUser(String email, AddMode addMode)
    throws AddressException;
    
    /**
     * Deletes the user. Returns true if user was deleted.
     */
    public boolean deleteUser(User user);
    
    /**
     * Returns the user with given email. Returns null if user does not exist.
     */
    public User getUser(String email)
    throws AddressException;
    
    /**
     * Returns the number of users.
     */
    public int getUserCount();

    
    /**
     * Returns true if the user is a persistent user.
     */
    public boolean isPersistent(User user);
    
    /**
     * Persists the user.
     */
    public void makePersistent(User user);
    
    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    public CloseableIterator<String> getEmailIterator();

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
    		SortDirection sortDirection);
    
    /**
     * Search for users matching search using LIKE
     */
    public CloseableIterator<String> searchEmail(String search, Integer firstResult, Integer maxResults,
    		SortDirection sortDirection);

    /**
     * Returns the number of users that will be returned by searchEmail.
     */
    public int getSearchEmailCount(String search);
}
