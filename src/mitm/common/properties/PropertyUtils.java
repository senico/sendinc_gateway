/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.util.Properties;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.math.NumberUtils;

/**
 * General Property utils
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertyUtils
{
    /**
     * Returns the property as an int. If propery is not found, or the property is not something that can
     * be represented by an Integer, defaultValue will be returned.
     */
    public static int getIntegerProperty(Properties properties, String name, int defaultValue)
    {
        int result = defaultValue;
        
        Object value = properties.get(name);
        
        if (value == null) {
            value = properties.getProperty(name);
        }
        
        if (value instanceof String) {
            result = NumberUtils.toInt((String) value, defaultValue);
        }
        else if (value instanceof Integer) {
            result = (Integer) value;
        }
        
        return result;
    }

    /**
     * Returns the property as an int. If propery is not found, or the property is not something that can
     * be represented by an Integer, defaultValue will be returned.
     */
    public static int getIntegerSystemProperty(String name, int defaultValue)
    {
        return getIntegerProperty(System.getProperties(), name, defaultValue);
    }
    
    /**
     * Returns the property as a boolean. If propery is not found, or the property is not something that can
     * be represented a boolean, defaultValue will be returned.
     */
    public static boolean getBooleanProperty(Properties properties, String name, boolean defaultValue)
    {
        boolean result = defaultValue;
        
        Object value = properties.get(name);
        
        if (value == null) {
            value = properties.getProperty(name);
        }
        
        if (value instanceof String) {
            result = BooleanUtils.toBoolean((String) value);
        }
        else if (value instanceof Boolean) {
            result = (Boolean) value;
        }
        
        return result;
    }
    
    /**
     * Returns the property as a boolean. If propery is not found, or the property is not something that can
     * be represented a boolean, defaultValue will be returned.
     */
    public static boolean getBooleanSystemProperty(String name, boolean defaultValue)
    {
        return getBooleanProperty(System.getProperties(), name, defaultValue);
    }
}
