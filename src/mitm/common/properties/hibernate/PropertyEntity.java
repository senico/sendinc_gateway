/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties.hibernate;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.MapKey;
import org.hibernate.annotations.Type;

@Entity(name = PropertyEntity.ENTITY_NAME)
@Table(name = "properties")
public class PropertyEntity implements Serializable
{
    public static final String ENTITY_NAME = "properties";

    private static final long serialVersionUID = -8836507664086059966L;

    /*
     * Use maximum length of an email address as max length
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_CATEGORY_LENGTH = 64 + 1 + 255;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The name will be used to identify multiple property objects.
     */
    @Column (name = "category", unique = true, nullable = true, length = MAX_CATEGORY_LENGTH)
    private String category;

    @CollectionOfElements
    @Column(name = "value")
    @MapKey(columns = {@Column(name = "name")})
    @Type(type="text")
    private Map<String, String> nameValues = new HashMap<String, String>();

    protected PropertyEntity() {
        /* Hibernate requires a default constructor */
    }

    public PropertyEntity(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Map<String, String> getNameValues() {
        return nameValues;
    }

    @Override
    public String toString() {
        return getCategory();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PropertyEntity)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        PropertyEntity rhs = (PropertyEntity) obj;

        return new EqualsBuilder()
            .append(category, rhs.category)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(category)
            .toHashCode();
    }
}
