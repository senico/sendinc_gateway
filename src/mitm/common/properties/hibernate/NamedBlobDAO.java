/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties.hibernate;

import java.util.List;

import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapter;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class NamedBlobDAO extends GenericHibernateDAO<NamedBlobEntity, Long>
{
    private String entityName = NamedBlobEntity.ENTITY_NAME;
    
    public NamedBlobDAO(SessionAdapter session)
    {
        super(session);
    }

    public NamedBlobEntity getNamedBlob(String category, String name)
    {
        Criteria criteria = createCriteria(entityName);
        
        criteria.add(Restrictions.eq(NamedBlobEntity.CATEGORY_COLUMN, category));
        criteria.add(Restrictions.eq(NamedBlobEntity.NAME_COLUMN, name));
        
        return (NamedBlobEntity) criteria.uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<NamedBlobEntity> getByCategory(String category, Integer firstResult, Integer maxResults) 
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq(NamedBlobEntity.CATEGORY_COLUMN, category));
        
        criteria.addOrder(Order.asc(NamedBlobEntity.NAME_COLUMN));
        
        if (firstResult != null) {
            criteria.setFirstResult(firstResult);
        }
        
        if (maxResults != null) {
            criteria.setMaxResults(maxResults);
        }
        
        return criteria.list();
    }

    public int getByCategoryCount(String category)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq(NamedBlobEntity.CATEGORY_COLUMN, category));

        criteria.setProjection(Projections.rowCount());
        
        return (Integer) criteria.uniqueResult();        
    }
    
    /*
     * A NamedBlobEntity can reference other NamedBlobEntity's. This method returns the number
     * of times the NamedBlobEntity is referenced by other NamedBlobEntity's.
     */
    public long getReferencedByCount(NamedBlobEntity entity)
    {
        /*
         * Raw SQL is used because it's much faster than using HQL
         * 
         * Note: the names used in the query can change if the generated SQL changes because the entities involve
         * changes
         */
        String sql = "select count(*) from named_blob_named_blob n where n.namedblobs_id= :otherid";

        Query query = createSQLQuery(sql);
                
        query.setParameter("otherid", entity.getId());
        
        return toLong(query.uniqueResult());        
    }
    
    /*
     * A NamedBlobEntity can reference other NamedBlobEntity's. This method returns all the NamedBlobEntity's
     * that references the NamedBlobEntity.
     */
    @SuppressWarnings("unchecked")
    public List<NamedBlobEntity> getReferencedBy(NamedBlobEntity entity, Integer firstResult,  Integer maxResults)
    {
        String hql = "select u from " + entityName + " u where :otherid in (select c.id from u.namedBlobs c)";

        Query query = createQuery(hql);
        
        query.setParameter("otherid", entity.getId());
        
        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }
        
        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }
        
        return query.list();
    }
    
    public void deleteNamedBlob(String category, String name)
    {
        NamedBlobEntity entity = getNamedBlob(category, name);
        
        if (entity != null) {
            delete(entity);
        }
    }
    
    public void deleteAllByCategory(String category) 
    {
        List<NamedBlobEntity> blobs = getByCategory(category, null, null);

        for (NamedBlobEntity blob : blobs) {
            delete(blob);
        }
    }
    
    public void deleteAll() 
    {
        List<NamedBlobEntity> blobs = findAll();

        for (NamedBlobEntity blob : blobs) {
            delete(blob);
        }
    }
}
