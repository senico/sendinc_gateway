/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties.hibernate;

import java.util.Collections;
import java.util.List;

import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.NamedBlobManager;
import mitm.common.util.Check;

/**
 * Implementation of NamedBlobManager.
 * 
 * @author Martijn Brinkers
 *
 */
public class NamedBlobManagerImpl implements NamedBlobManager
{
    /*
     * Manages database sessions
     */
    private final SessionManager sessionManager;
    
    public NamedBlobManagerImpl(SessionManager sessionManager)
    {
        Check.notNull(sessionManager, "sessionManager");
        
        this.sessionManager = sessionManager;
    }

    @Override
    public NamedBlob createNamedBlob(String category, String name)
    {
        NamedBlobEntity entity = new NamedBlobEntity();
        
        entity.setCategory(category);
        entity.setName(name);
        
        getDAO().makePersistent(entity);
        
        return entity;
    }

    @Override
    public void deleteNamedBlob(String category, String name) {
        getDAO().deleteNamedBlob(category, name);
    }

    @Override
    public List<? extends NamedBlob> getByCategory(String category, Integer firstResult, Integer maxResults)
    {
        return Collections.unmodifiableList(getDAO().getByCategory(category, firstResult, maxResults));
    }

    @Override
    public int getByCategoryCount(String category) {
        return getDAO().getByCategoryCount(category);
    }

    @Override
    public NamedBlob getNamedBlob(String category, String name) {
        return getDAO().getNamedBlob(category, name);
    }
    
    @Override
    public void deleteAll() {
        getDAO().deleteAll();
    }

    @Override
    public long getReferencedByCount(NamedBlob namedBlob)
    {
        if (!(namedBlob instanceof NamedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }
        
        return getDAO().getReferencedByCount((NamedBlobEntity) namedBlob);
    }
    
    @Override
    public List<? extends NamedBlob> getReferencedBy(NamedBlob namedBlob, Integer firstResult,  Integer maxResults)
    {
        if (!(namedBlob instanceof NamedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }
        
        return getDAO().getReferencedBy((NamedBlobEntity) namedBlob, firstResult, maxResults);
    }
    
    private NamedBlobDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new NamedBlobDAO(session);
    }    
}
