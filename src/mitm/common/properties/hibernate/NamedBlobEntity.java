/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties.hibernate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mitm.common.properties.NamedBlob;
import mitm.common.util.SetBridge;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity(name = NamedBlobEntity.ENTITY_NAME)
@Table(name = "named_blob",
        uniqueConstraints = {@UniqueConstraint(columnNames={NamedBlobEntity.CATEGORY_COLUMN, 
                NamedBlobEntity.NAME_COLUMN})}
)
public class NamedBlobEntity implements NamedBlob
{
    static final String CATEGORY_COLUMN = "category";
    static final String NAME_COLUMN = "name";
    static final String ENTITY_NAME = "NamedBlob";
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /*
     * The name will be used to identify multiple named blobs.
     */
    @Column (name = "category", nullable = false)
    private String category;
    
    /*
     * The name will be used to identify this named blob.
     */
    @Column (name = "name", nullable = false)
    private String name;
    
    /*
     * The mime message is stored as an entity to allow lazy loading (a byte[] cannot be lazy loaded). Lazy 
     * loading is required to support large binary blobs. If not lazy loaded, getting a list of 
     * NamedBlobEntity's will result in an Out of Memory.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.ALL})
    private BlobEntity blobEntity;
    
    @ManyToMany
    @Cascade(CascadeType.SAVE_UPDATE)
    private Set<NamedBlobEntity> namedBlobs = new HashSet<NamedBlobEntity>();
    
    public NamedBlobEntity() {
        // required by Hibernate
    }
    
    public NamedBlobEntity(String category) {
        this.category = category;
    }
    
    public Long getId() {
        return id;
    }

    @Override
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public byte[] getBlob() {
        return blobEntity != null ? blobEntity.getBlob() : null;
    }

    @Override
    public void setBlob(byte[] blob)
    {
        if (blobEntity == null) {
            blobEntity = new BlobEntity();
        }
        
        blobEntity.setBlob(blob);
    }

    @Override
    public Set<NamedBlob> getNamedBlobs() {
        return new SetBridge<NamedBlob, NamedBlobEntity>(namedBlobs, NamedBlobEntity.class);
    }

    @Override
    public String toString() {
        return getCategory() + ":" + getName();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof NamedBlobEntity)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        NamedBlobEntity rhs = (NamedBlobEntity) obj;
        
        return new EqualsBuilder()
            .append(category, rhs.category)
            .append(name, rhs.name)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(category)
            .append(name)
            .toHashCode();    
    }
}
