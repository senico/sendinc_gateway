/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.util.Collections;
import java.util.Set;

import mitm.common.util.Check;

public class DelegatedHierarchicalProperties implements HierarchicalProperties
{
    /*
     * The properties to delegate to
     */
    private final HierarchicalProperties source;
    
    /*
     * The parent properties to delegate to if the property is not found
     * on the source properties (can be null)
     */    
    private final HierarchicalProperties parent;

    public DelegatedHierarchicalProperties(HierarchicalProperties source, HierarchicalProperties parent)
    {
        Check.notNull(source, "source");
        
        this.source = source;
        this.parent = parent;
    }
    
    @Override
    public void deleteAll() 
    throws HierarchicalPropertiesException 
    {
        source.deleteAll();
    }

    @Override
    public void deleteProperty(String propertyName) 
    throws HierarchicalPropertiesException 
    {
        source.deleteProperty(propertyName);
    }

    @Override
    public String getCategory() {
        return source.getCategory();
    }

    public HierarchicalProperties getParent() 
    {
        return parent;
    }
    
    private String internalGetProperty(String propertyName, boolean checkParent, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        String value = source.getProperty(propertyName, decrypt);
        
        if (value == null && parent != null && checkParent) 
        {
            value = parent.getProperty(propertyName, decrypt);
        }
        
        return value;
    }

    @Override
    public String getProperty(String propertyName, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        return internalGetProperty(propertyName, true /* check parent */, decrypt);
    }
    
    @Override
    public void setProperty(String propertyName, String value, boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        source.setProperty(propertyName, value, encrypt);
    }
    
    @Override
    public boolean isInherited(String propertyName) 
    throws HierarchicalPropertiesException 
    {
    	return source.isInherited(propertyName);
    }
    
    @Override
    public Set<String> getProperyNames(boolean recursive) 
    throws HierarchicalPropertiesException
    {
        Set<String> names = source.getProperyNames(recursive);
        
        if (recursive && parent != null) {        
            names.addAll(parent.getProperyNames(recursive));
        }
        
        return Collections.unmodifiableSet(names);
    }    
}
