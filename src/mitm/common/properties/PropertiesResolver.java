/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import mitm.common.locale.CharacterEncoding;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helpers for resolving special property values. For example a property value that starts with file: will be
 * replaced by the file content
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesResolver
{
	private final static Logger logger = LoggerFactory.getLogger(PropertiesResolver.class);

	/*
	 * The separator that separates the schema and the path
	 */
    private static final String SCHEMA_SEPARATOR = "://";
	
    /*
     * The file schema name
     */
    private static final String FILE_SCHEMA = "file";
    
    /*
     * The literal schema name
     */
    private static final String LITERAL_SCHEMA = "literal";
    
	/*
	 * The base directory used when resolving files
	 */
	private File baseDir = null;
		
	public static class PropertiesResolverException extends Exception
	{
		private static final long serialVersionUID = -3728234973753818342L;

		public PropertiesResolverException(String message) {
	        super(message);
	    }

	    public PropertiesResolverException(Throwable cause) {
	        super(cause);
	    }

	    public PropertiesResolverException(String message, Throwable cause) {
	        super(message, cause);
	    }
	}
	
	/**
	 * Resolves special property value types (for example a property that starts with file:// is converted the 
	 * the actual file content).
	 * 
	 * Currently only file: binding is supported
	 */
	public void resolve(Properties properties)
	throws PropertiesResolverException
	{
		Enumeration<?> propertyNamesEnum = properties.propertyNames();
		
		while (propertyNamesEnum.hasMoreElements())
		{
			Object obj = propertyNamesEnum.nextElement();
			
			if (!(obj instanceof String)) {
				continue;
			}
			
			String key = (String) obj;
			
			String value = properties.getProperty(key);
			
			if (value == null) {
				continue;
			}
			
			int index = value.indexOf(SCHEMA_SEPARATOR);

			if (index == -1) {
				continue;
			}
			
			String binding = StringUtils.substring(value, 0, index).trim();
			String toResolve = StringUtils.substring(value, index + SCHEMA_SEPARATOR.length());
			
			if (FILE_SCHEMA.equalsIgnoreCase(binding)) 
			{
				File file = new File(baseDir, toResolve);
				
				try {
				    String resolved = FileUtils.readFileToString(file, CharacterEncoding.UTF_8);
					
					properties.setProperty(key, resolved);
				} 
				catch (IOException e)
				{
					String canonicalPath = toResolve;
					
					try {
						canonicalPath = file.getCanonicalPath();
					}
					catch(IOException ioe) {
						logger.error("Error getting canonical path.", ioe);
					}
					
					throw new PropertiesResolverException("Could not resolve file " + canonicalPath, e);
				}
			}
			else if (LITERAL_SCHEMA.equalsIgnoreCase(binding)) {
			    /*
			     * with literal binding the value after the binding should be taken
			     * literal.
			     */
                properties.setProperty(key, toResolve);
			}
			else {
				throw new PropertiesResolverException("Unknown binding " + binding);
			}
		}
	}
}
