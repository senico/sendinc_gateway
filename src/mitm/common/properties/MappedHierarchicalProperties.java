/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;

public class MappedHierarchicalProperties implements HierarchicalProperties 
{
    private final String category;
    private final HierarchicalProperties parent;
    private final Map<String, String> propertyMap;
    private final Encryptor encryptor;
    
    public MappedHierarchicalProperties(String category, HierarchicalProperties parent, 
    		Map<String, String> propertyMap, Encryptor encryptor)
    {
    	Check.notNull(propertyMap, "propertyMap");
    	
        this.category = category;
        this.parent = parent;
        this.propertyMap = propertyMap;
        this.encryptor = encryptor;
    }

    @Override
    public void deleteAll() {
        propertyMap.clear();
    }

    @Override
    public void deleteProperty(String propertyName) {
        propertyMap.remove(propertyName);
    }

    @Override
    public String getCategory() {
        return category;
    }

    public HierarchicalProperties getParent() 
    {
        return parent;
    }
    
    private String internalGetProperty(String propertyName, boolean checkParent, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        String value = propertyMap.get(propertyName);
        
        if (value == null && parent != null && checkParent) 
        {
            value = parent.getProperty(propertyName, decrypt);
        }
        else {
        	if (value != null && decrypt) 
        	{
        		if (encryptor == null) {
        			throw new HierarchicalPropertiesException("decrypt is true but encryptor is null.");
        		}

        		try {
					value = MiscStringUtils.toAsciiString(encryptor.decryptBase64(value));
				} 
        		catch (EncryptorException e) {
        			throw new HierarchicalPropertiesException(e);
				}
        	}
        }
        
        return value;
    }

    @Override
    public String getProperty(String propertyName, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        return internalGetProperty(propertyName, true /* check parent */, decrypt);
    }
    
    @Override
    public void setProperty(String propertyName, String value, boolean encrypt)
    throws HierarchicalPropertiesException
    {
    	if (value != null && encrypt) 
    	{
    		if (encryptor == null) {
    			throw new HierarchicalPropertiesException("encrypt is true but encryptor is null.");
    		}

    		try {
				value = encryptor.encryptBase64(MiscStringUtils.toAsciiBytes(value));
			} 
    		catch (EncryptorException e) {
    			throw new HierarchicalPropertiesException(e);
			}
    	}
    	
        propertyMap.put(propertyName, value);
    }
    
    @Override
    public boolean isInherited(String propertyName) 
    throws HierarchicalPropertiesException 
    {
        String value = internalGetProperty(propertyName, false /* do not check parent */, 
        		false /* no decrypt */);
        
        if (value != null) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public Set<String> getProperyNames(boolean recursive) 
    throws HierarchicalPropertiesException
    {
        Set<String> names = new HashSet<String>();
        
        names.addAll(propertyMap.keySet());
        
        if (recursive && parent != null) {        
            names.addAll(parent.getProperyNames(recursive));
        }
        
        return Collections.unmodifiableSet(names);
    }
}
