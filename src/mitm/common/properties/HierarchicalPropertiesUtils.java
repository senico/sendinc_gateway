/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper functions for setting and getting properties from HierarchicalProperties.
 * 
 * @author Martijn Brinkers
 *
 */
public class HierarchicalPropertiesUtils
{
    private final static Logger logger = LoggerFactory.getLogger(HierarchicalPropertiesUtils.class);
    
    /**
     * Copies all properties from source to target.
     */
    public static void copyProperties(HierarchicalProperties source, HierarchicalProperties target) 
    throws HierarchicalPropertiesException 
    {
        copyProperties(source, target, null);
    }

    /**
     * Copies all properties from source to target. Property names are matched against skipPattern and if they match
     * the property will be skipped (ie not copied).
     */
    public static void copyProperties(HierarchicalProperties source, HierarchicalProperties target, Pattern skipPattern) 
    throws HierarchicalPropertiesException
    {
        if (source == null) {
            return;
        }
        
        Collection<String> names = source.getProperyNames(true);
        
        for (String name : names)
        {
            Matcher matcher = null;
            
            if (skipPattern != null) {
                matcher = skipPattern.matcher(name);
            }
            
            if (matcher == null || !matcher.matches())
            {
                String value = source.getProperty(name, false);
                
                target.setProperty(name, value, false);
            }
        }
    }    
    
    /**
     * Returns the property with propertyName from properties if it exists. Returns defaultValue if property 
     * does not exist.
     */
    public static String getProperty(HierarchicalProperties properties, String propertyName, 
    		String defaultValue, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        String value = defaultValue;

        String property = properties.getProperty(propertyName, decrypt);

        if (property != null) {
            value = property;
        }
        
        return value;        
    }
    
    /**
     * Sets the boolean property named propertyName.
     */
    public static void setBoolean(HierarchicalProperties properties, String propertyName, 
    		Boolean value, boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        properties.setProperty(propertyName,  value != null ? value.toString() : null, encrypt);
    }
    
    /**
     * Returns the Boolean property with propertyName from properties if it exists, true if property equals "true", false if
     * not equals to "true". Returns defaultValue if property does not exist.
     */
    public static Boolean getBoolean(HierarchicalProperties properties, String propertyName, 
    		Boolean defaultValue, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        Boolean value = defaultValue;

        String property = properties.getProperty(propertyName, decrypt);

        if (property != null) {
            value = Boolean.valueOf(property);
        }
        
        return value;        
    }
    
    /**
     * Sets the integer property named propertyName.
     */
    public static void setInteger(HierarchicalProperties properties, String propertyName, Integer value,
    		boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        properties.setProperty(propertyName, value != null ? value.toString() : null, encrypt);
    }
    
    /**
     * Returns the Integer property with propertyName from properties if it exists and is a valid integer. 
     * Returns defaultValue if property does not exist or is not a valid integer.
     */
    public static Integer getInteger(HierarchicalProperties properties, String propertyName, 
    		Integer defaultValue, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        Integer value = defaultValue;

        String property = properties.getProperty(propertyName, decrypt);

        if (property != null)
        {
            try {
                value = Integer.parseInt(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property " + propertyName + " is not a number: " + property);
            }
        }
        
        return value;
    }

    /**
     * Sets the long property named propertyName.
     */
    public static void setLong(HierarchicalProperties properties, String propertyName, Long value,
    		boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        properties.setProperty(propertyName, value != null ? value.toString() : null, encrypt);
    }
    
    /**
     * Returns the Long property with propertyName from properties if it exists and is a valid long. 
     * Returns defaultValue if property does not exist or is not a valid long.
     * @throws HierarchicalPropertiesException 
     */
    public static Long getLong(HierarchicalProperties properties, String propertyName, Long defaultValue,
    		boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        Long value = defaultValue;

        String property = properties.getProperty(propertyName, decrypt);

        if (property != null)
        {
            try {
                value = Long.parseLong(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property " + propertyName + " is not a number: " + property);
            }
        }
        
        return value;
    }
    
    /**
     * Sets the binary property named propertyName. The byte[] is base64 encoded.
     * @throws HierarchicalPropertiesException 
     */
    public static void setBinary(HierarchicalProperties properties, String propertyName, byte[] value,
    		boolean encrypt) 
    throws HierarchicalPropertiesException
    {
    	String base64 = null;
    	
    	if (value != null) {
    		base64 = MiscStringUtils.toAsciiString(Base64.encodeBase64Chunked(value));
    	}
        
        properties.setProperty(propertyName, base64, encrypt);
    }
    
    /**
     * Returns the binary property with propertyName from properties if it exists. The binary property should be base64 encoded 
     * String. Returns defaultValue if property does not exist.
     */
    public static byte[] getBinary(HierarchicalProperties properties, String propertyName, 
    		byte[] defaultValue, boolean decrypt) 
    throws HierarchicalPropertiesException
    {
        byte[] value = defaultValue;

        String property = properties.getProperty(propertyName, decrypt);

        if (property != null) {
            value = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(property));
        }
        
        return value;
    }    
}
