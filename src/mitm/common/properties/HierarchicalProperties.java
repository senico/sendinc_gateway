/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import java.util.Set;

public interface HierarchicalProperties
{
    /**
     * Returns the name of this property object.
     */
    public String getCategory();
    
    /**
     * Returns a property value with the given name. If there is no property with the given name on
     * this properties object the parent is checked and then the parent of the parent etc. If there
     * is no property found null is returned. If the property is encrypted decrypt should be true
     * if the caller wants the property to be decrypted.
     */
    public String getProperty(String propertyName, boolean decrypt)
    throws HierarchicalPropertiesException;

    /**
     * Sets the property. If the caller wants to property to be encrypted encrypt should be true.
     */
    public void setProperty(String propertyName, String value, boolean encrypt)
    throws HierarchicalPropertiesException;
    
    /**
     * Deletes the property with the given name. If the property does not exist on this object
     * nothing happens. The parent property is not removed.
     */
    public void deleteProperty(String propertyName)
    throws HierarchicalPropertiesException;
    
    /**
     * Deletes all properties of this property object. The parent properties are not deleted.
     */
    public void deleteAll()
    throws HierarchicalPropertiesException;
    
    /**
     * Returns true if this properties does not have the property. The parent is not checked ie
     * the property should be from the top level properties object (the parent can have the property
     * or not that makes not difference because it always returns true).
     */
    public boolean isInherited(String propertyName)
    throws HierarchicalPropertiesException;
    
    /**
     * Returns an unmodifiable Set of all the property names. If recursive is true the property names
     * of the parent are returned as well.
     */
    public Set<String> getProperyNames(boolean recursive)
    throws HierarchicalPropertiesException;
}
