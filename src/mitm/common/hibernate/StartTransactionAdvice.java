/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import mitm.common.util.Check;

import org.aspectj.lang.ProceedingJoinPoint;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for Spring AOP advice for auto transactions. Extensions should add the @Aspect 
 * and @Around annotations.
 * 
 * @author Martijn Brinkers
 *
 */
public class StartTransactionAdvice
{
    private final static Logger logger = LoggerFactory.getLogger(StartTransactionAdvice.class);
    
    private final SessionManager sessionManager;
    
    public StartTransactionAdvice(SessionManager sessionManager)
    {
    	Check.notNull(sessionManager, "sessionManager");
        
        this.sessionManager = sessionManager;
    }
    
    public Object startTransaction(ProceedingJoinPoint pjp) 
    throws Throwable
    {
        Session session = sessionManager.newSession();
        
        Transaction tx = session.beginTransaction();
        
        try {
            Object result = null;
            
            Session savedSession = sessionManager.getSession();
            
            sessionManager.setSession(session);
            
            try {
                result = pjp.proceed();
            }
            finally {
                /* restore the saved session */
            	sessionManager.setSession(savedSession);
            }

            tx.commit();
            
            sessionManager.closeSession(session);
            
            return result;
        }
        catch(Exception e) {
            try {
                if (tx.isActive()) {
                    tx.rollback();
                }
            }
            catch(Exception rbe) {
                logger.warn("Exception rolling back transaction.", rbe);
            }

            try {
            	sessionManager.closeSession(session);
            }
            catch(Exception rbe) {
                logger.warn("Exception closing session.", rbe);
            }
            
            throw e;            
        }
    }
}
