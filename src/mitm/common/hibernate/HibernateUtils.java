/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import org.hibernate.SQLQuery;
import org.hibernate.StatelessSession;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateUtils
{
	private final static Logger logger = LoggerFactory.getLogger(HibernateUtils.class);
	
    /**
     * Drops the tables and re-creates the tables. Use with care because all data will be gone.
     * 
     * Note 1: it appears that with PostgreSQL you have to make sure that statements are not cached
     * otherwise 'ERROR: relation with OID  does not exist' errors will be thrown by PostgeSQL.
     * 
     * Note 2: it seems that indices are not recreated. See: http://www.hibernate.org/119.html#A10
     */
    public static void recreateTables(Configuration hibernateConfiguration) 
    {
        new SchemaExport(hibernateConfiguration).drop(true, true);        
        new SchemaUpdate(hibernateConfiguration).execute(true, true);        
    }
    
    /**
     * Returns true if the database of the sessionSource is active.
     */
    public static boolean isDatabaseActive(SessionManager sessionManager, String testQuery)
    {
    	boolean active = false;
    	
    	try {
        	StatelessSession session = null;

        	try {
    			session = sessionManager.openStatelessSession();
    			
    			SQLQuery query = session.createSQLQuery(testQuery);
    			
    			query.list();
    		}
    		finally {
    			session.close();
    		}
    		
    		active = true;
    	}
    	catch(RuntimeException e) {
    		logger.warn("Unable to open a stateless session. Database is probably not active.");
    	}
    	
    	return active;
    }
}
