/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 * {@link SessionAdapter} implementation for a Hibernate {@link StatelessSession}.
 *  
 * @author Martijn Brinkers
 *
 */
public class StatelessSessionAdapterImpl implements SessionAdapter 
{
    private final StatelessSession session;
    
    public StatelessSessionAdapterImpl(StatelessSession session) {
        this.session = session;
    }
    @Override
    public Object get(Class<?> clazz, Serializable serializable)
    throws HibernateException
    {
        return session.get(clazz, serializable);
    }
    
    @Override
    public Object get(Class<?> clazz, Serializable serializable, LockMode lockmode)
    throws HibernateException
    {
        return session.get(clazz, serializable, lockmode);
    }
    
    @Override
    public Criteria createCriteria(Class<?> clazz)
    {
        return session.createCriteria(clazz);
    }

    @Override
    public Criteria createCriteria(Class<?> clazz, String alias)
    {
        return session.createCriteria(clazz, alias);
    }

    @Override
    public Query createQuery(String query)
    throws HibernateException
    {
        return session.createQuery(query);
    }
    
    @Override
    public SQLQuery createSQLQuery(String query)
    throws HibernateException
    {
        return session.createSQLQuery(query);
    }

    @Override
    public void saveOrUpdate(Object entity)
    throws HibernateException
    {
        throw new HibernateException("saveOrUpdate is not supported by StatelessSession.");
    }
    
    @Override
    public void delete(Object entity)
    throws HibernateException
    {
        session.delete(entity);
    }

    @Override
    public void evict(Object entity)
    throws HibernateException
    {
        /* evict is not supported by StatelessSession. */        
    }
    
    @Override
    public void flush()
    throws HibernateException
    {
        /* flush is not supported by StatelessSession. */
    }

    @Override
    public void clear()
    throws HibernateException
    {
        /* clear is not supported by StatelessSession. */
    }
    
    @Override
    public void close()
    throws HibernateException
    {
        session.close();
    }
    
    @Override
    public Transaction beginTransaction()
    {
        return session.beginTransaction();
    }    
}
