/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;

/**
 * Wraps a {@link CloseableIterator} to make sure that the transaction is committed and the session is closed
 * when the iterator is closed.
 * 
 * @author Martijn Brinkers
 *
 * @param <T>
 */
public class CommitOnCloseIterator<T> implements CloseableIterator<T>
{
    private final static Logger logger = LoggerFactory.getLogger(CommitOnCloseIterator.class); 
    
    private final CloseableIterator<T> childIterator;
    private final SessionAdapter session;
    private final Transaction transaction;
    
    private boolean closed;
    
    public CommitOnCloseIterator(CloseableIterator<T> parentIterator, SessionAdapter session, Transaction transaction)
    {
        if (!transaction.isActive()) {
            throw new IllegalArgumentException("Transaction is not active.");
        }

        this.childIterator = parentIterator;
        this.session = session;
        this.transaction = transaction;
    }
    
    @Override
    public boolean hasNext()
    throws CloseableIteratorException
    {
        boolean hasNext = childIterator.hasNext();

        /*
         * childIterator.next() can result in closing of the child iterator. If that happens we also
         * need to close this iterator.
         */
        if (childIterator.isClosed()) {
            this.close();
        }
        
        return hasNext;
    }

    @Override
    public T next()
    throws CloseableIteratorException
    {
        T next = childIterator.next();
        
        /*
         * childIterator.next() can result in closing of the child iterator. If that happens we also
         * need to close this iterator.
         */
        if (childIterator.isClosed()) {
            this.close();
        }
        
        return next;
    }
    
    @Override
    public void close()
    throws CloseableIteratorException
    {
        if (!closed)
        {            
            logger.debug("Closing iterator.");
         
            closed = true;
            
            try {
                childIterator.close();
                
                transaction.commit();
            }
            catch(Exception e) {
                transaction.rollback();
                
                if (!(e instanceof CloseableIteratorException)) {
                    throw new CloseableIteratorException(e);
                }
                else {
                    throw (CloseableIteratorException) e;
                }
            }
            finally {
                session.close();
            }
        }        
    }    
    
    @Override
    public boolean isClosed()
    throws CloseableIteratorException
    {
        boolean childClosed = childIterator.isClosed();
        
        if (childClosed && !closed) 
        {
            logger.warn("Child is closed while this iterator is not closed. Closing this iterator.");
            close();
        }
        
        return childClosed;
    }    
}
