package mitm.common.hibernate;

public interface DatabaseActionRetryEvent
{
	public void onRetry();
}
