/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javassist.util.proxy.RuntimeSupport;
import mitm.common.hibernate.annotations.InjectHibernateSession;
import mitm.common.hibernate.annotations.InjectHibernateSessionSource;
import mitm.common.reflection.ReflectionRuntimeException;

import org.hibernate.Session;

public class HibernateSessionUtils
{
    public static void setSession(Object target, Session session) 
    {
        boolean sessionSet = false;

        if (target instanceof HibernateSessionListener) 
        {
            ((HibernateSessionListener) target).setSession(session);
            
            sessionSet = true;
        }
        else {
            Method[] methods = target.getClass().getMethods();
            
            for (Method method : methods)
            {
                if (method.isAnnotationPresent(InjectHibernateSession.class)) 
                {
                    String parameterDescriptor = RuntimeSupport.makeDescriptor(method);
                    
                    if (parameterDescriptor.equals("(Lorg/hibernate/Session;)V")) {
                        throw new ReflectionRuntimeException("@InjectHibernateSession method does not have the correct parameters.");                    
                    }
                    
                    try {
                        method.invoke(target, new Object[] {session});
                        
                        sessionSet = true;
                    }
                    catch (IllegalArgumentException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }
                    catch (IllegalAccessException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }
                    catch (InvocationTargetException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }                
                }
            }
        }

        if (!sessionSet) {
            throw new IllegalArgumentException("The Session could not be set because no suitable listener was found.");
        }
    }
    
    public static void setSessionSource(Object target, HibernateSessionSource sessionSource) 
    {
        boolean sessionSourceSet = false;
        
        if (target instanceof HibernateSessionListener) 
        {
            ((HibernateSessionListener) target).setSessionSource(sessionSource);
            
            sessionSourceSet = true;
        }
        else {
            Method[] methods = target.getClass().getMethods();
            
            for (Method method : methods)
            {
                if (method.isAnnotationPresent(InjectHibernateSessionSource.class)) 
                {
                    String parameterDescriptor = RuntimeSupport.makeDescriptor(method);
                    
                    if (!parameterDescriptor.equals("(Lmitm/common/hibernate/HibernateSessionSource;)V")) {
                        throw new ReflectionRuntimeException("@InjectHibernateSessionSource method does not have the correct parameters.");                    
                    }

                    try {
                        method.invoke(target, new Object[] {sessionSource});
                        
                        sessionSourceSet = true;
                    }
                    catch (IllegalArgumentException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }
                    catch (IllegalAccessException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }
                    catch (InvocationTargetException e) {
                        throw new ReflectionRuntimeException(e);                    
                    }                
                }
            }
        }
        
        if (!sessionSourceSet) {
            throw new IllegalArgumentException("The SessionSource could not be set because no suitable listener was found.");
        }
    }
}
