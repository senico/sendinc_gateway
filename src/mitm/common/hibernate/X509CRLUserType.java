/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.crl.X509CRLInspector;
import mitm.common.util.BigIntegerUtils;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

/**
 * Hibernate CompositeUserType that can be used to persist {@link X509CRL}.
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CRLUserType implements CompositeUserType
{
    private static final int ISSUER_COLUMN      = 0;
    private static final int NEXT_UPDATE_COLUMN = 1;
    private static final int THIS_UPDATE_COLUMN = 2;
    private static final int CRL_NUMBER_COLUMN  = 3;
    private static final int CRL_COLUMN         = 4;
    private static final int THUMBPRINT_COLUMN  = 5;

    /*
     * Subject and Issuer DN will be stored as CLOB because
     * there is no maximum size for a DN
     */
    private static final Type[] PROPERTY_TYPES = {
        Hibernate.TEXT,      /* issuer */
        Hibernate.TIMESTAMP, /* nextUpdate */
        Hibernate.TIMESTAMP, /* thisUpdate */
        Hibernate.STRING,    /* crlNumber */
        Hibernate.BINARY,    /* crl */
        Hibernate.STRING,    /* thumbprint */
    };
    
    private static final String[] PROPERTY_NAMES = {
        "issuer",
        "nextUpdate",
        "thisUpdate",
        "crlNumber",
        "crl",
        "thumbprint"
    };

    @Override
    public Object assemble(Serializable cached, SessionImplementor session, Object owner) 
    throws HibernateException 
    {
        return deepCopy(cached);
    }

    @Override
    public Object deepCopy(Object value) 
    throws HibernateException 
    {
        if (value == null) {
            return null;
        }

        if (!(value instanceof X509CRL)) {
            throw new IllegalArgumentException("Object is not a X509CRL.");
        }

        X509CRL original = (X509CRL) value;

        X509CRL copy = null;

        try {
            byte[] encodedCRL = original.getEncoded();

            CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory("X.509");

            copy = (X509CRL) factory.generateCRL(new ByteArrayInputStream(encodedCRL));
        }
        catch (CertificateException e) {
            throw new HibernateException(e);
        }
        catch (CRLException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchProviderException e) {
            throw new HibernateException(e);
        }

        return copy;
    }    

    @Override
    public Serializable disassemble(Object value, SessionImplementor arg1)
    throws HibernateException 
    {
        return (Serializable) deepCopy(value);
    }

    @Override
    public boolean equals(Object o1, Object o2) 
    throws HibernateException 
    {
        if (o1 == o2) {
            return true;
        }
        
        if ((o1 == null) || (o2 == null)) {
            return false;
        }
        
        /* fall back to X509CRL.equals */
        return o1.equals(o2);
    }

    @Override
    public int hashCode(Object object) 
    throws HibernateException 
    {
        return object.hashCode();
    }

    @Override
    public String[] getPropertyNames() {
        return PROPERTY_NAMES;
    }

    @Override
    public Type[] getPropertyTypes() {
        return PROPERTY_TYPES;
    }
    
    @Override
    public Object getPropertyValue(Object component, int property)
    throws HibernateException 
    {
        if (component == null) {
            return null;
        }

        if (!(component instanceof X509CRL)) {
            throw new IllegalArgumentException("Object is not a X509CRL.");
        }
        
        X509CRL crl = (X509CRL) component;

        try {
            byte[] encodedCRL = null;
            
            /*
             * Encoding the CRL can be time and memory consuming. We will therefore 
             * encode it once and use it for thumbprint as well
             */
            if (property == CRL_COLUMN || property == THUMBPRINT_COLUMN) {
                encodedCRL = crl.getEncoded();
            }
            
            switch(property) {
            case ISSUER_COLUMN      : return X509CRLInspector.getIssuerCanonical(crl);
            case NEXT_UPDATE_COLUMN : return crl.getNextUpdate();
            case THIS_UPDATE_COLUMN : return crl.getThisUpdate();
            case CRL_NUMBER_COLUMN  : return BigIntegerUtils.hexEncode(X509CRLInspector.getCRLNumber(crl));
            case CRL_COLUMN         : return encodedCRL;
            case THUMBPRINT_COLUMN  : return X509CRLInspector.getThumbprint(encodedCRL);
            }
        }
        catch (CRLException e) {
            throw new HibernateException(e);
        }
        catch (IOException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchProviderException e) {
            throw new HibernateException(e);
        }
        
        return null;
    }

    @Override
    public boolean isMutable() {
        /* X509CRL is immutable */
        return false;
    }
    
    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
    throws HibernateException, SQLException 
    {
        X509CRL crl = null;

        byte[] encodedCRL = rs.getBytes(names[CRL_COLUMN]);
        
        if (!rs.wasNull() && encodedCRL != null) 
        {
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory("X.509");
                
                crl = (X509CRL) factory.generateCRL(new ByteArrayInputStream(encodedCRL));
            } 
            catch (CertificateException e) {
                throw new HibernateException(e);
            }
            catch (NoSuchProviderException e) {
                throw new HibernateException(e);
            }
            catch (CRLException e) {
                throw new HibernateException(e);
            }
        }

        return crl;    
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) 
    throws HibernateException, SQLException 
    {
        try {            
            String issuer     = null;
            Date nextUpdate   = null;
            Date thisUpdate   = null;
            String crlNumber  = null;
            byte[] encodedCRL = null;
            String thumbprint = null;

            if (value != null) 
            { 
                if (!(value instanceof X509CRL)) {
                    throw new IllegalArgumentException("Object is not a X509CRL.");
                }

                X509CRL crl = (X509CRL) value;
                
                issuer     = X509CRLInspector.getIssuerCanonical(crl);
                nextUpdate = crl.getNextUpdate();
                thisUpdate = crl.getThisUpdate();
                crlNumber  = BigIntegerUtils.hexEncode(X509CRLInspector.getCRLNumber(crl));
                encodedCRL = crl.getEncoded();
                thumbprint = X509CRLInspector.getThumbprint(encodedCRL);
            }
            
            Hibernate.STRING.nullSafeSet(st, issuer, index + ISSUER_COLUMN);
            Hibernate.TIMESTAMP.nullSafeSet(st, nextUpdate, index + NEXT_UPDATE_COLUMN);
            Hibernate.TIMESTAMP.nullSafeSet(st, thisUpdate, index + THIS_UPDATE_COLUMN);
            Hibernate.STRING.nullSafeSet(st, crlNumber, index + CRL_NUMBER_COLUMN);
            Hibernate.BINARY.nullSafeSet(st, encodedCRL, index + CRL_COLUMN);
            Hibernate.STRING.nullSafeSet(st, thumbprint, index + THUMBPRINT_COLUMN);
        }
        catch (IOException e) {
            throw new HibernateException(e);
        }
        catch (CRLException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchProviderException e) {
            throw new HibernateException(e);
        }
    }
    
    @Override
    public Object replace(Object original, Object target, SessionImplementor session, Object owner) 
    throws HibernateException 
    {
        return original;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
        return X509CRL.class;
    }

    @Override
    public void setPropertyValue(Object comnponent, int property, Object value)
    throws HibernateException 
    {
        /* X509CRL is immutable */
    }
}
