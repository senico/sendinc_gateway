/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SecurityFactoryFactoryException;

/**
 * When we need to serialize a Certificate we do not want to rely on the build in serialization functionality 
 * of certificate because when it is deserialized the certificate will always be created by the default
 * security provider (ie. SUN) and not by the provider we want to use. We will therefore store the certificate
 * in encoded form and serialize this encoded form.
 * 
 * WARNING: this class is used for long term serialization. The class should there not be moved or renamed. Be careful
 * with any changes made to class members, changes might result in changes to deserialization.
 * 
 * @author Martijn Brinkers
 *
 */
public class EncodedCertificate implements Serializable
{
    private static final long serialVersionUID = -5555360454241029530L;

    private final byte[] encodedCertificate;
    private final String certificateType;
    
    public EncodedCertificate(Certificate certificate) 
    throws CertificateEncodingException 
    {
        this.encodedCertificate = certificate.getEncoded();
        this.certificateType = certificate.getType();
    }

    /**
     * Creates and returns a certificate from the encoded blob and certificate type using the default SecurityFactory.
     */
    public Certificate getCertificate() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException 
    {
        CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                createCertificateFactory(certificateType);

        Certificate certificate = factory.generateCertificate(
                new ByteArrayInputStream(encodedCertificate));
        
        return certificate;
    }
}