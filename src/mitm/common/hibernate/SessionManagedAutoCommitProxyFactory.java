/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.BasicProxyFactory;
import mitm.common.reflection.ProxyMethodHandler;
import mitm.common.reflection.ReflectionUtils;
import mitm.common.util.Check;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionManagedAutoCommitProxyFactory<T> extends BasicProxyFactory<T>
{
    private static final Logger logger = LoggerFactory.getLogger(SessionManagedAutoCommitProxyFactory.class);
    
    /*
     * Manages the Hibernate sessions
     */
    private final SessionManager sessionManager;
    
    /*
     * Wrapper class to keep track of recursion depth
     */
    private class RecursionDepth 
    {
        private int recursionDepth;
        
        private Session previousSession;
    }
    
    /*
     * The proxy can recursively be called when a function with @StartTransaction annotation calls another
     * method of the proxy having a @StartTransaction annotation as well. We need to do some bookkeeping 
     * to delay committing the transaction and reuse sessions.
     */
    private final ThreadLocal<RecursionDepth> recursions = new ThreadLocal<RecursionDepth>();
    
    public SessionManagedAutoCommitProxyFactory(Class<T> clazz, SessionManager sessionManager) 
    throws NoSuchMethodException
    {
        super(clazz);
        
        Check.notNull(sessionManager, "sessionManager");
        
        this.sessionManager = sessionManager;
        
        searchForStartTransactionAnnotations(clazz);
        
        this.setMethodHandler(new StartTransactionMethodHandler());            
    }
    
    /*
     * Searches all methods for @StartTransaction annotations
     */
    private void searchForStartTransactionAnnotations(Class<T> clazz)
    {
        Method[] methods = clazz.getMethods();
        
        for (Method method : methods)
        {
            if (method.isAnnotationPresent(StartTransaction.class)) {
                addMethod(method);
            }
        }
    }
    
    class StartTransactionMethodHandler implements ProxyMethodHandler
    {
        @Override
        public Object invoke(Object self, Method method, Method proceed, Object[] args) 
        throws Throwable 
        {
            if (logger.isDebugEnabled()) 
            {
                String methodDescriptor = ReflectionUtils.createMethodDescriptor(method);
                
                logger.debug("invoking '" + methodDescriptor + "'");
            }

            RecursionDepth recursion = recursions.get();
            
            if (recursion == null)
            {
            	recursion = new RecursionDepth();
                
                recursions.set(recursion);
            }
            
            if (recursion.recursionDepth == 0)
            {
                logger.debug("Creating new session.");
                
            	/*
            	 * Store the old session and set a new session
            	 */
            	recursion.previousSession = sessionManager.getSession();
            	
            	sessionManager.setSession(sessionManager.newSession());
            }

            Transaction tx = sessionManager.getSession().beginTransaction();
            
            recursion.recursionDepth++;
            
            try {
                try {
                    /* execute the real method */
                    Object result = proceed.invoke(self, args);

                    recursion.recursionDepth--;
                    
                    if (recursion.recursionDepth == 0)
                    {
                        logger.debug("committing transaction.");
                            
                        tx.commit();
                        
                        recursions.set(null);
                        
                        sessionManager.closeSession(sessionManager.getSession());
                    }
                    
                    return result;
                }
                finally {
                    if (recursion.recursionDepth == 0)
                    {
                        /* 
                         * restore previous session 
                         */
                        sessionManager.setSession(recursion.previousSession);
                    }
                }
            }
            catch(Exception e) {
                logger.debug("Exception in invoke.", e);
                
                try {
                    if (tx.isActive()) {
                        tx.rollback();
                    }
                }
                catch(Exception rbe) {
                    logger.warn("Exception rolling back transaction.", rbe);
                }

                recursions.set(null);
                
                try {
                    sessionManager.closeSession(sessionManager.getSession());
                }
                catch(Exception rbe) {
                    logger.warn("Exception closing session.", rbe);
                }
                
                /* 
                 * A checked exception gets wrapped in an InvocationTargetException. We want to throw the
                 * cause exception so we need to get the cause.
                 */
                if (e instanceof InvocationTargetException) 
                {
                    /* try to re-throw the source exception. */
                    Throwable cause = ExceptionUtils.getCause(e);
                    
                    if (cause != null) {
                        throw cause;
                    }
                }
                
                throw e;
            }
        }
    }
}
