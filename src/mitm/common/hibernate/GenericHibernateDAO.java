/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;

/**
 * Generic hibernate DAO class (bases on from http://www.hibernate.org/328.html).
 * 
 * This class is not thread safe (because Session is not thread safe)
 * 
 * @author Martijn Brinkers
 *
 * @param <T>
 * @param <ID>
 */
public abstract class GenericHibernateDAO<T, ID extends Serializable> 
    implements GenericDAO<T, ID> 
{
    private final Class<T> persistentClass;
    private SessionAdapter session;

    public GenericHibernateDAO(SessionAdapter session) 
    {
        this();
        
        this.session = session;
     }

    @SuppressWarnings("unchecked")
    public GenericHibernateDAO() 
    {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().
                getGenericSuperclass()).getActualTypeArguments()[0];
     }

    public void setSession(SessionAdapter session) 
    {
        this.session = session;
    }
    
    private SessionAdapter getCheckedSession()
    {
        if (session == null) {
            throw new IllegalStateException("Session is null.");            
        }
        
        return session;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findById(ID id) 
    {
        T entity;
        
        entity = (T) getCheckedSession().get(getPersistentClass(), id);

        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findById(ID id, LockMode lockMode) 
    {
        T entity;
        
        entity = (T) getCheckedSession().get(getPersistentClass(), id, lockMode);
        
        return entity;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return getCheckedSession().createCriteria(getPersistentClass()).list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> findByExample(T exampleInstance, String... excludeProperty) 
    {
        Criteria crit = getCheckedSession().createCriteria(getPersistentClass());
        
        Example example =  Example.create(exampleInstance);
        
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        
        return crit.list();
    }
    
    @Override
    public Query createQuery(String query) {
        return getCheckedSession().createQuery(query);
    }
    
    public SQLQuery createSQLQuery(String query) {
        return getCheckedSession().createSQLQuery(query);
    }
    
    @Override
    public Criteria createCriteria(String alias) {
        return getCheckedSession().createCriteria(getPersistentClass(), alias);
    }
    
    @Override
    public DetachedCriteria createDetachedCriteria(String alias) {
        return DetachedCriteria.forClass(getPersistentClass(), alias);
    }
    
    @Override
    public T makePersistent(T entity) 
    {
        getCheckedSession().saveOrUpdate(entity);
        
        return entity;
    }

    @Override
    public void delete(T entity) {
        getCheckedSession().delete(entity);
    }

    @Override
    public void evict(T entity) {
        getCheckedSession().evict(entity);
    }

    public void flush() {
        getCheckedSession().flush();
    }

    public void clear() {
        getCheckedSession().clear();
    }
    
    /*
     * Helper to convert the object to a long
     */
    protected long toLong(Object object)
    {
        if (object instanceof BigInteger) {
            return ((BigInteger)object).longValue();
        }
        else if (object instanceof Long) {
            return (Long)object;
        }
        else if (object instanceof Integer) {
            return (Integer)object;
        }
        
        return 0;
    }
    
    /*
     * Helper to convert the object to an int
     */
    protected int toInt(Object object)
    {
        if (object instanceof BigInteger) {
            return ((BigInteger)object).intValue();
        }
        else if (object instanceof Long) {
            return (int) (long) (Long)object;
        }
        else if (object instanceof Integer) {
            return (Integer)object;
        }
        
        return 0;
    }        
}