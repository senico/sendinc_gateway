/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import org.hibernate.Session;
import org.hibernate.StatelessSession;

/**
 * The SessionManager keeps track of active Hibernate sessions. Because a Session is not thread safe
 * Sessions are stored as a ThreadLocal. SessionManager is used to make sure that all services will
 * use the same session without having to set the session by hand for each service. SessionManager
 * also allows you to 'temporarily' use a new Session for a thread which can later be restored.
 * 
 * @author Martijn Brinkers
 *
 */
public interface SessionManager
{
    /**
     * Sets the current session
     */
    public void setSession(Session session);
        
    /**
     * Returns the active session for this thread. If there is no active session for this thread a session 
     * will be created by the sessionSource.
     */
    public Session getSession();
    
    /**
     * Returns the active session for this thread. If there is no active session for this thread and 
     * createIfNull is true a session  will be created by the sessionSource.
     */
    public Session getSession(boolean createIfNull);
    
	/**
	 * Creates a new Session. 
	 */
    public Session newSession();

    /**
     * Opens a new StatelessSession.
     */
    public StatelessSession openStatelessSession();
    
	/**
	 * Close the session 
	 */
    public void closeSession(Session session);
}
