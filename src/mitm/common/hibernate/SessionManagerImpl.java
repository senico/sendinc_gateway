/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import mitm.common.util.Check;

import org.apache.commons.lang.ObjectUtils;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionManagerImpl implements SessionManager
{
    private final static Logger logger = LoggerFactory.getLogger(SessionManager.class);
    
    /*
     * When we inject the session we have to make sure that the class is thread safe. Sessions are
     * not thread safe so we need to store a session for each thread.
     */
    private final ThreadLocal<Session> sessions = new ThreadLocal<Session>();

    /*
     * HibernateSessionSource will be used to create Hibernate sessions and stateless sessions 
     */
    private HibernateSessionSource sessionSource;

    
    public SessionManagerImpl(HibernateSessionSource sessionSource) 
    {
    	Check.notNull(sessionSource, "sessionSource");
    	
        this.sessionSource = sessionSource;
    }
    
    /**
     * Sets the current session
     */
    @Override
    public void setSession(Session session) 
    {
        logDebugInfo("setSession", session);
        
        sessions.set(session);
    }

    /**
     * Returns the active session for this thread. If there is no active session for this thread a session 
     * will be created by the sessionSource.
     */
    @Override
    public Session getSession() {
        return getSession(true);
    }

    /**
     * Returns the active session for this thread. If there is no active session for this thread and 
     * createIfNull is true a session  will be created by the sessionSource.
     */
    @Override
    public Session getSession(boolean createIfNull)
    {        
        Session currentSession = sessions.get();
        
        if (currentSession == null && createIfNull) 
        {
            logDebugInfo("currentSession == null");

            if (sessionSource == null) {
                throw new IllegalStateException("SessionSource or Session must be non-null.");
            }
            
            currentSession = sessionSource.getSession();
        }
     
        logDebugInfo("getSession", currentSession);
        
        return currentSession;
    }

    @Override
    public Session newSession() {
    	return sessionSource.newSession();
    }

    @Override
    public StatelessSession openStatelessSession() {
    	return sessionSource.openStatelessSession();
    }
    
    @Override
    public void closeSession(Session session) {
    	sessionSource.closeSession(session);
    }
    
    private void logDebugInfo(String message) {
        logDebugInfo(message, null);
    }
    
    private void logDebugInfo(String message, Object object) 
    {
        if (logger.isDebugEnabled()) 
        {
            logger.debug(message + ". Object: " + ObjectUtils.identityToString(object));            
        }
    }
}
