/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mitm.common.security.SecurityFactoryFactory;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

/**
 * Hibernate CompositeUserType that can be used to persist {@link CertPath}.
 * 
 * @author Martijn Brinkers
 *
 */
public class CertPathUserType implements CompositeUserType
{
    private static final int CERT_PATH_COLUMN      = 0;
    private static final int CERT_PATH_TYPE_COLUMN = 1;
        
    private static final Type[] PROPERTY_TYPES = {
        Hibernate.BINARY,    /* certPath */
        Hibernate.STRING,    /* certPathType */
        };
    
    private static final String[] PROPERTY_NAMES = {
        "certPath",
        "certPathType",
    };
    
    @Override
    public Object assemble(Serializable cached, SessionImplementor session, Object owner) 
    throws HibernateException 
    {
        return deepCopy(cached);
    }

    @Override
    public Object deepCopy(Object value) 
    throws HibernateException 
    {
        if (value == null) {
            return null;
        }
        CertPath original = (CertPath) value;

        CertPath copy = null;
        
        try {
            byte[] encodedCertPath = original.getEncoded();
            
            String certificateType = original.getType();
            
            CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                    createCertificateFactory(certificateType);
            
            copy = factory.generateCertPath(new ByteArrayInputStream(encodedCertPath));
        }
        catch (CertificateException e) {
            throw new HibernateException(e);
        }
        catch (NoSuchProviderException e) {
            throw new HibernateException(e);
        }

        return copy;
    }    

    @Override
    public Serializable disassemble(Object value, SessionImplementor session)
    throws HibernateException 
    {
        return (Serializable) deepCopy(value);
    }

    @Override
    public boolean equals(Object o1, Object o2) 
    throws HibernateException 
    {
        if (o1 == o2) {
            return true;
        }
        
        if ((o1 == null) || (o2 == null)) {
            return false;
        }
        
        /* fall back to CertPath.equals */
        return o1.equals(o2);
    }

    @Override
    public int hashCode(Object object) 
    throws HibernateException 
    {
        return object.hashCode();
    }
    
    @Override
    public String[] getPropertyNames() {
        return PROPERTY_NAMES;
    }

    @Override
    public Type[] getPropertyTypes() {
        return PROPERTY_TYPES;
    }
    
    @Override
    public Object getPropertyValue(Object component, int property)
    throws HibernateException 
    {
        if (component == null) {
            return null;
        }

        CertPath certPath = (CertPath) component;

        try {
            switch(property) {
            case CERT_PATH_COLUMN      : return certPath.getEncoded();
            case CERT_PATH_TYPE_COLUMN : return certPath.getType();
            }
        }
        catch(CertificateEncodingException e) {
            throw new HibernateException(e);
        }
        
        return null;
    }
    
    @Override
    public boolean isMutable() {
        /*  CertPath is immutable */
        return false;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
    throws HibernateException, SQLException 
    {
        CertPath certPath = null;

        byte[] encodedCertPath = rs.getBytes(names[CERT_PATH_COLUMN]);
                
        if (!rs.wasNull() && encodedCertPath != null) 
        {    
            String certPathType = rs.getString(names[CERT_PATH_TYPE_COLUMN]);
            
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory(certPathType);
                
                certPath = factory.generateCertPath(
                        new ByteArrayInputStream(encodedCertPath));
            } 
            catch (CertificateException e) {
                throw new HibernateException(e);
            }
            catch (NoSuchProviderException e) {
                throw new HibernateException(e);
            }
        }

        return certPath;    
    }
    
    
    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) 
    throws HibernateException, SQLException 
    {
        CertPath certPath = (CertPath) value;
        
        try {
            byte[] encodedCertPath = null;
            String certPathType = null;
            
            if (certPath != null) 
            {
                encodedCertPath = certPath.getEncoded();
                certPathType = certPath.getType();
            }
                
            Hibernate.BINARY.nullSafeSet(st, encodedCertPath, index + CERT_PATH_COLUMN);
            Hibernate.STRING.nullSafeSet(st, certPathType, index + CERT_PATH_TYPE_COLUMN);
        }
        catch(CertificateEncodingException e) {
            throw new HibernateException(e);
        }
    }
    

    @Override
    public Object replace(Object original, Object target, SessionImplementor session, Object owner) 
    throws HibernateException 
    {
        return original;
    }

    @Override
    public Class<?> returnedClass() {
        return CertPath.class;
    }
    
    @Override
    public void setPropertyValue(Object comnponent, int property, Object value)
    throws HibernateException 
    {
        /* CertPath is immutable */
    }    
}
