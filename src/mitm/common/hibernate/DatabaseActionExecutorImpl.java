/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseActionExecutorImpl implements DatabaseActionExecutor 
{
    private static final Logger logger = LoggerFactory.getLogger(DatabaseActionExecutorImpl.class);
    
    private SessionManager sessionManager;

    public DatabaseActionExecutorImpl(SessionManager sessionManager)
    {
        this.sessionManager = sessionManager;
    }

    @Override
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
        
    @Override
    public <T> T executeTransaction(DatabaseAction<T> action)
    throws DatabaseException
    {
        Session session = sessionManager.newSession();
        
        Transaction tx = session.beginTransaction();
        
        try {
            T result = action.doAction(session);

            tx.commit();
            
            sessionManager.closeSession(session);
            
            return result;
        }
        catch(Exception e) {
            try {
                if (tx.isActive()) {
                    tx.rollback();
                }
            }
            catch(Exception rbe) {
                logger.warn("Exception rolling back transaction.", rbe);
            }

            try {
            	sessionManager.closeSession(session);
            }
            catch(Exception rbe) {
                logger.warn("Exception closing session.", rbe);
            }
            
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }

            if (e instanceof RuntimeException) {
                throw (RuntimeException)e;
            }

            throw new DatabaseException(e);            
        }
    }

    @Override
    public <T> T executeTransaction(DatabaseAction<T> action, int retries)
    throws DatabaseException
    {
    	return executeTransaction(action, retries, null);
    }
    
    @Override
    public <T> T executeTransaction(DatabaseAction<T> action, int retries, DatabaseActionRetryEvent retryEvent)
    throws DatabaseException
    {
    	do {
    		try {
	    		return executeTransaction(action);
    		}
    		catch (ConstraintViolationException e)
    		{
    			if (retries <= 0) {
    				throw e;
    			}
    			else {
        			logger.warn("ConstraintViolationException. Retrying action. Message: " + e.getMessage());
        			
        			if (retryEvent != null) {
        				retryEvent.onRetry();
        			}
    			}

    			retries--;
    		}
    	}
    	while (true);
    }
    
    @Override
    public <T> void executeTransaction(DatabaseVoidAction action)
    throws DatabaseException
    {
        final DatabaseVoidAction finalAction = action;
        
        executeTransaction(new DatabaseAction<T>()
        {
            @Override
            public T doAction(Session session) 
            throws DatabaseException 
            {
                finalAction.doAction(session);
                return null;
            }
        });
    }    

    @Override
    public <T> void executeTransaction(DatabaseVoidAction action, int retries)
    throws DatabaseException
    {
    	executeTransaction(action, retries, null);
    }
    
    @Override
    public <T> void executeTransaction(DatabaseVoidAction action, int retries, DatabaseActionRetryEvent retryEvent)
    throws DatabaseException
    {
    	do {
    		try {
	    		executeTransaction(action);
	    		
	    		return;
    		}
    		catch (ConstraintViolationException e)
    		{
    			if (retries <= 0) {
    				throw e;
    			}
    			else {
        			logger.warn("ConstraintViolationException. Retrying action. Message: " + e.getMessage());
        			
        			if (retryEvent != null) {
        				retryEvent.onRetry();
        			}
    			}

    			retries--;
    		}
    	}
    	while (true);
    }
}
