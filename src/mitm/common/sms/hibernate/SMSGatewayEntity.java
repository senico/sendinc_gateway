/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.hibernate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import mitm.common.util.Check;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

@Entity(name = SMSGatewayEntity.ENTITY_NAME)
@Table(name = "sms")
@org.hibernate.annotations.Table(appliesTo = SMSGatewayEntity.ENTITY_NAME, indexes = {
        @Index(name = "sms_datelasttry_index", columnNames = {SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME})
        }
)
public class SMSGatewayEntity
{
    public static final String ENTITY_NAME = "sms";
    
    private static final int MAX_PHONE_NUMBER_LENGTH = 255; 
    
    private static final int MAX_MESSAGE_LENGTH = 2048; 

    private static final int MAX_ERROR_LENGTH = 1024; 
    
    public static final String ID_COLUMN_NAME = "id";
    public static final String PHONENUMBER_COLUMN_NAME = "phoneNumber";
    public static final String MESSAGE_COLUMN_NAME = "message";
    public static final String DATE_CREATED_COLUMN_NAME = "dateCreated";
    public static final String DATE_LAST_TRY_COLUMN_NAME = "dateLastTry";
    public static final String DATA_COLUMN_NAME = "data";
    public static final String LAST_ERROR_COLUMN_NAME = "lastError";
    
    /*
     * All the columns of the entity and their names
     */
    public static enum Columns
    {
        ID            (ID_COLUMN_NAME),
        PHONENUMBER   (PHONENUMBER_COLUMN_NAME), 
        MESSAGE       (MESSAGE_COLUMN_NAME), 
        DATE_CREATED  (DATE_CREATED_COLUMN_NAME), 
        DATE_LAST_TRY (DATE_LAST_TRY_COLUMN_NAME),
        DATA          (DATA_COLUMN_NAME),
        LAST_ERROR    (LAST_ERROR_COLUMN_NAME);
        
        private String column;
        
        private Columns(String column) {
            this.column = column;
        }
        
        public String getColumn() {
            return column;
        }
        
        @Override
        public String toString() {
            return column;
        }
    }    
    
    @Id
    @Column(name = ID_COLUMN_NAME)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column (name = PHONENUMBER_COLUMN_NAME, unique = false, nullable = false, length = MAX_PHONE_NUMBER_LENGTH)
    private String phoneNumber;
    
    @Column (name = MESSAGE_COLUMN_NAME, unique = false, nullable = false, length = MAX_MESSAGE_LENGTH)
    private String message;
    
    @Type(type = "timestamp")
    @Column (name = DATE_CREATED_COLUMN_NAME, unique = false, nullable = true)
    private Date dateCreated;

    @Type(type = "timestamp")
    @Column (name = DATE_LAST_TRY_COLUMN_NAME, unique = false, nullable = true)
    private Date dateLastTry;
    
    @Column (name = DATA_COLUMN_NAME, unique = false, nullable = true)
    byte[] data;

    @Column (name = LAST_ERROR_COLUMN_NAME, unique = false, nullable = true, length = MAX_ERROR_LENGTH)
    private String lastError;
    
    protected SMSGatewayEntity() {
        /* required by Hibernate */
    }
    
    public SMSGatewayEntity(String phoneNumber, String message, byte[] data, Date dateCreated)
    {
    	Check.notNull(phoneNumber, "phoneNumber");
    	Check.notNull(message, "message");
    	Check.notNull(dateCreated, "dateCreated");

        this.phoneNumber = phoneNumber;
        this.message = message;
        this.data = data;
        this.dateCreated = dateCreated;
    }

    public Long getID() {
        return id;
    }
    
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public String getMessage() {
        return message;
    }
    
    public Date getDateCreated() {
        return dateCreated;
    }
    
    public byte[] getData() {
        return data;
    }
    
    public Date getDateLastTry() {
        return dateLastTry;
    }

    public void setDateLastTry(Date dateLastTry) {
        this.dateLastTry = dateLastTry;
    }

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}
}
