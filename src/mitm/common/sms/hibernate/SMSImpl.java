/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.hibernate;

import java.io.Serializable;
import java.util.Date;

import mitm.common.sms.SMS;
import mitm.common.util.Check;

public class SMSImpl implements SMS
{
	private final Long ID;
    private final String phoneNumber;
    private final String message;
    private final Serializable data;
    private final Date created;
    private final Date lastTry;
    private final String lastError;

    public SMSImpl(String phoneNumber, String message, Serializable data) {
    	this(null, phoneNumber, message, data, null, null, null);
    }
 
    public SMSImpl(Long ID, String phoneNumber, String message, Serializable data, Date created, 
    		Date lastTry, String lastError)
    {
    	Check.notNull(phoneNumber, "phoneNumber");
    	Check.notNull(message, "message");
    	
        this.ID = ID;
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.data = data;
        this.created = created;
        this.lastTry = lastTry;
        this.lastError = lastError;
    }
    
    @Override
	public Long getID() {
		return ID;
	}
    
    @Override
    public String getMessage() {
        return message;
    }
    
    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    @Override
    public Serializable getData() {
        return data;
    }
    
    @Override
    public Date getCreated() {
    	return created;
    }

    @Override
    public Date getLastTry() {
    	return lastTry;
    }
    
    @Override
    public String getLastError() {
    	return lastError;
    }
}
