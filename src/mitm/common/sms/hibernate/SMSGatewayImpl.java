/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.hibernate;

import java.util.Date;
import java.util.List;

import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.sms.SMS;
import mitm.common.sms.SMSExpiredListener;
import mitm.common.sms.SMSGateway;
import mitm.common.sms.SMSGatewayException;
import mitm.common.sms.SMSTransport;
import mitm.common.sms.SortColumn;
import mitm.common.util.Check;
import mitm.common.util.DateTimeUtils;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMSGatewayImpl implements SMSGateway
{
    private final static Logger logger = LoggerFactory.getLogger(SMSGatewayImpl.class);
    
    /*
     * The name of the transport thread.
     */
    private final static String THREAD_NAME = "SMS transport";

    /*
     * The number of times a database action should be retried when a ConstraintViolation occurs
     */
    private final static int ACTION_RETRIES = 3;
    
    /*
     * Used for excuting database actions within a transaction
     */
    private final DatabaseActionExecutor databaseActionExecutor;
    
    /*
     * The transport that will be used to send the SMS.
     */
    private SMSTransport transport;

    /*
     * manages database sessions
     */
    private final SessionManager sessionManager;
    
    /*
     * Listener that's being called when a SMS has expired.
     */
    private final SMSExpiredListener expiredListener;
    
    /*
     * Time (in milliseconds) the thread will sleep if there is no SMS to sent.
     */
    private long sleepTime = DateUtils.MILLIS_PER_MINUTE * 1;
    
    /*
     * How often (in milliseconds) should we try to send a failed SMS.
     */
    private long updateInterval = DateUtils.MILLIS_PER_MINUTE * 5;
    
    /*
     * How long (in milliseconds) is a SMS valid.
     */
    private long expirationTime = DateUtils.MILLIS_PER_HOUR * 24;
    
    /*
     * The thread that will send the SMS messages.
     */
    private TransportThread transportThread;
    
    /*
     * Object used to notify the thread that it should wakeup.
     */
    private Object wakeup = new Object();

    public SMSGatewayImpl(SMSTransport transport, SMSExpiredListener expiredListener, SessionManager sessionManager)
    {
    	Check.notNull(transport, "transport");
    	Check.notNull(sessionManager, "sessionManager");
    	
        this.databaseActionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        this.transport = transport;
        this.sessionManager = sessionManager;
        this.expiredListener = expiredListener;
    }
    
    /**
     * Sets the sleepTime (in milliseconds).
     * 
     * Note: should not be called after start has been called.
     */
    public void setSleepTime(long sleepTime)
    {
        if (sleepTime < 0) {
            throw new IllegalArgumentException("sleepTime must be > 0");
        }
        
        this.sleepTime = sleepTime;
    }

    /**
     * Sets the expirationTime (in milliseconds).
     * 
     * Note: should not be called after start has been called.
     */
    public void setExpirationTime(long expirationTime)
    {
        if (expirationTime < 0) {
            throw new IllegalArgumentException("expirationTime must be > 0");
        }
        
        this.expirationTime = expirationTime;
    }

    /**
     * Sets the updateInterval (in milliseconds).
     * 
     * Note: should not be called after start has been called.
     */
    public void setUpdateInterval(long updateInterval)
    {
        if (updateInterval < 0) {
            throw new IllegalArgumentException("updateInterval must be > 0");
        }
        
        this.updateInterval = updateInterval;
    }
    
    @Override
    public synchronized void setTransport(SMSTransport transport) {
        this.transport = transport;
    }

    public synchronized SMSTransport getTransport() {
        return transport;
    }
        
    @Override
    public synchronized void start()
    {
        if (transportThread == null)
        {
            transportThread = new TransportThread(THREAD_NAME);
            
            transportThread.setDaemon(true);
            transportThread.start();
        }
    }
    
    @Override
    public void sendSMS(final SMS sms)
    throws SMSGatewayException
    {
        try {
            databaseActionExecutor.executeTransaction(new DatabaseVoidAction()
            {
                @Override
                public void doAction(Session session)
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        addSMSAction(session, sms);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }
                }
            });
        }
        catch (DatabaseException e) {
            throw new SMSGatewayException(e);
        }
        
        /*
         * Notify the transport thread that there is a new SMS so it can wakeup if asleep.
         */
        synchronized (wakeup) {
            wakeup.notify();
        }
    }
    
    private void addSMSAction(Session session, SMS sms)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            dao.addSMS(sms);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
        
    @Override
    public int getCount() 
    throws SMSGatewayException
    {
        try {
            return databaseActionExecutor.executeTransaction(new DatabaseAction<Integer>()
            {
                @Override
                public Integer doAction(Session session)
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        return getCountAction(session);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }                    
                }
            }, ACTION_RETRIES /* retry on a ConstraintViolationException */);
        }
        catch (DatabaseException e) {
            throw new SMSGatewayException(e);
        }
    }

    private int getCountAction(Session session)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            return dao.getAvailableCount(null);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    @Override
    public boolean delete(final long id) 
    throws SMSGatewayException 
    {
        try {
            return databaseActionExecutor.executeTransaction(new DatabaseAction<Boolean>()
            {
                @Override
                public Boolean doAction(Session session)
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        return deleteAction(session, id);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }                    
                }
            }, ACTION_RETRIES /* retry on a ConstraintViolationException */);
        }
        catch (DatabaseException e) {
            throw new SMSGatewayException(e);
        }
    }

    private boolean deleteAction(Session session, long id)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            SMSGatewayEntity entity = dao.findById(id);
            
            boolean deleted = false;
            
            if (entity != null) 
            {
            	dao.delete(entity);
            	
            	deleted = true;
            }
            
            return deleted;
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    @Override
    public List<SMS> getAll(final Integer firstResult, final Integer maxResults, 
    		final SortColumn sortColumn, final SortDirection sortDirection)
    throws SMSGatewayException 
    {
        try {
            return databaseActionExecutor.executeTransaction(new DatabaseAction<List<SMS>>()
            {
                @Override
                public List<SMS> doAction(Session session)
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        return getAllAction(session, firstResult, maxResults, sortColumn, sortDirection);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }                    
                }
            }, ACTION_RETRIES /* retry on a ConstraintViolationException */);
        }
        catch (DatabaseException e) {
            throw new SMSGatewayException(e);
        }
    }
    
    private List<SMS> getAllAction(Session session, Integer firstResult, Integer maxResults, 
    		SortColumn sortColumn, SortDirection sortDirection)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            return dao.getAll(firstResult, maxResults, sortColumn, sortDirection);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    private class TransportThread extends Thread
    {            
        public TransportThread(String threadName)
        {
            super(threadName);
        }
        
        @Override
        public void run() 
        {
            logger.info("Starting " + THREAD_NAME + ". Expiration time: " + expirationTime + 
                    " Sleep time: " + sleepTime + " Update interval: " + updateInterval);
            
            do {                
                try {
                    sendNextSMS();
                    
                    handleExpiredSMS();
                    
                    synchronized (wakeup) 
                    {
                        /*
                         * Sleep for some time if there is nothing to do.
                         */
                        if (getAvailableCount() == 0)
                        {
                            try {
                                wakeup.wait(sleepTime);
                            }
                            catch (InterruptedException e) {
                                // ignore
                            }
                        }
                    }
                }
                catch(Exception e) {
                    logger.error("Error in SMS transport thread.", e);
                }
            }
            while(true);
        }

        private void sendNextSMS() 
        throws DatabaseException
        {
            databaseActionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        Session previousSession = sessionManager.getSession();
                        
                        sessionManager.setSession(session);
                        
                        try {
                            sendNextSMSAction(session);
                        }
                        finally {
                            sessionManager.setSession(previousSession);                            
                        }                    
                    }
            });
        }

        /*
         * Returns the date after which entries are not yet ready for sending (because they have been tried recently)
         */
        private Date getNotAfter() 
        {
            Date now = new Date();

            return DateTimeUtils.addMilliseconds(now, -updateInterval); 
        }
        
        private void sendNextSMSAction(Session session) 
        throws DatabaseException
        {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));

            final Date notAfter = getNotAfter(); 
            
            SMSGatewayEntity sms = dao.getNextAvailable(notAfter);
            
            if (sms != null)
            {
                try {
                    getTransport().sendSMS(sms.getPhoneNumber(), sms.getMessage());
                    
                    /*
                     * The SMS has been sent so we can remove it.
                     */
                    
                    StrBuilder sb = new StrBuilder(256);
                    
                    sb.append("Successfully sent SMS with transport '");
                    sb.append(getTransport().getName());
                    sb.append("' to phone number '");
                    sb.append(sms.getPhoneNumber());
                    sb.append("'");
                    
                    logger.info(sb.toString());
                    
                    dao.delete(sms);
                }
                catch (Exception e) {
                    
                    StrBuilder sb = new StrBuilder(256);
                    
                    sb.append("Error sending SMS with transport '");
                    sb.append(getTransport().getName());
                    sb.append("' to phone number '");
                    sb.append(sms.getPhoneNumber());
                    sb.append("'");
                    
                    logger.error(sb.toString(), e);
                    
                    /*
                     * Update the date we have tried to sent the message and last error
                     */
                    sms.setDateLastTry(new Date());
                    sms.setLastError(ExceptionUtils.getRootCauseMessage(e));
                }
            }
        }

        /*
         * Returns the number of SMS that are ready to be sent.
         */
        private int getAvailableCount() 
        throws DatabaseException
        {
            final Date notAfter = getNotAfter(); 
            
            int count = databaseActionExecutor.executeTransaction(new DatabaseAction<Integer>()
            {
                @Override
                public Integer doAction(Session session) 
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        return getAvailableCountAction(session, notAfter);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }                                        
                }
            }, ACTION_RETRIES /* retry on a ConstraintViolationException */);
            
            return count;
        }
        
        private int getAvailableCountAction(Session session, Date notAfter) 
        throws DatabaseException
        {
            try {
                SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
                
                return dao.getAvailableCount(notAfter);
            }
            catch(Exception e) {
                if (e instanceof DatabaseException) {
                    throw (DatabaseException)e;
                }
                throw new DatabaseException(e);
            }        
        }
        
        private void handleExpiredSMS() 
        throws DatabaseException
        {
            Date now = new Date();
            
            final Date olderThan = DateTimeUtils.addMilliseconds(now, -expirationTime);
            
            databaseActionExecutor.executeTransaction(new DatabaseVoidAction()
            {
                @Override
                public void doAction(Session session) 
                throws DatabaseException 
                {
                    Session previousSession = sessionManager.getSession();
                    
                    sessionManager.setSession(session);
                    
                    try {
                        handleExpiredSMSAction(session, olderThan);
                    }
                    finally {
                        sessionManager.setSession(previousSession);                            
                    }                                        
                }
            });            
        }

        private void handleExpiredSMSAction(Session session, Date olderThan) 
        throws DatabaseException
        {
            try {
                SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
                
                List<SMSGatewayEntity> expired = dao.getExpired(olderThan);
                
                if (expired.size() > 0)
                {
                    for (SMSGatewayEntity smsEntity : expired)
                    {
                        StrBuilder sb = new StrBuilder(256);
                        
                        sb.append("The SMS to phone number '");
                        sb.append(smsEntity.getPhoneNumber());
                        sb.append("' is expired");
                        
                        logger.warn(sb.toString());

                        if (expiredListener != null) {
                            expiredListener.expired(SMSGatewayDAO.entityToSMS(smsEntity));
                        }
                        
                        dao.delete(smsEntity);
                    }
                }
            }
            catch(Exception e) {
                if (e instanceof DatabaseException) {
                    throw (DatabaseException)e;
                }
                throw new DatabaseException(e);
            }            
        }
    }    
}
