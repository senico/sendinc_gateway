/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.transport.clickatell;

/**
 * Clickatell message status
 * 
 * @author Martijn Brinkers
 *
 */
public enum MessageStatus 
{
    MESSAGE_UNKNOWN          ("001", "0x001", "Message unknown"),
    MESSAGE_QUEUED           ("002", "0x002", "Message queued"),
    DELIVERED_TO_GATEWAY     ("003", "0x003", "Delivered to gateway"),
    RECEIVED_BY_RECIPIENT    ("004", "0x004", "Received by recipient"),
    ERROR_WITH_MESSAGE       ("005", "0x005",  "Error with message"),
    USER_CANCELLED_MESSAGE   ("006", "0x006", "User cancelled message"),
    ERROR_DELIVERING_MESSAGE ("007", "0x007", "Error delivering message"),
    OK                       ("008", "0x008", "OK"),
    ROUTING_ERROR            ("009", "0x009", "Routing error"),
    MESSAGE_EXPIRED          ("010", "0x00A", "Message expired"),
    MESSAGE_QUEUED_FOR_LATER ("011", "0x00B", "Message queued for later"),
    OUT_OF_CREDIT            ("012", "0x00C", "Out of credit"),
    UNKNOWN                  ("UNKNOWN", "UNKNOWN", "Unknown");
    
    private final String number;
    private final String hex;
    private final String detail;
    
    private MessageStatus(String number, String hex, String detail)
    {
        this.number = number;
        this.hex = hex;
        this.detail = detail;
    }
    
    public String getNumber() {
        return number;
    }

    public String getHex() {
        return hex;
    }
    
    public String getDetail() {
        return detail;
    }
    
    public static MessageStatus getMessageStatus(String number)
    {
        for (MessageStatus status : MessageStatus.values())
        {
            if (status.getNumber().equalsIgnoreCase(number)) {
                return status;
            }
        }
        
        return MessageStatus.UNKNOWN;
    }
    
    public static MessageStatus getMessageStatusHex(String hex)
    {
        for (MessageStatus status : MessageStatus.values())
        {
            if (status.getHex().equalsIgnoreCase(hex)) {
                return status;
            }
        }
        
        return MessageStatus.UNKNOWN;
    }    
}
