/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.transport.clickatell;

import mitm.common.properties.HierarchicalPropertiesException;


/**
 * ClickatellParameters implementation that stores the parameters in memory.
 * 
 * @author Martijn Brinkers
 *
 */
public class StaticClickatellParameters implements ClickatellParameters
{
    private String apiID;
    private String user;
    private String password;
    private String from;

    public StaticClickatellParameters() {
        /*
         * empty on purpose
         */
    }
    
    public StaticClickatellParameters(String apiID, String user, String password, String from) 
    {
        this.apiID = apiID;
        this.user = user;
        this.password = password;
        this.from = from;
    }
    
    @Override
    public String getAPIID() {
        return apiID;
    }
    
    @Override
    public void setAPIID(String id) {
        this.apiID = id;
    }
    
    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }
    
    @Override
    public String getPassword() {
        return password;
    }
    
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
        
    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public void setFrom(String from) {
        this.from = from;
    }
    
    @Override
    public void copyTo(ClickatellParameters other)
    throws HierarchicalPropertiesException
    {
        other.setAPIID(apiID);
        other.setUser(user);
        other.setPassword(password);
        other.setFrom(from);
    }   
}
