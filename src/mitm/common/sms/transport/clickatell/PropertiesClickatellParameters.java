/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.transport.clickatell;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;


/**
 * Implementation of ClickatellParameters that reads and writes it's settings from
 * a HierarchicalProperties object.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesClickatellParameters implements ClickatellParameters
{
    private final static String BASE_PROP = "clickatell.";
    private final static String APIID_PROPERTY_NAME = BASE_PROP + "apiid";
    private final static String USER_PROPERTY_NAME = BASE_PROP + "user";
    private final static String PASSWORD_PROPERTY_NAME = BASE_PROP + "password";
    private final static String FROM_PROPERTY_NAME = BASE_PROP + "from";
    
    private final HierarchicalProperties properties;
    
    public PropertiesClickatellParameters(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }
    
    @Override
    public String getAPIID()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(APIID_PROPERTY_NAME, true);
    }
    
    @Override
    public void setAPIID(String id)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(APIID_PROPERTY_NAME, id, true);
    }
    
    @Override
    public String getUser()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(USER_PROPERTY_NAME, true);
    }

    @Override
    public void setUser(String user)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(USER_PROPERTY_NAME, user, true);
    }
    
    @Override
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PASSWORD_PROPERTY_NAME, true);
    }
    
    @Override
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PASSWORD_PROPERTY_NAME, password, true);
    }
        
    @Override
    public String getFrom()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(FROM_PROPERTY_NAME, true);
    }

    @Override
    public void setFrom(String from)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(FROM_PROPERTY_NAME, from, true);
    }
   
    @Override
    public void copyTo(ClickatellParameters other)
    throws HierarchicalPropertiesException
    {
        other.setAPIID(getAPIID());
        other.setUser(getUser());
        other.setPassword(getPassword());
        other.setFrom(getFrom());
    }    
}
