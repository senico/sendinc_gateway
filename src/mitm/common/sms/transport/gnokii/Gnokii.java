/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.transport.gnokii;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.common.util.MiscStringUtils;

import org.apache.commons.io.IOUtils;

/**
 * API for Gnoki (http://www.gnokii.org/) used to send SMS messages using an attached mobile
 * phone. Gnokii need to be installed and executable for this transport to function.
 * 
 * @author Martijn Brinkers
 *
 */
public class Gnokii
{
    private final static int DEFAULT_MAX_MESSAGE_SIZE = 160;
    
    private final static String GNOKII_BIN = "gnokii"; 
    
    /*
     * The maximum message size. If more than 160 more than one sms will be sent.
     */
    private int maxMessageSize = 160;
    
    /*
     * If true a delivery report will be sent by the SMS operator. 
     */
    private boolean requestForDeliveryReport = false;
    
    public void setMaxMessageSize(int maxMessageSize) 
    {
        if (maxMessageSize <= 0) {
            throw new IllegalArgumentException("maxMessageSize must be > 0");
        }
        
        this.maxMessageSize = maxMessageSize;
    }
    
    public int getMaxMessageSize() {
        return maxMessageSize;
    }
    
    public void setRequestForDeliveryReport(boolean requestForDeliveryReport) {
        this.requestForDeliveryReport = requestForDeliveryReport;
    }
    
    /**
     * Sends the message to the number.
     */
    public String sendSMS(String phoneNumber, String message)
    throws GnokiiException
    {
        List<String> cmd = new LinkedList<String>();
        
        cmd.add(GNOKII_BIN);
        cmd.add("--sendsms");
        cmd.add(phoneNumber);
        
        if (maxMessageSize != DEFAULT_MAX_MESSAGE_SIZE) 
        {
            cmd.add("-l");
            cmd.add(Integer.toString(maxMessageSize));
        }
        
        if (requestForDeliveryReport) {
            cmd.add("-r");
        }
        
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(cmd);
            processBuilder.redirectErrorStream(true);
            
            Process process = processBuilder.start();
            
            byte[] bytes = MiscStringUtils.toAsciiBytes(message);
            
            IOUtils.write(bytes, process.getOutputStream());
            
            process.getOutputStream().close();
            
            String output = IOUtils.toString(process.getInputStream());
            
            int exitValue;
            
            try {
                exitValue = process.waitFor();
            }
            catch (InterruptedException e) {
                throw new GnokiiException("Error sending SMS. Output: " + output);
            }
            
            if (exitValue != 0) {
                throw new GnokiiException("Error sending SMS. Output: " + output);
            }
            
            return output;
        }
        catch (IOException e) {
            throw new GnokiiException(e);
        }
    }
    
    public static String identify()
    throws GnokiiException
    {
        String[] cmd = new String[] {
            GNOKII_BIN, 
            "--identify"};

        try {
            ProcessBuilder processBuilder = new ProcessBuilder(cmd);
            processBuilder.redirectErrorStream(true);
            
            Process process = processBuilder.start();
            
            String output = IOUtils.toString(process.getInputStream());
            
            int exitValue;
            
            try {
                exitValue = process.waitFor();
            }
            catch (InterruptedException e) {
                throw new GnokiiException("Identify error. Output: " + output);
            }
            
            if (exitValue != 0) {
                throw new GnokiiException("Identify error. Output: " + output);
            }
            
            return output;
        }
        catch (IOException e) {
            throw new GnokiiException(e);
        }
    }
    
    public static void main(String[] args) 
    {
        try {
            Gnokii gnokii = new Gnokii();
            
            System.out.println(gnokii.sendSMS("+31624935722", "test"));
            //System.out.println(identify());
        }
        catch (GnokiiException e) {
            e.printStackTrace();
        }
    }
}
