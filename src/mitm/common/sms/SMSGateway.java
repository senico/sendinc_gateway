/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms;

import java.util.List;

import mitm.common.hibernate.SortDirection;

public interface SMSGateway
{
    /**
     * Adds the SMS to be sent to the gateway. Whether the SMS sent immediately or queued is up to the implementation.
     */
    public void sendSMS(SMS sms)
    throws SMSGatewayException;

    /**
     * Returns the number of SMS in the gateway
     */
    public int getCount()
    throws SMSGatewayException;
    
    /**
     * Deletes the SMS with the given ID. Returns true if deleted.
     */
    public boolean delete(long id)
    throws SMSGatewayException;
    
    /**
     * Returns all SMS
     */
    public List<SMS> getAll(Integer firstResult, Integer maxResults, SortColumn sortColumn, 
    		SortDirection sortDirection)
    throws SMSGatewayException;
    
    /**
     * Starts the gateway.
     */
    public void start();
    
    /**
     * Overrides the default SMS transport.
     */
    public void setTransport(SMSTransport transport);
}
