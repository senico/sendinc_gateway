/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.common.util.Check;

/**
 * Basic implementation of KeyCache. All entries are stored in memory and the entries are removed when they
 * are older than the expiration time or if the max number of items is reached. 
 * 
 * @author Martijn Brinkers
 *
 */
public class KeyCacheImpl implements KeyCache
{
    private final static Logger logger = LoggerFactory.getLogger(KeyCacheImpl.class);
    
    /*
     * Time in milliseconds an entry stays in the cache
     */
    private final long liveTime;

    /*
     * Maximum number of entries after clean
     */
    private final int maxEntriesLow;

    /*
     * Maximum number of entries when a cleanup will be started
     */
    private final int maxEntriesHigh;
    
    /*
     * The cache entries
     */
    private final Set<KeyWithTime> entries = new LinkedHashSet<KeyWithTime>();

    /*
     * Thread that periodically checks whether items are expired
     */
    private final ReaperThread reaperThread;
    
    public KeyCacheImpl(long liveTime, int maxEntriesLow, int maxEntriesHigh, long updateInterval)
    {
        this.liveTime = liveTime;
        this.maxEntriesLow = maxEntriesLow;
        this.maxEntriesHigh = maxEntriesHigh;
        
        if (maxEntriesLow > maxEntriesHigh) {
            throw new IllegalArgumentException("maxEntriesLow > maxEntriesHigh");
        }
        
        this.reaperThread = new ReaperThread(updateInterval);
        this.reaperThread.setDaemon(true);
        this.reaperThread.start();
    }
    
    /**
     * returns true if the key is in the cache
     */
    public synchronized boolean contains(String key) {
        return entries.contains(new KeyWithTime(key));
    }
    
    protected synchronized boolean add(String key, long time)
    {
        boolean added = false;
        
        if (!contains(key))
        { 
            entries.add(new KeyWithTime(key, time));
            
            added = true;
            
            if (entries.size() > maxEntriesHigh) {
                cleanup();
            }
        }
        
        return added;
    }

    @Override
    public synchronized boolean add(String key) {
        return add(key, System.currentTimeMillis());
    }
    
    /**
     * Removes all entries
     */
    public synchronized void clear() {
        entries.clear();
    }

    /**
     * returns the number of entries in the cache
     */
    public synchronized int size() {
        return entries.size();
    }
    
    /*
     * Check for expired items
     */
    private synchronized void cleanup()
    {
        /*
         * Because the set is a LinkedHashSet entries are sorted from old to new.
         * 
         * Step through items until a non expired item is found throwing away expired items
         */
        long now = System.currentTimeMillis();
        
        Iterator<KeyWithTime> entryIterator = entries.iterator();
        
        while (entryIterator.hasNext()) 
        {
            KeyWithTime entry = entryIterator.next();
            
            if ((now - entry.time) < liveTime) {
                /*
                 * The entry has not expired. We can stop because all other entries are newer because
                 * a LinkedHashSet is used.
                 */
                break;
            }

            /*
             * According to Javadoc of LinkedHashSet we can safely remove using the iterator without 
             * getting a ConcurrentModificationException 
             */
            entryIterator.remove();
        }

        if (entries.size() > maxEntriesHigh) {
            /*
             * Too many items in the cache so we need to start removing entries until we have 
             * maxEntriesLow entries left. We need to remove size() - maxEntriesLow entries.
             */
            int toRemove = entries.size() - maxEntriesLow;

            logger.warn("Cache over limit. Going to remove " + toRemove);
            
            entryIterator = entries.iterator();
            
            while (entryIterator.hasNext()) 
            {
                entryIterator.next();
                
                entryIterator.remove();
                
                toRemove--;
                
                if (toRemove <= 0) {
                    break;
                }
            }
        }
    }
    
    /**
     * Stops the reaper thread
     */
    public void stop() {
        reaperThread.requestStop();
    }
    
    /*
     * Wrapper class so we can store a timestamp with the key
     */
    private static class KeyWithTime
    {
        private final String key;
        private final long time;

        public KeyWithTime(String key) {
            this(key, 0);
        }
        
        public KeyWithTime(String key, long time)
        {
            Check.notNull(key, "key");
            
            this.key = key;
            this.time = time;
        }
        
        @Override
        public boolean equals(Object obj)
        {
            if (obj == null) { return false; }
            if (obj == this) { return true; }
            if (obj.getClass() != getClass()) {
                return false;
            }
            
            KeyWithTime other = (KeyWithTime) obj;
            
            return this.key.equals(other.key);
        }
        
        @Override
        public int hashCode() {
            return key.hashCode();
        }
    }
    
    private class ReaperThread extends Thread
    {
        private final AtomicBoolean stop = new AtomicBoolean();
        
        private final long updateInterval;
        
        public ReaperThread(long updateInterval)
        {
            super("KeyCacheImpl Thread");
            
            this.updateInterval = updateInterval;
        }
        
        @Override
        public void run() 
        {
            logger.info("Starting KeyCacheImpl thread.");
            
            while (!stop.get())
            {
                try {
                    try {
                        Thread.sleep(updateInterval);
                    }
                    catch (InterruptedException e) {
                        /* ignore */
                    }
                    final int sz = size();
                    if (sz > 0) {
                        logger.info("Cleaning Key Cache. Cache size: " + sz);
                        cleanup();
                    }
                } 
                catch(RuntimeException e) {
                    logger.error("RuntimeException in KeyCacheImpl#Reaper", e);
                }
            }

            logger.info("Finishing KeyCacheImpl thread.");
        }
        
        public void requestStop()
        {
            stop.set(true);

            interrupt();
        }
    }
}
