/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import mitm.common.util.Check;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of CacheEntry that stores the streams in temporary files.
 * 
 * @author Martijn Brinkers
 *
 */
public class FileStreamCacheEntry implements CacheEntry
{
	private final static Logger logger = LoggerFactory.getLogger(FileStreamCacheEntry.class);
	
	/*
	 * The id of this entry.
	 */
	private final String id;
	
	/*
	 * General data object
	 */
	private Object object;

	/*
	 * Temporary file used to store the stream in
	 */
	private File file;
	
	public FileStreamCacheEntry(String id)
	{
		Check.notNull(id, "id");
		
		this.id = id;
	}
	
	@Override
    public String getId() {
		return id;
	}

	@Override
    public synchronized void setObject(Object object) {
		this.object = object;
	}

	@Override
    public synchronized Object getObject() {
		return object;
	}

	protected File getFile(boolean createInfNull) 
	throws IOException
	{
		if (file == null && createInfNull)
		{
			file = File.createTempFile("cache", null);
			
			file.deleteOnExit();
		}
		
		return file;
	}
	
	@Override
    public synchronized void store(InputStream input) 
	throws CacheException
	{
		try {
			FileOutputStream fos = new FileOutputStream(getFile(true));
			
			IOUtils.copy(input, fos);
			
			IOUtils.closeQuietly(fos);
		} 
		catch (FileNotFoundException e) {
			throw new CacheException(e);
		} 
		catch (IOException e) {
			throw new CacheException(e);
		}
	}

	@Override
    public synchronized void writeTo(OutputStream output) 
	throws CacheException
	{
		try {
			File file = getFile(false);
			
			if (file == null) {
				throw new CacheException("Entry is empty");
			}
			
			InputStream fis = new BufferedInputStream(new FileInputStream(file));
			
			try {
			    IOUtils.copy(fis, output);
			}
			finally {
			    IOUtils.closeQuietly(fis);
			}
		}
		catch (FileNotFoundException e) {
			throw new CacheException(e);
		} 
		catch (IOException e) {
			throw new CacheException(e);
		}
	}
	
	@Override
    public synchronized boolean isValid()
	throws CacheException
	{
		try {
			boolean hasStream = false;
			
			File file = getFile(false);

			if (file != null)
			{
				hasStream = file.exists();
			}
			
			return hasStream;
		}
		catch (FileNotFoundException e) {
			throw new CacheException(e);
		} 
		catch (IOException e) {
			throw new CacheException(e);
		}
	}

	@Override
    public synchronized boolean release() 
	throws CacheException
	{
		try {
			boolean deleted = true;
			
			File file = getFile(false);

			if (file != null && file.exists())
			{
				deleted = file.delete();
				
				if (!deleted) {
					logger.warn("File " + file + " could not be deleted.");
				}
			}
			
			return deleted;
		} 
		catch (IOException e) {
			throw new CacheException(e);
		}
	}
}
