/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import mitm.common.util.Check;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentCacheImpl implements ContentCache
{
	private final static Logger logger = LoggerFactory.getLogger(ContentCacheImpl.class);
	
	/*
	 * Time to wait (in milliseconds) before a new check for stale cache is started 
	 */
	private long checkDelay = DateUtils.MILLIS_PER_MINUTE * 1;
	
	/*
	 * Map from id -> CacheEntriesWrapper
	 */
	private final Map<String, CacheEntriesWrapper> wrappers = new HashMap<String, CacheEntriesWrapper>();
	
	/*
	 * Runnable that removes stale entries
	 */
	private AtomicReference<Reaper> atomicReaper = new AtomicReference<Reaper>();
	
	/*
	 * The reaper thread that will run reaper
	 */
	private Thread reaperThread;
	
	/*
	 * The maximum time a non-accessed item will be cached 
	 */
	private long maxStaleTime;
	
	public ContentCacheImpl(long maxStaleTime)
	{
		if (maxStaleTime < 0) {
			throw new IllegalArgumentException("maxStaleTime must be positive.");
		}
		
		this.maxStaleTime = maxStaleTime;
	}
	
	protected CacheEntriesWrapper getWrapper(String key, boolean createIfNull)
	{
		Check.notNull(key, "key");
		
		CacheEntriesWrapper wrapper = wrappers.get(key);
		
		if (wrapper == null && createIfNull) 
		{
			wrapper = new CacheEntriesWrapper(key);
			
			wrappers.put(key, wrapper);
		}

		return wrapper;
	}
	
	/**
	 * Sets the checkDelay time. This should not be called after the cache has been started.
	 */
	public void setCheckDelay(long checkDelay) {
		this.checkDelay = checkDelay;
	}
	
	/**
	 * Sets the maxStaleTime time.
	 */
	public synchronized void setMaxStaleTime(long maxStaleTime) {
		this.maxStaleTime = maxStaleTime;
	}
	
	public synchronized long getMaxStaleTime() {
		return maxStaleTime;
	}
	
	@Override
    public synchronized void addEntry(String key, CacheEntry entry)
	throws CacheException
	{
		getWrapper(key, true).getCacheEntries(true /* update last accessed */).put(entry.getId(), entry);
	}
	
	/*
	 * Checks if there are still entries with the given key, if not remove the entry with key
	 */
	private void checkForEmpty(CacheEntriesWrapper wrapper)
	{
		if (wrapper != null)
		{
			Map<String, CacheEntry> entries = wrapper.getCacheEntries(false /* NO update last accessed */);

			if (entries.size() == 0)
			{
				/*
				 * If all entries are removed we can completely remove the key'd wrapper
				 */
				wrappers.remove(wrapper.getKey());
			}
		}
	}
	
	@Override
    public synchronized void removeEntry(String key, String id)
	throws CacheException
	{
		CacheEntriesWrapper wrapper = getWrapper(key, false);

		if (wrapper != null)
		{
			Map<String, CacheEntry> entries = wrapper.getCacheEntries(false /* NO update last accessed */);

			CacheEntry entry = entries.get(id);
			
			if (entry != null)
			{
				if (entry.release()) {
					entries.remove(id);
				}
				else {
					logger.warn("Not all resources were released.");
				}
			}
			
			checkForEmpty(wrapper);
		}
	}

	@Override
    public synchronized void removeAllEntries(String key)
	throws CacheException
	{
		CacheEntriesWrapper wrapper = getWrapper(key, false);

		if (wrapper != null)
		{
			Map<String, CacheEntry> entries = wrapper.getCacheEntries(false /* NO update last accessed */);

			/*
			 * While stepping through all the entries we need to keep track of all the entries we need to remove
			 */
			List<CacheEntry> entriesToRemove = new LinkedList<CacheEntry>();
			
			for (CacheEntry entryToRemove : entries.values())
			{
				if (entryToRemove != null)
				{
					if (entryToRemove.release()) {
						entriesToRemove.add(entryToRemove);
					}
					else {
						logger.warn("Not all resources were released.");
					}
				}
			}
			
			for (CacheEntry entryToRemove : entriesToRemove) {
				entries.remove(entryToRemove.getId()); 
			}
			
			checkForEmpty(wrapper);
		}
	}
	
	@Override
    public synchronized CacheEntry getEntry(String key, String id)
	throws CacheException
	{
		CacheEntry entry = null;
		
		CacheEntriesWrapper wrapper = getWrapper(key, false);
		
		if (wrapper != null)
		{
			Map<String, CacheEntry> entries = wrapper.getCacheEntries(true /* update last accessed */);

			entry = entries.get(id);			

			if (entry != null && !entry.isValid())
			{
				/*
				 * Remove the entry because there it is no longer valid
				 */
				if (entry.release()) {
					entries.remove(entry.getId());
				}
				else {
					logger.warn("Not all resources were released.");
				}

				entry = null;

				checkForEmpty(wrapper);
			}
		}
		
		return entry;
	}

	@Override
    public synchronized List<CacheEntry> getAllEntries(String key)
	throws CacheException
	{
		List<CacheEntry> entriesToReturn = new LinkedList<CacheEntry>();
		
		CacheEntriesWrapper wrapper = getWrapper(key, false);
		
		if (wrapper != null)
		{
			Map<String, CacheEntry> allEntries = wrapper.getCacheEntries(true /* update last accessed */);
		
			/*
			 * While stepping through all the entries we need to keep track of all the entries we need to remove
			 */
			List<CacheEntry> entriesToRemove = new LinkedList<CacheEntry>();
			
			for (CacheEntry entry : allEntries.values())
			{
				if (entry.isValid()) {
					entriesToReturn.add(entry);
				}
				else {
					/*
					 * Remove the entry because there is no valid stream
					 */
					entriesToRemove.add(entry);
				}
			}
			
			for (CacheEntry toRemove : entriesToRemove)
			{
				/*
				 * Remove the entry because there it is no longer valid
				 */
				if (toRemove.release()) {
					allEntries.remove(toRemove.getId());
				}
				else {
					logger.warn("Not all resources were released.");
				}
			}			

			checkForEmpty(wrapper);
		}
		
		return entriesToReturn;
	}
	
	@Override
    public synchronized void start()
	{
		if (atomicReaper.get() == null)
		{
			atomicReaper.set(new Reaper());
			
			reaperThread = new Thread(atomicReaper.get(), "ContentCacheImpl Thread");
			
			reaperThread.setDaemon(true);
			reaperThread.start();
		}
	}

	@Override
    public void stop()
	{
		Reaper reaper = atomicReaper.get();
		
		if (reaper != null) 
		{
			reaper.stop();
			
			reaperThread.interrupt();

			try {
				reaperThread.join(DateUtils.MILLIS_PER_SECOND * 30);
			} 
			catch (InterruptedException e) {
				/* ignore */
			}
		}
	}

	public synchronized void kick()
	{
		if (reaperThread != null)	{
			reaperThread.interrupt();
		}
	}
	
	private static class CacheEntriesWrapper
	{
		/*
		 * Maps from id -> CacheEntry
		 */
		private final Map<String, CacheEntry> cacheEntries = new LinkedHashMap<String, CacheEntry>();
		
		/*
		 * Last time (in milliseconds) this CacheEntriesWrapper was accessed
		 */
		private long lastAccessed;
		
		/*
		 * Key of this wrapper
		 */
		private final String key;
		
		public CacheEntriesWrapper(String key)
		{
			Check.notNull(key, "key");
			
			this.key = key;
			
			this.lastAccessed = System.currentTimeMillis();
		}
		
		public Map<String, CacheEntry> getCacheEntries(boolean updateLastAccessed)
		{
			if (updateLastAccessed) {
				lastAccessed = System.currentTimeMillis();
			}
						
			return cacheEntries;
		}
		
		public long getLastAccessed() {
			return lastAccessed;
		}
		
		public String getKey() {
			return key;
		}
	}
	
	private class Reaper implements Runnable
	{
		private AtomicBoolean stop = new AtomicBoolean();
		
		private void removeStaleEntries(long maxStaleTime)
		{
			Collection<CacheEntriesWrapper> cached;
			
			/*
			 * We need to synchronize access to wrappers and create a copy of the values to make
			 * it thread safe. Because we make a copy we only have to lock the wrappers for a
			 * very short time.
			 */
			synchronized (this)
			{				
				cached = new ArrayList<CacheEntriesWrapper>(wrappers.values());
			}
			
			for (CacheEntriesWrapper wrapper : cached)
			{
				long now = System.currentTimeMillis();
								
				if ((now - wrapper.getLastAccessed()) > maxStaleTime)
				{
					String key = wrapper.getKey();
					
					logger.debug("Cached entries with key: " + key + " are stale and will be deleted.");

					try {
						removeAllEntries(key);
					} 
					catch (CacheException e) {
						logger.error("Exception while deleting stale entry with key: " + key, e);
					}
				}
			}
		}
		
		@Override
        public void run() 
		{
			while (!stop.get())
			{
				try {
					try {
						Thread.sleep(checkDelay);
					}
					catch (InterruptedException e) {
						/* ignore */
					}
	
					if (!stop.get())
					{
						/*
						 * get a local (in thread safe manner) copy of maxStaleTime
						 */
						long maxStaleTime = getMaxStaleTime();

						removeStaleEntries(maxStaleTime);
					}
				} 
				catch(RuntimeException e) {
					logger.error("RuntimeException in ContentCacheImpl#Reaper", e);
				}
			}
			
			/*
			 * Remove all cached items
			 */
			removeStaleEntries(-1);
		}
		
		public void stop() {
			stop.set(true);
		}
	}
}
