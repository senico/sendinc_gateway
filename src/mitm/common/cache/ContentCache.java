/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.util.List;

/**
 * ContentCache is a really basic cache that can be used to store objects and raw streams. This can be used for example by 
 * web applications that allows the user to upload files. The files need to be temporarily stored until the 
 * the user is finished with the page.
 * 
 * @author Martijn Brinkers
 *
 */
public interface ContentCache
{
	/**
	 * Associates the FileCacheEntry with the key.
	 */
	public void addEntry(String key, CacheEntry entry)
	throws CacheException;

	/**
	 * Removes the CacheEntry associated with key and with the given id.
	 */
	public void removeEntry(String key, String id)
	throws CacheException;

	/**
	 * Remove all cache entries with the given key.
	 */
	public void removeAllEntries(String key)
	throws CacheException;
	
	/**
	 * Returns a list of the files associated with the given key and the given id. If there is no item with the key 
	 * and id null is returned.
	 */
	public CacheEntry getEntry(String key, String id)
	throws CacheException;

	/**
	 * Returns a list of the files associated with the given key. If there are not items with the given key
	 * an empty set will be returned
	 */
	public List<CacheEntry> getAllEntries(String key)
	throws CacheException;
	
	/**
	 * Start the cache (can for example start a reaper thread that removes stale cache items)
	 */
	public void start();

	/**
	 * Stops the cache
	 */
	public void stop();
}
