/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * CacheEntry is a container for storing stream content 
 * cached file.
 * 
 * @author Martijn Brinkers
 *
 */
public interface CacheEntry
{
	/**
	 * The name of the this element. Can be used to identify a specific element.
	 */
	public String getId();

	/**
	 * A general purpose property that can be used to store meta info. The meta info is typically kept in memory so
	 * it must not be too large.
	 */
	public Object getObject();
	public void setObject(Object info);
	
	/**
	 * Stores the input content into the entry. It is implementation specific whether data is stored on disk, memory, database etc.
	 */
	public void store(InputStream input)
	throws CacheException;
	
	/**
	 * Writes this entries content into the output stream.
	 */
	public void writeTo(OutputStream output)
	throws CacheException;
	
	/**
	 * True if the entry is still valid. If false is returned the cache might remove the entry.
	 */
	public boolean isValid()
	throws CacheException;
	
	/**
	 * When called this cache entry should cleanup all resources. It returns true if all resources are released, false
	 * if not
	 */
	public boolean release()
	throws CacheException;
}
