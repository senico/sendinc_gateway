/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RateCounterImpl implements RateCounter
{
	private final static Logger logger = LoggerFactory.getLogger(RateCounterImpl.class);
	
	/*
	 * Time to wait (in milliseconds) before a new check for items that extends their lifetime 
	 */
	private AtomicLong checkDelay = new AtomicLong(DateUtils.MILLIS_PER_MINUTE * 1);

	/*
	 * Runnable that removes entries that have extended their lifetime.
	 */
	private AtomicReference<Reaper> atomicReaper = new AtomicReference<Reaper>();
	
	/*
	 * The reaper thread that will run reaper
	 */
	private Thread reaperThread;

	/*
	 * The map mapping key to sub keys
	 */
	private final Map<String, Set<SubKeyItem>> keyMap = new HashMap<String, Set<SubKeyItem>>(); 
	
	private static class SubKeyItem
	{
		private final String id;
		
		private final long lifetime;
		
		private final long created;
		
		public SubKeyItem(String id, long lifetime)
		{
			Check.notNull(id, "id");
			
			this.id = id;
			this.lifetime = lifetime;
			
			this.created = System.currentTimeMillis();
		}

		public String getId() {
			return id;
		}

		public boolean isValid() 
		{
			long now = System.currentTimeMillis();
			
			return (now - created) < lifetime;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (obj == this) {
				return true;
			}
			
			if (!(obj instanceof SubKeyItem)) {
				return false;
			}
			
			SubKeyItem other = (SubKeyItem) obj;
			
			return this.id.equals(other.getId());
		}
		
		@Override
		public int hashCode() {
			return id.hashCode();
		}
	}
	
	private Set<SubKeyItem> getKeyedItems(String key, boolean createIfNull)
	{
		Set<SubKeyItem> items = keyMap.get(key);
		
		if (items == null && createIfNull)
		{
			items = new HashSet<SubKeyItem>();
			
			keyMap.put(key, items);
		}
		
		return items;
	}
	
	private synchronized void checkValidity(String key)
	{
		Set<SubKeyItem> items = getKeyedItems(key, false);
		
		if (logger.isDebugEnabled()) {
		    logger.debug("key {}, #items {}", key, CollectionUtils.getSize(items));
		}
		
		if (items != null)
		{
			List<SubKeyItem> itemsToRemove = new LinkedList<SubKeyItem>();
			
			for (SubKeyItem item : items)
			{
				if (!item.isValid()) {
					itemsToRemove.add(item);
				}
			}
			
			items.removeAll(itemsToRemove);
			
			/*
			 * If there are no more items left associated with the key we can remove the
			 * key
			 */
			if (items.size() == 0) {
				keyMap.remove(key);
			}
		}
	}
	
	@Override
    public synchronized void addKey(String key, String subKey, long lifetime)
	{
		Check.notNull(key, "key");
		Check.notNull(subKey, "subKey");
		
		Set<SubKeyItem> items = getKeyedItems(key, true);
		
		items.add(new SubKeyItem(subKey, lifetime));
	}

	@Override
    public synchronized int getItemCount(String key)
	{
		Check.notNull(key, "key");
		
		int count = 0;

		checkValidity(key);
		
		Set<SubKeyItem> items = getKeyedItems(key, false);
		
		if (items != null) {
			count = items.size();
		}
		
		return count;
	}
	
	@Override
    public synchronized void start()
	{
		if (atomicReaper.get() == null)
		{
			atomicReaper.set(new Reaper());
			
			reaperThread = new Thread(atomicReaper.get(), "RateCounterImpl Thread");
			
			reaperThread.setDaemon(true);
			reaperThread.start();
			
			logger.info("RateCounterImpl reaper thread started.");
		}
	}

	@Override
    public void stop() 
	{
		Reaper reaper = atomicReaper.get();
		
		if (reaper != null) 
		{
			reaper.stop();
			
			reaperThread.interrupt();

			try {
				reaperThread.join(DateUtils.MILLIS_PER_SECOND * 30);
			} 
			catch (InterruptedException e) {
				/* ignore */
			}
		}
	}
	
	private class Reaper implements Runnable
	{
		private AtomicBoolean stop = new AtomicBoolean();
		
		@Override
        public void run() 
		{
			while (!stop.get())
			{
				try {
					try {
						Thread.sleep(checkDelay.get());
					}
					catch (InterruptedException e) {
						/* ignore */
					}
	
					if (!stop.get())
					{
						/*
						 * We need to make a copy of all keys because while stepping through the items
						 * some items will be removed (which can result if a concurrent modification exception
						 * if done on the collection you are stepping through).
						 */
						Set<String> keys;
						
						synchronized (this) {
							keys = new HashSet<String>(keyMap.keySet());
						}
						
						logger.debug("{} number of keys in keyMap", keys.size());
						
						for (String key : keys)	{
							checkValidity(key);
						}
					}
				} 
				catch(RuntimeException e) {
					logger.error("RuntimeException in RateCounterImpl#Reaper", e);
				}
			}
		}
		
		public void stop() {
			stop.set(true);
		}
	}
	
	/**
	 * Sets the checkDelay time.
	 */
	public void setCheckDelay(long checkDelay)
	{
		this.checkDelay.set(checkDelay);
		
		Reaper reaper = atomicReaper.get();

		if (reaper != null) {
			reaperThread.interrupt();
		}
	}
	
	/*
	 * for testing purposes
	 */
	protected synchronized Map<String, Set<SubKeyItem>> getKeyMap() {
		return new HashMap<String, Set<SubKeyItem>>(keyMap);
	}
}
