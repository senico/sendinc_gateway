package mitm.common.postfix;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import mitm.common.util.Check;
import mitm.common.util.ProcessRunner;
import mitm.common.util.SizeUtils;

import org.apache.commons.lang.time.DateUtils;

public class PostfixBase
{
    /*
     * Maximal size of a command output (just to protect against a runaway command)
     */
    protected final static long MAX_OUTPUT_LENGTH = SizeUtils.MB * 10;
    
    /*
     * The maximum time a command may run after which it is destroyed
     */
    protected final static long TIMEOUT = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * The common script that will delegate the calls to the postfix binaries
     */
    private final File script;
    
    protected PostfixBase(File script)
    {
        Check.notNull(script, "script");
        
        this.script = script;
    }
    
    protected String getScriptPath() {
        return script.getPath();
    }
    
    protected List<String> getBaseCMD()
    {
        List<String> cmd = new LinkedList<String>();
        
        cmd.add(getScriptPath());
        
        return cmd;
    }
    
    protected ProcessRunner createProcessRunner()
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setTimeout(TIMEOUT);
        
        return runner;
    }
}
