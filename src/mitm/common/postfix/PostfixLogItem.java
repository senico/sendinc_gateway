/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.text.StrBuilder;

/**
 * A log entry for the Postfix log. The Postfix log entry is made up of (possibly) multiple lines belonging to 
 * one message (identified by the queue ID. There are log lines that do not have a queue ID 
 * (like connect, disconnect etc.). Although some of these queue-id'less lines can be associated with a queue ID
 * (connect for example) for now these will be treated as separated lines.
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PostfixLogItem
{
    /*
     * The queue ID (can be null)
     */
    @XmlElement
    private String queueID;

    /*
     * The log lines belonging to this queue ID
     */
    @XmlElement
    private List<String> lines;

    protected PostfixLogItem() {
        /*
         * JAXB requires default constructor
         */
    }
    
    public PostfixLogItem(String queueID)
    {
        this.queueID = queueID;
        this.lines = new ArrayList<String>();
    }
    
    public String getQueueID() {
        return queueID;
    }
    
    public List<String> getLines() {
        return lines;
    }
    
    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder(256);
        
        sb.append("queueID: ").append(queueID).appendSeparator("; ");
        sb.append("Lines: ").appendWithSeparators(lines, ", ");
        
        return sb.toString();
    }
}
