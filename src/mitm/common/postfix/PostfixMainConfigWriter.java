/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.common.locale.CharacterEncoding;
import mitm.common.util.Check;
import mitm.common.util.ProcessRunner;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;

/**
 * Java interface to the copy-postfix-main-confix.sh script that copies it's input to the postfix main config (main.cf).
 * The copy-postfix-main-confix.sh script must be allowed to overwrite the postfix main config. The best way to to this
 * is by adding copy-postfix-main-confix.sh to the sudoers list and allow it to run without password.
 * 
 * @author Martijn Brinkers
 *
 */
public class PostfixMainConfigWriter 
{
    /*
     * The maximum time a command may run after which it is destroyed
     */
    private final static long TIMEOUT = 30 * DateUtils.MILLIS_PER_SECOND;
    
    /*
     * The script that will be executed to write the Postfix main config
     */
    private final File script;
    
    public PostfixMainConfigWriter(File script)
    {
        Check.notNull(script, "script");
        
        this.script = script;
    }
    
	/**
	 * Sets the new postfix main config by executing the script 
	 * 
	 * @throws PostfixException
	 */
	public void writeConfig(String config)
	throws PostfixException
	{
		Check.notNull(config, "config");
		
        List<String> cmd = new LinkedList<String>();
		
        cmd.add(script.getPath());
        
        try {
            ProcessRunner runner = new ProcessRunner();
            
            runner.setTimeout(TIMEOUT);
            runner.setInput(IOUtils.toInputStream(config, CharacterEncoding.US_ASCII));
                        
            runner.run(cmd);
		} 
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}
}
