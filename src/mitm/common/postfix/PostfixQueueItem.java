/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An entry from the Postfix mail queue
 * 
 * @author Martijn Brinkers
 *
 */
@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PostfixQueueItem 
{
    @XmlElement
	private String queueID;

    @XmlElement
	private PostfixQueueStatus status;

    @XmlElement
	private int messageSize;

    @XmlElement
	private String arrivalTime;
    
    @XmlElement
	private String sender;
    
    @XmlElement
	private String failure;
    
    @XmlElement
	private List<String> recipients;
	
    protected PostfixQueueItem() {
        /* JAXB requires a default constructor */        
    }
    
	public PostfixQueueItem(String queueID, PostfixQueueStatus status, int messageSize, String arrivalTime, String sender, 
			String failure, List<String> recipients)
	{
		this.queueID = queueID;
		this.status = status;
		this.messageSize = messageSize;
		this.arrivalTime = arrivalTime;
		this.sender = sender;
		this.recipients = recipients;
		this.failure = failure;
	}
	
	/**
	 * The queue ID of this item.
	 */
	public String getQueueID() {
		return queueID;
	}
	
	/**
	 * Returns the queue status.
	 */
	public PostfixQueueStatus getQueueStatus() {
		return status;
	}
	
	/**
	 * The size of the message in octets.
	 */
	public int getMessageSize() {
		return messageSize;
	}
	
	/**
	 * The arrival time. 
	 * 
	 * According to RFC 3164 the date should always be encoded as follows: 
	 * 
	 * "The TIMESTAMP field is the local time and is in the format of "Mmm dd hh:mm:ss" (without the quote marks)..."
	 * 
	 * We will not parse the date because we do not require it (we just use the date for displaying purposes)
	 */
	public String getArrivalTime() {
		return arrivalTime;
	}
	
	/**
	 * The sender of the message 
	 */
	public String getSender() {
		return sender;
	}
	
	/**
	 * The recipients that still need to be delivered. 
	 */
	public List<String> getRecipients() {
		return recipients;
	}
	
	/**
	 * The reason for any possible delivery failure.  
	 */
	public String getFailure() {
		return failure;
	}
}
