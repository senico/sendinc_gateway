/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.io.File;
import java.io.IOException;
import java.util.List;

import mitm.common.util.MiscStringUtils;
import mitm.common.util.ProcessRunner;
import mitm.common.util.SizeLimitedOutputStream;

import org.apache.commons.io.output.ByteArrayOutputStream;

/**
 * Interface to the postqueue command. This is done by calling a script that in turn calls the postfix command.
 * 
 * @author Martijn Brinkers
 *
 */
public class Postqueue extends PostfixBase
{
    private static final String POSTQUEUE_CMD = "postqueue";
    
    public Postqueue(File script)
    {
        super(script);
    }

    /**
	 * Flush the queue: attempt to deliver all queued mail.
	 * 
	 * @throws PostfixException
	 */
	public void flush()
	throws PostfixException
	{
        List<String> cmd = getBaseCMD();

        cmd.add(POSTQUEUE_CMD);
        
        cmd.add("-f");

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		} 
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}

	/**
	 * Schedule immediate delivery of deferred mail with the specified queue ID.
	 * 
	 * @param queueID
	 * @throws PostfixException
	 */
	public void deliver(String queueID) 
	throws PostfixException
	{
		if (!PostfixUtils.isValidQueueID(queueID)) {
			throw new IllegalQueueIDException(queueID);
		}
		
        List<String> cmd = getBaseCMD();

        cmd.add(POSTQUEUE_CMD);
        
        cmd.add("-i");
        cmd.add(queueID);

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		} 
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}
	
	/**
	 *  Produce a traditional sendmail-style queue listing.  This option implements the traditional 
	 *  mailq command, by  contacting  the  Postfix  showq(8) daemon.
	 *
     * Each  queue entry shows the queue file ID, message size, arrival time, sender, and the recipients 
     * that still need to be delivered.  If mail could not be delivered upon the last attempt, the reason 
     * for failure is shown. This mode of operation is implemented by executing the postqueue(1) command. 
     * The queue ID string is followed by an optional status character:
     *
     *  * The message is in the active queue, i.e. the message is selected for delivery.
     *
     *  ! The message is in the hold queue, i.e. no further delivery attempt will be made until the mail 
     *    is taken off hold.
     *
	 * @return
	 * @throws PostfixException
	 */
	public String list() 
	throws PostfixException
	{
        List<String> cmd = getBaseCMD();

        cmd.add(POSTQUEUE_CMD);
        
        cmd.add("-p");

        try {
            ProcessRunner runner = createProcessRunner();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, MAX_OUTPUT_LENGTH);
            
            runner.setOutput(slos);
            
            runner.run(cmd);
            
            return MiscStringUtils.toAsciiString(bos.toByteArray());
		}
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}
}
