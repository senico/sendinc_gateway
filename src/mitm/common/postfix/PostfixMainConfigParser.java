/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

public class PostfixMainConfigParser
{
	private static final String LF = "\n";

	/*
	 * name prefix of non name/value lines
	 */
	public static final String OTHER_LINE_NAME = "other_";
	
	/*
	 * Pattern for a continuation line
	 */
	private static final Pattern CONTINUATION = Pattern.compile("^\\s+.*");

	/*
	 * Pattern for a name value line
	 */
	private static final Pattern NAME_VALUE = Pattern.compile("(?s)^(\\S+)\\s*=\\s*(.*)");
	
	private static List<String> normalize(Reader config) 
	{
		List<String> lines = new LinkedList<String>();

		LineIterator it = new LineIterator(config);

		String line = ""; 

		try {
			while (it.hasNext())
			{
				String nextLine = it.nextLine();

				Matcher matcher = CONTINUATION.matcher(nextLine);

				if (!nextLine.trim().startsWith("#") && matcher.matches())
				{
					/* its a continuation line */
					line = line + LF + nextLine;
				}
				else {
					matcher = NAME_VALUE.matcher(nextLine);

					if (matcher.matches())
					{
						/* its a name/value line */
						
						if (StringUtils.isNotEmpty(line))
						{
							/* store the previous line because it is finished */
							lines.add(line);
						}

						line = nextLine;
					}
					else {
						/* it's some other line like a comment */
						if (StringUtils.isNotEmpty(line))
						{
							/* store the previous line because it is finished */
							lines.add(line);
						}
						lines.add(nextLine);
						
						line = "";
					}
				}
			}
			if (StringUtils.isNotEmpty(line)) {
				lines.add(line);
			}
		} 
		finally {
			LineIterator.closeQuietly(it);
		}
		
		return lines;
	}
	
	public static LinkedHashMap<String, String> parse(Reader config)
	{
		LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		
		List<String> lines = normalize(config);

		int i = 0;
		
		for (String line : lines)
		{
			Matcher matcher = NAME_VALUE.matcher(line);

			if (!line.startsWith("#") && matcher.matches())
			{
				String value = matcher.group(2);
				
				if (value == null) {
					value = "";
				}
				
				values.put(matcher.group(1), value);
			}
			else {
				values.put(OTHER_LINE_NAME + i++, line);
			}
		}
		
		return values;
	}
}
