/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class PostfixMainConfigBuilder
{
	/*
	 * The property name prefix for each property value
	 */
	private static final String PREFIX = "djigzo_";

	private static final String MY_HOSTNAME = "myhostname";
	private static final String MY_DESTINATION = "mydestination";
	private static final String MY_NETWORKS = "mynetworks";
	private static final String RELAY_HOST = "relayhost";
	private static final String RELAY_HOST_MX_LOOKUP = "relayhost_mx_lookup";
	private static final String RELAY_HOST_PORT = "relayhost_port";
	private static final String RELAY_DOMAINS = "relay_domains";
	private static final String BEFORE_FILTER_MESSAGE_SIZE_LIMIT = "before_filter_message_size_limit";
	private static final String AFTER_FILTER_MESSAGE_SIZE_LIMIT = "after_filter_message_size_limit";
	private static final String MAILBOX_SIZE_LIMIT = "mailbox_size_limit";
	private static final String SMTP_HELO_NAME = "smtp_helo_name";
	private static final String RELAY_TRANSPORT_HOST = "relay_transport_host";
	private static final String RELAY_TRANSPORT_HOST_MX_LOOKUP = "relay_transport_host_mx_lookup";
	private static final String RELAY_TRANSPORT_HOST_PORT = "relay_transport_host_port";
	private static final String REJECT_UNVERIFIED_RECIPIENT = "reject_unverified_recipient";
	private static final String UNVERIFIED_RECIPIENT_REJECT_CODE = "unverified_recipient_reject_code";	
	private static final String PARENT_DOMAIN_MATCHES_SUBDOMAINS = "parent_domain_matches_subdomains";
	
	/*
	 * Maps all the 'name/value' pairs of the postfix main configuration. A non name/value pair is mapped
	 * as other_xx -> line 
	 */
	private final Map<String, String> values;
	
	public PostfixMainConfigBuilder(Reader config)
	{
		Check.notNull(config, "config");
		
		this.values = PostfixMainConfigParser.parse(config);
	}
	
	public String getMyHostname() {
		return getValue(MY_HOSTNAME, "");
	}
	
	public void setMyHostname(String myHostname) {
		setValue(MY_HOSTNAME, myHostname);
	}
	
	public Set<String> getMyDestinations() {
		return splitMultivalueUnmodifiable(getValue(MY_DESTINATION, ""));
	}

	public void setMyDestination(Set<String> destination) {
		setValue(MY_DESTINATION, StringUtils.join(destination, ", "));
	}

	public Set<String> getMyNetworks() {
		return splitMultivalueUnmodifiable(getValue(MY_NETWORKS, ""));
	}

	public void setMyNetworks(Set<String> networks) {
		setValue(MY_NETWORKS, StringUtils.join(networks, ", "));
	}

	public String getRelayHost() {
		return getValue(RELAY_HOST, "");
	}

	public void setRelayHost(String relayHost) {
		setValue(RELAY_HOST, relayHost);
	}

	public boolean isRelayHostMxLookup() {
		return StringUtils.isNotEmpty(getValue(RELAY_HOST_MX_LOOKUP, ""));
	}
	
	public void setRelayHostMxLookup(boolean value)
	{
		setValue(RELAY_HOST_MX_LOOKUP, value ? "mx" : null);
	}
	
	public Integer getRelayHostPort() {
		return getIntValue(RELAY_HOST_PORT, 25);
	}

	public void setRelayHostPort(Integer port) {
		setIntValue(RELAY_HOST_PORT, port);
	}
	
	public Set<String> getRelayDomains() {
		return splitMultivalueUnmodifiable(getValue(RELAY_DOMAINS, ""));
	}

	public void setRelayDomains(Set<String> relayDomains) {
		setValue(RELAY_DOMAINS, StringUtils.join(relayDomains, ", "));
	}

	public Integer getBeforeFilterMessageSizeLimit() {
		return getIntValue(BEFORE_FILTER_MESSAGE_SIZE_LIMIT, 10240000);
	}

	public void setBeforeFilterMessageSizeLimit(Integer limit) {
		setIntValue(BEFORE_FILTER_MESSAGE_SIZE_LIMIT, limit);
	}

	public Integer getAfterFilterMessageSizeLimit() {
		return getIntValue(AFTER_FILTER_MESSAGE_SIZE_LIMIT, 102400000);
	}

	public void setAfterFilterMessageSizeLimit(Integer limit) {
		setIntValue(AFTER_FILTER_MESSAGE_SIZE_LIMIT, limit);
	}

	public Integer getMailboxSizeLimit() {
		return getIntValue(MAILBOX_SIZE_LIMIT, 51200000);
	}

	public void setMailboxSizeLimit(Integer limit) {
		setIntValue(MAILBOX_SIZE_LIMIT, limit);
	}
	
	public String getSMTPHeloName() {
		return getValue(SMTP_HELO_NAME, "");
	}

	public void setSMTPHeloName(String name) {
		setValue(SMTP_HELO_NAME, name);
	}
	
	public String getRelayTransportHost() {
		return getValue(RELAY_TRANSPORT_HOST, "");
	}

	public void setRelayTransportHost(String relayTransportHost) {
		setValue(RELAY_TRANSPORT_HOST, relayTransportHost);
	}

	public boolean isRelayTransportHostMxLookup() {
		return StringUtils.isNotEmpty(getValue(RELAY_TRANSPORT_HOST_MX_LOOKUP, ""));
	}
	
	public void setRelayTransportHostMxLookup(boolean value)
	{
		setValue(RELAY_TRANSPORT_HOST_MX_LOOKUP, value ? "mx" : null);
	}
	
	public Integer getRelayTransportHostPort() {
		return getIntValue(RELAY_TRANSPORT_HOST_PORT, 25);
	}

	public void setRelayTransportHostPort(Integer port) {
		setIntValue(RELAY_TRANSPORT_HOST_PORT, port);
	}
	
	public boolean isRejectUnverifiedRecipient() {
		return StringUtils.isNotEmpty(getValue(REJECT_UNVERIFIED_RECIPIENT, ""));
	}
	
	public void setRejectUnverifiedRecipient(boolean value)
	{
		setValue(REJECT_UNVERIFIED_RECIPIENT, value ? "reject" : null);
	}

	public String getUnverifiedRecipientRejectCode() {
		return getValue(UNVERIFIED_RECIPIENT_REJECT_CODE, "450");
	}

	public void setUnverifiedRecipientRejectCode(String code) {
		setValue(UNVERIFIED_RECIPIENT_REJECT_CODE, code);
	}
	
	public boolean isMatchSubdomains()
	{
	    Set<String> values = splitMultivalueUnmodifiable(getValue(PARENT_DOMAIN_MATCHES_SUBDOMAINS, ""));
	    
	    return values.contains("relay_domains");
	}
	
	public void setMatchSubdomains(boolean match)
	{
        Set<String> matchingDomains = splitMultivalue(getValue(PARENT_DOMAIN_MATCHES_SUBDOMAINS, ""));
	    
        if (match) {
            matchingDomains.add("relay_domains");
        }
        else {
            matchingDomains.remove("relay_domains");
        }
        
	    setValue(PARENT_DOMAIN_MATCHES_SUBDOMAINS, StringUtils.join(matchingDomains, ", "));
	}
	
	public void writeConfig(Writer writer) 
	throws IOException
	{
		for (Map.Entry<String,String> entry : values.entrySet())
		{
			String name = entry.getKey();
			String value = entry.getValue();
			
			if (name.startsWith(PostfixMainConfigParser.OTHER_LINE_NAME)) {
				writer.write(value);
			}
			else {
				writer.write(name);
				writer.write(" = ");
				writer.write(StringUtils.defaultString(value));
			}
			
			writer.write("\n");
		}
	}
	
	private void setValue(String name, String value){
		values.put(PREFIX + name, value);
	}
	
	private String getValue(String name, String defaultIfNull)
	{
		String value = values.get(PREFIX + name);
		
		if (value == null) {
			value = defaultIfNull;
		}
		else {
			value = value.trim();
		}
		
		return value;
	}
	
	private Integer getIntValue(String name, Integer defaultValue) {
		return NumberUtils.toInt(getValue(name, null), defaultValue);
	}

	private void setIntValue(String name, Integer value){
		values.put(PREFIX + name, value != null ? value.toString() : null);
	}

    private static Set<String> splitMultivalue(String value)
    {
        if (StringUtils.isEmpty(value)) {
            return new LinkedHashSet<String>();
        }
        
        /*
         * Multi value fields can either be separated by comma's or whitespace
         */
        String split = (value.indexOf(',') != -1) ? ",\\s*" : "\\s+";
        
        return CollectionUtils.asLinkedHashSet(value.split(split));
    }
	
	private static Set<String> splitMultivalueUnmodifiable(String value) {
		return Collections.unmodifiableSet(splitMultivalue(value));
	}
}
