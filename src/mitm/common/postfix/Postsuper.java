/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import java.io.File;
import java.io.IOException;
import java.util.List;

import mitm.common.util.ProcessRunner;

/**
 * Interface to the postsuper command. This is done by calling a script that in turn calls the postfix command.
 * 
 * @author Martijn Brinkers
 *
 */
public class Postsuper extends PostfixBase
{
    private static final String POSTSUPER_CMD = "postsuper";
    
    public Postsuper(File script)
    {
        super(script);
    }
	
	/**
	 * Delete one message with the named queue ID from the named mail queue(s).
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 * 
	 * mailQueue is the name of the mail queue from which a message should be deleted. 
	 * If mailQueue is null it will default to (hold, incoming, active and deferred).
 	 * 
	 * @throws PostfixException
	 */
	public void delete(String queueID, String mailQueue)
	throws PostfixException
	{
		if (!PostfixUtils.isValidQueueID(queueID)) {
			throw new IllegalQueueIDException(queueID);
		}

		if (mailQueue != null && !PostfixUtils.isValidQueue(mailQueue)) {
			throw new IllegalQueueException(mailQueue);
		}
		
        List<String> cmd = getBaseCMD();
		
        cmd.add(POSTSUPER_CMD);
        
        cmd.add("-d");
        cmd.add(queueID);
        
        if (mailQueue != null) {
        	cmd.add(mailQueue);
        }

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		}
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}

	/**
	 * Put mail "on hold" so that no attempt is made to deliver it.
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 * 
	 * mailQueue is the name of the mail queue from which a message should be deleted. 
	 * If mailQueue is null it will default to (hold, incoming, active and deferred).
 	 * 
	 * @throws PostfixException
	 */
	public void hold(String queueID, String mailQueue)
	throws PostfixException
	{
		if (!PostfixUtils.isValidQueueID(queueID)) {
			throw new IllegalQueueIDException(queueID);
		}

		if (mailQueue != null && !PostfixUtils.isValidQueue(mailQueue)) {
			throw new IllegalQueueException(mailQueue);
		}
		
        List<String> cmd = getBaseCMD();
        
        cmd.add(POSTSUPER_CMD);
        
        cmd.add("-h");
        cmd.add(queueID);
        
        if (mailQueue != null) {
        	cmd.add(mailQueue);
        }

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		}
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}
	
	/**
	 * Release  mail  that  was  put  "on  hold".
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 * 
	 * mailQueue is the name of the mail queue from which a message should be deleted. 
	 * If mailQueue is null it will default to (hold, incoming, active and deferred).
 	 * 
	 * @throws PostfixException
	 */
	public void release(String queueID, String mailQueue)
	throws PostfixException
	{
		if (!PostfixUtils.isValidQueueID(queueID)) {
			throw new IllegalQueueIDException(queueID);
		}

		if (mailQueue != null && !PostfixUtils.isValidQueue(mailQueue)) {
			throw new IllegalQueueException(mailQueue);
		}
		
        List<String> cmd = getBaseCMD();
        
        cmd.add(POSTSUPER_CMD);
        
        cmd.add("-H");
        cmd.add(queueID);
        
        if (mailQueue != null) {
        	cmd.add(mailQueue);
        }

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		}
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}

	/**
	 * Requeue  the  message  with  the named queue ID from the named mail queue(s).
	 * 
	 * If queueID is ALL all messages from the named mail queue will be deleted.
	 * 
	 * mailQueue is the name of the mail queue from which a message should be deleted. 
	 * If mailQueue is null it will default to (hold, incoming, active and deferred).
 	 * 
	 * @throws PostfixException
	 */
	public void requeue(String queueID, String mailQueue)
	throws PostfixException
	{
		if (!PostfixUtils.isValidQueueID(queueID)) {
			throw new IllegalQueueIDException(queueID);
		}

		if (mailQueue != null && !PostfixUtils.isValidQueue(mailQueue)) {
			throw new IllegalQueueException(mailQueue);
		}
		
        List<String> cmd = getBaseCMD();
        
        cmd.add(POSTSUPER_CMD);
        
        cmd.add("-r");
        cmd.add(queueID);
        
        if (mailQueue != null) {
        	cmd.add(mailQueue);
        }

        try {
            ProcessRunner runner = createProcessRunner();

            runner.run(cmd);
		}
        catch (IOException e) {
        	throw new PostfixException(e);
		}
	}
}
