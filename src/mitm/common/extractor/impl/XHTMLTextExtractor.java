/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 */
package mitm.common.extractor.impl;

import java.io.IOException;
import java.io.Writer;

import javax.activation.MimeTypeParseException;

import mitm.common.extractor.TextExtractorContext;
import mitm.common.mail.MimeUtils;
import mitm.common.util.RewindableInputStream;

import org.apache.commons.io.input.CloseShieldInputStream;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.ext.DefaultHandler2;

/**
 * 
 * XHTML text extractor. The extractor can cope with malformed HTML and converts any HTML to XHTML
 * while scanning for text. 
 * 
 * @author Martijn Brinkers
 *
 */
public class XHTMLTextExtractor extends AbstractXMLBaseTextExtractor 
{
    private final static Logger logger = LoggerFactory.getLogger(XHTMLTextExtractor.class);
        
    /*
     * Creating a HTMLSchema is a heavy action so we will create a HTMLSchema
     * which can be reused in a thread safe manner.
     */
    private static final HTMLSchema CACHED_HTML_SCHEMA = new ThreadSafeHTMLSchema();
    
    /*
     * SAX event handler that looks for <meta http-equiv="Content-Type" content=""> 
     * tags in the header section and tries to retrieve the encoding.
     */
    private static class FindEncodingHandler extends DefaultHandler2
    {        
        /*
         * Some sanity check
         */
        private static final int MAX_ATTRIBUTES = 64;

        /*
         * True if we found the head tag
         */
        private boolean headFound;
        
        /*
         * The content encoding
         */
        private String encoding;
        
        private String parseContentType(String contentType)
        {
            String result = null;
            
            try {
                result = MimeUtils.getCharsetFromContentType(contentType);
            }
            catch (MimeTypeParseException e) {
                logger.debug("The following Content-Type could not be parsed: " + contentType);
            }
            
            return result;
        }
        
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        throws SAXException
        {
            if ("head".equalsIgnoreCase(qName))
            {
                headFound = true;
                
                return;
            }
            
            if (headFound && "meta".equalsIgnoreCase(qName))
            {
                if (attributes != null)
                {
                    String contentType = null;
                    
                    boolean httpEquivFound = false;

                    int attrToCheck = attributes.getLength();
                    
                    /*
                     * Limit the number of attributes as a sanity check
                     */
                    if (attrToCheck > MAX_ATTRIBUTES)
                    {
                        logger.warn("Number of attributes exceeds MAX_ATTRIBUTES " + MAX_ATTRIBUTES);
                        
                        attrToCheck = MAX_ATTRIBUTES;
                    }
                    
                    for (int i = 0; i < attrToCheck; i++)
                    {
                        String attrName = attributes.getQName(i);
                        
                        if ("http-equiv".equalsIgnoreCase(attrName) && 
                                "content-type".equalsIgnoreCase(attributes.getValue(i)))
                        {
                            httpEquivFound = true;
                            
                            if (contentType != null) {
                                break;
                            }
                        }
                        
                        if ("content".equalsIgnoreCase(attrName))
                        {
                            contentType = attributes.getValue(i);
                            
                            if (httpEquivFound) {
                                break;
                            }
                        }
                        
                    }
                    
                    if (httpEquivFound && contentType != null)
                    {
                        this.encoding = parseContentType(contentType);

                        /*
                         * We are done scanning so force a stop
                         */
                        throw new StopParsingException();
                    }
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        throws SAXException
        {
            if (headFound && "head".equalsIgnoreCase(qName)) {
                /*
                 * Done looking for encoding. We assume that the meta tags are within 
                 * the header section
                 */
                throw new StopParsingException();
            }
        }
        
        public String getEncoding() {
            return encoding;
        }
    }
    
    private Parser createParser()
    throws SAXNotRecognizedException, SAXNotSupportedException
    {
        Parser parser = new Parser();
        
        /*
         * Creating a HTMLSchema is a heavy action so reuse the HTMLSchema
         */
        parser.setProperty(Parser.schemaProperty, CACHED_HTML_SCHEMA);

        return parser;
    }
    
    @Override
    protected String getEncodingFromDocument(RewindableInputStream input, TextExtractorContext context)
    throws IOException, SAXException
    {
        String encoding = null;
        
        long savedPosition = input.getPosition();
        
        try {
            /*
             * Parse the HTML and start looking for <meta http-equiv="Content-Type" content="">
             * in the header section of the XHTML.
             */
            Parser parser = createParser();
            
            /*
             * Wrap input in a CloseShieldInputStream to prevent the parser from closing the input
             */
            InputSource inputSource = new InputSource(new CloseShieldInputStream(input));

            FindEncodingHandler findEncodingHandler = new FindEncodingHandler();
            
            parser.setContentHandler(findEncodingHandler);
            
            try {
                parser.parse(inputSource);
            }
            catch(StopParsingException e) {
                /*
                 * content encoding was found. 
                 */
            }
            
            encoding = findEncodingHandler.getEncoding();
        }
        finally {
            input.setPosition(savedPosition);
        }
        
        return encoding;
    }

    @Override
    protected void parse(RewindableInputStream input, TextExtractorContext context, Writer writer)
    throws IOException, SAXException
    {
        Parser parser = createParser();
        
        InputSource inputSource = new InputSource(new CloseShieldInputStream(input));

        inputSource.setEncoding(getEncoding(input, context));
        
        ExtractTextHandler parserHandler = new ExtractTextHandler(writer);
        
        parser.setContentHandler(parserHandler);
        /*
         * Make sure comments are extracted as well
         */
        parser.setProperty(Parser.lexicalHandlerProperty, parserHandler);
        
        parser.parse(inputSource);
    }
    
    public XHTMLTextExtractor(int threshold, long maxPartSize)
    {
        super(threshold, maxPartSize);
    }
}
