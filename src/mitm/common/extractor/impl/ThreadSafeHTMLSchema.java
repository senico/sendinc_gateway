/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 */
package mitm.common.extractor.impl;

import org.ccil.cowan.tagsoup.ElementType;
import org.ccil.cowan.tagsoup.HTMLSchema;

/**
 * Creating an instance of HTMLSchema is pretty heavy. HTMLSchema however is not synchronized. To make
 * sure that HTMLSchema can be reused we will override all methods and synchronize access.
 * 
 * @author Martijn Brinkers
 *
 */
public class ThreadSafeHTMLSchema extends HTMLSchema
{
    public ThreadSafeHTMLSchema() {
        super();
    }
    
    @Override
    public synchronized void elementType(String name, int model, int memberOf, int flags)
    {
        super.elementType(name, model, memberOf, flags);
    }

    @Override
    public synchronized ElementType rootElementType()
    {
        return super.rootElementType();
    }

    @Override
    public synchronized void attribute(String elemName, String attrName, String type, String value)
    {
        super.attribute(elemName, attrName, type, value);
    }

    @Override
    public synchronized void parent(String name, String parentName)
    {
        super.parent(name, parentName);
    }

    @Override
    public synchronized void entity(String name, int value)
    {
        super.entity(name, value);
    }

    @Override
    public synchronized ElementType getElementType(String name)
    {
        return super.getElementType(name);
    }

    @Override
    public synchronized int getEntity(String name)
    {
        return super.getEntity(name);
    }

    @Override
    public synchronized String getURI()
    {
        return super.getURI();
    }

    @Override
    public synchronized String getPrefix()
    {
        return super.getPrefix();
    }

    @Override
    public synchronized void setURI(String uri)
    {
        super.setURI(uri);
    }

    @Override
    public synchronized void setPrefix(String prefix)
    {
        super.setPrefix(prefix);
    }
}
