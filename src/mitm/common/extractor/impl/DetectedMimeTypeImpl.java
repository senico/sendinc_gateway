/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor.impl;

import mitm.common.extractor.DetectedMimeType;
import mitm.common.mime.MediaType;
import mitm.common.mime.MimeTypes;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class DetectedMimeTypeImpl implements DetectedMimeType
{
    /*
     * This mime type
     */
    private final MediaType mediaType;
    
    /*
     * Used to lookup the super type
     */
    private final MimeTypes mimeTypes;
    
    /**
     * The default mime type (which is ocet-stream)
     */
    public static final DetectedMimeType DEFAULT_MIME_TYPE = new DetectedMimeTypeImpl();
    
    public DetectedMimeTypeImpl(MediaType mediaType, MimeTypes mimeTypes)
    {
        Check.notNull(mediaType, "mediaType");
        Check.notNull(mimeTypes, "mimeTypes");
        
        this.mediaType = mediaType;
        this.mimeTypes = mimeTypes;
    }
    
    /*
     * Private constructor for the default type
     */
    private DetectedMimeTypeImpl()
    {
        this.mediaType = MediaType.OCTET_STREAM;
        this.mimeTypes = null;;
    }
    
    @Override
    public String getType() {
        return StringUtils.lowerCase(mediaType.getType());
    }

    @Override
    public String getSubType() {
        return StringUtils.lowerCase(mediaType.getSubtype());
    }
    
    @Override
    public String toString() {
        return getType() + "/" + getSubType();
    }
    
    @Override
    public DetectedMimeType getSuperType()
    {
        MediaType superType = null;
        
        if (mimeTypes != null) {
            superType = mimeTypes.getMediaTypeRegistry().getSupertype(mediaType);
        }
        
        return superType != null ? new DetectedMimeTypeImpl(superType, mimeTypes) : null;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof DetectedMimeType)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        DetectedMimeType rhs = (DetectedMimeType) obj;
        
        return new EqualsBuilder()
            .append(getType(), rhs.getType())
            .append(getSubType(), rhs.getSubType())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getType())
            .append(getSubType())
            .toHashCode();    
    }
}
