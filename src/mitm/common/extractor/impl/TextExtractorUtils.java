/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor.impl;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;

import mitm.common.extractor.ExtractedPart;
import mitm.common.extractor.TextExtractor;
import mitm.common.extractor.TextExtractorContext;
import mitm.common.extractor.TextExtractorEventHandler;
import mitm.common.util.Check;
import mitm.common.util.FileConstants;
import mitm.common.util.MiscIOUtils;
import mitm.common.util.RewindableInputStream;
import mitm.common.util.SizeLimitedOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.DeferredFileOutputStream;
import org.apache.commons.lang.StringUtils;

/**
 * General helper methods for TextExtractor's 
 * 
 * @author Martijn Brinkers
 *
 */
public class TextExtractorUtils
{
    /**
     * Closes the ExtractedPart without throwing an exception
     */
    public static void closeQuitely(ExtractedPart part)
    {
        if (part == null) {
            return;
        }
        
        try {
            part.close();
        }
        catch (IOException e) {
            /* ignore */
        }        
    }
    
    /**
     * Closes all the ExtractedPart's without throwing an exception
     */
    public static void closeQuitely(Collection<? extends ExtractedPart> parts)
    {
        if (parts == null) {
            return;
        }
        
        for (ExtractedPart part : parts) {
            closeQuitely(part);
        }
    }
    
    /**
     * Should be implemented for {@link TextExtractorUtils#fireTextEvent(TextExtractorEventHandler, 
     * String, TextExtractorWriter, int, int)}
     */
    public static interface TextExtractorWriterHandler
    {
        public void write(Writer writer)
        throws IOException;
    }

    /**
     * Should be implemented for {@link TextExtractorUtils#fireAttachmentEvent(TextExtractorEventHandler, 
     * String, TextExtractorWriter, int, int)}
     */
    public static interface TextExtractorAttachmentHandler
    {
        public void write(OutputStream output)
        throws IOException;
    }
    
    /**
     * Creates a name for an embedded document based on the parent name and the name of the 
     * embedded document (can be a word, excel, image etc.)
     */
    public static String createEmbeddedName(String parentName, String childName)
    {
        return StringUtils.isNotEmpty(parentName) ? parentName + "/" + StringUtils.defaultString(childName) :
                StringUtils.defaultString(childName);
    }
    
    /**
     * conveniance method that writes data and fires a 
     * {@link TextExtractorEventHandler#textEvent(mitm.common.extractor.ExtractedPart)} event. The Data is written
     * to disk if total bytes exceed threshold. If total bytes written exceeds maxSize, an IOException will
     * be thrown.
     * 
     */
    public static void fireTextEvent(TextExtractorEventHandler handler, TextExtractorContext context, 
            TextExtractorWriterHandler writerHandler, int threshold, long maxSize)
    throws IOException
    {
        Check.notNull(handler, "handler");
        Check.notNull(writerHandler, "writerHandler");
        
        DeferredFileOutputStream output = new DeferredFileOutputStream(threshold, 
                FileConstants.TEMP_FILE_PREFIX, null, null);

        RewindableInputStream content = null;
        
        try {
            /*
             * Wrap the output in a buffered stream and protect against 'zip bombs'.
             */
            SizeLimitedOutputStream buffered = new SizeLimitedOutputStream(
                    new BufferedOutputStream(output), maxSize); 

            Writer writer = new OutputStreamWriter(buffered, TextExtractor.ENCODING);

            try {
                writerHandler.write(writer);
            }
            finally {
                /*
                 * Must close to make sure all data is written. 
                 * 
                 * Note: if DeferredFileOutputStream uses a file, the file should NOT 
                 * be deleted here because it will be deleted when the returned 
                 * DeferredBufferedInputStream is closed.
                 */
                writer.close();
            }

            content = new RewindableInputStream(MiscIOUtils.toInputStream(output), threshold);

            handler.textEvent(new ExtractedPartImpl(context, content, buffered.getByteCount()));
        }
        catch(IOException e)
        {
            /*
             * Must close the RewindableInputStream to prevent a possible temp file leak
             */
            IOUtils.closeQuietly(content);
            
            /*
             * Delete the tempfile (if) used by DeferredFileOutputStream.
             */
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }
        catch(RuntimeException e)
        {
            /*
             * Must close the RewindableInputStream to prevent a possible temp file leak
             */
            IOUtils.closeQuietly(content);

            /*
             * Delete the tempfile (if) used by DeferredFileOutputStream.
             */
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }    
    }
    
    /**
     * conveniance method that writes data and fires a 
     * {@link TextExtractorEventHandler#textEvent(mitm.common.extractor.ExtractedPart)} event. The Data is written
     * to disk if total bytes exceed threshold. If total bytes written exceeds maxSize, an IOException will
     * be thrown.
     * 
     */
    public static void fireAttachmentEvent(TextExtractorEventHandler handler, TextExtractorContext context, 
            TextExtractorAttachmentHandler attachmentHandler, int threshold, long maxSize)
    throws IOException
    {
        Check.notNull(handler, "handler");
        Check.notNull(attachmentHandler, "attachmentHandler");
        
        DeferredFileOutputStream output = new DeferredFileOutputStream(threshold, 
                FileConstants.TEMP_FILE_PREFIX, null, null);

        RewindableInputStream content = null;
        
        try {
            /*
             * Wrap the output in a buffered stream and protect against 'zip bombs'.
             */
            SizeLimitedOutputStream buffered = new SizeLimitedOutputStream(
                    new BufferedOutputStream(output), maxSize); 

            try {
                attachmentHandler.write(buffered);
            }
            finally {
                /*
                 * Must close to make sure all data is written. 
                 * 
                 * Note: if DeferredFileOutputStream uses a file, the file should NOT 
                 * be deleted here because it will be deleted when the returned 
                 * DeferredBufferedInputStream is closed.
                 */
                buffered.close();
            }

            content = new RewindableInputStream(MiscIOUtils.toInputStream(output), threshold);

            handler.attachmentEvent(new ExtractedPartImpl(context, content, buffered.getByteCount()));
        }
        catch(IOException e)
        {
            /*
             * Must close the RewindableInputStream to prevent a possible temp file leak
             */
            IOUtils.closeQuietly(content);

            /*
             * Delete the tempfile (if) used by DeferredFileOutputStream.
             */
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }
        catch(RuntimeException e)
        {
            /*
             * Must close the RewindableInputStream to prevent a possible temp file leak
             */
            IOUtils.closeQuietly(content);

            /*
             * Delete the tempfile (if) used by DeferredFileOutputStream.
             */
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }    
    }    
}
    