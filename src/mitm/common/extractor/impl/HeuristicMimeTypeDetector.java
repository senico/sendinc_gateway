/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor.impl;

import java.io.IOException;

import mitm.common.extractor.DetectedMimeType;
import mitm.common.extractor.MimeTypeDetector;
import mitm.common.mime.MediaType;
import mitm.common.mime.MimeMediaType;
import mitm.common.mime.MimeTypes;
import mitm.common.util.Check;
import mitm.common.util.RewindableInputStream;

import org.apache.commons.lang.StringUtils;

/**
 * Implementation of MimeTypeDetector that uses Tika's MIME type detection to detect the mime type.
 * 
 * @author Martijn Brinkers
 *
 */
public class HeuristicMimeTypeDetector implements MimeTypeDetector
{
    /*
     * MimeTypes is used to detect the mime type based on filename, magic bytes etc.
     */
    private final MimeTypes mimeTypes;
    
    public HeuristicMimeTypeDetector(MimeTypes mimeTypes) 
    throws IOException
    {
        Check.notNull(mimeTypes, "mimeTypes");
        
        this.mimeTypes = mimeTypes;
    }
        
    @Override
    public DetectedMimeType detectMimeType(RewindableInputStream input, String resourceName)
    throws IOException
    {
        MediaType type = null;
        
        long savedPosition = input.getPosition();
        
        try {
            MimeMediaType mimeType = mimeTypes.getMimeType(input);
            
            if (mimeType != null) {
                type = mimeType.getType();
            }
        }
        finally {
            input.setPosition(savedPosition);
        }
        
        if ((type == null || MediaType.OCTET_STREAM.equals(type)) && StringUtils.isNotEmpty(resourceName))
        {
            /*
             * Try to detect using the resourceName
             */
            type = mimeTypes.getMimeType(resourceName).getType();
        }
        
        if (type == null) {
            type = MediaType.OCTET_STREAM;
        }
        
        return new DetectedMimeTypeImpl(type, mimeTypes);
    }
}
