/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor.impl;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;

import mitm.common.extractor.TextExtractor;
import mitm.common.extractor.TextExtractorContext;
import mitm.common.extractor.TextExtractorEventHandler;
import mitm.common.extractor.impl.TextExtractorUtils.TextExtractorWriterHandler;
import mitm.common.util.Check;
import mitm.common.util.RewindableInputStream;

import org.apache.commons.io.input.ClosedInputStream;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

/**
 * Base class for XML based text extractors.
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractXMLBaseTextExtractor implements TextExtractor 
{
    // TODO make tags and attributes to extract selectable
    
    /*
     * The threshold at which a temp file will be used.
     */
    private final int threshold;
    
    /*
     * The maximum size a part can get (this is to prevent against 'zip bombs').
     */
    private final long maxPartSize;
    
    /*
     * The default encoding to use if the encoding cannot be found.
     */
    private String defaultEncoding = "UTF-8";
    
    /*
     * If the encoding cannot be determined, try to auto detect the encoding
     */
    private boolean autoDetectEncoding = true;
    
    /*
     * The minimal auto detect confidence level (0-100) to accept a match
     */
    private int autoDetectConfidence = 20;
    
    /**
     * Exception which can be used to stop SAX parsing.
     */
    protected static class StopParsingException extends SAXException
    {
        private static final long serialVersionUID = -5175279725869548331L;
    }
    
    protected static class ExtractTextHandler extends DefaultHandler2
    {        
        private final Writer writer;
        
        public ExtractTextHandler(Writer writer) {
            this.writer = writer;
        }
        
        private void writeText(String text)
        throws SAXException
        {
            try {
                text = StringUtils.trim(text);
                
                if (StringUtils.isNotEmpty(text))
                {
                    writer.write(text);
                    writer.write(' ');
                }
            }
            catch(IOException e) {
                throw new SAXException(e);
            }
        }
        
        @Override
        public void characters(char[] ch, int start, int length)
        throws SAXException
        {
            writeText(new String(ch, start, length));
        }
        
        @Override
        public void comment(char[] ch, int start, int length)
        throws SAXException
        {
            writeText(new String(ch, start, length));
        }
              
        private InputSource getAlwaysClosedInput()
        {
            /*
             * Make sure external resources are never resolved. Need to return an 
             * always closed InputSource to prevent fallback to default resolver.
             */
            return new InputSource(new ClosedInputStream());
        }
        
        @Override
        public InputSource getExternalSubset(String name, String baseURI) {
            return getAlwaysClosedInput();
        }
        
        @Override
        public InputSource resolveEntity(String publicId, String systemId) {
            return getAlwaysClosedInput();
        }

        @Override
        public InputSource resolveEntity(String name, String publicId, String baseURI, String systemId) {
            return getAlwaysClosedInput();
        }
    }

    private String autoDetectEncoding(RewindableInputStream input)
    throws IOException
    {
        String charsetName = null;
        
        CharsetDetector detector = new CharsetDetector();
        
        detector.setText(input);
        detector.enableInputFilter(true);
        
        CharsetMatch[] matches = detector.detectAll();
        
        if (matches != null)
        {
            /*
             * There might be multiple encodings. Find the first valid one.
             */
            for (CharsetMatch match : matches)
            {
                if (match == null) {
                    continue;
                }
                
                if (Charset.isSupported(match.getName()))
                {
                    /*
                     * Only accept when the confidence exceeds the set limit
                     */
                    if (match.getConfidence() > autoDetectConfidence) {
                        charsetName = match.getName();
                    }
                    
                    break;
                }
            }
        }
        
        if (charsetName == null) {
            charsetName = defaultEncoding;
        }
        
        return charsetName;
    }

    
    /**
     * Implement to return the encoding information from the document (for example
     * for HTML <meta http-equiv="Content-Type" content="">). Return null if no encoding found. 
     */
    protected abstract String getEncodingFromDocument(RewindableInputStream input, TextExtractorContext context)
    throws IOException, SAXException;

    /**
     * Implement to parse the input document
     */
    protected abstract void parse(RewindableInputStream input, TextExtractorContext context, Writer writer)
    throws IOException, SAXException;
    
    protected String getEncoding(RewindableInputStream input, TextExtractorContext context)
    throws IOException, SAXException
    {
        String encoding = getEncodingFromDocument(input, context);

        if (encoding == null)
        {
            encoding = context.getEncoding();

            if (encoding == null)
            {
                if (autoDetectEncoding) {
                    encoding = autoDetectEncoding(input);
                }
                
                if (encoding == null) {
                    encoding = defaultEncoding;
                }
            }
        }
        
        return encoding;
    }
    
    public AbstractXMLBaseTextExtractor(int threshold, long maxPartSize)
    {
        if (threshold < 0) {
            throw new IllegalArgumentException("threshold must be >= 0");
        }

        if (maxPartSize < 0) {
            throw new IllegalArgumentException("maxPartSize must be >= 0");
        }
        
        this.threshold = threshold;
        this.maxPartSize = maxPartSize;
    }
        
    @Override
    public void extract(final RewindableInputStream input, final TextExtractorContext context, 
            TextExtractorEventHandler handler)
    throws IOException
    {
        Check.notNull(input, "input");
        Check.notNull(context, "context");
        Check.notNull(handler, "handler");

        TextExtractorWriterHandler writerHandler = new TextExtractorWriterHandler()
        {
            @Override
            public void write(Writer writer)
            throws IOException
            {
                try {
                    parse(input, context, writer);
                }
                catch (SAXException e) {
                    throw new IOException(e);
                }
            }            
        };
        
        TextExtractorUtils.fireTextEvent(handler, context, writerHandler, threshold, maxPartSize);
    }

    public String getDefaultEncoding() {
        return defaultEncoding;
    }

    public void setDefaultEncoding(String defaultEncoding) {
        this.defaultEncoding = defaultEncoding;
    }

    public int getThreshold() {
        return threshold;
    }

    public long getMaxPartSize() {
        return maxPartSize;
    }

    public boolean isAutoDetectEncoding() {
        return autoDetectEncoding;
    }

    public void setAutoDetectEncoding(boolean autoDetectEncoding) {
        this.autoDetectEncoding = autoDetectEncoding;
    }

    public int getAutoDetectConfidence() {
        return autoDetectConfidence;
    }

    public void setAutoDetectConfidence(int autoDetectConfidence) {
        this.autoDetectConfidence = autoDetectConfidence;
    }
}
