/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 */
package mitm.common.extractor.impl;

import mitm.common.extractor.TextExtractorException;
import mitm.common.extractor.TextExtractorFactory;

/**
 * {@link TextExtractorFactory} that creates an instance of {@link XHTMLTextExtractor}.
 * 
 * @author Martijn Brinkers
 *
 */
public class XHTMLTextExtractorFactory extends AbstractTextExtractorFactory
{
    /*
     * The TextExtractor instance.
     */
    private XHTMLTextExtractor extractor;
    
    public XHTMLTextExtractorFactory(String[] mimeTypes) {
        super(mimeTypes);
    }
    
    /**
     * Returnes a singleton instance of {@link XHTMLTextExtractor}
     */
    @Override
    public synchronized XHTMLTextExtractor createTextExtractor()
    throws TextExtractorException
    {
        if (extractor == null) {
            extractor = new XHTMLTextExtractor(getThreshold(), getMaxPartSize());
        }
        
        return extractor;
    }
}
