/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor;

import mitm.common.util.SizeUtils;

/**
 * Default values for some general TextExtractor settings.
 * 
 * @author Martijn Brinkers
 *
 */
public class TextExtractorDefaults
{
    /**
     * The maximum allowed number of sub-documents
     */
    public final static int MAX_SUB_DOCUMENTS = 64;
    
    /**
     * The threshold at which a temp file will be used. 1048576 is used because a {@link RewindableInputStream} is
     * used for which is best to use a threshold from a specific range.
     */
    public final static int MEM_DISK_THRESHOLD = 1048576;
    
    /**
     * The maximum size a part can get (this is to prevent against 'zip bombs').
     */
    public final static long MAX_PART_SIZE = SizeUtils.MB * 20;

    /**
     * Extract embedded documents
     */
    public final static boolean EXTRACT_EMBEDDED_DOCUMENTS = true;
    
    /**
     * Extract images from documents
     */
    public final static boolean EXTRACT_IMAGES = false;

    /**
     * Extract meta data
     */
    public final static boolean EXTRACT_META_DATA = true;    
    
    /**
     * If false (the default) the extractor tries to handle exceptions where possible and 
     * continue scanning the text.
     */
    public final static boolean FAIL_ON_EXCEPTION = false;
}
