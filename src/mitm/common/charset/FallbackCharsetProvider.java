/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.charset;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.charset.spi.CharsetProvider;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CharsetProvider which fallback to US-ASCII if no provider can be found. Make sure that the JAR providing this is
 * the last one on the classpath. To make sure this CharsetProvider is the last tried you should add all external
 * CharsetProviders to the java.nio.charset.spi.CharsetProvider file in the order they need to be loaded.
 * 
 * @author martijn
 *
 */
public class FallbackCharsetProvider extends CharsetProvider
{
    private static final Logger logger = Logger.getLogger(FallbackCharsetProvider.class.getName());

    private static final String DEFAULT_CHARSET = "US-ASCII"; 
    
    public FallbackCharsetProvider() {
        logger.log(Level.FINE, "Instantiating FallbackCharsetProvider.");
    }
    
    @Override
    public Charset charsetForName(String charsetName) 
    {
        Charset charset = null;

        if (charsetName != null)
        {
            try {
                charset = Charset.forName(charsetName);
            }
            catch(IllegalCharsetNameException e) {
                logger.log(Level.WARNING, "IllegalCharsetNameException. Message: " + e);
            }
            catch(UnsupportedCharsetException e) {
                logger.log(Level.WARNING, "UnsupportedCharsetException. Message: " + e);
            }
            
            if (charset == null) 
            {
                logger.log(Level.WARNING, "Falback to default charset: " + DEFAULT_CHARSET);
                /* 
                 * fallback to default charset 
                 */
                charset = Charset.forName(DEFAULT_CHARSET);
            }
        }
        
        return charset;
    }
    
    @Override
    public Iterator<Charset> charsets() 
    {
        return null;
    }
}
