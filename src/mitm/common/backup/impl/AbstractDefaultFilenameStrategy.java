/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.backup.impl;

import java.util.Calendar;

import mitm.common.backup.BackupException;
import mitm.common.backup.FilenameStrategy;
import mitm.common.backup.Strategy;
import mitm.common.properties.HierarchicalPropertiesException;

/**
 * The default implementation of FilenameStrategy. 
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractDefaultFilenameStrategy implements FilenameStrategy
{
    protected abstract DefaultFilenameStrategySettings getDefaultFilenameStrategySettings()
    throws BackupException;
    
    private String getFilenameDayOf(String prefix, int field)
    {
        return prefix + Calendar.getInstance().get(field);
    }
    
    private String getFilenameTimestamp(String prefix)
    {
        return prefix + System.currentTimeMillis();
    }
    
    @Override
    public String getFilename(String prefix)
    throws BackupException
    {
        try {
            Strategy strategy = getDefaultFilenameStrategySettings().getStrategy();
            
            if (strategy == null) {
                strategy = Strategy.TIMESTAMP;
            }
            
            switch(strategy)
            {
            case DAY_OF_WEEK  : return getFilenameDayOf(prefix, Calendar.DAY_OF_WEEK);
            case DAY_OF_MONTH : return getFilenameDayOf(prefix, Calendar.DAY_OF_MONTH);
            case DAY_OF_YEAR  : return getFilenameDayOf(prefix, Calendar.DAY_OF_YEAR);
            case TIMESTAMP    : return getFilenameTimestamp(prefix);
            }
            
            throw new IllegalArgumentException("Unknown strategy: " + getDefaultFilenameStrategySettings().getStrategy());
        }
        catch (HierarchicalPropertiesException e) {
            throw new BackupException(e);
        }
    }    
}
