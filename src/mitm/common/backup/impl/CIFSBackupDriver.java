/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.backup.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import mitm.common.backup.BackupDriver;
import mitm.common.backup.BackupException;
import mitm.common.cifs.SMBFileParameters;
import mitm.common.cifs.SMBURLBuilder;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BackupDriver implementation that uses JCIFS to write and read the backup from a 
 * network resource (using the SMB protocol).
 * 
 * @author Martijn Brinkers
 *
 */
public class CIFSBackupDriver implements BackupDriver
{
    private final static Logger logger = LoggerFactory.getLogger(CIFSBackupDriver.class);
    
    private final SMBFileParameters smbFileParameters;
    
    public CIFSBackupDriver(SMBFileParameters smbFileParameters)
    {
        Check.notNull(smbFileParameters, "smbFileParameters");
        
        this.smbFileParameters = smbFileParameters;
    }
    
    private String getURL(String filename)
    throws MalformedURLException, HierarchicalPropertiesException
    {
        smbFileParameters.setFile(filename);
        
        SMBURLBuilder builder = new SMBURLBuilder();
        
        return builder.buildURL(smbFileParameters);
    }
    
    private SmbFile createSmbFile(String filename)
    throws MalformedURLException, HierarchicalPropertiesException
    {
        String url = getURL(filename);
        
        return new SmbFile(url);        
    }
    
    @Override
    public void writeBackup(String filename, InputStream backup)
    throws BackupException
    {
        Check.notNull(filename, "filename");
        Check.notNull(backup, "backup");
        
        try {
            SmbFile smbFile = createSmbFile(filename);
            
            if (!smbFile.exists())
            {
                logger.debug(smbFile + " does not yet exist.");
                                
                smbFile.createNewFile();
            }
            
            if (!smbFile.canWrite()) {
                throw new BackupException(smbFile + " cannot be written.");
            }
            
            SmbFileOutputStream output = new SmbFileOutputStream(smbFile);
            
            try {
                IOUtils.copy(backup, output);
            }
            finally {
                IOUtils.closeQuietly(output);
            }
        }
        catch(MalformedURLException e) {
            throw new BackupException(e);
        }
        catch (SmbException e) {
            throw new BackupException(e);
        }
        catch (UnknownHostException e) {
            throw new BackupException(e);
        }
        catch (IOException e) {
            throw new BackupException(e);
        }
        catch (HierarchicalPropertiesException e) {
            throw new BackupException(e);
        }
    }
    
    @Override
    public boolean isValid()
    throws BackupException
    {
        try {
            SmbFile smbFile = createSmbFile(null);
            
            return smbFile.exists();
        }
        catch (SmbException e) {
            throw new BackupException(e);
        }
        catch (MalformedURLException e) {
            throw new BackupException(e);
        }
        catch (HierarchicalPropertiesException e) {
            throw new BackupException(e);
        }
    }
    
    /**
     * Returns all the Access Control Entry's (ACE) of the share. 
     */
    public ACE[] getSecurity()
    throws BackupException
    {
        try {
            SmbFile smbFile = createSmbFile(null);
            
            return smbFile.getSecurity();
        }
        catch (SmbException e) {
            throw new BackupException(e);
        }
        catch (MalformedURLException e) {
            throw new BackupException(e);
        }
        catch (HierarchicalPropertiesException e) {
            throw new BackupException(e);
        }
        catch (IOException e) {
            throw new BackupException(e);
        }
    }
}
