/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.repository;

import java.util.Collection;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * MailRepositoryItem embeds a MIME message and some extra meta information.
 * 
 * @author Martijn Brinkers
 *
 */
public interface MailRepositoryItem
{
    /**
     * Returns the unique ID of this item
     */
    public String getID();
    
    /**
     * The name of the repository this item belons to
     */
    public String getRepository();
    
    /**
     * Returns the MIME message of this item.
     */
    public MimeMessage getMimeMessage()
    throws MessagingException;

    /**
     * Returns the message-id of the mime message.
     */
    public String getMessageID();
    
    /**
     * Returns the subject of the mime message.
     */
    public String getSubject();
    
    /**
     * The unfiltered (but decoded) from header of the message including user part. If the from header contains
     * multiple addresses, they will be comma separated.
     * 
     * Note: because the from contains the user part and possibly multiple addresses, it cannot be used directly
     * for retrieving User objects. It should only be used for displaying purposes.
     */
    public String getFromHeader();
    
    /**
     * Returns the envelope recipients of the message. 
     */
    public Collection<InternetAddress> getRecipients()
    throws AddressException;

    public void setRecipients(Collection<InternetAddress> recipients)
    throws AddressException;
    
    /**
     * Returns the envelope sender of the message. Null if the sender is the null sender
     */
    public InternetAddress getSender()
    throws AddressException;
        
    public void setSender(InternetAddress sender)
    throws AddressException;
    
    
    /**
     * Returns the originator of the message. The originator can be for example the envelope sender or 
     * the from header. The caller decides which email address is used for the originator.
     */
    public InternetAddress getOriginator()
    throws AddressException;
    
    public void setOriginator(InternetAddress originator)
    throws AddressException;    
    
    /**
     * The remote IP address of the sender, or null if unknown.
     */
    public String getRemoteAddress();
    
    public void setRemoteAddress(String remoteAddress);
    
    /**
     * Date at which this item was created
     */
    public Date getCreated();

    /**
     * Date at which this item was last updated
     */
    public Date getLastUpdated();
    public void setLastUpdated(Date date);
    
    /**
     * Some additional general purpose storage which can be used to store additional data
     */
    public byte[] getAdditionalData();    
    public void setAdditionalData(byte[] data);
}
