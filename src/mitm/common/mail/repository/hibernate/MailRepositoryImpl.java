/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.repository.hibernate;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.repository.MailRepository;
import mitm.common.mail.repository.MailRepositoryItem;
import mitm.common.mail.repository.MailRepositorySearchField;
import mitm.common.util.Check;
import mitm.common.util.IDGenerator;

/**
 * Implementation of MailRepository that stores MailRepositoryItem's in the database.
 * 
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryImpl implements MailRepository
{
    /*
     * The name of this repository to use
     */
    private final String repositoryName;
    
    /*
     * Used for generating secure and uniqe IDs
     */
    private final IDGenerator idCreator;
    
    /*
     * Manages database sessions
     */
    private final SessionManager sessionManager;
    
    public MailRepositoryImpl(IDGenerator idCreator, SessionManager sessionManager, String repositoryName)
    {
        Check.notNull(idCreator, "idCreator");
        Check.notNull(sessionManager, "sessionManager");
        
        this.idCreator = idCreator;
        this.sessionManager = sessionManager;
        this.repositoryName = repositoryName;
    }
    
    @Override
    public String getName() {
        return repositoryName;
    }

    @Override
    public MailRepositoryItem createItem(MimeMessage message)
    throws MessagingException, IOException
    {
        return new MailRepositoryItemEntity(idCreator.createID(), repositoryName, message);
    }

    /*
     * For testing date created 
     */
    public MailRepositoryItem createItem(MimeMessage message, Date created)
    throws MessagingException, IOException
    {
        return new MailRepositoryItemEntity(idCreator.createID(), repositoryName, message, created);
    }

    @Override
    public void addItem(MailRepositoryItem item)
    {
        if (!(item instanceof MailRepositoryItemEntity)) {
            throw new IllegalArgumentException("The item is-not-a MailRepositoryItemEntity.");
        }
        getDAO().makePersistent((MailRepositoryItemEntity) item);
    }

    @Override
    public void deleteItem(String id)
    {
        MailRepositoryDAO dao = getDAO();
        
        MailRepositoryItemEntity item = dao.findById(id);
        
        if (item != null) {
            dao.delete(item);
        }
    }

    @Override
    public MailRepositoryItem getItem(String id) {
        return getDAO().findById(id);
    }

    @Override
    public int getItemCount() {
        return getDAO().getItemCount(repositoryName);
    }

    @Override
    public List<? extends MailRepositoryItem> getItems(Integer firstResult, Integer maxResults)
    {
        return getDAO().getItems(repositoryName, firstResult, maxResults);
    }

    @Override
    public List<? extends MailRepositoryItem> getItemsBefore(Date before, Integer firstResult, Integer maxResults) {
        return getDAO().getItemsBefore(repositoryName, before, firstResult, maxResults);
    }    
    
    @Override
    public int getSearchCount(MailRepositorySearchField searchField, String key) {
        return getDAO().getSearchCount(repositoryName, searchField, key);
    }

    @Override
    public List<? extends MailRepositoryItem> searchItems(MailRepositorySearchField searchField, String key, 
            Integer firstResult, Integer maxResults)
    {
        return getDAO().searchItems(repositoryName, searchField, key, firstResult, maxResults);
    }
    
    private MailRepositoryDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new MailRepositoryDAO(session);
    }
}
