/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.repository.hibernate;

import java.util.Date;
import java.util.List;

import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.mail.repository.MailRepositorySearchField;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * DAO for MailRepository.
 *
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryDAO extends GenericHibernateDAO<MailRepositoryItemEntity, String>
{
    private String entityName = MailRepositoryItemEntity.ENTITY_NAME;

    public MailRepositoryDAO(SessionAdapter session)
    {
        super(session);
    }

    /**
     * Returns the number of items in the MailRepository.
     */
    public int getItemCount(String repository)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq(MailRepositoryItemEntity.REPOSITORY_COLUMN_NAME, repository));

        criteria.setProjection(Projections.rowCount());

        return (Integer) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<MailRepositoryItemEntity> getItems(String repository, Integer firstResult, Integer maxResults)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq(MailRepositoryItemEntity.REPOSITORY_COLUMN_NAME, repository));

        criteria.addOrder(Order.asc(MailRepositoryItemEntity.CREATED_COLUMN_NAME));

        if (firstResult != null) {
            criteria.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            criteria.setMaxResults(maxResults);
        }

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<MailRepositoryItemEntity> getItemsBefore(String repository, Date before, Integer firstResult,
            Integer maxResults)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq(MailRepositoryItemEntity.REPOSITORY_COLUMN_NAME, repository));
        criteria.add(Restrictions.lt(MailRepositoryItemEntity.CREATED_COLUMN_NAME, before));

        criteria.addOrder(Order.asc(MailRepositoryItemEntity.CREATED_COLUMN_NAME));

        if (firstResult != null) {
            criteria.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            criteria.setMaxResults(maxResults);
        }

        return criteria.list();
    }

    private Query createSearchForRecipientsQuery(String baseSQL, String repository, String key)
    {
        Query query = createQuery(baseSQL + " from " + entityName + " c join c.recipients r where r like :key " +
                "and repository = :repository");

        query.setString("key", key);
        query.setString("repository", repository);

        return query;
    }

    @SuppressWarnings("unchecked")
    private List<MailRepositoryItemEntity> searchForRecipients(String repository, String key, Integer firstResult,
            Integer maxResults)
    {
        Query query = createSearchForRecipientsQuery("select distinct c", repository, key);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    private Criterion createSearchFieldCriterion(MailRepositorySearchField searchField, String key)
    {
        switch(searchField)
        {
        case ID         : return Restrictions.ilike(MailRepositoryItemEntity.ID_COLUMN_NAME, key);
        case MESSAGE_ID : return Restrictions.ilike(MailRepositoryItemEntity.MESSAGE_ID_COLUMN_NAME, key);
        case SUBJECT    : return Restrictions.ilike(MailRepositoryItemEntity.SUBJECT_COLUMN_NAME, key);
        case SENDER     : return Restrictions.ilike(MailRepositoryItemEntity.SENDER_COLUMN_NAME, key);
        case FROM       : return Restrictions.ilike(MailRepositoryItemEntity.FROM_HEADER_COLUMN_NAME, key);
        default: break;
        }

        throw new IllegalArgumentException("Unsupported searchField " + searchField);
    }

    @SuppressWarnings("unchecked")
    public List<MailRepositoryItemEntity> searchItems(String repository, MailRepositorySearchField searchField,
            String key, Integer firstResult, Integer maxResults)
    {
        /*
         * We need to handle the search for recipients somewhat differently because the
         * recipients are stored in a separate table
         */
        if (searchField == MailRepositorySearchField.RECIPIENTS) {
            return searchForRecipients(repository, key, firstResult, maxResults);
        }
        else {
            Criteria criteria = createCriteria(entityName);
            criteria.add(Restrictions.eq(MailRepositoryItemEntity.REPOSITORY_COLUMN_NAME, repository));

            criteria.addOrder(Order.asc(MailRepositoryItemEntity.CREATED_COLUMN_NAME));

            if (searchField != null) {
                criteria.add(createSearchFieldCriterion(searchField, key));
            }

            if (firstResult != null) {
                criteria.setFirstResult(firstResult);
            }

            if (maxResults != null) {
                criteria.setMaxResults(maxResults);
            }

            return criteria.list();
        }
    }

    private long getSearchCountRecipients(String repository, String key)
    {
        Query query = createSearchForRecipientsQuery("select count (distinct c)", repository, key);

        return (Long) query.uniqueResult();
    }

    public int getSearchCount(String repository, MailRepositorySearchField searchField, String key)
    {
        /*
         * We need to handle the search for recipients somewhat differently because the
         * recipients are stored in a separate table
         */
        if (searchField == MailRepositorySearchField.RECIPIENTS) {
            return (int) getSearchCountRecipients(repository, key);
        }
        else {
            Criteria criteria = createCriteria(entityName);

            criteria.setProjection(Projections.rowCount());
            criteria.add(Restrictions.eq(MailRepositoryItemEntity.REPOSITORY_COLUMN_NAME, repository));

            if (searchField != null) {
                criteria.add(createSearchFieldCriterion(searchField, key));
            }

            return (Integer) criteria.uniqueResult();
        }
    }

    /*
     * For testing purposes. Deleting all items this way is sub-optimal if there are a lot of items
     */
    public void deleteAll()
    {
        for (MailRepositoryItemEntity item : findAll()) {
            delete(item);
        }
    }
}
