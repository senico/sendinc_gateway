/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.repository;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * MailRepository stores MailRepositoryItem's.
 * 
 * @author Martijn Brinkers
 *
 */
public interface MailRepository
{
    /**
     * The name of the repository
     */
    public String getName();
    
    /**
     * Creates, but not adds, a new MailRepositoryItem.
     * @throws IOException 
     * @throws MessagingException 
     */
    public MailRepositoryItem createItem(MimeMessage message)
    throws MessagingException, IOException;
    
    /**
     * Adds the MailRepositoryItem. The ID of the MailRepositoryItem should not already been 
     * added (i.e. the ID should be unique).
     */
    public void addItem(MailRepositoryItem item);

    /**
     * Deletes the item with the given id.
     */
    public void deleteItem(String id);
    
    /**
     * Returns the item with the id. Null if item is not found.
     */
    public MailRepositoryItem getItem(String id);
    
    /**
     * Returns maxResults items starting at firstResult sorted on creation date.
     */
    public List<? extends MailRepositoryItem> getItems(Integer firstResult, Integer maxResults);

    /**
     * Returns items that were created before the given date sorted on creation date 
     */
    public List<? extends MailRepositoryItem> getItemsBefore(Date before, Integer firstResult, Integer maxResults);
    
    /**
     * Returns maxResults items starting at firstResult with the given search sorted on creation date. 
     */
    public List<? extends MailRepositoryItem> searchItems(MailRepositorySearchField searchField, String key, 
            Integer firstResult, Integer maxResults);
    
    /**
     * Returns the number of items in the repository
     */
    int getItemCount();

    /**
     * Returns the number of items in the repository with the given search.
     */
    int getSearchCount(MailRepositorySearchField searchField, String key);
}
