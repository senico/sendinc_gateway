/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.filter;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.MimeUtils;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.NotHeaderNameMatcher;
import mitm.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import mitm.common.security.smime.SMIMEHeader;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Removes unsupported attachments from email.
 * 
 * This class is mainly used to filter email sent to a Blackberry before encryption.
 * 
 * @author Martijn Brinkers
 *
 */
public class UnsupportedFormatStripper
{
    private final static Logger logger = LoggerFactory.getLogger(UnsupportedFormatStripper.class);
    
    /*
     * Container for storing attachment matching pairs. Attachments are matched against
     * the content-type and filename.
     */
    public static class MatchingPair
    {
        private final String contentTypeWildcard;
        
        private final String filenameWildcard;
        
        public MatchingPair(String contentTypeWildcard, String filenameWildcard)
        {
            this.contentTypeWildcard = contentTypeWildcard;
            this.filenameWildcard = filenameWildcard;
        }

        public boolean isMatch(Part part)
        throws MessagingException
        {
            String filename = StringUtils.defaultString(HeaderUtils.decodeTextQuietly(
                    MimeUtils.getFilenameQuietly(part)));

            return FilenameUtils.wildcardMatch(filename, filenameWildcard, IOCase.INSENSITIVE) && 
                        ("*".equals(contentTypeWildcard) || part.isMimeType(contentTypeWildcard));
        }
        
        @Override
        public String toString() {
            return "(" + contentTypeWildcard + "|" + filenameWildcard + ")";
        }
    }
    
    /*
     * Container for parts and modified flag for thread safe usage
     */
    private static class PartsContext
    {
        /*
         * The collected parts of the message we will retain
         */
        private final List<Part> parts = new LinkedList<Part>();

        /*
         * True if HTML part(s) of attachments have been removed 
         */
        private boolean modified;

        public List<Part> getParts() {
            return parts;
        }

        public boolean isModified() {
            return modified;
        }

        public void setModified(boolean modified) {
            this.modified = modified;
        }
    }
    
    /*
     * Collection of matchers that will match the attachments we will support
     */
    private final List<MatchingPair> attachmentMatchers = Collections.synchronizedList(new LinkedList<MatchingPair>());
    
    /*
     * Maximum depth of a mime structure we will scan (as a protection against too deep structures)
     */
    private final int MAX_DEPTH = 8;
    
    /*
     * If true S/MIME parts won't be removed 
     */
    private AtomicBoolean keepSMIMEParts = new AtomicBoolean(true);

    /*
     * If total size of all parts is larger than maxMessageSize new parts will be skipped.
     * Note: the total message size can be larger due to overhead of encoding (like base64 encoding etc.)
     */
    private AtomicInteger maxMessageSize = new AtomicInteger(1024 * 1024 * 32);
            
    public UnsupportedFormatStripper()
    {
        /*
         * Add default supported attachments
         */
        attachmentMatchers.add(new MatchingPair("text/html", "*"));
        attachmentMatchers.add(new MatchingPair("message/rfc822", "*"));
        attachmentMatchers.add(new MatchingPair("*", "*.jpg"));
        attachmentMatchers.add(new MatchingPair("*", "*.jpeg"));
        attachmentMatchers.add(new MatchingPair("*", "*.bmp"));
        attachmentMatchers.add(new MatchingPair("*", "*.png"));
    }
    
    public UnsupportedFormatStripper(List<MatchingPair> attachmentMatchers)
    {
        this.attachmentMatchers.addAll(attachmentMatchers);
    }
    
    /*
     * Calculate what the message size will be when a new part is added
     */
    private int getNewMessageSize(List<Part> currentParts, Part newPart)
    throws MessagingException
    {
        int size = newPart.getSize();
        
        for (Part part : currentParts) {
            size = size + part.getSize();
        }
        
        return size;
    }
    
    private boolean addSupportedPart(Part part, PartsContext context)
    throws MessagingException
    {        
        List<Part> parts = context.getParts();
        
        boolean add = false;
        
        if (keepSMIMEParts.get())
        {
            SMIMEHeader.Type sMIMEType = SMIMEHeader.getSMIMEContentType(part);
            
            if (SMIMEHeader.Type.NO_SMIME != sMIMEType) {
                add = true;
            }
        }
        
        if (!add)
        {
            if (getNewMessageSize(parts, part) < maxMessageSize.get())
            {
                for (MatchingPair matcher : attachmentMatchers)
                {
                    if (matcher.isMatch(part))
                    {
                        add = true;
                        
                        break;
                    }
                }
            }
            else {
                logger.debug("Attachment stripped because total message size is larger than maximum.");
            }
        }
        
        if (add) {
            parts.add(part);
        }
        
        return add;
    }
        
    private void skipPart(Part part, boolean addSkipWarning, PartsContext context)
    throws MessagingException
    {
        List<Part> parts = context.getParts();
        
        if (logger.isDebugEnabled()) {
            logger.debug("skipping part with content-type: " + part.getContentType());
        }
    
        if (addSkipWarning)
        {
            MimeBodyPart skipWarningPart = new MimeBodyPart();
            
            String filename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));
            
            if (StringUtils.isBlank(filename)) {
                filename = "<unknown>";
            }
            
            String body = String.format("Attachment with filename %s was removed.", filename);

            skipWarningPart.setText(body);
            
            parts.add(skipWarningPart);
        }
        
        context.setModified(true);
    }
    
    private void copyNonContentHeaders(Part source, Part target) 
    throws MessagingException
    {
        HeaderMatcher contentMatcher = new ProtectedContentHeaderNameMatcher(new String[]{});

        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);
       
        /* copy all non-content headers from source message to the new message */
        HeaderUtils.copyHeaders(source, target, nonContentMatcher);    
    }

    private MimeMessage buildMessage(MimeMessage source, PartsContext context)
    throws MessagingException, IOException
    {        
        List<Part> parts = context.getParts();

        assert(parts.size() > 0);

        MimeMessage newMessage;
        
        if (parts.size() > 1) 
        {
            /* there is more than one S/MIME part so we need to create a multipart message */
            Multipart mp = new MimeMultipart();

            for (Part part : parts)
            {
                MimeBodyPart bodyPart = BodyPartUtils.toMimeBodyPart(part);
                
                mp.addBodyPart(bodyPart);
            }

            newMessage = new MimeMessage(MailSession.getDefaultSession());
            
            newMessage.setContent(mp);
        }
        else {
            newMessage = BodyPartUtils.toMessage(parts.get(0), true /* clone */);
        }
        
        copyNonContentHeaders(source, newMessage);
        
        return newMessage;
    }

    private void scanPart(Part part, PartsContext context, int depth)
    throws MessagingException, IOException
    {
        if (depth > MAX_DEPTH) {
            return;
        }
        
        if (part.isMimeType("multipart/mixed"))
        {
            MimeMultipart parts = (MimeMultipart) part.getContent();

            int partCount = parts.getCount();

            for (int i = 0; i < partCount; i++) 
            {
                Part child = parts.getBodyPart(i);

                scanPart(child, context, ++depth);
            }
        } 
        else if (part.isMimeType("multipart/*"))
        {
            /*
             * We will always add any multipart that's not a mixed multipart
             */
            context.getParts().add(part);
        }
        else if (part.isMimeType("text/plain")) {
            context.getParts().add(part);
        }
        else if (part.isMimeType("text/html")) {
            context.getParts().add(part);
        }
        else if (!addSupportedPart(part, context))
        {
            skipPart(part, true, context);
        }
    }
    
    /**
     * Strips unsupported attachments. Returns a new message if HTML part(s) or
     * attachments have been removed and null if the message was not modified (ie
     * there were no unsuported formats to remove)
     */
    public MimeMessage strip(MimeMessage message)
    throws MessagingException
    {
        MimeMessage newMessage = null;
        
        PartsContext context = new PartsContext();
        
        try {
            scanPart(message, context, 0);
            
            if (context.isModified() && context.getParts().size() > 0) {
                newMessage = buildMessage(message, context);
            }
            
            return newMessage;
        }
        catch (IOException e) {
            throw new MessagingException("Error stripping unsupported formats.", e);
        }
    }
    
    /**
     * If true S/MIME attachments will be retained. 
     */
    public void setKeepSMIMEParts(boolean keepSMIMEParts) {
        this.keepSMIMEParts.set(keepSMIMEParts);
    }

    public boolean isKeepSMIMEParts() {
        return keepSMIMEParts.get();
    }
    
    /**
     * If an attachment is larger than the given size (default 32K) the attachment
     * will be stripped.
     */
    public void setMaxMessageSize(int maxMessageSize) {
        this.maxMessageSize.set(maxMessageSize);
    }
    
    public int getMaxMessageSize() {
        return maxMessageSize.get();
    }
}
