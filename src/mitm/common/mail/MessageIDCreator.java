/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import java.net.InetAddress;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class that creates a unique message-id
 * 
 * @author Martijn Brinkers
 *
 */
public class MessageIDCreator
{
    private final static Logger logger = LoggerFactory.getLogger(MessageIDCreator.class);
    
    /*
     * The global instance
     */
    private static final MessageIDCreator instance = new MessageIDCreator();
    
    /**
     * Creates a unique message-id for an email
     */
    public String createUniqueMessageID()
    {
        return "<" + UUID.randomUUID().toString() + "@" + getHostname() + ">"; 
    }

    /*
     * Returns the name of the host running on.
     * 
     * NOte: do not cache the result otherwise the admin need to restart Djigzo if the hostname is changed.
     */
    private String getHostname()
    {        
        String hostname = null;

        InetAddress localHost = null;

        try {
            localHost = InetAddress.getLocalHost();
            
            if (localHost != null) {
                hostname = StringUtils.trimToNull(localHost.getHostName());
            }
        } 
        catch (Exception e) {
            /*
             * Can happen if the name of the host is invalid  
             */
            logger.debug("Error getting localhost", e);
        }

        if (hostname == null) {
            hostname = "localhost";
        }
        
        return hostname;
    }   
    
    /**
     * Returns the global (singleton) instance of the MessageIDCreator.
     */
    public static MessageIDCreator getInstance() {
        return instance;
    }
}
