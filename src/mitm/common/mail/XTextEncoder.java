/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 * 
 * Martijn Brinkers elects to include this software under the CDDL license.
 */

package mitm.common.mail;

/**
 * Encoder/decoder for XTEXT according to RFC 1891
 * 
 * "xtext" is formally defined as follows:
 *
 *    xtext = *( xchar / hexchar )
 *
 *    xchar = any ASCII CHAR between "!" (33) and "~" (126) inclusive,
 *         except for "+" and "=".
 *
 * ; "hexchar"s are intended to encode octets that cannot appear
 * ; as ASCII characters within an esmtp-value.
 *
 *    hexchar = ASCII "+" immediately followed by two upper case
 *         hexadecimal digits
 *         
 *         
 * Code based on Javamail 1.4 (com.sun.mail.smtp.SMTPTransport) which is CDDL licensed
 */
public class XTextEncoder
{

    private static char[] hexchar = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
        };
    
    /**
     * XTEXT encodes the given string.
     * 
     * Based on Javamail 1.4
     */
    public static String encode(String s) 
    {
        StringBuffer sb = null;
        
        if (s == null) {
            return null;
        }
        
        for (int i = 0; i < s.length(); i++) 
        {
            char c = s.charAt(i);
            
            if (c >= 128) {
                throw new IllegalArgumentException("Only ASCII characters are supported: " + s);
            }
            
            if (c < '!' || c > '~' || c == '+' || c == '=') 
            {
                if (sb == null) {
                    sb = new StringBuffer(s.length() + 4);
                    sb.append(s.substring(0, i));
                }
                
                sb.append('+');
                sb.append(hexchar[((int)c) >> 4]);
                sb.append(hexchar[((int)c)& 0x0f]);
            } 
            else {
                if (sb != null) sb.append(c);
            }
        }
        return sb != null ? sb.toString() : s;
    }
    
    /**
     * XTEXT Decodes the given string.
     */
    public static String decode(String s) 
    {
        if (s == null) {
            return null;
        }
        
        StringBuffer sb = null;
        
        char[] encoded = new char[2];
        
        int index = -1;
        
        for (int i = 0; i < s.length(); i++) 
        {
            char c = s.charAt(i);
            
            if (index != -1) 
            {
                encoded[index] = c;
                
                index++;
                
                if (index >= encoded.length) 
                {
                    /* 
                     * We have a completely encoded char so we can decode.
                     */
                    int ms = (char) (Character.digit(encoded[0], 16) << 4);
                    int ls = (char) Character.digit(encoded[1], 16);
                    
                    if (ms != -1 && ls != -1) {
                        char decoded = (char) (ms + ls);
                        
                        sb.append(decoded);
                    }
                        
                    index = -1;
                }
            }
            else if (c == '+') 
            {
                /* 
                 * we should xtext decode the next two characters.
                 */ 
                index = 0;
                
                if (sb == null) 
                {
                    sb = new StringBuffer(s.length() + 4);
                    sb.append(s.substring(0, i));
                }
                
            } 
            else {
                if (sb != null) {
                    sb.append(c);
                }
            }
        }
        
        return sb != null ? sb.toString() : s;
    }
}
