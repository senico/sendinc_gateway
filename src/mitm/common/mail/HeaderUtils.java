/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.util.Check;

public class HeaderUtils
{
    /*
     * Helper class that will copy source headers when the headers match
     */
    private static abstract class HeaderCopier
    {
        public void copyHeaders(Enumeration<Header> sourceHeaders, HeaderMatcher matcher)
        throws MessagingException
        {
            /*
             * Javamail reverses Received and Return-Path headers. Because the headers are
             * already reversed in the source the Received and Return-Path headers are added 
             * in the wrong order. We therefore need to add them reversed. Javamail knows
             * about the order of the Received and Return-Path headers so adding them 
             * afterwards does not matter.
             */
            List<Header> reversed = null;
            
            while (sourceHeaders.hasMoreElements())
            {
                Header header = sourceHeaders.nextElement();
                
                if (matcher.isMatch(header))
                {
                    String name = header.getName();
                    
                    if ("Received".equalsIgnoreCase(name) || "Return-Path".equalsIgnoreCase(name))
                    {
                        if (reversed == null) {
                            reversed = new LinkedList<Header>();
                        }
                        /*
                         * Add in reversed order
                         */
                        reversed.add(0, header);
                    }
                    else {
                        addHeader(name, header.getValue());
                    }
                }
            }
            
            if (reversed != null)
            {
                /*
                 * Add the Received and Return-Path headers in reversed order
                 */
                for (Header header : reversed) {
                    addHeader(header.getName(), header.getValue());
                }
            }
        }
        
        public abstract void addHeader(String name, String value)
        throws MessagingException;
    }
    
    public static void copyHeaders(Enumeration<Header> sourceHeaders, final Part destination, HeaderMatcher matcher) 
    throws MessagingException
    {
        HeaderCopier headerCopier = new HeaderCopier()
        {
            @Override
            public void addHeader(String name, String value)
            throws MessagingException
            {
                destination.addHeader(name, value);
            }            
        };
        
        headerCopier.copyHeaders(sourceHeaders, matcher);
    }

    public static void copyHeaders(Enumeration<Header> sourceHeaders, final InternetHeaders destination, 
            HeaderMatcher matcher) 
    throws MessagingException
    {
        HeaderCopier headerCopier = new HeaderCopier()
        {
            @Override
            public void addHeader(String name, String value)
            throws MessagingException
            {
                destination.addHeader(name, value);
            }            
        };
        
        headerCopier.copyHeaders(sourceHeaders, matcher);
    }
    
    @SuppressWarnings("unchecked")
    public static void copyHeaders(Part sourcePart, Part destination, HeaderMatcher matcher) 
    throws MessagingException
    {
        copyHeaders(sourcePart.getAllHeaders(), destination, matcher);
    }
    
    @SuppressWarnings("unchecked")
    public static void removeHeaders(Part part, HeaderMatcher matcher) 
    throws MessagingException
    {
        Enumeration<Header> headers = part.getAllHeaders();
        
        while (headers.hasMoreElements())
        {
            Header header = headers.nextElement();
            
            if (matcher.isMatch(header)) {
                part.removeHeader(header.getName());
            }
        }
    }
    
    /**
     * Returns an array of headers names that do not exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getNonMatchingHeaderNames(Enumeration<Header> existingHeaders, String... headersToMatch)
    {
    	Check.notNull(existingHeaders, "existingHeaders");
    	Check.notNull(headersToMatch, "headersToMatch");
        
        Set<String> set = new HashSet<String>();
        
        for (String headerName : headersToMatch)
        {
            if (headerName != null) 
            {
                set.add(headerName.toLowerCase());
            }
        }
        
        while (existingHeaders.hasMoreElements())
        {
            Header header = existingHeaders.nextElement();
            
            if (header.getName() != null) 
            {
                String headerName = header.getName().toLowerCase();
                
                set.remove(headerName);
            }
        }
        
        String[] result = new String[set.size()];
        
        return set.toArray(result);
    }

    /**
     * Returns an array of headers names that do not exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getNonMatchingHeaderNames(Enumeration<Header> existingHeaders, Enumeration<Header> headersToMatch)
    {
    	Check.notNull(headersToMatch, "headersToMatch");
        
        Set<String> headerNames = new HashSet<String>();
        
        while (headersToMatch.hasMoreElements())
        {
            Header header = headersToMatch.nextElement();
            
            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();
                
                headerNames.add(headerName);
            }
        }
        
        String[] result = new String[headerNames.size()];
        
        return getNonMatchingHeaderNames(existingHeaders, headerNames.toArray(result));
    }
    
    /**
     * Returns an array of headers names from headersToMatch that do exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getMatchingHeaderNames(Enumeration<Header> existingHeaders, String... headersToMatch)
    {
    	Check.notNull(existingHeaders, "existingHeaders");
    	Check.notNull(headersToMatch, "headersToMatch");
        
        List<String> matching = new LinkedList<String>();
        
        Set<String> set = new HashSet<String>();
        
        for (String headerName : headersToMatch)
        {
            if (headerName != null)
            {
                set.add(headerName.toLowerCase());
            }
        }
        
        while (existingHeaders.hasMoreElements())
        {
            Header header = existingHeaders.nextElement();
            
            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();
                
                if (set.contains(headerName)) {
                    matching.add(headerName);
                }
            }
        }
        
        String[] result = new String[matching.size()];
        
        return matching.toArray(result);
    }
    

    /**
     * Returns an array of headers names from headersToMatch that do exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getMatchingHeaderNames(Enumeration<Header> existingHeaders, Enumeration<Header> headersToMatch)
    {
    	Check.notNull(headersToMatch, "headersToMatch");
        
        Set<String> headerNames = new HashSet<String>();
        
        while (headersToMatch.hasMoreElements())
        {
            Header header = headersToMatch.nextElement();
            
            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();
                
                headerNames.add(headerName);
            }
        }
        
        String[] result = new String[headerNames.size()];
        
        return getMatchingHeaderNames(existingHeaders, headerNames.toArray(result));
    }
    
    /**
     * Encodes and folds the header value so it only contains ASCII characters (RFC 2047).
     * 
     * @param headerName Headername is only used to calculate where to fold
     * @param headerValue The header value that need to be folded and converted to ASCII
     * @return the header value folded and RFC 2047 encoded
     * @throws UnsupportedEncodingException 
     */
    public static String encodeHeaderValue(String headerName, String headerValue) 
    throws UnsupportedEncodingException
    {
        if (headerValue == null) {
            return null;
        }
        
        int nameLength = (headerName == null ? 0 : headerName.length());
        
        return MimeUtility.fold(nameLength, MimeUtility.encodeText(headerValue));
    }

    /**
     * Unfolds and decodes the header value 
     *  
     * @param headerValue
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeHeaderValue(String headerValue) 
    throws UnsupportedEncodingException
    {
        if (headerValue == null) {
            return null;
        }
        
        return MimeUtility.decodeText(MimeUtility.unfold(headerValue));
    }
    
    /**
     * Adds the headerline at the top of the headers. The rationale for this method
     * is that Javamail has no direct way to add a header at the start of the headers.
     * 
     * Note 1: It's assume that the header line is already correctly encoded
     * Note 2: If saveMessage is called on the resuling message, the headers are resorted again
     */
    public static void prependHeaderLine(MimeMessage message, String headerLine)
    throws MessagingException
    {
        List<String> original = new LinkedList<String>();
        
        Enumeration<?> headerEnum = message.getAllHeaderLines();
        
        while (headerEnum.hasMoreElements()) {
            original.add((String) headerEnum.nextElement());
        }

        /*
         * remove all existing headers
         */
        headerEnum = message.getAllHeaders();
                
        while (headerEnum.hasMoreElements())
        {
            Header header = (Header) headerEnum.nextElement();
            
            message.removeHeader(header.getName());
        }
        
        /*
         * add the new header line and then add the original headers
         */
        message.addHeaderLine(headerLine);
        
        for (String line : original) {
            message.addHeaderLine(line);
        }
    }
    
    /**
     * Calls MimeUtility#decodeText for the input. When the input cannot be decoded the input will 
     * be returned as-is (ie. no exception will be thrown).
     */
    public static String decodeTextQuietly(String input)
    {
        if (input == null) {
            return null;
        }
        
        String decoded;
        
        try {
            decoded = MimeUtility.decodeText(input);
        }
        catch (UnsupportedEncodingException e) {
            decoded = input;
        }
        
        return decoded;
    }
    
    /**
     * Skip headers. The input stream's current position is positioned at the start of the body.
     */
    public static void skipHeaders(InputStream input)
    throws IOException
    {
        /*
         * A crude but fast way to detect the header body separator. We assume that
         * the input uses CR/LF pairs
         */
        int newLineCount = 0;
        
        int i;
        
        try {
            while((i = input.read()) != -1)
            {
                if (i == '\r' || i == '\n') {
                    newLineCount++;
                }
                else {
                    newLineCount = 0;
                }
                
                if (newLineCount == 4) {
                    break;
                }
            }
        }
        catch (EOFException ioe) {
            // ignore
        }
    }        
}
