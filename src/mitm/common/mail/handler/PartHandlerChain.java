/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.handler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Part;

import mitm.common.mail.PartHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A MessageHandler that calls the added message handlers in consecutive order.
 *  
 * @author Martijn Brinkers
 *
 */
public class PartHandlerChain implements PartHandler
{
    private Logger logger = LoggerFactory.getLogger(PartHandlerChain.class);
    
    /*
     * List of message handlers
     */
    private final List<PartHandler> chain = new LinkedList<PartHandler>();

    /*
     * If true continue with the next handler in the chain when the current handler 
     * throws an exception. If false the chain will stop handling the message. 
     */
    private boolean continueOnException = true;
    
    public PartHandlerChain(PartHandler... messageHandlers) 
    {
        addMessageHandlers(messageHandlers);
    }

    public PartHandlerChain() {
        /* do nothing */
    }    

    /**
     * If true the next handler in the chain will be called when the previous threw an 
     * exception. If false the exception thrown by a handler will stop the chain and the
     * exception will be re-thrown. 
     */
    public void setContinueOnException(boolean continueOnException) {
        this.continueOnException = continueOnException;
    }
    
    public void addMessageHandlers(PartHandler... messageHandlers)
    {
        chain.addAll(Arrays.asList(messageHandlers));
    }
    
    @Override
    public Part handlePart(Part part)
    throws MessagingException 
    {
        if (part == null) {
            return null;
        }
        
        for (PartHandler handler : chain) 
        {
            try {
                part = handler.handlePart(part);
            }
            catch (MessagingException e) {
                if (!continueOnException) {
                    throw e;
                }
                
                logger.error("Error in message handler: " + handler.toString(), e);
            }
        }
        
        return part;
    }
    
    @Override
    public String toString() {
        return PartHandlerChain.class.getCanonicalName();
    }    
}
