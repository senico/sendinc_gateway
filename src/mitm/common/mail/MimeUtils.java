/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;

import mitm.common.locale.CharacterEncoding;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * General MIME utility functions
 * 
 * @author Martijn Brinkers
 *
 */
public class MimeUtils
{
    private final static Logger logger = LoggerFactory.getLogger(MimeUtils.class);
    
    /**
     * Returns the filename of the part. If getting the filename results in a
     * MessagingException the exception is handled and null is returned.
     * 
     * The reason this function is used is that sometimes mail contains an invalid
     * Content-Disposition header. This results in an exception being thrown when
     * accessing the filename.
     */
    public static String getFilenameQuietly(Part part)
    {
        String filename = null;
        
        try {
            filename = part.getFileName();
        }
        catch (MessagingException e) {
            logger.debug("Error while getting the filename.", e);
        }
        
        return filename;
    }
    
    /**
     * Returns the content type text/subtype for the text. The charset is ascii if text only contains ascii characters. 
     * If text contains non-ascii characters the encoding will be charsetIfNotAscii.
     */
    public static String getContentTypeForText(String text, String charsetIfNotAscii, String subtype)
    {
        String charset = CharacterEncoding.US_ASCII;

        if(!MiscStringUtils.isPrintableAscii(text)) {
            charset = charsetIfNotAscii; 
        }

        return "text/" + subtype + "; charset=" + MimeUtility.quote(charset, "()<>@,;:\\\"\t []/?=");
    }
    
    /**
     * Parses the contentType and returns the Java Charset found in the content type. Null if charset 
     * parameter is not found.
     */
    public static String getCharsetFromContentType(String contentType)
    throws MimeTypeParseException
    {
        MimeType mimeType = new MimeType(contentType);

        String charset = mimeType.getParameter("charset");
        
        return StringUtils.isNotEmpty(charset) ? MimeUtility.javaCharset(charset) : null;
    }
}
