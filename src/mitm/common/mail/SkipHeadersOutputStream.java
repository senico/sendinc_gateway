/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import java.io.IOException;
import java.io.OutputStream;

import mitm.common.util.Check;

/**
 * OutputStream extension that skips writing data to the underlying output stream until the
 * message headers are written. 
 * 
 * @author Martijn Brinkers
 *
 */
public class SkipHeadersOutputStream extends OutputStream
{
    /*
     * The output to delegate the calls to
     */
    private final OutputStream delegate;
    
    /*
     * True if the headers were found
     */
    private boolean headersFound;

    /*
     * Keeps track of the mr of CR and LF found in succession
     */
    private int newLineCount;
    
    public SkipHeadersOutputStream(OutputStream delegate)
    {
        Check.notNull(delegate, "delegate");
        
        this.delegate = delegate;
    }
    
    @Override
    public void write(int b)
    throws IOException
    {
        if (headersFound) {
            delegate.write(b);
        }
        else {
            /*
             * A crude but fast way to detect the header body separator. We assume that
             * the input uses CR/LF pairs
             */
            if (b == '\r' || b == '\n') {
                newLineCount++;
            }
            else {
                newLineCount = 0;
            }
            
            if (newLineCount == 4) {
                headersFound = true;
            }
        }
    }
    
    @Override
    public void close()
    throws IOException
    {
        delegate.close();
    }    

    @Override
    public void flush()
    throws IOException
    {
        delegate.flush();
    }    
}
