/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.pdf;

import com.lowagie.text.pdf.PdfWriter;

public enum OpenPermission 
{
    NONE                     (0,                                  "none"),
    ALLOW_PRINTING           (PdfWriter.ALLOW_PRINTING,           "allowPrinting"),
    ALLOW_MODIFY_CONTENTS    (PdfWriter.ALLOW_MODIFY_CONTENTS,    "allowModifyContents"),
    ALLOW_COPY               (PdfWriter.ALLOW_COPY,               "allowCopy"),
    ALLOW_MODIFY_ANNOTATIONS (PdfWriter.ALLOW_MODIFY_ANNOTATIONS, "allowModifyAnnotations"),
    ALLOW_FILL_IN            (PdfWriter.ALLOW_FILL_IN,            "allowFillIn"),
    ALLOW_SCREENREADERS      (PdfWriter.ALLOW_SCREENREADERS,      "allowScreenReaders"),
    ALLOW_ASSEMBLY           (PdfWriter.ALLOW_ASSEMBLY,           "allowAssembly"),
    ALLOW_DEGRADED_PRINTING  (PdfWriter.ALLOW_DEGRADED_PRINTING,  "allowDegradedPrinting"),
    ALL                      (PdfWriter.ALLOW_PRINTING | 
                              PdfWriter.ALLOW_MODIFY_CONTENTS |
                              PdfWriter.ALLOW_COPY |
                              PdfWriter.ALLOW_MODIFY_ANNOTATIONS |
                              PdfWriter.ALLOW_FILL_IN |
                              PdfWriter.ALLOW_SCREENREADERS |
                              PdfWriter.ALLOW_ASSEMBLY |
                              PdfWriter.ALLOW_DEGRADED_PRINTING,  "all")
    
    ;

    private final int intPermission;
    private final String name;
    
    private OpenPermission(int intPermission, String name)
    {
        this.intPermission = intPermission;
        this.name = name;
    }
    
    public int intValue() {
        return intPermission;
    }
    
    public String getName() {
        return name;
    }
    
    public static OpenPermission fromName(String name)
    {
        for (OpenPermission permission : OpenPermission.values()) 
        {
            if (permission.getName().equalsIgnoreCase(name)) 
            {
                return permission;
            }
        }
        
        return null;
    }
}
