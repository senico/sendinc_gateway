/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.pdf;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import mitm.common.util.Check;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;

/**
 * Implementation of FontProvider that reads the .ttf files from the provided font 
 * directory at startup. 
 * 
 * @author Martijn Brinkers
 *
 */
public class FileFontProvider implements FontProvider
{
    private final static Logger logger = LoggerFactory.getLogger(FileFontProvider.class);
    
    /*
     * The fonts read from the font dir
     */
    private final Collection<Font> fonts;
    
    public FileFontProvider(File fontDir)
    {
        Check.notNull(fontDir, "fontDir");
        
        String[] fontFiles = fontDir.list(new SuffixFileFilter(".ttf"));
        
        if (fontFiles == null) {
            fontFiles = new String[]{};
        }
        
        fonts = new ArrayList<Font>(fontFiles.length);
        
        for (String fontFile : fontFiles)
        {
            logger.info("Adding font " + fontFile);

            try {
                fonts.add(new Font(BaseFont.createFont(new File(fontDir, fontFile).getPath(), 
                        BaseFont.IDENTITY_H, true)));
            }
            catch (Throwable e) {
                logger.error("Error loading font " + fontFile, e);
            }
        }
    }
    
    @Override
    public Collection<Font> getFonts() {
        return fonts;
    }
}
