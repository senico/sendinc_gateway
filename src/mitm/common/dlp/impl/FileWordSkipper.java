/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import mitm.common.dlp.WordSkipper;
import mitm.common.util.Check;
import mitm.common.util.UnicodeReader;

import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;

/**
 * A WordSkipper implementation that reads the words to be skipped from a file. Each word should be on a separate line. 
 * Lines that start with # are comment lines and are skipped.
 * 
 * @author Martijn Brinkers
 *
 */
public class FileWordSkipper implements WordSkipper
{
    /*
     * The set of words to skip. The words will be stored with lowercase.
     */
    private Set<String> words = new HashSet<String>();
    
    /*
     * The file to read the words from
     */
    private final File file;
    
    public FileWordSkipper(File file)
    throws IOException
    {
        Check.notNull(file, "file");
        
        this.file = file;
        
        loadFile();
    }

    private void loadFile()
    throws IOException
    {
        if (!file.exists()) {
            throw new IOException("File " + file.getAbsolutePath() + " does not exist.");
        }
        
        /*
         * Read the file using a Unicode aware reader
         */
        UnicodeReader unicodeReader = new UnicodeReader(new FileInputStream(file), CharEncoding.UTF_8);

        LineIterator lineIterator = new LineIterator(unicodeReader);
                
        while (lineIterator.hasNext())
        {
            String line = StringUtils.trimToNull(lineIterator.nextLine());
            
            if (line == null || line.startsWith("#")) {
                continue;
            }
            
            words.add(line.toLowerCase());
        }
    }
    
    @Override
    public synchronized boolean isSkip(String word)
    {
        if (word == null) {
            return true;
        }
        
        return words.contains(word.toLowerCase());
    }
}
