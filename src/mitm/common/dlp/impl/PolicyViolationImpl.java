/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;

import org.apache.commons.lang.text.StrBuilder;

/**
 * Default implementation of PolicyViolation.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyViolationImpl implements PolicyViolation
{
    private static final long serialVersionUID = 8399148439361400478L;
    
    private final String policy;
    private final String rule;
    private final String match;
    private final PolicyViolationPriority priority;

    public PolicyViolationImpl(String policy, String rule, String match, PolicyViolationPriority priority)
    {
        this.policy = policy;
        this.rule = rule;
        this.match = match;
        this.priority = priority;
    }
    
    @Override
    public String getPolicy() {
        return policy;
    }

    @Override
    public String getRule() {
        return rule;
    }

    @Override
    public String getMatch() {
        return match;
    }
    
    @Override
    public PolicyViolationPriority getPriority() {
        return priority;
    }
    
    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder(256);
        
        sb.append("Policy: ").append(policy).
           append(", Rule: ").append(rule).
           append(", Priority: ").append(priority).
           append(", Match: ").append(match);
        
        return sb.toString();
    }
}
