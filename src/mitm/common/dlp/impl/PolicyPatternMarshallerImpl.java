/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import mitm.common.cache.PatternCache;
import mitm.common.dlp.MatchFilterRegistry;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyPatternMarshaller;
import mitm.common.util.Check;
import mitm.common.util.MarshallException;

/**
 * Used to marshall and unmarshall PolicyPatternImpl to/from xml.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternMarshallerImpl implements PolicyPatternMarshaller
{
    /*
     * Used to get cached compiled Pattern's from a reg expr string.
     */
    private final PatternCache patternCache;
    
    /*
     * Used to get MatchFilter from a name. 
     */
    private final MatchFilterRegistry matchFilterRegistry;

    /*
     * Note: Unmarshaller is not thread safe so we need to synchronize
     * 
     * See http://jaxb.java.net/guide/Performance_and_thread_safety.html
     */
    private final Unmarshaller unmarshaller;

    /*
     * Note: Marshaller is not thread safe so we need to synchronize
     */
    private final Marshaller marshaller;
    
    public PolicyPatternMarshallerImpl(PatternCache patternCache, MatchFilterRegistry matchFilterRegistry)
    throws JAXBException
    {
        Check.notNull(patternCache, "patternCache");
        Check.notNull(matchFilterRegistry, "matchFilterRegistry");
        
        this.patternCache = patternCache;
        this.matchFilterRegistry = matchFilterRegistry;
        
        JAXBContext jcontext = JAXBContext.newInstance(PolicyPatternImpl.class.getPackage().getName());
        
        unmarshaller = jcontext.createUnmarshaller();
        marshaller = jcontext.createMarshaller();
    }
    
    /**
     * Unmarshals the input xml
     * 
     * TODO: check if using a pool of marshallers/unmarshallers is faster than synchronizing on unmarshaller
     */
	@Override
    public PolicyPatternImpl unmarshall(InputStream input) 
	throws MarshallException
	{
	    try {
    	    PolicyPatternImpl policyPattern = null;
    		
    	    Object object;
    	    
    	    synchronized(unmarshaller) {  
    	        object = unmarshaller.unmarshal(input);
    	    }
    		
    		if (object instanceof JAXBElement) 
    		{
    			JAXBElement<?> element = (JAXBElement<?>) object;
    			
    			if (element.getValue() instanceof PolicyPatternImpl) 
    			{
    			    policyPattern = (PolicyPatternImpl) element.getValue();
    			    
    			    policyPattern.setPatternCache(patternCache);
    			    policyPattern.setMatchFilterRegistry(matchFilterRegistry);
    			}
    		}
    		
    		return policyPattern;
	    }
	    catch(JAXBException e) {
	        throw new MarshallException(e);
	    }
	}
	
	/**
	 * Marshalls the policyPattern to the output stream.
	 * 
     * TODO: check if using a pool of marshallers/unmarshallers is faster than synchronizing on unmarshaller
	 */
	@Override
    public void marshall(PolicyPattern policyPattern, OutputStream output)
	throws MarshallException
	{
	    try {
	        synchronized (marshaller) {
                /*
                 * marshal without requiring a @XmlRootElement
                 * 
                 * See: http://weblogs.java.net/blog/kohsuke/archive/2006/03/why_does_jaxb_p.html
                 */
                marshaller.marshal(new JAXBElement<PolicyPattern>(
                          new QName(null, PolicyPatternImpl.XML_TYPE_NAME), PolicyPattern.class, policyPattern), output);
	        }
        }
        catch(JAXBException e) {
            throw new MarshallException(e);
        }
	}
}