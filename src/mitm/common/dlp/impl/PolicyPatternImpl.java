/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import mitm.common.cache.PatternCache;
import mitm.common.dlp.MatchFilter;
import mitm.common.dlp.MatchFilterRegistry;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.util.Check;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * A PolicyPattern implementation that can be marshalled and unmarshalled to/from XML using JAXB. 
 * 
 * @author Martijn Brinkers
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(name = PolicyPatternImpl.XML_TYPE_NAME)
public class PolicyPatternImpl implements PolicyPattern
{
    protected final static String XML_TYPE_NAME = "PolicyPattern";
    
    /*
     * The name of this policy pattern (aka the rule)
     */
    private String name;

    /*
     * The reg exp pattern of this rule 
     */
    private Pattern pattern;
    
    /*
     * filter that will be used to filter any matching content.
     */
    private MatchFilter matchFilter;
    
    /*
     * The reg exp pattern of this rule 
     */
    private String regExp;
    
    /*
     * The name of the filter that will be used to filter any matching content.
     */
    private String matchFilterName;
    
    /*
     * Explains what this pattern policy does.
     */
    private String description;

    /*
     * If the pattern matches the content more than the threshold, the policy is violated.
     */
    private int threshold;

    /*
     * The priority associated with this pattern
     */
    private PolicyViolationPriority priority;
    
    /*
     * Used for caching patterns.
     */
    private PatternCache patternCache;
    
    /*
     * Registry used to lookup MatchFilter's based on the name of the MatchFilter. 
     */
    private MatchFilterRegistry matchFilterRegistry;
    
    
    public PolicyPatternImpl(String name, Pattern pattern)
    {
        Check.notNull(name, "name");
        Check.notNull(pattern, "pattern");
        
        this.name = name;
        
        setPattern(pattern);
    }

    public PolicyPatternImpl() {
        /*
         * JAXB requires a default constructor
         */
    }

    @Override
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @XmlElement
    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    @XmlElement
    public String getMatchFilterName() {
        return matchFilterName;
    }

    public void setMatchFilterName(String matchFilterName) {
        this.matchFilterName = matchFilterName;
    }

    @Override
    @XmlElement
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    @XmlElement
    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
    
    @Override
    @XmlElement
    public PolicyViolationPriority getPriority() {
        return priority;
    }

    public void setPriority(PolicyViolationPriority priority) {
        this.priority = priority;
    }
        
    public void setMatchFilter(MatchFilter matchFilter)
    {
        this.matchFilter = matchFilter;
        
        matchFilterName = matchFilter != null ? matchFilter.getName() : null;
    }
    
    @Override
    public MatchFilter getMatchFilter()
    {
        if (matchFilterName != null && (matchFilter == null || !matchFilterName.equals(matchFilter.getName())))
        {
            Check.notNull(matchFilterRegistry, "matchFilterRegistry");
            
            matchFilter = matchFilterRegistry.getMatchFilter(matchFilterName);
        }

        return matchFilter;
    }

    public void setPattern(Pattern pattern)
    {
        this.pattern = pattern;
        
        regExp = pattern != null ? pattern.pattern() : null;
    }
    
    @Override
    public Pattern getPattern()
    {
        if (regExp != null && (pattern == null || !regExp.equals(pattern.pattern())))
        {
            Check.notNull(patternCache, "patternCache");

            pattern = patternCache.getPattern(regExp);
        }

        return pattern;
    }

    public PatternCache getPatternCache() {
        return patternCache;
    }

    public void setPatternCache(PatternCache patternCache) {
        this.patternCache = patternCache;
    }

    public MatchFilterRegistry getMatchFilterRegistry() {
        return matchFilterRegistry;
    }

    public void setMatchFilterRegistry(MatchFilterRegistry matchFilterRegistry) {
        this.matchFilterRegistry = matchFilterRegistry;
    }    
 
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternImpl)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        PolicyPatternImpl rhs = (PolicyPatternImpl) obj;
        
        return new EqualsBuilder()
            .append(name, rhs.name)
            .isEquals();
    }
    
    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().
            append(name).
            toHashCode();
    }
    
    /**
     * Creates a new instance of PolicyPatternImpl with the name and copies all public data from 
     * the pattern to clone to the new instance.
     */
    public static PolicyPatternImpl clone(PolicyPattern pattern, String name)
    {
        PolicyPatternImpl clone = new PolicyPatternImpl();
        
        clone.name = name;

        if (pattern != null)
        {
            clone.setDescription(pattern.getDescription());
            clone.setPattern(pattern.getPattern());
            clone.setThreshold(pattern.getThreshold());
            clone.setPriority(pattern.getPriority());
            clone.setMatchFilter(pattern.getMatchFilter());
        }
        
        return clone;
    }
}
