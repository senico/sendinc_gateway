/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.template;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;

import mitm.common.locale.CharacterEncoding;
import mitm.common.locale.DefaultLocale;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * TemplateBuilder is a helper class to build process Freemarker templates.
 * 
 * @author Martijn Brinkers
 *
 */
public class TemplateBuilderImpl implements TemplateBuilder
{
    /*
     * Freemarker configuration. We will use a static Configuration so we do not have to create
     * a new Configuration every time.
     * 
     * Note: According to Freemarker documentation
     * In a multithreaded environment Configuration  instances, Template instances and data-models 
     * should be handled as immutable (read-only) objects. That is, you create and initialize them 
     * (for example with set... methods), and then you don't modify them later (e.g. you don't call set...).
     */
    private final Configuration templateConfig;
    
    public TemplateBuilderImpl(File templateBaseDir)
    throws IOException
    {
        Check.notNull(templateBaseDir, "templateBaseDir");
        
        templateConfig = new Configuration();
        templateConfig.setDefaultEncoding(CharacterEncoding.UTF_8);
        templateConfig.setEncoding(DefaultLocale.getDefaultLocale(), CharacterEncoding.UTF_8);

        templateConfig.setObjectWrapper(new DefaultObjectWrapper());
        
        TemplateUtils.addDefaultSharedVariables(templateConfig);

        templateConfig.setTemplateLoader(new FileTemplateLoader(templateBaseDir));
    }
    
    /**
     * Returns the template with the given name
     */
    @Override
    public Template getTemplate(String name)
    throws IOException
    {
        return templateConfig.getTemplate(name);
    }
    
    /**
     * Creates a template from the provided source
     */
    @Override
    public Template createTemplate(Reader reader)
    throws IOException
    {
        return new Template("ftl", reader, templateConfig, CharacterEncoding.UTF_8); 
    }
        
    /**
     * Creates a template from the templateSource and processes the template. The result in stored
     * in the output.
     */
    @Override
    public void buildTemplate(String templateSource, SimpleHash properties, OutputStream output)
    throws TemplateBuilderException
    {
        try {            
            Writer writer = new OutputStreamWriter(output);
    
            createTemplate(new StringReader(templateSource)).process(properties, writer);
        }
        catch(IOException e) {
            throw new TemplateBuilderException(e);
        }
        catch (TemplateException e) {
            throw new TemplateBuilderException(e);
        }
    }
    
    /**
     * Creates a template from the templateSource and processes the template. The result is returned.
     */
    @Override
    public String buildTemplate(String templateSource, SimpleHash properties)
    throws TemplateBuilderException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        buildTemplate(templateSource, properties, bos);

        return MiscStringUtils.toUTF8String(bos.toByteArray());
    }    
}
