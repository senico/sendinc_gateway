package mitm.common.tools;

import java.io.File;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Iterator;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.mailet.Mail;

/**
 * Tool which can be used to inspect mail FileObjectStore files.
 * 
 * @author Martijn Brinkers
 *
 */
public class MailUtil
{
    @SuppressWarnings("static-access")
    private static Options createCommandLineOptions()
    {
        Options options = new Options();

        Option inOption = OptionBuilder.withArgName("in")
            .hasArg().withDescription("input file")
            .create("in");
        inOption.setRequired(true);
        options.addOption(inOption);
        
        return options;
    }
    
    private static PrintStream print(String text)
    {
        System.out.print(StringUtils.defaultString(text));
        
        return System.out;
    }

    private static PrintStream println(String text)
    {
        System.out.println(StringUtils.defaultString(text));
        
        return System.out;
    }

    private static void printAttributeValue(Serializable serializable)
    {
        if (serializable == null) {
            return;
        }
        
        if (serializable instanceof Throwable) 
        {
            Throwable t = ((Throwable) serializable);
            
            println(ExceptionUtils.getFullStackTrace(t));
        }
        else {
            println(serializable.toString());
        }
    }
    
    public static void main(String[] args) 
    {
        try {
            CommandLineParser parser = new BasicParser();
            
            Options options = createCommandLineOptions();
            
            HelpFormatter formatter = new HelpFormatter();
    
            CommandLine commandLine;
            
            try {
                commandLine = parser.parse(options, args);
            }
            catch (ParseException e) {
                formatter.printHelp("MailUtil", options, true);
    
                throw e;
            }
        
            String inFile = commandLine.getOptionValue("in");
            
            Mail mail = (Mail) SerializationUtils.deserialize(FileUtils.readFileToByteArray(new File(inFile)));
            
            print("Name: ").println(mail.getName());
            print("Recipients: ").println(StringUtils.join(mail.getRecipients(), ", "));
            print("Sender: ").println(mail.getSender());
            print("State: ").println(mail.getState());
            print("Remote Addr: ").println(mail.getRemoteAddr());
            print("Error message: ").println(mail.getErrorMessage());
            print("Last updated: ").println(mail.getLastUpdated());
            println("*** Attributes ***");
            
            @SuppressWarnings("unchecked")
            Iterator<String> nameIterator = mail.getAttributeNames();
            
            while (nameIterator.hasNext())
            {
                String name = nameIterator.next();
                
                print(name + ": ");
                
                printAttributeValue(mail.getAttribute(name));
            }
        }
        catch (Exception e)
        {
            System.out.println("***** ERROR ****");
            e.printStackTrace();
        }
    }
}
