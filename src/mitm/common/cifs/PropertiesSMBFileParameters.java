/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cifs;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

/**
 * Implementation of SMBFileParameters that reads and writes it's settings from
 * a HierarchicalProperties object.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesSMBFileParameters implements SMBFileParameters
{
    private final static String BASE_PROP = "smbFileParameters.";
    private final static String DOMAIN = BASE_PROP + "domain";
    private final static String USERNAME = BASE_PROP + "username";
    private final static String PASSWORD = BASE_PROP + "password";
    private final static String SERVER = BASE_PROP + "server";
    private final static String PORT = BASE_PROP + "port";
    private final static String SHARE = BASE_PROP + "share";
    private final static String DIR = BASE_PROP + "dir";
    private final static String FILE = BASE_PROP + "file";
    
    private final HierarchicalProperties properties;
    
    public PropertiesSMBFileParameters(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }

    @Override
    public String getDomain()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(DOMAIN, false);
    }

    @Override
    public void setDomain(String domain)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(DOMAIN, domain, false);
    }

    @Override
    public String getUsername()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(USERNAME, false);
    }

    @Override
    public void setUsername(String username)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(USERNAME, username, false);
    }

    @Override
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PASSWORD, true);
    }

    @Override
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PASSWORD, password, true);
    }

    @Override
    public String getServer()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(SERVER, false);
    }

    @Override
    public void setServer(String server)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(SERVER, server, false);
    }

    @Override
    public Integer getPort()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, PORT, null, false);
    }

    @Override
    public void setPort(Integer port)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, PORT, port, false);
    }

    @Override
    public String getShare()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(SHARE, false);
    }

    @Override
    public void setShare(String share)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(SHARE, share, false);
    }

    @Override
    public String getDir()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(DIR, false);
    }

    @Override
    public void setDir(String dir)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(DIR, dir, false);
    }

    @Override
    public String getFile()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(FILE, false);
    }

    @Override
    public void setFile(String file)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(FILE, file, false);
    }
    
    @Override
    public void copyTo(SMBFileParameters other)
    throws HierarchicalPropertiesException
    {
        other.setDomain(this.getDomain());
        other.setUsername(this.getUsername());
        other.setPassword(this.getPassword());
        other.setServer(this.getServer());
        other.setPort(this.getPort());
        other.setShare(this.getShare());
        other.setDir(this.getDir());
        other.setFile(this.getFile());
    }    
}
