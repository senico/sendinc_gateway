/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cifs;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;

import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

/**
 * Helper class for building SMB URLs (see http://jcifs.samba.org/src/docs/api/jcifs/smb/SmbFile.html)
 * 
 * @author Martijn Brinkers
 *
 */
public class SMBURLBuilder
{    
    private String urlEncode(String input)
    throws UnsupportedEncodingException
    {
        return URLEncoder.encode(input, "UTF-8");
    }

    private String addTrailingSlash(String input)
    {
        if (!input.endsWith("/")) {
            input = input + "/";
        }
        
        return input;
    }

    private String removeStartingSlash(String input)
    {
        if (input.startsWith("/")) {
            input = StringUtils.substring(input, 1);
        }
        
        return input;
    }

    private String normalizeShareAndDir(String input)
    {
        input = StringUtils.trimToEmpty(input);
        
        if (StringUtils.isNotEmpty(input))
        {
            input = removeStartingSlash(input);
            input = addTrailingSlash(input);
        }
        
        return input;
    }
    
    private String buildUserInfo(SMBFileParameters parameters) 
    throws MalformedURLException, HierarchicalPropertiesException
    {		
        try {
            StrBuilder sb = new StrBuilder();

            if (StringUtils.isNotBlank(parameters.getDomain()))
            {
                sb.append(urlEncode(parameters.getDomain())).append(";");
            }

            if (StringUtils.isNotBlank(parameters.getUsername()))
            {
                sb.append(urlEncode(parameters.getUsername()));

                if (StringUtils.isNotBlank(parameters.getPassword()))
                {
                    sb.append(":").append(urlEncode(parameters.getPassword()));
                }
            }

            return sb.toString();
        } 
        catch (UnsupportedEncodingException e) {
            throw new MalformedURLException("UTF-8 is unsupported.");
        }
    }

    public String buildURL(SMBFileParameters parameters)
    throws MalformedURLException, HierarchicalPropertiesException
    {
        StrBuilder sb = new StrBuilder();

        sb.append("smb://");

        if (StringUtils.isNotBlank(parameters.getServer()))
        {
            String userInfo = buildUserInfo(parameters);

            if (StringUtils.isNotBlank(userInfo)) {
                sb.append(userInfo).append("@");
            }	

            sb.append(StringUtils.trim(parameters.getServer()));

            if (parameters.getPort() != null) {
                sb.append(":").append(parameters.getPort());
            }	

            sb.append("/");

            if (StringUtils.isNotBlank(parameters.getShare())) {
                sb.append(normalizeShareAndDir(parameters.getShare()));
            }   

            if (StringUtils.isNotBlank(parameters.getDir())) {
                sb.append(normalizeShareAndDir(parameters.getDir()));
            }   

            if (StringUtils.isNotBlank(parameters.getFile())) {
                sb.append(StringUtils.trim(parameters.getFile()));
            }   
        }
        return sb.toString();
    }
}
