/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cifs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import mitm.common.properties.HierarchicalPropertiesException;

@XmlType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StaticSMBFileParameters implements SMBFileParameters
{
    /*
     * The SMB domain
     */
    @XmlElement
    private String domain;

    /*
     * The SMB user
     */
    @XmlElement
    private String username;

    /*
     * The SMB password
     */
    @XmlElement
    private String password;

    /*
     * The SMB server
     */
    @XmlElement
    private String server;

    /*
     * The SMB port
     */
    @XmlElement
    private Integer port;

    /*
     * The SMB share (must end with /)
     */
    @XmlElement
    private String share;

    /*
     * The SMB dir (must end with /)
     */
    @XmlElement
    private String dir;

    /*
     * The SMB file
     */
    @XmlElement
    private String file;

    public StaticSMBFileParameters() {
        /* JAXB requires a default constructor */        
    }
    
    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getServer() {
        return server;
    }

    @Override
    public void setServer(String server) {
        this.server = server;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String getShare() {
        return share;
    }

    @Override
    public void setShare(String share) {
        this.share = share;
    }

    @Override
    public String getDir() {
        return dir;
    }

    @Override
    public void setDir(String dir) {
        this.dir = dir;
    }

    @Override
    public String getFile() {
        return file;
    }

    @Override
    public void setFile(String file) {
        this.file = file;
    }
    
    @Override
    public void copyTo(SMBFileParameters other)
    throws HierarchicalPropertiesException
    {
        other.setDomain(this.domain);
        other.setUsername(this.username);
        other.setPassword(this.password);
        other.setServer(this.server);
        other.setPort(this.port);
        other.setShare(this.share);
        other.setDir(this.dir);
        other.setFile(this.file);
    }
}
