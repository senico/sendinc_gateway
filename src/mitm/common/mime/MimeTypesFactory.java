/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mitm.common.mime;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import mitm.common.util.Check;

import org.apache.commons.io.IOUtils;


/**
 * Creates instances of MimeTypes.
 * 
 * Taken from Tika (tika.apache.org) with minor changes by Martijn Brinkers. 
 * 
 */
public class MimeTypesFactory
{
    /**
     * Creates an empty instance; same as calling new MimeTypes().
     *
     * @return an empty instance
     */
    public static MimeTypes create() {
        return new MimeTypes();
    }

    /**
     * Creates and returns a MimeTypes instance from the specified input stream.
     * Does not close the input stream.
     * @throws IOException if the stream can not be read
     * @throws MimeMediaTypeException if the type configuration is invalid
     */
    public static MimeTypes create(InputStream inputStream)
    throws IOException, MimeMediaTypeException
    {
        Check.notNull(inputStream, "inputStream");
        
        MimeTypes mimeTypes = new MimeTypes();
        
        new MimeTypesReader(mimeTypes).read(inputStream);
        
        return mimeTypes;
    }
    
    /**
     * Creates and returns a MimeTypes instance from the specified file.
     */
    public static MimeTypes create(File mimeTypes)
    throws IOException, MimeMediaTypeException
    {
        Check.notNull(mimeTypes, "mimeTypes");
        
        FileInputStream input = new FileInputStream(mimeTypes);
        
        try {
            return create(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }
}
