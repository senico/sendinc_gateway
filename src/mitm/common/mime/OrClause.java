/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mitm.common.mime;

import java.util.List;

/**
 *
 * Taken from Tika (tika.apache.org) with minor changes by Martijn Brinkers.
 */
class OrClause implements Clause
{
    public static final long serialVersionUID = 7023331219101561848L;

    private final List<Clause> clauses;

    public OrClause(List<Clause> clauses) {
        this.clauses = clauses;
    }

    @Override
    public boolean eval(byte[] data)
    {
        for (Clause clause : clauses)
        {
            if (clause.eval(data)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size()
    {
        int size = 0;

        for (Clause clause : clauses) {
            size = Math.max(size, clause.size());
        }
        return size;
    }

    @Override
    public String toString() {
        return "or" + clauses;
    }
}
