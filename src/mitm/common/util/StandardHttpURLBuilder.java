/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.Mac;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

/**
 * Implementation of URLBuilder for building HTTP URLs.
 * 
 * Examaple URL
 * 
 * http://www.example.com?param1=value1&param2=value2
 * 
 * @author Martijn Brinkers
 *
 */
public class StandardHttpURLBuilder implements URLBuilder
{
	private static class Parameter
	{
		private String name;
		private String value;
		
		Parameter(String name, String value)
		{
			this.name = name;
			this.value = value;
		}
		
		public String getName() {
			return name;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	/*
	 * The base URL for the final URL. example: http://www.example.com
	 */
	private String baseURL;
	
	/*
	 * The list of parameters
	 */
	private List<Parameter> parameters = new LinkedList<Parameter>();
    
    @Override
	public void setBaseURL(String baseURL)
	throws URLBuilderException
	{
		Check.notNull(baseURL, "baseURL");

		this.baseURL = StringUtils.removeEnd(baseURL.trim(), "/");
	}
	
    @Override
	public void addParameter(String name, String value)
	throws URLBuilderException
	{
		if (StringUtils.isEmpty(name)) {
			throw new URLBuilderException("parameter name cannot be empty.");
		}
		
		name = name.trim();
		
		if (value == null) {
			value = "";
		}
		
		parameters.add(new Parameter(name, value));
	}
	
    @Override
	public String addHMAC(String name, Mac mac)
	throws URLBuilderException
	{
		Check.notNull(mac, "mac");
		
		if (parameters.size() == 0) {
			throw new URLBuilderException("There are no values.");
		}
		
		for (Parameter parameter : parameters) 
		{
			mac.update(MiscStringUtils.toAsciiBytes(parameter.getName()));
			mac.update(MiscStringUtils.toAsciiBytes(parameter.getValue()));
		}
		
		byte[] hmac = mac.doFinal();
		
		String base32 = Base32.encode(hmac);
		
		parameters.add(new Parameter(name, base32));
		
		return base32;
	}

    @Override
	public String buildURL()
	throws URLBuilderException
	{
		String url = baseURL;

		if (!StringUtils.contains(url, '?') && parameters.size() > 0) {
			url = url + '?';
		}
		
		url = url + buildQueury();
		
		return url;
	}
	
    @Override
	public String buildQueury()
	throws URLBuilderException
	{
		StrBuilder sb = new StrBuilder(256);
		
		for (Parameter parameter : parameters) 
		{
			try {
				sb.appendSeparator("&");
				sb.append(URLEncoder.encode(parameter.getName(), "UTF-8"));
				sb.append("=");
				sb.append(URLEncoder.encode(parameter.getValue(), "UTF-8"));
			} 
			catch (UnsupportedEncodingException e) {
				throw new URLBuilderException(e);
			}
			
		}
		
		return sb.toString();
	}
	
    @Override
	public void reset()
	throws URLBuilderException
	{
		baseURL = null;
		parameters.clear();
	}
}
