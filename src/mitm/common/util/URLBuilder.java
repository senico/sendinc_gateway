/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import javax.crypto.Mac;

public interface URLBuilder
{
	/**
	 * Sets the base URL (for example: http://www.example.com)
	 */
	public void setBaseURL(String baseURL)
	throws URLBuilderException;
	
	/**
	 * Adds a parameter with name and given value
	 */
	public void addParameter(String name, String value)
	throws URLBuilderException;

	/**
	 * Calculates the HMAC of the parameters (with key) that are added and adds the HMAC value as parameter with the name.
	 * The HMAC calculation is done for the parameters currently added. Parameters added after calculating the
	 * HMAC are not used for the HMAC calculation. The mac object should be initialized before calling addHMAC. 
	 * The hmac value will be returned (and added to the parameters) as a Base64 encoded string
	 */
	public String addHMAC(String name, Mac mac)
	throws URLBuilderException;
	
	/**
	 * Builds and returns the URL
	 */
	public String buildURL()
	throws URLBuilderException;

	/**
	 * Builds and returns only the query part of the URL
	 */
	public String buildQueury()
	throws URLBuilderException;
	
	/**
	 * Resets the URLBuilder to it's initial state.
	 */
	public void reset()
	throws URLBuilderException;
}
