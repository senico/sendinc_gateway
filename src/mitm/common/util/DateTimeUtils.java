/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

public class DateTimeUtils
{
    public static final int NANOS_PER_SECOND = 1000000000;
    public static final int NANOS_PER_MILLIS = 1000000;
    
    public static long nanosecondsToSeconds(long nanoseconds) {
        return (long) (nanoseconds / NANOS_PER_SECOND);
    }

    public static Long nanosecondsToSeconds(Long nanoseconds) {
        return nanoseconds != null ? nanosecondsToSeconds((long)nanoseconds) : null;
    }

    public static long nanosecondsToMilliseconds(long nanoseconds) {
        return (long) (nanoseconds / NANOS_PER_MILLIS);
    }

    public static Long nanosecondsToMilliseconds(Long nanoseconds) {
        return nanoseconds != null ? nanosecondsToMilliseconds((long)nanoseconds) : null;
    }

    public static Long millisecondsToNanoseconds(Long milliseconds) {
        return milliseconds != null ? millisecondsToNanoseconds((long)milliseconds) : null;
    }

    public static long millisecondsToNanoseconds(long milliseconds) {
        return milliseconds * NANOS_PER_MILLIS;
    }
    
    public static long millisecondsToSeconds(long milliseconds) {
        return (long) (milliseconds / DateUtils.MILLIS_PER_SECOND);
    }
    
    public static Long millisecondsToSeconds(Long milliseconds) {
        return milliseconds != null ? millisecondsToSeconds((long)milliseconds) : null;
    }
    
    public static long millisecondsToMinutes(long milliseconds) {
        return (long) (milliseconds / DateUtils.MILLIS_PER_MINUTE);
    }

    public static Long millisecondsToMinutes(Long milliseconds) {
        return milliseconds != null ? millisecondsToMinutes((long)milliseconds) : null;
    }
    
    public static long millisecondsToHours(long milliseconds) {
        return (long) (milliseconds / DateUtils.MILLIS_PER_HOUR);
    }

    public static Long millisecondsToHours(Long milliseconds) {
        return milliseconds != null ? millisecondsToHours((long)milliseconds) : null;
    }
    
    public static long minutesToMilliseconds(long minutes) {
        return (long) (minutes * DateUtils.MILLIS_PER_MINUTE);
    }

    public static Long minutesToMilliseconds(Long minutes) {
        return minutes != null ? minutesToMilliseconds((long)minutes) : null;
    }
    
    public static long diffMilliseconds(Date date1, Date date2) {
        return date1.getTime() - date2.getTime();
    }

    public static long diffSeconds(Date date1, Date date2) {
        return (date1.getTime() - date2.getTime()) / DateUtils.MILLIS_PER_SECOND;
    }

    public static long diffMinutes(Date date1, Date date2) {
        return (date1.getTime() - date2.getTime()) / DateUtils.MILLIS_PER_MINUTE;
    }

    public static long diffHours(Date date1, Date date2) {
        return (date1.getTime() - date2.getTime()) / DateUtils.MILLIS_PER_HOUR;
    }
    
    public static long diffDays(Date date1, Date date2) {
        return (date1.getTime() - date2.getTime()) / DateUtils.MILLIS_PER_DAY;
    }
    
    public static Date addMilliseconds(Date date, long milliseconds) 
    {
    	Check.notNull(date, "date");
    	
        return new Date(date.getTime() + milliseconds);
    }
    
    /**
     * Compares the two dates. Returns a negative integer, zero, or a positive integer as the first date is less 
     * than, equal to, or greater than the second. 
     */
    public static int compare(Date date, Date otherDate)
    {
        if (date == null && otherDate == null) {
            return 0;
        }
        
        if (date == null) {
            return -1;
        }
        
        if (otherDate == null) {
            return 1;
        }
        
        return date.compareTo(otherDate);
    }
}
