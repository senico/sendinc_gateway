/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

/**
 * InputStream extension that wraps another InputStream and makes sure that CR and LF always
 * come in CR/LF pairs
 * 
 * @author Martijn Brinkers
 *
 */
public class CRLFInputStream extends InputStream
{
    private final static int CR = '\r';
    private final static int LF = '\n';
    
    /*
     * The wrapped input stream
     */
    private final PushbackInputStream delegate;
    
    /*
     * True if the previous byte was CR
     */
    private boolean prevCR;
    
    /*
     * True if at end of stream
     */
    private boolean atEnd;
    
    public CRLFInputStream(InputStream delegate)
    {
        Check.notNull(delegate, "delegate");
        
        this.delegate = new PushbackInputStream(delegate);
    }

    @Override
    public int read()
    throws IOException
    {
        if (atEnd) {
            return -1;
        }
        
        int read = delegate.read();
        
        if (read == -1)
        {
            atEnd = true;
            
            /*
             * If the previous written was a CR we still need to
             * output a LF even if we are at the end or the stream
             */
            if (prevCR) {
                return LF;
            }
            else {
                return -1;
            }
        }
         
        if (read == LF)
        {
            if (prevCR)
            {
                prevCR = false;
                
                return LF;
            }
            else {
                /*
                 * LF which is not prefixed with CR
                 */
                delegate.unread(read);

                prevCR = true;
                
                return CR;
            }
        }
        else if (read == CR)
        {
            if (prevCR) {
                /*
                 * CR after a CR
                 */
                delegate.unread(read);

                prevCR = false;

                return LF;
            }
            else {
                prevCR = true;
                
                return CR;
            }
        }
        else
        {
            /*
             * No CR or LF
             */
            if (prevCR) {
                /*
                 * missing LF after CR
                 */
                delegate.unread(read);
                
                prevCR = false;

                return LF;
            }
            else {
                prevCR = false;
                
                return read;
            }
        }        
    }
}
