/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.input.CountingInputStream;

/**
 * An InputStream that throws a {@link LimitReachedException} when the number of bytes read exceeds the set 
 * maximum and throwException is true. If throwException is false and the max is reached, this InputStream no
 * longer returns any data. The {@link LimitReachedException} is only thrown the first time the limit is 
 * reached. Successive calls will not return any data (i.e., read's return -1 etc.). 
 * 
 * @author Martijn Brinkers
 *
 */
public class SizeLimitedInputStream extends CountingInputStream
{
    public static final String MAXIMUM_REACHED = "Maximum bytes read have been reached."; 
    
    /*
     * Maximum bytes to read.
     * 
     * Note: it can happen that more bytes are read when read(byte b[], int off, int len) is called.
     */
    private final long maxBytes;
    
    /*
     * Throw exception if max reached
     */
    private final boolean throwException;
    
    /*
     * True if the maximum has been reached
     */
    private boolean maxReached;
    
    public SizeLimitedInputStream(InputStream in, long maxBytes, boolean throwException)
    {
        super(in);
        
        this.maxBytes = maxBytes;
        this.throwException = throwException;
    }

    public SizeLimitedInputStream(InputStream in, long maxBytes) {
        this(in, maxBytes, true);
    }

    private void checkSize() 
    throws IOException
    {
        if (maxBytes > 0  && getByteCount() > maxBytes)
        {
            maxReached = true;
            
            if (throwException) {
                throw new LimitReachedException(MAXIMUM_REACHED);
            }
        }
    }
    
    @Override
    public int read(byte b[])
    throws IOException
    {
        if (maxReached) {
            return -1;
        }
        
        int r = super.read(b);
    
        checkSize();
        
        return r; 
    }

    @Override
    public int read(byte b[], int off, int len)
    throws IOException
    {
        if (maxReached) {
            return -1;
        }
        
        int r = super.read(b, off, len);

        checkSize();
        
        return r; 
    }

    @Override
    public int read()
    throws IOException
    {
        if (maxReached) {
            return -1;
        }
        
        int r = super.read();

        checkSize();
        
        return r; 
    }

    @Override
    public long skip(long length)
    throws IOException
    {
        if (maxReached) {
            return 0;
        }

        long r = super.skip(length);

        checkSize();
        
        return r; 
    }    
    
    @Override
    public int available()
    throws IOException
    {
        if (maxReached) {
            return 0;
        }

        return super.available();
    }
}
