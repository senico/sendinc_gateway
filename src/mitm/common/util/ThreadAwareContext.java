/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.HashMap;
import java.util.Map;

/**
 * ThreadAwareContext is a helper class which can be used to store data only accessible from the thread that 
 * added it. This can be useful when you have a 'complex' class with multiple methods. Normally you would 
 * create a state object and every method would accept the state object. This becomes cumbersome when you 
 * have a lot of methods and you can use this helper instead. Callers have to make sure that at the end of 
 * the 'activation context' clear gets called to make sure that there are no memory leaks. 
 * 
 * @author Martijn Brinkers
 *
 */
public class ThreadAwareContext implements Context
{
	private ThreadLocal<Map<String, Object>> context = new ThreadLocal<Map<String, Object>>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key, Class<T> type)
	{
		Check.notNull(type, "type");
		
		Map<String, Object> map = context.get();
		
		if (map == null) {
			return null;
		}
		
		Object value = map.get(key);
		
		if (value != null && !type.isAssignableFrom(value.getClass())) {
			throw new IllegalArgumentException("The object retrieved is not a " + type.getCanonicalName());
		}
		
		return (T) value;
	}
	
    @Override
	public <T> void set(String key, T instance)
	{
		Map<String, Object> map = context.get();
		
		if (map == null) 
		{
			map = new HashMap<String, Object>();
			
			context.set(map);
		}
		
		map.put(key, instance);
	}
	
	public void clear() {
		context.set(null);
	}
}
