/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.Writer;

/**
 * A writer that delegates to an underlying writer and checks whether the writer is closed. This can be 
 * used when a writer does not support close, yet the caller wants to close the writer.
 * 
 * @author Martijn Brinkers
 * 
 */
public class CheckCloseWriter extends Writer
{
    /*
     * The Writer to delegate to
     */
    private final Writer delegate;
    
    /*
     * Set to true if the stream is closed.
     */
    private boolean closed;
    
    public CheckCloseWriter(Writer delegate)
    {
        Check.notNull(delegate, "delegate");
        
        this.delegate = delegate;
    }

    private void checkClosed()
    throws IOException
    {
        if (closed) {
            throw new IOException("The Writer is closed.");
        }
    }
    
    @Override
    public void write(int c)
    throws IOException
    {
        checkClosed();
        
        delegate.write(c);
    }

    @Override
    public void write(char cbuf[])
    throws IOException
    {
        checkClosed();
        
        delegate.write(cbuf);
    }

    @Override
    public void write(char ac[], int i, int j)
    throws IOException
    {
        checkClosed();
        
        delegate.write(ac, i, j);
    }

    @Override
    public void write(String str)
    throws IOException
    {
        checkClosed();
        
        delegate.write(str);
    }

    @Override
    public void write(String str, int off, int len)
    throws IOException
    {
        checkClosed();
        
        delegate.write(str, off, len);
    }

    @Override
    public Writer append(CharSequence csq)
    throws IOException
    {
        checkClosed();
        
        delegate.append(csq);
        
        return this;
    }

    @Override
    public Writer append(CharSequence csq, int start, int end)
    throws IOException
    {
        checkClosed();
        
        delegate.append(csq, start, end);
        
        return this;
    }

    @Override
    public Writer append(char c)
    throws IOException
    {
        checkClosed();
        
        delegate.append(c);

        return this;
    }

    @Override
    public void flush()
    throws IOException
    {
        checkClosed();
        
        delegate.flush();
    }

    @Override
    public void close()
    throws IOException
    {
        closed = true;
        
        delegate.close();
    }
    
    @Override
    public String toString() {
        return delegate.toString();
    }
}
