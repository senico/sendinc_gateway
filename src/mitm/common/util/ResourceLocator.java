/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.commons.discovery.Resource;
import org.apache.commons.discovery.resource.ClassLoaders;
import org.apache.commons.discovery.tools.ResourceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for loading resources. Resources can be loaded from file or from the classpath
 * using the classloader.
 *
 * @author martijn
 *
 */
public class ResourceLocator
{
    private static final Logger logger = LoggerFactory.getLogger(ResourceLocator.class);

    private String systemProperty;
    private String baseDir;

    /**
     * Sets the system property which will be used (if set) to tell the search
     * where the properties file is residing.
     *
     * @param systemProperty
     */
    public void setSystemProperty(String systemProperty) {
        this.systemProperty = systemProperty;
    }

    /**
     * Sets the base directory to be used for looking up the properties file.
     */
    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    /**
     * tries to load the resource by name. Returns null if the resource cannot be found.
     */
    @SuppressWarnings("resource")
    public InputStream getResourceAsStream(String resourceName)
    throws FileNotFoundException
    {
        InputStream input = null;

        File file = null;

        if (systemProperty != null)
        {
            String base = System.getProperty(systemProperty);

            if (base != null) {
                file = new File(base, resourceName);
            }
        }

        if (file == null || !file.exists()) {
            file = new File(baseDir, resourceName);
        }

        if (file.exists())
        {
            logger.debug("Resource found at: " + file);

            input = new FileInputStream(file);
        }
        else {
            ClassLoaders classLoaders = ClassLoaders.getAppLoaders(null, null, true);

            Resource resource = ResourceUtils.getResource(null, resourceName, classLoaders);

            if (resource != null)
            {
                logger.debug("Resource found at: " + resource.getResource());

                input = resource.getResourceAsStream();
            }
        }

        return input;
    }
}
