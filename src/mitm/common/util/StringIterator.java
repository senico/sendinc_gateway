/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Is used to extract strings from an input stream. 
 * 
 * Note: This is strictly speaking not a Java Iterator because it does not implement Iterator. The problem with
 * Iterator is that the methods cannot throw any exceptions.
 * 
 * This class is not thread safe.
 * 
 * @author Martijn Brinkers
 *
 */
public class StringIterator
{
    /*
     * The maximum size of the initial buffer size
     */
    private final static int DEFAULT_MAX_INIT_BUFFER_SIZE = SizeUtils.KB * 64;
    
    /*
     * The reader used to read the strings from
     */
    private final Reader reader;
    
    /*
     * The maximum size of the string that will be returned. If the total string length is larger than length
     * the total string will be split into multiple strings. 
     */
    private final int length;
    
    /*
     * The maximum initial buffer size
     */
    private final int maxInitBufferSize;
    
    /**
     * Reads strings from the input, the returned strings are max length size and the string is considered to 
     * be encoded with the specified encoding.
     */
    public StringIterator(InputStream input, int length, String encoding, int maxInitBufferSize)
    throws IOException
    {
        Check.notNull(input, "input");
        Check.notNull(encoding, "encoding");
        
        if (length <= 0) {
            throw new IllegalArgumentException("Length should be > 0");
        }

        if (maxInitBufferSize <= 0) {
            throw new IllegalArgumentException("maxInitBufferSize should be > 0");
        }
        
        this.length = length;
        this.maxInitBufferSize = maxInitBufferSize;
        
        reader = new BufferedReader(new InputStreamReader(input, encoding));
    }

    public StringIterator(InputStream input, int length, String encoding)
    throws IOException
    {
        this(input, length, encoding, DEFAULT_MAX_INIT_BUFFER_SIZE);
    }
    
    /**
     * Returns the next available string. If no new string is available, null will be returned.
     */
    public String getNext()
    throws IOException
    {
        int bufferSize = Math.min(length, maxInitBufferSize);
        
        StringBuilder sb = new StringBuilder(bufferSize);
        
        boolean done = false;
        
        do {
            int c = reader.read();
            
            if (c != -1) {
                sb.appendCodePoint(c);
            }
            else {
                done = true;
            }
            
            if (sb.length() >= length) {
                done = true;
            }
        }
        while(!done);
                
        return sb.length() > 0 ? sb.toString() : null; 
    }
}
