/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Some general purpose regular expression utilities
 * 
 * @author Martijn Brinkers
 *
 */
public class RegExprUtils 
{
    /*
     * Pattern used for splitting up the input into a reg expr and value (see splitRegExp)
     */
    private static final Pattern SPLIT_REG_EXP_PATTERN = Pattern.compile("\\s*/([^/]*)/(?:\\s(.*))?"); 
    
	/**
	 * Escapes all regular expression special chars from the input. 
	 */
	public static String escape(String input) {
	    return Pattern.quote(input);
	}
	
	/**
	 * Splits the input into the reg expression and value. The input should be provided as
	 * 
	 *  /REG-EXP/ VALUE
	 * 
	 * Returns null if input is null or if input does not match the above pattern
	 * 
	 * Example:
	 * input: /^test$/ abc def -> [0]=^test$ [1]=abc def 
	 */
	public static String[] splitRegExp(String input)
	{
	    if (StringUtils.isEmpty(input)) {
	        return null;
	    }
	    
	    Matcher matcher = SPLIT_REG_EXP_PATTERN.matcher(input);
	    
	    String[] result = null;
	    
	    if (matcher.matches()) {
	        result = new String[]{matcher.group(1), matcher.group(2)};
	    }
	    
	    return result;
	}
}
