/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * A writer that delegates to an underlying writer and limits the number of characters that can be written to the
 * underlying writer. 
 * 
 * Note: This writer will not split up the data to write. This means that if data is about to be written that when 
 * written will exceed the limit, no data will be written.
 * 
 * @author Martijn Brinkers
 * 
 */
public class SizeLimitedWriter extends Writer
{
    /*
     * The Writer to delegate to
     */
    private final Writer delegate;
    
    /*
     * the maximum number of chars that can be written.
     */
    private final long limit;
    
    /*
     * If true, an exception will be thrown when the limit is reached. If false writing new data will be silently
     * discarded when the limit is reached. 
     */
    private final boolean exceptionOnLimitReached;
    
    /*
     * The nr of chars written
     */
    private long count;
    
    public SizeLimitedWriter(Writer delegate, long limit, boolean exceptionOnLimitReached)
    {
        Check.notNull(delegate, "delegate");

        if (limit < 0) {
            limit = 0;
        }
        
        this.delegate = delegate;
        this.limit = limit;
        this.exceptionOnLimitReached = exceptionOnLimitReached;
    }

    /*
     * Checks if the limit will be reached when writing toWrite nr of characters. 
     */
    private boolean isLimitReached(int toWrite)
    throws IOException
    {
        if (toWrite < 0) {
            toWrite = 0;
        }
        
        count = count + toWrite;
        
        if (count > limit) 
        {
            if (exceptionOnLimitReached) {
                throw new LimitReachedException("The limit is reached.");
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public void write(int c)
    throws IOException
    {
        if (!isLimitReached(1)) {
            delegate.write(c);
        }
    }

    @Override
    public void write(char cbuf[])
    throws IOException
    {
        if (!isLimitReached(ArrayUtils.getLength(cbuf))) {
            delegate.write(cbuf);
        }
    }

    @Override
    public void write(char ac[], int off, int len)
    throws IOException
    {
        if (!isLimitReached(len)) {
            delegate.write(ac, off, len);
        }
    }

    @Override
    public void write(String str)
    throws IOException
    {
        if (!isLimitReached(StringUtils.length(str))) {
            delegate.write(str);
        }
    }

    @Override
    public void write(String str, int off, int len)
    throws IOException
    {
        if (!isLimitReached(len)) {
            delegate.write(str, off, len);
        }
    }

    @Override
    public Writer append(CharSequence csq)
    throws IOException
    {
        if (!isLimitReached(csq != null ? csq.length() : 0)) {
            delegate.append(csq);
        }
        
        return this;
    }

    @Override
    public Writer append(CharSequence csq, int start, int end)
    throws IOException
    {
        if (!isLimitReached(end - start)) {
            delegate.append(csq, start, end);
        }
        
        return this;
    }

    @Override
    public Writer append(char c)
    throws IOException
    {
        if (!isLimitReached(1)) {
            delegate.append(c);
        }

        return this;
    }

    @Override
    public void flush()
    throws IOException
    {
        delegate.flush();
    }

    @Override
    public void close()
    throws IOException
    {
        delegate.close();
    }
    
    @Override
    public String toString() {
        return delegate.toString();
    }

    public long getCount() {
        return count;
    }
}
