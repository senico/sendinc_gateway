/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * a reader that adds CR/LF pairs when the with exceeds the specified width.
 * 
 * This reader is not thread safe.
 * 
 * @author Martijn Brinkers
 *
 */
public class FixedWidthReader extends BufferedReader 
{
    private static final int BUFF_SIZE     = 1024;

    private static final int EOF           = -1;
    private static final int CR            = 13;
    private static final int LF            = 10;
    
    /* default width (see RFC2822) */
    private static final int DEFAULT_WIDTH = 78;
    
    private final StringBuffer buffer = new StringBuffer(BUFF_SIZE);

    private int width = DEFAULT_WIDTH;
    
    public FixedWidthReader(Reader reader, int width)
    {
        super(reader);
        
        this.width = width; 
    }

    public FixedWidthReader(Reader reader)
    {
        this(reader, DEFAULT_WIDTH);
    }
    
    @Override
    public String readLine() 
    throws IOException
    {
        buffer.delete(0, buffer.length());

        int inChar = EOF;
        
        while (buffer.length() < width) 
        {
            inChar = read();

            /* We want a fixed width so we must skip CR LF */
            if (inChar == CR || inChar == LF) {
                continue;
            }

            if (inChar == EOF) {
                break;
            }
            
            buffer.append((char)inChar);
        }
        
        String line = buffer.toString();

        /*
         * we want to return null when we reached EOF and
         * there were no characters read
         */
        if (inChar == EOF && line.length() == 0) 
        {
            line = null;
        }
        
        return line;
    }

    /**
     * Returns the input string but with CR/LF pairs at the column specified by width
     * @param input
     * @param width
     * @return
     * @throws IOException
     */
    public static String fixedWidth(final String input, int width) 
    throws IOException 
    {
        if (input == null) {
            return null;
        }
        
        Reader stringReader = new StringReader(input);
        
        FixedWidthReader reader = new FixedWidthReader(stringReader, width);
        
        String result = "";
        String line = null;
        
        do {
            line = reader.readLine();

            if (line != null) 
            {
                if (result.length() > 0) {
                    result = result + "\r\n";
                }

                result = result + line;
            }
        } 
        while (line != null);
        
        return result;
    }
    
    public static String fixedWidth(final String input) 
    throws IOException 
    {
        return fixedWidth(input, DEFAULT_WIDTH);
    }
}
