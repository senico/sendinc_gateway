/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.Enumeration;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.varia.NullAppender;

/**
 * Utility methods for logging.
 * 
 * @author Martijn Brinkers
 *
 */
public class LogUtils
{
    /**
     * Disables all Log4J and JSE logging.
     */
    public static void disableLogging()
    {
        /*
         *  Disable Log4J logging.
         */
        BasicConfigurator.configure(new NullAppender());
        
        /*
         * Disable JSE logging
         */
        java.util.logging.LogManager logManager = java.util.logging.LogManager.getLogManager();
        
        Enumeration<String> loggerEnum = logManager.getLoggerNames();
        
        while (loggerEnum.hasMoreElements()) 
        {
            String loggerName = loggerEnum.nextElement();
            
            java.util.logging.Logger.getLogger(loggerName).setLevel(java.util.logging.Level.OFF);
        }
    }
    
    /**
     * Logs on error level. Only logs stacktrace if debug is enabled
     */
    public static void logErrorStackTraceOnDebug(org.slf4j.Logger logger, String message, Throwable t)
    {
        if (logger.isDebugEnabled()) {
            logger.error(message, t);
        }
        else {
            logger.error(message + ", Message: " + ExceptionUtils.getRootCauseMessage(t));
        }
    }

    /**
     * Logs on warn level. Only logs stacktrace if debug is enabled
     */
    public static void logWarnStackTraceOnDebug(org.slf4j.Logger logger, String message, Throwable t)
    {
        if (logger.isDebugEnabled()) {
            logger.warn(message, t);
        }
        else {
            logger.warn(message + ", Message: " + ExceptionUtils.getRootCauseMessage(t));
        }
    }
    
    /**
     * Returns the loglevel of the SLF4J logger
     */
    public static LogLevel getLogLevel(org.slf4j.Logger logger)
    {
        Check.notNull(logger, "logger");
        
        if (logger.isTraceEnabled()) {
            return LogLevel.TRACE;
        }
        else if (logger.isDebugEnabled()) {
            return LogLevel.DEBUG;
        }
        else if (logger.isInfoEnabled()) {
            return LogLevel.INFO;
        }
        else if (logger.isWarnEnabled()) {
            return LogLevel.WARN;
        }
        else if (logger.isErrorEnabled()) {
            return LogLevel.ERROR;
        }
        
        return LogLevel.OFF;
    }
    
    /**
     * Returns the log level from a Log4j log level
     */
    public static LogLevel toLogLevel(org.apache.log4j.Level level)
    {
        if (org.apache.log4j.Level.INFO.equals(level)) {
            return LogLevel.INFO;
        }
        else if (org.apache.log4j.Level.WARN.equals(level)) {
            return LogLevel.WARN;
        }
        else if (org.apache.log4j.Level.ERROR.equals(level)) {
            return LogLevel.ERROR;
        }
        else if (org.apache.log4j.Level.DEBUG.equals(level)) {
            return LogLevel.DEBUG;
        }
        else if (org.apache.log4j.Level.TRACE.equals(level)) {
            return LogLevel.TRACE;
        }
        else if (org.apache.log4j.Level.OFF.equals(level)) {
            return LogLevel.OFF;
        }
        
        return LogLevel.INFO;
    }    
    
    /**
     * Retruns the Log4J level from a Log Level
     */
    public static org.apache.log4j.Level toLog4JLevel(LogLevel logLevel)
    {
        switch(logLevel)
        {
        case INFO  : return org.apache.log4j.Level.INFO; 
        case WARN  : return org.apache.log4j.Level.WARN;
        case ERROR : return org.apache.log4j.Level.ERROR;
        case DEBUG : return org.apache.log4j.Level.DEBUG;
        case TRACE : return org.apache.log4j.Level.TRACE;
        case OFF   : return org.apache.log4j.Level.OFF;
        }
        
        return org.apache.log4j.Level.INFO;
    }    
}
