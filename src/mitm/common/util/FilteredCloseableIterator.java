/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilteredCloseableIterator<T> implements CloseableIterator<T>
{
    private final static Logger logger = LoggerFactory.getLogger(FilteredCloseableIterator.class); 

    private final CloseableIterator<T> sourceIterator;
    private final MatchEvent<T> matchEvent;
    
    private T nextElement;

    public static interface MatchEvent<T> {
        public boolean hasMatch(T element);
    }
    
    public FilteredCloseableIterator(CloseableIterator<T> sourceIterator, MatchEvent<T> matchEvent)
    {
    	Check.notNull(sourceIterator, "sourceIterator");
    	
        this.sourceIterator = sourceIterator;
        this.matchEvent = matchEvent;
    }

    public FilteredCloseableIterator(CloseableIterator<T> sourceIterator) 
    {
        this(sourceIterator, null);
    }
    
    protected boolean hasMatch(T element)
    {
        boolean match = true;
        
        if (matchEvent != null) {
            match = matchEvent.hasMatch(element);
        }
        
        return match;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public boolean hasNext()
    throws CloseableIteratorException
    {
        if (isClosed()) {
            throw new CloseableIteratorException("The source iterator is closed.");
        }

        if (nextElement != null) {
            return true;
        }
        
        T newElement = null;

        boolean hasNext = false;
        
        while(sourceIterator.hasNext())
        {
            Object object = sourceIterator.next();
            
            newElement = (T) object;
            
            if (!hasMatch(newElement)) {
                continue;
            }
            
            hasNext = true;
            break;
        }
        
        if (hasNext) {
            nextElement = newElement;
        }
        else {
            /* close if we have arrived at the last entry */
            close();
        }

        return hasNext;        
    }

    @Override
    public T next()
    throws CloseableIteratorException
    {
        if (isClosed()) {
            throw new CloseableIteratorException("The source iterator is closed.");
        }

        if (nextElement == null)
        {
            if (!hasNext()) {
                throw new CloseableIteratorException("There is not next entry.");
            }
        }
        
        T copy = nextElement;
        
        nextElement = null;
        
        return copy;
    }
    
    @Override
    public void close()
    throws CloseableIteratorException
    {
        logger.debug("Closing source iterator.");

        if (!sourceIterator.isClosed()) {
            sourceIterator.close();
        }        
    }
    
    @Override
    public boolean isClosed()
    throws CloseableIteratorException
    {
        return sourceIterator.isClosed();
    }    
}
