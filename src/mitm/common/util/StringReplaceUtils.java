/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class StringReplaceUtils
{
    private final static Pattern REPLACE_NON_XML_PATTERN = Pattern.compile("[^\\x09\\x0A\\x0D\\u0020-\\uD7FF]");

    private final static Pattern REPLACE_NON_ASCII_PATTERN = Pattern.compile("[^\\p{ASCII}]");
    
    /**
     * Returns the input string with all tabs replaced by tabWidth spaces.
     */
    public static String replaceTabsWithSpaces(String input, int tabWidth) {
        return input.replaceAll("\t", StringUtils.rightPad("", tabWidth));
    }
    
    /**
     * Replaces all characters from the input string that are not valid for XML with replaceWith
     * 
     * (why we need this? see: https://issues.apache.org/jira/browse/CXF-1771 and
     * http://dssheep.blogspot.com/2008/01/xml-10-versus-web-services.html)
     */
    public static String replaceNonXML(String input, String replaceWith)
    {
        Check.notNull(replaceWith, "replaceWith");
        
        if (input == null) {
            return null;
        }
        
        /*
         * See http://www.w3.org/TR/2000/REC-xml-20001006#charsets
         */
        return REPLACE_NON_XML_PATTERN.matcher(input).replaceAll(replaceWith);
    }

    /**
     * For each String in the list the non-XML characters are replaced. The returned list is the same instance as input
     * (it's an in place replacement)
     */
    public static List<String> replaceNonXML(List<String> input, String replaceWith)
    {
        Check.notNull(replaceWith, "replaceWith");

        if (input == null) {
            return null;
        }
        
        ListIterator<String> listIterator = input.listIterator();

        while (listIterator.hasNext()) 
        {
            String line = listIterator.next();

            listIterator.set(StringReplaceUtils.replaceNonXML(line, "#"));
        }
        
        return input;
    }   

    /**
     * For each String in the list the non-XML characters are replaced. The returned set is a new instance
     * of LinkedHashSet
     */
    public static Set<String> replaceNonXML(Set<String> input, String replaceWith)
    {
        Check.notNull(replaceWith, "replaceWith");

        if (input == null) {
            return null;
        }
        
        Set<String> result = new LinkedHashSet<String>();

        for (String s : input)
        {
            s = StringReplaceUtils.replaceNonXML(s, "#");
            
            result.add(s);
        }
        
        return result;
    }   
    
    /**
     * Replaces all non ASCII characters from the input string with replaceWith
     */
    public static String replaceNonASCII(String input, String replaceWith)
    {
        Check.notNull(replaceWith, "replaceWith");

        if (input == null) {
            return null;
        }
        
        return REPLACE_NON_ASCII_PATTERN.matcher(input).replaceAll(replaceWith);
    }
}
