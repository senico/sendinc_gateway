package mitm.common.util;

import org.apache.commons.lang.CharSet;

/**
 * Filename utility methods not found in Apache commons FilenameUtils
 * 
 * @author Martijn Brinkers
 *
 */
public class MiscFilenameUtils
{
    /*
     * The valid chars in a filename
     */
    private final static String FILENAME_VALID_CHARS = "-[ 0-9a-zA-Z_.]";

    /*
     * The set with valid chars in a filename
     */
    private final static CharSet filenameCharSet = CharSet.getInstance(FILENAME_VALID_CHARS);

    
    /**
     * Removes all characters that are not [0-9a-zA-Z_-.] 
     */
    public static String removeIllegalFilenameChars(String filename)
    {
        if (filename == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < filename.length(); i++)
        {
            char c = filename.charAt(i);
            
            if (!filenameCharSet.contains(c)) {
                continue;
            }
            
            sb.append(c);
        }
        
        return sb.toString();
    }

    /**
     * Replaces all characters that are not [0-9a-zA-Z_-.] with replace char 
     */
    public static String replaceIllegalFilenameChars(String filename, char replaceChar)
    {
        if (filename == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < filename.length(); i++)
        {
            char c = filename.charAt(i);
            
            if (!filenameCharSet.contains(c)) {
                c = replaceChar;
            }
            
            sb.append(c);
        }
        
        return sb.toString();
    }
}
