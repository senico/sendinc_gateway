/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * KeyedBarrier allows a callable to be executed only if there is not already another callable using the 
 * same key is executing. The KeyedBarrier can be used to serialize access to a long term method based on 
 * a key. 
 * 
 * @author Martijn Brinkers
 *
 * @param <T> Key type
 * @param <U> callable return type
 */
public class KeyedBarrier<T, U>
{
    private final static Logger logger = LoggerFactory.getLogger(KeyedBarrier.class);
    
    /*
     * The max wait time (in msec) before trying again
     */
    private final long waitTime;

    /*
     * ALl the keys 
     */
    private Set<T> keys = new HashSet<T>();
    
    @SuppressWarnings("serial")
    public static class KeyedBarrierTimeoutException extends Exception
    {
        private KeyedBarrierTimeoutException(String message) {
            super(message);
        }
    }
    
    public KeyedBarrier(long waitTime) {
        this.waitTime = waitTime;
    }

    public KeyedBarrier() {
        this(DateUtils.MILLIS_PER_SECOND * 10);
    }

    public U execute(T key, Callable<U> callable, long timeout)
    throws Exception
    {
        Check.notNull(key, "key");
        Check.notNull(callable, "runnable");
        
        boolean added = false;
        
        try {            
            long started = System.currentTimeMillis();
            
            synchronized (keys)
            {
                boolean allowed = false;
                
                do {
                    if (keys.contains(key))
                    {
                        try {
                            logger.debug("Waiting for key {} to be removed", key);

                            keys.wait(waitTime);
                        } 
                        catch (InterruptedException e) {
                            /*
                             * ignore
                             */
                        }
                        
                        if (timeout > 0 && (System.currentTimeMillis() - started) > timeout) {
                            throw new KeyedBarrierTimeoutException("Time exceeded the timeout value " + timeout);
                        }
                    }
                    else {
                        allowed = true;
                    }
                }
                while (!allowed);
                
                keys.add(key);
                
                added = true;
            }
            
            /*
             * Execute the runnable since we now know that only one runnable is executing 
             * with this key.
             */
            return callable.call();
        }
        finally
        {
            /*
             * After executing the Runnable, we need to notify the waiting threads that they can try
             * again.
             */
            synchronized (keys)
            {
                /*
                 * Only remove the key if it was added. It can happen that added is false when a 
                 * KeyedBarrierTimeoutException was thrown and we do not want to remove the key 
                 * if it was added by a different thread
                 */
                if (added) {
                    keys.remove(key);
                }
                
                keys.notify();
            }
        }        
    }
}
