/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

/**
 * Iterator which can be used to retrieve name value pairs (name=value). Lines that do not match a line value
 * pair are skipped.
 * 
 * @author martijn
 *
 */
public class NameValueLineIterator implements Iterator<NameValueLineIterator.Entry>
{
    private final LineIterator lineIterator;

    /* the regular expression used to detect name value pairs */
    private final static Pattern nameValuePattern = Pattern.compile("^\\s*(.*?)\\s*=\\s*(.*?)\\s*$");
        
    private Entry cachedEntry;
    
    public static class Entry 
    {
        private Entry(String name, String value) 
        {
            this.name = name;
            this.value = value;
        }
        
        private String name;
        private String value;
        
        public String getName() {
            return name;
        }
        
        public String getValue() {
            return value;
        }
    }

    public NameValueLineIterator(InputStream input, String charset) 
    throws IOException
    {
        lineIterator = IOUtils.lineIterator(input, charset);
    }

    public NameValueLineIterator(InputStream input) 
    throws IOException
    {
        this(input, "US-ASCII"); 
    }

    public NameValueLineIterator(Reader reader) 
    throws IOException
    {
        lineIterator = IOUtils.lineIterator(reader);
    }
    
    @Override
    public boolean hasNext() 
    {
        if (cachedEntry != null) {
            return true;
        }
        
        while (lineIterator.hasNext()) 
        {
            String line = lineIterator.nextLine();
            
            if (line != null)
            {
                Matcher matcher = nameValuePattern.matcher(line);
                
                if (matcher.matches()) 
                {
                    String name = matcher.group(1);
                    String value = matcher.group(2);
                    
                    cachedEntry = new Entry(name, value);
                    
                    return true;
                }
            }
        }
        
        return false;
    }

    @Override
    public Entry next()
    {
        if (!hasNext()) {
            throw new NoSuchElementException("No more entries");
        }
        else {
            Entry entry = cachedEntry;
            
            cachedEntry = null;
            
            return entry;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is unsupported.");
    }
}
