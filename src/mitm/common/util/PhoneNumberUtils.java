/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Phone number related helpers like validation of phone numbers etc.
 * 
 * @author Martijn Brinkers
 *
 */
public class PhoneNumberUtils
{
    private final static Logger logger = LoggerFactory.getLogger(PhoneNumberUtils.class);
    
    private final static Pattern PHONE_NUMBER_PATTERN = Pattern.compile("(?:\\s*\\+?\\s*)(\\d{4,25})(?:\\s*)");
    
    /**
     * Checks if the input can be a valid phone number. The phone number must have a country code (country
     * codes are not checked) and must not start with a 0.
     * If input cannot be a valid phone number of input is null null is returned otherwise the phone number
     * is returned (with start and end spaces removed).
     */
    public static String filterAndValidatePhoneNumber(String input)
    {        
        if (input == null) {
            return null;
        }
     
        String phoneNumber = null;
        
        Matcher matcher = PHONE_NUMBER_PATTERN.matcher(input);
        
        if (matcher.matches()) 
        {
            phoneNumber = matcher.group(1);
            
            if (phoneNumber.startsWith("0")) 
            {
                logger.warn("Phone number starts with 0 which is not a valid country code.");
                
                phoneNumber = null;
            }
        }
        
        return phoneNumber;
    }
    
    /**
     * Adds the country code if phone number starts with 0. Returns null if phone
     * number is null. 
     */
    public static String addCountryCode(String phoneNumber, String countryCode)
    {
        if (phoneNumber == null) {
            return null;
        }
        
        phoneNumber = phoneNumber.trim();
        
        if (StringUtils.isNotBlank(countryCode) && phoneNumber.startsWith("0")) {
            phoneNumber = StringUtils.defaultString(countryCode) + phoneNumber.substring(1);
        }
        
        return phoneNumber;
    }
}
