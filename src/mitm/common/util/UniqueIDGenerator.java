/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import mitm.common.security.crypto.RandomGenerator;

import org.apache.commons.lang.StringUtils;

/**
 * IDGenerator implementation that creates a unique ID.
 * 
 * @author Martijn Brinkers
 *
 */
public class UniqueIDGenerator implements IDGenerator
{
    /*
     * The number of random bytes to generate
     */
    private final int randomBytes;

    /*
     * The base ID  
     */
    private final String baseID;
    
    /*
     * The random generator used for generate random numbers
     */
    private RandomGenerator randomGenerator;
    
    /*
     * Will increase with 1 for every new ID
     */
    private long counter;
        
    /**
     * Creates an instance of SecureUniqueIDCreator. The initial base value of the ID will be based on the 
     * current time in milliseconds. It's therefore advised to make sure that the system clock is in sync. The 
     * RandomGenerator should be a secure random generator and the number of random bytes should be long enough to
     * make guessing impossible.
     * 
     * @param randomGenerator the generator for generating random nr's (make sure a secure random generator is used)
     * @param randomBytes the number of random bytes to generate
     * @param baseID the base ID. This can be helpful to distiguish multiple SecureUniqueIDCreator's instances. 
     */
    public UniqueIDGenerator(RandomGenerator randomGenerator, int randomBytes, String baseID)
    {
        Check.notNull(randomGenerator, "randomGenerator");
        
        this.randomGenerator = randomGenerator;
        this.randomBytes = randomBytes;
        this.baseID = StringUtils.defaultString(baseID) + Long.toString(System.currentTimeMillis());
    }

    /**
     * Creates an instance of SecureUniqueIDCreator. The initial base value of the ID will be based on the 
     * current time in milliseconds. It's therefore advised to make sure that the system clock is in sync. The 
     * RandomGenerator should be a secure random generator and the number of random bytes should be long enough to
     * make guessing impossible.
     * 
     * @param randomGenerator the generator for generating random nr's (make sure a secure random generator is used)
     * @param randomBytes the number of random bytes to generate
     */
    public UniqueIDGenerator(RandomGenerator randomGenerator, int randomBytes)
    {
        this(randomGenerator, randomBytes, null);
    }
    
    /**
     * Creates a new ID. The ID will be based on the current time, an increasing counter and on randomly generated
     * bytes. The random bytes will be base32 encoded.
     */
    @Override
    public synchronized String createID() {
        return baseID + Long.toString(counter++) + Base32.encode(randomGenerator.generateRandom(randomBytes));
    }
}
