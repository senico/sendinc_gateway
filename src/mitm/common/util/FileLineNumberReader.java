/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Reader implementation that can be used to get the number of lines in files and get a 'page'
 * of the file. The files will be treated as being just one big concatenated file. This is useful
 * when reading log files that are being log-rotated.
 * 
 * Note: FileLineNumberReader requires access to the actual File because it need to be able to re-read
 * the file because we need random access. 
 * 
 * This implementation uses {@link java.io.LineNumberReader} to read the lines. 
 * 
 * @author Martijn Brinkers
 *
 */
public class FileLineNumberReader extends Reader 
{
	private final Collection<File> files;
	
	private final LineNumberReader lineNumberReader; 

	public FileLineNumberReader(File file)
	throws FileNotFoundException
	{
		this(Collections.singleton(file));
	}
	
	public FileLineNumberReader(Collection<File> files)
	throws FileNotFoundException
	{
		Check.notNull(files, "files");
		
		if (files.size() == 0) {
			throw new IllegalArgumentException("files need to contain at least one File.");
		}
		
		this.files = files;
		this.lineNumberReader = createLineNumberReader();
	}

	private LineNumberReader createLineNumberReader() 
	throws FileNotFoundException 
	{
		Collection<Reader> readers = new ArrayList<Reader>(files.size());
		
		for (File file : files) 
		{
			Check.notNull(file, "file");
			
			readers.add(new FileReader(file));
		}
				
		MetaReader metaReader = new MetaReader(readers);
		
		return new LineNumberReader(metaReader);
	}
	
	@Override
	public void close() 
	throws IOException 
	{
		lineNumberReader.close();
	}

	@Override
	public int read(char[] cbuf, int off, int len) 
	throws IOException 
	{
		return lineNumberReader.read(cbuf, off, len);
	}
	
	/**
	 * Must be overridden to add actual filtering
	 */
	protected boolean isMatch(String line) 
	{
		return true;
	}
	
	/**
	 * returns the number of lines in the file.
	 * @return
	 * @throws IOException 
	 */
	public int getLineCount() 
	throws IOException 
	{
		LineNumberReader lineNumberReader = createLineNumberReader();

		try {
			int lineCount = 0;
			
			String line;
			
			while ((line = lineNumberReader.readLine()) != null)
			{
				if (isMatch(line)) {
					lineCount++;
				}
			}
	
			return lineCount;
		}
		finally {
			lineNumberReader.close();
		}
	}
	
	/**
	 * Returns a List of Strings from the file. If there are less items in the file than 
	 * nrOfItems the number of items returned is less than items.
	 */
	public List<String> readLines(int startIndex, int nrOfItems) 
	throws IOException
	{
		if (startIndex < 0) {
			throw new IllegalArgumentException("startIndex must be >= 0");
		}

		if (nrOfItems < 0) {
			throw new IllegalArgumentException("nrOfItems must be >= 0");
		}

		List<String> list = new LinkedList<String>();
		
		LineNumberReader lineNumberReader = createLineNumberReader();
	
		int currentLineNumber = lineNumberReader.getLineNumber();
		
		try {
			while (currentLineNumber != startIndex)
			{
				String line = lineNumberReader.readLine();
				
				if (line == null) {
					break;
				}
				
				if (isMatch(line)) {
					currentLineNumber++;
				}
			}
	
			while (currentLineNumber < startIndex + nrOfItems)
			{
				String line = lineNumberReader.readLine();
				
				if (line == null) {
					break;
				}
				
				if (isMatch(line))
				{
					list.add(line);
					
					currentLineNumber++;
				}
			}
		}
		finally {
			lineNumberReader.close();
		}
		
		return list;
	}
}
