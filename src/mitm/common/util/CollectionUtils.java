/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

import mitm.common.reflection.ReflectionUtils;

import org.apache.commons.lang.ObjectUtils;

public class CollectionUtils
{
    /**
     * Copies elements from the target collection to the source collection only when the elements implements all the 
     * clazz classes.
     * 
     * @param <T>
     * @param source
     * @param target
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> void copyCollectionFiltered(Collection source, Collection target, Class<?>... clazz) 
    {
    	Check.notNull(source, "source");
    	Check.notNull(target, "target");
    	Check.notNull(clazz, "clazz");
    	
        for (Object object : source)
        {
            if (ReflectionUtils.isInstanceOf(object.getClass(), clazz))
            {
                target.add((T) object);
            }
        }
    }
    
    /**
     * Converts the collection to an array of Strings by calling org.apache.commons.lang.ObjectUtils on each item. 
     * If items is null null will be returned. If an item is null the item will be "nullString".
     */
    public static String[] toStringArray(Collection<?> items, String nullString)
    {
        if (items == null) {
            return null;
        }
        
        String[] result = new String[items.size()];

        int i = 0;
        
        for (Object obj : items)
        {
            result[i] = ObjectUtils.toString(obj, nullString);
            
            i++;
        }
        
        return result;
    }
    
    /**
     * Converts the collection to a List of Strings by calling org.apache.commons.lang.ObjectUtils on each item. If 
     * items is null an empty List will be returned (i.e., a non-null List instance is always returned). If an item is 
     * null the item will be "nullString".
     * 
     * @param items
     * @return
     */
    public static ArrayList<String> toStringList(Collection<?> items, String nullString)
    {
        int size = items != null ? items.size() : 0;
        
        ArrayList<String> sl = new ArrayList<String>(size);
        
        if (items != null)
        {
            for (Object obj : items) {
                sl.add(ObjectUtils.toString(obj, nullString));
            }
        }
        
        return sl;
    }
    
    /**
     * Converts the collection to a Set of Strings by calling org.apache.commons.lang.ObjectUtils on each item of items. 
     * If items is null an empty Set will be returned (i.e., a non-null Set instance is always returned). If an item is 
     * null the item will be "nullString".
     * 
     * @param items
     * @return
     */
    public static LinkedHashSet<String> toStringSet(Collection<?> items, String nullString)
    {
        int size = items != null ? items.size() : 0;
        
        LinkedHashSet<String> set = new LinkedHashSet<String>(size);
        
        if (items != null)
        {
            for (Object obj : items) {
                set.add(ObjectUtils.toString(obj, nullString));
            }
        }
        
        return set;
    }
    
    /**
     * Returns the array as a set
     */
    public static <T> LinkedHashSet<T> asLinkedHashSet(T[] array)
    {
    	LinkedHashSet<T> set = new LinkedHashSet<T>();
    	
    	for (T obj : array) {
    		set.add(obj);
    	}
    	
    	return set;
    }
    
    
    /**
     * Returns the size of the collection. If the collection is null, 0 will be returned.
     */
    public static int getSize(Collection<?> collection)
    {
        return collection != null ? collection.size() : 0;
    }

    /**
     * Returns true if the the size of the collection is 0 or if the collection is null.
     */
    public static boolean isEmpty(Collection<?> collection) {
        return getSize(collection) == 0;
    }
    
    /**
     * Returns true if the the size of the collection is > 0.
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }
}
