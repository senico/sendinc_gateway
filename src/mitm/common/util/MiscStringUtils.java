/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.UnsupportedEncodingException;

import mitm.common.locale.CharacterEncoding;

import org.apache.commons.lang.StringUtils;

public class MiscStringUtils
{
    
    /**
     * returns true if the char c is not ascii
     */
    public static boolean isNotPrintableAscii(char c)
    {
        return c >= 127 || (c < 32 && c != '\r' && c != '\n' && c != '\t');
    }

    /**
     * returns true if the char c is ascii or CR/LF/TAB.
     */
    public static boolean isPrintableAscii(char c)
    {
        return !isNotPrintableAscii(c);
    }
    
    /**
     * returns true if all the characters in the string are ascii or CR/LF/TAB. 
     */
    public static boolean isPrintableAscii(String s)
    {
        if (StringUtils.isEmpty(s)) {
            return true;
        }
        
        for (int i = 0; i < s.length(); i++)
        {
            if (isNotPrintableAscii(s.charAt(i))) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Converts the input to US-ASCII
     */
    public static String toAscii(String input) 
    {
        try {
            byte[] b = toAsciiBytes(input);
            
            return b != null ? new String(b, CharacterEncoding.US_ASCII) : null;
        }
        catch (UnsupportedEncodingException e) {
            /* all JVMs must support US-ASCII */
            throw new IllegalStateException("ASCII support is missing.");
        }
    }
    
    /**
     * Converts the specified string to byte array of ASCII characters.
     *
     * @param data the string to be encoded
     * @return byte array containing the encoded ascii string.
     */
    public static byte[] toAsciiBytes(String data) 
    {
        if (data == null) {
            return null;
        }

        try {
            return data.getBytes(CharacterEncoding.US_ASCII);
        } 
        catch (UnsupportedEncodingException e) {
            /* all JVMs must support US-ASCII */
            throw new IllegalStateException("ASCII support is missing.");
        }
    }
    
    /**
     * Converts the byte array of ASCII characters to a string. 
     *
     * @param data the byte array to be encoded
     * @return The string containing ascii characters 
     */
    public static String toAsciiString(byte[] data) 
    {
        if (data == null) {
            return null;
        }

    	return toAsciiString(data, 0, data.length);
    }
    
    /**
     * Converts the byte array of ASCII characters to a string
     *
     * @param data the byte array to be encoded
     * @param offset the first byte to encode
     * @param length bytes to encode 
     * @return The string converted string
     */
    public static String toAsciiString(byte[] data, int offset, int length) 
    {

        if (data == null) {
            return null;
        }

        try {
            return new String(data, offset, length, CharacterEncoding.US_ASCII);
        } 
        catch (UnsupportedEncodingException e) {
            /* all JVMs must support US-ASCII */
            throw new IllegalStateException("ASCII support is missing.");
        }
    }

    /**
     * Converts the specified string to byte array of UTF-8 characters.
     *
     * @param data the string to be encoded
     * @return byte array containing the encoded UTF-8 string.
     */
    public static byte[] toUTF8Bytes(String data) 
    {
        if (data == null) {
            return null;
        }

        try {
            return data.getBytes(CharacterEncoding.UTF_8);
        } 
        catch (UnsupportedEncodingException e) {
            /* all JVMs must support UTF_8 */
            throw new IllegalStateException("UTF-8 support is missing.");
        }
    }
    
    /**
     * Converts the byte array with UTF-8 encoded string to a string. 
     *
     * @param data the byte array to be encoded
     * @return The string containing ascii characters 
     */
    public static String toUTF8String(byte[] data) 
    {
        if (data == null) {
            return null;
        }

        return toUTF8String(data, 0, data.length);
    }
    
    /**
     * Converts the byte array with UTF-8 encoded string to a string. 
     *
     * @param data the byte array to be encoded
     * @param offset the first byte to encode
     * @param length bytes to encode 
     * @return The string converted string
     */
    public static String toUTF8String(byte[] data, int offset, int length) 
    {

        if (data == null) {
            return null;
        }

        try {
            return new String(data, offset, length, CharacterEncoding.UTF_8);
        } 
        catch (UnsupportedEncodingException e) {
            /* all JVMs must support UTF-8 */
            throw new IllegalStateException("UTF-8 support is missing.");
        }
    }
    
    /**
     * Removes control characters from the input string
     */
    public static String removeControlChars(String input)
    {
        if (input == null) {
            return null;
        }
        
        char[] chars = new char[input.length()];
        
        int index = 0;
        
        for (int i = 0; i < input.length(); i++)
        {
            char c = input.charAt(i);
            
            if (c >= ' ')
            {
                chars[index] = c;
                
                index++;
            }
        }
        
        return new String(chars, 0, index);
    }
    
    /**
     * If the input string starts and ends with quoteChar the starting and ending
     * quoteChar will be removed. The input string is not trimmed so any leading 
     * or ending white spaces will result in the string not to be unquoted. 
     * @param input
     * @param quoteChar
     * @return
     */
    public static String unquote(String input, char quoteChar)
    {
        if (input == null) {
            return null;
        }
     
        /* if length < 2 the input cannot be quoted */
        if (input.length() < 2) {
            return input;
        }
        
        String unquoted = input;
        
        if (isQuoted(input, quoteChar)) {
            unquoted = input.substring(1, input.length() - 1);
        }
        
        return unquoted;
    }

    public static boolean isQuoted(String input, char quoteChar)
    {
        if (input == null) {
            return false;
        }
     
        /* if length < 2 the input cannot be quoted */
        if (input.length() < 2) {
            return false;
        }

        return input.charAt(0) == quoteChar && input.charAt(input.length() - 1) == quoteChar;
    }
    
    /** 
     * Restricts the length of the input string to a maximum length.
     */
    public static String restrictLength(String input, int maxLength) 
    {
        return restrictLength(input, maxLength, false);
    }
    
    /**
     * Restricts the length of the input string to a maximum length. If addDots is true ... will be added if the string
     * is too long. The final length will always be smaller or equal to maxLength (ie the dots do not make it any longer)
     * There is one exception when the size < 3 and addDots is true.
     */
    public static String restrictLength(String input, int maxLength, boolean addDots, String dots) 
    {
        if (input == null) {
            return null;
        }
                
        if (maxLength < 0) {
            maxLength = 0;
        }
                
        if (input.length() > maxLength) 
        {
            /*
             * reduce length if we add the dots
             */
            if (addDots) 
            {
                maxLength = maxLength - 3;
                
                if (maxLength < 0) {
                    maxLength = 0;
                }
            }
            
            input = input.substring(0, maxLength);
            
            if (addDots) {
                input = input + StringUtils.defaultString(dots);
            }
        }
        
        return input;
    }

    /**
     * Restricts the length of the input string to a maximum length. If addDots is true ... will be added if the string
     * is too long. The final length will always be smaller or equal to maxLength (ie the dots do not make it any longer)
     * There is one exception when the size < 3 and addDots is true.
     */
    public static String restrictLength(String input, int maxLength, boolean addDots) 
    {
        return restrictLength(input, maxLength, addDots, "...");
    }
    
    /**
     * If input does not end with endsWith it will append endsWith and return the result. If input does end with
     * endsWith input will be returned.
     */
    public static String ensureEndsWith(String input, String endsWith) 
    {
    	return StringUtils.endsWith(input, endsWith) ? StringUtils.defaultString(input) : 
    		StringUtils.defaultString(input) + StringUtils.defaultString(endsWith);
    }
    
    /**
     * Replaces the last chars with replaceCharWith
     * 
     * Example:
     * 
     * replaceLastChars("abc123", 3, "mn") returns "abcmnmnmn"
     */
    public static String replaceLastChars(String input, int lastChars, String replaceCharWith)
    {
        input = StringUtils.defaultString(input);
     
        int maxLength = input.length() - lastChars;
        
        if (maxLength < 0) {
            maxLength = 0;
        }
        
        String dots = StringUtils.repeat(StringUtils.defaultString(replaceCharWith), 
                input.length() - maxLength);
        
        return StringUtils.substring(input, 0, maxLength) + dots;
    }
}
