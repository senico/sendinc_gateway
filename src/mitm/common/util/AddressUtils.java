/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class AddressUtils
{
	private static final String IPV4_REGEX = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
	private static final String IPV6_HEX4DECCOMPRESSED_REGEX = "\\A((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?) ::((?:[0-9A-Fa-f]{1,4}:)*)(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
	private static final String IPV6_6HEX4DEC_REGEX = "\\A((?:[0-9A-Fa-f]{1,4}:){6,6})(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
	private static final String IPV6_HEXCOMPRESSED_REGEX = "\\A((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)\\z";
	private static final String IPV6_REGEX = "\\A(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}\\z";
	
	private static final Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);
	private static final Pattern IPV6_HEX4DECCOMPRESSED_PATTERN = Pattern.compile(IPV6_HEX4DECCOMPRESSED_REGEX);
	private static final Pattern IPV6_6HEX4DEC_PATTERN = Pattern.compile(IPV6_6HEX4DEC_REGEX);
	private static final Pattern IPV6_HEXCOMPRESSED_PATTERN = Pattern.compile(IPV6_HEXCOMPRESSED_REGEX);
	private static final Pattern IPV6_REGEX_PATTERN = Pattern.compile(IPV6_REGEX);
	
	private static final Pattern NETWORK_PATTERN = Pattern.compile("^\\s*\\[?([\\p{XDigit}.:]+)\\]?((?:/)(\\d+))?\\s*$");
	
	/**
	 * Returns true if the address is a valid IP address.
	 * @param address
	 * @return
	 */
	public static boolean isValidIPv4Address(String address)
	{
		if (address == null) {
			return false;
		}

		return IPV4_PATTERN.matcher(address).matches();
	}

	/**
	 * Returns true if the address is a valid IPv6 address.
	 * @param address
	 * @return
	 */
	public static boolean isValidIPv6Address(String address)
	{
		if (address == null) {
			return false;
		}

		return IPV6_HEX4DECCOMPRESSED_PATTERN.matcher(address).matches() || 
			IPV6_6HEX4DEC_PATTERN.matcher(address).matches() ||
			IPV6_HEXCOMPRESSED_PATTERN.matcher(address).matches() ||
			IPV6_REGEX_PATTERN.matcher(address).matches();
	}
	
	/**
	 * Returns true if the address is a valid IPv4 or IPv6 network with optional CIDR netmask.
	 * Examples: 
	 * 1) 192.168.0.0/32
	 * 2) 127.0.0.1
	 * 
	 * @param address
	 * @return
	 */
	public static boolean isValidNetwork(String network)
	{
		if (network == null) {
			return false;
		}

		boolean isValid = false;

		Matcher matcher = NETWORK_PATTERN.matcher(network);
		
		if (matcher.matches())
		{
			String address = matcher.group(1);
			String cidr = matcher.group(3);
			
			if (isValidIPv4Address(address) || isValidIPv6Address(address))
			{
				isValid = true;
				
				if (StringUtils.isNotEmpty(cidr))
				{
					int bits = NumberUtils.toInt(cidr, -1);
					
					isValid = bits >= 0;
				}
			}
		}
		
		return isValid;
	}
}
