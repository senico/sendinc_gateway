/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

public class PageReader extends LineNumberReader 
{
	private final int pageSize;
	
	private int currentPage = -1;
	
	public PageReader(Reader in, int pageSize) 
	{
		super(in);

		if (pageSize < 1) {
			throw new IllegalArgumentException("pageSize must be >= 1");
		}
		
		this.pageSize = pageSize;
	}
	
	public List<String> getPage(int page) 
	throws IOException 
	{
		if (page < 0) {
			throw new IllegalArgumentException("page must be >= 0");
		}
		
		int activePage = getLineNumber() / pageSize;
		
		if (page < activePage) {
			throw new IOException("The requested page is before the current page.");
		}

		currentPage = -1;
		
		List<String> lines = new LinkedList<String>();
		
		while (activePage <= page)
		{
			if (activePage != currentPage)
			{
				lines.clear();
				
				currentPage = activePage;
			}
			
			String line = readLine();
			
			if (line == null) {
				break;
			}

			lines.add(line);
			
			if (lines.size() > pageSize) {
				lines.remove(0);
			}

			activePage = getLineNumber() / pageSize;
		}
		
		return lines;
	}
	
	public int getCurrentPage() {
		return currentPage;
	}
}
