/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloseableIteratorUtils
{
    private final static Logger logger = LoggerFactory.getLogger(CloseableIteratorUtils.class);
    
    /**
     * Returns the elements from the {@link CloseableIterator} as a List.
     */
    public static <T> List<T> toList(CloseableIterator<T> iterator) 
    throws CloseableIteratorException 
    {
        List<T> list = new LinkedList<T>();
        
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        
        if (!iterator.isClosed()) {
        	throw new IllegalStateException("Iterator should have been closed.");
        }
        
        return list;
    }
    
    /**
     * Returns the number of elements from the iterator. It does this by stepping through
     * all entries and keeping a count. Depending on the iterator this can be a slow 
     * process.
     */
    public static long getCount(CloseableIterator<?> iterator) 
    throws CloseableIteratorException 
    {
        long count = 0;
        
        while (iterator.hasNext())
        {
            iterator.next();
            
            count++;
        }
        
        if (!iterator.isClosed()) {
            throw new IllegalStateException("Iterator should have been closed.");
        }
        
        return count;
    }
    
    /**
     * Close the iterator without throwing exceptions.
     * 
     * Note: RuntimeExceptions can be thrown
     */
    public static void closeQuietly(CloseableIterator<?> iterator)
    {
        if (iterator != null)
        {
            try {
                iterator.close();
            } 
            catch (CloseableIteratorException e) {
                logger.debug("Error closing iterator.", e);
            }
        }
    }
}
