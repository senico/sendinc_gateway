/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.common.locale.CharacterEncoding;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.ProcessRunner;
import mitm.common.util.SizeLimitedOutputStream;
import mitm.common.util.SizeUtils;

public class FetchmailManagerImpl implements FetchmailManager
{
    /*
     * The maximum time a command may run after which it is destroyed
     */
    private final static long TIMEOUT = 30 * DateUtils.MILLIS_PER_SECOND;
    
    /*
     * Maximal size of a command output (just to protect against a runaway command)
     */
    private final static long MAX_OUTPUT_LENGTH = SizeUtils.KB * 10;
    
    /*
     * Used to get some of the global templates
     */
    private final GlobalPreferencesManager globalPreferencesManager;
    
    /*
     * The external command (script or bin) to run for configuring Fetchmail
     */
    private final File externalApplyCommand;

    public FetchmailManagerImpl(GlobalPreferencesManager globalPreferencesManager, File externalApplyCommand)
    {
        Check.notNull(globalPreferencesManager, "globalPreferencesManager");
        Check.notNull(externalApplyCommand, "externalApplyCommand");
        
        this.globalPreferencesManager = globalPreferencesManager;
        this.externalApplyCommand = externalApplyCommand;
    }
    
    private FetchmailProperties getFetchmailProperties()
    throws HierarchicalPropertiesException
    {
        return new FetchmailPropertiesImpl(globalPreferencesManager.getGlobalUserPreferences().getProperties());   
    }
    
    @Override
    public FetchmailConfig getConfig()
    throws FetchmailException
    {
        try {
            return getFetchmailProperties().getFetchmailConfig();
        }
        catch (HierarchicalPropertiesException e) {
            throw new FetchmailException(e);
        } 
    }

    @Override
    public void setConfig(FetchmailConfig config)
    throws FetchmailException
    {
        try {
            getFetchmailProperties().setFetchmailConfig(config);
        }
        catch (HierarchicalPropertiesException e) {
            throw new FetchmailException(e);
        } 
    }
    
    private String getFirstLine(ByteArrayOutputStream bos)
    throws IOException
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        
        LineIterator it = IOUtils.lineIterator(bis, "US-ASCII");
        
        String result = null;
        
        if (it.hasNext()) {
            result = it.nextLine();
        }
        
        return result;
    }
    
    /*
     * Runs an external process that configures and restarts fetchmail
     */
    private void runExternalApplyProcess(String xml)
    throws IOException
    {
        List<String> cmd = new LinkedList<String>();
        
        cmd.add(externalApplyCommand.getPath());
        
        ProcessRunner runner = new ProcessRunner();
        
        runner.setTimeout(TIMEOUT);
        
        /*
         * The XML will be piped to the process
         */
        runner.setInput(IOUtils.toInputStream(xml, CharacterEncoding.US_ASCII));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        SizeLimitedOutputStream slos = new SizeLimitedOutputStream(bos, MAX_OUTPUT_LENGTH, true);
        
        runner.setError(slos);
        
        try {
            runner.run(cmd);
        }
        catch(IOException e)
        {
            /*
             * We would like to report the error message from the external program
             */
            String error = getFirstLine(bos);
            
            if (StringUtils.isNotBlank(error)) {
                throw new IOException(error);
            }
            
            throw e;
        }
    }
    
    @Override
    public void applyConfig()
    throws FetchmailException
    {
        try {
            String xml = getFetchmailProperties().getFetchmailXMLConfig();
            
            if (StringUtils.isEmpty(xml)) {
                throw new FetchmailException("Fetchmail configuration is not set.");
            }
            
            runExternalApplyProcess(xml);
        }
        catch (HierarchicalPropertiesException e) {
            throw new FetchmailException(e);
        }
        catch (IOException e) {
            throw new FetchmailException(e);
        }
    }
}
