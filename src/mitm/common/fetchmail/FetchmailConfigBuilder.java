/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;

import mitm.common.util.MiscStringUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

public class FetchmailConfigBuilder
{
    private final static String START_TOKEN = "### START-AUTO-CONFIG ###";
    private final static String END_TOKEN   = "### END-AUTO-CONFIG ###";
    
    /**
     * Writes the new config to targetConfig. Lines between START_TOKEN and END_TOKEN are replaced with the 
     * new config from FetchmailConfig.
     */
    public static void createConfig(FetchmailConfig config, InputStream sourceConfig, OutputStream targetConfig)
    throws IOException
    {
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(sourceConfig, "US-ASCII"));

        StrBuilder outputBuilder = new StrBuilder();
        
        boolean inBlock = false;
        boolean blockInjected = false;
        
        String line;
        
        do {
            line = reader.readLine();
            
            if (line != null)
            {
                if (inBlock)
                {
                    if (line.startsWith(END_TOKEN))
                    {
                        inBlock = false;
                        blockInjected = true;
                        
                        injectConfig(config, outputBuilder);
                        
                        outputBuilder.appendln(line);
                    }                    
                }
                else {
                    if (!blockInjected && line.startsWith(START_TOKEN)) {
                        inBlock = true;
                    }

                    outputBuilder.appendln(line);
                }
            }
        }
        while(line != null);
        
        targetConfig.write(MiscStringUtils.toAsciiBytes(outputBuilder.toString()));
    }
    
    /*
     * Removes any control characters and escapes " by replacing it with \x22
     */
    private static String escape(String input)
    {
        if (input == null) {
            return null;
        }
        
        input = MiscStringUtils.removeControlChars(input);
        
        return "\"" + StringUtils.replace(input, "\"", "\\x22") + "\"";
    }
    
    private static void injectConfig(FetchmailConfig config, StrBuilder output)
    {
        if (config.isCheckCertificate()) {
            output.append("sslcertck").appendNewLine();
        }

        output.append("set daemon ").append(config.getPollInterval()).appendNewLine();
        output.append("set postmaster ").append(escape(config.getPostmaster())).appendNewLine();
        
        for (Poll poll : config.getPolls())
        {
            output.append("poll ").append(escape(StringUtils.defaultString(poll.getServer(), "undefined")));

            if (poll.getPort() != null) {
                output.append(" service ").append(poll.getPort());
            }

            if (poll.getProtocol() != null) {
                output.append(" proto ").append(poll.getProtocol());
            }

            output.append(poll.isUIDL() ? " uidl " : " no uidl");;
            
            if (poll.getUsername() != null) {
                output.append(" user ").append(escape(poll.getUsername()));
            }
            
            if (poll.getPassword() != null) {
                output.append(" password ").append(escape(poll.getPassword()));
            }

            if (poll.getForwardTo() != null) {
                output.append(" is ").append(escape(poll.getForwardTo()));
            }
            
            if (poll.getFolder() != null) {
                output.append(" folder ").append(escape(poll.getFolder()));
            }
            
            output.append(" options");

            if (poll.isSSL()) {
                output.append(" ssl");
            }
            
            output.append(poll.isIdle() ? " idle" : " no idle");;
            output.append(poll.isKeep() ? " keep" : " no keep");;
            
            output.appendNewLine();
        }
    }
}
