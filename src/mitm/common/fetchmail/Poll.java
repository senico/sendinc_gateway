/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Poll")
public class Poll
{
    @XmlElement
    private String id;

    @XmlElement
    private String server;

    @XmlElement
    private Integer port;
    
    @XmlElement
    private Protocol protocol;
    
    @XmlElement
    private Authentication authentication;

    @XmlElement
    private String username;

    @XmlElement
    private String password;

    @XmlElement
    private boolean uidl;
    
    @XmlElement
    private String principal;

    @XmlElement
    private boolean ssl;
    
    @XmlElement
    private boolean keep;
    
    @XmlElement
    private boolean idle;

    @XmlElement
    private String folder;

    @XmlElement
    private String forwardTo;

    public void setID(String id) {
        this.id = id;
    }
    
    public String getID() {
        return id;
    }
    
    public void setServer(String server) {
        this.server = server;
    }
    
    public String getServer() {
        return server;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
    
    public Integer getPort() {
        return port;
    }
    
    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }
    
    public Protocol getProtocol() {
        return protocol;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }
    public Authentication getAuthentication() {
        return authentication;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setUIDL(boolean enable) {
        this.uidl = enable;
    }
    
    public boolean isUIDL() {
        return uidl;
    }
    
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    public String getPrincipal() {
        return principal;
    }

    public void setSSL(boolean enable) {
        this.ssl = enable;
    }
    
    public boolean isSSL() {
        return ssl;
    }
    
    public void setKeep(boolean keep) {
        this.keep = keep;
    }
    
    public boolean isKeep() {
        return keep;
    }
    
    public void setIdle(boolean enable) {
        this.idle = enable;
    }
    
    public boolean isIdle() {
        return idle;
    }
    
    public void setFolder(String folder) {
        this.folder = folder;
    }
    
    public String getFolder() {
        return folder;
    }

    public void setForwardTo(String email) {
        this.forwardTo = email;
    }
    
    public String getForwardTo() {
        return forwardTo;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        
        if (obj.getClass() != getClass()) {
          return false;
        }
        
        Poll rhs = (Poll) obj;
        
        return new EqualsBuilder()
            .append(StringUtils.lowerCase(server), StringUtils.lowerCase(rhs.server))
            .append(port, rhs.port)
            .append(protocol, rhs.protocol)
            .append(username, rhs.username)
            .isEquals();
    }
    
    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(StringUtils.lowerCase(server))
            .append(port)
            .append(protocol)
            .append(username)
            .toHashCode();
    }
}
