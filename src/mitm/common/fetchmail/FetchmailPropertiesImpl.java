/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;

public class FetchmailPropertiesImpl implements FetchmailProperties
{
    /*
     * The name of the xml config property 
     */
    private final static String PROPERTY_NAME = "fetchmail.config";
    
    /*
     * The properties to delegate to
     */
    private final HierarchicalProperties properties;
    
    public FetchmailPropertiesImpl(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }
    
    @Override
    public String getFetchmailXMLConfig()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PROPERTY_NAME, true /* decrypt */);
    }
    
    @Override
    public FetchmailConfig getFetchmailConfig()
    throws HierarchicalPropertiesException
    {
        FetchmailConfig config = null;
        
        String xml = getFetchmailXMLConfig();

        try {
            if (StringUtils.isNotEmpty(xml)) {
                config = FetchmailConfigFactory.unmarshall(IOUtils.toInputStream(xml, CharEncoding.US_ASCII));
            }
        }
        catch (JAXBException e) {
            throw new HierarchicalPropertiesException("Error unmarshalling XML.", e);
        }
        catch (IOException e) {
            throw new HierarchicalPropertiesException("Error reading XML.", e);
        }        
        
        if (config == null) {
            config = new FetchmailConfig();
        }
        
        return config;
    }

    @Override
    public void setFetchmailConfig(FetchmailConfig config)
    throws HierarchicalPropertiesException
    {
        String xml = null;
        
        if (config != null)
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            try {
                FetchmailConfigFactory.marshall(config, bos);
                
                xml = new String(bos.toByteArray(), CharEncoding.US_ASCII);
            }
            catch (JAXBException e) {
                throw new HierarchicalPropertiesException("Error marshalling FetchmailConfig.", e);
            }
            catch (UnsupportedEncodingException e) {
                throw new HierarchicalPropertiesException("Error decoding.", e);
            }
        }
        
        properties.setProperty(PROPERTY_NAME, xml, true /* encrypt */);
    }
}
