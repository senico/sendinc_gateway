/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * FetchmailConfigurator is used for configuring fetchmail. It expects an XML file with the new configuration on the
 * standard input and a source file parameter as the source fetchmail config. The new config will be written to 
 * standard output.
 * 
 * Note: it is assumed that all data is US-ASCII encoded.
 * 
 * @author Martijn Brinkers
 *
 */
public class FetchmailConfigWriter
{
    /*
     * Source config file
     */
    private File sourceFile;
        
    private FetchmailConfigWriter(String[] args)
    throws IllegalArgumentException
    {
        if (args == null || args.length != 1) {
            throw new IllegalArgumentException("source config file expected.");
        }
        
        sourceFile = new File(args[0]);
        
        if (!sourceFile.exists()) {
            throw new IllegalArgumentException("source file does not exist.");
        }
    }
    
    private void writeConfig()
    throws JAXBException, FileNotFoundException, IOException
    {
        FetchmailConfig config = FetchmailConfigFactory.unmarshall(System.in);
        
        InputStream input = new FileInputStream(sourceFile); 
        
        try {
            /*
             * Read the complete input file so we can overwrite it when source and target are equal.
             */
            String content = IOUtils.toString(input, "US-ASCII");

            FetchmailConfigBuilder.createConfig(config, IOUtils.toInputStream(content, "US-ASCII"), System.out);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }
    
    public static void main(String[] args)
    {
        try {
            FetchmailConfigWriter configurator = new FetchmailConfigWriter(args);
            
            configurator.writeConfig();
        }
        catch (JAXBException e) {
            System.err.println("Error reading input. " + ExceptionUtils.getRootCauseMessage(e));
        }
        catch (IllegalArgumentException e) {
            System.err.println("Illegal argument. " + ExceptionUtils.getRootCauseMessage(e));
        }
        catch (FileNotFoundException e) {
            System.err.println("File not found. " + ExceptionUtils.getRootCauseMessage(e));
        }
        catch (IOException e) {
            System.err.println("Error reading file. " + ExceptionUtils.getRootCauseMessage(e));
        }
    }
}
