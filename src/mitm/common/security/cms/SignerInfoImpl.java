/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Date;

import mitm.common.security.asn1.ASN1Utils;
import mitm.common.security.certificate.X500PrincipalUtils;
import mitm.common.security.digest.Digest;
import mitm.common.security.smime.SMIMEAttributeUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.SystemUtils;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;

public class SignerInfoImpl implements SignerInfo
{
    private final SignerInformation signerInformation;

    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    public SignerInfoImpl(SignerInformation signerInformation, String nonSensitiveProvider, 
            String sensitiveProvider)
    {
        this.signerInformation = signerInformation;
        this.nonSensitiveProvider = nonSensitiveProvider;
        
        /*
         * Note: nonSensitiveProvider is currently not used
         */
    }
    
    @Override
    public int getVersion() {
        return signerInformation.getVersion();
    }

    @Override
    public SignerIdentifier getSignerId()
    throws IOException 
    {
        SignerId id = signerInformation.getSID();
        
        return new SignerIdentifierImpl(X500PrincipalUtils.fromX500Name(id.getIssuer()), id.getSerialNumber(), 
                id.getSubjectKeyIdentifier()); 
    }

    @Override
    public String getDigestAlgorithmOID() {
        return signerInformation.getDigestAlgOID();
    }
    
    @Override
    public byte[] getDigestAlgorithmParams() {
        return signerInformation.getDigestAlgParams();
    }

    @Override
    public String getEncryptionAlgorithmOID() {
        return signerInformation.getEncryptionAlgOID();
    }

    @Override
    public byte[] getEncryptionAlgorithmParams() {
        return signerInformation.getEncryptionAlgParams();
    }

    @Override
    public AttributeTable getSignedAttributes() {
        return signerInformation.getSignedAttributes();
    }

    @Override
    public AttributeTable getUnsignedAttributes() {
        return signerInformation.getUnsignedAttributes();
    }
    
    @Override
    public Date getSigningTime() {
        return SMIMEAttributeUtils.getSigningTime(getSignedAttributes());
    }
    
    @Override
    public boolean verify(PublicKey key, String provider)
    throws SignerInfoException
    {
        try {
            JcaSimpleSignerInfoVerifierBuilder verifierBuilder = new JcaSimpleSignerInfoVerifierBuilder(); 
            
            verifierBuilder.setProvider(provider);
            
            return signerInformation.verify(verifierBuilder.build(key));
        }
        catch (CMSException e) {
            throw new SignerInfoException(e);
        } 
        catch (OperatorCreationException e) {
            throw new SignerInfoException(e);
        }
    }

    @Override
    public boolean verify(PublicKey key)
    throws SignerInfoException
    {
        return verify(key, nonSensitiveProvider);
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        String digestName = getDigestAlgorithmOID();
        
        Digest digest = Digest.fromOID(digestName);
        
        if (digest != null) {
            digestName = digest.toString();
        }
        
        Date signingTime = getSigningTime();
        
        sb.append("CMS Version: ");
        sb.append(getVersion());
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("*** [SignerId] ***");
        sb.append(SystemUtils.LINE_SEPARATOR);
        
        try {
            sb.append(getSignerId());
        } 
        catch (IOException e) {
            sb.append("Error getting signer Id. Message: " + e.getMessage());
        }
        
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Digest: ");
        sb.append(digestName);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Encryption alg. OID: ");
        sb.append(getEncryptionAlgorithmOID());
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Signing time: ");
        sb.append(ObjectUtils.toString(signingTime));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Signed attributes: ");
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(ASN1Utils.dump(getSignedAttributes()));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Unsigned attributes: ");
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(ASN1Utils.dump(getUnsignedAttributes()));
        
        return sb.toString();
    }
}
