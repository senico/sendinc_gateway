/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.io.InputStream;

import org.bouncycastle.asn1.ASN1SequenceParser;
import org.bouncycastle.asn1.ASN1StreamParser;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CMSContentTypeClassifier

{
    private static Logger logger = LoggerFactory.getLogger(CMSContentTypeClassifier.class);
    
    /**
     * Returns the CMS content type of the provided sequence.
     * 
     * See RFC3852 for content types
     * 
     * @param sequenceParser
     * @return
     */
    public static CMSContentType getContentType(ASN1SequenceParser sequenceParser) 
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;
        
        try {
            ContentInfoParser contentInfoParser = new ContentInfoParser(sequenceParser);
            
            DERObjectIdentifier derContentType = contentInfoParser.getContentType();
                
            if (CMSObjectIdentifiers.data.equals(derContentType)) {
                contentType = CMSContentType.DATA;
            }
            else if (CMSObjectIdentifiers.signedData.equals(derContentType)) {
                contentType = CMSContentType.SIGNEDDATA;
            }
            else if (CMSObjectIdentifiers.envelopedData.equals(derContentType)) {
                contentType = CMSContentType.ENVELOPEDDATA;
            }
            else if (CMSObjectIdentifiers.signedAndEnvelopedData.equals(derContentType)) {
                contentType = CMSContentType.SIGNEDANDENVELOPEDDATA;
            }
            else if (CMSObjectIdentifiers.digestedData.equals(derContentType)) {
                contentType = CMSContentType.DIGESTEDDATA;
            }
            else if (CMSObjectIdentifiers.encryptedData.equals(derContentType)) {
                contentType = CMSContentType.ENCRYPTEDDATA;
            }
            else if (CMSObjectIdentifiers.compressedData.equals(derContentType)) {
                contentType = CMSContentType.COMPRESSEDDATA;
            }
        } 
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }
        return contentType;
    }
    
    /**
     * Returns the CMS content type of the provided byte array
     * 
     * See RFC3852 for content types
     * 
     * @param sequenceParser
     * @return
     */
    public static CMSContentType getContentType(byte[] data) 
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;

        try {
            ASN1StreamParser streamParser = new ASN1StreamParser(data);
            
            ASN1SequenceParser sequenceParser = (ASN1SequenceParser) streamParser.readObject();
            
            contentType = getContentType(sequenceParser);
        }
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }
        
        return contentType;
    }
    
    /**
     * Returns the CMS content type of the provided sequence.
     * 
     * See RFC3852 for content types
     * 
     * @param sequenceParser
     * @return
     */
    public static CMSContentType getContentType(InputStream input) 
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;

        try {
            ASN1StreamParser streamParser = new ASN1StreamParser(input);
            
            Object object = streamParser.readObject();
            
            if (object instanceof ASN1SequenceParser) 
            {
                ASN1SequenceParser sequenceParser = (ASN1SequenceParser) object;
                contentType = getContentType(sequenceParser);
            } 
            else {
                logger.warn("Object is not a ASN1SequenceParser.");
            }
        }
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }
        
        return contentType;
    }
}
