/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.security.AlgorithmParameters;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;

import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSEnvelopedDataParser;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JceAlgorithmIdentifierConverter;

/**
 * Adapter to give org.bouncycastle.cms.CMSEnvelopedDataParser a common interface.
 * 
 * @author Martijn Brinkers
 *
 */
public class CMSEnvelopedDataParserAdapterImpl implements CMSEnvelopedDataAdapter
{
    /*
     * CMS enveloped data
     */
    private final CMSEnvelopedDataParser envelopedData;
    
    /*
     * Factory for security related functionality
     */
    private final SecurityFactory securityFactory;
    
    public CMSEnvelopedDataParserAdapterImpl(CMSEnvelopedDataParser envelopedData)
    {
        this.envelopedData = envelopedData;
        
        this.securityFactory = SecurityFactoryFactory.getSecurityFactory();        
    }
    
    @Override
    public String getEncryptionAlgOID() {
        return envelopedData.getEncryptionAlgOID();
    }

    @Override
    public byte[] getEncryptionAlgParams() {
        return envelopedData.getEncryptionAlgParams();
    }
    
    @Override
    public AlgorithmParameters getEncryptionAlgorithmParameters() 
    throws CMSException 
    {
        return new JceAlgorithmIdentifierConverter().setProvider(securityFactory.getNonSensitiveProvider()).
                getAlgorithmParameters(envelopedData.getContentEncryptionAlgorithm());
    }
    
    @Override
    public RecipientInformationStore getRecipientInfos() {
        return envelopedData.getRecipientInfos();
    }
    
    @Override
    public AttributeTable getUnprotectedAttributes() 
    throws IOException 
    {
        return envelopedData.getUnprotectedAttributes();
    }
}
