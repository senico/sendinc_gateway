/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Date;

import org.bouncycastle.asn1.cms.AttributeTable;

public interface SignerInfo
{
    /**
     * Return the version number of the SignerInfo structure. It's 1 or 3. 
     * See http://www.ietf.org/rfc/rfc3852.txt. 5.3.  SignerInfo Type
     */
    public int getVersion();

    /**
     * returns the signer identifier of this signer
     * @throws IOException 
     */
    public SignerIdentifier getSignerId()
    throws IOException;
    
    /**
     * The OID of the hash algorithm used to sign the data
     * @return
     */
    public String getDigestAlgorithmOID();
    
    /**
     * The encoded parameters accompanying the digest algorithm. Returns null if the algorithm
     * does not have any parameters.
     */
    public byte[] getDigestAlgorithmParams();

    /**
     * The encryption algorithm which was used to encrypt the digest.
     */
    public String getEncryptionAlgorithmOID();

    /**
     * The encoded parameters accompanying the encryption algorithm. Returns null if the algorithm
     * does not have any parameters.
     */
    public byte[] getEncryptionAlgorithmParams();

    /**
     * Table of signed attributes. 
     */
    public AttributeTable getSignedAttributes();

    /**
     * Table of unsigned attributes. 
     */
    public AttributeTable getUnsignedAttributes();
    
    /**
     * The signing time signed attribute. Null if unavailable. 
     */
    public Date getSigningTime();
    
    /**
     * Verifies the signature by checking if the private key that signed the message 
     * is associated with the provided public key.
     * @param key
     * @param provider
     * @return
     */
    public boolean verify(PublicKey key, String provider)
    throws SignerInfoException;

    /**
     * Verifies the signature by checking if the private key that signed the message 
     * is associated with the provided public key.
     * @param key
     * @param provider
     * @return
     */
    public boolean verify(PublicKey key)
    throws SignerInfoException;
}
