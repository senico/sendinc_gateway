/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.PrivateKey;

import mitm.common.security.KeyIdentifier;
import mitm.common.security.certificate.X500PrincipalUtils;
import mitm.common.util.Check;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.Recipient;
import org.bouncycastle.cms.RecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.jcajce.JceAlgorithmIdentifierConverter;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of RecipientInfo
 * 
 * @author Martijn Brinkers
 *
 */
public class RecipientInfoImpl implements RecipientInfo
{
    private Logger logger = LoggerFactory.getLogger(RecipientInfoImpl.class);
    
    private final RecipientInformation recipientInformation;

    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    /*
     * The provider for sensitive operations
     */
    private final String sensitiveProvider;
    
    private KeyIdentifier recipientIdentifier;

    
    public RecipientInfoImpl(RecipientInformation recipientInformation, String nonSensitiveProvider, 
            String sensitiveProvider)
    throws RecipientInfoException
    {
        this.recipientInformation = recipientInformation;
        this.nonSensitiveProvider = nonSensitiveProvider;
        this.sensitiveProvider = sensitiveProvider;
        
        initRecipientId();
    }
    
    private Recipient getRecipient(Key key)
    throws RecipientInfoException
    {
        Check.notNull(key, "key");

        /*
         * We currently only support Public/Private keys
         */
        if (!(key instanceof PrivateKey)) {
            throw new RecipientInfoException("The key is not a PrivateKey");
        }

        JceKeyTransEnvelopedRecipient recipient = new JceKeyTransEnvelopedRecipient((PrivateKey) key);
        
        recipient.setProvider(sensitiveProvider);
        recipient.setContentProvider(nonSensitiveProvider);
        
        return recipient;
    }
    
    @Override
    public byte[] getContent(Key key)
    throws RecipientInfoException 
    {
        try {
            return recipientInformation.getContent(getRecipient(key));
        }
        catch (CMSException e) {
            throw new RecipientInfoException(e);
        }
    }

    @Override
    public InputStream getContentStream(Key key)
    throws RecipientInfoException 
    {
        try {
            return recipientInformation.getContentStream(getRecipient(key)).getContentStream();
        }
        catch (CMSException e) {
            throw new RecipientInfoException(e);
        } 
        catch (IOException e) {
            throw new RecipientInfoException(e);
        }
    }

    @Override
    public String getKeyEncryptionAlgorithmOID()
    {
        return recipientInformation.getKeyEncryptionAlgOID();
    }

    @Override
    public AlgorithmParameters getKeyEncryptionAlgorithmParameters() 
    throws RecipientInfoException
    {
        try {
            return new JceAlgorithmIdentifierConverter().setProvider(nonSensitiveProvider).
                    getAlgorithmParameters(recipientInformation.getKeyEncryptionAlgorithm());
        }
        catch (CMSException e) {
            throw new RecipientInfoException(e);
        }
    }

    /*
     * Convert the BC version of RecipientId to our own RecipientId
     */
    private KeyTransRecipientId getKeyTransRecipientId(org.bouncycastle.cms.KeyTransRecipientId otherRecipientId)
    throws RecipientInfoException
    {
        try {
            KeyTransRecipientIdImpl recipientId = new KeyTransRecipientIdImpl(
                    X500PrincipalUtils.fromX500Name(otherRecipientId.getIssuer()), 
                    otherRecipientId.getSerialNumber(), 
                    otherRecipientId.getSubjectKeyIdentifier());
            
            return recipientId;
        }
        catch (IOException e) {
            throw new RecipientInfoException(e);
        }
    }
    
    private void initRecipientId()
    throws RecipientInfoException
    {
        /*
         * We currently only support KeyTransRecipientInformation
         */
        if (recipientInformation instanceof KeyTransRecipientInformation)
        {
            RecipientId recipientId = recipientInformation.getRID();
            
            if (recipientId instanceof org.bouncycastle.cms.KeyTransRecipientId)
            {
                recipientIdentifier = getKeyTransRecipientId((org.bouncycastle.cms.KeyTransRecipientId) 
                        recipientId);
            }
            else {
                logger.warn("Unsupported recipientId type. Class: " + recipientId.getClass());
            }
        }
        else {
            logger.warn("Unsupported recipientInformation type. Class: " + recipientInformation.getClass());
        }
    }
    
    @Override
    public KeyIdentifier getRecipientId()
    {
        return recipientIdentifier;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(getRecipientId().toString());
        sb.append("/");
        sb.append(getKeyEncryptionAlgorithmOID());
                
        AlgorithmParameters keyAlgParams = null; 
            
        try {
            keyAlgParams = getKeyEncryptionAlgorithmParameters();
            
            sb.append("/");
            sb.append(keyAlgParams);
        }
        catch (RecipientInfoException e) {
            /* There were no algorithm parameters. */
        }
        
        return sb.toString();
    }
}
