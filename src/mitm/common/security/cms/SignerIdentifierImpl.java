/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertSelector;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.certificate.X509CertSelectorBuilder;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.util.BigIntegerUtils;
import mitm.common.util.HexUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.SystemUtils;

public class SignerIdentifierImpl implements SignerIdentifier
{
    private final X500Principal issuer;
    private final BigInteger serialNumber;
    private final byte[] subjectKeyIdentifier;
    
    public SignerIdentifierImpl(X500Principal issuer, BigInteger serialNumber, byte[] subjectKeyIdentifier)
    {
        this.issuer = issuer;
        this.serialNumber = serialNumber;
        this.subjectKeyIdentifier = subjectKeyIdentifier;
    }

    public SignerIdentifierImpl(X509Certificate certificate) 
    throws IOException
    {
        this(certificate.getIssuerX500Principal(), certificate.getSerialNumber(), 
                X509CertificateInspector.getSubjectKeyIdentifier(certificate));
    }
    
    @Override
    public X500Principal getIssuer() {
        return issuer;
    }

    @Override
    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    @Override
    public byte[] getSubjectKeyIdentifier() {
        return subjectKeyIdentifier;
    }
    
    @Override
    public boolean match(X509Certificate certificate) 
    throws IOException
    {
        if (certificate == null) {
            return false;
        }

        if (issuer != null && !issuer.equals(certificate.getIssuerX500Principal())) {
            return false;
        }

        if (serialNumber != null && !serialNumber.equals(certificate.getSerialNumber())) {
            return false;
        }

        if (subjectKeyIdentifier != null && !Arrays.equals(subjectKeyIdentifier, 
                X509CertificateInspector.getSubjectKeyIdentifier(certificate))) 
        {
            return false;
        }

        return true;
    }
     
    @Override
    public CertSelector getSelector()
    throws IOException 
    {
        X509CertSelector selector = new X509CertSelector();
        
        selector.setIssuer(issuer);
        selector.setSerialNumber(serialNumber);
        
        if (subjectKeyIdentifier != null) 
        {
            /* 
             * X509CertSelector expects a DER encoded subjectKeyIdentifier.
             */
            X509CertSelectorBuilder.setSubjectKeyIdentifier(selector, subjectKeyIdentifier);
        }

        return selector;
    }    
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Issuer: ");
        sb.append(ObjectUtils.toString(issuer));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Serial number: ");
        sb.append(BigIntegerUtils.hexEncode(serialNumber, ""));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("SubjectKeyIdentifier: ");
        sb.append(HexUtils.hexEncode(subjectKeyIdentifier));
        
        return sb.toString();
    }
}
