/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.KeyStoreException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import mitm.common.security.keystore.BasicKeyStore;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CMSEnvelopedInspectorImpl implements CMSEnvelopedInspector
{
    private Logger logger = LoggerFactory.getLogger(CMSEnvelopedInspectorImpl.class);
    
    private final CMSEnvelopedDataAdapter envelopedDataAdapter;
    private final BasicKeyStore basicKeyStore;
    
    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    /*
     * The provider for sensitive operations
     */
    private final String sensitiveProvider;

    private List<RecipientInfo> recipients;
    
    public CMSEnvelopedInspectorImpl(CMSEnvelopedDataAdapter envelopedDataAdapter, BasicKeyStore basicKeyStore, 
            String nonSensitiveProvider, String sensitiveProvider)
    {
        this.envelopedDataAdapter = envelopedDataAdapter;
        this.basicKeyStore = basicKeyStore;
        this.nonSensitiveProvider = nonSensitiveProvider;
        this.sensitiveProvider = sensitiveProvider;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RecipientInfo> getRecipients()
    throws CryptoMessageSyntaxException 
    {
        if (recipients == null) 
        {
            recipients = new LinkedList<RecipientInfo>();
    
            RecipientInformationStore store = envelopedDataAdapter.getRecipientInfos();
            
            Collection<RecipientInformation> cmsRecipients = store.getRecipients();
            
            for (RecipientInformation recipientInformation : cmsRecipients) 
            {
                /*
                 *  currently only KeyTransRecipientInformation is supported
                 */
                if (recipientInformation instanceof KeyTransRecipientInformation) 
                {
                    try {
                        recipients.add(new RecipientInfoImpl(recipientInformation, nonSensitiveProvider, 
                                sensitiveProvider));
                    }
                    catch (RecipientInfoException e) {
                        throw new CryptoMessageSyntaxException(e);
                    }
                } 
                else {
                    logger.warn("Unsopported recipientInformation. Class: " + recipientInformation.getClass());
                }
            }
        }
        
        return Collections.unmodifiableList(recipients);
    }
    
    @Override
    public String getEncryptionAlgorithmOID()
    {
        return envelopedDataAdapter.getEncryptionAlgOID();
    }
    
    @Override
    public AlgorithmParameters getEncryptionAlgorithmParameters() 
    throws CryptoMessageSyntaxException 
    {
        try {
            return envelopedDataAdapter.getEncryptionAlgorithmParameters();
        }
        catch (CMSException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }

    @Override
    public AttributeTable getUnprotectedAttributes() 
    throws CryptoMessageSyntaxException 
    {
        try {
            return envelopedDataAdapter.getUnprotectedAttributes();
        }
        catch (IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }

    private Collection<? extends Key> getKeys(RecipientInfo recipientInfo) 
    throws KeyStoreException
    {
        Collection<? extends Key> keys = basicKeyStore.getMatchingKeys(recipientInfo.getRecipientId());
        
        return keys;
    }
    
    @Override
    public byte[] getContent() 
    throws CryptoMessageSyntaxException 
    {
        try {
            byte[] content = IOUtils.toByteArray(getContentStream());
            
            return content;
        }
        catch(IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }

    @Override
    public InputStream getContentStream() 
    throws CryptoMessageSyntaxException
    {
        if (basicKeyStore == null) {
            throw new KeyNotFoundException("There is no basicKeyStore.");
        }

        Collection<RecipientInfo> recipients = getRecipients();
        
        Collection<? extends Key> keys;

        InputStream input;

        boolean keyFound = false;
        
        for (RecipientInfo recipientInfo : recipients) 
        {
            try {
                /* get all the private keys matching this recipient */
                keys = getKeys(recipientInfo);
                
                for (Key key : keys)
                {
                    keyFound = true;
                    
                    try {
                        /* 
                         * decrypt the message using the key
                         */
                        input = recipientInfo.getContentStream(key);
                        
                        if (input != null) {
                            return input;
                        }
                    }
                    catch (RecipientInfoException e) {
                        logger.error("Exception getting content stream. Trying next Key.", e);
                    }
                }
            }
            catch (KeyStoreException e) {
                logger.error("Exception getting keys for recipientId. Trying next recipientId.", e);
            } 
        }
        
        if (recipients.size() == 0) {
            throw new CryptoMessageSyntaxException("There are no CMS recipients.");
        } 
        else if (!keyFound) {
            throw new KeyNotFoundException("A suitable decryption key could not be found.");
        }
        
        throw new CryptoMessageSyntaxException("Content is null.");
    }
}
