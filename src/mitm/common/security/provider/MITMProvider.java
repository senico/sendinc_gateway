/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.provider;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

import mitm.common.hibernate.SessionManager;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.certstore.jce.X509CertStoreProvider;
import mitm.common.security.keystore.jce.KeyStoreHibernate;
import mitm.common.security.password.PBEncryptionImpl;

/**
 * A Java security provider so we can register our own extensions like the DatabaseKeyStore.
 * 
 * This providers name is "mitm".
 * 
 * @author Martijn Brinkers
 *
 */
public class MITMProvider extends Provider
{
    private static final long serialVersionUID = -3732651865763895561L;
    
    public static final String PROVIDER           = "mitm"; 
    public static final String DATABASE_KEYSTORE  = "DatabaseKeyStore"; 
    public static final String DATABASE_CERTSTORE = "DatabaseCertStore"; 
    
    private static MITMProvider provider;
    
    private static SessionManager sessionManager;
    
    /*
     * Local extension of KeyStoreHibernate because it requires the session source
     * and a password encryptor.
     */
    protected static class KeyStoreHibernateImpl extends KeyStoreHibernate
    {
        public KeyStoreHibernateImpl() 
        throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException 
        {
            super(sessionManager, new PBEncryptionImpl());
        }
    }
    
    public MITMProvider() 
    {
        super(PROVIDER, 1.0, "MITM provider.");

        /* register the JCE classes */
        put("KeyStore."  + DATABASE_KEYSTORE, KeyStoreHibernateImpl.class.getName());
        put("CertStore." + DATABASE_CERTSTORE, X509CertStoreProvider.class.getName());
        
        put("CertificateFactory.X.509", X509CertificateFactory.class.getName());
        put("Alg.Alias.CertificateFactory.X509", "X.509");
    }
    
    /**
     * Initializes this provider. Must be called once before use. 
     * 
     * @param sessionSource
     */
    public static synchronized MITMProvider initialize(SessionManager sessionManager) 
    {
        setSessionManager(sessionManager);        

        if (provider == null) 
        {            
            provider = new MITMProvider();
            
            Security.addProvider(provider);
        }
        
        return provider;
    }
    
    public static void setSessionManager(SessionManager sessionManager) {
        MITMProvider.sessionManager = sessionManager;        
    }
    
    public static Provider getProvider() {
        return provider;
    }
}
