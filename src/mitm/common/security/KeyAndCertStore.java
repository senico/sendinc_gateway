/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertStoreException;
import java.util.Collection;

import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.keystore.BasicKeyStore;

public interface KeyAndCertStore extends X509CertStoreExt, BasicKeyStore
{
    /**
     * Add the keyAndCertificate to the underlying store. Returns true if a certificate is added or, if the certificate
     * was already in the store by the entry did not yet have a private key
     */
    public boolean addKeyAndCertificate(KeyAndCertificate keyAndCertificate) 
    throws CertStoreException, KeyStoreException;
        
    /**
     * Returns a KeyAndCertificate associated with the certStoreEntry. If the certStoreEntry has an
     * associates key entry and the key exists the key is returned.
     */    
    public KeyAndCertificate getKeyAndCertificate(X509CertStoreEntry certStoreEntry)
    throws CertStoreException, KeyStoreException;
    
    /**
     * Synchronizes the CertStore and/or the KeyStore. Certificates from the KeyStore are placed in the
     * CertStore and the key alias will be set. A check is done on certificates from the CertStore with a 
     * key alias whether the key is still available in the KeyStore.
     * 
     * The sync method can for example be used when an external KeyStore (for example an HSM) is attached.
     */
    public void sync(SyncMode syncMode) 
    throws KeyStoreException, CertStoreException;
    
    /**
     * Overrides BasicKeyStore.getMatchingKeys to return collection extending PrivateKey
     * @see mitm.common.security.keystore.BasicKeyStore#getMatchingKeys(mitm.common.security.KeyIdentifier)
     */
    @Override
    public Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier)
    throws KeyStoreException;    

    /**
     * Overrides BasicKeyStore.getMatchingKeys to return a bounded collection extending PrivateKey
     * @see mitm.common.security.keystore.BasicKeyStore#getMatchingKeys(mitm.common.security.KeyIdentifier)
     */
    public Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier, Integer firstResult, Integer maxResults)
    throws KeyStoreException;    
}
