/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.otp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.ArrayUtils;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.util.Base32;
import mitm.common.util.Check;

/**
 * HMAC based OTPGenerator implementation
 * 
 * @author Martijn Brinkers
 *
 */
public class HMACOTPGenerator implements OTPGenerator
{
    /*
     * The HMAC algorithm to use
     */
    private final String algorithm;
    
    /*
     * Factory for creating security class instances
     */
    private final SecurityFactory securityFactory;
    
    public HMACOTPGenerator(String algorithm)
    {
        Check.notNull(algorithm, "algorithm");

        this.algorithm = algorithm;

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    private Mac createMAC(byte[] secret)
    throws OTPException
    {

        try {
            Mac mac = securityFactory.createMAC(algorithm);
            
            SecretKeySpec keySpec = new SecretKeySpec(secret, "raw");
            
            mac.init(keySpec);
            
            return mac;
        } 
        catch (NoSuchAlgorithmException e) {
            throw new OTPException(e);
        } 
        catch (NoSuchProviderException e) {
            throw new OTPException(e);
        } 
        catch (InvalidKeyException e) {
            throw new OTPException(e);
        }
    }
    
    @Override
    public String generate(byte[] secret, byte[] counter, int byteLength)
    throws OTPException
    {
        Mac mac = createMAC(secret);
        
        /*
         * Convert the password to base32 to make it easier for end users to read and
         * limit the number of bytes to make the password not too long
         */            
        return Base32.encode(ArrayUtils.subarray(mac.doFinal(counter), 0, byteLength));
    }
}
