/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.bouncycastle;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.TrustAnchor;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;

import mitm.common.security.SecurityFactory;
import mitm.common.security.certificate.X509CertificateBuilder;
import mitm.common.security.certificate.impl.StandardX509CertificateBuilder;
import mitm.common.security.crl.X509CRLBuilder;
import mitm.common.security.crl.X509CRLBuilderImpl;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.security.crypto.impl.RandomGeneratorImpl;

public class SecurityFactoryBouncyCastle implements SecurityFactory
{
    public static final String PROVIDER_NAME = "BC";
      
    public SecurityFactoryBouncyCastle()
    throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        InitializeBouncycastle.initialize();
    }

    @Override
    public CertificateFactory createCertificateFactory(String certificateType) 
    throws CertificateException, NoSuchProviderException 
    {
        return CertificateFactory.getInstance(certificateType, PROVIDER_NAME);
    }

    @Override
    public MessageDigest createMessageDigest(String digestType)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return MessageDigest.getInstance(digestType, PROVIDER_NAME);
    }
    
    @Override
    public X509CertificateBuilder createX509CertificateBuilder() 
    {
        return new StandardX509CertificateBuilder(PROVIDER_NAME, PROVIDER_NAME);
    }
    
    @Override
    public KeyPairGenerator createKeyPairGenerator(String keyType)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return KeyPairGenerator.getInstance(keyType, PROVIDER_NAME);
    }
    
    @Override
    public SecretKeyFactory createSecretKeyFactory(String algorithm) 
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return SecretKeyFactory.getInstance(algorithm, PROVIDER_NAME);
    }

    @Override
    public KeyFactory createKeyFactory(String algorithm) 
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return KeyFactory.getInstance(algorithm, PROVIDER_NAME);
    }

    @Override
    public CertPathBuilder createCertPathBuilder(String algorithm)
    throws NoSuchProviderException, NoSuchAlgorithmException
    {
        return CertPathBuilder.getInstance(algorithm, PROVIDER_NAME);
    }
    
    @Override
    public PKIXBuilderParameters createPKIXBuilderParameters(Set<TrustAnchor> trustAnchors, CertSelector targetConstraints) 
    throws InvalidAlgorithmParameterException
    {
        return new PKIXBuilderParameters(trustAnchors, targetConstraints);
    }
    
    @Override
    public SecureRandom createSecureRandom(String algorithm) 
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        /*
         * BouncyCastle does not have its own SecureRandom generator so
         * we will use the default one
         */
        return SecureRandom.getInstance(algorithm); 
    }
    
    @Override
    public SecureRandom createSecureRandom()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        /*
         * Use the default secure random. On Linux the default secure random implementation will be 
         * sun.security.provider.NativePRNG which uses /dev/random or /dev/urandom as a random source. 
         */
        return new SecureRandom();
    }
    
    @Override
    public KeyStore createKeyStore(String keyStoreType) 
    throws KeyStoreException, NoSuchProviderException
    {   
        return KeyStore.getInstance(keyStoreType, PROVIDER_NAME);
    }
    
    @Override
    public CertStore createCertStore(String certStoreType, CertStoreParameters certStoreParameters) 
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException 
    {
        return CertStore.getInstance(certStoreType, certStoreParameters, PROVIDER_NAME);
    }

    @Override
    public Cipher createCipher(String algorithm) 
    throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException
    {
        return Cipher.getInstance(algorithm, PROVIDER_NAME);
    }

    @Override
    public RandomGenerator createRandomGenerator()
	throws NoSuchAlgorithmException, NoSuchProviderException
	{
    	return new RandomGeneratorImpl();
	}
    
    @Override
    public Mac createMAC(String algorithm)
	throws NoSuchAlgorithmException, NoSuchProviderException
	{
    	return Mac.getInstance(algorithm, PROVIDER_NAME);
	}
    
    @Override
    public X509CRLBuilder createX509CRLBuilder() {
        return new X509CRLBuilderImpl(PROVIDER_NAME, PROVIDER_NAME);
    }
    
    @Override
    public String getNonSensitiveProvider() {
        return PROVIDER_NAME;
    }

    @Override
    public String getSensitiveProvider() {
        return PROVIDER_NAME;
    }    
}
