/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.bouncycastle;

import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mitm.common.util.CollectionUtils;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.util.CollectionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Store implementation that stores X509Certificate as X509CertificateHolder's
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CertificateHolderStore extends CollectionStore
{
    private final static Logger logger = LoggerFactory.getLogger(X509CertificateHolderStore.class);
    
    public X509CertificateHolderStore(Collection<? extends Certificate> certificates)
    {
        super(convert(certificates));
    }
    
    private static Collection<X509CertificateHolder> convert(Collection<? extends Certificate> certificates)
    {
        List<X509CertificateHolder> holders = new ArrayList<X509CertificateHolder>(CollectionUtils.
                getSize(certificates));
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                try {
                    holders.add(new X509CertificateHolder(certificate.getEncoded()));
                } 
                catch (CertificateEncodingException e) {
                    logger.warn("Error encoding certificate. Skipping certificate", e);
                } 
                catch (IOException e) {
                    logger.warn("Error encoding certificate. Skipping certificate", e);
                }
            }
        }
        
        return holders;
    }
}
