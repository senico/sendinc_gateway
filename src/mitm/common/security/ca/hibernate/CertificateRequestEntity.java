/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.hibernate;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.security.auth.x500.X500Principal;

import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.KeyEncoder;
import mitm.common.security.KeyEncoderException;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;

import org.hibernate.annotations.Index;

@Entity(name = CertificateRequestEntity.ENTITY_NAME)
@Table(name = "certificateRequest")
@org.hibernate.annotations.Table(appliesTo = CertificateRequestEntity.ENTITY_NAME, indexes = {
        @Index(name = "certificateRequest_next_update_index", columnNames = {CertificateRequestEntity.NEXT_UPDATE_COLUMN_NAME}),
        @Index(name = "certificateRequest_created_index", columnNames = {CertificateRequestEntity.CREATED_COLUMN_NAME}),
        @Index(name = "certificateRequest_email_index", columnNames = {CertificateRequestEntity.EMAIL_COLUMN_NAME})
        }
)
public class CertificateRequestEntity implements CertificateRequest
{
    protected static final String ENTITY_NAME = "CertificateRequest";

    protected static final String ID_COLUMN_NAME = "id";
    protected static final String EMAIL_COLUMN_NAME = "email";
    protected static final String CREATED_COLUMN_NAME = "created";
    protected static final String NEXT_UPDATE_COLUMN_NAME = "nextUpdate";
    
    /**
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255; 

    private static final int MAX_INFO_LENGTH = 1024; 
    private static final int MAX_MESSAGE_LENGTH = 1024; 
    private static final int MAX_CRL_DIST_POINT_LENGTH = 1024; 
    
    @Id
    @Column(name = ID_COLUMN_NAME)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = CREATED_COLUMN_NAME, nullable = false)
    private Date created;
    
    @Column
    private X500Principal subject;
    
    @Column (name = EMAIL_COLUMN_NAME, length = MAX_EMAIL_LENGTH)
    private String email;
    
    @Column
    private int validity;
    
    @Column
    private String signatureAlgorithm;

    @Column
    private int keyLength;
    
    @Column(length = MAX_CRL_DIST_POINT_LENGTH)
    private String crlDistPoint;
    
    @Column
    private byte[] publicKey;
    
    @Column
    private byte[] privateKey;
    
    @Column
    private String certificateHandlerName;

    @Column(length = MAX_INFO_LENGTH)
    private String info;
    
    @Column
    private byte[] data;

    @Column
    private int iteration;    
    
    @Column
    private Date lastUpdated;

    @Column(name = NEXT_UPDATE_COLUMN_NAME)
    private Date nextUpdate;
    
    @Column(length = MAX_MESSAGE_LENGTH)
    private String lastMessage;
    
    protected CertificateRequestEntity() {
        /* required by Hibernate */
    }

    public CertificateRequestEntity(String certificateHandlerName) {
        this(certificateHandlerName, new Date());
    }
    
    protected CertificateRequestEntity(String certificateHandlerName, Date created)
    {
        Check.notNull(certificateHandlerName, "certificateHandlerName");
        
        this.certificateHandlerName = certificateHandlerName;
        this.created = created;
    }

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public Date getCreated() {
        return created;
    }
    
    @Override
    public void setSubject(X500Principal subject) {
        this.subject = subject;
    }
    
    @Override
    public X500Principal getSubject() {
        return subject;
    }

    @Override
    public void setEmail(String email) {
        this.email = EmailAddressUtils.canonicalize(email);
    }
    
    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setValidity(int days) {
        this.validity = days;
    }
    @Override
    public int getValidity() {
        return validity;
    }

    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
    
    @Override
    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    @Override
    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }
    
    @Override
    public int getKeyLength() {
        return keyLength;
    }
    
    @Override
    public void setCRLDistributionPoint(String crlDistPoint) {
        this.crlDistPoint = crlDistPoint;
    }
    
    @Override
    public String getCRLDistributionPoint() {
        return crlDistPoint;
    }
    
    @Override
    public String getCertificateHandlerName() {
        return certificateHandlerName;
    }
    
    @Override
    public void setInfo(String info) {
        this.info = info;
    }
    
    @Override
    public String getInfo() {
        return info;
    }
    
    @Override
    public void setData(byte[] data) {
        this.data = data;
    }
    
    @Override
    public byte[] getData() {
        return data;
    }

    @Override
    public void setIteration(int iteration) {
        this.iteration = iteration;
    }
    
    @Override
    public int getIteration() {
        return iteration;
    }
    
    @Override
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setNextUpdate(Date nextUpdate) {
        this.nextUpdate = nextUpdate;
    }
    
    @Override
    public Date getNextUpdate() {
        return nextUpdate;
    }
    
    @Override
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
    
    @Override
    public String getLastMessage() {
        return lastMessage;
    }

    @Override
    public void setKeyPair(KeyPair keyPair, Encryptor encryptor)
    throws KeyEncoderException
    {
        if (keyPair != null)
        {
            publicKey = KeyEncoder.encode(keyPair.getPublic(), encryptor); 
            privateKey = KeyEncoder.encode(keyPair.getPrivate(), encryptor); 
        }
        else {
            publicKey = null;
            privateKey = null;
        }
    }
    
    @Override
    public KeyPair getKeyPair(Encryptor encryptor)
    throws KeyEncoderException
    {
        PublicKey publicKey = this.publicKey != null ? (PublicKey) KeyEncoder.decode(this.publicKey, encryptor) : null;
        
        PrivateKey privateKey = this.privateKey != null ? (PrivateKey) KeyEncoder.decode(this.privateKey, encryptor) : null;
        
        return publicKey != null || privateKey != null ? new KeyPair(publicKey, privateKey) : null;
    }
}
