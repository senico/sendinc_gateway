/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.hibernate;

import java.util.Date;
import java.util.List;

import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.ca.CertificateRequestStore;
import mitm.common.security.ca.Match;
import mitm.common.util.Check;

/**
 * CertificateRequestStore implementation that stores the requests in the database. This implementation requires that
 * the CertificateRequest is-a CertificateRequestEnity (see CertificateRequestDAO for more info).
 * 
 * @author Martijn Brinkers
 *
 */
public class CertificateRequestStoreImpl implements CertificateRequestStore
{
    /*
     * Handles the database session state.
     */
    private final SessionManager sessionManager;
    
    public CertificateRequestStoreImpl(SessionManager sessionManager)
    {
        Check.notNull(sessionManager, "sessionManager");
        
        this.sessionManager = sessionManager;
    }

    protected CertificateRequest getNextRequest(Date date) {
        return getDAO().getNextRequest(date);
    }
    
    @Override
    public CertificateRequest getNextRequest() {
        return getNextRequest(new Date());
    }
    
    @Override
    public void addRequest(CertificateRequest request) {
        getDAO().addRequest(request);
    }

    @Override
    public CertificateRequest getRequest(Long id) {
        return getDAO().findById(id);
    }

    @Override
    public void deleteRequest(Long id) {
        getDAO().deleteRequest(id);
    }
    
    @Override
    public List<? extends CertificateRequest> getRequestsByEmail(String email, Match match, Integer firstResult, 
            Integer maxResults)
    {
        return getDAO().getRequestsByEmail(email, match, firstResult, maxResults);
    }

    @Override
    public List<? extends CertificateRequest> getAllRequests(Integer firstResult, Integer maxResults) {
        return getDAO().getAllRequests(firstResult, maxResults);
    }
    
    @Override
    public int getSize() {
        return getDAO().getSize();
    }

    @Override
    public int getSizeByEmail(String email, Match match) {
        return getDAO().getSizeByEmail(email, match);
    }
    
    private CertificateRequestDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new CertificateRequestDAO(session);
    }
}
