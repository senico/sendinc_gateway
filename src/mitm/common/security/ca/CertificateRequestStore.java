/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import java.util.List;

/**
 * The CertificateRequestStore will store pending certificate requests.
 * 
 * @author Martijn Brinkers
 *
 */
public interface CertificateRequestStore
{
    /**
     * Returns a CertificateRequest from the store which should be handled. Typically the request with the oldest
     * nextUpdate will be returned. The caller is responsible for updating the nextUpdate value. If not the same
     * request wil be returned the next call.
     */
    public CertificateRequest getNextRequest();
    
    /**
     * Adds a new certificate request to the store 
     */
    public void addRequest(CertificateRequest request);

    /**
     * Returns the certificate request with the given ID. Null if no request found with ID.
     */
    public CertificateRequest getRequest(Long id);

    /**
     * Removed the certificate request with the given ID.
     */
    public void deleteRequest(Long id);
    
    /**
     * Returns all the pending certificate requests for the given email address
     */
    public List<? extends CertificateRequest> getRequestsByEmail(String email, Match match, Integer firstResult, 
            Integer maxResults);

    /**
     * Returns all the pending certificate requests
     */
    public List<? extends CertificateRequest> getAllRequests(Integer firstResult, Integer maxResults);
    
    /**
     * Returns the number of certificate requests
     */
    public int getSize();

    /**
     * Returns the number of certificate requests with the given email
     */
    public int getSizeByEmail(String email, Match match);
}
