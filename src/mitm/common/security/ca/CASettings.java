/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import mitm.common.properties.HierarchicalPropertiesException;

public interface CASettings
{
    /**
     * The thumbprint of the certificate that will sign the issued certificates.
     */
    public void setSignerThumbprint(String thumbprint)
    throws HierarchicalPropertiesException;
 
    public String getSignerThumbprint()
    throws HierarchicalPropertiesException;

    /**
     * The default common name used when issuing certificates
     */
    public void setDefaultCommonName(String commonName)
    throws HierarchicalPropertiesException;

    public String getDefaultCommonName()
    throws HierarchicalPropertiesException;
    
    /**
     * The default number of days the certificate should be valid. Must be > 0.
     */
    public void setValidity(Integer days)
    throws HierarchicalPropertiesException;

    public Integer getValidity()
    throws HierarchicalPropertiesException;
    
    /**
     * The default length of the RSA key in bites (must >= 1024)
     */
    public void setKeyLength(Integer length)
    throws HierarchicalPropertiesException;

    public Integer getKeyLength()
    throws HierarchicalPropertiesException;
    
    /**
     * The default signature algorithm
     */
    public void setSignatureAlgorithm(String signatureAlgorithm)
    throws HierarchicalPropertiesException;

    public String getSignatureAlgorithm()
    throws HierarchicalPropertiesException;
    
    /**
     * The email address of messages sent by CA
     */
    public void setCAEmail(String email)
    throws HierarchicalPropertiesException;

    public String getCAEmail()
    throws HierarchicalPropertiesException;
    
    /**
     * The random generated password length in bytes
     */
    public void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException;

    public Integer getPasswordLength()
    throws HierarchicalPropertiesException;

    /**
     * The CRL distributionpoint added to the end user certificate
     */
    public void setCRLDistributionPoint(String url)
    throws HierarchicalPropertiesException;
    
    public String getCRLDistributionPoint()
    throws HierarchicalPropertiesException;
    
    /**
     * If true the CRL distributionpoint (if available) is added to the end user certificate
     */
    public void setAddCRLDistributionPoint(boolean add)
    throws HierarchicalPropertiesException;

    public boolean isAddCRLDistributionPoint()
    throws HierarchicalPropertiesException;
    
    /**
     * If true the PFX password will be stored for the user
     */
    public void setStorePFXPassword(boolean store)
    throws HierarchicalPropertiesException;        

    public boolean isStorePFXPassword()
    throws HierarchicalPropertiesException;    
    
    /**
     * The default certificate request handler to use 
     */
    public void setCertificateRequestHandler(String certificateRequestHandler)
    throws HierarchicalPropertiesException;
    
    public String getCertificateRequestHandler()
    throws HierarchicalPropertiesException;
}
