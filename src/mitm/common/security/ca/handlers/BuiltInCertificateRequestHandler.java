/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;

import org.apache.commons.lang.StringUtils;

import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.ca.CAException;
import mitm.common.security.ca.CASettings;
import mitm.common.security.ca.CASettingsProvider;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.ca.CertificateRequestHandler;
import mitm.common.security.ca.CertificateRequestHandlerRegistry;
import mitm.common.security.ca.RequestParameters;
import mitm.common.security.ca.RequestParametersImpl;
import mitm.common.security.ca.KeyAndCertificateIssuer;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;

/**
 * A CertificateRequestHandler implementation that uses the built-in CA server to request a new certificate.
 * 
 * @author Martijn Brinkers
 *
 */
public class BuiltInCertificateRequestHandler implements CertificateRequestHandler
{
    public final static String NAME = "built-in";
    
    /*
     * Used for getting the signing certificate
     */
    private final KeyAndCertStore keyAndCertStore;
    
    /*
     * Used to get the CASettings
     */
    private final CASettingsProvider settingsProvider;
    
    /*
     * Used for issuing certificates
     */
    private final KeyAndCertificateIssuer keyAndCertificateIssuer;
    
    public BuiltInCertificateRequestHandler(CertificateRequestHandlerRegistry registry, KeyAndCertStore keyAndCertStore, 
            CASettingsProvider settingsProvider, KeyAndCertificateIssuer keyAndCertificateIssuer)
    {
        Check.notNull(keyAndCertStore, "keyAndCertStore");
        Check.notNull(settingsProvider, "settingsProvider");
        Check.notNull(keyAndCertificateIssuer, "keyAndCertificateIssuer");
        
        this.keyAndCertStore = keyAndCertStore;
        this.settingsProvider = settingsProvider;
        this.keyAndCertificateIssuer = keyAndCertificateIssuer;

        if (registry != null) {
            registry.registerHandler(this);
        }
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @Override
    public boolean isInstantlyIssued() {
        return true;
    }
    
    @Override
    public String getCertificateHandlerName() {
        return NAME;
    }
    
    private KeyAndCertificate getSigner()
    throws HierarchicalPropertiesException, CertStoreException, KeyStoreException, CAException
    {
        KeyAndCertificate signer = null;

        CASettings settings = settingsProvider.getSettings();
    
        String thumbprint = settings.getSignerThumbprint();
        
        if (StringUtils.isBlank(thumbprint)) {
            throw new CAException("There is no CA specified.");
        }
        
        X509CertStoreEntry storeEntry = keyAndCertStore.getByThumbprint(thumbprint);
        
        if (storeEntry != null) {
            signer = keyAndCertStore.getKeyAndCertificate(storeEntry);
        }
        
        return signer;
    }

    private RequestParameters toRequestParameters(CertificateRequest request)
    {
        RequestParameters requestParameters = new RequestParametersImpl();
        
        requestParameters.setSubject(request.getSubject());
        requestParameters.setEmail(request.getEmail());
        requestParameters.setValidity(request.getValidity());
        requestParameters.setKeyLength(request.getKeyLength());
        requestParameters.setSignatureAlgorithm(request.getSignatureAlgorithm());
        requestParameters.setCRLDistributionPoint(request.getCRLDistributionPoint());
        
        return requestParameters;
    }
    
    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException
    {
        try {
            KeyAndCertificate signer = getSigner();
            
            if (signer == null || signer.getPrivateKey() == null || signer.getCertificate() == null) {
                throw new CAException("CA signer not found.");
            }
            
            KeyAndCertificate issued = keyAndCertificateIssuer.issueKeyAndCertificate(
                    toRequestParameters(request), signer);
                        
            return issued;
        }
        catch(HierarchicalPropertiesException e) {
            throw new CAException(e);
        }
        catch (CertStoreException e) {
            throw new CAException(e);
        }
        catch (KeyStoreException e) {
            throw new CAException(e);
        }
    }
}
