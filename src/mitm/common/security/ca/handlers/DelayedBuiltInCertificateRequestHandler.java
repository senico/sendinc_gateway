/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;

import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.ca.CAException;
import mitm.common.security.ca.CASettings;
import mitm.common.security.ca.CASettingsProvider;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.ca.CertificateRequestHandler;
import mitm.common.security.ca.CertificateRequestHandlerRegistry;
import mitm.common.security.ca.RequestParameters;
import mitm.common.security.ca.RequestParametersImpl;
import mitm.common.security.ca.KeyAndCertificateIssuer;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of a CertificateRequestHandler does not immediately issue a certificate. The certificate takes a 
 * specified number of steps before it is issued.
 * 
 * @author Martijn Brinkers
 *
 */
public class DelayedBuiltInCertificateRequestHandler implements CertificateRequestHandler
{
    private final static Logger logger = LoggerFactory.getLogger(DelayedBuiltInCertificateRequestHandler.class);
    
    public final static String NAME = "delayed built-in";
    
    /*
     * Used for getting the signing certificate
     */
    private final KeyAndCertStore keyAndCertStore;
    
    /*
     * Used to get the CASettings
     */
    private final CASettingsProvider settingsProvider;
    
    /*
     * Used for issuing certificates
     */
    private final KeyAndCertificateIssuer keyAndCertificateIssuer;
    
    /*
     * Number of steps it takes to issue a certificate
     */
    private final int steps;
    
    /*
     * Will be used to persistently store data in the CertificateRequest 
     */
    private static class DataWrapper
    {
        private final static int VERSION = 1;
        
        /*
         * The current step
         */
        private int step;
        
        public static byte[] encode(DataWrapper wrapper)
        throws IOException
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream output = new ObjectOutputStream(bos);
        
            output.writeInt(VERSION);
            output.writeInt(wrapper.step);
            output.close();
            
            return bos.toByteArray();
        }
        
        public static DataWrapper decode(byte[] encoded)
        throws IOException
        {
            DataWrapper wrapper = new DataWrapper();
            
            if (encoded == null) {
                return wrapper;
            }
            
            ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(encoded));
            
            int version = input.readInt();
            
            if (version != VERSION)
            {
                throw new IOException("Unexpected version. Expected " + VERSION + 
                        " but got " + version);
            }
            
            wrapper.step = input.readInt();
            
            return wrapper;
        }
    }
    
    public DelayedBuiltInCertificateRequestHandler(CertificateRequestHandlerRegistry registry, KeyAndCertStore keyAndCertStore, 
            CASettingsProvider settingsProvider, KeyAndCertificateIssuer keyAndCertificateIssuer, int steps)
    {
        Check.notNull(keyAndCertStore, "keyAndCertStore");
        Check.notNull(settingsProvider, "settingsProvider");
        Check.notNull(keyAndCertificateIssuer, "keyAndCertificateIssuer");
        
        this.keyAndCertStore = keyAndCertStore;
        this.settingsProvider = settingsProvider;
        this.keyAndCertificateIssuer = keyAndCertificateIssuer;
        this.steps = steps;

        if (registry != null) {
            registry.registerHandler(this);
        }
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @Override
    public boolean isInstantlyIssued() {
        return false;
    }
    
    @Override
    public String getCertificateHandlerName() {
        return NAME;
    }
    
    private KeyAndCertificate getSigner()
    throws HierarchicalPropertiesException, CertStoreException, KeyStoreException, CAException
    {
        KeyAndCertificate signer = null;

        CASettings settings = settingsProvider.getSettings();
    
        String thumbprint = settings.getSignerThumbprint();
        
        if (StringUtils.isBlank(thumbprint)) {
            throw new CAException("There is no CA specified.");
        }
        
        X509CertStoreEntry storeEntry = keyAndCertStore.getByThumbprint(thumbprint);
        
        if (storeEntry != null) {
            signer = keyAndCertStore.getKeyAndCertificate(storeEntry);
        }
        
        return signer;
    }

    private RequestParameters toIssueParameters(CertificateRequest request)
    {
        RequestParameters issueParameters = new RequestParametersImpl();
        
        issueParameters.setSubject(request.getSubject());
        issueParameters.setEmail(request.getEmail());
        issueParameters.setValidity(request.getValidity());
        issueParameters.setKeyLength(request.getKeyLength());
        issueParameters.setSignatureAlgorithm(request.getSignatureAlgorithm());
        issueParameters.setCRLDistributionPoint(request.getCRLDistributionPoint());
        
        return issueParameters;
    }
    
    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException
    {
        try {
            /*
             * Get the data out of the general data store
             */
            DataWrapper extraInfo = DataWrapper.decode(request.getData());

            logger.info("Certificate request at step " + extraInfo.step);
                        
            /*
             * Wait X steps to actually issue the certificate
             */
            if (extraInfo.step < steps)
            {
                request.setLastMessage("Finished step " + extraInfo.step);
                /*
                 * Store the data back into the request and update the message
                 */
                extraInfo.step++;
                request.setData(DataWrapper.encode(extraInfo));
                
                /*
                 * We are not yet finished so return null
                 */
                return null;
            }
            
            KeyAndCertificate signer = getSigner();
            
            if (signer == null || signer.getPrivateKey() == null || signer.getCertificate() == null) {
                throw new CAException("CA signer not found.");
            }
            
            KeyAndCertificate issued = keyAndCertificateIssuer.issueKeyAndCertificate(
                    toIssueParameters(request), signer);
                        
            return issued;
        }
        catch(HierarchicalPropertiesException e) {
            throw new CAException(e);
        }
        catch (CertStoreException e) {
            throw new CAException(e);
        }
        catch (KeyStoreException e) {
            throw new CAException(e);
        }
        catch (IOException e) {
            throw new CAException(e);
        }
    }
}
