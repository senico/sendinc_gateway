/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.ejbca;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.TrustAllX509TrustManager;
import mitm.common.security.ca.CAException;
import mitm.common.security.ca.CertificateRequest;
import mitm.common.security.ca.CertificateRequestHandler;
import mitm.common.security.ca.CertificateRequestHandlerRegistry;
import mitm.common.security.ca.handlers.ejbca.ws.ApprovalException_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.AuthorizationDeniedException_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.CADoesntExistsException_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.CertificateResponse;
import mitm.common.security.ca.handlers.ejbca.ws.EJBCAConst;
import mitm.common.security.ca.handlers.ejbca.ws.EjbcaException_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.EjbcaWS;
import mitm.common.security.ca.handlers.ejbca.ws.NotFoundException_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.UserDataVOWS;
import mitm.common.security.ca.handlers.ejbca.ws.UserDoesntFullfillEndEntityProfile_Exception;
import mitm.common.security.ca.handlers.ejbca.ws.WaitingForApprovalException_Exception;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.X500PrincipalUtils;
import mitm.common.util.Base64Utils;
import mitm.common.util.Check;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CertificateRequestHandler implementation that requests certificates from an EJBA server.
 * 
 * @author Martijn Brinkers
 *
 */
public class EJBCACertificateRequestHandler implements CertificateRequestHandler 
{
    private final static Logger logger = LoggerFactory.getLogger(EJBCACertificateRequestHandler.class);
    
    public static final String NAME = "EJBCA";
    
    /*
     * The EJBCACertificateRequestHandlerSettings
     */
    private final EJBCACertificateRequestHandlerSettings requestHandlerSettings;
    
    /*
     * Factory used to create security object instances
     */
    private final SecurityFactory securityFactory;
    
    /*
     * Used by the KeyPairGenerator
     * 
     * Note: SecureRandom is not thread safe.
     */
    private final SecureRandom randomSource;

    /*
     * Mapping from OID to name which is used to convert a DN to string. EJBCA seems to require certain
     * DN names to be names and not OID's.
     */
    private final Map<String, String> oidMapping = new HashMap<String, String>();
    
    /*
     * The EJBCA web service client
     */
    private EjbcaWS ejbcaWS;
    
    public EJBCACertificateRequestHandler(EJBCACertificateRequestHandlerSettings requestHandlerSettings,
    		CertificateRequestHandlerRegistry registry)
    throws CAException
    {
    	Check.notNull(requestHandlerSettings, "requestHandlerSettings");
    	Check.notNull(registry, "registry");
    	
    	this.requestHandlerSettings = requestHandlerSettings;
    	
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        try {
            randomSource = securityFactory.createSecureRandom();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CAException("Error creating secure random.");
        }
        catch (NoSuchProviderException e) {
            throw new CAException("Error creating secure random.");
        }
        
        if (registry != null) {
            registry.registerHandler(this);
        }
        
        oidMapping.put("2.5.4.42", "givenName");
        oidMapping.put("2.5.4.4", "surname");
        oidMapping.put("1.2.840.113549.1.9.1", "emailAddress");
    }

    @Override
    public boolean isEnabled()
    {
        boolean enabled = requestHandlerSettings.isEnabled();
        
        if (enabled)
        {
            /*
             * If enabled, check if certain required settings are set
             */
            try {
                if (requestHandlerSettings.getWebServiceURL() == null || 
                        requestHandlerSettings.getKeyStore() == null)
                {
                    logger.warn("Not all required settings are set.");

                    enabled = false;
                }
            }
            catch (Exception e) {
                logger.error("Error checking isEnabled.", e);
            }
        }
        
        return enabled;
    }

    @Override
    public boolean isInstantlyIssued() {
        return true;
    }

    @Override
    public String getCertificateHandlerName() {
        return NAME;
    }

    /*
     * Note: Random source is not thread safe so we need to synchronize.
     */
    private synchronized KeyPair generateKeyPair(int keyLength)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
        
        keyPairGenerator.initialize(keyLength, randomSource);
        
        return keyPairGenerator.generateKeyPair();
    }
    
    private EjbcaWS getEjbcaWS()
    throws CAException
    {
    	if (ejbcaWS == null)
    	{
    	    try {
                JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        		
                factory.setServiceClass(EjbcaWS.class);
                factory.setAddress(requestHandlerSettings.getWebServiceURL().toExternalForm());
                factory.setServiceName(EJBCAConst.SERVICE_NAME);
                EjbcaWS localEjbcaWS = (EjbcaWS) factory.create();
                
                KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                        KeyManagerFactory.getDefaultAlgorithm());
    
                char[] password = requestHandlerSettings.getKeyStorePassword() != null ? 
                            requestHandlerSettings.getKeyStorePassword().toCharArray() : null;
                
                keyManagerFactory.init(requestHandlerSettings.getKeyStore(), password);
    
                KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
    
                Client proxy = ClientProxy.getClient(localEjbcaWS);
                                
                TLSClientParameters tlsClientParameters = new TLSClientParameters();

                tlsClientParameters.setDisableCNCheck(requestHandlerSettings.isDisableCNCheck());

                if (requestHandlerSettings.isSkipCertificateCheck())
                {
                    /*
                     * Use a TrustManager that skips all checks 
                     */
                    tlsClientParameters.setTrustManagers(new TrustManager[]{new TrustAllX509TrustManager()});
                }
                else {
                    KeyStore trustStore = requestHandlerSettings.getTrustStore();
                    
                    if (trustStore != null)
                    {
                        /*
                         * Use the provided trust store
                         */
                        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                                TrustManagerFactory.getDefaultAlgorithm());
                        
                        trustManagerFactory.init(trustStore);
                        
                        tlsClientParameters.setTrustManagers(trustManagerFactory.getTrustManagers());
                    }
                }

                tlsClientParameters.setKeyManagers(keyManagers);
                
                HTTPConduit conduit = (HTTPConduit) proxy.getConduit();

                conduit.setTlsClientParameters(tlsClientParameters);         

                ejbcaWS = localEjbcaWS;
    	    }
    	    catch (NoSuchAlgorithmException e) {
    	        throw new CAException(e);
    	    } 
    	    catch (UnrecoverableKeyException e) {
                throw new CAException(e);
            } 
    	    catch (KeyStoreException e) {
                throw new CAException(e);
            }
    	}
    	
    	return ejbcaWS;
    }
    
    private String subjectDNToString(X500Principal subject) {
        return subject.getName(X500Principal.RFC1779, oidMapping);
    }
    
    private ContentSigner getContentSigner(String signatureAlgorithm, PrivateKey privateKey)
    throws OperatorCreationException
    {
        JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(signatureAlgorithm);
        
        contentSignerBuilder.setProvider(securityFactory.getSensitiveProvider());
        
        return contentSignerBuilder.build(privateKey);
    }
    
    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException
    {
        KeyAndCertificate keyAndCertificate = null;
        
        try {
            UserDataVOWS userData = new UserDataVOWS();
            
            userData.setEmail(request.getEmail());
            userData.setUsername(request.getEmail());
            userData.setPassword(requestHandlerSettings.getDefaultUserPassword());
            userData.setSubjectDN(subjectDNToString(request.getSubject()));
            userData.setSubjectAltName("rfc822Name=" + request.getEmail());
            userData.setEndEntityProfileName(requestHandlerSettings.getEndEntityProfileName());
            userData.setCaName(requestHandlerSettings.getCAName());
            userData.setCertificateProfileName(requestHandlerSettings.getCertificateProfileName());
            userData.setStatus(EJBCAConst.STATUS_NEW);
            userData.setTokenType(EJBCAConst.TOKEN_TYPE_USERGENERATED);
                        
            if (logger.isDebugEnabled())
            {
                StrBuilder sb = new StrBuilder();
                
                sb.append("UserDataVOWS: ").
                    append("[Email: ").append(userData.getEmail()).
                    append(", DefaultUserPassword: ").append(userData.getPassword() != null ? "***" : "").
                    append(", SubjectDN: ").append(userData.getSubjectDN()).
                    append(", SubjectAltName: ").append(userData.getSubjectAltName()).
                    append(", EndEntityProfileName: ").append(userData.getEndEntityProfileName()).
                    append(", CaName: ").append(userData.getCaName()).
                    append(", CertificateProfileName: ").append(userData.getCertificateProfileName()).
                    append("]");
                
                logger.debug(sb.toString());
            }
            
            KeyPair keyPair = generateKeyPair(request.getKeyLength());

            PKCS10CertificationRequestBuilder requestBuilder = new PKCS10CertificationRequestBuilder(
                    X500PrincipalUtils.toX500Name(request.getSubject()), SubjectPublicKeyInfo.getInstance(
                    keyPair.getPublic().getEncoded()));
            
            PKCS10CertificationRequest pkcs10 = requestBuilder.build(getContentSigner("SHA1WithRSA", 
                    keyPair.getPrivate()));
            
            String base64PKCS10 = Base64Utils.encode(pkcs10.getEncoded());

            CertificateResponse certificateResponse;
            
            /*
             * Since were not sure whether the WS proxy is thread safe, we will synchronize on this
             */
            synchronized (this)
            {                
                certificateResponse = getEjbcaWS().certificateRequest(userData,
                        base64PKCS10,
                        EJBCAConst.CERT_REQ_TYPE_PKCS10,
                        null,
                        EJBCAConst.RESPONSETYPE_CERTIFICATE);
            }
            
            if (certificateResponse != null && certificateResponse.getData() != null)
            {
                /*
                 * The result is a base64 encoded certificate 
                 */
                Collection<X509Certificate> certificates = CertificateUtils.readX509Certificates(
                        new ByteArrayInputStream(Base64.decode(certificateResponse.getData())));
    
                if (CollectionUtils.isNotEmpty(certificates))
                {
                    X509Certificate generatedCertificate = null;
                    
                    for (X509Certificate certificate : certificates)
                    {
                        if (certificate != null)
                        {
                            /*
                             * Use the first one (EJBCA probably will always return just one certificate)
                             */
                            generatedCertificate = certificate;
                            
                            break;
                        }
                    }
                    
                    if (generatedCertificate != null)
                    {
                        keyAndCertificate = new KeyAndCertificateImpl(keyPair.getPrivate(), 
                                generatedCertificate);
                    }
                    else {
                        logger.warn("No certificates found");
                    }
                }
                else {
                    logger.warn("No certificates found");
                }
            }
            else {
                logger.warn("certificateResponse is empty");
            }
                        
            return keyAndCertificate;
        } 
        catch (NoSuchAlgorithmException e) {
            throw new CAException("Error requesting a certificate", e);
        } 
        catch (NoSuchProviderException e) {
            throw new CAException("Error requesting a certificate", e);
        } 
        catch (CADoesntExistsException_Exception e) {
            throw new CAException("CA doesn't exist.", e);
        } 
        catch (UserDoesntFullfillEndEntityProfile_Exception e) {
            throw new CAException("User doesn't fullfill end entity profile.", e);
        } 
        catch (EjbcaException_Exception e) {
            throw new CAException("EJBCA exception.", e);
        } 
        catch (AuthorizationDeniedException_Exception e) {
            throw new CAException("Authorization denied.", e);
        } 
        catch (WaitingForApprovalException_Exception e) {
            throw new CAException("Waiting for approval.", e);
        } 
        catch (ApprovalException_Exception e) {
            throw new CAException("Approval exception.", e);
        } 
        catch (NotFoundException_Exception e) {
            throw new CAException("Not found exception.", e);
        } 
        catch (CertificateException e) {
            throw new CAException("Certificate exception.", e);
        } 
        catch (Base64DecodingException e) {
            throw new CAException("Error Base64 decoding.", e);
        } 
        catch (OperatorCreationException e) {
            throw new CAException("Error creating a PKCS#10 request.", e);
        } 
        catch (IOException e) {
            throw new CAException("Error requesting a certificate", e);
        } 
    }
}
