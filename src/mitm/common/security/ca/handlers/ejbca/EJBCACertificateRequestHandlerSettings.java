/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.ejbca;

import java.net.URL;
import java.security.KeyStore;

import mitm.common.security.ca.CAException;

public interface EJBCACertificateRequestHandlerSettings
{
    /**
     * If true, the EJBCA certificate request handler will be enabled.
     */
    public void setEnabled(boolean enabled);
    public boolean isEnabled();
    
	/**
	 * The URL of the EJBCA web service
	 */
	public void setWebServiceURL(URL url);
	public URL getWebServiceURL();
	
	/**
	 * If true, all peer SSL/TLS certificates are blindly accepted (i.e.,
	 * no certificate check is done)
	 */
	public void setSkipCertificateCheck(boolean skip);
	public boolean isSkipCertificateCheck();
	
	/**
	 * If set, the common name of the SSL/TLS certificate is not checked 
	 */
	public void setDisableCNCheck(boolean disable);
	public boolean isDisableCNCheck();
	
	/**
	 * The EJBCA CA to use for the request
	 */
	public void setCAName(String name);
	public String getCAName();
	
	/**
	 * The EJBCA end entity profile to use for the request
	 */
	public void setEndEntityProfileName(String name);
	public String getEndEntityProfileName();

	/**
	 * The EJBCA certificate profile to use for the request
	 */
	public void setCertificateProfileName(String name);
	public String getCertificateProfileName();

	/**
	 * The default password to use for a new user added to EJBCA. Set to null (the default) if
	 * no password should be used.
	 */
	public void setDefaultUserPassword(String password);
	public String getDefaultUserPassword();
	
	/**
	 * The KeyStore containing the client side SSL/TLS key which is required by EJBCA. 
	 */
	public KeyStore getKeyStore()
	throws CAException;
	
	/**
	 * Password for the KeyStore key entries 
	 */
	public String getKeyStorePassword();
	
    /**
     * The KeyStore containing the trusted certificates 
     */
    public KeyStore getTrustStore()
    throws CAException;
    
    /**
     * Password for the KeyStore key entries 
     */
    public String getTrustStorePassword();
}
