package mitm.common.security.ca.handlers.ejbca.ws;

import javax.xml.namespace.QName;

public class EJBCAConst
{
	public static final QName SERVICE_NAME = new QName("http://ws.protocol.core.ejbca.org/", "EjbcaWSService");
	
    public static final java.lang.String TOKEN_TYPE_USERGENERATED = "USERGENERATED"; 
    public static final java.lang.String TOKEN_TYPE_JKS           = "JKS";
    public static final java.lang.String TOKEN_TYPE_PEM           = "PEM";
    public static final java.lang.String TOKEN_TYPE_P12           = "P12";
    
    public static final int STATUS_NEW = 10;        // New user
    public static final int STATUS_FAILED = 11;     // Generation of user certificate failed
    public static final int STATUS_INITIALIZED = 20;// User has been initialized
    public static final int STATUS_INPROCESS = 30;  // Generation of user certificate in process
    public static final int STATUS_GENERATED = 40;  // A certificate has been generated for the user
    public static final int STATUS_REVOKED = 50;  // The user has been revoked and should not have any more certificates issued
    public static final int STATUS_HISTORICAL = 60; // The user is old and archived
    public static final int STATUS_KEYRECOVERY  = 70; // The user is should use key recovery functions in next certificate generation.
    
    /**
     * Indicates that the requester want a BASE64 encoded certificate in the CertificateResponse object.
     */
    public static String RESPONSETYPE_CERTIFICATE    = "CERTIFICATE";
    /**
     * Indicates that the requester want a BASE64 encoded pkcs7 in the CertificateResponse object.
     */
    public static String RESPONSETYPE_PKCS7          = "PKCS7";
    /**
     * Indicates that the requester want a BASE64 encoded pkcs7 with the complete chain in the CertificateResponse object.
     */
    public static String RESPONSETYPE_PKCS7WITHCHAIN = "PKCS7WITHCHAIN";
    
    /**
     * Request data types for WS
     */
    public static final int CERT_REQ_TYPE_PKCS10    = 0;
    public static final int CERT_REQ_TYPE_CRMF      = 1;
    public static final int CERT_REQ_TYPE_SPKAC     = 2;
    public static final int CERT_REQ_TYPE_PUBLICKEY = 3;    
}
