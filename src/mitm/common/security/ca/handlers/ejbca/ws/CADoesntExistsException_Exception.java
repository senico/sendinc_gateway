
package mitm.common.security.ca.handlers.ejbca.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.12
 * Fri Aug 19 17:26:02 CEST 2011
 * Generated source version: 2.2.12
 * 
 */

@WebFault(name = "CADoesntExistsException", targetNamespace = "http://ws.protocol.core.ejbca.org/")
public class CADoesntExistsException_Exception extends Exception {
    public static final long serialVersionUID = 20110819172602L;
    
    private mitm.common.security.ca.handlers.ejbca.ws.CADoesntExistsException caDoesntExistsException;

    public CADoesntExistsException_Exception() {
        super();
    }
    
    public CADoesntExistsException_Exception(String message) {
        super(message);
    }
    
    public CADoesntExistsException_Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public CADoesntExistsException_Exception(String message, mitm.common.security.ca.handlers.ejbca.ws.CADoesntExistsException caDoesntExistsException) {
        super(message);
        this.caDoesntExistsException = caDoesntExistsException;
    }

    public CADoesntExistsException_Exception(String message, mitm.common.security.ca.handlers.ejbca.ws.CADoesntExistsException caDoesntExistsException, Throwable cause) {
        super(message, cause);
        this.caDoesntExistsException = caDoesntExistsException;
    }

    public mitm.common.security.ca.handlers.ejbca.ws.CADoesntExistsException getFaultInfo() {
        return this.caDoesntExistsException;
    }
}
