/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.ejbca;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import mitm.common.security.ca.CAException;
import mitm.common.security.keystore.KeyStoreLoader;

/**
 * Static implementation of EJBCACertificateRequestHandlerSettings
 * 
 * @author martijn
 *
 */
public class StaticEJBCACertificateRequestHandlerSettings implements EJBCACertificateRequestHandlerSettings
{
    /*
     * Path to the key store file
     */
    private File keyStoreFile;

    /*
     * The key store type of the KeyStoreFile (PKCS12, JKS etc.). Leave null or empty to 'guess'
     * the type based on file extension
     */
    private String keyStoreType;

    /*
     * The password of the keyStoreFile
     */
    private String keyStorePassword;

    /*
     * Path to the trust store file
     */
    private File trustStoreFile;

    /*
     * The key store type of the TrustStoreFile (PKCS12, JKS etc.). Leave null or empty to 'guess'
     * the type based on file extension
     */
    private String trustStoreType;

    /*
     * The password of the TrustStoreFile
     */
    private String trustStorePassword;
    
    private boolean enabled;
	private URL webServiceURL;
	private boolean skipCertificateCheck;
	private boolean disableCNCheck;
	private String caName;
	private String endEntityProfileName;
    private String certificateProfileName;
    private String defaultUserPassword;
	
	/*
	 * KeyStore with the client key for EJBCA authentication
	 */
	private KeyStore keyStore;

    /*
     * KeyStore with the trusted issuers
     */
    private KeyStore trustStore;

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    @Override
    public boolean isEnabled() {
        return enabled;
    }
    
	@Override
    public void setWebServiceURL(URL url) {
		this.webServiceURL = url;
	}
	
    @Override
	public URL getWebServiceURL() {
		return webServiceURL;
	}
	
    @Override
	public void setSkipCertificateCheck(boolean skip) {
		this.skipCertificateCheck = skip;
	}
	
    @Override
	public boolean isSkipCertificateCheck() {
		return skipCertificateCheck;
	}
	
    @Override
    public void setDisableCNCheck(boolean disable) {
        this.disableCNCheck = disable;
    }

    @Override
    public boolean isDisableCNCheck() {
        return disableCNCheck;
    }
    
    @Override
	public void setCAName(String name) {
		this.caName = name;
	}
	
    @Override
	public String getCAName() {
		return caName;
	}
	
    @Override
	public void setEndEntityProfileName(String name) {
		this.endEntityProfileName = name;
	}
	
    @Override
	public String getEndEntityProfileName() {
		return endEntityProfileName;
	}

    @Override
	public void setCertificateProfileName(String name) {
		this.certificateProfileName = name;
	}
	
    @Override
	public String getCertificateProfileName() {
		return certificateProfileName;
	}

    @Override
    public void setDefaultUserPassword(String password) {
        this.defaultUserPassword = password;
        
    }

    @Override
    public String getDefaultUserPassword() {
        return defaultUserPassword;
    }
    
    @Override
    public synchronized KeyStore getKeyStore()
    throws CAException
    {
        try {
            if (keyStore == null)
            {
                KeyStoreLoader keyStoreLoader = new KeyStoreLoader();

                keyStoreLoader.setKeyStoreType(keyStoreType);
                
                char[] password = keyStorePassword != null ? keyStorePassword.toCharArray() : null; 
                
                keyStore = keyStoreLoader.loadKeyStore(keyStoreFile, password);
            }
            
            return keyStore;
        }
        catch (KeyStoreException e) {
            throw new CAException(e);
        } 
        catch (FileNotFoundException e) {
            throw new CAException(e);
        } 
        catch (NoSuchAlgorithmException e) {
            throw new CAException(e);
        } 
        catch (CertificateException e) {
            throw new CAException(e);
        } 
        catch (IOException e) {
            throw new CAException(e);
        } 
        catch (NoSuchProviderException e) {
            throw new CAException(e);
        }
    }

    @Override
    public String getKeyStorePassword() {
        return keyStorePassword;
    }

    @Override
    public synchronized KeyStore getTrustStore()
    throws CAException
    {
        try {
            if (trustStore == null && trustStoreFile != null)
            {
                KeyStoreLoader keyStoreLoader = new KeyStoreLoader();

                keyStoreLoader.setKeyStoreType(trustStoreType);
                
                char[] password = trustStorePassword != null ? trustStorePassword.toCharArray() : null; 
                
                trustStore = keyStoreLoader.loadKeyStore(trustStoreFile, password);
            }
            
            return trustStore;
        }
        catch (KeyStoreException e) {
            throw new CAException(e);
        } 
        catch (FileNotFoundException e) {
            throw new CAException(e);
        } 
        catch (NoSuchAlgorithmException e) {
            throw new CAException(e);
        } 
        catch (CertificateException e) {
            throw new CAException(e);
        } 
        catch (IOException e) {
            throw new CAException(e);
        } 
        catch (NoSuchProviderException e) {
            throw new CAException(e);
        }
    }
    
    @Override
    public String getTrustStorePassword() {
        return trustStorePassword;
    }
    
    public void setKeyStorePassword(String password) {
        this.keyStorePassword = password;
    }
    
    public File getKeyStoreFile() {
        return keyStoreFile;
    }

    public void setKeyStoreFile(File keyStoreFile) {
        this.keyStoreFile = keyStoreFile;
    }

    public String getKeyStoreType() {
        return keyStoreType;
    }

    public void setKeyStoreType(String keyStoreType) {
        this.keyStoreType = keyStoreType;
    }

    public File getTrustStoreFile() {
        return trustStoreFile;
    }

    public void setTrustStoreFile(File trustStoreFile) {
        this.trustStoreFile = trustStoreFile;
    }

    public String getTrustStoreType() {
        return trustStoreType;
    }

    public void setTrustStoreType(String trustStoreType) {
        this.trustStoreType = trustStoreType;
    }

    public void setTrustStorePassword(String trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }
}
