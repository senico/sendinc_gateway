/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import mitm.common.net.HTTPMethodExecutor;
import mitm.common.net.NetUtils;
import mitm.common.net.HTTPMethodExecutor.ResponseHandler;
import mitm.common.scheduler.TaskScheduler;
import mitm.common.util.Check;
import mitm.common.util.MiscArrayUtils;
import mitm.common.util.SizeLimitedInputStream;
import mitm.common.util.SizeUtils;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tier2PartnerDetails
{
    private final static Logger logger = LoggerFactory.getLogger(Tier2PartnerDetails.class);
    
    /*
     * Maximum allowed response size returned from a call to the Comodo service
     */
    private final static int MAX_HTTP_RESPONSE_SIZE = 5 * SizeUtils.KB;

    /*
     * Tier 1 or Tier 2 username.
     */
    private String loginName;

    /*
     * Tier 1 or Tier 2 password.
     */
    private String loginPassword;
    
    /*
     * Tier 2 Account ID (this parameter is required when the "loginName" and "loginPassword" are those of
     * the Tier 1; otherwise, this parameter should be omitted)
     */
    private String accountID;

    /*
     * The connection settings
     */
    private final ComodoConnectionSettings connectionSettings;
    
    /*
     * True if an error occurred.
     */
    private boolean error;
    
    /*
     * The status code
     */
    private CustomClientStatusCode errorCode;
    
    /*
     * The error message if error is true
     */
    private String errorMessage;
    
    /*
     * The level of validation performed on the company controlling the account (e.g. "Class 3")
     */
    private String verificationLevel;
    
    /*
     * The status of the account (e.g. "Not yet active", "Active", "Suspended", etc) 
     */
    private String accountStatus;

    /*
     * The status of the Reseller functionality of the account (e.g. "Awaiting Authorization", "Authorized", "Rejected")
     */
    private String resellerStatus;
    
    /*
     * The status of the Web Host Reseller functionality of the account (e.g. "Awaiting Authorization", 
     * "Authorized", "Rejected")
     */
    private String webHostResellerStatus;
    
    /*
     * The status of the E-PKI functionality of the account (e.g. "Awaiting Authorization", "Authorized", "Rejected")
     */
    private String epkiStatus;
    
    /*
     * The maximum number of concurrently "live" CCCs that this Tier 2 is permitted to have (Certain Tier 2 
     * Resellers only)
     */
    private String capLiveCCCs;

    /*
     * The highest number of concurrently "live" CCCs that this Tier 2 has ever had (Certain Tier 2 Resellers only)
     */
    private String peakLiveCCCs;
    
    /*
     * The number of "live" CCCs that this Tier 2 currently has (Certain Tier 2 Resellers only)
     */
    private String currentLiveCCCs;

    /*
     * A comma-separated list of domain names that have been authorized to be used in email address fields in CCCs order 
     * by this Partner. The special value "authorizedDomains=NONE" means that no domain names are authorized.
     * (If this parameter is omitted, all domain names are authorized).
     */
    private String authorizedDomains;    
    
    private ResponseHandler responseHandler = new ResponseHandler()
    {
        @Override
        public void handleResponse(int statusCode, HttpMethod httpMethod, TaskScheduler watchdog) 
        throws IOException, HttpException
        {
            Tier2PartnerDetails.this.handleResponse(statusCode, httpMethod);
        }
    };
    
    public Tier2PartnerDetails(ComodoConnectionSettings connectionSettings)
    {
        Check.notNull(connectionSettings, "connectionSettings");
        Check.notNull(connectionSettings.getTier2PartnerDetailsURL(), "url");
        
        this.connectionSettings = connectionSettings;
    }
    
    private String getValue(Map<String, String[]> parameters, String name) {
        return MiscArrayUtils.safeGet(parameters.get(name.toLowerCase()), 0);
    }
    
    private void handleResponse(int statusCode, HttpMethod httpMethod)
    throws IOException
    {
        if (statusCode != HttpStatus.SC_OK) {
            throw new IOException("Error Tier2 partner details. Message: " + httpMethod.getStatusLine());
        }
        
        InputStream input = httpMethod.getResponseBodyAsStream();
        
        if (input == null) {
            throw new IOException("Response body is null.");                    
        }
        
        /*
         * we want to set a max on the number of bytes to download. We do not want a rogue server to return 1GB.
         */
        InputStream limitInput = new SizeLimitedInputStream(input, MAX_HTTP_RESPONSE_SIZE);
        
        String response = IOUtils.toString(limitInput, CharEncoding.US_ASCII);
        
        if (logger.isDebugEnabled()) {
            logger.debug("Response:\r\n" + response);
        }
        
        Map<String, String[]> parameters = NetUtils.parseQuery(response);
        
        errorCode = CustomClientStatusCode.fromCode(getValue(parameters, "errorCode"));
        
        if (errorCode.getID() < CustomClientStatusCode.SUCCESSFUL.getID())
        {
            error = true;
        
            errorMessage = getValue(parameters, "errorMessage");
        }
        else {
            error = false;

            verificationLevel     = getValue(parameters, "verificationLevel");
            accountStatus         = getValue(parameters, "accountStatus");
            resellerStatus        = getValue(parameters, "resellerStatus");
            webHostResellerStatus = getValue(parameters, "webHostResellerStatus");
            epkiStatus            = getValue(parameters, "epkiStatus");
            capLiveCCCs           = getValue(parameters, "capLiveCCCs");
            peakLiveCCCs          = getValue(parameters, "peakLiveCCCs");
            currentLiveCCCs       = getValue(parameters, "currentLiveCCCs");
            authorizedDomains     = getValue(parameters, "authorizedDomains");
        }
    }        
    
    private void reset()
    {
        error = false;
        errorCode = null;
        errorMessage = null;
        verificationLevel = null;
        accountStatus = null;
        resellerStatus = null;
        webHostResellerStatus = null;
        epkiStatus = null;
        capLiveCCCs = null;
        peakLiveCCCs = null;
        currentLiveCCCs = null;
        authorizedDomains = null;
    }
    
    /**
     * Requests a certificate. Returns true if a certificate was successfully requested.    
     */
    public boolean retrieveDetails()
    throws CustomClientCertException
    {
        reset();
        
        if (StringUtils.isEmpty(loginName)) {
            throw new CustomClientCertException("loginName must be specified.");
        }

        if (StringUtils.isEmpty(loginPassword)) {
            throw new CustomClientCertException("loginPassword must be specified.");
        }
        
        PostMethod postMethod = new PostMethod(connectionSettings.getTier2PartnerDetailsURL());
        
        NameValuePair[] data = {
                new NameValuePair("loginName", loginName),
                new NameValuePair("loginPassword", loginPassword)
        };
        
        if (accountID != null) {
            data = (NameValuePair[]) ArrayUtils.add(data, new NameValuePair("accountID", accountID));
        }
        
        postMethod.setRequestBody(data);
        
        HTTPMethodExecutor executor = new HTTPMethodExecutor(connectionSettings.getTotalTimeout(), 
                connectionSettings.getProxyInjector());
        
        executor.setConnectTimeout(connectionSettings.getConnectTimeout());
        executor.setReadTimeout(connectionSettings.getReadTimeout());
        
        try {
            executor.executeMethod(postMethod, responseHandler);
        }
        catch (IOException e) {
            throw new CustomClientCertException(e);
        }
        
        return !error;
    }
    
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public boolean isError() {
        return error;
    }

    public CustomClientStatusCode getErrorCode() {
        return errorCode;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public String getVerificationLevel() {
        return verificationLevel;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public String getResellerStatus() {
        return resellerStatus;
    }

    public String getWebHostResellerStatus() {
        return webHostResellerStatus;
    }

    public String getEpkiStatus() {
        return epkiStatus;
    }

    public String getCapLiveCCCs() {
        return capLiveCCCs;
    }

    public String getPeakLiveCCCs() {
        return peakLiveCCCs;
    }

    public String getCurrentLiveCCCs() {
        return currentLiveCCCs;
    }

    public String getAuthorizedDomains() {
        return authorizedDomains;
    }    
}
