/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

/**
 * Implementation of ComodoSettings that reads and writes it's settings from
 * a HierarchicalProperties object.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesComodoSettings implements ComodoSettings
{
    private final static String BASE_PROP = "ca.handler.comodo.";
    private final static String LOGIN_NAME = BASE_PROP + "loginName";
    private final static String LOGIN_PASSWORD = BASE_PROP + "loginPassword";
    private final static String AP = BASE_PROP + "ap";
    private final static String CA_CERTIFICATE_ID = BASE_PROP + "cACertificateID";
    private final static String AUTO_AUTHORIZE = BASE_PROP + "autoAuthorize";
    
    private final HierarchicalProperties properties;
    
    public PropertiesComodoSettings(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }
    
    @Override
    public void setLoginName(String loginName)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(LOGIN_NAME, loginName, false);
    }
    
    @Override
    public String getLoginName()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(LOGIN_NAME, false);
    }

    @Override
    public void setLoginPassword(String loginPassword)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(LOGIN_PASSWORD, loginPassword, true /* encrypt */);
    }
    
    @Override
    public String getLoginPassword()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(LOGIN_PASSWORD, true /* decrypt */);
    }

    @Override
    public void setAP(String ap)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(AP, ap, true /* encrypt */);
    }
    
    @Override
    public String getAP()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(AP, true /* decrypt */);
    }
    
    @Override
    public void setCACertificateID(Integer cACertificateID)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, CA_CERTIFICATE_ID, cACertificateID, false);
    }
    
    @Override
    public Integer getCACertificateID()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, CA_CERTIFICATE_ID, null, false);
    }
    
    @Override
    public void setAutoAuthorize(boolean autoAuthorize)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, AUTO_AUTHORIZE, autoAuthorize, false);
    }
    
    @Override
    public boolean isAutoAuthorize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(properties, AUTO_AUTHORIZE, false, false);
    }
}
