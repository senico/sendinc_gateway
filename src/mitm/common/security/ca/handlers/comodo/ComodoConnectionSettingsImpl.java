/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

import mitm.common.net.ProxyInjector;
import mitm.common.util.Check;

/**
 * ComodoConnectionSettings is a helper class used to specify settings used when connecting to Comodo services with 
 * HTTPS.
 * 
 * @author Martijn Brinkers
 *
 */
public class ComodoConnectionSettingsImpl implements ComodoConnectionSettings
{
    public final static String DEFAULT_APPLY_CUSTOM_CLIENT_CERT_URL = 
            "https://secure.comodo.net/products/!ApplyCustomClientCert";
    
    public final static String DEFAULT_COLLECT_CCC_URL = 
            "https://secure.comodo.net/products/download/CollectCCC";
    
    public final static String DEFAULT_TIER2_PARTNER_DETAILS_URL = 
            "https://secure.comodo.net/products/!Tier2PartnerDetails";

    public final static String DEFAULT_AUTO_AUTHORIZE_URL = 
        "https://secure.comodo.net/products/!AutoAuthorize";
        
    /*
     * The URL of the ApplyCustomClientCert service. Defaults to:
     * 
     * https://secure.comodo.net/products/!ApplyCustomClientCert
     */
    private String applyCustomClientCertURL = DEFAULT_APPLY_CUSTOM_CLIENT_CERT_URL;
    
    /*
     * The URL of the CollectCCC service. Defaults to:
     * 
     * https://secure.comodo.net/products/download/CollectCCC
     */
    private String collectCustomClientCertURL = DEFAULT_COLLECT_CCC_URL;

    /*
     * The URL of the Tier2PartnerDetails service. Defaults to:
     * 
     * https://secure.comodo.net/products/!Tier2PartnerDetails
     */
    private String tier2PartnerDetailsURL = DEFAULT_TIER2_PARTNER_DETAILS_URL;

    /*
     * The URL of the Tier2PartnerDetails service. Defaults to:
     * 
     * https://secure.comodo.net/products/!AutoAuthorize
     */
    private String autoAuthorizeURL = DEFAULT_AUTO_AUTHORIZE_URL;
    
    /* 
     * max time in milliseconds the total HTTP call may take
     */
    private int totalTimeout;
    
    /* 
     * max time in milliseconds the HTTP connection may take
     */
    private int connectTimeout;

    /* 
     * socket read timeout in milliseconds
     */
    private int readTimeout;

    /*
     * Used to set the proxy for HttpClient
     */
    private final ProxyInjector proxyInjector;
    
    public ComodoConnectionSettingsImpl(int totalTimeout, ProxyInjector proxyInjector)
    {
        if (totalTimeout <= 0) {
            throw new IllegalArgumentException("totalTimeout should be > 0");
        }
        
        this.totalTimeout = totalTimeout; 
        this.proxyInjector = proxyInjector;
    }

    @Override
    public String getApplyCustomClientCertURL() {
        return applyCustomClientCertURL;
    }
    
    @Override
    public void setApplyCustomClientCertURL(String url)
    {
        Check.notNull(url, "url");
        
        this.applyCustomClientCertURL = url;
    }

    @Override
    public String getCollectCustomClientCertURL() {
        return collectCustomClientCertURL;
    }
    
    @Override
    public void setCollectCustomClientCertURL(String url)
    {
        Check.notNull(url, "url");

        this.collectCustomClientCertURL = url;
    }

    @Override
    public String getTier2PartnerDetailsURL() {
        return tier2PartnerDetailsURL;
    }
    
    @Override
    public void setTier2PartnerDetailsURL(String url)
    {
        Check.notNull(url, "url");

        this.tier2PartnerDetailsURL = url;
    }
    
    @Override
    public String getAutoAuthorizeURL() {
        return autoAuthorizeURL;
    }
    
    @Override
    public void setAutoAuthorizeURL(String url)
    {
        Check.notNull(url, "url");

        this.autoAuthorizeURL = url;
    }
    
    @Override
    public int getTotalTimeout() {
        return totalTimeout;
    }

    @Override
    public void setTotalTimeout(int totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    @Override
    public int getConnectTimeout() {
        return connectTimeout;
    }

    @Override
    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    @Override
    public int getReadTimeout() {
        return readTimeout;
    }

    @Override
    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    @Override
    public ProxyInjector getProxyInjector() {
        return proxyInjector;
    }
}
