/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

import mitm.common.net.ProxyInjector;

/**
 * ComodoConnectionSettings is a helper class used to specify settings used when connecting to Comodo services with 
 * HTTPS.
 * 
 * @author Martijn Brinkers
 *
 */
public interface ComodoConnectionSettings
{
    /**
     * The URL of the ApplyCustomClientCert service. Defaults to:
     * 
     * https://secure.comodo.net/products/!ApplyCustomClientCert
     */
    public String getApplyCustomClientCertURL();
    public void setApplyCustomClientCertURL(String url);

    /**
     * The URL of the CollectCCC service. Defaults to:
     * 
     * https://secure.comodo.net/products/download/CollectCCC
     */
    public String getCollectCustomClientCertURL();
    public void setCollectCustomClientCertURL(String url);

    /**
     * The URL of the Tier2PartnerDetails service. Defaults to:
     * 
     * https://secure.comodo.net/products/!Tier2PartnerDetails
     */
    public String getTier2PartnerDetailsURL();
    public void setTier2PartnerDetailsURL(String url);
    
    /**
     * The URL of the AutoAuthorize service. Defaults to:
     * 
     * https://secure.comodo.net/products/!AutoAuthorize
     */
    public String getAutoAuthorizeURL();
    public void setAutoAuthorizeURL(String url);
    
    /**
     * max time in milliseconds the total HTTP call may take
     */
    public int getTotalTimeout();
    public void setTotalTimeout(int totalTimeout);

    /** 
     * max time in milliseconds the HTTP connection may take
     */
    public int getConnectTimeout();
    public void setConnectTimeout(int connectTimeout);

    /**
     * socket read timeout in milliseconds
     */
    public int getReadTimeout();
    public void setReadTimeout(int readTimeout);

    /**
     * Returns the ProxyInjector
     */
    public ProxyInjector getProxyInjector();
}
