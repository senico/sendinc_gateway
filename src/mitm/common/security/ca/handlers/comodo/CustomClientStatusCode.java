/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

public enum CustomClientStatusCode
{
    CERTIFICATES_ATTACHED         (  "2",   2, "Certificates Attached"),
    CERTIFICATES_AVAILABLE        (  "1",   1, "Certificates Available"),
    SUCCESSFUL                    (  "0",   0, "Success"),
    REQUEST_NOT_OVER_HTTPS        ( "-1",  -1, "Request was not made over https!"),
    UNRECOGNISED_ARGUMENT         ( "-2",  -2, "Unrecognised argument!"),
    ARGUMENT_IS_MISSING           ( "-3",  -3, "The 'xxxx' argument is missing!"),
    ARGUMENT_IS_INVALID           ( "-4",  -4, "The value of the 'xxxx' argument is invalid!"),
    NOT_A_VALID_ISO_3166_COUNTRY  ( "-7",  -7, "'xx' is not a valid ISO-3166 country!"),
    CSR_IS_NOT_VALID_BASE64       ( "-9",  -9, "The CSR is not valid Base-64 data!"),
    CSR_CANNOT_BE_DECODED         ("-10", -10, "The CSR cannot be decoded!"),
    UNSUPPORTED_ALGORITHM         ("-11", -11, "The CSR uses an unsupported algorithm!"),
    INVALID_SIGNATURE             ("-12", -12, "The CSR has an invalid signature!"),
    UNSUPPORTED_KEY_SIZE          ("-13", -13, "The CSR uses an unsupported key size!"),
    UNKNOWN_ERROR                 ("-14", -14, "An unknown error occurred!"),
    PERMISSION_DENIED             ("-16", -16, "Permission denied!"),
    REQUEST_USED_GET              ("-17", -17, "Request used GET rather than POST!"),
    NOT_WAITING_FOR_AUTHORIZATION ("-27", -27, "Order 'xxxx' is not currently Awaiting Authorization/Rejection!");

    private String code;
    private int id;
    private String message;
    
    private CustomClientStatusCode(String code, int id, String message)
    {
        this.code = code;
        this.id = id;
        this.message = message;
    }
    
    public String getCode() {
        return code;
    }

    public int getID() {
        return id;
    }
    
    public String getMessage() {
        return message;
    }
    
    /**
     * Returns the ApplyCustomClientCertErrorCode with the given code. Null if there was no match.
     */
    public static CustomClientStatusCode fromCode(String code) 
    {
        for (CustomClientStatusCode error : CustomClientStatusCode.values())
        {
            if (error.getCode().equals(code)) {
                return error;
            }
        }
        
        return UNKNOWN_ERROR;
    }
    
}
