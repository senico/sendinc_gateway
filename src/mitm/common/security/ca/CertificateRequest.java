/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import java.security.KeyPair;
import java.util.Date;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.KeyEncoderException;
import mitm.common.security.crypto.Encryptor;

/**
 * A CertificateRequest is a request for signing a certificate. The CertificateRequest will be handled by a 
 * CertificateRequestHandler.
 * 
 * @author Martijn Brinkers
 *
 */
public interface CertificateRequest
{
    /**
     * The ID uniquely identifies this request. The ID might be null when the request is not yet persistent
     */
    public Long getID();
    
    /**
     * The date the CertificateRequest was created
     */
    public Date getCreated();
    
    /**
     * The subject for the certificate 
     */
    public void setSubject(X500Principal subject);
    public X500Principal getSubject();
    
    /**
     * Email will be added as an AltName
     */
    public void setEmail(String email);
    public String getEmail();
    
    /**
     * The number of days the certificate should be valid. Must be > 0.
     */
    public void setValidity(int validity);
    public int getValidity();
        
    /**
     * The signature algorithm used to sign the certificate
     */
    public void setSignatureAlgorithm(String SignatureAlgorithm);
    public String getSignatureAlgorithm();
    
    /**
     * The length of the key for the request
     */
    public void setKeyLength(int KeyLength);
    public int getKeyLength();
    
    /**
     * The CRL distribution point to add
     */
    public void setCRLDistributionPoint(String CRLDistributionPoint);
    public String getCRLDistributionPoint();

    /**
     * The public and private key pair associated with the request.
     */
    public void setKeyPair(KeyPair keyPair, Encryptor encryptor)    
    throws KeyEncoderException;

    public KeyPair getKeyPair(Encryptor encryptor)
    throws KeyEncoderException;

    /**
     * Info will be used to provide some user viewable information (freeform text) about the request.
     */
    public void setInfo(String info);
    public String getInfo();
    
    /**
     * A general data object. Certificate providers can store whatever they like.
     */
    public void setData(byte[] data);
    public byte[] getData();
        
    /**
     * A counter that keeps track how many times this request was handled.
     */
    public void setIteration(int iteration);
    public int getIteration();
    
    /**
     * The date at which this request was last updated. 
     */
    public void setLastUpdated(Date lastUpdated);
    public Date getLastUpdated();

    /**
     * The date at which this request require an update .
     */
    public void setNextUpdate(Date nextUpdate);
    public Date getNextUpdate();
    
    /**
     * The message (for example a warning/error message) since the last update.  
     * This can be set by the certificate provider
     */
    public void setLastMessage(String message);
    public String getLastMessage();
    
    /**
     * The name of the CertificateHandler which is responsible for handling this request
     */
    public String getCertificateHandlerName();    
}
