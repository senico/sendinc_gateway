/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import mitm.common.security.KeyAndCertificate;

/**
 * A CertificateRequestHandler is responsible for handling the certificate request. For example a 
 * CertificateRequestHandler can be created for Comodo, Verisign, EJBCA etc. 
 * 
 * @author Martijn Brinkers
 *
 */
public interface CertificateRequestHandler
{
    /**
     * True if this CertificateRequestHandler can be used. A CertificateRequestHandler can for example return false
     * if the CertificateRequestHandler is not correctly setup.
     */
    public boolean isEnabled();
    
    /**
     * Truie if the certificate is immediately issued when handleRequest is called the first time. False if the 
     * certificate request might take more than one call to handleRequest.  
     */
    public boolean isInstantlyIssued();
    
    /**
     * The name of the CertificateRequestHandler. The provider name should be unique.
     */
    public String getCertificateHandlerName();    
    
    /**
     * Handles the certificate request. If the certificate is issued a, KeyAndCertificate instance is returned.
     * If the certificate is not yet issued, null will be returned. The CertificateRequestHandler might update
     * some CertificateRequest parameters (for example data). The CertificateRequestHandler might be 
     * called more than once. This is required for certificate providers that require multiple steps over some
     * time period.
     */
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException;
}
