/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import javax.security.auth.x500.X500Principal;

public class RequestParametersImpl implements RequestParameters
{
    /*
     * Subject of the certificate to be issued
     */
    private X500Principal subject;
    
    /*
     * Email of the certificate (stored in AtlName)
     */
    private String email;
    
    /*
     * The number of days the certificate is valid from now.
     */
    private int validity;

    /*
     * The length of the RSA key for the certificate
     */
    private int keyLength;

    /*
     * Algorithm for signing the certificates
     */
    private String signatureAlgorithm;

    /*
     * The CRL distribution point to add to the certificate
     */
    private String crlDistributionPoint;
    
    /*
     * The name of the certificate request handler that will handle this request
     */
    private String certificateRequestHandler;
    
    @Override
    public X500Principal getSubject() {
        return subject;
    }

    @Override
    public void setSubject(X500Principal subject) {
        this.subject = subject;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int getValidity() {
        return validity;
    }

    @Override
    public void setValidity(int days)
    {
        if (days < 0) {
            throw new IllegalArgumentException("days must be > 0");
        }
        
        this.validity = days;
    }

    @Override
    public int getKeyLength() {
        return keyLength;
    }

    @Override
    public void setKeyLength(int keyLength)
    {
        if (keyLength < 1024) {
            throw new IllegalArgumentException("keyLength must be >= 1024");
        }
        
        this.keyLength = keyLength;
    }
    
    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
    @Override
    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }
    
    @Override
    public void setCRLDistributionPoint(String crlDistributionPoint) {
        this.crlDistributionPoint = crlDistributionPoint;
    }
    
    @Override
    public String getCRLDistributionPoint() {
        return crlDistributionPoint;
    }
    
    @Override
    public void setCertificateRequestHandler(String certificateRequestHandler) {
        this.certificateRequestHandler = certificateRequestHandler;
    }
    
    @Override
    public String getCertificateRequestHandler() {
        return certificateRequestHandler;
    }
}
