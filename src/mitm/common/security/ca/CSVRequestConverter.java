/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mitm.common.locale.CharacterEncoding;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.certificate.X500PrincipalBuilder;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;
import mitm.common.util.UnicodeReader;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import au.com.bytecode.opencsv.CSVReader;

/**
 * CSVRequestConverter converts a comma separated list of values to a list of RequestParameters. This can be used
 * for bulk requesting of certificates.
 * 
 * Note: class is not thread safe
 * 
 * @author Martijn Brinkers
 *
 */
public class CSVRequestConverter
{
    private static final int DEFAULT_MAX_VALUE_LENGTH = 256;

    private static final int MAX_COLUMN_NAME_LENGTH = 32;

    private static final int DEFAULT_MAX_LINES = 1000;
    
    /*
     * The supported columns
     */
    private static enum Column {
        EMAIL, ORGANISATION, COMMONNAME, FIRSTNAME, LASTNAME;
    }
    
    /*
     * the oder of the columns found in the CSV
     */
    private Column[] columnOrder;
    
    /*
     * Maps a name of a column to the actual column
     */
    private Map<String, Column> namesToColumns;
    
    /*
     * The found email addresses
     */
    private Set<String> foundEmails;
    
    /*
     * If true duplicate email addresses are allowed
     */
    private boolean allowDuplicates;
    
    /*
     * The max length of an individual column value. This is used as a sanity check.
     */
    private int maxValueLength = DEFAULT_MAX_VALUE_LENGTH;

    /*
     * The max number of lines the CSV may have
     */
    private int maxLines = DEFAULT_MAX_LINES;
    
    private Column getColumn(String name)
    throws IOException
    {
        if (StringUtils.isBlank(name)) {
            throw new IOException("column name is missing.");
        }
        
        if (namesToColumns == null)
        {
            namesToColumns = new HashMap<String, Column>();
            
            namesToColumns.put("email", Column.EMAIL);
            namesToColumns.put("e", Column.EMAIL);
    
            namesToColumns.put("organisation", Column.ORGANISATION);
            namesToColumns.put("organization", Column.ORGANISATION);
            namesToColumns.put("org", Column.ORGANISATION);
            namesToColumns.put("o", Column.ORGANISATION);
    
            namesToColumns.put("commonname", Column.COMMONNAME);
            namesToColumns.put("cn", Column.COMMONNAME);
    
            namesToColumns.put("firstname", Column.FIRSTNAME);
            namesToColumns.put("fn", Column.FIRSTNAME);
            namesToColumns.put("givenname", Column.FIRSTNAME);
            namesToColumns.put("gn", Column.FIRSTNAME);
    
            namesToColumns.put("lastname", Column.LASTNAME);
            namesToColumns.put("ln", Column.LASTNAME);
            namesToColumns.put("surname", Column.LASTNAME);
            namesToColumns.put("sn", Column.LASTNAME);
        }
        
        return namesToColumns.get(name.trim().toLowerCase());
    }
    
    private void readHeader(CSVReader reader)
    throws IOException, RequestConverterException
    {
        String[] headers = reader.readNext();
        
        if (headers == null) {
            throw new IOException("No header found.");
        }
        
        ArrayList<Column> columns = new ArrayList<Column>();
        
        for (String header : headers)
        {
            header = MiscStringUtils.restrictLength(header, MAX_COLUMN_NAME_LENGTH, true);
            
            Column column = getColumn(header);
            
            if (column == null)
            {
                throw new RequestConverterException("There is no column named " + 
                        MiscStringUtils.toAscii(MiscStringUtils.removeControlChars(header)));
            }
            
            if (columns.contains(column)) {
                throw new RequestConverterException("Column " + column + " is already used.");
            }
            
            columns.add(column);
        }
        
        columnOrder = new Column[columns.size()];
        
        columnOrder = columns.toArray(columnOrder);
    }
    
    private void checkRequiredColumns()
    throws RequestConverterException
    {
        if (!ArrayUtils.contains(columnOrder, Column.EMAIL)) {
            throw new RequestConverterException("Email column is missing.");
        }

        if (!ArrayUtils.contains(columnOrder, Column.COMMONNAME)) {
            throw new RequestConverterException("Common name column is missing.");
        }
    }
    
    private void setEmail(String email, RequestParameters request, X500PrincipalBuilder subjectBuilder, int column, 
            int currentLine)
    throws RequestConverterException
    {
        String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
        
        if (filteredEmail == null)
        {
            throw new RequestConverterException("Email address " + email + " at line " + currentLine + " and column " + 
                    column + " is not a valid email address.");
        }
        
        if (!allowDuplicates && foundEmails.contains(filteredEmail))
        {
            throw new RequestConverterException("Email address " + email + " at line " + currentLine + " and column " + 
                    column + " is already used.");
        }
        
        foundEmails.add(filteredEmail);
        
        request.setEmail(filteredEmail);
        subjectBuilder.setEmail(filteredEmail);
    }

    private void setCommonName(String cn, X500PrincipalBuilder subjectBuilder, int column, int currentLine)
    throws RequestConverterException
    {
        if (StringUtils.isEmpty(cn))
        {
            throw new RequestConverterException("Common name at line " + currentLine + " and column " + 
                    column + " is missing.");
        }
        
        subjectBuilder.setCommonName(cn);
    }
    
    /**
     * Converts the CSV to a list of RequestParameters. The first row should contain the column order. The CSV is 
     * considered to be US-ASCII encoded unless a Byte Ordering Mark (BOM) is used. 
     *  
     * 
     * The following columns are supported:
     * 
     * EMAIL, ORGANISATION, COMMONNAME, FIRSTNAME, LASTNAME
     * 
     * Multiple aliases for the columns are available and names are case insensitive.
     * 
     * EMAIL        : [email, e]
     * ORGANISATION : [organisation, org, o]
     * COMMONNAME   : [commonname, cn]
     * FIRSTNAME    : [firstname, fn, givenname, gn]
     * LASTNAME     : [lastname, ln, surname, sn]
     * 
     * NOTE: all other fields, or fields that are not specified in the CSV, of the returned RequestParameters are 
     * NOT set.
     */
    public List<RequestParameters> convertCSV(InputStream csv)
    throws IOException, RequestConverterException
    {
        Check.notNull(csv, "csv");
        
        foundEmails = new HashSet<String>();  
        
        CSVReader reader = new CSVReader(new UnicodeReader(csv, CharacterEncoding.US_ASCII));
        
        readHeader(reader);
        
        checkRequiredColumns();
        
        List<RequestParameters> result = new LinkedList<RequestParameters>();
        
        String [] line;
        
        int currentLine = 0;
        
        while ((line = reader.readNext()) != null)
        {
            currentLine++;
            
            if (currentLine > maxLines) {
                throw new RequestConverterException("Maximum number of lines exceeded (" + maxLines + ").");
            }
            
            if (line == null || (line.length == 1 && StringUtils.isBlank(line[0])))
            {
                /*
                 * Skip empty lines 
                 */
                continue;
            }
            
            if (line.length != columnOrder.length)
            {
                throw new RequestConverterException("Line " + currentLine + " does not contain the correct " + 
                        "number of columns: " + StringUtils.join(line, ", "));
            }
            
            RequestParameters request = new RequestParametersImpl();
            
            X500PrincipalBuilder subjectBuilder = new X500PrincipalBuilder();
            
            for (int column = 0; column < line.length; column++)
            {
                String value = StringUtils.trim(line[column]);
                
                /*
                 * Length sanity check
                 */
                if (StringUtils.length(value) > maxValueLength)
                {
                    throw new RequestConverterException("The column value " + StringUtils.defaultString(value) + 
                            " at line " + currentLine + " and column " + column + " exceeds the maximum number of" +  
                            " characters (" + maxValueLength + ").");
                }
                                
                switch(columnOrder[column])
                {
                case EMAIL: setEmail(value, request, subjectBuilder, column, currentLine); break;
                case ORGANISATION: subjectBuilder.setOrganisation(value) ; break;
                case COMMONNAME: setCommonName(value, subjectBuilder, column, currentLine) ; break;
                case FIRSTNAME: subjectBuilder.setGivenName(value) ; break;
                case LASTNAME: subjectBuilder.setSurname(value) ; break;
                
                default:
                    throw new RequestConverterException("Unsupported column " + columnOrder[column]);
                }
                
                request.setSubject(subjectBuilder.buildPrincipal());
            }
            
            result.add(request);
        }
        
        return result;
    }

    public int getMaxValueLength() {
        return maxValueLength;
    }

    public void setMaxValueLength(int maxValueLength) {
        this.maxValueLength = maxValueLength;
    }

    public int getMaxLines() {
        return maxLines;
    }

    public void setMaxLines(int maxLines) {
        this.maxLines = maxLines;
    }

    public boolean isAllowDuplicates() {
        return allowDuplicates;
    }

    public void setAllowDuplicates(boolean allowDuplicates) {
        this.allowDuplicates = allowDuplicates;
    }
}
