/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

/**
 * Implementation of CASettings that reads and writes it's settings from
 * a HierarchicalProperties object.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesCASettings implements CASettings
{
    private final static String BASE_PROP = "ca.";
    private final static String SIGNER_THUMBPRINT = BASE_PROP + "signerThumbprint";
    private final static String DEFAULT_COMMON_NAME = BASE_PROP + "defaultCommonName";
    private final static String VALIDITY = BASE_PROP + "validity";
    private final static String KEY_LENGTH = BASE_PROP + "keyLength";
    private final static String SIGNATURE_ALGORITHM = BASE_PROP + "signatureAlgorithm";
    private final static String CA_EMAIL = BASE_PROP + "caEmail";
    private final static String PASSWORD_LENGTH = BASE_PROP + "passwordLength";
    private final static String CRL_DIST_POINT = BASE_PROP + "crlDistPoint";
    private final static String ADD_CRL_DIST_POINT = BASE_PROP + "addCRLDistPoint";
    private final static String STORE_PFX_PASSWORD = BASE_PROP + "storePFXPassword";
    private final static String CERTIFICATE_REQUEST_HANDLER = BASE_PROP + "certificateRequestHandler";
    
    private final HierarchicalProperties properties;
    
    public PropertiesCASettings(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }

    @Override
    public void setSignerThumbprint(String thumbprint)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(SIGNER_THUMBPRINT, thumbprint, false);
    }
 
    @Override
    public String getSignerThumbprint()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(SIGNER_THUMBPRINT, false);
    }
    
    @Override
    public void setDefaultCommonName(String commonName)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(DEFAULT_COMMON_NAME, commonName, false);
    }

    @Override
    public String getDefaultCommonName()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(DEFAULT_COMMON_NAME, false);
    }
    
    @Override
    public void setValidity(Integer days)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, VALIDITY, days, false);
    }

    @Override
    public Integer getValidity()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, VALIDITY, 365 * 5, false);
    }
    
    @Override
    public void setKeyLength(Integer length)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, KEY_LENGTH, length, false);
    }

    @Override
    public Integer getKeyLength()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, KEY_LENGTH, 1024, false);
    }
    
    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(SIGNATURE_ALGORITHM, signatureAlgorithm, false);
    }

    @Override
    public String getSignatureAlgorithm()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(SIGNATURE_ALGORITHM, false);
    }
    
    @Override
    public void setCAEmail(String email)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(CA_EMAIL, email, false);
    }

    @Override
    public String getCAEmail()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(CA_EMAIL, false);
    }
    
    @Override
    public void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, PASSWORD_LENGTH, length, false);
    }

    @Override
    public Integer getPasswordLength()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, PASSWORD_LENGTH, 16, false);
    }    
    
    @Override
    public void setCRLDistributionPoint(String url)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(CRL_DIST_POINT, url, false);
    }
    
    @Override
    public String getCRLDistributionPoint()
    throws HierarchicalPropertiesException 
    {
        return properties.getProperty(CRL_DIST_POINT, false);
    }
    
    @Override
    public void setAddCRLDistributionPoint(boolean add)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, ADD_CRL_DIST_POINT, add, false);
    }        

    @Override
    public boolean isAddCRLDistributionPoint()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(properties, ADD_CRL_DIST_POINT, false, false);
    }
    
    @Override
    public void setStorePFXPassword(boolean store)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, STORE_PFX_PASSWORD, store, false);
    }        

    @Override
    public boolean isStorePFXPassword()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(properties, STORE_PFX_PASSWORD, false, false);
    }
    
    @Override
    public void setCertificateRequestHandler(String certificateRequestHandler)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(CERTIFICATE_REQUEST_HANDLER, certificateRequestHandler, false);
    }
    
    @Override
    public String getCertificateRequestHandler()
    throws HierarchicalPropertiesException 
    {
        return properties.getProperty(CERTIFICATE_REQUEST_HANDLER, false);
    }    
}
