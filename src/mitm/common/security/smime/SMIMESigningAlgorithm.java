/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;

/**
 * The signing algorithms that are supported by the s/mime modules.
 * 
 * @author Martijn Brinkers
 */
public enum SMIMESigningAlgorithm 
{    
    MD2WITHRSA                 ("MD2WITHRSA", PKCSObjectIdentifiers.md2WithRSAEncryption, "MD2WithRSA"),
    MD2WITHRSAENCRYPTION       ("MD2WITHRSAENCRYPTION", PKCSObjectIdentifiers.md2WithRSAEncryption, "MD2WithRSAEncryption"),
    MD5WITHRSA                 ("MD5WITHRSA", PKCSObjectIdentifiers.md5WithRSAEncryption, "MD5WithRSA"),
    MD5WITHRSAENCRYPTION       ("MD5WITHRSAENCRYPTION", PKCSObjectIdentifiers.md5WithRSAEncryption, "MD5WithRSAEncryption"),
    SHA1WITHRSAENCRYPTION      ("SHA1WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1WithRSAEncryption"),
    SHA1WITHRSA                ("SHA1WITHRSA", PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1WithRSA"),
    SHA224WITHRSAENCRYPTION    ("SHA224WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WithRSAEncryption"),
    SHA224WITHRSA              ("SHA224WITHRSA", PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WithRSA"),
    SHA256WITHRSAENCRYPTION    ("SHA256WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WithRSAEncryption"),
    SHA256WITHRSA              ("SHA256WITHRSA", PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WithRSA"),
    SHA384WITHRSAENCRYPTION    ("SHA384WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WithRSAEncryption"),
    SHA384WITHRSA              ("SHA384WITHRSA", PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WithRSA"),
    SHA512WITHRSAENCRYPTION    ("SHA512WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WithRSAEncryption"),
    SHA512WITHRSA              ("SHA512WITHRSA", PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WithRSA"),
    RIPEMD160WITHRSAENCRYPTION ("RIPEMD160WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160, "RIPEMD160WithRSAEncryption"),
    RIPEMD160WITHRSA           ("RIPEMD160WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160, "RIPEMD160WithRSA"),
    RIPEMD128WITHRSAENCRYPTION ("RIPEMD128WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128, "RIPEMD128WithRSAEncryption"),
    RIPEMD128WITHRSA           ("RIPEMD128WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128, "RIPEMD128WithRSA"),
    RIPEMD256WITHRSAENCRYPTION ("RIPEMD256WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256, "RIPEMD256WithRSAEncryption"),
    RIPEMD256WITHRSA           ("RIPEMD256WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256, "RIPEMD256WithRSA");
    
    private final String algorithm;
    private final ASN1ObjectIdentifier oid; 
    private final String friendlyName;
    
    private SMIMESigningAlgorithm(String algorithm, ASN1ObjectIdentifier oid, String friendlyName)
    {
        this.algorithm = algorithm;
        this.oid = oid;
        this.friendlyName = friendlyName;
    }
    
    public String getName() {
        return friendlyName;
    }
    
    public ASN1ObjectIdentifier getOID() {
        return oid;
    }
    
    public String getAlgorithm() {
        return algorithm;
    }
    
    public static SMIMESigningAlgorithm fromOID(String oid)
    {
        for (SMIMESigningAlgorithm algorithm : SMIMESigningAlgorithm.values())
        {
            if (algorithm.oid.getId().equals(oid)) {
                return algorithm;
            }
        }
        
        return null;
    }
    
    public static SMIMESigningAlgorithm fromName(String name)
    {
        name = StringUtils.trimToNull(name);
        
        if (name != null)
        {
            for (SMIMESigningAlgorithm algorithm : SMIMESigningAlgorithm.values())
            {
                if (algorithm.getName().equalsIgnoreCase(name)) {
                    return algorithm;
                }
            }
        }
        
        return null;
    }

    @Override
    public String toString() {
        return friendlyName;
    }
}
