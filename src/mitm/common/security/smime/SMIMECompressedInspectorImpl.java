/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.io.IOException;
import java.io.InputStream;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.cms.CMSAdapterFactory;
import mitm.common.security.cms.CMSCompressedDataAdapter;
import mitm.common.security.cms.CMSCompressedInspector;
import mitm.common.security.cms.CMSCompressedInspectorImpl;
import mitm.common.security.cms.CryptoMessageSyntaxException;

import org.bouncycastle.cms.CMSCompressedData;
import org.bouncycastle.cms.CMSException;

public class SMIMECompressedInspectorImpl implements SMIMECompressedInspector
{
    private final CMSCompressedInspector compressedInspector;
    
    public SMIMECompressedInspectorImpl(Part compressedPart) 
    throws SMIMEInspectorException, MessagingException
    {
        try {
            compressedInspector = new CMSCompressedInspectorImpl(getCompressedDataAdapter(compressedPart));
        }
        catch (CMSException e) {
            throw new SMIMEInspectorException(e);
        }
        catch (IOException e) {
            throw new SMIMEInspectorException(e);
        }
    }
    
    protected CMSCompressedDataAdapter getCompressedDataAdapter(Part compressedPart) 
    throws MessagingException, CMSException, IOException
    {
        CMSCompressedData compressed = new CMSCompressedData(compressedPart.getInputStream());
        
        CMSCompressedDataAdapter compressedAdapter = CMSAdapterFactory.createAdapter(compressed);
        
        return compressedAdapter;
    }
    
    @Override
    public InputStream getContentStream()
    throws CryptoMessageSyntaxException
    {
        return compressedInspector.getContentStream();
    }

    @Override
    public MimeBodyPart getContentAsMimeBodyPart() 
    throws MessagingException, CryptoMessageSyntaxException
    {
        MimeBodyPart mimeBodyPart = new MimeBodyPart(getContentStream());
        
        return mimeBodyPart;
    }
    
    @Override
    public MimeMessage getContentAsMimeMessage()
    throws MessagingException, CryptoMessageSyntaxException 
    {
        MimeMessage message = MailUtils.loadMessage(getContentStream());
        
        return message;
    }
}
