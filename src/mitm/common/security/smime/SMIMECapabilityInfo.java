/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.math.BigInteger;

public class SMIMECapabilityInfo
{
    private final String oid;
    private final BigInteger keySize;

    public SMIMECapabilityInfo(String oid, BigInteger keySize) 
    {
        this.oid = oid;
        this.keySize = keySize;
    }

    public String getOID() {
        return oid;
    }

    /**
     * Returns the key size if available. Null if key size if not applicable or if key size is 
     * not available. 
     */
    public Integer getkeySize() 
    {
        /*
         * although key size is a BigInteger ASN.1 speaking we will convert
         * it to an int.
         */
        return keySize != null ? keySize.intValue() : null;
    }

    /**
     * Returns the friendly name of the algorithm if the algorithm is known. If not
     * it returns the oid.
     */
    public String getAlgorithmName() 
    {
        SMIMEEncryptionAlgorithm algorithm = getAlgorithm();

        String name = (algorithm != null ? algorithm.toString() : oid);

        return name;
    }

    /**
     * Returns the SMIMEEncryptionAlgorithm for the oid. Null if the algorithm is
     * unknown
     */
    public SMIMEEncryptionAlgorithm getAlgorithm() {
        return SMIMEEncryptionAlgorithm.fromOID(oid);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getAlgorithmName());

        if (keySize != null)
        {
            sb.append(", ").append(keySize);
        }

        return sb.toString();
    }
}
