/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.crypto.SecretKey;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * SMIMEBuilder is used for building S/MIME encrypted, signed and compressed messages.
 * 
 * @author Martijn Brinkers
 *
 */
public interface SMIMEBuilder
{
    /**
     * Adds a new signer. The message will be signed and the signer will be identified by issuer/serial number.
     * The following default signed attributes will be added: SMIME capability, signing time.
     */
    public void addSigner(PrivateKey privateKey, X509Certificate signer, SMIMESigningAlgorithm algorithm) 
    throws SMIMEBuilderException;

    /**
     * Adds a new signer. The message will be signed and the signer will be identified by issuer/serial number.
     * The following default signed attributes will be added: SMIME capability, signing time, encryption key preference,
     * and Outlook specific version of encryption key preference (see 
     * {@link SMIMEAttributeUtils#getDefaultSignedAttributes(X509Certificate)})
     */
    public void addSigner(PrivateKey privateKey, X509Certificate signer, SMIMESigningAlgorithm algorithm, 
            X509Certificate encryptionKeyPreference) 
    throws SMIMEBuilderException;
    
    /**
     * Adds a new signer. The message will be signed and the signer will be identified by the subjectKeyIdentifier.
     * The following default signed attributes will be added: SMIME capability, signing time.
     */
    public void addSigner(PrivateKey privateKey, byte[] subjectKeyIdentifier, SMIMESigningAlgorithm algorithm)
    throws SMIMEBuilderException; 
    
    /**
     * Adds certificates to the CMS
     */
    public void addCertificates(Collection<X509Certificate> certificates)
    throws SMIMEBuilderException; 

    /**
     * Adds certificates to the CMS
     */
    public void addCertificates(X509Certificate... certificates)
    throws SMIMEBuilderException; 
    
    /**
     * Encrypts the message with the certificate. The recipient will be identified by a KeyTransRecipientInfo.
     * The provided mode determines whether issuer/serial nr and/or Subject key indentifier will be used. 
     */
    public void addRecipient(X509Certificate certificate, SMIMERecipientMode mode)
    throws SMIMEBuilderException; 

    /**
     * Encrypts the message with a secret key. The recipient will be identified by a KEKRecipientInfo.
     */
    public void addRecipient(SecretKey secretKey, byte[] keyIdentifier);

    /**
     * Digitally sign the message in clear or opaque mode.
     */
    public void sign(SMIMESignMode signMode) 
    throws SMIMEBuilderException; 
    
    /**
     * Encrypt the message with the provided algorithm and key strength.
     */
    public void encrypt(SMIMEEncryptionAlgorithm algorithm, int keySize)
    throws SMIMEBuilderException; 

    /**
     * Encrypt the message with the provided algorithm using the default key strength of the algorithm.
     */
    public void encrypt(SMIMEEncryptionAlgorithm algorithm)
    throws SMIMEBuilderException; 
    
    /**
     * Compress the message.
     */
    public void compress()
    throws SMIMEBuilderException; 
        
    /**
     * Build the final message. 
     */
    public MimeMessage buildMessage() 
    throws SMIMEBuilderException, MessagingException; 
    
    /**
     * If true, the deprecated S/MIME content-types x-pkcs7* headers will be used.
     */
    public void setUseDeprecatedContentTypes(boolean useDeprecatedContentTypes);
    public boolean isUseDeprecatedContentTypes();
}
