/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.cms.CMSAdapterFactory;
import mitm.common.security.cms.CMSEnvelopedDataAdapter;
import mitm.common.security.cms.CMSEnvelopedInspector;
import mitm.common.security.cms.CMSEnvelopedInspectorImpl;
import mitm.common.security.cms.CryptoMessageSyntaxException;
import mitm.common.security.cms.KeyNotFoundException;
import mitm.common.security.cms.RecipientInfo;
import mitm.common.security.keystore.BasicKeyStore;

import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSException;

public class SMIMEEnvelopedInspectorImpl implements SMIMEEnvelopedInspector
{
    private final CMSEnvelopedInspector envelopedInspector;

    public SMIMEEnvelopedInspectorImpl(Part envelopedPart, BasicKeyStore basicKeyStore, String nonSensitiveProvider, 
            String sensitiveProvider) 
    throws SMIMEInspectorException, MessagingException 
    {
        try {
            envelopedInspector = new CMSEnvelopedInspectorImpl(getEnvelopedDataAdapter(envelopedPart), basicKeyStore, 
                    nonSensitiveProvider, sensitiveProvider);
        }
        catch (CMSException e) {
            throw new SMIMEInspectorException(e);
        }
        catch (IOException e) {
            throw new SMIMEInspectorException(e);
        }
    }
    
    protected CMSEnvelopedDataAdapter getEnvelopedDataAdapter(Part envelopedPart) 
    throws MessagingException, CMSException, IOException
    {
        CMSEnvelopedData enveloped = new CMSEnvelopedData(envelopedPart.getInputStream());
        
        CMSEnvelopedDataAdapter envelopedAdapter = CMSAdapterFactory.createAdapter(enveloped);
        
        return envelopedAdapter;
    }
    
    @Override
    public List<RecipientInfo> getRecipients()
    throws CryptoMessageSyntaxException
    {
        return envelopedInspector.getRecipients();
    }

    @Override
    public String getEncryptionAlgorithmOID() {
        return envelopedInspector.getEncryptionAlgorithmOID();
    }
    
    @Override
    public AlgorithmParameters getEncryptionAlgorithmParameters()
    throws CryptoMessageSyntaxException
    {
        return envelopedInspector.getEncryptionAlgorithmParameters();
    }

    @Override
    public AttributeTable getUnprotectedAttributes() 
    throws CryptoMessageSyntaxException
    {
        return envelopedInspector.getUnprotectedAttributes();
    }

    @Override
    public byte[] getContent()
    throws KeyNotFoundException, CryptoMessageSyntaxException
    {
        return envelopedInspector.getContent();
    }
    
    @Override
    public InputStream getContentStream()
    throws KeyNotFoundException, CryptoMessageSyntaxException
    {
        return envelopedInspector.getContentStream();
    }
    
    @Override
    public MimeBodyPart getContentAsMimeBodyPart()
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException
    {
        MimeBodyPart mimeBodyPart = new MimeBodyPart(getContentStream());
        
        return mimeBodyPart;
    }

    @Override
    public MimeMessage getContentAsMimeMessage()
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException 
    {
        MimeMessage decryptedMessage = MailUtils.loadMessage(getContentStream());
        
        return decryptedMessage;
    }
}
