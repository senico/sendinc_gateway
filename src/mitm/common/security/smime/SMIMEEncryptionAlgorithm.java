/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.security.AlgorithmParameters;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.spec.RC2ParameterSpec;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The encryption algorithms that are supported by the s/mime modules.
 * 
 * @author Martijn Brinkers
 *
 * Note: IDEA was here as well but it seems that IDEA is patented so it's removed
 */
public enum SMIMEEncryptionAlgorithm 
{    
    AES256_CBC      (NISTObjectIdentifiers.id_aes256_CBC,                "AES256",      true,  256),
    AES192_CBC      (NISTObjectIdentifiers.id_aes192_CBC,                "AES192",      true,  192),
    AES128_CBC      (NISTObjectIdentifiers.id_aes128_CBC,                "AES128",      true,  128),
    DES_EDE3_CBC    (PKCSObjectIdentifiers.des_EDE3_CBC,                 "3DES",        true,  168),
    RC2_CBC         (PKCSObjectIdentifiers.RC2_CBC,                      "RC2",         false, 128),
    CAST5_CBC       (new ASN1ObjectIdentifier("1.2.840.113533.7.66.10"), "CAST5",       false, 128),
    CAMELLIA256_CBC (NTTObjectIdentifiers.id_camellia256_cbc,            "CAMELLIA256", true,  256),
    CAMELLIA192_CBC (NTTObjectIdentifiers.id_camellia192_cbc,            "CAMELLIA192", true,  192),
    CAMELLIA128_CBC (NTTObjectIdentifiers.id_camellia128_cbc,            "CAMELLIA128", true,  128),
    SEED_CBC        (KISAObjectIdentifiers.id_seedCBC,                   "SEED",        true,  128);
 
    private final static Logger logger = LoggerFactory.getLogger(SMIMEEncryptionAlgorithm.class);
    
    private final ASN1ObjectIdentifier oid; 
    private final String algorithm;
    private final String friendlyName;
    private final boolean fixedSize;
    private final int defaultKeySize;
    
    private SMIMEEncryptionAlgorithm(ASN1ObjectIdentifier oid, String friendlyName, boolean fixedSize, int defaultKeySize)
    {
        this.oid = oid;
        this.algorithm = oid.getId();
        this.friendlyName = friendlyName;
        this.fixedSize = fixedSize;
        this.defaultKeySize = defaultKeySize;
    }
    
    public String getName() {
        return friendlyName;
    }
    
    public ASN1ObjectIdentifier getOID() {
        return oid;
    }
    
    public String getAlgorithm() {
        return algorithm;
    }
    
    public boolean isFixedSize() {
        return fixedSize;
    }
    
    public int defaultKeySize() {
        return defaultKeySize;
    }
    
    public static SMIMEEncryptionAlgorithm fromOID(String oid)
    {
        for (SMIMEEncryptionAlgorithm algorithm : SMIMEEncryptionAlgorithm.values())
        {
            if (algorithm.oid.getId().equals(oid)) {
                return algorithm;
            }
        }
        
        return null;
    }
    
    public static SMIMEEncryptionAlgorithm fromName(String name)
    {
        name = StringUtils.trimToNull(name);
        
        if (name != null)
        {
            for (SMIMEEncryptionAlgorithm algorithm : SMIMEEncryptionAlgorithm.values())
            {
                if (algorithm.getName().equalsIgnoreCase(name)) {
                    return algorithm;
                }
            }
        }
        
        return null;
    }
    
    /**
     * Returns the effective key size (in bits) of the given algorithm. Returns -1 if the key size
     * cannot be determined.  
     * @param algorithm
     * @param parameters
     * @return
     */
    public static int getKeySize(SMIMEEncryptionAlgorithm algorithm, AlgorithmParameters parameters) 
    {
        int strength = -1;
        
        if (algorithm == null) {
            return strength;
        }
        
        if (algorithm.fixedSize) {
            strength = algorithm.defaultKeySize();
        }
        else {
            if (parameters != null) {
                if (algorithm == RC2_CBC) 
                {
                    try {
                        RC2ParameterSpec rc2Params = parameters.getParameterSpec(
                                RC2ParameterSpec.class);
                        
                        strength = rc2Params.getEffectiveKeyBits();
                    }
                    catch (InvalidParameterSpecException e) {
                        logger.error("RC2ParameterSpec is invalid.", e);
                    }
                }
                // TODO: add CAST5_CBC support
            }
        }
        
        return strength;
    }
    
    @Override
    public String toString() {
        return friendlyName;
    }
}
