/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.selector;

import java.security.cert.CertStoreException;
import java.util.HashSet;
import java.util.Set;

import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EmailSelector is a helper class that selects certificates from a X509CertStoreExt based
 * on the given matcher and whether the certificate should contain an associated private key
 * (to be precise it should have a key alias which normally is the alias of the key stored
 * in the associated KeyStore).
 * 
 * @author Martijn Brinkers
 *
 */
public class EmailSelector
{
    private final static Logger logger = LoggerFactory.getLogger(EmailSelector.class);
    
    /*
     * The store to get the certificates from
     */
    private final X509CertStoreExt certStore;
    
    /*
     * The MatchListener (callback) will determine whether a certificate is acceptable or not
     */
    private final MatchListener matchListener;
    
    private final MissingKeyAlias missingKeyAlias;
    
    /*
     * Determines whether expired certificates are accepted
     */
    private Expired allowExpired = Expired.NOT_ALLOWED; 
    
    /*
     * Sets a maximum to the number of certificates that will be returned.
     */
    private int maxMatch = 100;
    
    public static interface MatchListener {
        public boolean match(X509CertStoreEntry certStoreEntry);
    }
    
    public EmailSelector(X509CertStoreExt certStore, MatchListener matchListener, MissingKeyAlias missingKeyAlias) 
    {
    	Check.notNull(certStore, "certStore");
    	
        this.certStore = certStore;
        this.matchListener = matchListener;
        this.missingKeyAlias = missingKeyAlias;
    }

    public void setMaxMatch(int maxMatch) {
        this.maxMatch = maxMatch;
    }
    
    public void setAllowExpired(Expired allowExpired) {
        this.allowExpired = allowExpired;
    }
    
    protected boolean match(X509CertStoreEntry certStoreEntry) 
    {
        boolean match = false;
        
        if (matchListener != null) {
            match = matchListener.match(certStoreEntry);
        }
        
        return match;
    }
    
    public Set<X509CertStoreEntry> getMatchingEntries(String email)
    {
        Set<X509CertStoreEntry> matchingEntries = new HashSet<X509CertStoreEntry>();
        
        CloseableIterator<? extends X509CertStoreEntry> certStoreIterator = null;
        /*
         * Get all valid certificates with matching email address
         */
        try {
            certStoreIterator = certStore.getByEmail(email, Match.EXACT, allowExpired, missingKeyAlias);
        }
        catch (CertStoreException e) {
            logger.error("Error executing getByEmail.", e);
        }
        
        if (certStoreIterator != null)
        {
            try {
                try {
                    while (certStoreIterator.hasNext())
                    {
                        X509CertStoreEntry certStoreEntry = certStoreIterator.next();
                        
                        if (certStoreEntry != null && !matchingEntries.contains(certStoreEntry)) 
                        {
                            if (match(certStoreEntry)) {
                                matchingEntries.add(certStoreEntry);
                            }
                        }
                        
                        if (matchingEntries.size() >= maxMatch) 
                        {
                            break;
                        }
                    }
                }
                finally {
                    certStoreIterator.close();
                }
            }
            catch(CloseableIteratorException e) {
                logger.error("Error while iterating.", e);
            }
        }
        
        return matchingEntries;
    }
}
