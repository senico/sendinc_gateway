/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.selector;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Set;

import mitm.common.security.KeyAndCertificate;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.certificate.validator.CertificateValidatorChain;
import mitm.common.security.certificate.validator.IsValidForSMIMESigning;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SigningKeyAndCertificateSelector is a CertificateSelector implementation that selects certificates
 * used for digital signatures for a specific email address. The certificate is checked to see if the 
 * certificate is valid, trusted and can be used for email signing.
 * 
 * @author Martijn Brinkers
 *
 */
public class SigningKeyAndCertificateSelector implements KeyAndCertificateSelector
{
    private final static Logger logger = LoggerFactory.getLogger(SigningKeyAndCertificateSelector.class);
    
    private final PKISecurityServices pKISecurityServices;
    private final EmailSelector emailSelector;
    
    public SigningKeyAndCertificateSelector(PKISecurityServices pKISecurityServices)
    {
    	Check.notNull(pKISecurityServices, "pKISecurityServices");
    	
        this.pKISecurityServices = pKISecurityServices;
        
        EmailSelector.MatchListener matchListener = new EmailSelector.MatchListener()
        {
            @Override
            public boolean match(X509CertStoreEntry certStoreEntry)
            {
                return SigningKeyAndCertificateSelector.this.match(certStoreEntry);
            }
        };
        
        emailSelector = new EmailSelector(pKISecurityServices.getKeyAndCertStore(), matchListener, 
                MissingKeyAlias.NOT_ALLOWED);
    }
    
    public void setMaxCertificates(int maxCertificates) {
        emailSelector.setMaxMatch(maxCertificates);
    }
    
    private boolean match(X509CertStoreEntry certStoreEntry)
    {
        boolean match = false;
     
        if (certStoreEntry != null && certStoreEntry.getCertificate() != null)
        {
            X509Certificate certificate = certStoreEntry.getCertificate();
            
            CertificateValidatorChain chain = new CertificateValidatorChain();
            
            /*
             * Add the following CertificateValidators to the chain so we can check if the certificate is
             * trusted, not revoked and can be used for S/MIME signing.
             */
            chain.addValidators(new IsValidForSMIMESigning());
            chain.addValidators(pKISecurityServices.getPKITrustCheckCertificateValidatorFactory().
            		createValidator(null));
            
            try {
                match = chain.isValid(certificate);
            }
            catch (CertificateException e) {
                logger.error("Error validating certificate.", e);
            }
        }
        
        return match;
    }
    
    @Override
    public Set<KeyAndCertificate> getMatchingKeyAndCertificates(String email)
    {
        Set<KeyAndCertificate> keyAndCertificates = new HashSet<KeyAndCertificate>();
        
        Set<X509CertStoreEntry> matchingEntries = emailSelector.getMatchingEntries(email);
        
        for (X509CertStoreEntry certStoreEntry : matchingEntries)
        {
            KeyAndCertificate keyAndCertificate;
            
            try {
                keyAndCertificate = pKISecurityServices.getKeyAndCertStore().getKeyAndCertificate(certStoreEntry);
            }
            catch (CertStoreException e) {
                logger.error("Error getting getKeyAndCertificate.", e);
                
                continue;
            }
            catch (KeyStoreException e) {
                logger.error("Error getting getKeyAndCertificate.", e);
                
                continue;
            }
            
            if (keyAndCertificate != null && keyAndCertificate.getPrivateKey() != null)
            {
                keyAndCertificates.add(keyAndCertificate);
            }
        }
        
        return keyAndCertificates;
    }
}
