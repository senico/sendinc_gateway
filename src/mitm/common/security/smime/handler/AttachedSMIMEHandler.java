/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.PartException;
import mitm.common.mail.PartScanner;
import mitm.common.mail.PartScanner.PartListener;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.NotHeaderNameMatcher;
import mitm.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sometimes S/MIME messages are modified by content scanners or disclaimer services (SMTP servers that add a disclaimer).
 * Most non S/MIME aware disclaimer servers add a banner to the message and change the content-type of the message to 
 * a multipart/mixed. The encrypted part gets added as an attachment. The message is no longer a valid S/MIME message and
 * SMTP clients (like Outlook) are no longer able to digest the message. AttachedSMIMEHandler tries to create a valid 
 * S/MIME message from the attached encrypted blob by changing the content-type of the attachment and attach the blob
 * as an encrypted message (a RFC 822 attachment). Now Outlook and other SMTP clients are able to read the S/MIME message. 
 * 
 * 
 * @author Martijn Brinkers
 *
 */
public class AttachedSMIMEHandler
{
    private final static Logger logger = LoggerFactory.getLogger(AttachedSMIMEHandler.class);
    
    private final PartHandler partHandler;

    /*
     * Name of the attachment given to the attached RFC822 message when a S/MIME blob
     * is converted to an attached message (RFC822 attachment)
     */
    private String messageFilename = "attached message.eml";
    
    /*
     * Collection of S/MIME parts found in the source message
     */
    private List<MimeMessage> sMIMEMessages = new LinkedList<MimeMessage>();
    
    /*
     * Collection of non S/MIME parts found in the source message
     */
    private List<Part> nonSMIMEParts = new LinkedList<Part>();
    
    public static interface PartHandler {
        public MimeMessage handlePart(Part source)
        throws SMIMEHandlerException; 
    }
    
    public AttachedSMIMEHandler(PartHandler partHandler)
    {
    	Check.notNull(partHandler, "partHandler");
    	
        this.partHandler = partHandler;
    }
    
    /*
     * Sets the name of the rfc822 file that gets attached when the message contains more than one 
     * body item.
     */
    public void setMessageFilename(String filename) {
        this.messageFilename = filename;
    }

    @SuppressWarnings("unchecked")
    private void copyNonContentHeaders(Part source, Part target) 
    throws MessagingException
    {
        /* we only want to copy headers from source to target that are not already in the target */
        String[] protectedHeaders = HeaderUtils.getMatchingHeaderNames(target.getAllHeaders(), 
                source.getAllHeaders());

        HeaderMatcher contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeaders);

        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);
       
        /* copy all non-content headers from source message to the new message */
        HeaderUtils.copyHeaders(source, target, nonContentMatcher);    
    }
        
    private boolean onPart(Part parent, Part part)
    throws PartException
    {
        MimeMessage newMessage = null;
        
        try {
            /* if the part is a attached message (rfc822) we should inspect the attached message */
            if (part.isMimeType("message/rfc822")) 
            {
                try {
                    Part messagePart = BodyPartUtils.extractFromRFC822(part);
    
                    newMessage = partHandler.handlePart(messagePart);
                }
                catch (MessagingException e) {
                    logger.warn("Unable to handle attached RFC822 message.", e);
                }
                catch (IOException e) {
                    logger.warn("Unable to handle attached RFC822 message.", e);
                }                
            }
            else {
                newMessage = partHandler.handlePart(part);
                
                if (newMessage != null)
                {
                    /* The part should have the headers of the parent */
                    copyNonContentHeaders(parent, newMessage);
                }
            }
        }
        catch (MessagingException e) {
            throw new PartException(e);
        }
        catch (SMIMEHandlerException e) {
            throw new PartException(e);
        }
        
        if (newMessage != null) {
            /* part was an S/MIME message. */
            sMIMEMessages.add(newMessage);
        }
        else {
            /* part was not S/MIME. */
            
            boolean addPart = true;
            
            try {
                /* If the part does not contain relevant data we do not have to add it. */ 
                if (part.isMimeType("text/*")) 
                {
                    try {
                        Object content = part.getContent();
                        
                        if (content instanceof String) 
                        {
                            String s = (String) content;
                            
                            if (s.trim().length() == 0) 
                            {
                                logger.debug("Part is empty.");
                                
                                addPart = false;
                            }
                        }
                    } catch(UnsupportedEncodingException e) {
                        /* ignore */
                    }
                } 
                else {
                    if (part.getSize() == 0) 
                    {
                        logger.debug("Part size is 0.");

                        addPart = false;
                    }
                }
            }
            catch (IOException e) {
                logger.error("Error investigating part.", e);
                
                addPart = false;
            }
            catch (MessagingException e) {
                logger.error("Error investigating part.", e);
                
                addPart = false;
            }
            
            if (addPart) {
                nonSMIMEParts.add(part);
            }
        }
        
        return true;
    }       
    
    /*
     * Build a message from S/MIME parts and non S/MIME parts
     */
    private MimeMessage buildMessage(Part parent)
    throws MessagingException, IOException
    {
    	if (sMIMEMessages.size() <= 0) {
    		throw new IllegalStateException("sMIMEMessages.size() <= 0");
    	}
    	
        MimeMessage newMessage = null;
        
        int bodyCount = sMIMEMessages.size() + nonSMIMEParts.size();
        
        if (bodyCount > 1) 
        {
            /* there is more than one S/MIME part so we need to create a multipart message */
            Multipart mp = new MimeMultipart();

            for (Part part : nonSMIMEParts)
            {
                MimeBodyPart bodyPart = BodyPartUtils.toMimeBodyPart(part);
                
                mp.addBodyPart(bodyPart);
            }

            for (MimeMessage message : sMIMEMessages)
            {
                MimeBodyPart bodyPart = BodyPartUtils.toRFC822(message, messageFilename);
                
                mp.addBodyPart(bodyPart);
            }
            
            newMessage = new MimeMessage(MailSession.getDefaultSession());
            
            newMessage.setContent(mp);
        }
        else {
            newMessage = sMIMEMessages.get(0);
        }
        
        copyNonContentHeaders(parent, newMessage);
        
        return newMessage;
    }
    
    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException
    {
        PartListener partListener = new PartScanner.PartListener() 
        {
            @Override
            public boolean onPart(Part parent, Part part, Object context)
            throws PartException
            {
                return AttachedSMIMEHandler.this.onPart(parent, part);
            }
        };

        PartScanner partScanner = new PartScanner(partListener, 1);

        MimeMessage newMessage = null;
        
        try {
            partScanner.scanPart(source);
    
            /* we will only return a message if there was a S/MIME message */
            if (sMIMEMessages.size() > 0) {
                newMessage = buildMessage(source);
            }
        }
        catch (MessagingException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (IOException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (PartException e) {
            throw new SMIMEHandlerException(e);
        }
        
        return newMessage;
    }
}
