/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.smime.SMIMEType;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RecursiveSMIMEHandler recursively handles S/MIME messages (i.e., it handles message with multiple S/MIME layers).
 * 
 * @author Martijn Brinkers
 *
 */
public class RecursiveSMIMEHandler
{
    private Logger logger = LoggerFactory.getLogger(RecursiveSMIMEHandler.class);
    
    /*
     * Provides PKI related functionality
     */
    private final PKISecurityServices securityServices;
    
    /*
     * If true security info (signed and/or encrypted) will be added to the headers
     */
    private boolean removeSignature;

    /*
     * If true security info (signed and/or encrypted) will be added to the headers
     */
    private boolean addInfo = true;

    /* 
     * sanity check to stop processing when a message contains too many parts
     */
    private int maxRecursion = 256;
    
    /*
     * If true and the message is encrypted the message will be decrypted
     */
    private boolean decrypt = true;

    /*
     * If true and the message is compressed the message will be decompressed
     */
    private boolean decompress = true;
    
    /*
     * Determines which headers will be encrypted and/or signed
     */
    private String[] protectedHeaders = ProtectedContentHeaderNameMatcher.DEFAULT_PROTECTED_CONTENT_HEADERS;
    
    /*
     * Listener which will be called with the certificates contained in the message
     */
    private CertificateCollectionEvent certificatesEventListener; 
    
    /*
     * SMIMEInfoHandler will be used to add meta inforamtion about the S/MIME layers to the message
     */
    private SMIMEInfoHandler smimeInfoHandler;
    
    public RecursiveSMIMEHandler(PKISecurityServices securityServices)
    {
    	Check.notNull(securityServices, "securityServices");
        
        this.securityServices = securityServices;
        
        this.smimeInfoHandler = new SMIMEInfoHandlerImpl(securityServices); 
    }
    
    /**
     * If true and the message was signed the signature will be removed. 
     */
    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRemoveSignature()
    {
        return removeSignature;
    }

    public void setMaxRecursion(int maxRecursion) {
        this.maxRecursion = maxRecursion;
    }
    
    /**
     * If true S/MIME security info will be added to the resulting message. 
     */
    public void setAddInfo(boolean addInfo) {
        this.addInfo = addInfo;
    }

    public boolean isAddInfo() {
        return addInfo;
    }

	public boolean isDecrypt() {
		return decrypt;
	}

	/**
     * If true and the message is encrypted the message will be decrypted
	 */
	public void setDecrypt(boolean decrypt) {
		this.decrypt = decrypt;
	}

	public boolean isDecompress() {
		return decompress;
	}

	/**
     * If true and the message is compressed the message will be decompressed
	 */
	public void setDecompress(boolean decompress) {
		this.decompress = decompress;
	}    

	/**
     * Listener which will be called with the certificates contained in the message
	 */
    public void setCertificatesEventListener(CertificateCollectionEvent event) {
        this.certificatesEventListener = event;
    }

    private SMIMEHandler createSMIMEHandler(int level)
    {
        SMIMEHandler sMIMEHandler = new SMIMEHandler(securityServices);
        
        sMIMEHandler.setDecrypt(decrypt);
        sMIMEHandler.setDecompress(decompress);
        sMIMEHandler.setRemoveSignature(isRemoveSignature());
        sMIMEHandler.setCertificatesEventListener(certificatesEventListener);
        sMIMEHandler.setProtectedHeaders(protectedHeaders);
        
        return sMIMEHandler;
    }
        
    private AttachedSMIMEHandler.PartHandler createAttachedPartHandler(int recursionCount)
    {
        return new AttachedPartHandler(recursionCount);
    }
    
    private MimeMessage internalHandlePart(Part source, int level, int recursionCount)
    throws SMIMEHandlerException 
    {
        logger.debug("internalHandlePart");
        
        level++;
        recursionCount++;
        
        if (recursionCount > maxRecursion) 
        {
            logger.warn("Maximum recursion depth reached.");
            
            return null;
        }
        
        SMIMEHandler sMIMEHandler = createSMIMEHandler(level);
        
        MimeMessage newMessage = sMIMEHandler.handlePart(source);
        
        if (newMessage != null) 
        {
            if (addInfo)
            {
                Check.notNull(smimeInfoHandler, "smimeInfoHandler");
                
                newMessage = smimeInfoHandler.handle(newMessage, sMIMEHandler, 
                        level - 1 /* -1 because level starts with 1 */);
            }
            
            /*
             * we need to check whether we need recursively handle the message. If the message
             * is signed but the signature is not removed we do not need to continue because
             * we cannot change the message. If the message was encrypted but the message
             * could not be decrypted there is no need to continue.
             */
            if ((sMIMEHandler.getSMIMEType() != SMIMEType.SIGNED || removeSignature) && 
                (sMIMEHandler.getSMIMEType() != SMIMEType.ENCRYPTED || sMIMEHandler.isDecrypted()))
            {
                MimeMessage recursiveMessage = internalHandlePart(newMessage, level, recursionCount);
                
                if (recursiveMessage != null) 
                {
                    logger.debug("Recursive S/MIME message.");
                    
                    newMessage = recursiveMessage;
                }
            }
        }
        else {
            /*
             * it's not a S/MIME message but it can still contain S/MIME message if it's multipart/mixed
             * or rfc822 message
             */
            try {
                if (source.isMimeType("multipart/mixed") || source.isMimeType("message/rfc822")) 
                {
                    AttachedSMIMEHandler attachedHandler = new AttachedSMIMEHandler(createAttachedPartHandler(recursionCount));
                    
                    newMessage = attachedHandler.handlePart(source);
                }
            }
            catch (MessagingException e) {
                throw new SMIMEHandlerException(e);
            }
        }
        
        return newMessage;
    }
    
    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException 
    {
        return internalHandlePart(source, 0, 0);
    }
    
    private class AttachedPartHandler implements AttachedSMIMEHandler.PartHandler
    {
        private final int recursionCount;
        
        public AttachedPartHandler(int recursionCount) {
            this.recursionCount = recursionCount;
        }
        
        @Override
        public MimeMessage handlePart(Part source)
        throws SMIMEHandlerException
        {
            /* 
             * nested S/MIME message should start with level 0 
             */
            return internalHandlePart(source, 0, recursionCount);
        }        
    }

    public String[] getProtectedHeaders() {
        return protectedHeaders;
    }

    public void setProtectedHeaders(String[] protectedHeaders) {
        this.protectedHeaders = protectedHeaders;
    }

    public SMIMEInfoHandler getSmimeInfoHandler() {
        return smimeInfoHandler;
    }

    public void setSmimeInfoHandler(SMIMEInfoHandler smimeInfoHandler) {
        this.smimeInfoHandler = smimeInfoHandler;
    }
}
