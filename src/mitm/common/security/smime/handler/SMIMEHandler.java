/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import java.io.IOException;
import java.security.cert.CertificateException;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.NotHeaderNameMatcher;
import mitm.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.cms.CryptoMessageSyntaxException;
import mitm.common.security.cms.KeyNotFoundException;
import mitm.common.security.smime.SMIMEHeader;
import mitm.common.security.smime.SMIMEInspector;
import mitm.common.security.smime.SMIMEInspectorException;
import mitm.common.security.smime.SMIMEInspectorImpl;
import mitm.common.security.smime.SMIMEType;
import mitm.common.util.Check;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SMIMEHandler handles S/MIME encrypted, signed or compressed messages. Encrypted messages will be decrypted,
 * compressed message will be decompressed and certificates will be extracted from signed messages. Depending
 * on certain settings signatures from signed message can be removed and security info, like if the message
 * was signed and if so by who, can be added to a message. SMIMEHandler only handles one S/MIME layer, ie.
 * if a message is signed and then encrypted SMIMEHandler will only remove the encryption layer. SMIMEHandler
 * should be used multiple times to handle all S/MIME layers. See RecursiveSMIMEHandler for a handler that
 * recursively handles all S/MIME layers.
 * 
 * Note: This class is not thread safe
 * 
 * @author Martijn Brinkers
 *
 */
public class SMIMEHandler
{
    private Logger logger = LoggerFactory.getLogger(SMIMEHandler.class);
    
    private final PKISecurityServices securityServices;
    
    /*
     * Factory that creates security related instances
     */
    private final SecurityFactory securityFactory;
    
    /*
     * If true and the message is signed the signature will be removed
     */
    private boolean removeSignature;
    
    /*
     * True if the message was decrypted
     */
    private boolean decrypted;
    
    /*
     * Tye S/MIME type of the message (signed, encrypted etc.)
     */
    private SMIMEType smimeType;

    /*
     * If true and the message is encrypted the message will be decrypted
     */
    private boolean decrypt = true;

    /*
     * If true and the message is compressed the message will be decompressed
     */
    private boolean decompress = true;
    
    /*
     * If headers are signed or encrypted, which headers should be copied to the outer message
     */
    private String[] protectedHeaders = ProtectedContentHeaderNameMatcher.DEFAULT_PROTECTED_CONTENT_HEADERS;
    
    /*
     * The SMIMEInspector can be used to inspect the S/MIME message
     */
    private SMIMEInspector inspector;
    
    /*
     * Listener that gets called when certificate are extracted from a message.
     */
    private CertificateCollectionEvent certificatesEventListener; 
    
    public SMIMEHandler(PKISecurityServices securityServices)
    {
    	Check.notNull(securityServices, "securityServices");
    	Check.notNull(securityServices.getKeyAndCertStore(), "securityServices.getKeyAndCertStore()");
    	        
        this.securityServices = securityServices;
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }
    
    /**
     * If true and the message was signed the signature will be removed. 
     */
    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRemoveSignature()
    {
        return removeSignature;
    }

    /**
     * Sets the listener that gets called when certificate are extracted from a message.
     */    
    public void setCertificatesEventListener(CertificateCollectionEvent event) {
        this.certificatesEventListener = event;
    }
    
    /**
     * True if the message was encrypted  and the message was successfully decrypted. 
     * This property is only valid when smimeType is ENCRYPTED. 
     */
    public boolean isDecrypted() {
        return decrypted;
    }

    public SMIMEType getSMIMEType() {
        return smimeType;
    }
    
    @SuppressWarnings("unchecked")
    private void copyNonContentHeaders(Part source, Part target) 
    throws MessagingException
    {
        HeaderMatcher contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeaders);

        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);
        
        /*
         * We do not want to allow the protected blob to add unknown headers to the message 
         * so we first want to remove all headers accept content headers and some protected 
         * headers (like subject, to etc.) from the target.
         */
        HeaderUtils.removeHeaders(target, nonContentMatcher); 
        
        /*
         * we only want to copy some protected headers from source to target if the target does not
         * have these headers.
         */
        String[] protectedHeadersToCopy = HeaderUtils.getMatchingHeaderNames(target.getAllHeaders(), 
                protectedHeaders);

        contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeadersToCopy);

        nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);
       
        /* copy all non-content headers from source message to the new message */
        HeaderUtils.copyHeaders(source, target, nonContentMatcher);    
    }
    
    private MimeMessage handleSigned(Part source, SMIMEInspector inspector) 
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException, IOException, CertificateException
    {
        logger.debug("Handling signed message.");
        
        if (inspector.getSMIMEType() != SMIMEType.SIGNED) {
        	throw new IllegalStateException("SMIMEType.SIGNED expected");
        }
        
        MimeMessage newMessage = null;
        
        if (removeSignature) 
        {
            newMessage = inspector.getContentAsMimeMessage();
            
            copyNonContentHeaders(source, newMessage);
        }
        
        if (certificatesEventListener != null) {
            certificatesEventListener.certificatesEvent(inspector.getSignedInspector().getCertificates());
        }
        
        if (newMessage == null)
        {
            /* we need to make a clone of the source message. We always want the return a new instance. */
            newMessage = BodyPartUtils.toMessage(source, true);
            
            /* we may have to 'repair' the content type if it's not the correct type for S/MIME */
            SMIMEHeader.Type sMIMEType = SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES);
            
            if (sMIMEType != SMIMEHeader.Type.OPAQUE_SIGNED && !newMessage.isMimeType("multipart/*")) 
            {
                newMessage.setHeader("Content-Type", "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=signed-data");
                newMessage.setHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
                newMessage.setHeader("Content-Description", "S/MIME Cryptographic Signed Data");
            }            
        }
        
        return newMessage;
    }
    
    private MimeMessage handleEncrypted(Part source, SMIMEInspector inspector) 
    throws MessagingException, CryptoMessageSyntaxException, IOException
    {
        logger.debug("Handling encrypted message.");

        if (inspector.getSMIMEType() != SMIMEType.ENCRYPTED) {
        	throw new IllegalStateException("SMIMEType.ENCRYPTED expected");
        }
        
        MimeMessage newMessage = null;
        
        try {
            newMessage = inspector.getContentAsMimeMessage();

            copyNonContentHeaders(source, newMessage);
            
            decrypted = true;
        }
        catch (KeyNotFoundException e) {
            logger.warn("A decryption key for the message could not be found.");
        }

        if (newMessage == null) 
        {
            /* we need to make a clone of the source message. We always want to return a new instance. */
            newMessage = BodyPartUtils.toMessage(source, true);
            
            /* we may have to 'repair' the content type if it's not the correct type for S/MIME */
            if (SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES) != SMIMEHeader.Type.ENCRYPTED) 
            {
                newMessage.setHeader("Content-Type", "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data");
                newMessage.setHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
                newMessage.setHeader("Content-Description", "S/MIME Encrypted Message");
            }
        }
        
        return newMessage;
    }

    private MimeMessage handleCompressed(Part source, SMIMEInspector inspector) 
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException, IOException
    {
        logger.debug("Handling compressed message.");
        
        if (inspector.getSMIMEType() != SMIMEType.COMPRESSED) {
        	throw new IllegalStateException("SMIMEType.COMPRESSED expected");
        }
        
        MimeMessage newMessage = inspector.getContentAsMimeMessage();
                    
        copyNonContentHeaders(source, newMessage);
        
        return newMessage;
    }
        
    /**
     * The message will be decrypted if encrypted. The message will be decompressed if compressed. 
     * The signature will be removed if signed and removeSignature is true. If the message is not
     * a S/MIME message null is returned. If the message is encrypted but a suitable decryption
     * key could not be found the message will not be decrypted but returned encrypted.
     */
    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException 
    {
        try {
        	inspector = new SMIMEInspectorImpl(source, securityServices.getKeyAndCertStore(), 
        			securityFactory.getNonSensitiveProvider(), 
        			securityFactory.getSensitiveProvider());

            smimeType = inspector.getSMIMEType();
            
            if (smimeType == SMIMEType.NONE) {
                return null;
            }
            
            MimeMessage message;
        
            switch (smimeType)
            {
            case SIGNED     : message = handleSigned(source, inspector); break;
            case ENCRYPTED  : message = decrypt ? handleEncrypted(source, inspector) : null; break;
            case COMPRESSED : message = decompress ? handleCompressed(source, inspector) : null; break;
            
            default:
                throw new IllegalArgumentException("Unknown S/MIME type " + smimeType);
            }
            
            return message;
        }
        catch (KeyNotFoundException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (CryptoMessageSyntaxException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (CertificateException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (MessagingException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (IOException e) {
            throw new SMIMEHandlerException(e);
        }
        catch (SMIMEInspectorException e) {
            throw new SMIMEHandlerException(e);
        }
    }

	public boolean isDecrypt() {
		return decrypt;
	}

	public void setDecrypt(boolean decrypt) {
		this.decrypt = decrypt;
	}

	public boolean isDecompress() {
		return decompress;
	}

	public void setDecompress(boolean decompress) {
		this.decompress = decompress;
	}    
	
	public SMIMEInspector getSMIMEInspector() {
	    return inspector;
	}

    public String[] getProtectedHeaders() {
        return protectedHeaders;
    }

    public void setProtectedHeaders(String... protectedHeaders) {
        this.protectedHeaders = protectedHeaders;
    }
}
