/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.BodyPartUtils;
import mitm.common.security.cms.CryptoMessageSyntaxException;
import mitm.common.security.cms.KeyNotFoundException;
import mitm.common.security.keystore.BasicKeyStore;
import mitm.common.util.Check;

public class SMIMEInspectorImpl implements SMIMEInspector
{
    private SMIMESignedInspector signedInspector;
    private SMIMEEnvelopedInspector envelopedInspector;
    private SMIMECompressedInspector compressedInspector;

    private final Part part;
    private final SMIMEType type;

    public SMIMEInspectorImpl(Part part, BasicKeyStore basicKeyStore, String provider)
    throws MessagingException, IOException, SMIMEInspectorException
    {
    	this (part, basicKeyStore, provider, provider);
    }

    public SMIMEInspectorImpl(Part part, BasicKeyStore basicKeyStore, String nonSensitiveProvider,
    		String sensitiveProvider)
    throws MessagingException, IOException, SMIMEInspectorException
    {
    	Check.notNull(part, "part");

        this.part = part;

        type = SMIMEUtils.getSMIMEType(part);

        switch(type) {
        case SIGNED:
            signedInspector = new SMIMESignedInspectorImpl(part, nonSensitiveProvider, sensitiveProvider);
            break;
        case ENCRYPTED:
            envelopedInspector = new SMIMEEnvelopedInspectorImpl(part, basicKeyStore, nonSensitiveProvider,
                    sensitiveProvider);
            break;
        case COMPRESSED:
            compressedInspector = new SMIMECompressedInspectorImpl(part);
            break;

        default: break;
        }
    }

    @Override
    public SMIMEType getSMIMEType() {
        return type;
    }

    @Override
    public SMIMESignedInspector getSignedInspector() {
        return signedInspector;
    }

    @Override
    public SMIMEEnvelopedInspector getEnvelopedInspector() {
        return envelopedInspector;
    }

    @Override
    public SMIMECompressedInspector getCompressedInspector() {
        return compressedInspector;
    }

    /**
     * Returns the content as a MimeBodyPart. The content depends on the S/MIME type.
     */
    @Override
    public MimeBodyPart getContentAsMimeBodyPart()
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException
    {
        try {
            switch (getSMIMEType())
            {
            case SIGNED     : return getSignedInspector().getContent();
            case ENCRYPTED  : return getEnvelopedInspector().getContentAsMimeBodyPart();
            case COMPRESSED : return getCompressedInspector().getContentAsMimeBodyPart();
            case NONE       : return BodyPartUtils.toMimeBodyPart(part);
            default:
                throw new IllegalStateException("Unknown S/MIME type.");
            }
        }
        catch (IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }

    /**
     * Returns the content as a MimeMessage. The content depends on the S/MIME type.
     */
    @Override
    public MimeMessage getContentAsMimeMessage()
    throws KeyNotFoundException, MessagingException, CryptoMessageSyntaxException
    {
        try {
            switch (getSMIMEType())
            {
            case SIGNED     : return getSignedInspector().getContentAsMimeMessage();
            case ENCRYPTED  : return getEnvelopedInspector().getContentAsMimeMessage();
            case COMPRESSED : return getCompressedInspector().getContentAsMimeMessage();
            case NONE       : return BodyPartUtils.toMessage(part);
            default:
                throw new IllegalStateException("Unknown S/MIME type.");
            }
        }
        catch (IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }
}
