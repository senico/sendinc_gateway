/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityFactoryFactory
{
    private final static Logger logger = LoggerFactory.getLogger(SecurityFactoryFactory.class);
    
    /*
     * If the system property is set, the builder will be used to create the SecurityFactory instance.
     */
    private final static String SECURITY_FACTORY_BUILDER_SYSTEM_PROPERTY = "djigzo.security-factory.builder";
    
    /*
     * The SecurityFactory which is used to create security classes (like CertificateFactory etc.).
     */
    private static SecurityFactory factory;
    
    /**
     * Returns the system wide SecurityFactory
     */
    public static synchronized SecurityFactory getSecurityFactory()
    throws SecurityFactoryFactoryException
    {
        if (factory == null)
        {
            try {
                /*
                 * Check if a SecurityFactoryBuilder is specified in the system settings
                 */
                String builderClazz = System.getProperty(SECURITY_FACTORY_BUILDER_SYSTEM_PROPERTY);
                
                if (StringUtils.isNotEmpty(builderClazz))
                {
                    Class<?> builderClass = Class.forName(builderClazz);
                    
                    if (!SecurityFactoryBuilder.class.isAssignableFrom(builderClass)) {
                        throw new SecurityFactoryFactoryException(builderClazz + " is-not-a SecurityFactoryBuilder.");
                    }
                    
                    SecurityFactoryBuilder builder = (SecurityFactoryBuilder) builderClass.newInstance();
                    
                    factory = builder.createSecurityFactory();
                }
                else {
                    /*
                     * Use the default security factory
                     */
                    factory = new DefaultSecurityFactory();
                }

                logger.info("SecurityFactory instance created. Class: {}", factory.getClass());
            }
            catch (InstantiationException e) {
                throw new SecurityFactoryFactoryException(e);
            }
            catch (IllegalAccessException e) {
                throw new SecurityFactoryFactoryException(e);
            }
            catch (ClassNotFoundException e) {
                throw new SecurityFactoryFactoryException(e);
            }
        }
        
        return factory;
    }
    
    /**
     * Sets the new global SecurityFactory.
     */
    public static synchronized void setSecurityFactory(SecurityFactory securityFactory)
    {
        logger.info("Setting SecurityFactory {}", securityFactory.getClass());
        
        factory = securityFactory;
    }
}
