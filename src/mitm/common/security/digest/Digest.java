/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.digest;

import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;

public enum Digest 
{    
    MD5       (PKCSObjectIdentifiers.md5.getId(),            "MD5",        "MD5"),
    SHA1      (OIWObjectIdentifiers.idSHA1.getId(),          "SHA-1",      "SHA-1"),
    SHA224    (NISTObjectIdentifiers.id_sha224.getId(),      "SHA-224",    "SHA-224"),
    SHA256    (NISTObjectIdentifiers.id_sha256.getId(),      "SHA-256",    "SHA-256"),
    SHA384    (NISTObjectIdentifiers.id_sha384.getId(),      "SHA-384",    "SHA-384"),
    SHA512    (NISTObjectIdentifiers.id_sha512.getId(),      "SHA-512",    "SHA-512"),
    GOST3411  (CryptoProObjectIdentifiers.gostR3411.getId(), "GOST-3411",  "GOST-3411"),
    RIPEMD128 (TeleTrusTObjectIdentifiers.ripemd128.getId(), "RIPEMD-128", "RIPEMD-128"),
    RIPEMD160 (TeleTrusTObjectIdentifiers.ripemd160.getId(), "RIPEMD-160", "RIPEMD-160"),
    RIPEMD256 (TeleTrusTObjectIdentifiers.ripemd256.getId(), "RIPEMD-256", "RIPEMD-256");
    
    private final String oid;
    private final String name;
    private final String factoryName;
    
    private Digest(String oid, String name, String factoryName)
    {
        this.oid = oid;
        this.name = name;
        this.factoryName = factoryName;
    }
    
    public String getOID() {
        return oid;
    }

    public String getName() {
        return name;
    }
    
    public String getFactoryName() {
        return factoryName;
    }

    /**
     * Returns the Digest with the given OID. Null if there was no match.
     */
    public static Digest fromOID(String oid) 
    {
        for (Digest digest : Digest.values())
        {
            if (digest.getOID().equals(oid)) {
                return digest;
            }
        }
        
        return null;
    }

    /**
     * Returns the Digest with the given name. Null if there was no match.
     */
    public static Digest fromName(String name) 
    {
        for (Digest digest : Digest.values())
        {
            if (digest.getName().equalsIgnoreCase(name)) {
                return digest;
            }
        }
        
        return null;
    }
        
    @Override
    public String toString() {
        return name;
    }
}
