/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.security.Provider;
import java.security.Security;

import mitm.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import mitm.common.util.Check;

/**
 * General security algorithm utility class.
 * 
 * @author Martijn Brinkers
 *
 */
public class AlgorithmUtils
{
    /**
     * Returns the name of the algorithm with the given OID from the provider. If
     * algorithm is not found the OID is returned.
     * 
     */
    public static String getAlgorithmName(String oid, Provider provider)
    {
        Check.notNull(oid, "oid");

        String algorithmName = provider.getProperty("Alg.Alias.Signature." + oid);

        if (algorithmName != null) {
            return algorithmName;
        }
        
        return oid;
    }
    
    /**
     * Returns the name of the algorithm with the given OID. If
     * algorithm is not found the OID is returned.
     * 
     */
    public static String getAlgorithmName(String oid)
    {
        Check.notNull(oid, "oid");
        
        /*
         * First check whether the BC provider knows the name of the algorithm
         */
        Provider bcProvider = Security.getProvider(SecurityFactoryBouncyCastle.PROVIDER_NAME);

        if (bcProvider != null)
        {
            String algorithmName = getAlgorithmName(oid, bcProvider);

            if (!oid.equals(algorithmName))
            {
                return algorithmName;
            }
        }

        /*
         * Continue looking with the other providers
         */
        Provider[] providers = Security.getProviders();

        for (Provider provider : providers)
        {
            String algorithmName = getAlgorithmName(oid, provider);

            if (!oid.equals(algorithmName))
            {
                return algorithmName;
            }
        }

        return oid;        
    }
}
