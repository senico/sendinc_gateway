/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore.hibernate;

import java.io.Serializable;
import java.security.cert.Certificate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

@Entity(name = KeyStoreEntryHibernate.ENTITY_NAME)
@Table(name = "keystore",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"storeName", "alias"})}
)
@org.hibernate.annotations.Table(appliesTo = KeyStoreEntryHibernate.ENTITY_NAME, indexes = {
        @Index(name = "keystore_storename_index", columnNames = {"storeName"}),
        @Index(name = "keystore_alias_index"    , columnNames = {"alias"})
        }
)
public class KeyStoreEntryHibernate implements Serializable
{
    public static final String ENTITY_NAME = "KeyStore";

    private static final long serialVersionUID = 6370971993598648591L;

    /**
     * Maximum size of the store name.
     */
    private static final int MAX_STORE_NAME_LENGTH = 255;

    /**
     * Maximum size of the key alias.
     */
    private static final int MAX_ALIAS_LENGTH = 1024;

    /**
     * Maximum size of the thumbprint
     */
    private static final int MAX_THUMBPRINT_LENGTH = 255;

    /**
     * Maximum size of the certificate type
     */
    private static final int MAX_CERTIFICATE_TYPE_LENGTH = 255;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /*
     * The name of the store this entry belongs to.
     */
    @Column (name = "storeName", unique = false, nullable = false, length = MAX_STORE_NAME_LENGTH)
    private String storeName;

    /*
     * The certificate associated with this entry.
     */
    @Type(type = "mitm.common.hibernate.CertificateUserType")
    @Columns(columns = {
            @Column(name = "certificate"),
            @Column(name = "certificateType", length = MAX_CERTIFICATE_TYPE_LENGTH),
            @Column(name = "thumbprint", length = MAX_THUMBPRINT_LENGTH, unique = false)
    })
    private Certificate certificate;

    /*
     * The associated certificate chain.
     */
    @Type(type = "mitm.common.hibernate.CertificateArrayUserType")
    @Columns(columns = {
            @Column(name = "certificateChain")
    })
    private Certificate[] certificateChain;

    /*
     * The encoded key material.
     */
    @Column (name = "encodedKey", unique = false, nullable = true)
    byte[] encodedKey;

    /*
     * The alias of this key entry.
     */
    @Column (name = "alias", unique = false, nullable = false, length = MAX_ALIAS_LENGTH)
    private String alias;

    /*
     * Date this entry was created
     */
    @Type(type = "timestamp")
    @Column (name = "creationDate", unique = false, nullable = true)
    private Date creationDate;

    public KeyStoreEntryHibernate(String storeName, Certificate certificate, Certificate[] certificateChain, byte[] encodedKey,
            String alias, Date creationDate)
    {
        this.storeName = storeName;
        this.certificate = certificate;
        this.certificateChain = certificateChain;
        this.encodedKey = encodedKey;
        this.alias = alias;
        this.creationDate = creationDate;
    }

    public KeyStoreEntryHibernate(String storeName, String alias, Date creationDate)
    {
        this.storeName = storeName;
        this.alias = alias;
        this.creationDate = creationDate;
    }

    protected KeyStoreEntryHibernate() {
        /* Hibernate requires a default constructor */
    }

    public String getStoreName() {
        return storeName;
    }

    /**
     * Returns the certificate associated with this entry
     * @return the certificate
     */
    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * Returns the certificate chain associated with this entry.
     * @return the certificate chain
     */
    public Certificate[] getChain() {
        return certificateChain;
    }

    public void setChain(Certificate[] chain) {
        this.certificateChain = chain;
    }

    /**
     * Returns the raw (encoded) key material.
     * @return the encoded key
     */
    public byte[] getEncodedKey() {
        return encodedKey;
    }

    public void setEncodedKey(byte[] encodedKey) {
        this.encodedKey = encodedKey;
    }

    /**
     * Returns the alias of this entry.
     * @return the alias of this entry
     */
    public String getKeyAlias() {
        return alias;
    }

    /**
     * The date this entry was created.
     *
     * @return The date this entry was created.
     */
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date date) {
        this.creationDate = date;
    }

    /**
     * KeyStoreEntryHibernate is equal if and only if the alias and storeName are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof KeyStoreEntryHibernate)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        KeyStoreEntryHibernate rhs = (KeyStoreEntryHibernate) obj;

        return new EqualsBuilder()
            .append(alias, rhs.alias)
            .append(storeName, rhs.storeName)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(alias)
            .append(storeName)
            .toHashCode();
    }
}
