/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Helper class that can load a KeyStore
 * 
 * @author Martijn Brinkers
 *
 */
public class KeyStoreLoader
{
    /*
     * The key store type to use. If null, the key store type is
     * 'guessed' based on the input.
     */
    private String keyStoreType;
    
    /*
     * Mapping from extension to KeyStoreType
     */
    private final static Map<String, String> extensionMap = new HashMap<String, String>();
    
    {
        extensionMap.put("", "JKS");
        extensionMap.put("jks", "JKS");
        extensionMap.put("pfx", "PKCS12");
        extensionMap.put("p12", "PKCS12");
        extensionMap.put("uber", "UBER");
    }
    
    public KeyStore loadKeyStore(File file, char[] password)
    throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException
    {
        if (file == null) {
            throw new IOException("file cannot be null");
        }
        
        if (!file.exists()) {
            throw new FileNotFoundException("file does not exist");
        }
                
        if (StringUtils.isEmpty(keyStoreType)) {
            determineKeyStoreTypeFromFile(file);
        }
        
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                
        InputStream input = new FileInputStream(file);

        try {
            keyStore.load(input, password);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
             
        return keyStore;
    }

    private void determineKeyStoreTypeFromFile(File file)
    throws KeyStoreException
    {
        String extension = StringUtils.defaultString(StringUtils.lowerCase(
                FilenameUtils.getExtension(file.getName())));

        keyStoreType = extensionMap.get(extension);
        
        if (keyStoreType == null) {
            throw new KeyStoreException("Unable to determine key store type for extension " + extension);
        }
    }
    
    public String getKeyStoreType() {
        return keyStoreType;
    }

    public void setKeyStoreType(String keyStoreType) {
        this.keyStoreType = keyStoreType;
    }    
}
