/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.util.Check;

/**
 * An implementation of KeyStoreProvider that uses the default SecurityFactory for the creation of the KeyStore.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultSecurityFactoryKeyStoreProvider implements KeyStoreProvider
{
    /*
     * The KeyStore type to create (PKCS12, JKS etc.) 
     */
    private final String keyStoreType; 
    
    /*
     * The KeyStore password (can be null)
     */
    private char[] keyStorePassword;
    
    /*
     * The KeyStore instance
     */
    private KeyStore keyStore;
    
    public DefaultSecurityFactoryKeyStoreProvider(String keyStoreType)
    {
        Check.notNull(keyStoreType, "keyStoreType");
        
        this.keyStoreType = keyStoreType;
    }
    
    @Override
    public synchronized KeyStore getKeyStore()
    throws KeyStoreException
    {
        try {
            if (keyStore == null)
            {
                /*
                 * Use a local instance before setting the class instance. KeyStore#load might fail
                 */
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore(keyStoreType);

                keyStore.load(null, keyStorePassword);
                
                this.keyStore = keyStore;
            }
        } 
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        } 
        catch (SecurityFactoryFactoryException e) {
            throw new KeyStoreException(e);
        } 
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        } 
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        } 
        catch (IOException e) {
            throw new KeyStoreException(e);
        }

        return keyStore;
    }

    public void setKeyStorePassword(String keyStorePassword)
    {
        if (keyStorePassword != null) {
            this.keyStorePassword = keyStorePassword.toCharArray();
        }
    }    
}
