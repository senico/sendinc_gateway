/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore.dao;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.List;

import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.security.certificate.CertificateInspector;
import mitm.common.security.keystore.hibernate.KeyStoreEntryHibernate;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class KeyStoreDAOHibernate extends GenericHibernateDAO<KeyStoreEntryHibernate, Long> implements KeyStoreDAO
{
    private final String storeName;
    private static final String ENTITY_NAME = KeyStoreEntryHibernate.ENTITY_NAME;

    
    public KeyStoreDAOHibernate(String storeName, Session session) 
    {
        super(SessionAdapterFactory.create(session));
        
        this.storeName = storeName;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAliases()
    {
        final String hql = "select k.alias from " + ENTITY_NAME + " k where storeName = :storeName";
        
        Query query = createQuery(hql);
        
        query.setString("storeName", storeName);
        
        return query.list();
    }
    
    @Override
    public KeyStoreEntryHibernate getEntryByAlias(String alias)
    {
        Criteria criteria = createCriteria(ENTITY_NAME);
        
        criteria.add(Restrictions.eq("alias", alias));
        criteria.add(Restrictions.eq("storeName", storeName));
        
        return (KeyStoreEntryHibernate) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public KeyStoreEntryHibernate getEntryByCertificate(Certificate certificate) 
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        Criteria criteria = createCriteria(ENTITY_NAME);

        CertificateInspector inspector = new CertificateInspector(certificate);
        
        criteria.add(Restrictions.eq("certificate.thumbprint", inspector.getThumbprint()));
        criteria.add(Restrictions.eq("storeName", storeName));
        
        List<KeyStoreEntryHibernate> entries = criteria.list();
        
        KeyStoreEntryHibernate entry = null;
        
        /* get the first entry (if exist) */
        if (entries.size() > 0) {
            entry = entries.get(0);
        }
        
        return entry;
    }

    @Override
    public Long getEntryCount()
    {
        final String hql = "select count (*) from " + ENTITY_NAME + " k where storeName = :storeName";
        
        Query query = createQuery(hql);
        
        query.setString("storeName", storeName);
        
        return (Long) query.uniqueResult();
    }
}
