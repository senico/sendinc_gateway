/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.NoSuchProviderRuntimeException;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certpath.CertificatePathBuilder;
import mitm.common.util.CollectionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PKIXCRLPathBuilder implements CRLPathBuilder
{
    private final static Logger logger = LoggerFactory.getLogger(PKIXCRLPathBuilder.class);

    public final static String CRL_ISSUER_NOT_FOUND = "CRL issuer could not be found.";
    
    private final CertificatePathBuilder pathBuilder;
    private final SecurityFactory securityFactory;
    
    public PKIXCRLPathBuilder(CertificatePathBuilder pathBuilder)     
    {
        this.pathBuilder = pathBuilder;
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }
    
    /*
     * Returns true if the crl was signed with the private key associated 
     * with the provided public key
     */
    private boolean isSignedBy(X509CRL crl, PublicKey publicKey) 
    throws NoSuchProviderException
    {
        try {
            crl.verify(publicKey, securityFactory.getNonSensitiveProvider());
            
            return true;
        }
        catch (InvalidKeyException e) {
            logger.error("Error verifying CRL.", e);
        }
        catch (CRLException e) {
            logger.error("Error verifying CRL.", e);
        }
        catch (NoSuchAlgorithmException e) {
            logger.error("Error verifying CRL.", e);
        }
        catch (SignatureException e) {
            logger.debug("Error verifying CRL. Message: " + e.getMessage());
        }
        
        return false;
    }
    
    /*
     * Checks the trust anchors whether the CRL is signed by one of the trust anchors.
     */
    private TrustAnchor getPossibleTrustAnchorIssuer(X509CRL crl)
    throws CertPathBuilderException, NoSuchProviderException, CertStoreException
    {
        TrustAnchor foundTrustAnchor = null;
        
        X500Principal crlIssuer = crl.getIssuerX500Principal();
        
        for (TrustAnchor trustAnchor : pathBuilder.getTrustAnchors())
        {
            X500Principal trustAnchorSubject = (trustAnchor.getTrustedCert() != null ? 
                    trustAnchor.getTrustedCert().getSubjectX500Principal() : trustAnchor.getCA());
            
            if (trustAnchorSubject.equals(crlIssuer)) 
            {
                /* check if the CRL is signed by this trust anchor */
                
                PublicKey publicKey = (trustAnchor.getTrustedCert() != null ? trustAnchor.getTrustedCert().getPublicKey() : 
                        trustAnchor.getCAPublicKey());

                if (isSignedBy(crl, publicKey))
                {
                    foundTrustAnchor = trustAnchor;                    
                    break;
                }
            }
        }
        
        return foundTrustAnchor;
    }
    
    /*
     * Returns a set of certificates with subject equal to crl issuer.
     */
    private Set<X509Certificate> getPossibleIssuers(X509CRL crl)
    {
        Set<X509Certificate> possibleIssuers = new HashSet<X509Certificate>();
        
        Set<CertStore> stores = pathBuilder.getCertStores();
        
        X509CertSelector selector = new X509CertSelector();
        
        boolean[] crlKeyUsage = new boolean[]{false, false, false, false, false, 
                false, true /* cRLSign */, false, false};
        
        selector.setKeyUsage(crlKeyUsage);
        
        selector.setSubject(crl.getIssuerX500Principal());
        
        for (CertStore store : stores)
        {
            try {
                Collection<? extends Certificate> certificates = store.getCertificates(selector);
                
                CollectionUtils.copyCollectionFiltered(certificates, possibleIssuers, X509Certificate.class);
            }
            catch (CertStoreException e) {
                logger.error("Error while getting certificates.", e);
            }
        }
        
        return possibleIssuers;
    }

    private CertPath createEmptyCertPath() 
    throws CertificateException, NoSuchProviderException
    {
        /* because the CRL was signed by the trust anchor we must make an empty certPath */
        CertificateFactory factory = securityFactory.createCertificateFactory("X.509");
        
        CertPath certPath = factory.generateCertPath(new LinkedList<X509Certificate>());

        return certPath;
    }
    
    @Override
    public CertPathBuilderResult buildPath(X509CRL crl) 
    throws CertPathBuilderException, NoSuchProviderRuntimeException
    {
        try {
            TrustAnchor trustAnchor = getPossibleTrustAnchorIssuer(crl);
        
            if (trustAnchor != null)
            {
                /* the CRL was directly signed by the trust anchor */            
                PublicKey publicKey = (trustAnchor.getTrustedCert() != null ? trustAnchor.getTrustedCert().getPublicKey() : 
                        trustAnchor.getCAPublicKey());
                
                /* because the CRL was signed by the trust anchor we must make an empty certPath */
                CertPath certPath = createEmptyCertPath();
                
                CertPathBuilderResult result = new PKIXCertPathBuilderResult(certPath, trustAnchor, null, publicKey);
                
                return result;
            }

            Set<X509Certificate> possibleIssuers = getPossibleIssuers(crl);
            
            X509Certificate issuerCertificate = null;
            
            for (X509Certificate certificate : possibleIssuers)
            {
                if (certificate == null) {
                    continue;
                }
                
                if (isSignedBy(crl,certificate.getPublicKey())) 
                {
                    issuerCertificate = certificate;
                    break;
                }
            }
            
            if (issuerCertificate == null) {
                /* we did not find a valid issuer. */
                throw new CertPathBuilderException(CRL_ISSUER_NOT_FOUND);
            }
            
            CertPathBuilderResult result = pathBuilder.buildPath(issuerCertificate);
            
            return result;
        }
        catch(CertificateException e) {
            throw new CertPathBuilderException(e);
        }
        catch (NoSuchProviderException e) {
            throw new NoSuchProviderRuntimeException(e);
        }
        catch (CertStoreException e) {
            throw new CertPathBuilderException(e);
        }
    }
}
