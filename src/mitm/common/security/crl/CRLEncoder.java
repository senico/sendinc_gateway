/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import mitm.common.security.asn1.ASN1Encoder;
import mitm.common.security.asn1.DERUtils;
import mitm.common.security.asn1.ObjectEncoding;

import org.bouncycastle.asn1.ASN1EncodableVector;

public class CRLEncoder
{
	/*
	 * Converts to CRL to a byte array
	 */
    public static byte[] encode(X509CRL crl, ObjectEncoding encoding) 
    throws CRLException, IOException
    {
       switch(encoding) 
       {
       case DER : return crl.getEncoded();
       case PEM   : return ASN1Encoder.encodePEM(Collections.singletonList(crl));
       default:
           throw new IllegalArgumentException("Unknown encoding: " + encoding);
       }
    }
	
    /**
     * Convert the collection of CRLs to a byte array using the specified encoding.
     */
    public static byte[] encode(Collection<X509CRL> crls, ObjectEncoding encoding) 
    throws CRLException, IOException
    {
       switch(encoding) 
       {
       case DER : return encodePKCS7(crls);
       case PEM   : return ASN1Encoder.encodePEM(crls);
       default:
           throw new IllegalArgumentException("Unknown encoding: " + encoding);
       }
    }
        
    private static byte[] encodePKCS7(Collection<X509CRL> crls) 
    throws IOException, CRLException
    {
        ASN1EncodableVector asn1CRLs = new ASN1EncodableVector();
        
        Iterator<X509CRL> crlIterator = crls.iterator(); 
        
        while (crlIterator.hasNext())
        {
            X509CRL crl = crlIterator.next();
            
            asn1CRLs.add(DERUtils.toDERObject(crl));
        }

        return ASN1Encoder.encodePKCS7(null /* no certs */, asn1CRLs);
    }
}
