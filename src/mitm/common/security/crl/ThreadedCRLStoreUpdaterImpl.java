/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.util.Check;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadedCRLStoreUpdaterImpl implements ThreadedCRLStoreUpdater
{
    private final static Logger logger = LoggerFactory.getLogger(ThreadedCRLStoreUpdaterImpl.class);
    
    private final static String THREAD_NAME = "CRL Updater thread";
    
    private final CRLStoreUpdater storeUpdater;

    private final SessionManager sessionManager;
    
    /*
     * Time between consecutive updates in milliseconds.
     */
    private long updateInterval;
    
    private final DatabaseActionExecutor actionExecutor;
    
    private Thread updateThread;
    
    private Updater updater;
    
    private class Updater implements Runnable
    {            
        /*
         * Used to wake-up the thread
         */
        private Object wakeupToken = new Object();
        
        @Override
        public void run() 
        {
            logger.info("Starting " + THREAD_NAME);
            
            do {                
                try {                    
                    actionExecutor.executeTransaction(
                        new DatabaseVoidAction()
                        {
                            @Override
                            public void doAction(Session session)
                            throws DatabaseException
                            {
                                Session previousSession = sessionManager.getSession();
    
                                sessionManager.setSession(session);
    
                                try {
                                    updateCRLStore(session);
                                }
                                finally {
                                    /* restore the session */
                                	sessionManager.setSession(previousSession);                                                    
                                }
                            }
                    });
                }
                catch(Exception e) {
                    logger.error("Error updating the CRL store.", e);
                }
                
                try {
                	synchronized (wakeupToken) {
						wakeupToken.wait(updateInterval);
					}
                }
                catch (InterruptedException e) {
                    // ignore.
                }
            }
            while(true);
        }
        
        private void updateCRLStore(Session session) 
        throws DatabaseException
        {
            try {
            	logger.info("Updating CRL store.");
            	
                storeUpdater.update();
            }
            catch (CRLStoreException e) {
                throw new DatabaseException(e);
            }
        }
        
        public void wakeup() 
        {
        	synchronized (wakeupToken) {
        		wakeupToken.notify();
			}
        }
    }
    
    public ThreadedCRLStoreUpdaterImpl(CRLStoreUpdater storeUpdater, SessionManager sessionManager, 
            long updateInterval)
    {
    	Check.notNull(storeUpdater, "storeUpdater");
    	Check.notNull(sessionManager, "sessionManager");
        
        this.storeUpdater = storeUpdater;
        this.sessionManager = sessionManager;
        this.updateInterval = updateInterval;
        
        this.actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);        
    }
    
    @Override
    public synchronized void start()
    {
        if (updateThread == null)
        {
        	updater = new Updater();
        	
            updateThread = new Thread(updater, THREAD_NAME);
            
            updateThread.setDaemon(true);
            updateThread.start();
        }
    }
    
    @Override
    public void update() 
    {
    	updater.wakeup();
    }
}