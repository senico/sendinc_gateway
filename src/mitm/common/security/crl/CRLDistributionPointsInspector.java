/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.security.cert.CRLException;
import java.util.HashSet;
import java.util.Set;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRLDistributionPointsInspector
{
    private final static Logger logger = LoggerFactory.getLogger(CRLDistributionPointsInspector.class);

    private CRLDistPoint crlDistPoint;
    
    public CRLDistributionPointsInspector(CRLDistPoint crlDistPoint) {
        this.crlDistPoint = crlDistPoint;
    }
  
    /*
     * Returns a set of URIs in (in String form) from the distribution point names. Some certificates
     * contain illegal URIs (for example they contain spaces). 
     */
    public Set<String> getURIDistributionPointNames()
    throws CRLException
    {        
        return getURIDistributionPointNames(crlDistPoint);
    }
    
    /*
     * Returns a set of URIs in (in String form) from the distribution point names. Some certificates
     * contain illegal URIs (for example they contain spaces). 
     */
    public static Set<String> getURIDistributionPointNames(CRLDistPoint crlDistPoint)
    throws CRLException
    {        
        try {
            Set<String> uris = new HashSet<String>();
    
            if (crlDistPoint == null) {
                return uris;
            }
            
            DistributionPoint[] distributionPoints = crlDistPoint.getDistributionPoints();
            
            if (distributionPoints != null)
            {
                for (DistributionPoint distributionPoint : distributionPoints)
                {           
                    if (distributionPoint == null) 
                    {
                        logger.debug("Distributionpoint is null.");
                        continue;
                    }
                    
                    DistributionPointName distributionPointName = distributionPoint.getDistributionPoint();
                    
                    /* We will only return full names containing URIs */
                    if (distributionPointName != null && distributionPointName.getType() == 
                                DistributionPointName.FULL_NAME)
                    {
                        ASN1Encodable name = distributionPointName.getName();
                        
                        if (name != null)
                        {
                            GeneralName[] names = GeneralNames.getInstance(name).getNames();
                            
                            for (GeneralName generalName : names)
                            {
                                if (generalName != null && generalName.getTagNo() == GeneralName.uniformResourceIdentifier && 
                                        generalName.getName() != null)
                                {
                                    String uri = DERIA5String.getInstance(generalName.getName()).getString();
                                
                                    uris.add(uri);
                                }
                            }
                        }
                    }
                }
            }
            
            return uris;
        }
        catch(IllegalArgumentException e) {
            /*
             * Can be thrown when the CRL dist. point contains illegal ASN1.
             */
            throw new CRLException("Error getting the CRL distribution point names.", e);
        }
    }
}
