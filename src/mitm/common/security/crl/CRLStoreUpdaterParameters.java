/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.security.cert.CertPathBuilderException;
import java.util.Collection;

import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certstore.BasicCertStore;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;

/**
 * Parameters used for the CRL store updater. For some implementations some getters can return dynamic 
 * data and it is therefore not advised to 'long term' store any result.
 * 
 * @author Martijn Brinkers
 *
 */
public interface CRLStoreUpdaterParameters
{
    /**
     * Returns the CertStore that will be searched for CRL distribution points.
     */
    public Collection<? extends BasicCertStore> getCertStores()
    throws CRLStoreException;
    
    /**
     * Returns the path builder used to check the trust of a certificate.
     */
    public CertificatePathBuilderFactory getCertificatePathBuilderFactory()
    throws CertPathBuilderException;
    
    /**
     * Returns the CRL store used to store the newly downloaded CRLs and searched for CRL distribution points.
     */
    public X509CRLStoreExt getCRLStore()
    throws CRLStoreException;
    
    /**
     * Returns the CRL path builder used to check the validity of the downloaded CRLs.
     */
    public CRLPathBuilderFactory getCRLPathBuilderFactory()
    throws CRLStoreException;
    
    /**
     * Returns the CRLDownloader used to download CRLs.
     */
    public CRLDownloader getCRLDownloader()
    throws CRLStoreException;
    
    /**
     * Returns the maintainer of CRL stores (replacing old CRLs with new ones etc.) 
     */
    public CRLStoreMaintainer getCRLStoreMaintainer()
    throws CRLStoreException;
    
    /**
     * If true the CRL will only be downloaded when the Certificate or CRL is trusted.
     */
    public boolean checkTrust();
}
