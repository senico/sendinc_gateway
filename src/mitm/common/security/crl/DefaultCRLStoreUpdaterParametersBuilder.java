/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.security.cert.CertPathBuilderException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import mitm.common.security.PKISecurityServices;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certstore.BasicCertStore;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.util.Check;

/**
 * Builder for a CRLStoreUpdaterParameters instance which uses default implementations for all required 
 * objects.
 * 
 * @author Martijn Brinkers
 *
 */
public class DefaultCRLStoreUpdaterParametersBuilder
{
    private final PKISecurityServices securityServices;
    
    private final CRLDownloader crlDownloader;
    
    private final CRLStoreMaintainer crlStoreMaintainer;
    
    /* 
     * If true CRLs will only be downloaded when certificates are trusted 
     */
    private boolean checkTrust = true;
        
    public DefaultCRLStoreUpdaterParametersBuilder(PKISecurityServices securityServices,
    		CRLDownloader crlDownloader, CRLStoreMaintainer crStoreMaintainer) 
    {
    	Check.notNull(securityServices, "securityServices");
    	Check.notNull(crlDownloader, "crlDownloader");
    	Check.notNull(crStoreMaintainer, "crStoreMaintainer");
    	
        this.securityServices = securityServices;
        this.crlDownloader = crlDownloader;
        this.crlStoreMaintainer = crStoreMaintainer;
    }
    
    public void setCheckTrust(boolean checkTrust) {
        this.checkTrust = checkTrust;
    }
    
    public CRLStoreUpdaterParameters createCRLStoreUpdaterParameters()
    throws CRLStoreException
    {
    	return new CRLStoreUpdaterParametersImpl();
    }
    
    private class CRLStoreUpdaterParametersImpl implements CRLStoreUpdaterParameters
    {
        protected CRLStoreUpdaterParametersImpl() {
            checkState();
        }
        
        /*
         * Checks if all necessary parameters are set.
         */
        private void checkState()
        throws IllegalStateException
        {
        	Check.notNull(securityServices.getKeyAndCertStore(), "securityServices.getKeyAndCertStore()");
        	Check.notNull(securityServices.getRootStore(), "securityServices.getRootStore()");
        	Check.notNull(securityServices.getCRLStore(), "securityServices.getCRLStore()");
        	Check.notNull(securityServices.getCertificatePathBuilderFactory(), "securityServices.getCertificatePathBuilderFactory()");
        }
        
        @Override
        public Collection<? extends BasicCertStore> getCertStores()
        {
        	List<BasicCertStore> stores = new LinkedList<BasicCertStore>();
        	
        	stores.add(securityServices.getKeyAndCertStore());
        	stores.add(securityServices.getRootStore());
        	
            return stores;
        }
        
        @Override
        public CertificatePathBuilderFactory getCertificatePathBuilderFactory()
        throws CertPathBuilderException
        {
            return securityServices.getCertificatePathBuilderFactory();
        }
        
        @Override
        public X509CRLStoreExt getCRLStore() {
            return securityServices.getCRLStore();
        }
        
        @Override
        public CRLPathBuilderFactory getCRLPathBuilderFactory()
        throws CRLStoreException
        {
        	return securityServices.getCRLPathBuilderFactory();
        }
        
        @Override
        public CRLDownloader getCRLDownloader() {
            return crlDownloader;
        }
        
        @Override
        public CRLStoreMaintainer getCRLStoreMaintainer() {
            return crlStoreMaintainer;
        }

        @Override
        public boolean checkTrust() {
            return checkTrust;
        }
    }
}
