/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import mitm.common.scheduler.AbstractCancelableTask;
import mitm.common.scheduler.Task;
import mitm.common.scheduler.TaskScheduler;
import mitm.common.scheduler.ThreadInterruptTimeoutTask;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.util.Check;
import mitm.common.util.CollectionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LDAPCRLDownloadHandler implements CRLDownloadHandler
{
    private static final Logger logger = LoggerFactory.getLogger(HTTPCRLDownloadHandler.class);

    private final static String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";

    private final CRLDownloadParameters downloadParameters;
    
    private static class LazyInitializingInitialDirContext extends InitialDirContext
    {
        public LazyInitializingInitialDirContext() 
        throws NamingException 
        {
            super(true);
        }
        
        @Override
        public void init(Hashtable<?,?> environment)
        throws NamingException
        {
            super.init(environment);
        }
    }
    
    public LDAPCRLDownloadHandler(CRLDownloadParameters downloadParameters) 
    {
        this.downloadParameters = downloadParameters;
    }
    
    @Override
    public boolean canHandle(URI uri) 
    {
    	Check.notNull(uri, "uri");
        
        String scheme = uri.getScheme();
        
        return scheme != null && scheme.equalsIgnoreCase("ldap"); 
    }

    @Override
    public Collection<? extends CRL> downloadCRLs(URI uri)
    throws IOException, CRLException 
    {
        /* sanity check */
        if (!canHandle(uri)) {
            throw new IllegalArgumentException("The uri cannot be handled by this handler.");            
        }

        Collection<CRL> crls = new LinkedList<CRL>();
        
        Properties properties = new Properties();
        
        /*
         * Info on timeouts: http://java.sun.com/docs/books/tutorial/jndi/newstuff/readtimeout.html
         * basic guide about LDAP http://java.sun.com/products/jndi/tutorial/basics/prepare/initial.html
         */
        properties.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        properties.put(Context.PROVIDER_URL, uri.toString());
        properties.put(Context.SECURITY_AUTHENTICATION, "none");
        properties.put(Context.REFERRAL, "follow");
        properties.put("com.sun.jndi.ldap.connect.timeout", Long.toString(downloadParameters.getConnectTimeout()));
        properties.put("com.sun.jndi.ldap.read.timeout", Long.toString(downloadParameters.getReadTimeout()));
        
        try {
            final LazyInitializingInitialDirContext dirContext = new LazyInitializingInitialDirContext();
                
            final String name = LDAPCRLDownloadHandler.class.getCanonicalName();
            
            /* watchdog to prevent runaway downloads */
            TaskScheduler watchdog = new TaskScheduler(name);
            
            try {
                Task closeContextTask = new AbstractCancelableTask() {
                    @Override
                    public void doRun()
                    throws Exception
                    {
                        dirContext.close();
                    }
                };
                
                watchdog.addTask(closeContextTask, downloadParameters.getTotalTimeout());
                
                Task threadWatchdogTask = new ThreadInterruptTimeoutTask(Thread.currentThread(), name);
    
                /* we want the close to fire first so add 10% */
                watchdog.addTask(threadWatchdogTask, (long) (downloadParameters.getTotalTimeout() * 1.1));
                
                dirContext.init(properties);
            
                getCRLs(crls, dirContext, new String[]{"certificateRevocationList;binary"});
                getCRLs(crls, dirContext, new String[]{"authorityRevocationList;binary"});
                getCRLs(crls, dirContext, new String[]{"deltaRevocationList;binary"});
            }
            finally {
                watchdog.cancel();
            }
        }
        catch(NamingException e) {
            throw new IOException(e);
        }
        
        return crls;
    }

    private void getCRLs(Collection<CRL> crls, DirContext dirContext, String[] attributeIDs)
    throws NamingException, CRLException 
    {
        Attributes attributes = dirContext.getAttributes("", attributeIDs);
        
        NamingEnumeration<? extends Attribute> attributeEnumerator = attributes.getAll();
        
        while (attributeEnumerator.hasMoreElements())
        {
            Attribute attribute = attributeEnumerator.nextElement();
            
            if (attribute != null) 
            {
                try {
                    Object object = attribute.get();
                    
                    if (object instanceof byte[]) 
                    {
                        byte[] bytes = (byte[]) object;
                        
                        if (bytes != null)
                        {
                            if (bytes.length > downloadParameters.getMaxBytes()) 
                            {
                                logger.warn("Bytes returned by LDAP server exceed limit.");
                                continue;
                            }
                            
                            InputStream input = new ByteArrayInputStream(bytes);
                            
                            Collection<? extends CRL> found = CRLUtils.readCRLs(input);
                            
                            if (found == null || found.size() == 0) {
                                logger.debug("No CRLs found in the downloaded stream.");
                            }
                            else {
                                CollectionUtils.copyCollectionFiltered(found, crls, CRL.class);
                            }
                        }
                    }
                }
                catch (NamingException e) {
                    logger.error("Error getting attribute value.", e);
                }
                catch (CertificateException e) {
                    logger.error("Error creating CRL out of byte array.", e);
                }
                catch (NoSuchProviderException e) {
                    logger.error("Error creating CRL out of byte array.", e);
                }
                catch (SecurityFactoryFactoryException e) {
                    logger.error("Error creating CRL out of byte array.", e);
                }
            }
        }
    }
}
