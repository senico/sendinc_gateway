/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.certificate.X500PrincipalUtils;

import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.x509.TBSCertList;
import org.bouncycastle.jce.provider.X509CRLObject;

/**
 * X509CRLObject extension that uses an enumerator to enumerate over the CRL objects. This implementation
 * requires a lot less memory than X509CRLObject.
 * 
 * @author Martijn Brinkers
 *
 */
public class OptimizedX509CRLObject extends X509CRLObject
{
    private final CertificateList certificateList;
    private final boolean isIndirect;
    
    public OptimizedX509CRLObject(CertificateList certificateList)
    throws CRLException
    {
        super(certificateList);
        
        this.certificateList = certificateList;
        
        try {
            this.isIndirect = isIndirect();
        }
        catch (IOException e) {
            throw new CRLException(e);
        }
    }
    
    private boolean isIndirect() 
    throws IOException 
    {
        boolean indirect = false;
        
        IssuingDistributionPoint idp = X509CRLInspector.getIssuingDistributionPoint(this);
        
        if (idp != null)
        {
            if (idp.isIndirectCRL()) 
            {
                indirect = true;
            }            
        }
        
        return indirect;
    }
    
    @Override
    public X509CRLEntry getRevokedCertificate(BigInteger serialNumber)
    {
        Enumeration<? extends X509CRLEntry> enumeration = getRevokedCertificatesEnumeration();

        while (enumeration.hasMoreElements()) 
        {
            X509CRLEntry entry = enumeration.nextElement();

            if (entry.getSerialNumber().equals(serialNumber)) {
                return entry;
            }
        }
        
        return null;
    }
        
    @Override
    public Set<? extends X509CRLEntry> getRevokedCertificates()
    {
        Set<X509CRLEntry> set = null;
        
        Enumeration<? extends X509CRLEntry> enumeration = getRevokedCertificatesEnumeration();
        
        while (enumeration.hasMoreElements()) 
        {
            if (set == null) {
                set = new HashSet<X509CRLEntry>();
            }
            
            X509CRLEntry entry = enumeration.nextElement();
            
            set.add(entry);
        }
        
        return set;
    }
    
    @Override
    public boolean isRevoked(Certificate certificate)
    {
        if (!(certificate instanceof X509Certificate)) {
            throw new RuntimeException("Certificate must be a X509 Certificate.");
        }

        BigInteger serial = ((X509Certificate)certificate).getSerialNumber();
        
        return getRevokedCertificate(serial) != null;
    }
    
    public Enumeration<? extends X509CRLEntry> getRevokedCertificatesEnumeration() 
    {
        return new CRLEntryEnumeration();
    }

    /*
     * Internally used to enumerate over the CRLEntries
     */
    private class CRLEntryEnumeration implements Enumeration<X509CRLEntry>
    {
        private final Enumeration<TBSCertList.CRLEntry> revokedCertificateEntries;

        private X500Principal previousCertificateIssuer;

        @SuppressWarnings("unchecked")
        public CRLEntryEnumeration() 
        {        
            this.revokedCertificateEntries = certificateList.getRevokedCertificateEnumeration();        
            this.previousCertificateIssuer = getIssuerX500Principal();
        }
        
        @Override
        public boolean hasMoreElements() 
        {
            return revokedCertificateEntries != null && revokedCertificateEntries.hasMoreElements();
        }

        @Override
        public X509CRLEntry nextElement() 
        {
            if (revokedCertificateEntries == null) {
                throw new IllegalStateException("There is no next because revokedCertificatesEnum is null.");
            }
            
            TBSCertList.CRLEntry entry = revokedCertificateEntries.nextElement();

            X509CRLEntry crlObject = new OptimizedX509CRLEntryObject(entry, isIndirect, 
                    X500PrincipalUtils.toX500Name(previousCertificateIssuer));
            
            previousCertificateIssuer = crlObject.getCertificateIssuer();
            
            return crlObject;
        }        
    }    
}
