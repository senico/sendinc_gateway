package mitm.common.security.crl;

import java.math.BigInteger;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.common.security.KeyAndCertificate;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.util.BigIntegerUtils;
import mitm.common.util.Check;
import mitm.common.util.HexUtils;

public class X509CRLBuilderHelper
{
    private final static Logger logger = LoggerFactory.getLogger(X509CRLBuilderHelper.class);
    
    /*
     * Used for storage and retrieval of CRLs
     */
    private final X509CRLStoreExt crlStore;

    /*
     * Used to build the CRL 
     */
    private final X509CRLBuilder crlBuilder;
    
    /**
     * Creates a new X509CRLBuilderHelper. The provided X509CRLBuilder instance should 
     * already be populated with the relevant settings.
     */
    public X509CRLBuilderHelper(X509CRLStoreExt crlStore, X509CRLBuilder crlBuilder)
    {
        Check.notNull(crlStore, "crlStore");
        Check.notNull(crlBuilder, "crlBuilder");
        
        this.crlStore = crlStore;
        this.crlBuilder = crlBuilder;
    }
    
    /**
     * Adds the serial numbers to the revocation list. The serial number must be in hex form.
     */
    public void addEntries(Collection<String> revokedSerials)
    throws CRLException
    {
        if (revokedSerials != null)
        {
            Date revocationDate = new Date();
            
            for (String serialHex : revokedSerials)
            {
                if (serialHex == null) {
                    continue;
                }
                    
                if (!HexUtils.isHex(serialHex)) {
                    throw new CRLException(serialHex + " is not a hex number.");
                }
                
                BigInteger serial = BigIntegerUtils.hexDecode(serialHex.trim());
                
                crlBuilder.addCRLEntry(serial, revocationDate, 0);
            }
        }
    }
    
    public X509CRL generateCRL(KeyAndCertificate issuer, boolean updateExistingCRL)
    throws CRLException
    {        
        try {
            if (updateExistingCRL)
            {
                CRLLocator locator = new CRLLocator(crlStore);
    
                List<X509CRL> existingCRLs = locator.findCRLs(issuer.getCertificate());
                
                for (X509CRL existingCRL : existingCRLs)
                {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Adding existing CRL: " + existingCRL);
                    }
                    
                    crlBuilder.addCRL(existingCRL);
                }
            }
            
            return crlBuilder.generateCRL(issuer);
        }
        catch (NoSuchProviderException e) {
            throw new CRLException(e);
        }
    }
}
