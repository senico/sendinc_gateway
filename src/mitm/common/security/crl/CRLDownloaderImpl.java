/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.IOException;
import java.net.URI;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import mitm.common.net.ProxyInjector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRLDownloaderImpl implements CRLDownloader 
{
    private final static Logger logger = LoggerFactory.getLogger(CRLDownloaderImpl.class);
    
    private final Collection<CRLDownloadHandler> downloadHandlers;
   
    /*
     * Used to set the proxy for HttpClient
     */
    private final ProxyInjector proxyInjector;

    public CRLDownloaderImpl(CRLDownloadParameters downloadParameters)
    {
        this(downloadParameters, null);
    }
    
    public CRLDownloaderImpl(CRLDownloadParameters downloadParameters, ProxyInjector proxyInjector)
    {
        this.downloadHandlers = Collections.synchronizedCollection(new LinkedList<CRLDownloadHandler>());
        this.proxyInjector = proxyInjector;
        
        registerDefaultHandlers(downloadParameters);
    }
    
    private void registerDefaultHandlers(CRLDownloadParameters downloadParameters)
    {
        addDownloadHandler(new HTTPCRLDownloadHandler(downloadParameters, proxyInjector));
        addDownloadHandler(new LDAPCRLDownloadHandler(downloadParameters));
    }
    
    public void addDownloadHandler(CRLDownloadHandler downloadHandler) {
        downloadHandlers.add(downloadHandler);
    }
    
    @Override
    public Collection<? extends CRL> downloadCRLs(URI uri)
    throws IOException, CRLException
    {
        /* look for a handler that supports the uri */
        
        for (CRLDownloadHandler handler : downloadHandlers)
        {
            if (handler.canHandle(uri))
            {
                return handler.downloadCRLs(uri);
            }
        }
        
        logger.debug("Could not find a handler for: " + uri);
        
        return new LinkedList<X509CRL>();
    }
}
