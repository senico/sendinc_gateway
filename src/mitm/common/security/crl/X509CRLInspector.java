/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Date;

import mitm.common.security.asn1.ASN1Utils;
import mitm.common.security.asn1.DERUtils;
import mitm.common.security.certificate.X500PrincipalInspector;
import mitm.common.security.certificate.X509ExtensionInspector;
import mitm.common.security.digest.Digest;
import mitm.common.security.digest.Digests;

import org.apache.commons.lang.text.StrBuilder;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.X509Extension;

public class X509CRLInspector extends X509ExtensionInspector
{
    private final X509CRL crl;
    
    public X509CRLInspector(X509CRL crl) 
    {
        super(crl);
        
        this.crl = crl;
    }
    
    /**
     * Returns the issuer DN in a canonical RFC2253 format
     * @return the issuer DN in a canonical RFC2253 format
     */
    public String getIssuerCanonical() 
    {
        return getIssuerCanonical(this.crl);
    }

    /**
     * Returns the issuer DN in a canonical RFC2253 format
     * @param certificate
     * @return
     */
    public static String getIssuerCanonical(X509CRL crl) 
    {
        return X500PrincipalInspector.getCanonical(crl.getIssuerX500Principal());
    }
    
    /**
     * Returns the issuer DN in a friendly format
     * @return the issuer DN in a friendly format
     */
    public String getIssuerFriendly() 
    {
        return getIssuerFriendly(this.crl);
    }

    /**
     * Returns the issuer DN in a friendly format
     * @param certificate
     * @return
     */
    public static String getIssuerFriendly(X509CRL crl) 
    {
        return X500PrincipalInspector.getFriendly(crl.getIssuerX500Principal());
    }
    
    /**
     * Returns the crl number extension if present, null if not present
     */
    public BigInteger getCRLNumber() 
    throws IOException
    {
        return getCRLNumber(crl);
    }

    /**
     * Returns the crl number extension if present, null if not present
     */
    public static BigInteger getCRLNumber(X509CRL crl) 
    throws IOException
    {
        byte[] derCRLNumber = crl.getExtensionValue(X509Extension.cRLNumber.getId());
        
        BigInteger crlNumber = null;
        
        if (derCRLNumber != null) 
        {
            ASN1Encodable extension = DERUtils.fromExtensionValue(derCRLNumber);
            
            /* CRL number must be a positive number */
            crlNumber = CRLNumber.getInstance(extension).getCRLNumber();
        }
        
        return crlNumber;
    }
        
    /**
     * Returns the deltaIndicator extension
     */
    public static BigInteger getDeltaIndicator(X509CRL crl) 
    throws IOException
    {
        BigInteger deltaIndicator = null;
        
        ASN1Object derDeltaCRLIndicator = ASN1Utils.getExtensionValue(crl, X509Extension.deltaCRLIndicator.getId());
        
        if (derDeltaCRLIndicator != null) 
        {
            /* CRL number must be a positive number */
            deltaIndicator = CRLNumber.getInstance(derDeltaCRLIndicator).getCRLNumber();
        }
        
        return deltaIndicator;
    }
    
    /**
     * Returns the deltaIndicator extension
     */
    public BigInteger getDeltaIndicator() 
    throws IOException
    {
        return getDeltaIndicator(crl);
    }

    /**
     * Returns true if crl is a delta crl
     */
    public static boolean isDeltaCRL(X509CRL crl) 
    throws IOException
    {
        return getDeltaIndicator(crl) != null;
    }
    
    /**
     * Returns true if crl is a delta crl
     */
    public boolean isDeltaCRL() 
    throws IOException
    {
        return getDeltaIndicator() != null;
    }
    
    /**
     * Calculates the thumbprint of the CRL using the given digest algorithm. 
     */
    public String getThumbprint(Digest digest)
    throws NoSuchAlgorithmException, NoSuchProviderException, CRLException 
    {
        return getThumbprint(this.crl, digest);
    }

    /**
     * Calculates the thumbprint of the CRL using the given digest algorithm. 
     * @throws CRLException 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     */
    public static String getThumbprint(X509CRL crl, Digest digest) 
    throws NoSuchAlgorithmException, NoSuchProviderException, CRLException 
    {
        return getThumbprint(crl.getEncoded(), digest);
    }
    
    /**
     * Calculates the thumbprint of the CRL using SHA-512. 
     */
    public String getThumbprint() 
    throws NoSuchAlgorithmException, NoSuchProviderException, CRLException 
    {
        return getThumbprint(this.crl);
    }    

    /**
     * Calculates the thumbprint of the CRL using SHA-512. 
     */
    public static String getThumbprint(X509CRL crl) 
    throws NoSuchAlgorithmException, NoSuchProviderException, CRLException 
    {
        return getThumbprint(crl, Digest.SHA512);
    }        

    /**
     * Calculates the thumbprint of the encoded CRL using SHA-512. 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     */
    public static String getThumbprint(byte[] encodedCRL, Digest digest) 
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return Digests.digestHex(encodedCRL, digest);
    }        

    /**
     * Calculates the thumbprint of the encoded CRL using SHA-512. 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     */
    public static String getThumbprint(byte[] encodedCRL) 
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return getThumbprint(encodedCRL, Digest.SHA512);
    }
    
    /**
     * Returns true if nextUpdate is before the current date
     */
    public boolean isExpired() {
    	return isExpired(crl);
    }
    
    /**
     * Returns true if nextUpdate is before the current date
     */
    public static boolean isExpired(X509CRL crl)
    {
    	boolean expired = false;
    	
    	Date now = new Date();
    	
    	expired = crl.getNextUpdate() != null && now.after(crl.getNextUpdate());
    	
    	return expired;
    }
    
    /**
     * Returns a string representation of the CRL which can be used for logging etc.
     */
    @Override
    public String toString() 
    {
        return toString(crl);
    }
    
    /**
     * Returns a string representation of the certificate which can be used for logging etc.
     */
    public static String toString(X509CRL crl) 
    {
    	if (crl == null) {
    		return "";
    	}
    	
        StrBuilder sb = new StrBuilder(1024);
        
        sb.append("Issuer: ");
        sb.append(getIssuerFriendly(crl));
        sb.appendSeparator("; ");
        sb.append("CRL number: ");
        try {
            sb.append(getCRLNumber(crl));
        }
        catch (IOException e1) {
            // ignore
        }
        sb.appendSeparator("; ");
        sb.append("Thumbprint: ");
        try {
            sb.append(getThumbprint(crl));
        }
        catch (CRLException e) {
            // ignore
        }
        catch (NoSuchAlgorithmException e) {
            // ignore
        }
        catch (NoSuchProviderException e) {
            // ignore
        }
        sb.appendSeparator("; ");
        sb.append("SHA1: ");
        try {
            sb.append(getThumbprint(crl, Digest.SHA1));
        }
        catch (CRLException e) {
            // ignore
        }
        catch (NoSuchAlgorithmException e) {
            // ignore
        }
        catch (NoSuchProviderException e) {
            // ignore
        }
        
        return sb.toString();
    }    
} 
