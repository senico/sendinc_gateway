/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.IOException;
import java.security.cert.X509CRLEntry;
import java.util.Date;

import mitm.common.security.asn1.ASN1Utils;
import mitm.common.util.BigIntegerUtils;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.bouncycastle.asn1.DEREnumerated;
import org.bouncycastle.asn1.x509.X509Extension;

public class X509CRLEntryInspector
{
    private final X509CRLEntry crlEntry;
    
    public X509CRLEntryInspector(X509CRLEntry crlEntry) {
        this.crlEntry = crlEntry;
    }
    
    /**
     * Returns the reason code, null of this extension does not exist.
     */
    public static Integer getReasonCode(X509CRLEntry crlEntry) 
    throws IOException
    {
        Integer code = null;
        
        DEREnumerated reasonCode = DEREnumerated.getInstance(ASN1Utils.getExtensionValue(
                crlEntry, X509Extension.reasonCode.getId()));
        
        if (reasonCode != null) {
            code = reasonCode.getValue().intValue();
        }
        
        return code;
    }
    
    /**
     * Returns the reason code, null of this extension does not exist.
     */
    public Integer getReasonCode() 
    throws IOException
    {
        return getReasonCode(crlEntry);
    }
    
    /**
     * Returns the revocation reason of the crl entry. If there is no reason null is returned.
     */
    public static RevocationReason getReason(X509CRLEntry crlEntry)
    throws IOException
    {
        RevocationReason reason = null;

        Integer reasonCode = getReasonCode(crlEntry);
        
        if (reasonCode != null) {
            reason = RevocationReason.fromTag(reasonCode);
        }
        
        return reason;
    }

    /**
     * Returns the revocation reason of the crl entry. If there is no reason UNKNOWN is returned.
     */
    public RevocationReason getReason()
    throws IOException
    {
        return getReason(crlEntry);
    }
    
    /**
     * Returns the serial number of the entry as a hex string. If serial number is
     * missing (should never occur) an empty string is returned.
     */
    public static String getSerialNumberHex(X509CRLEntry crlEntry) {
        return BigIntegerUtils.hexEncode(crlEntry.getSerialNumber(), "");
    }
    
    public String getSerialNumberHex() {
        return getSerialNumberHex(crlEntry);
    }
    
    /**
     * Returns a string representation of the crlEntry
     */
    public static String toString(X509CRLEntry crlEntry)
    {
        Date revocationDate = crlEntry.getRevocationDate();
        
        String reason;
        
        try {
            reason = ObjectUtils.toString(getReason(crlEntry), "");
        }
        catch (IOException e) {
            reason = "";
        }
        
        return getSerialNumberHex(crlEntry) + "\t" + 
            (revocationDate != null ? DateFormatUtils.ISO_DATE_FORMAT.format(revocationDate) : "") +
            "\t" + reason;
    }
}
