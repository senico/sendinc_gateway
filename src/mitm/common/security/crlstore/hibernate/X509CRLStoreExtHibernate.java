/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.hibernate;

import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.util.Collection;

import mitm.common.hibernate.CommitOnCloseIterator;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.dao.X509CRLStoreDAO;
import mitm.common.security.crlstore.dao.X509CRLStoreDAOHibernate;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;

import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class X509CRLStoreExtHibernate implements X509CRLStoreExt
{
    /*
     * The name of this store. The database can store more than one CertStore.
     */
    private String storeName;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
    public X509CRLStoreExtHibernate(String storeName, SessionManager sessionManager) 
    {
    	Check.notNull(sessionManager, "sessionManager");
    	
    	this.sessionManager = sessionManager;
        this.storeName = storeName;
    }
                
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    
    @Override
    public void addCRL(X509CRL crl)
    throws CRLStoreException
    {
        getDAO().addCRL(crl);
    }
    
    @Override
    public X509CRL getCRL(String thumbprint)
    throws CRLStoreException
    {
        return getDAO().getCRL(thumbprint);
    }
    
    @Override
    public void remove(X509CRL crl)
    throws CRLStoreException
    {
        getDAO().remove(crl);
    }
    
    @Override
    public void replace(X509CRL oldCRL, X509CRL newCRL)
    throws CRLStoreException
    {
        getDAO().replace(oldCRL, newCRL);
    }
    
    @Override
    public boolean contains(X509CRL crl)
    throws CRLStoreException
    {
        return getDAO().contains(crl);
    }

    @Override
    public Collection<X509CRL> getCRLs(CRLSelector crlSelector)
    throws CRLStoreException
    {
        return getCRLs(crlSelector, null, null);
    }
    
    /*
     * A call to getCertificates can result in a lot of records being returned which can result in an 
     * out of memory because the first level cache grows with every object. There are a lot of possible
     * solutions but we will use a StatelessSession because we are only interested in the certificate
     * and not in the entry itself (StatelessSession does not load collection etc.). The drawback of 
     * this approach is that getCertificates has it's own transaction.
     */
    @Override
    public Collection<X509CRL> getCRLs(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException
    {   
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        try {
            Collection<X509CRL> crls = getStatelessDAO(statelessSession).getCRLs(crlSelector, firstResult, maxResults);
            
            tx.commit();
            
            return crls;
        }
        catch(CRLStoreException e) {
            tx.rollback();
            
            throw e;
        }
        finally {
            statelessSession.close();
        }
    }

    @Override
    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector) 
    throws CRLStoreException
    {
        return getCRLIterator(crlSelector, null, null);
    }
    
    /*
     * We want to use a StatelessSession for performance and memory reasons but we do want to wrap it in it's own
     * transaction. We will therefore wrap the transaction and session into a wrapper iterator that will commit
     * the transaction and close the session when the iterator is closed. 
     * We need to support a large number of certificates (10th of thousands) and we therefore have to work with
     * StatelessSessions and iterators.
     */
    @Override
    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults) 
    throws CRLStoreException
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        try {
            CloseableIterator<X509CRL> iterator = getStatelessDAO(statelessSession).getCRLIterator(crlSelector,
                    firstResult, maxResults);
            
            CloseableIterator<X509CRL> commitOnCloseIterator = new CommitOnCloseIterator<X509CRL>(iterator, 
                    SessionAdapterFactory.create(statelessSession), tx);
            
            return commitOnCloseIterator;
        }
        catch(CRLStoreException e) 
        {
            try {
                tx.rollback();
            }
            finally {
                statelessSession.close();
            }
            
            throw e;
        }
    }

    @Override
    public CloseableIterator<X509CRLStoreEntryHibernate> getCRLStoreIterator(CRLSelector crlSelector) 
    throws CRLStoreException
    {
        return getCRLStoreIterator(crlSelector, null, null);
    }
    
    @Override
    public CloseableIterator<X509CRLStoreEntryHibernate> getCRLStoreIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults) 
    throws CRLStoreException
    {
        return getDAO().getCRLStoreIterator(crlSelector, firstResult, maxResults);        
    }
    
    @Override
    public void removeAllEntries()
    throws CRLStoreException
    {
        getDAO().removeAllEntries();
    }

    @Override
    public long size() {
    	return getDAO().size();
    }

    private String getStoreName()
    {
        if (storeName == null) {
            throw new IllegalStateException("storeName should be set.");
        }

        return storeName;
    }    
    
    private X509CRLStoreDAO getDAO()
    {
        if (storeName == null) {
            throw new IllegalStateException("storeName should be set.");
        }
        
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new X509CRLStoreDAOHibernate(session, getStoreName());
    }
    
    private X509CRLStoreDAO getStatelessDAO(StatelessSession statelessSession) 
    {
        SessionAdapter session = SessionAdapterFactory.create(statelessSession);
        
        return new X509CRLStoreDAOHibernate(session, getStoreName());
    }    
}
