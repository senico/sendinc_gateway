/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.hibernate;

import java.io.Serializable;
import java.security.cert.X509CRL;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mitm.common.security.crlstore.X509CRLStoreEntry;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

/**
 * Entity class for storing a CRL into a database.
 *
 * @author Martijn Brinkers
 *
 */
@Entity(name = X509CRLStoreEntryHibernate.ENTITY_NAME)
@Table(name = "crls",
        uniqueConstraints = {@UniqueConstraint(columnNames={"storeName", "thumbprint"})}
)
@org.hibernate.annotations.Table(appliesTo = "crls", indexes = {
        @Index(name = "crls_store_name_index", columnNames = {"storeName"}),
        @Index(name = "crls_issuer_index",     columnNames = {"issuer"}),
        @Index(name = "crls_crlnumber_index",  columnNames = {"crlNumber"}),
        @Index(name = "crls_thumbprint_index", columnNames = {"thumbprint"})
        }
)
public class X509CRLStoreEntryHibernate implements X509CRLStoreEntry, Serializable
{
    public static final String ENTITY_NAME = "CRLs";

    private static final long serialVersionUID = 1904851871942022087L;

    /**
     * Maximum size of the store name
     */
    private static final int MAX_STORE_NAME_LENGTH = 255;

    /**
     * The maximum length of a serial number.
     * Should be at least 20 octets (see http://www.ietf.org/rfc/rfc3280.txt 5.2.3 CRL Number)
     */
    private static final int MAX_CRL_NUMBER_LENGTH = 1024;

    /**
     * Maximum length of the thumbprint
     */
    private static final int MAX_THUMBPRINT_LENGTH = 255;


    protected X509CRLStoreEntryHibernate() {
        /* Hibernate requires a default constructor */
    }

    public X509CRLStoreEntryHibernate(X509CRL crl, String storeName, Date creationDate)
    {
        this.crl = crl;
        this.storeName = storeName;
        this.creationDate = creationDate;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The store name will be used to store multiple certificate stores in the database
     */
    @Column (name = "storeName", unique = false, nullable = true, length = MAX_STORE_NAME_LENGTH)
    private String storeName;

    @Type(type = "mitm.common.hibernate.X509CRLUserType")
    @Columns(columns = {
        @Column(name = "issuer"),
        @Column(name = "nextUpdate"),
        @Column(name = "thisUpdate"),
        @Column(name = "crlNumber", length = MAX_CRL_NUMBER_LENGTH),
        @Column(name = "crl"),
        @Column(name = "thumbprint", length = MAX_THUMBPRINT_LENGTH)
    })
    private X509CRL crl;

    /*
     * Date this entry was created
     */
    @Type(type = "timestamp")
    @Column (name = "creationDate", unique = false, nullable = true)
    private Date creationDate;

    @Override
    public String getStoreName() {
        return storeName;
    }

    @Override
    public X509CRL getCRL() {
        return crl;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * X509CRLStoreEntry is equal if and only if the CRLs and storeNames are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof X509CRLStoreEntryHibernate)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        X509CRLStoreEntryHibernate rhs = (X509CRLStoreEntryHibernate) obj;

        return new EqualsBuilder()
            .append(crl, rhs.crl)
            .append(storeName, rhs.storeName)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(crl)
            .append(storeName)
            .toHashCode();
    }
}
