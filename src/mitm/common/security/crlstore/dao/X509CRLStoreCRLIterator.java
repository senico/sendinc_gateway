/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.dao;

import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;

import mitm.common.hibernate.AbstractScrollableResultsIterator;
import mitm.common.security.crlstore.hibernate.X509CRLStoreEntryHibernate;

import org.apache.commons.lang.IllegalClassException;
import org.hibernate.ScrollableResults;

public class X509CRLStoreCRLIterator extends AbstractScrollableResultsIterator<X509CRL>
{
    private final CRLSelector crlSelector;

    public X509CRLStoreCRLIterator(ScrollableResults results, CRLSelector crlSelector)
    {
        super(results);
        
        this.crlSelector = crlSelector;
    }

    public X509CRLStoreCRLIterator(ScrollableResults results)
    {
        this(results, null);
    }

    @Override
    protected Object parseElement(Object element)
    {
        if (!(element instanceof X509CRLStoreEntryHibernate)) 
        {
            throw new IllegalClassException("The object returned from the ScrollableResults" + 
                    " is not a X509CRLStoreEntryHibernate.");
        }
               
        X509CRLStoreEntryHibernate crlStoreEntry = (X509CRLStoreEntryHibernate) element;
        
        return crlStoreEntry.getCRL();
    }
    
    @Override
    protected boolean hasMatch(X509CRL crl)
    {        
        if (crl == null) {
            return false;
        }
     
        if (crlSelector == null) {
            return true;
        }

        boolean match = false;
                
        if (crl != null && crlSelector.match(crl))
        {
            match = true;
        }
        
        return match;
    }
}