/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.dao;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import javax.security.auth.x500.X500Principal;

import mitm.common.hibernate.GenericHibernateDAO;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.security.NoSuchProviderRuntimeException;
import mitm.common.security.certificate.X500PrincipalInspector;
import mitm.common.security.crl.X509CRLInspector;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.hibernate.X509CRLStoreEntryHibernate;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;

import org.apache.commons.lang.ObjectUtils;
import org.hibernate.Criteria;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class X509CRLStoreDAOHibernate extends GenericHibernateDAO<X509CRLStoreEntryHibernate, Long>
    implements X509CRLStoreDAO
{
	private final static Logger logger = LoggerFactory.getLogger(X509CRLStoreDAOHibernate.class);
	
    private final String storeName;
    private final String entityName;
    
    public X509CRLStoreDAOHibernate(SessionAdapter session, String storeName, String entityName) 
    {
        super(session);

        this.storeName = storeName;
        this.entityName = entityName;
    }
    
    public X509CRLStoreDAOHibernate(SessionAdapter session, String storeName) 
    {
        this(session, storeName, X509CRLStoreEntryHibernate.ENTITY_NAME);
    }

    @Override
    public void addCRL(X509CRL crl)
    throws CRLStoreException
    {
        Date creationDate = new Date();
        
        X509CRLStoreEntryHibernate entry = new X509CRLStoreEntryHibernate(crl, storeName, creationDate);
        
        makePersistent(entry);
    }
    
    @Override
    public X509CRL getCRL(String thumbprint) 
    throws CRLStoreException
    {
    	X509CRL crl = null;
    	
    	X509CRLStoreEntryHibernate entry = getEntry(thumbprint);
    	
    	if (entry != null) {
    		crl = entry.getCRL();
    	}
    	
    	return crl;
    }

    @Override
    public void remove(X509CRL crl)
    throws CRLStoreException
    {
        X509CRLStoreEntryHibernate entry = getEntry(crl);
        
        if (entry != null) {
            delete(entry);
        }
    }
    
    @Override
    public void replace(X509CRL oldCRL, X509CRL newCRL)
    throws CRLStoreException
    {
    	try {
	    	if (ObjectUtils.equals(X509CRLInspector.getThumbprint(oldCRL), 
	    			X509CRLInspector.getThumbprint(newCRL))) 
	    	{
	    		logger.warn("oldCRL is equal to newCRL so won't replace.");
	    		
	    		return;
	    	}
    	}
    	catch(NoSuchAlgorithmException e) {
    		throw new CRLStoreException(e);
    	} 
    	catch (CRLException e) {
    		throw new CRLStoreException(e);
		}
    	catch (NoSuchProviderException e) {
    		throw new NoSuchProviderRuntimeException(e);
		} 
    	
    	/*
    	 * We need to add the new one before removing the old one otherwise there is a small
    	 * time-gap ('race condition') where there is no CRL.
    	 * 
    	 * We need to check if the newCRL is not already in the store. This can happen when
    	 * a CRL is added by a user (ie not automatically downloaded) because if it was added
    	 * by a user it did not yet replace the old one because replacing is done by CRL store
    	 * maintainer.
    	 */
    	if (!contains(newCRL)) 
    	{
    		logger.debug("newCRL is already in the CRL store.");

    		addCRL(newCRL);
    	}
    	
        remove(oldCRL);
    }
    
    @Override
    public boolean contains(X509CRL crl)
    throws CRLStoreException
    {
        if (crl == null) {
            return false;
        }
        
        return getEntry(crl) != null;
    }        

    @Override
    public Collection<X509CRL> getCRLs(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException
    {
        Collection<X509CRL> foundCRLs = new LinkedList<X509CRL>();

        CloseableIterator<X509CRL> iterator = getCRLIterator(crlSelector, firstResult, maxResults);
        
        try {
            try {
                while (iterator.hasNext())
                {
                    X509CRL crl = iterator.next();
                    
                    if (crl != null && (crlSelector == null || crlSelector.match(crl))) {
                        foundCRLs.add(crl);
                    }
                }
            }
            finally {
                /* we must close the iterator */
                iterator.close();
            }
        }
        catch(CloseableIteratorException e) {
            throw new CRLStoreException(e);
        }
        
        return foundCRLs;
    }

    @Override
    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    {
        ScrollableResults results = getEntriesScrollable(crlSelector, firstResult, maxResults);
        
        return new X509CRLStoreCRLIterator(results, crlSelector);
    }    

    @Override
    public CloseableIterator<X509CRLStoreEntryHibernate> getCRLStoreIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults) 
    {
        ScrollableResults results = getEntriesScrollable(crlSelector, firstResult, maxResults);

        return new X509CRLStoreEntryIterator(results, crlSelector);
    }
    
    @Override
    public void removeAllEntries()
    throws CRLStoreException
    {
        /*
         * I tried using HQL and delete but that did not work because of a constraint violation.
         * Using delete seems not to cascade to the certificate_email table.
         */
        CloseableIterator<X509CRLStoreEntryHibernate> iterator = getCRLStoreIterator(null, null, null);
        
        try {
            try {
                while (iterator.hasNext())
                {
                    X509CRLStoreEntryHibernate entry = iterator.next();
    
                    /* evict the entry to save memory */
                    this.evict(entry);
                    this.delete(entry);
                }
            }
            finally {
                /* we must close the iterator */
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new CRLStoreException(e);
        }
    }
    
    @Override
    public long size() 
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq("storeName", storeName));
        
        criteria.setProjection(Projections.rowCount());
        
        return (Integer) criteria.uniqueResult();        
    }

    private X509CRLStoreEntryHibernate getEntry(X509CRL crl) 
    throws CRLStoreException
    {
    	try {
			return getEntry(X509CRLInspector.getThumbprint(crl));
		} 
    	catch (NoSuchAlgorithmException e) {
            throw new CRLStoreException(e);
		} 
    	catch (NoSuchProviderException e) {
            throw new NoSuchProviderRuntimeException(e);
		} 
    	catch (CRLException e) {
            throw new CRLStoreException(e);
		}
    }
    
    private X509CRLStoreEntryHibernate getEntry(String thumbprint) 
    throws CRLStoreException
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq("storeName", storeName));
        
        criteria.add(Restrictions.eq("crl.thumbprint", thumbprint));
        
        return (X509CRLStoreEntryHibernate) criteria.uniqueResult();
    }
    
    private void addSelectorCriterias(X509CRLSelector crlSelector, Criteria criteria)
    {
        Collection<X500Principal> crlSelectorIssuers = crlSelector.getIssuers(); 
        
        if (crlSelectorIssuers != null && crlSelectorIssuers.size() > 0)
        {
            Disjunction issuerCriteria = Restrictions.disjunction();
            
            for (X500Principal issuer : crlSelectorIssuers)
            {
                String issuerCanonical = X500PrincipalInspector.getCanonical(issuer);
                
                issuerCriteria.add(Restrictions.eq("crl.issuer", issuerCanonical));
            }
            
            criteria.add(issuerCriteria);
        }
        
        Date dateAndTime = crlSelector.getDateAndTime();
        
        if (dateAndTime != null)
        {
            criteria.add(Restrictions.lt("crl.thisUpdate", dateAndTime));
            criteria.add(Restrictions.gt("crl.nextUpdate", dateAndTime));
        }
    }

    private ScrollableResults getEntriesScrollable(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    {
        Criteria criteria = createCriteria(entityName);
        criteria.add(Restrictions.eq("storeName", storeName));

        if (crlSelector instanceof X509CRLSelector) 
        {
            /* we can do some optimizations by using SQL */
            X509CRLSelector x509CRLSelector = (X509CRLSelector) crlSelector;

            addSelectorCriterias(x509CRLSelector, criteria);
        }

        /* sort on thisUpdate date */
        criteria.addOrder(Order.desc("crl.thisUpdate"));
        
        if (firstResult != null) {
            criteria.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            criteria.setMaxResults(maxResults);
        }

        return criteria.scroll(ScrollMode.FORWARD_ONLY);
    }
}
