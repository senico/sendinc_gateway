/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import mitm.common.util.MiscArrayUtils;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DEROutputStream;

public class DERUtils
{
    /*
     * Wrapper class used for sorting a Vector
     */
    private static class DEREntry implements Comparable<DEREntry>
    {
        private ASN1Encodable derEncodable;
        private byte[] encoded;

        public DEREntry(ASN1Encodable derEncodable)
        throws IOException
        {
            this.derEncodable = derEncodable;
            this.encoded = toByteArray(derEncodable);
        }

        @Override
        public int compareTo(DEREntry that)
        {
            return MiscArrayUtils.compareArray(encoded, that.encoded);
        }
    }

    /**
     * Bouncycastle DERSet sorts the entries in the set (required by DER encoding) but uses a slow
     * sort method. You can use this method to do a pre-sort using a faster method before creating
     * the DERSet.
     * @param asn1Certificates
     * @return
     * @throws IOException
     */
    public static ASN1EncodableVector sortASN1EncodableVector(ASN1EncodableVector asn1Certificates)
    throws IOException
    {
        ASN1EncodableVector sorted = new ASN1EncodableVector();

        List<DEREntry> sortingList = new Vector<DEREntry>(asn1Certificates.size());

        for (int i = 0; i < asn1Certificates.size(); i++)
        {
            DEREntry entry = new DEREntry(asn1Certificates.get(i));
            sortingList.add(entry);
        }

        Collections.sort(sortingList);

        for (DEREntry entry : sortingList)
        {
            sorted.add(entry.derEncodable);
        }

        return sorted;
    }

    public static ASN1Primitive toDERObject(Certificate certificate)
    throws CertificateEncodingException, IOException
    {
        final ASN1InputStream stream = new ASN1InputStream(certificate.getEncoded());
        ASN1Primitive p = stream.readObject();
        stream.close();
        return p;
    }

    public static ASN1Primitive toDERObject(X509CRL crl)
    throws CRLException, IOException
    {
        final ASN1InputStream stream = new ASN1InputStream(crl.getEncoded());
        ASN1Primitive p = stream.readObject();
        stream.close();
        return p;
    }

    public static byte[] toByteArray(ASN1Encodable obj)
    throws IOException
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DEROutputStream       derOutput = new DEROutputStream(output);

        derOutput.writeObject(obj);
        derOutput.close();

        return output.toByteArray();
    }

    /**
     * Tries to get the octets from a DER encoded octet string.
     * @param derEncoded
     * @return
     * @throws IOException
     */
    public static byte[] getOctets(byte[] derEncoded)
    {
        if (derEncoded == null) {
            return null;
        }
        return ASN1OctetString.getInstance(derEncoded).getOctets();
    }

    /**
     * Converts the byte array to a DER encoded octet string.
     * @param data
     * @return
     * @throws IOException
     */
    public static byte[] toDEREncodedOctetString(byte[] data)
    throws IOException
    {
        return new DEROctetString(data).getEncoded(ASN1Encoding.DER);
    }

    public static ASN1Encodable fromExtensionValue(byte[] derEncoded)
    throws IOException
    {
        ASN1OctetString octetString = (ASN1OctetString) ASN1Primitive.fromByteArray(derEncoded);

        return ASN1Primitive.fromByteArray(octetString.getOctets());
    }
}
