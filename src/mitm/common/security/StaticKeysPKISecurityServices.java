/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.util.Collection;

import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.crl.CRLPathBuilderFactory;
import mitm.common.security.crl.RevocationChecker;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;

/**
 * Implementation of PKISecurityServices that works with {@link StaticKeysKeyAndCertStore}. All calls except
 * getKeyAndCertStore are delegated to the provided PKISecurityServices instance.
 * 
 * The StaticKeysPKISecurityServices is used when a message may only be decrypted with a subset of the private keys. 
 * 
 * @author Martijn Brinkers
 *
 */
public class StaticKeysPKISecurityServices implements PKISecurityServices
{
    /*
     * The delegate to delegate most calls to.
     */
    private final PKISecurityServices delegate;
    
    /*
     * The static KeyAndCertStore that's being used for #getKeyAndCertStore
     */
    private final KeyAndCertStore keyAndCertStore;

    public StaticKeysPKISecurityServices(PKISecurityServices delegate, Collection<KeyAndCertificate> keysAndCertificates)
    {
        Check.notNull(delegate, "delegate");
        Check.notNull(keysAndCertificates, "keysAndCertificates");
        
        this.delegate = delegate;
        
        keyAndCertStore = new StaticKeysKeyAndCertStore(delegate.getKeyAndCertStore(), keysAndCertificates);
    }
    
    @Override
    public KeyAndCertStore getKeyAndCertStore() {
        return keyAndCertStore;
    }

    @Override
    public X509CertStoreExt getRootStore() {
        return delegate.getRootStore();
    }

    @Override
    public X509CRLStoreExt getCRLStore() {
        return delegate.getCRLStore();
    }

    @Override
    public CertificatePathBuilderFactory getCertificatePathBuilderFactory() {
        return delegate.getCertificatePathBuilderFactory();
    }

    @Override
    public CRLPathBuilderFactory getCRLPathBuilderFactory() {
        return delegate.getCRLPathBuilderFactory();
    }

    @Override
    public PKITrustCheckCertificateValidatorFactory getPKITrustCheckCertificateValidatorFactory() {
        return delegate.getPKITrustCheckCertificateValidatorFactory();
    }

    @Override
    public RevocationChecker getCRLStoreRevocationChecker()
    throws CRLStoreException
    {
        return delegate.getCRLStoreRevocationChecker();
    }

    @Override
    public TrustAnchorBuilder getTrustAnchorBuilder() {
        return delegate.getTrustAnchorBuilder();
    }

    @Override
    public Encryptor getEncryptor() {
        return delegate.getEncryptor();
    }
}
