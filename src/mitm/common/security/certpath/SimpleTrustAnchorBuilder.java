/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SimpleTrustAnchorBuilder implements TrustAnchorBuilder 
{
    private final Set<TrustAnchor> trustAnchors = Collections.synchronizedSet(new HashSet<TrustAnchor>());
    
    @Override
    public void addCertificates(CertStore certStore) 
    throws CertStoreException
    {
        Collection<? extends Certificate> found = certStore.getCertificates(new X509CertSelector());
        
        addCertificates(found);
    }
    
    @Override
    public void addCertificates(Collection<? extends Certificate> certificates)
    throws CertStoreException
    {
        for (Certificate certificate : certificates)
        {
            /* only X509Certificats are supported */
            if (certificate instanceof X509Certificate)
            {
                TrustAnchor trustAnchor = new TrustAnchor((X509Certificate) certificate, null);
                
                trustAnchors.add(trustAnchor);
            }
        }
    }
    
    @Override
    public Set<TrustAnchor> getTrustAnchors() 
    throws CertStoreException
    {
        return Collections.unmodifiableSet(trustAnchors);
    }
    
    @Override
    public void refresh() {
        /* do nothing */
    }
}
