/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.common.security.NoSuchProviderRuntimeException;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;

/**
 * Implementation of CertificatePathBuilder that tries to build a certificate chain (aka path) using
 * the PKIX rules.
 * 
 * @author Martijn Brinkers
 *
 */
public class PKIXCertificatePathBuilder implements CertificatePathBuilder
{
    public static String NO_ROOTS_ERROR_MESSAGE = "There are no roots.";
    
    /*
     * Factory for creating security classes
     */
    private final SecurityFactory securityFactory;
    
    /*
     * The set of roots
     */
    private Set<TrustAnchor> trustAnchors;
    
    /*
     * We do not want revocation checking by default. Most of the times it's better to use
     * the dedicated CRL checker.
     */
    private boolean revocationEnabled = false;
    
    /*
     * The date used for validity checking etc.
     */
    private Date date;
    
    private Set<CertStore> certStores = new HashSet<CertStore>();
    private List<PKIXCertPathChecker> certPathCheckers = new LinkedList<PKIXCertPathChecker>();
    
    public PKIXCertificatePathBuilder() 
    {
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }
    
    @Override
    public void setTrustAnchors(Set<TrustAnchor> trustAnchors) {
        this.trustAnchors = trustAnchors;
    }
    
    @Override
    public Set<TrustAnchor> getTrustAnchors() 
    throws CertStoreException 
    {
        return trustAnchors;
    }
    
    @Override
    public void addCertStore(CertStore store) {
        certStores.add(store);
    }
    
    @Override
    public Set<CertStore> getCertStores() {
        return certStores;
    }
    
    @Override
    public void addCertPathChecker(PKIXCertPathChecker certPathChecker) {
        certPathCheckers.add(certPathChecker);
    }
    
    @Override
    public void setDate(Date date) {
        this.date = date;
    }
    
    @Override
    public void setRevocationEnabled(boolean enabled)
    {
        this.revocationEnabled = enabled;
    }
    
    @Override
    public CertPathBuilderResult buildPath(X509Certificate certificate)
    throws CertPathBuilderException
    {
        X509CertSelector certSelector = new X509CertSelector();
        
        certSelector.setCertificate(certificate);
        
        return buildPath(certSelector);
    }
    
    @Override
    public CertPathBuilderResult buildPath(X509CertSelector certSelector)
    throws CertPathBuilderException
    {
        try {
            Set<TrustAnchor> trustAnchors = getTrustAnchors(); 
            
            /*
             * trustAnchors cannot be null or empty because createPKIXBuilderParameters will
             * not accept it. We will therefore throw an exception if there are no trust anchors
             */
            if (trustAnchors == null || trustAnchors.size() == 0) {
                throw new CertPathBuilderException(NO_ROOTS_ERROR_MESSAGE);
            }
            
            PKIXBuilderParameters parameters = securityFactory.createPKIXBuilderParameters(
                    trustAnchors, certSelector);
            
            for (CertStore certStore : certStores) {
                parameters.addCertStore(certStore);            
            }
            
            for (PKIXCertPathChecker certPathChecker : certPathCheckers) {
                parameters.addCertPathChecker(certPathChecker);            
            }
            
            parameters.setRevocationEnabled(revocationEnabled);
            
            if (date != null) {
                parameters.setDate(date) ;
            }
            
            CertPathBuilder certPathbuilder;
            
            try {
                certPathbuilder = securityFactory.createCertPathBuilder("PKIX");
            }
            catch (NoSuchProviderException e) {
                throw new NoSuchProviderRuntimeException(e);
            }

            return certPathbuilder.build(parameters);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CertPathBuilderException(e);
        }
        catch (InvalidAlgorithmParameterException e) {
            throw new CertPathBuilderException(e);
        }
        catch (CertStoreException e) {
            throw new CertPathBuilderException(e);
        }
    }
}
