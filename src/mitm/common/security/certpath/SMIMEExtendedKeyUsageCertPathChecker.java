/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mitm.common.security.certificate.ExtendedKeyUsageType;
import mitm.common.security.certificate.X509CertificateInspector;

import org.bouncycastle.asn1.x509.X509Extension;

/**
 * If a certificate has an ExtendedKeyUsage extension this CertPathChecker
 * checks that the extended key usage contains S/MIME and or Any.
 *  
 * @author Martijn Brinkers
 *
 */
public class SMIMEExtendedKeyUsageCertPathChecker extends PKIXCertPathChecker
{
    public static final String MISSING_SMIME_EXTENDED_KEY_USAGE = "Certificate has a criticial 'extended key usage' but " + 
            "EMAILPROTECTION parameter is missing.";
    
    @Override
    public void check(Certificate certificate, Collection<String> unresolvedCritExts)
    throws CertPathValidatorException 
    {
        if (!(certificate instanceof X509Certificate)) {
            throw new CertPathValidatorException("Certificate is not a X509Certificate.");
        }

        /*
         * Check if the certificate has extended key usage set and if so that it contains EMAILPROTECTION ANYKEYUSAGE.
         */
        try {
            Set<ExtendedKeyUsageType> extendedKeyUsages = X509CertificateInspector.getExtendedKeyUsage(
                        (X509Certificate) certificate);
            
            if (extendedKeyUsages != null && !extendedKeyUsages.contains(ExtendedKeyUsageType.ANYKEYUSAGE) && 
                    !extendedKeyUsages.contains(ExtendedKeyUsageType.EMAILPROTECTION))
            {
                throw new MissingSMIMExtendedKeyUsageException(MISSING_SMIME_EXTENDED_KEY_USAGE);                
            }
            
            /*
             * Check if the unresolved critical extensions contains the extended key usage. If so we have
             * to remove it because we have handled it.
             */
            if (unresolvedCritExts != null && unresolvedCritExts.contains(X509Extension.extendedKeyUsage.getId()))
            {
                unresolvedCritExts.remove(X509Extension.extendedKeyUsage.getId());
            }
        }
        catch (CertificateParsingException e) {
            throw new CertPathValidatorException(e);
        }
    }

    @Override
    public Set<String> getSupportedExtensions() 
    {
        Set<String> supported = new HashSet<String>();
        
        supported.add(X509Extension.extendedKeyUsage.getId());
        
        return supported;
    }

    @Override
    public void init(boolean forward)
    throws CertPathValidatorException 
    {
        /* do nothing */
    }

    @Override
    public boolean isForwardCheckingSupported() 
    {
        return true;
    }
}
