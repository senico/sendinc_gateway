/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;

import mitm.common.security.certstore.BasicCertStore;
import mitm.common.security.certstore.jce.BasicCertStoreParameters;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.Check;

public class DefaultCertificatePathBuilderFactory implements CertificatePathBuilderFactory
{
	/*
	 * The Used to get the trusted anchors (AKA Roots).
	 */
	private final TrustAnchorBuilder trustAnchorBuilder;

	/*
	 * The certificate stores to add to the path builder
	 */
	private final BasicCertStore[] certStores;
	
	public DefaultCertificatePathBuilderFactory(TrustAnchorBuilder trustAnchorBuilder,
			BasicCertStore... certStores)
	{
		Check.notNull(trustAnchorBuilder, "trustAnchorBuilder");
		Check.notNull(certStores, "certStores");
		
		this.trustAnchorBuilder = trustAnchorBuilder;
		this.certStores = certStores;
	}
	
	
    /* 
     * we must convert the BasicCertStore to a JCE CertStore because CertificatePathBuilder requires
     * a CertStore.
     */
    private void addCertStore(CertificatePathBuilder pathBuilder, BasicCertStore certStore) 
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException 
    {
        CertStoreParameters certStoreParams = new BasicCertStoreParameters(certStore);
        
        CertStore jceCertStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, certStoreParams, MITMProvider.PROVIDER);
        pathBuilder.addCertStore(jceCertStore);
    }
    
    @Override
    public CertificatePathBuilder createCertificatePathBuilder() 
    throws CertPathBuilderException
    {
        CertificatePathBuilder pathBuilder = new TrustAnchorBuilderPKIXCertificatePathBuilder(
        		trustAnchorBuilder);
        
        /* 
         * we will not use the built in revocation checker. Revocation checking will be done separately.
         */
        pathBuilder.setRevocationEnabled(false);
        
        try {
	        for (BasicCertStore certStore : certStores) {
	        	addCertStore(pathBuilder, certStore);
	        }
	        
	        /*
	         * Add some checks for critical extension we know how to handle. 
	         */
	        pathBuilder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
	        pathBuilder.addCertPathChecker(new CRLDistPointCertPathChecker());	        
        }
        catch(InvalidAlgorithmParameterException e) {
        	throw new CertPathBuilderException(e);
        } 
        catch (NoSuchAlgorithmException e) {
        	throw new CertPathBuilderException(e);
		} 
        catch (NoSuchProviderException e) {
        	throw new CertPathBuilderException(e);
		}
        
        return pathBuilder;
    }
}
