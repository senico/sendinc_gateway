/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.Cipher;

import mitm.common.util.Check;
import mitm.common.util.ProcessRunner;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JCEPolicyManager
{
	private final static Logger logger = LoggerFactory.getLogger(JCEPolicyManager.class);
	
	/*
	 * The US export file from the unlimited strength policy file we need to install
	 */
	private static final String US_EXPORT_POLICY_FILE = "US_export_policy.jar";
	
    /*
     * The local policy file from the unlimited strength policy file we need to install
     */
	private static final String LOCAL_POLICY_FILE = "local_policy.jar";
	
	private final static int ZIP_BUFFER_SIZE = 2048;
	
	/*
	 * The maximum time a command may run after which it is destroyed
	 */
	private final static long TIMEOUT = 30 * DateUtils.MILLIS_PER_SECOND;

	/*
	 * The install script (script that will copy the jars to the correct location of the JRE)
	 */
	private final File script;
	
	public JCEPolicyManager(File script)
	{
	    Check.notNull(script, "script");
	    
	    this.script = script;
	}
	
	/**
	 * Returns true if the JCE unlimited strength jurisdiction policy files are installed.
	 */
	public static boolean isUnlimitedStrength()
	{
		boolean unlimited = false;
		
		try {
			int maxAllowed = Cipher.getMaxAllowedKeyLength("AES");
			
			unlimited = maxAllowed == Integer.MAX_VALUE;    		
		} 
		catch (NoSuchAlgorithmException e) {
			logger.warn("Unable to instantiate cipher.");
		} 
		
		return unlimited;
	}

	private void copyPolicyFile(File source, File destination) 
	throws IOException 
	{
		List<String> cmd = new LinkedList<String>();
		
        cmd.add(script.getPath());
        
        cmd.add(source.getAbsolutePath());
        cmd.add(destination.getAbsolutePath());
        
        ProcessRunner runner = new ProcessRunner();
        
        runner.setTimeout(TIMEOUT);
                    
        runner.run(cmd);
	}
	
	/*
	 * Returns the path where the jce policy files should be stored.
	 */
	private File getJCEPolicyLibPath()
	{
		return new File(System.getProperty("java.home"), "/lib/security/");
	}
	
	private void installJCEPolicy(ZipInputStream zis, String filename) 
	throws IOException
	{
		byte data[] = new byte[ZIP_BUFFER_SIZE];

		File jarFile = File.createTempFile("jce", ".jar");
		
		try {
			FileOutputStream fos = new FileOutputStream(jarFile);
			
			OutputStream output = new BufferedOutputStream(fos, ZIP_BUFFER_SIZE);

			File destination = new File(getJCEPolicyLibPath(), filename);
			
			try {
				int count;
		
				while ((count = zis.read(data, 0, ZIP_BUFFER_SIZE)) != -1) {
					output.write(data, 0, count);
				}
			}
			finally {
				output.flush();
				output.close();
			}
			
			copyPolicyFile(jarFile, destination);
		} 
		finally {
			jarFile.delete();
		}
	}
	
	/**
	 * Installs the jce policy into the current JRE (JRE is found using java.home system property). The input must
	 * 
	 * @param jcePolicy an input stream to the policy file in zip format as downloaded from SUN site.
	 * @param copyScript path to the shell script that will copy the jar file to the correct location
	 * @throws IOException 
	 */
	public void installJCEPolicy(InputStream jcePolicy)
	throws IOException
	{
		/*
		 * decodedPolicy should now contain a zip file with the policy files
		 */
		ZipInputStream zis = new ZipInputStream(jcePolicy);
		
		ZipEntry zipEntry;
		
		boolean exportFound = false;
		boolean localPolicyFound = false;
		
		while ((zipEntry = zis.getNextEntry()) != null)
		{
			String name = zipEntry.getName();
			
			if (name == null) {
				continue;
			}
			
			if (name.endsWith("/" + US_EXPORT_POLICY_FILE))
			{
				installJCEPolicy(zis, US_EXPORT_POLICY_FILE);
				
				exportFound = true;
			}
			
			if (name.endsWith("/" + LOCAL_POLICY_FILE))
			{
				installJCEPolicy(zis, LOCAL_POLICY_FILE);
				
				localPolicyFound = true;
			}
		}
		
		if (!exportFound || !localPolicyFound) {
			throw new IOException("Not all policy files were found in the zip.");
		}
	}
}
