/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.crl.CRLPathBuilderFactory;
import mitm.common.security.crl.RevocationChecker;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crypto.Encryptor;

public interface PKISecurityServices
{
    /**
     * Returns the store which stores certificates and keys.
     */
    public KeyAndCertStore getKeyAndCertStore();

    /**
     * Returns the RootStore. 
     */
    public X509CertStoreExt getRootStore();
    
    /**
     * Returns the CRLStore. 
     */
    public X509CRLStoreExt getCRLStore();
    
    /**
     * Returns a factory to create a path builder used to check the trust of a certificate. 
     * Most likely the CertificatePathBuilder returned from the factory is not thread safe and 
     * should therefore be created on-demand and not shared between threads..
     */
    public CertificatePathBuilderFactory getCertificatePathBuilderFactory();

    /**
     * Returns a factory to create a CRL path builder used to check the trust of a CRL. 
     * Most likely the CRLPathBuilder returned from the factory is not thread safe and 
     * should therefore be created on-demand and not shared between threads..
     */
    public CRLPathBuilderFactory getCRLPathBuilderFactory();
    
    /**
     * Creates a PKITrustCheckCertificateValidatorFactory which can be used to check the trust
     * status of a certificate (not revoked, not expired, signed by a trusted root etc.). 
     * Most likely the PKITrustCheckCertificateValidator created is not thread safe and should 
     * therefore be created on-demand and not shared between threads.
     */
    public PKITrustCheckCertificateValidatorFactory getPKITrustCheckCertificateValidatorFactory();
    
    /**
     * Returns the CRL revocation checker.
     */
    public RevocationChecker getCRLStoreRevocationChecker() 
    throws CRLStoreException;
    
    /**
     * Returns the associated TrustAnchorBuilder.
     */
    public TrustAnchorBuilder getTrustAnchorBuilder();

    /**
     * Returns the system encryptor. Strictly speaking the Encryptor is not necessary a PKI
     * based encryptor.
     */
    public Encryptor getEncryptor();
}
