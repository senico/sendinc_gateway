/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.security.PublicKey;
import java.security.interfaces.DSAKey;
import java.security.interfaces.RSAKey;

import javax.crypto.interfaces.DHKey;

public class PublicKeyInspector 
{
    /**
     * Returns the key length of the public key.
     */
    public static int getKeyLength(PublicKey key)
    {
        int length = -1;
        
        if (key instanceof RSAKey) {
            length = ((RSAKey) key).getModulus().bitLength();
        }
        else if (key instanceof DSAKey) {
            length = ((DSAKey) key).getParams().getP().bitLength();
        }
        else if (key instanceof DHKey) {
            length = ((DHKey) key).getParams().getP().bitLength();
        }
        
        return length;
    }
    
    /**
     * Returns a string representation of the public key algorithm.
     */
    public static String getFriendlyAlgorithm(PublicKey key)
    {
        String algorithm = "Unknown";
        
        if (key instanceof RSAKey) {
            algorithm = "RSA";
        }
        else if (key instanceof DSAKey) {
            algorithm = "DSA";
        }
        else if (key instanceof DHKey) {
            algorithm = "DH";
        }
        
        return algorithm;
    }    
}
