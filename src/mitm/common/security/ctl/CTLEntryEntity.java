/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ctl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mitm.common.util.Check;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.MapKey;
import org.hibernate.annotations.Type;

@Entity(name = CTLEntryEntity.ENTITY_NAME)
@Table(name = CTLEntryEntity.TABLE_NAME,
        uniqueConstraints = {@UniqueConstraint(columnNames={"name", "thumbprint"})}
)
public class CTLEntryEntity implements CTLEntry
{
    public static final String TABLE_NAME = "ctl";
    public static final String ENTITY_NAME = "ctl";

    private static final int MAX_NAME_LENGTH = 255;
    private static final int MAX_THUMBPRINT_LENGTH = 255;

    private static final String STATUS_PROPERTY_NAME = "status";
    private static final String ALLOW_EXPIRED_PROPERTY_NAME = "allowExpired";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /*
     * A CTLEntry is 'owned' by a named CTL
     */
    @Column (name = "name", unique = false, nullable = true, length = MAX_NAME_LENGTH)
    private String name;

    @Column (name = "thumbprint", unique = false, nullable = false, length = MAX_THUMBPRINT_LENGTH)
    private String thumbprint;

    /*
     * We will store some properties as name/value pairs. The advantage
     * is that that we do not need to change the database structure when
     * we want to add new properties. Although this probably is not
     * "how it should be done" with a relational database I prefer this
     * approach because database changes are a pain.
     */
    @CollectionOfElements
    @Column(name = "value")
    @MapKey(columns = {@Column(name = "name")})
    @Type(type="text")
    private Map<String, String> nameValues = new HashMap<String, String>();

    protected CTLEntryEntity() {
        /* Hibernate requires a default constructor */
    }

    public CTLEntryEntity(String name, String thumbprint)
    {
        Check.notNull(thumbprint, "thumbprint");

        this.name = name;
        this.thumbprint = thumbprint;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getThumbprint() {
        return thumbprint;
    }

    @Override
    public CTLEntryStatus getStatus()
    {
        CTLEntryStatus status = CTLEntryStatus.fromName(nameValues.get(STATUS_PROPERTY_NAME));

        if (status == null) {
            status = CTLEntryStatus.WHITELISTED;
        }

        return status;
    }

    @Override
    public void setStatus(CTLEntryStatus status) {
        nameValues.put(STATUS_PROPERTY_NAME, ObjectUtils.toString(status, null));
    }

    @Override
    public boolean isAllowExpired()
    {
        String value = nameValues.get(ALLOW_EXPIRED_PROPERTY_NAME);

        return value != null ? BooleanUtils.toBoolean(value) : false;
    }

    @Override
    public void setAllowExpired(boolean allowExpired) {
        nameValues.put(ALLOW_EXPIRED_PROPERTY_NAME, Boolean.toString(allowExpired));
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof CTLEntryEntity)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        CTLEntryEntity rhs = (CTLEntryEntity) obj;

        return new EqualsBuilder()
            .append(name, rhs.name)
            .append(thumbprint, rhs.thumbprint)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .append(thumbprint)
            .toHashCode();
    }
}
