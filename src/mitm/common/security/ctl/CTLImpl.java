/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ctl;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.List;

import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CTLImpl implements CTL
{
    private final static Logger logger = LoggerFactory.getLogger(CTLImpl.class);
    
    protected final static String NOT_LISTED_MESSAGE  = "Certificate not found in CTL.";
    protected final static String BLACKLISTED_MESSAGE = "Certificate found in CTL and is blacklisted.";
    protected final static String EXPIRED_MESSAGE     = "Certificate found in CTL and is whitelisted but expired.";
    protected final static String WHITELISTED_MESSAGE = "Certificate found in CTL and is whitelisted.";
    
    private final String name;
    
    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
    protected CTLImpl(String name, SessionManager sessionManager)
    {
        Check.notNull(sessionManager, "sessionManager");
        
        this.name = name;
        this.sessionManager = sessionManager;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public List<? extends CTLEntry> getEntries(Integer firstResult, Integer maxResults)
    throws CTLException
    {
        return getDAO().getCTLs(firstResult, maxResults);
    }

    @Override
    public int size()
    throws CTLException
    {
        return getDAO().size();
    }
    
    @Override
    public void deleteAll()
    throws CTLException
    {
        getDAO().delete();
    }
    
    @Override
    public CTLEntry getEntry(String thumbprint)
    throws CTLException
    {
        if (StringUtils.isEmpty(thumbprint)) {
            return null;
        }
        
        return getDAO().getCTL(thumbprint);
    }

    @Override
    public CTLEntry getEntry(X509Certificate certificate)
    throws CTLException
    {
        if (certificate == null) {
            return null;
        }
        
        try {
            return getEntry(X509CertificateInspector.getThumbprint(certificate));
        }
        catch (CertificateEncodingException e) {
            throw new CTLException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CTLException(e);
        }
        catch (NoSuchProviderException e) {
            throw new CTLException(e);
        }
    }
    
    @Override
    public CTLEntry createEntry(String thumbprint)
    throws CTLException
    {
        return new CTLEntryEntity(name, thumbprint);
    }

    @Override
    public CTLEntry createEntry(X509Certificate certificate)
    throws CTLException
    {
        Check.notNull(certificate, "certificate");
        
        try {
            return createEntry(X509CertificateInspector.getThumbprint(certificate));
        }
        catch (CertificateEncodingException e) {
            throw new CTLException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CTLException(e);
        }
        catch (NoSuchProviderException e) {
            throw new CTLException(e);
        }
    }
    
    @Override
    public void addEntry(CTLEntry entry)
    throws CTLException
    {
        if (!(entry instanceof CTLEntryEntity)) {
            throw new CTLException("This CTL only accepts CTLEntryEntity.");
        }
         
        getDAO().makePersistent((CTLEntryEntity) entry);
    }
    
    @Override
    public void deleteEntry(CTLEntry entry)
    throws CTLException
    {
        if (!(entry instanceof CTLEntryEntity)) {
            throw new CTLException("This CTL only accepts CTLEntryEntity.");
        }
         
        getDAO().delete((CTLEntryEntity) entry);
    }
    
    @Override
    public CTLValidityResult checkValidity(X509Certificate certificate)
    throws CTLException
    {
        Check.notNull(certificate, "certificate");
        
        CTLEntry entry = getEntry(certificate);
        
        if (entry == null)
        {
            logger.debug(NOT_LISTED_MESSAGE);

            return new CTLValidityResultImpl(CTLValidity.NOT_LISTED, NOT_LISTED_MESSAGE);
        }
        
        CTLEntryStatus status = entry.getStatus();
        
        if (CTLEntryStatus.BLACKLISTED == status)
        {
            logger.debug(BLACKLISTED_MESSAGE);
            
            return new CTLValidityResultImpl(CTLValidity.INVALID, BLACKLISTED_MESSAGE);
        }
        else if (CTLEntryStatus.WHITELISTED == status)
        {
            if (!entry.isAllowExpired() && X509CertificateInspector.isExpired(certificate))
            {
                logger.debug(EXPIRED_MESSAGE);
                
                return new CTLValidityResultImpl(CTLValidity.INVALID, EXPIRED_MESSAGE);
            }

            logger.debug(WHITELISTED_MESSAGE);
            
            return new CTLValidityResultImpl(CTLValidity.VALID, WHITELISTED_MESSAGE);
        }
            
        throw new IllegalStateException("Unknown CTLEntryStatus: " + status);
    }
    
    private CTLDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new CTLDAO(session, name);
    }
}
