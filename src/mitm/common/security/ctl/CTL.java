/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ctl;

import java.security.cert.X509Certificate;
import java.util.List;

/**
 * A Certificate Trust List (CTL) contains entries that determine whether a certificate
 * is valid or not. This can be used as a certificate white- or blacklist. The main 
 * difference between a CTL and a CRL is that a CRL is issued by the issuer of a certificate
 * whereas a CTL is 'issued' by the user of a certificate. 
 * 
 * @author Martijn Brinkers
 *
 */
public interface CTL
{
    /**
     * A CTL has a certain name.
     */
    public String getName();
    
    /**
     * Returns all the CTL entries.
     */
    public List<? extends CTLEntry> getEntries(Integer firstResult, Integer maxResults)
    throws CTLException;

    /**
     * Returns the number of CTL entries.
     */
    public int size()
    throws CTLException;

    /**
     * Removes all entries
     */
    public void deleteAll()
    throws CTLException;
    
    /**
     * Return the CRL entry with the given thumbprint. Returns null if there is no CTL entry
     * with the given thumbprint.
     */
    public CTLEntry getEntry(String thumbprint)
    throws CTLException;

    /**
     * Return the CRL entry with the given certificate thumbprint. Returns null if there is no CTL entry
     * with the given thumbprint. 
     * 
     * This is a conveniance overload of getCTLEntry(String)
     */
    public CTLEntry getEntry(X509Certificate certificate)
    throws CTLException;
    
    /**
     * Creates a new CTL entry. The CRL entry is only created and not added.
     */
    public CTLEntry createEntry(String thumbprint)
    throws CTLException;

    /**
     * Creates a new CTL entry. The CRL entry is only created and not added.
     * 
     * This is a conveniance overload of createCTLEntry(String)
     */
    public CTLEntry createEntry(X509Certificate certificate)
    throws CTLException;
    
    /**
     * Adds the CTL entry.
     */
    public void addEntry(CTLEntry entry)
    throws CTLException;

    /**
     * Removes the CTL entry.
     */
    public void deleteEntry(CTLEntry entry)
    throws CTLException;
    
    /**
     * Returns true if the certificate is valid. Whether a certificate is valid depends
     * on whether a CTL entry is found for the certificate and on the settings of the
     * CRL entry. If there is no CTL entry for the given certificate isValid always 
     * returns false. 
     */
    public CTLValidityResult checkValidity(X509Certificate certificate)
    throws CTLException;
}
