/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.TrustAnchor;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;

import mitm.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import mitm.common.security.certificate.X509CertificateBuilder;
import mitm.common.security.crl.X509CRLBuilder;
import mitm.common.security.crl.X509CRLBuilderImpl;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.security.provider.MITMProvider;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class DefaultSecurityFactory implements SecurityFactory
{
    /*
     * The majority of factory funtions will be delegated to the Bouncycastle factory
     */
    private final SecurityFactory bouncyCastleSecurityFactory;
    
    public DefaultSecurityFactory() 
    throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        bouncyCastleSecurityFactory = new SecurityFactoryBouncyCastle();
        
        /*
         * The MITMProvider must be initialized
         */
        MITMProvider.initialize(null);
    }
    
    @Override
    public CertPathBuilder createCertPathBuilder(String algorithm)
    throws NoSuchProviderException, NoSuchAlgorithmException 
    {
        return bouncyCastleSecurityFactory.createCertPathBuilder(algorithm);
    }

    @Override
    public CertStore createCertStore(String certStoreType, CertStoreParameters certStoreParameters)
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createCertStore(certStoreType, certStoreParameters);
    }

    @Override
    public CertificateFactory createCertificateFactory(String certificateType)
    throws CertificateException, NoSuchProviderException 
    {
        /*
         * We will use the MITM provider for the CertificateFactory because the MITM provider is
         * optimized for very large CRLs (like the DOD crl)
         */
        return CertificateFactory.getInstance(certificateType, MITMProvider.PROVIDER);
    }

    @Override
    public Cipher createCipher(String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException 
    {
        return bouncyCastleSecurityFactory.createCipher(algorithm);
    }

    @Override
    public RandomGenerator createRandomGenerator()
	throws NoSuchAlgorithmException, NoSuchProviderException
	{
    	return bouncyCastleSecurityFactory.createRandomGenerator();
	}
    
    @Override
    public KeyFactory createKeyFactory(String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createKeyFactory(algorithm);
    }

    @Override
    public KeyPairGenerator createKeyPairGenerator(String keyType)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createKeyPairGenerator(keyType);
    }

    @Override
    public KeyStore createKeyStore(String keyStoreType)
    throws KeyStoreException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createKeyStore(keyStoreType);
    }

    @Override
    public MessageDigest createMessageDigest(String digestType)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createMessageDigest(digestType);
    }

    @Override
    public PKIXBuilderParameters createPKIXBuilderParameters(Set<TrustAnchor> trustAnchors, CertSelector targetConstraints)
    throws InvalidAlgorithmParameterException 
    {
        return bouncyCastleSecurityFactory.createPKIXBuilderParameters(trustAnchors, targetConstraints);
    }

    @Override
    public SecretKeyFactory createSecretKeyFactory(String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createSecretKeyFactory(algorithm);
    }

    @Override
    public SecureRandom createSecureRandom(String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createSecureRandom();
    }

    @Override
    public SecureRandom createSecureRandom()
    throws NoSuchAlgorithmException, NoSuchProviderException 
    {
        return bouncyCastleSecurityFactory.createSecureRandom();
    }

    @Override
    public X509CertificateBuilder createX509CertificateBuilder() 
    {
        return bouncyCastleSecurityFactory.createX509CertificateBuilder();
    }

    @Override
    public Mac createMAC(String algorithm)
	throws NoSuchAlgorithmException, NoSuchProviderException
	{
    	return bouncyCastleSecurityFactory.createMAC(algorithm);
	}
    
    @Override
    public X509CRLBuilder createX509CRLBuilder()
    {
        /*
         * Use the optimized X509CRL from the MITMProvider for the creation of the CRL 
         */
        return new X509CRLBuilderImpl(BouncyCastleProvider.PROVIDER_NAME /* siging provider */, 
                MITMProvider.PROVIDER /* CRL provider */);
    }
    
    @Override
    public String getNonSensitiveProvider() {
        return bouncyCastleSecurityFactory.getNonSensitiveProvider();
    }

    @Override
    public String getSensitiveProvider() {
        return bouncyCastleSecurityFactory.getSensitiveProvider();
    }
}
