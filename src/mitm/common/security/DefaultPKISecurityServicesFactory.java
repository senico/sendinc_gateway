/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.crl.CRLPathBuilderFactory;
import mitm.common.security.crl.RevocationChecker;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crypto.Encryptor;
import mitm.common.util.Check;

public class DefaultPKISecurityServicesFactory implements PKISecurityServicesFactory
{
    private final KeyAndCertStore keyAndCertStore;
    private final X509CertStoreExt rootStore;
    private final X509CRLStoreExt crlStore;
    private final TrustAnchorBuilder trustAnchorBuilder;
    private final RevocationChecker revocationChecker;
    private final Encryptor encryptor;
    private final CertificatePathBuilderFactory certificatePathBuilderFactory;
    private final CRLPathBuilderFactory crlPathBuilderFactory;
    private final PKITrustCheckCertificateValidatorFactory certificateValidatorFactory;
    
    public DefaultPKISecurityServicesFactory(KeyAndCertStore keyAndCertStore, X509CertStoreExt rootStore, 
    		X509CRLStoreExt crlStore, TrustAnchorBuilder trustAnchorBuilder, RevocationChecker revocationChecker, 
    		Encryptor encryptor, CertificatePathBuilderFactory certificatePathBuilderFactory,
    		CRLPathBuilderFactory crlPathBuilderFactory,
    		PKITrustCheckCertificateValidatorFactory certificateValidatorFactory)
    {
    	Check.notNull(keyAndCertStore, "keyAndCertStore");
    	Check.notNull(rootStore, "rootStore");
    	Check.notNull(crlStore, "crlStore");
    	Check.notNull(trustAnchorBuilder, "trustAnchorBuilder");
    	Check.notNull(revocationChecker, "revocationChecker");
    	Check.notNull(encryptor, "encryptor");
    	Check.notNull(certificatePathBuilderFactory, "certificatePathBuilderFactory");
    	Check.notNull(crlPathBuilderFactory, "crlPathBuilderFactory");
    	Check.notNull(certificateValidatorFactory, "certificateValidatorFactory");
    	
        this.keyAndCertStore = keyAndCertStore;
        this.rootStore = rootStore;
        this.crlStore = crlStore;
        this.trustAnchorBuilder = trustAnchorBuilder;
        this.revocationChecker = revocationChecker;
        this.encryptor = encryptor;
        this.certificatePathBuilderFactory = certificatePathBuilderFactory;
        this.crlPathBuilderFactory = crlPathBuilderFactory;
        this.certificateValidatorFactory = certificateValidatorFactory;
    }
    
    @Override
    public PKISecurityServices createPKISecurityServices() {
        return new PKISecurityServicesImpl();
    }

    private class PKISecurityServicesImpl implements PKISecurityServices
    {
        @Override
        public KeyAndCertStore getKeyAndCertStore() {
            return keyAndCertStore;
        }

        @Override
        public X509CertStoreExt getRootStore() {
            return rootStore;
        }

        @Override
        public X509CRLStoreExt getCRLStore() {
            return crlStore;
        }
        
        @Override
        public CertificatePathBuilderFactory getCertificatePathBuilderFactory()
        {
        	return certificatePathBuilderFactory;
        }

        @Override
        public CRLPathBuilderFactory getCRLPathBuilderFactory() {
        	return crlPathBuilderFactory;
        }

        @Override
        public PKITrustCheckCertificateValidatorFactory getPKITrustCheckCertificateValidatorFactory() {
        	return certificateValidatorFactory;
        }
		
        @Override
        public RevocationChecker getCRLStoreRevocationChecker() 
        throws CRLStoreException 
        {
            return revocationChecker;            
        }
        
        @Override
        public TrustAnchorBuilder getTrustAnchorBuilder() {
            return trustAnchorBuilder;            
        }
        
        @Override
        public Encryptor getEncryptor() {
        	return encryptor;
        }
    }
}
