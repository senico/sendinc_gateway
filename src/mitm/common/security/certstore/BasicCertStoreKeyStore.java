/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;

import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorAdapter;

/**
 * 
 * Simple CertProvider implementation that allows a KeyStore to be used as a X509BasicCertStore.  
 * It is recommended to use an optimized X509BasicCertStore. This X509BasicCertStore implementation will 
 * serially step through all available certificates which is slow when a lot of certificates
 * are involved. 
 * 
 * @author Martijn Brinkers
 *
 */
public class BasicCertStoreKeyStore implements X509BasicCertStore
{
    private final KeyStore keyStore;
        
    /**
     * The given keyStore will be used for the CertProvider. 
     * 
     * @param keyStore
     */
    public BasicCertStoreKeyStore(KeyStore keyStore) 
    {
        this.keyStore = keyStore;
    }

    @Override
    public Collection<X509Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        Collection<X509Certificate> certificates = new LinkedList<X509Certificate>();
        
        try {
            Enumeration<String> aliases = keyStore.aliases();
            
            while (aliases.hasMoreElements()) 
            {
                String alias = aliases.nextElement();
                
                Certificate certificate = keyStore.getCertificate(alias);
                
                if (certificate != null && certificate instanceof X509Certificate) 
                {
                    if (certSelector == null || certSelector.match(certificate)) {
                        certificates.add((X509Certificate) certificate);
                    }
                }
                
            }
        }
        catch(KeyStoreException e) {
            throw new CertStoreException(e);
        }
        
        return certificates;
    }
    
    @Override
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector) 
    throws CertStoreException
    {
        Collection<X509Certificate> certificates = getCertificates(certSelector);
        
        return new CloseableIteratorAdapter<X509Certificate>(certificates.iterator());
    }
}
