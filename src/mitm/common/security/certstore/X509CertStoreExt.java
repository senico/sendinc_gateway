/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore;

import java.io.IOException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import mitm.common.util.CloseableIterator;

public interface X509CertStoreExt extends X509BasicCertStore
{
    /**
     * Searches for entries with matching email address
     * @return List of entries or null of no match was found
     */
    public CloseableIterator<? extends X509CertStoreEntry> getByEmail(String email, Match match, Expired expired, 
            MissingKeyAlias missingKeyAlias)
    throws CertStoreException;

    /**
     * Searches for entries with matching email address (bounded)
     * @return List of entries or null of no match was found
     */
    public CloseableIterator<? extends X509CertStoreEntry> getByEmail(String email, Match match, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException;

    /**
     * Returns the number of entries that will be returned by a similar call to getByEmail.
     */
    public long getByEmailCount(String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException;
    
    /**
     * Searches the subject friendly name using ILIKE.
     */
    public CloseableIterator<? extends X509CertStoreEntry> searchBySubject(String subject, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException;

    /**
     * Returns the number of entries that will be returned by a similar call to searchBySubject.
     */
    public long getSearchBySubjectCount(String subject, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException;
    
    /**
     * Searches the issuer friendly name using ILIKE.
     */
    public CloseableIterator<? extends X509CertStoreEntry> searchByIssuer(String issuer, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException;

    /**
     * Returns the number of entries that will be returned by a similar call to searchByIssuer.
     */
    public long getSearchByIssuerCount(String issuer, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException;
    
    /**
     * Searches for an entry with the given thumbprint (SHA-512)
     * @param alias
     * @return List of entries or null of no match was found
     */
    public X509CertStoreEntry getByThumbprint(String thumbprint)
    throws CertStoreException;
    
    /**
     * Searches for an entry with the given Certificate (using the thumbprint as the search key)
     * @param certificate
     * @return Entry or null of no match was found
     */
    public X509CertStoreEntry getByCertificate(X509Certificate certificate)
    throws CertStoreException;

    /**
     * Returns true if the certificate is in the store.
     * @param certificate
     * @return
     * @throws CertStoreException
     * @throws IOException
     */
    public boolean contains(X509Certificate certificate)
    throws CertStoreException; 
    
    /**
     * Returns the entry that was added last (ie the newest entry)
     */
    public X509CertStoreEntry getLatest();

    /**
     * Returns the number of entries in this store  
     */
    public long size();

    /**
     * Returns the number of entries in this store  
     */
    public long size(Expired expired, MissingKeyAlias missingKeyAlias);
    
    /**
     * Adds the certificate to the database with the associated alias
     * @param certificate
     * @param keyAlias associated key alias (null is allowed)
     * @return the newly created entry
     * @throws CertificateParsingException
     * @throws IOException
     */
    public X509CertStoreEntry addCertificate(X509Certificate certificate, String keyAlias) 
    throws CertStoreException, CertificateAlreadyExistsException;

    /**
     * Adds the certificate to the database. The certPath and key alias will be null. 
     * @param certificate
     * @return the newly created entry
     * @throws CertificateParsingException
     * @throws IOException
     */
    public X509CertStoreEntry addCertificate(X509Certificate certificate)
    throws CertStoreException, CertificateAlreadyExistsException;

    /**
     * Removes the entry identified by the given certificate if available. 
     * @param certificate
     * @throws CertStoreException
     * @throws IOException
     */
    public void removeCertificate(X509Certificate certificate)
    throws CertStoreException;
    
    /**
     * Remove all X509CertStoreEntries.
     * @throws CertStoreException 
     */
    public void removeAllEntries() 
    throws CertStoreException;
    
    /**
     * Returns a bounded iterator of X509CertStoreEntry objects for which the certificate in these entries 
     * match the given selector.
     * @throws CertStoreException 
     */
    public CloseableIterator<? extends X509CertStoreEntry> getCertStoreIterator(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
            Integer firstResult, Integer maxResults) 
    throws CertStoreException;
    
    /**
     * Returns a bounded collection of X509Certificates from the store that match the given selector.
     */
    @Override
    public Collection<X509Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException;
    
    /**
     * Returns an iterator over the X509Certificates from the store that match the given selector.
     */
    @Override
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector) 
    throws CertStoreException;

    /**
     * Sets the listener that listens for store events
     */
    public void setStoreEventListener(X509StoreEventListener eventListener);
}
