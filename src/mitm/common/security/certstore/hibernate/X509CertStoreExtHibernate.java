/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.hibernate;

import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import mitm.common.hibernate.CommitOnCloseIterator;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.security.certstore.CertificateAlreadyExistsException;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.X509StoreEventListener;
import mitm.common.security.certstore.dao.X509CertStoreDAO;
import mitm.common.security.certstore.dao.X509CertStoreDAOHibernate;
import mitm.common.util.Check;
import mitm.common.util.CloseableIterator;

import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class X509CertStoreExtHibernate implements X509CertStoreExt
{
    /*
     * The name of this store. The database can store more than one CertStore.
     */
    private String storeName;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;
    
    /*
     * Listener that will be notified when the store changes (add or remove certificates)
     */
    private final AtomicReference<X509StoreEventListener> eventListener = new AtomicReference<X509StoreEventListener>();
    
    
    public X509CertStoreExtHibernate(String storeName, SessionManager sessionManager)
    {
    	Check.notNull(sessionManager, "sessionManager");
    	
        this.storeName = storeName;
        this.sessionManager = sessionManager;
    }
            
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
         
    @Override
    public X509CertStoreEntryHibernate addCertificate(X509Certificate certificate, String keyAlias) 
    throws CertStoreException, CertificateAlreadyExistsException
    {
        /*
         * we will check whether this certificate is already in the store because a certificate
         * may only be stored once. Adding this check does not guarantee that the certificate will 
         * be added when the transaction is committed because a 'race condition' is always 
         * possible.
         */
        X509CertStoreEntryHibernate entry = getByCertificate(certificate);
        
        if (entry != null) {
            throw new CertificateAlreadyExistsException("Certificate is already stored.");
        }

        entry = getDAO().addCertificate(certificate, keyAlias);
        
        fireChangeEvent();
        
        return entry;
    }

    @Override
    public X509CertStoreEntryHibernate addCertificate(X509Certificate certificate)
    throws CertStoreException 
    {
        return addCertificate(certificate, null);
    }
    
    @Override
    public void removeCertificate(X509Certificate certificate) 
    throws CertStoreException
    {
        getDAO().removeCertificate(certificate);

        fireChangeEvent();
    }
    
    @Override
    public void removeAllEntries() 
    throws CertStoreException
    {
        getDAO().removeAllEntries();
        
        fireChangeEvent();
    }
     
    @Override
    public X509CertStoreEntryHibernate getLatest()
    {
        return getDAO().getLatest();
    }

    @Override
    public long size() {
        return getDAO().getRowCount();
    }
    
    @Override
    public long size(Expired expired, MissingKeyAlias missingKeyAlias) {
        return getDAO().getRowCount(expired, missingKeyAlias, new Date());
    }
    
    @Override
    public X509CertStoreEntryHibernate getByCertificate(X509Certificate certificate)
    throws CertStoreException 
    {
        return getDAO().getByCertificate(certificate);
    }    

    @Override
    public boolean contains(X509Certificate certificate)
    throws CertStoreException
    {
        return getDAO().getByCertificate(certificate) != null;
    }

    @Override
    public CloseableIterator<X509CertStoreEntryHibernate> getByEmail(String email, Match match,
            Expired expired, MissingKeyAlias missingKeyAlias) 
    throws CertStoreException
    {
        return getByEmail(email, match, expired, missingKeyAlias, null, null);
    }
    
    @Override
    public CloseableIterator<X509CertStoreEntryHibernate> getByEmail(String email, Match match, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().getByEmail(email, match, expired, missingKeyAlias, new Date(), 
        		firstResult, maxResults);        
    }
        
    @Override
    public long getByEmailCount(String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
    	return getDAO().getByEmailCount(email, match, expired, missingKeyAlias, new Date());
    }

    @Override
    public CloseableIterator<X509CertStoreEntryHibernate> searchBySubject(String subject, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().searchBySubject(subject, expired, missingKeyAlias, new Date(), firstResult, maxResults);
    }

    @Override
    public long getSearchBySubjectCount(String subject, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
    	return getDAO().getSearchBySubjectCount(subject, expired, missingKeyAlias, new Date());
    }
    
    @Override
    public CloseableIterator<X509CertStoreEntryHibernate> searchByIssuer(String issuer, Expired expired, 
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().searchByIssuer(issuer, expired, missingKeyAlias, new Date(), firstResult, maxResults);
    }

    @Override
    public long getSearchByIssuerCount(String issuer, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
    	return getDAO().getSearchByIssuerCount(issuer, expired, missingKeyAlias, new Date());
    }
    
    @Override
    public X509CertStoreEntryHibernate getByThumbprint(String thumbprint) {
        return getDAO().getByThumbprint(thumbprint);
    }
    
    @Override
    public CloseableIterator<X509CertStoreEntryHibernate> getCertStoreIterator(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
        Integer firstResult, Integer maxResults) 
    throws CertStoreException
    {
        return getDAO().getCertStoreIterator(certSelector, missingKeyAlias, firstResult, 
        		maxResults);        
    }

    /*
     * A call to getCertificates can result in a lot of records being returned which can result in an 
     * out of memory because the first level cache grows with every object. There are a lot of possible
     * solutions but we will use a StatelessSession because we are only interested in the certificate
     * and not in the entry itself (StatelessSession does not load collection etc.). The drawback of 
     * this approach is that getCertificates has it's own transaction.
     * 
     * Note: Because the statelessSession uses it's own session/transaction it can happen that changes made in other
     * sessions/transactions are not seen until the transactions have been committed.
     */
    public Collection<X509Certificate> getCertificates(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {   
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        try {
            Collection<X509Certificate> certificates = getStatelessDAO(statelessSession).getCertificates(certSelector, 
                    missingKeyAlias, firstResult, maxResults);
            
            tx.commit();
            
            return certificates;
        }
        catch(CertStoreException e) {
            tx.rollback();
            
            throw e;
        }
        finally {
            statelessSession.close();
        }
    }

    @Override
    public Collection<X509Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        return getCertificates(certSelector, MissingKeyAlias.ALLOWED, null, null);
    }

    /*
     * We want to use a StatelessSession for performance and memory reasons but we do want to wrap it in it's own
     * transaction. We will therefore wrap the transaction and session into a wrapper iterator that will commit
     * the transaction and close the session when the iterator is closed. 
     * We need to support a large number of certificates (10th of thousands) and we therefore have to work with
     * StatelessSessions and iterators.
     * 
     * Note: Because the statelessSession uses it's own session/transaction it can happen that changes made in other
     * sessions/transactions are not seen until the transactions have been committed.
     */
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
            Integer firstResult, Integer maxResults) 
    throws CertStoreException
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        Transaction tx = statelessSession.beginTransaction();
        
        try {
            CloseableIterator<X509Certificate> iterator = getStatelessDAO(statelessSession).getCertificateIterator(certSelector,
                    missingKeyAlias, firstResult, maxResults);
            
            CloseableIterator<X509Certificate> commitOnCloseIterator = new CommitOnCloseIterator<X509Certificate>(iterator, 
                    SessionAdapterFactory.create(statelessSession), tx);
            
            return commitOnCloseIterator;
        }
        catch(CertStoreException e)
        {
            try {
                tx.rollback();
            }
            finally {
                statelessSession.close();
            }
            
            throw e;
        }
    }

    @Override
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector) 
    throws CertStoreException
    {
        return getCertificateIterator(certSelector, MissingKeyAlias.ALLOWED, null, null);
    }
    
    private String getStoreName()
    {
        if (storeName == null) {
            throw new IllegalStateException("storeName should be set.");
        }

        return storeName;
    }
    
    private X509CertStoreDAO getDAO()
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
        
        return new X509CertStoreDAOHibernate(session, getStoreName());
    }

    private X509CertStoreDAO getStatelessDAO(StatelessSession statelessSession)
    {
        SessionAdapter session = SessionAdapterFactory.create(statelessSession);
        
        return new X509CertStoreDAOHibernate(session, getStoreName());
    }
        
    @Override
    public void setStoreEventListener(X509StoreEventListener eventListener) {
    	this.eventListener.set(eventListener);
    }
    
    private void fireChangeEvent() 
    {
    	if (eventListener.get() != null) {
    		eventListener.get().onChange();
    	}
    }
}
