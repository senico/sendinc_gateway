/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.hibernate;

import java.io.IOException;
import java.io.Serializable;
import java.security.cert.CertPath;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mitm.common.mail.EmailAddressUtils;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.X509CertStoreEntry;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

/**
 * Entity class for storing a certificate with a possible CertPath and key alias into the database.
 * 
 * @author Martijn Brinkers
 *
 */
@Entity(name = X509CertStoreEntryHibernate.ENTITY_NAME)
@Table(name = "certificates",
        uniqueConstraints = {@UniqueConstraint(columnNames={"storeName", "thumbprint"})}
)
@org.hibernate.annotations.Table(appliesTo = X509CertStoreEntryHibernate.ENTITY_NAME, indexes = {
        @Index(name = "certificates_store_name_index",     columnNames = {"storeName"}),
        @Index(name = "certificates_issuer_index",         columnNames = {"issuer"}),
        @Index(name = "certificates_serial_index",         columnNames = {"serial"}),
        @Index(name = "certificates_subject_key_id_index", columnNames = {"subjectKeyIdentifier"}),
        @Index(name = "certificates_subject_index",        columnNames = {"subject"}),
        @Index(name = "certificates_key_alias_index",      columnNames = {"keyAlias"}),
        @Index(name = "certificates_thumbprint_index",     columnNames = {"thumbprint"}),
        @Index(name = "certificates_creationdate_index",   columnNames = {"creationdate"})
        }
)
public class X509CertStoreEntryHibernate implements X509CertStoreEntry, Serializable
{
    public static final String ENTITY_NAME = "Certificates";
    
    private static final long serialVersionUID = -4412179516778140205L;

    /**
     * The maximum length of a serial number. 
     * Should be at least 20 octets (see http://www.ietf.org/rfc/rfc3280.txt appendix B)
     */
    private static final int MAX_SERIAL_LENGTH = 1024;
    
    /**
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255; 
        
    /**
     * Maximum length of the key alias.
     */
    private static final int MAX_KEY_ALIAS_LENGTH = 1024; 
    
    /**
     * Maximum length of the thumbprint
     */
    private static final int MAX_THUMBPRINT_LENGTH = 255; 
    
    /**
     * Maximum length of the cert path type
     */
    private static final int MAX_CERT_PATH_TYPE_LENGTH = 255; 

    /**
     * Maximum length of the store name
     */
    private static final int MAX_STORE_NAME_LENGTH = 255; 
    
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The store name will be used to store multiple certificate stores in the database
     */
    @Column (name = "storeName", unique = false, nullable = true, length = MAX_STORE_NAME_LENGTH)
    private String storeName;
    
    @Type(type = "mitm.common.hibernate.X509CertificateUserType")
    @Columns(columns = {
        @Column(name = "notBefore"),
        @Column(name = "notAfter"),
        @Column(name = "issuer"),
        @Column(name = "issuerFriendly"),
        @Column(name = "serial", length = MAX_SERIAL_LENGTH),
        @Column(name = "subjectKeyIdentifier"),
        @Column(name = "subject"),
        @Column(name = "subjectFriendly"),
        @Column(name = "certificate"),
        @Column(name = "thumbprint", length = MAX_THUMBPRINT_LENGTH)
    })
    private X509Certificate certificate;

    /*
     * The certificate chain.
     */
    @Type(type = "mitm.common.hibernate.CertPathUserType")
    @Columns(columns = {
        @Column(name = "certPath", nullable = true),
        @Column(name = "certPathType", length = MAX_CERT_PATH_TYPE_LENGTH, unique = false, nullable = true)
    })
    private CertPath certificatePath;

    /*
     * The date the chain was updated
     */
    @Type(type = "timestamp")
    @Column (name = "datePathUpdated", unique = false, nullable = true)
    private Date datePathUpdated;
    
    /*
     * Date this entry was created
     */
    @Type(type = "timestamp")
    @Column (name = "creationDate", unique = false, nullable = true)
    private Date creationDate;
    
    /*
     * A collection of email addresses from the certificate.
     */
    @CollectionOfElements
    @JoinTable(name = "certificates_email",
            joinColumns = @JoinColumn(name = "certificates_id"))
    @Column(name = "email", length = MAX_EMAIL_LENGTH)
    private Set<String> email = new HashSet<String>();
    
    /*
     * If this certificate has an associated private key this will be the alias
     */
    @Column (name = "keyAlias", unique = false, nullable = true, length = MAX_KEY_ALIAS_LENGTH)
    private String keyAlias;

    public X509CertStoreEntryHibernate(X509Certificate certificate, String keyAlias, String storeName, Date creationDate) 
    throws CertificateParsingException, IOException 
    {
        this.certificate = certificate;
        this.keyAlias = keyAlias;
        this.storeName = storeName;
        this.creationDate = creationDate;
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        List<String> unfilteredEmail = inspector.getEmail();
        
        /*
         * filter and normalize the email addresses to make sure that we only add 
         * valid email addresses
         */
        email.addAll(EmailAddressUtils.canonicalizeAndValidate(unfilteredEmail, true));
    }

    protected X509CertStoreEntryHibernate() {
        /* Hibernate requires a default constructor */
    }
    
    public Long getID() {
        return id; 
    }
    
    public String getStoreName() {
        return storeName;
    }
    
    @Override
    public X509Certificate getCertificate() {
        return certificate;
    }

    @Override
    public void setCertificatePath(CertPath certificatePath) {
        this.certificatePath = certificatePath;
    }
    
    @Override
    public CertPath getCertificatePath() {
        return certificatePath;
    }

    @Override
    public void setDatePathUpdated(Date date) {
        this.datePathUpdated = date;
    }
    
    @Override
    public Date getDatePathUpdated() {
        return datePathUpdated;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }
    
    @Override
    public Set<String> getEmail() {
        return email;
    }
    
    @Override
    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }
    
    @Override
    public String getKeyAlias() {
        return keyAlias;
    }

    @Override
    public String toString() {
        return certificate.toString();
    }
    
    /**
     * X509CertStoreEntry is equal if and only if the certificates and storeNames are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof X509CertStoreEntryHibernate)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        X509CertStoreEntryHibernate rhs = (X509CertStoreEntryHibernate) obj;
        
        return new EqualsBuilder()
            .append(certificate, rhs.certificate)
            .append(storeName, rhs.storeName)
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(certificate)
            .append(storeName)
            .toHashCode();    
    }    
}
