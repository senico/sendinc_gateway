/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.jce;

import java.security.cert.CRL;
import java.security.cert.CRLSelector;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.util.Collection;

import mitm.common.security.certstore.BasicCertStore;
import mitm.common.security.crlstore.BasicCRLStore;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorAdapter;

/**
 * Adapter which provides a BasicCertStore and BasicCRLStore interface for a JCE CertStore. 
 * 
 * @author Martijn Brinkers
 *
 */
public class CertStoreAdapter implements BasicCertStore, BasicCRLStore
{
    private final CertStore certStore;
    
    public CertStoreAdapter(CertStore certStore)
    {
        this.certStore = certStore;
    }
    
    @Override
    public Collection<? extends Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        return certStore.getCertificates(certSelector);
    }
    
    @Override
    public CloseableIterator<? extends Certificate> getCertificateIterator(CertSelector certSelector) 
    throws CertStoreException
    {
        Collection<? extends Certificate> certificates = getCertificates(certSelector);
        
        return new CloseableIteratorAdapter<Certificate>(certificates.iterator());
    }

    @Override
    public Collection<? extends CRL> getCRLs(CRLSelector crlSelector)
    throws CRLStoreException
    {
        try {
            return certStore.getCRLs(crlSelector);
        }
        catch (CertStoreException e) {
            throw new CRLStoreException(e);
        }
    }
    
    @Override
    public CloseableIterator<? extends CRL> getCRLIterator(CRLSelector crlSelector)
    throws CRLStoreException
    {
        Collection<? extends CRL> crls = getCRLs(crlSelector);
        
        return new CloseableIteratorAdapter<CRL>(crls.iterator());
    }
}
