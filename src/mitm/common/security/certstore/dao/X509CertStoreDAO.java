/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;

import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.util.CloseableIterator;

public interface X509CertStoreDAO 
{
    /**
     * Searches for entries with matching email address (bounded)
     * @return List of entries or null of no match was found
     */
    public CloseableIterator<X509CertStoreEntryHibernate> getByEmail(String email, Match match, Expired expired, 
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults);

    /**
     * Returns the number of rows that getByEmail would return 
     */
    public long getByEmailCount(String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias, Date date);
    
    /**
     * Searches the subject friendly name using ILIKE.
     */
    public CloseableIterator<X509CertStoreEntryHibernate> searchBySubject(String subject, Expired expired, 
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults);

    /**
     * Returns the number of rows that searchBySubject would return when maxResults is MAXINT.
     */
    public int getSearchBySubjectCount(String subject, Expired expired, MissingKeyAlias missingKeyAlias, Date date);
    
    /**
     * Searches the issuer friendly name using ILIKE.
     */
    public CloseableIterator<X509CertStoreEntryHibernate> searchByIssuer(String issuer, Expired expired, 
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults);

    /**
     * Returns the number of rows that searchByIssuer would return when maxResults is MAXINT.
     */
    public int getSearchByIssuerCount(String issuer, Expired expired, MissingKeyAlias missingKeyAlias, Date date);
    
    /**
     * Searches for an entry with the given thumbprint (SHA-512)
     * @param alias
     * @return List of entries or null of no match was found
     */
    public X509CertStoreEntryHibernate getByThumbprint(String thumbprint);
        
    /**
     * Searches for an entry with the given Certificate (using the thumbprint as the search key)
     * @param certificate
     * @return Entry or null of no match was found
     */
    public X509CertStoreEntryHibernate getByCertificate(X509Certificate certificate)
    throws CertStoreException;
        
    /**
     * Returns the entry that was added last (ie the newest entry)
     */
    public X509CertStoreEntryHibernate getLatest();

    /**
     * Returns the number of total entries in this store  
     */
    public int getRowCount();

    /**
     * Returns the number of total entries in this store  
     */
    public int getRowCount(Expired expired, MissingKeyAlias missingKeyAlias, Date date);
    
    /**
     * Adds the certificate to the database with the associated alias
     * @param certificate
     * @param keyAlias associated key alias (null is allowed)
     * @return the newly created entry
     * @throws CertificateParsingException
     * @throws IOException
     */
    public X509CertStoreEntryHibernate addCertificate(X509Certificate certificate, String keyAlias) 
    throws CertStoreException;
    
    /**
     * Removes the entry identified by the given certificate if available. 
     * @param certificate
     * @throws IOException 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     * @throws CertificateParsingException 
     * @throws CertificateEncodingException 
     */
    public void removeCertificate(X509Certificate certificate) 
    throws CertStoreException;
    
    /**
     * Remove all X509CertStoreEntryHibernate entries.
     * @throws CertStoreException 
     */
    public void removeAllEntries() 
    throws CertStoreException;
    
    /**
     * Returns a bounded collection of certificates matching the selector.
     * @param certSelector
     * @return
     * @throws CertStoreException
     */
    public Collection<X509Certificate> getCertificates(CertSelector certSelector, MissingKeyAlias missingKeyAlias, 
            Integer firstResult, Integer maxResults)
    throws CertStoreException;
    
    /**
     * Returns an iterator over the certificates for matching certificates
     * @param certSelector
     * @return
     * @throws CertStoreException
     */
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector, MissingKeyAlias missingKeyAlias, 
            Integer firstResult, Integer maxResults)
    throws CertStoreException;
        
    /**
     * Returns an iterator over all the entries
     * @return
     * @throws CertStoreException
     */
    public CloseableIterator<X509CertStoreEntryHibernate> getCertStoreIterator(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
        Integer firstResult, Integer maxResults) 
    throws CertStoreException;
}
