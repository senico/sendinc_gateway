/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.spec.SecretKeySpec;

import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.util.Check;

public class KeyEncoder
{
    /*
     * The current version of the binary encoded key blob
     */
    private final static int CURRENT_VERSION = 1;
    
    /*
     * The type of the key
     */
    private enum KeyType {PRIVATE, PUBLIC, SECRET};
    
    /*
     * The encoding of the key
     */
    private enum Format {PKCS8, X509, RAW};

    /**
     * Specialized version of KeyEncoderException so we can detect this specific exception.
     */
    public static class KeyWasEncryptedKeyEncoderException extends KeyEncoderException
    {
        private static final long serialVersionUID = -7315057571811920035L;

        public KeyWasEncryptedKeyEncoderException(String message) {
            super(message);
        }
    }
    
    private static Format toFormat(String format) 
    throws KeyEncoderException
    {
        if (format.equals("PKCS#8") || format.equals("PKCS8")) {
            return Format.PKCS8;
        }
        
        if (format.equals("X.509") || format.equals("X509")) {
            return Format.X509;
        }

        if (format.equals("RAW")) {
            return Format.RAW;
        }
        
        throw new KeyEncoderException("Unknown key format " + format);
    }
    
    private static SecurityFactory getSecurityFactory() {
        return SecurityFactoryFactory.getSecurityFactory();
    }
    
    private static void assertIsClass(Object o, Class<?> clazz) 
    throws KeyEncoderException
    {
        if (o == null) {
            throw new KeyEncoderException("Value is null.");
        }
        
        if (!o.getClass().equals(clazz)) {
            throw new KeyEncoderException(clazz.getName() + " expected but got a " + o.getClass().getName());
        }
    }

    /**
     * Converts the key to a byte array. The key will not be encrypted
     */
    public static byte[] encode(Key key)
    throws KeyEncoderException
    {
        return encode(key, null);
    }
    
    /**
     * Converts the key to a byte array. The key will be encrypted with the encryptor. 
     * 
     * WARNING: If encryptor is null the key won't be encrypted.
     */
    public static byte[] encode(Key key, Encryptor encryptor)
    throws KeyEncoderException
    {
        Check.notNull(key, "key");

        boolean encrypt = encryptor != null;
        
        try {
            KeyType keyType;
            
            if (key instanceof PrivateKey) {
                keyType = KeyType.PRIVATE;
            }
            else { 
                if (key instanceof PublicKey) {
                    keyType = KeyType.PUBLIC;
                } 
                else {
                    /*
                     * Assume the key is a secret key.
                     * 
                     *  Note: this is not always correct. A key can in principle be some application specific type.
                     */
                    keyType = KeyType.SECRET;
                }
            }        
            
            byte[] encodedKey = encrypt ? encryptor.encrypt(key.getEncoded()) : key.getEncoded();  
            
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream output = new ObjectOutputStream(bos);
            
            output.writeInt(CURRENT_VERSION);
            output.writeObject(key.getAlgorithm());
            output.writeObject(key.getFormat());
            output.writeObject(keyType);
            output.writeBoolean(encrypt);
            output.writeObject(encodedKey);
            output.close();
            
            return bos.toByteArray();
        }
        catch(IOException e) {
            throw new KeyEncoderException(e);
        }
        catch (EncryptorException e) {
            throw new KeyEncoderException(e);
        }
    }

    /**
     * Converts the byte array to a Key. A KeyEncoderException exception will be thrown if the key was encrypted.
     */
    public static Key decode(byte[] encoded)
    throws KeyEncoderException
    {
        return decode(encoded, null);
    }
    
    /**
     * Converts the byte array to a Key. If the stored key was encrypted it will be decrypted with the provided 
     * password and encryptor. A KeyEncoderException exception will be thrown if the key was encrypted but the 
     * password or encryptor was null. 
     */
    public static Key decode(byte[] encoded, Encryptor encryptor)
    throws KeyEncoderException
    {
        Check.notNull(encoded, "encoded");
        
        try {
            ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(encoded));
            
            int version = input.readInt();
            
            if (version != CURRENT_VERSION)
            {
                throw new KeyEncoderException("Unexpected version. Expected " + CURRENT_VERSION + 
                        " but got " + version);
            }
            
            Object o = input.readObject();
            assertIsClass(o, String.class);
            
            String algorithm = (String) o;
            
            o = input.readObject();
            assertIsClass(o, String.class);

            String format = (String) o;
            
            o = input.readObject();
            assertIsClass(o, KeyType.class);
            
            KeyType keyType = (KeyType) o;

            boolean encrypted = input.readBoolean();
            
            o = input.readObject();
            assertIsClass(o, byte[].class);
            
            byte[] encodedKey = (byte[])o;;
            
            if (encrypted)
            {
                if (encryptor == null) {
                    throw new KeyWasEncryptedKeyEncoderException("The key was encrypted but encryptor was null.");
                }
                
                encodedKey = encryptor.decrypt(encodedKey);
            }
            
            KeySpec keySpec;

            Format keyFormat = toFormat(format);
            
            switch (keyFormat) {
            case PKCS8:
                keySpec = new PKCS8EncodedKeySpec(encodedKey);
                break;
            case X509:
                keySpec = new X509EncodedKeySpec(encodedKey);
                break;
            case RAW:
                return new SecretKeySpec(encodedKey, algorithm);

            default:
                throw new KeyEncoderException("Unknown key format " + keyFormat);
            }
            
            SecurityFactory securityFactory = getSecurityFactory();
            
            switch (keyType) {
            case PRIVATE:
                return securityFactory.createKeyFactory(algorithm).generatePrivate(keySpec);
            case PUBLIC:
                return securityFactory.createKeyFactory(algorithm).generatePublic(keySpec);
            case SECRET:
                return securityFactory.createSecretKeyFactory(algorithm).generateSecret(keySpec);

            default:
                throw new KeyEncoderException("Unknown key type " + keyType);
            }
        }
        catch (IOException e) {
            throw new KeyEncoderException(e);
        }
        catch (ClassNotFoundException e) {
            throw new KeyEncoderException(e);
        }
        catch (InvalidKeySpecException e) {
            throw new KeyEncoderException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyEncoderException(e);
        }
        catch (NoSuchProviderException e) {
            throw new KeyEncoderException(e);
        }
        catch (EncryptorException e) {
            throw new KeyEncoderException(e);
        }
    }
}
