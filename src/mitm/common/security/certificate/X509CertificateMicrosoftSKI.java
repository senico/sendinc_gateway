/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;

import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.X509Extension;

/**
 * Extension of X509Certificate which is used to work around a non standard way to calculate the
 * subject key identier (SKI) used by Microsoft products when the SKI is not part of the 
 * certificate.
 * 
 * For info why this workaround is needed see: https://bugzilla.mozilla.org/show_bug.cgi?id=559243 and
 * http://www.ietf.org/mail-archive/web/smime/current/msg18730.html
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CertificateMicrosoftSKI extends X509Certificate
{
    /*
     * The certificate to delegate all calls to
     */
    private final X509Certificate delegate;
    
    public X509CertificateMicrosoftSKI(X509Certificate delegate)
    {
        Check.notNull(delegate, "delegate");
        
        this.delegate = delegate;
    }
    
    @Override
    public void checkValidity()
    throws CertificateExpiredException, CertificateNotYetValidException
    {
        delegate.checkValidity();
    }

    @Override
    public void checkValidity(Date date)
    throws CertificateExpiredException, CertificateNotYetValidException
    {
        delegate.checkValidity(date);
    }

    @Override
    public int getBasicConstraints() {
        return delegate.getBasicConstraints();
    }

    @Override
    public Principal getIssuerDN() {
        return delegate.getIssuerDN();
    }

    @Override
    public boolean[] getIssuerUniqueID() {
        return delegate.getIssuerUniqueID();
    }

    @Override
    public boolean[] getKeyUsage() {
        return delegate.getKeyUsage();
    }

    @Override
    public Date getNotAfter() {
        return delegate.getNotAfter();
    }

    @Override
    public Date getNotBefore() {
        return delegate.getNotBefore();
    }

    @Override
    public BigInteger getSerialNumber() {
        return delegate.getSerialNumber();
    }

    @Override
    public String getSigAlgName() {
        return delegate.getSigAlgName();
    }

    @Override
    public String getSigAlgOID() {
        return delegate.getSigAlgOID();
    }

    @Override
    public byte[] getSigAlgParams() {
        return delegate.getSigAlgParams();
    }

    @Override
    public byte[] getSignature() {
        return delegate.getSignature();
    }

    @Override
    public Principal getSubjectDN() {
        return delegate.getSubjectDN();
    }

    @Override
    public boolean[] getSubjectUniqueID() {
        return delegate.getSubjectUniqueID();
    }

    @Override
    public byte[] getTBSCertificate()
    throws CertificateEncodingException
    {
        return delegate.getTBSCertificate();
    }

    @Override
    public int getVersion() {
        return delegate.getVersion();
    }

    @Override
    public byte[] getEncoded()
    throws CertificateEncodingException
    {
        return delegate.getEncoded();
    }

    @Override
    public PublicKey getPublicKey() {
        return delegate.getPublicKey();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    @Override
    public void verify(PublicKey publickey)
    throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, 
    SignatureException
    {
        delegate.verify(publickey);
    }

    @Override
    public void verify(PublicKey publickey, String sigProvider)
    throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, 
    SignatureException
    {
        delegate.verify(publickey, sigProvider);
    }

    @Override
    public Set<String> getCriticalExtensionOIDs() {
        return delegate.getCriticalExtensionOIDs();
    }

    @Override
    public byte[] getExtensionValue(String oid)
    {
        byte[] ski;
        
        ski = delegate.getExtensionValue(oid);
        
        if (ski == null && StringUtils.equals(X509Extension.subjectKeyIdentifier.getId(), oid))
        {
            /*
             * The subject key indentifier (SKI) is not part of the certificate. We need to calculate 
             * the SKI in Microsoft's non-standard way and return it. 
             */
            try {
                ski = X509CertificateInspector.calculateSubjectKeyIdentifierMicrosoft(delegate);
                
                /*
                 * X509Certificate wraps the extension in a DER object
                 */
               ski = new DEROctetString(new DEROctetString(ski)).getEncoded();
            }
            catch (IOException e) {
                /*
                 * getExtensionValue cannot throw exceptions. We will therefore silently
                 * discard the exception and return null.
                 */
                ski = null;
            }
        }
        
        return ski;
    }

    @Override
    public Set<String> getNonCriticalExtensionOIDs() {
        return delegate.getNonCriticalExtensionOIDs();
    }

    @Override
    public boolean hasUnsupportedCriticalExtension() {
        return delegate.hasUnsupportedCriticalExtension();
    }
}
