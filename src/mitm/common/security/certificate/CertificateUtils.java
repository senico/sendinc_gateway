/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CertSelector;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.util.CollectionUtils;

import org.slf4j.Logger;

public class CertificateUtils
{
    private final static Logger logger = org.slf4j.LoggerFactory.getLogger(CertificateUtils.class);
    /**
     * Loads certificates from the specified input
     * @param input
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static Collection<? extends Certificate> readCertificates(InputStream input) 
    throws CertificateException, NoSuchProviderException
    {
        CertificateFactory fac = SecurityFactoryFactory.getSecurityFactory().
                createCertificateFactory("X.509");
        
        return fac.generateCertificates(input);
    }

    /**
     * Loads certificates from the specified input. Only X509certificates are returned.
     * @param input
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static Collection<X509Certificate> readX509Certificates(InputStream input) 
    throws CertificateException, NoSuchProviderException
    {
        Collection<? extends Certificate> certificates = readCertificates(input);
        
        Collection<X509Certificate> x509Certificates = new LinkedList<X509Certificate>();
        
        CollectionUtils.copyCollectionFiltered(certificates, x509Certificates, X509Certificate.class);
        
        return x509Certificates;        
    }
    
    /**
     * Loads the certificate from the specified file
     * @param file
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     * @throws FileNotFoundException
     */
    public static Collection<? extends Certificate> readCertificates(File file) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        FileInputStream input = new FileInputStream(file);
        BufferedInputStream buffered = new BufferedInputStream(input);
        
        try {
            return readCertificates(buffered);
        } 
        finally {
            try {
                buffered.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
            try {
                input.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
        }
    }

    /**
     * Loads the certificate from the specified file. Only X509Certificates are returned.
     * @param file
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     * @throws FileNotFoundException
     */
    public static Collection<X509Certificate> readX509Certificates(File file) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        Collection<? extends Certificate> certificates = readCertificates(file);
        
        Collection<X509Certificate> x509Certificates = new LinkedList<X509Certificate>();
        
        CollectionUtils.copyCollectionFiltered(certificates, x509Certificates, X509Certificate.class);
        
        return x509Certificates;
    }

    
    /**
     * Writes the certificate to the output stream
     * @param certificate
     * @param output
     * @throws CertificateEncodingException
     * @throws IOException
     */
    public static void writeCertificate(Certificate certificate, OutputStream output) 
    throws CertificateEncodingException, IOException
    {
        output.write(certificate.getEncoded());
    }

    /**
     * Writes the certificate to the specified file
     * @param certificate
     * @param file
     * @throws CertificateEncodingException
     * @throws IOException
     */
    public static void writeCertificate(Certificate certificate, File file) 
    throws CertificateEncodingException, IOException
    {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream buffered = new BufferedOutputStream(outputStream);

        try {
            writeCertificate(certificate, buffered);
        } 
        finally {
            try {
                buffered.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
            try {
                outputStream.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
        }
    }
    
    /**
     * Writes the certificates to a stream in the provided format
     * @param certificates
     * @param output
     * @param encoding
     * @throws IOException 
     * @throws CertificateEncodingException 
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(Collection<? extends Certificate> certificates, OutputStream output, 
            ObjectEncoding encoding) 
    throws CertificateEncodingException, IOException 
    {
        byte[] encoded = CertificateEncoder.encode(certificates, encoding);
        
        output.write(encoded);
    }

    /**
     * Writes the certificates to a stream in PKCS#7 format
     * @param certificates
     * @param output
     * @throws IOException 
     * @throws CertificateEncodingException 
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(Collection<? extends Certificate> certificates, OutputStream output) 
    throws CertificateEncodingException, IOException 
    {
        writeCertificates(certificates, output, ObjectEncoding.DER);
    }
    
    /**
     * Writes the certificates to the specified file in the provided format
     * @param certificates
     * @param file
     * @param encoding
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(Collection<? extends Certificate> certificates, File file, ObjectEncoding encoding) 
    throws IOException, CertificateException, NoSuchProviderException
    {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream buffered = new BufferedOutputStream(outputStream);
        
        try {
            writeCertificates(certificates, buffered, encoding);
        } 
        finally {
            try {
                buffered.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
            try {
                outputStream.close();
            } 
            catch(IOException e) {
                /* log and ignore */
                logger.error("IOException.", e);
            }
        }
    }
    
    /**
     * Writes the certificates to the specified file in PKCS#7 format
     * @param certificates
     * @param file
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(Collection<? extends Certificate> certificates, File file) 
    throws IOException, CertificateException, NoSuchProviderException
    {
        writeCertificates(certificates, file, ObjectEncoding.DER);
    }
    
    
    /**
     * Returns a list of certificates matching the selector
     * @param <T>
     * @param certificates
     * @param selector
     * @return
     */
    public static <T extends Certificate> List<T> getMatchingCertificates(Collection<T> certificates, CertSelector selector) 
    {
        List<T> selected = new LinkedList<T>();
        
        if (certificates != null && selector != null)
        {
            Iterator<T> certificateIt = certificates.iterator();
            
            while (certificateIt.hasNext())
            {
                T certificate = certificateIt.next();

                if (selector.match(certificate)) {
                    selected.add(certificate);
                }
            }
        }
        
        return selected;
    }

    /**
     * Returns an array of all the X509Certificates from the input. Never null.
     * 
     * @param certificates
     * @return
     */
    public static X509Certificate[] getX509Certificates(Certificate... certificates)
    {
        X509Certificate[] result;
        
        if (certificates != null)
        {
            List<X509Certificate> x509Certs = new ArrayList<X509Certificate>(certificates.length);
            
            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate) {
                    x509Certs.add((X509Certificate) certificate);
                }
            }
            
            result = new X509Certificate[x509Certs.size()];
            
            result = x509Certs.toArray(result);
        }
        else {
            result = new X509Certificate[]{};
        }
        
        return result;
    }
    
    /**
     * Returns a List of all the X509Certificates from the input. Never null.
     * 
     * @param certificates
     * @return
     */
    public static List<X509Certificate> getX509Certificates(Collection<? extends Certificate> certificates)
    {
        List<X509Certificate> result; 
        
        if (certificates != null)
        {
            result = new ArrayList<X509Certificate>(certificates.size());
            
            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate) {
                    result.add((X509Certificate) certificate);
                }
            }
        }
        else {
            result = new ArrayList<X509Certificate>();
        }
        
        return result;
    }
}
