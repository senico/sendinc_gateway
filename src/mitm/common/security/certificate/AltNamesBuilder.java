/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.util.List;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;

/**
 * Helper to build a GeneralNames instance containing the specified AltNames.
 * 
 * TODO: Add support for other AltNames
 * 
 * @author Martijn Brinkers
 *
 */
public class AltNamesBuilder
{
    private String[] rfc822Names;
    private String[] dnsNames;
    
    public void setRFC822Names(List<String> names) {
        setRFC822Names(names.toArray(this.rfc822Names));
    }

    public void setRFC822Names(String... names) {
        this.rfc822Names = names;
    }
    
    public void setDNSNames(List<String> names) {
        setDNSNames(names.toArray(this.dnsNames));
    }

    public void setDNSNames(String... names) {
        this.dnsNames = names;
    }
    
    private void addGeneralNames(String[] names, AltNameType altNameType, ASN1EncodableVector altNames) 
    {
        if (names == null) {
            return;
        }
        
        /* add all the strings */ 
        for (String name : names) 
        {
            /*
             * GeneralName encodes the string to the correct DER encoding based
             * on the tag value
             */
            GeneralName generalName = new GeneralName(altNameType.getTag(), name);
            
            altNames.add(generalName);
        }
    }
    
    /**
     * Builds a GeneralNames instance with the provided altNames 
     * @return
     */
    public GeneralNames buildAltNames() 
    {
        ASN1EncodableVector listOfNames = new ASN1EncodableVector();
        
        addGeneralNames(rfc822Names, AltNameType.RFC822NAME, listOfNames);
        addGeneralNames(dnsNames, AltNameType.DNSNAME, listOfNames);
        
        DERSequence derEncodedNames = new DERSequence(listOfNames);
        
        GeneralNames altNames = GeneralNames.getInstance(derEncodedNames);
        
        return altNames;
    }
}
