/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x509.GeneralNames;

public interface X509CertificateBuilder
{
    public void setVersion(CertificateVersion version);
    public CertificateVersion getVersion();
    
    public void setSerialNumber(BigInteger serialNumber);
    public BigInteger getSerialNumber();
    
    public void setSubject(X500Principal subject);
    public X500Principal getSubject();
    
    public void setIssuer(X500Principal issuer);
    public X500Principal getIssuer();
    
    public void setNotBefore(Date date);
    public Date getNotBefore();

    public void setNotAfter(Date date);
    public Date getNotAfter();
    
    public void setSignatureAlgorithm(String signatureAlgorithm);
    public String getSignatureAlgorithm();
    
    public void setAltNames(GeneralNames altNames, boolean critical);
    public GeneralNames getAltNames();    
    public boolean isAltNamesCritical();
    
    public void setKeyUsage(Set<KeyUsageType> keyUsage, boolean critical);
    public Set<KeyUsageType> getKeyUsage();
    public boolean isKeyUsageCritical();
    
    public void setExtendedKeyUsage(Set<ExtendedKeyUsageType> keyUsage, boolean critical);
    public Set<ExtendedKeyUsageType> getExtendedKeyUsage();
    public boolean isExtendedKeyUsageCritical();
    
    public void setIsCA(boolean isCA, boolean critical);
    public boolean isCA();
    public boolean isCACritical();

    public void addSubjectKeyIdentifier(boolean add);
    public boolean isAddSubjectKeyIdentifier();

    public void addAuthorityKeyIdentifier(boolean add);
    public boolean isAddAuthorityKeyIdentifier();
    
    public void setPathLengthConstraint(Integer pathLengthConstraint);
    public Integer getPathLengthConstraint();
    
    public void setPublicKey(PublicKey publicKey);
    public PublicKey getPublicKey();

    public void setCRLDistributionPoints(Collection<String> uris);
    
    /**
     * Generates a certificate with issuerCertificate as issuer. If issuerCertificate is null 
     * Issuer property is used
     */
    public X509Certificate generateCertificate(PrivateKey issuerPrivateKey, X509Certificate issuerCertificate) 
    throws CertificateBuilderException;
}
