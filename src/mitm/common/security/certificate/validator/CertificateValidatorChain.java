/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.validator;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Allows multiple CertificateValidators to be tried one after the other. If one of them fails the ones not tried
 * will not be tried and isValid returns false. If there are no CertificateValidators isValid returns false.
 * 
 * @author Martijn Brinkers
 *
 */
public class CertificateValidatorChain implements CertificateValidator
{
    private List<CertificateValidator> chain = new LinkedList<CertificateValidator>();

    private final String name;
    
    private CertificateValidator lastInvalid;
    
    public CertificateValidatorChain(String name, CertificateValidator... validators) 
    {
        this.name = name;
        
        addValidators(validators);
    }

    public CertificateValidatorChain(CertificateValidator... validators) 
    {
        this(null, validators);
    }
    
    public CertificateValidatorChain(String name) 
    {
        this.name = name;
    }

    public CertificateValidatorChain() 
    {
        this((String) null);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    public void addValidators(CertificateValidator... validators)
    {
        chain.addAll(Arrays.asList(validators));
    }
    
    @Override
    public boolean isValid(Certificate certificate)
    throws CertificateException
    {
        lastInvalid = null;
        
        boolean valid = false;
        
        for (CertificateValidator validator : chain)
        {
            valid = validator.isValid(certificate);
            
            if (!valid) 
            {
                lastInvalid = validator;
                break;
            }
        }
        
        return valid;
    }
    
    /**
     * Returns the CertificateValidator that resulted in isValid false. Null if
     * there is no lastInvalid.
     */
    public CertificateValidator getFailingValidator() {
        return lastInvalid;
    }
    
    @Override
    public String getFailureMessage() 
    {
        String message = "";
        
        if (lastInvalid != null) {
            message = lastInvalid.getFailureMessage();
        }
        
        return message;
    }
}
