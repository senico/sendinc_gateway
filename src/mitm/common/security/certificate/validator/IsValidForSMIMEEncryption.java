/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.validator;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.common.security.certificate.ExtendedKeyUsageType;
import mitm.common.security.certificate.KeyUsageType;
import mitm.common.security.certificate.X509CertificateInspector;

/**
 * This CertificateValidator checks if a certificate can be used for S/MIME encryption.
 * 
 * If a certificate has a key usage the key usage should contain keyEncipherment.
 * If a certificate has an extended key usage the extended key usage should contain anyExtendedKeyUsage
 * and/or id-kp-emailProtection.
 * 
 * This class is not thread safe.
 * 
 * @author Martijn Brinkers
 *
 */
public class IsValidForSMIMEEncryption implements CertificateValidator
{
    private final String name;

    private String failureMessage = "";
    
    public IsValidForSMIMEEncryption(String name) {
        this.name = name;
    }

    public IsValidForSMIMEEncryption() {
        this(null);
    }
    
    @Override
    public String getName() {
        return name;
    }
        
    /**
     * Returns true if the certificate allows KEYENCIPHERMENT (keyUsage extension) and allows 
     * EMAILPROTECTION (extended keyUsage extension).
     */
    @Override
    public boolean isValid(Certificate certificate)
    throws CertificateException
    {
        if (!(certificate instanceof X509Certificate)) 
        {
            failureMessage = "Certificate is not a X509Certificate";
            
            return false;
        }
        
        failureMessage = "";
        
        X509Certificate x509Certificate = (X509Certificate) certificate;
        
        Set<KeyUsageType> keyUsage = X509CertificateInspector.getKeyUsage(x509Certificate);
        
        boolean valid = (keyUsage == null) || (keyUsage.contains(KeyUsageType.KEYENCIPHERMENT));
        
        if (!valid) {
            failureMessage = "Key usage does not allow " + KeyUsageType.KEYENCIPHERMENT;
        }
        else {
            Set<ExtendedKeyUsageType> extendedKeyUsage = X509CertificateInspector.getExtendedKeyUsage(x509Certificate);
            
            valid = (extendedKeyUsage == null || extendedKeyUsage.contains(ExtendedKeyUsageType.ANYKEYUSAGE) || 
                    extendedKeyUsage.contains(ExtendedKeyUsageType.EMAILPROTECTION));
            
            if (!valid) {
                failureMessage = "Extended key usage does not allow " + ExtendedKeyUsageType.EMAILPROTECTION;
            }            
        }
        
        return valid;
    }
    
    @Override
    public String getFailureMessage() {
        return failureMessage;
    }
}
