/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.io.IOException;
import java.util.List;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.RFC4519Style;

/**
 * Builder class for building X500Principal instances.
 * 
 * @author Martijn Brinkers
 *
 */
public class X500PrincipalBuilder
{
    private String[] email;
    private String[] organisation;
    private String[] organisationalUnit;
    private String[] countryCode;
    private String[] state;
    private String[] locality;
    private String[] commonName;
    private String[] givenName;
    private String[] surname;
    
    public void setEmail(List<String> email)
    {
        if (email != null)
        {
            this.email = new String[email.size()];
    
            setEmail(email.toArray(this.email));
        }
    }

    public void setEmail(String... email)
    {
        this.email = email;
    }

    public void setOrganisation(List<String> organisation)
    {
        if (organisation != null)
        {
            this.organisation = new String[organisation.size()];
            
            setOrganisation(organisation.toArray(this.organisation));
        }
    }

    public void setOrganisation(String... organisation)
    {
        this.organisation = organisation;
    }
    
    public void setOrganisationalUnit(List<String> organisationalUnit)
    {
        if (organisationalUnit != null)
        {
            this.organisationalUnit = new String[organisationalUnit.size()];
    
            setOrganisationalUnit(organisationalUnit.toArray(this.organisationalUnit));
        }
    }

    public void setOrganisationalUnit(String... organisationalUnit)
    {
        this.organisationalUnit = organisationalUnit;
    }
    
    public void setCountryCode(List<String> countryCode)
    {
        if (countryCode != null)
        {
            this.countryCode = new String[countryCode.size()];
    
            setCountryCode(countryCode.toArray(this.countryCode));
        }
    }

    public void setCountryCode(String... countryCode)
    {
        this.countryCode = countryCode;
    }

    public void setState(List<String> state)
    {
        if (state != null)
        {
            this.state = new String[state.size()];
    
            setState(state.toArray(this.state));
        }
    }

    public void setState(String... state)
    {
        this.state = state;
    }
    
    public void setLocality(List<String> locality)
    {
        if (locality != null)
        {
            this.locality = new String[locality.size()];
    
            setLocality(locality.toArray(this.locality));
        }
    }

    public void setLocality(String... locality)
    {
        this.locality = locality;
    }

    public void setCommonName(List<String> cn)
    {
        if (cn != null)
        {
            this.commonName = new String[cn.size()];
    
            setCommonName(cn.toArray(this.commonName));
        }
    }

    public void setCommonName(String... cn)
    {
        this.commonName = cn;
    }
    
    public void setGivenName(List<String> givenName)
    {
        if (givenName != null)
        {
            this.givenName = new String[givenName.size()];
    
            setGivenName(givenName.toArray(this.givenName));
        }
    }

    public void setGivenName(String... givenName)
    {
        this.givenName = givenName;
    }
    
    public void setSurname(List<String> surname)
    {
        if (surname != null)
        {
            this.surname = new String[surname.size()];
    
            setSurname(surname.toArray(this.surname));
        }
    }

    public void setSurname(String... surname)
    {
        this.surname = surname;
    }

    private void add(ASN1ObjectIdentifier oid, String[] values, X500NameBuilder nameBuilder)
    {
        if (values != null && values.length > 0)
        {
            for (String value : values) {
                nameBuilder.addRDN(oid, value);
            }
        }
    }
    
    /**
     * Builds the X500Principal with the specified elements
     * 
     * Example DNs:
     * 
     * CN=DOD CLASS 3 EMAIL CA-9, OU=PKI, OU=DoD, O=U.S. Government, C=US
     * CN=Thawte Personal Freemail Issuing CA, O=Thawte Consulting (Pty) Ltd., C=ZA
     * CN=Senter Certification Authority SubCA, OU=Certification Authority, O=Senter, L=Den Haag, ST=Zuid-Holland, 
     *          C=NL, EMAILADDRESS=SenterCA@Senter.nl
     * CN=Intel Corporation Basic Enterprise Issuing CA 1, OU=Information Technology Enterprise Business Computing, 
     *      O=Intel Corporation, L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com
     * 
     */
    public X500Principal buildPrincipal() 
    throws IOException 
    {        
        X500NameBuilder nameBuilder = new X500NameBuilder(RFC4519Style.INSTANCE);

        add(RFC4519Style.c, countryCode, nameBuilder);
        add(RFC4519Style.st, state, nameBuilder);
        add(RFC4519Style.l, locality, nameBuilder);
        add(RFC4519Style.o, organisation, nameBuilder);
        add(RFC4519Style.ou, organisationalUnit, nameBuilder);
        add(RFC4519Style.cn, commonName, nameBuilder);
        add(RFC4519Style.sn, surname, nameBuilder);
        add(RFC4519Style.givenName, givenName, nameBuilder);
        add(PKCSObjectIdentifiers.pkcs_9_at_emailAddress, email, nameBuilder);
        
        return X500PrincipalUtils.fromX500Name(nameBuilder.build());
    }
}
