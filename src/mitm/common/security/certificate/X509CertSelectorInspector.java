/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.io.IOException;
import java.security.cert.X509CertSelector;

import mitm.common.security.asn1.DERUtils;

/**
 * Helper class for inspecting a {@link X509CertSelector}. SUN's implementation of {@link X509CertSelector}
 * uses a DER encoded subjectKeyIdentifier whereas Bouncycastle works with the actual bytes of the
 * subjectKeyIdentifier. This helper class converts the byte arrays to the format understood by X509CertSelector.
 * Other convenience methods can later be added as well.
 * 
 * @author Martijn Brinkers
 *
 */
public class X509CertSelectorInspector
{
    private final X509CertSelector certSelector;
        
    public X509CertSelectorInspector(X509CertSelector certSelector) {
        this.certSelector = certSelector;
    }

    public byte[] getSubjectKeyIdentifier() 
    throws IOException
    {
        return getSubjectKeyIdentifier(certSelector);
    }
    
    public static byte[] getSubjectKeyIdentifier(X509CertSelector certSelector) 
    throws IOException
    {
        /*
         * X509CertSelector expects a DER encoded subjectKeyIdentifier. We therefore need to DER 
         * decode the subjectKeyIdentifier
         */
        byte[] subjectKeyIdentifier = null; 
            
        if (certSelector.getSubjectKeyIdentifier() != null) {
            subjectKeyIdentifier = DERUtils.getOctets(certSelector.getSubjectKeyIdentifier());
        }
        
        return subjectKeyIdentifier;
    }    
}
