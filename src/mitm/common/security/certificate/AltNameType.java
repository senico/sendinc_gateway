/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

public enum AltNameType 
{
    /**
     * See http://www.ietf.org/rfc/rfc3280.txt
     * 
     *  GeneralNames :: = SEQUENCE SIZE (1..MAX) OF GeneralName
     *
     *  GeneralName ::= CHOICE {
     *    otherName                       [0]     OtherName,
     *    rfc822Name                      [1]     IA5String,
     *    dNSName                         [2]     IA5String,
     *    x400Address                     [3]     ORAddress,
     *    directoryName                   [4]     Name,
     *    ediPartyName                    [5]     EDIPartyName,
     *    uniformResourceIdentifier       [6]     IA5String,
     *    iPAddress                       [7]     OCTET STRING,
     *    registeredID                    [8]     OBJECT IDENTIFIER}
    */

    OTHERNAME                 (0, "otherName"),
    RFC822NAME                (1, "rfc822Name"),
    DNSNAME                   (2, "dnsName"),
    X400ADDRESS               (3, "x400Address"),
    DIRECTORYNAME             (4, "directoryName"),
    EDIPARTYNAME              (5, "ediPartyName"),
    UNIFORMRESOURCEIDENTIFIER (6, "uniformResourceIdentifier"),
    IPADDRESS                 (7, "iPAddress"),
    REGISTEREDID              (8, "registeredID");
    
    private final int tag;
    private final String friendlyName;
    
    private AltNameType(int tag, String friendlyName)
    {
        this.tag = tag;
        this.friendlyName = friendlyName;
    }
    
    public int getTag() {
        return tag;
    }
    
    @Override
    public String toString() {
        return friendlyName;
    }
}
