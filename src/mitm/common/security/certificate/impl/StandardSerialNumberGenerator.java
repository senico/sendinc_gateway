/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.impl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.certificate.SerialNumberGenerator;

/**
 * Implementation of {@link SerialNumberGenerator}. This SerialNumberGenerator generates a unique serial number using
 * a random generator and the current time in milliseconds. The generated serial number is always a positive number.
 * 
 * @author Martijn Brinkers
 *
 */
public class StandardSerialNumberGenerator implements SerialNumberGenerator
{
    /* the default number of random bytes to use for generating the serial number */
    private final static int DEFAULT_RANDOM_SIZE = 10;
    
    private final SecureRandom randomSource;
    private final int randomSize;
    
    /**
     * Constructor for StandardSerialNumberGenerator using the specified number of random bytes
     * 
     * @param randomSize
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws SecurityFactoryFactoryException
     */
    public StandardSerialNumberGenerator(int randomSize) 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        this.randomSize = randomSize;
        
        randomSource = SecurityFactoryFactory.getSecurityFactory().createSecureRandom();        
    }

    /**
     * Constructor for StandardSerialNumberGenerator using 10 random bytes
     *
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws SecurityFactoryFactoryException
     */
    public StandardSerialNumberGenerator() 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        this(DEFAULT_RANDOM_SIZE);
    }
    
    /**
     * Generates a serial number using the current time and some
     * randomly generated data.
     * @throws IOException 
     */
    @Override
    public BigInteger generate()
    throws IOException 
    {        
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        
        DataOutputStream dataOutput = new DataOutputStream(byteOutput);

        /* add the current time in milliseconds */
        dataOutput.writeLong(System.currentTimeMillis());
        
        /* generate some random data */
        byte[] randomBytes = new byte[randomSize];
        
        randomSource.nextBytes(randomBytes);
        
        dataOutput.write(randomBytes);        
        
        dataOutput.flush();
        
        /*
         * make sure the serial number is positive (PKIX requires positive serial numbers.
         */
        BigInteger serialNumber = new BigInteger(1, byteOutput.toByteArray());
        
        return serialNumber;
    }
}
