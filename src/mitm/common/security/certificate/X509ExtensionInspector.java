/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.io.IOException;
import java.security.cert.X509Extension;

import mitm.common.security.asn1.ASN1Utils;
import mitm.common.security.asn1.DERUtils;
import mitm.common.security.crl.X509CRLInspector;
import mitm.common.util.HexUtils;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;

/**
 * Inspector for {@link X509Extension}. This class is used as a base class for {@link X509CertificateInspector} and
 * {@link X509CRLInspector}.
 * 
 * @author Martijn Brinkers
 *
 */
public class X509ExtensionInspector
{
    private final X509Extension extension;
    
    public X509ExtensionInspector(X509Extension extension) {
        this.extension = extension;
    }
    
    /**
     * Returns the subjectKeyIdentifier extension if present.
     * @return the hex value of subjectKeyIdentifier. Null if extension is not present
     * @throws IOException 
     */
    public String getSubjectKeyIdentifierHex() 
    throws IOException 
    {
        return getSubjectKeyIdentifierHex(this.extension);
    }

    /**
     * Returns the subjectKeyIdentifier extension if present.
     * 
     * @param extension
     * @return
     * @throws IOException
     */
    public static String getSubjectKeyIdentifierHex(X509Extension extension) 
    throws IOException 
    {
        byte[] id = getSubjectKeyIdentifier(extension);
        
        String hex = HexUtils.hexEncode(id);
        
        return hex;
    }
    
    /**
     * Returns the SubjectKeyIdentifier extension
     * @return
     * @throws IOException
     */
    public byte[] getSubjectKeyIdentifier() 
    throws IOException 
    {
        return getSubjectKeyIdentifier(this.extension);
    }

    /**
     * Returns the SubjectKeyIdentifier extension
     * 
     * @param extension
     * @return
     * @throws IOException
     */
    public static byte[] getSubjectKeyIdentifier(X509Extension extension) 
    throws IOException 
    {
        /*
         * The extension returned by X509Extension is still DER encoded so it must be decoded
         */ 
        return DERUtils.getOctets(DERUtils.getOctets(extension.getExtensionValue(
                org.bouncycastle.asn1.x509.X509Extension.subjectKeyIdentifier.getId())));
    }

    /**
     * Returns the AuthorityKeyIdentifier extension if available.
     * @throws IOException
     */
    public AuthorityKeyIdentifier getAuthorityKeyIdentifier()
    throws IOException
    {
        return getAuthorityKeyIdentifier(extension);
    }

    /**
     * Returns the AuthorityKeyIdentifier extension for the extension if available.
     * 
     * @param extension
     * @return
     * @throws IOException
     */
    public static AuthorityKeyIdentifier getAuthorityKeyIdentifier(X509Extension extension) 
    throws IOException
    {
        AuthorityKeyIdentifier authorityKeyIdentifier = null;
        
        ASN1Object derObject = ASN1Utils.getExtensionValue(extension, org.bouncycastle.asn1.x509.X509Extension.
                authorityKeyIdentifier.getId());
        
        if (derObject != null) {
            authorityKeyIdentifier = AuthorityKeyIdentifier.getInstance(derObject);
        }
    
        return authorityKeyIdentifier;
    }
    
    /**
     * Returns the basic constraints if available. Null if not available 
     */
    public BasicConstraints getBasicConstraints() 
    throws IOException
    {
        return getBasicConstraints(extension);
    }
    
    /**
     * Returns the basic constraints if available. Null if not available 
     */
    public static BasicConstraints getBasicConstraints(X509Extension extension) 
    throws IOException
    {
        BasicConstraints basicConstraints = null;
        
        ASN1Object derBasicConstraints = ASN1Utils.getExtensionValue(extension, org.bouncycastle.asn1.x509.
                X509Extension.basicConstraints.getId());
        
        if (derBasicConstraints != null) {
            basicConstraints = BasicConstraints.getInstance(derBasicConstraints);
        }
        
        return basicConstraints;        
    }
    
    /**
     * Returns the CRLDistPoint if available.
     *  
     * @throws IOException
     */
    public CRLDistPoint getCRLDistibutionPoints() 
    throws IOException
    {
        return getCRLDistibutionPoints(extension);
    }

    /**
     * Returns the CRLDistPoint for the extension if available.
     * 
     * @param certificate
     * @throws IOException
     */
    public static CRLDistPoint getCRLDistibutionPoints(X509Extension extension) 
    throws IOException
    {
        CRLDistPoint crlDistributionPoints = null;
        
        ASN1Object derObject = ASN1Utils.getExtensionValue(extension, org.bouncycastle.asn1.x509.X509Extension.
                cRLDistributionPoints.getId());
        
        if (derObject != null) {
            crlDistributionPoints = CRLDistPoint.getInstance(derObject);
        }
        
        return crlDistributionPoints;
    }
    
    /**
     * Returns the IssuingDistributionPoint extension
     */
    public static IssuingDistributionPoint getIssuingDistributionPoint(X509Extension extension) 
    throws IOException
    {
        IssuingDistributionPoint idp = null;
        
        ASN1Object derIDP = ASN1Utils.getExtensionValue(extension, org.bouncycastle.asn1.x509.X509Extension.
                issuingDistributionPoint.getId());
        
        if (derIDP != null) {
            idp = IssuingDistributionPoint.getInstance(derIDP);
        }
        
        return idp;
    }
    
    /**
     * Returns the IssuingDistributionPoint extension
     */
    public IssuingDistributionPoint getIssuingDistributionPoint() 
    throws IOException
    {
        return getIssuingDistributionPoint(extension);
    }
    
    /**
     * Returns the FreshestCRL distribution point for the CRL if available.
     * 
     * @param crl
     * @throws IOException
     */
    public CRLDistPoint getFreshestCRL() 
    throws IOException
    {
        return getFreshestCRL(extension);
    }
    
    /**
     * Returns the FreshestCRL distribution point for the CRL if available.
     * 
     * @param crl
     * @throws IOException
     */
    public static CRLDistPoint getFreshestCRL(X509Extension extension) 
    throws IOException
    {
        CRLDistPoint distributionPoints = null;
        
        ASN1Object derObject = ASN1Utils.getExtensionValue(extension, org.bouncycastle.asn1.x509.X509Extension.
                freshestCRL.getId());
        
        if (derObject != null) {
            distributionPoints = CRLDistPoint.getInstance(derObject);
        }
        
        return distributionPoints;
    }
}
