/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;

import mitm.common.security.digest.Digest;
import mitm.common.security.digest.Digests;

public class CertificateInspector
{
    private final Certificate certificate;
    
    public CertificateInspector(Certificate certificate) {
        this.certificate = certificate;
    }
    
    /**
     * Calculates the thumbprint of the certificate using the given digest algorithm. 
     * @param digest
     * @return
     * @throws CertificateEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public String getThumbprint(Digest digest) 
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        return getThumbprint(this.certificate, digest);
    }

    /**
     * Calculates the thumbprint of the certificate using the given digest algorithm. 
     * @param certificate
     * @param digest
     * @return
     * @throws CertificateEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static String getThumbprint(Certificate certificate, Digest digest) 
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        return Digests.digestHex(certificate.getEncoded(), digest);
    }
    
    
    /**
     * Calculates the thumbprint of the certificate using SHA-512. 
     * @return Hex representation of the SHA-512 thumbprint
     * @throws CertificateEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public String getThumbprint() 
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        return getThumbprint(this.certificate);
    }    

    /**
     * Calculates the thumbprint of the certificate using SHA-512. 
     * @param certificate
     * @return
     * @throws CertificateEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static String getThumbprint(Certificate certificate) 
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        return getThumbprint(certificate, Digest.SHA512);
    }    
}
