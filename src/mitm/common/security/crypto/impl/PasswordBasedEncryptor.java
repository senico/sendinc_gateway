/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crypto.impl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.security.password.PBEncryption;
import mitm.common.security.password.PBEncryptionImpl;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;

/**
 * Implementation of Encryptor that encrypts and decrypts with a password using AES128 (@See PBEncryption)
 * The 
 * 
 * @author Martijn Brinkers
 *
 */
public class PasswordBasedEncryptor implements Encryptor
{
    private final static String DEFAULT_ALGORITHM = "PBEWITHSHA256AND128BITAES-CBC-BC";
    
    /*
     * larger iteration count improves security because "brute force" password cracking takes
     * longer. Disadvantage, naturally, is that it takes longer to decrypt the key.
     */
    private final static int DEFAULT_ITERATION_COUNT = 256; 

    /*
     * default salt length in bytes (16 = 128 bits/8)
     */
    private final static int DEFAULT_SALT_LENGTH = 16; 
    
    private final String password;
    
    /*
     * The class that does the encryption/decryption.
     */
    private final PBEncryption pbEncryption;

    public PasswordBasedEncryptor(String password)
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException 
    {
        this(password, DEFAULT_ITERATION_COUNT, DEFAULT_SALT_LENGTH, DEFAULT_ALGORITHM);
    }
    
    public PasswordBasedEncryptor(String password, int iterationCount, int saltLength, 
            String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException 
    {
        Check.notNull(password, "password");
        Check.notNull(algorithm, "algorithm");
    	
        this.password = password;
        
        this.pbEncryption = new PBEncryptionImpl(iterationCount, saltLength, algorithm);
    }
    
    @Override
    public byte[] encrypt(byte[] data)
    throws EncryptorException    
    {
    	Check.notNull(data, "data");
        
        try {
            return pbEncryption.encrypt(data, password.toCharArray());
        }
        catch (InvalidKeyException e) {
            throw new EncryptorException(e);
        }
        catch (InvalidKeySpecException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchProviderException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchPaddingException e) {
            throw new EncryptorException(e);
        }
        catch (IllegalBlockSizeException e) {
            throw new EncryptorException(e);
        }
        catch (BadPaddingException e) {
            throw new EncryptorException(e);
        }
        catch (IOException e) {
            throw new EncryptorException(e);
        }
    }
    
    @Override
    public String encryptBase64(byte[] data)
    throws EncryptorException
    {
        byte[] encrypted = encrypt(data);
        
        String base64 = MiscStringUtils.toAsciiString(Base64.encodeBase64(encrypted));
        
        return base64;
    }
    
    @Override
    public byte[] decrypt(byte[] data) 
    throws EncryptorException    
    {
    	Check.notNull(data, "data");
        
        try {
            return pbEncryption.decrypt(data, password.toCharArray());
        }
        catch (InvalidKeyException e) {
            throw new EncryptorException(e);
        }
        catch (InvalidKeySpecException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchProviderException e) {
            throw new EncryptorException(e);
        }
        catch (NoSuchPaddingException e) {
            throw new EncryptorException(e);
        }
        catch (IllegalBlockSizeException e) {
            throw new EncryptorException(e);
        }
        catch (BadPaddingException e) {
            throw new EncryptorException(e);
        }
        catch (IOException e) {
            throw new EncryptorException(e);
        }
    }
    
    @Override
    public byte[] decryptBase64(String base64Data)
    throws EncryptorException
    {
    	Check.notNull(base64Data, "base64Data");
        
        byte[] data = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(base64Data));
        
        return decrypt(data);
    }
}
