/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crypto;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.SerializationException;

/**
 * The PBEncryptedOutputStreamParameters is used to store the relevant info for the Password Based Encryption (PBE) classes.
 * When something is encrypted using PBE, in order to decrypt we need to have access to the salt and iteration 
 * count.
 * 
 * @author Martijn Brinkers
 *
 */
public class PBEncryptedStreamParameters
{
    private static final long serialVersionUID = -8617451983656008567L;

    private String algorithm;
    private byte[] salt;
    private int iterationCount;
    
    protected PBEncryptedStreamParameters(String algorithm, byte[] salt, int iterationCount) 
    {
    	this.algorithm = algorithm;
        this.salt = salt;
        this.iterationCount = iterationCount;
    }

    /**
     * Creates a PBEncryptionParameters from the encoded form.
     */
    protected PBEncryptedStreamParameters(InputStream input) 
    throws IOException 
    {
        fromInputStream(input);
    }
        
    public String getAlgorithm() {
    	return algorithm;
    }
    
    public byte[] getSalt() {
        return salt;
    }
    
    public int getIterationCount() {
        return iterationCount;
    }
    
    /**
     * Returns the encoded form of PBEncryptionParameters.
     */
    public byte[] getEncoded() 
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        DataOutputStream out = new DataOutputStream(bos);
        
        out.writeLong(serialVersionUID);
        
        out.writeUTF(algorithm);
        out.writeInt(salt.length);
        out.write(salt);

        out.writeInt(iterationCount);

        return bos.toByteArray();
    }

    private void fromInputStream(InputStream input) 
    throws IOException 
    {
        DataInputStream dis = new DataInputStream(input);
        
        long version = dis.readLong();

        if (version != serialVersionUID) {
            throw new SerializationException("Version expected '" + serialVersionUID + "' but got '" + version);
        }

        algorithm = dis.readUTF();
        
        int saltSize = dis.readInt();

        this.salt = new byte[saltSize];

        dis.readFully(salt);

        this.iterationCount = dis.readInt();
    }    
}
