package mitm.common.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.ssl.SSLSocket;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CipherSuiteOverrideSSLProtocolSocketFactory extends SSLProtocolSocketFactory
{
    private final static Logger logger = LoggerFactory.getLogger(CipherSuiteOverrideSSLProtocolSocketFactory.class);
    
    /*
     * The acceptable cipherSuites (comma separated)
     */
    private final String[] cipherSuites;

    public CipherSuiteOverrideSSLProtocolSocketFactory(String[] cipherSuites)
    {
        logger.info("Enabled Cipher Suites: [" + StringUtils.join(cipherSuites, ", ") + "]");

        this.cipherSuites = cipherSuites;
    }

    private void logSocketInfo(Socket socket)
    {
        if (logger.isDebugEnabled())
        {
            if (socket instanceof SSLSocket)
            {
                SSLSocket sslSocket = (SSLSocket) socket;
                
                logger.debug("Default enabled Cipher Suites: [" + StringUtils.join(
                        sslSocket.getEnabledCipherSuites(), ", ") + "]");
            }        
            else {
                logger.debug("Socket is-not-a SSLSocket");
            }
        }
    }
    
    private void setEnabledCipherSuites(Socket socket)
    {
        if (socket instanceof SSLSocket && ArrayUtils.getLength(cipherSuites) > 0)
        {
            SSLSocket sslSocket = (SSLSocket) socket;

            sslSocket.setEnabledCipherSuites(cipherSuites);

            if (logger.isDebugEnabled())
            {
                logger.debug("Enabled Cipher Suites set to: [" + StringUtils.join(
                        sslSocket.getEnabledCipherSuites(), ", ") + "]");
            }
        }        
    }
    
    @Override
    public Socket createSocket(Socket existingSocket, String host, int port, boolean autoClose)
    throws IOException, UnknownHostException
    {
        Socket socket = super.createSocket(existingSocket, host, port, autoClose);
        
        logSocketInfo(socket);
        
        setEnabledCipherSuites(socket);
        
        return socket;
    }

    @Override
    public Socket createSocket(String host, int port)
    throws IOException, UnknownHostException
    {
        Socket socket = super.createSocket(host, port);
        
        logSocketInfo(socket);

        setEnabledCipherSuites(socket);
        
        return socket;
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localAddress, int localPort)
    throws IOException, UnknownHostException
    {
        Socket socket = super.createSocket(host, port, localAddress, localPort);
        
        logSocketInfo(socket);

        setEnabledCipherSuites(socket);
        
        return socket;
    }
    
    @Override
    public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, 
            HttpConnectionParams params)
    throws IOException, UnknownHostException, ConnectTimeoutException
    {
        Socket socket = super.createSocket(host, port, localAddress, localPort, params);
        
        logSocketInfo(socket);

        setEnabledCipherSuites(socket);
        
        return socket;
    }
    
    /**
     * Overrides the https protocol handler with CipherSuiteOverrideSSLProtocolSocketFactory
     */
    public static void register(String cipherSuites)
    {
        String[] suites = StringUtils.split(cipherSuites, " ,\n\r\t");
        
        Protocol sslProtocol = new Protocol("https", (ProtocolSocketFactory) 
                new CipherSuiteOverrideSSLProtocolSocketFactory(suites), 443);
        
        Protocol.registerProtocol("https", sslProtocol);
    }
}
