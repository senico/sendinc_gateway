/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.net;

import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.properties.HierarchicalPropertiesUtils;
import mitm.common.util.Check;

public class PropertiesProxySettings implements ProxySettings
{
    /*
     * The property names
     */
    private final static String BASE_PROP = "proxy.";
    private final static String PROXY_USERNAME = BASE_PROP + "username";
    private final static String PROXY_PASSWORD = BASE_PROP + "password";
    private final static String PROXY_DOMAIN = BASE_PROP + "domain";
    private final static String PROXY_HOST = BASE_PROP + "host";
    private final static String PROXY_PORT = BASE_PROP + "port";
    private final static String PROXY_ENABLED = BASE_PROP + "enabled";
    
    private final HierarchicalProperties properties;
    
    public PropertiesProxySettings(HierarchicalProperties properties)
    {
        Check.notNull(properties, "properties");
        
        this.properties = properties;
    }
    
    @Override
    public String getUsername()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PROXY_USERNAME, true /* encrypt */);
    }
    
    @Override
    public void setUsername(String username)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PROXY_USERNAME, username, true /* encrypted */);
    }
    
    @Override
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PROXY_PASSWORD, true /* encrypt */);
    }

    @Override
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PROXY_PASSWORD, password, true /* encrypted */);
    }
    
    @Override
    public String getDomain()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PROXY_DOMAIN, false);
    }

    @Override
    public void setDomain(String domain)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PROXY_DOMAIN, domain, false);
    }
    
    @Override
    public String getHost()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(PROXY_HOST, false);
    }

    @Override
    public void setHost(String host)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(PROXY_HOST, host, false);
    }

    @Override
    public int getPort()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, PROXY_PORT, 8080, false);
    }
    
    @Override
    public void setPort(int port)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, PROXY_PORT, port, false);   
    }
    
    @Override
    public boolean isEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(properties, PROXY_ENABLED, false, false);
    }
    
    @Override
    public void setEnabled(boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, PROXY_ENABLED, enabled, false);
    }
}
