/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import mitm.common.util.Check;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;

public class BasicProxyFactory<T>
{
    private final Class<T> clazz;
    private ProxyMethodHandler methodHandler;
    
    /*
     * Set of methods that need to be handled.
     */
    private final Set<String> methods = new HashSet<String>();
    
    public BasicProxyFactory(Class<T> clazz) 
    throws NoSuchMethodException
    {
        this(clazz, null);
    }

    public BasicProxyFactory(Class<T> clazz, ProxyMethodHandler methodHandler) 
    throws NoSuchMethodException
    {
        this.clazz = clazz;
        this.methodHandler = methodHandler;
    }
    
    public void setMethodHandler(ProxyMethodHandler methodHandler) {
        this.methodHandler = methodHandler;
    }
    
    public void addMethod(String methodName, String parameterDescriptor)
    throws NoSuchMethodException
    {
        Method m = ReflectionUtils.findMethod(clazz, methodName, parameterDescriptor, true /* search super */);
        
        if (m == null) {
            throw new NoSuchMethodException("Method '" + ReflectionUtils.createMethodDescriptor(methodName, parameterDescriptor) + 
                        "' not found");
        }
        
        methods.add(ReflectionUtils.createMethodDescriptor(methodName, parameterDescriptor));
    }

    public void addMethod(Method method)
    {
        methods.add(ReflectionUtils.createMethodDescriptor(method));
    }
    
    protected MethodHandler createMethodHandler()
    {
        return new MethodHandler()
        {
            @Override
            public Object invoke(Object self, Method method, Method proceed, Object[] args) 
            throws Throwable 
            {
                return methodHandler.invoke(self, method, proceed, args);
            }
        };
    }
    
    protected MethodFilter createMethodFilter()
    {
        return new MethodFilter() 
        {
            @Override
            public boolean isHandled(Method m) 
            {
                String methodDescriptor = ReflectionUtils.createMethodDescriptor(m);
                
                return methods.contains(methodDescriptor);
            }
        };
    }
    
    protected void postConstruction(T newInstance) 
    throws ProxyFactoryException {
        /* must be overridden to add behavior */
    }
    
    @SuppressWarnings("unchecked")
    public T createProxy()
    throws ProxyFactoryException 
    {
    	try {
	        ProxyFactory proxyFactory = new ProxyFactory();
	        
	        proxyFactory.setSuperclass(clazz);
	        
	        proxyFactory.setFilter(createMethodFilter());
	        
	        Class<T> c = proxyFactory.createClass();
	        
	        T newInstance = c.newInstance();
	
	        if (methodHandler != null) 
	        {
	            MethodHandler mi = createMethodHandler();
	            
	            ((ProxyObject)newInstance).setHandler(mi);
	        }
	        
	        postConstruction(newInstance);
	        
	        return newInstance;
    	}
    	catch(InstantiationException e) {
    		throw new ProxyFactoryException(e);
    	} 
    	catch (IllegalAccessException e) {
    		throw new ProxyFactoryException(e);
		}
    }
    
    @SuppressWarnings("unchecked")
    public T createProxy(Class<?>[] parameterTypes, Object[] parameters)
    throws ProxyFactoryException 
    {
    	Check.notNull(parameterTypes, "parameterTypes");
    	Check.notNull(parameters, "parameters");
    	
    	if (parameterTypes.length != parameters.length) {
    		throw new IllegalArgumentException("parameterTypes and parameters mismatch.");
    	}
    	
    	try {
	        ProxyFactory proxyFactory = new ProxyFactory();
	        
	        proxyFactory.setSuperclass(clazz);
	        
	        proxyFactory.setFilter(createMethodFilter());
	        
	        Class<T> proxyClass = proxyFactory.createClass();
	
	        Constructor<T> constructor = proxyClass.getConstructor(parameterTypes);
	        
	        T newInstance = constructor.newInstance(parameters);
	
	        if (methodHandler != null) 
	        {
	            MethodHandler mi = createMethodHandler();
	            
	            ((ProxyObject)newInstance).setHandler(mi);
	        }
	        
	        postConstruction(newInstance);
	        
	        return newInstance;
    	}
    	catch(InstantiationException e) {
    		throw new ProxyFactoryException(e);
    	} 
    	catch (IllegalAccessException e) {
    		throw new ProxyFactoryException(e);
    	} 
    	catch (SecurityException e) {
    		throw new ProxyFactoryException(e);
		} 
    	catch (NoSuchMethodException e) {
    		throw new ProxyFactoryException(e);
		} 
    	catch (IllegalArgumentException e) {
    		throw new ProxyFactoryException(e);
		} 
    	catch (InvocationTargetException e) {
    		throw new ProxyFactoryException(e);
		}
    }    
}
