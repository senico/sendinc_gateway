/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import javassist.util.proxy.RuntimeSupport;

public class ReflectionUtils
{
    /**
     * Creates a complete method descriptor (method name and parameter descriptor)
     * @param name
     * @param parameterDescriptor
     * @return
     */
    public static String createMethodDescriptor(String name, String parameterDescriptor) {
        return name + ":" + parameterDescriptor;
    }

    /**
     * Creates a complete method descriptor (method name and parameter descriptor)
     * @param method
     * @return
     */
    public static String createMethodDescriptor(Method method) {
        return createMethodDescriptor(method.getName(), RuntimeSupport.makeDescriptor(method));
    }
    
    /**
     * Tries to find a method with the given name and parameters from the super classes interfaces included.
     * Interfaces are searched as well.
     * @param clazz
     * @param methodName
     * @param parameterDescriptor
     * @return
     */
    public static Method findSuperMethod(Class<?> clazz, String methodName, String parameterDescriptor) 
    {
        Method method = findSuperMethodInternal(clazz.getSuperclass(), methodName, parameterDescriptor);
        
        if (method == null) {
            method = searchInterfaces(clazz, methodName, parameterDescriptor);
        }

        return method;
    }    
    
    /*
     * This find does not search the interfaces
     */
    private static Method findSuperMethodInternal(Class<?> clazz, String methodName, String parameterDescriptor) 
    {
        Method method = findMethod(clazz, methodName, parameterDescriptor, false /* do not search super */);
        
        if (method != null) {
            return method; 
        }

        Class<?> superClass = clazz.getSuperclass();
        
        if (superClass != null) 
        {
            method = findSuperMethodInternal(superClass, methodName, parameterDescriptor);
            
            if (method != null) {
                return method;
            }
        }

        return searchInterfaces(clazz, methodName, parameterDescriptor);
    }

    /**
     * Tries to find a method with the given name and parameters from the implemented interfaces. 
     * @param clazz
     * @param methodName
     * @param parameterDescriptor
     * @return
     */
    public static Method searchInterfaces(Class<?> clazz, String methodName, String parameterDescriptor) 
    {
        Method method = null;
        
        Class<?>[] interfaces = clazz.getInterfaces();
        
        for (Class<?> intf : interfaces) 
        {
            method = findSuperMethodInternal(intf, methodName, parameterDescriptor);

            if (method != null) {
                return method;
            }
        }

        return method;
    }

    /**
     * Returns true if clazz is an instance of all the others
     */
    public static boolean isInstanceOf(Class<?> clazz, Class<?>... others)
    {
        for (Class<?> intf : others)
        {
            if (!intf.isAssignableFrom(clazz)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Tries to find a method with the given name and parameters. Only the method of clazz are searched ie.
     * no super class or interfaces are searched.
     * 
     * @param clazz
     * @param methodName
     * @param parameterDescriptor
     * @return
     */
    public static Method findMethod(Class<?> clazz, String methodName, String parameterDescriptor, boolean searchSuper) 
    {
        Method[] methods = clazz.getDeclaredMethods();
        
        for (Method method : methods) 
        {
            if (method.getName().equals(methodName) && 
                    RuntimeSupport.makeDescriptor(method).equals(parameterDescriptor)) 
            { 
                return method;
            }
        }
        
        if (searchSuper) {
            return findSuperMethod(clazz, methodName, parameterDescriptor);
        }
        
        return null;
    }
    
    public static boolean hasDefaultConstructor(Class<?> clazz)
    {
        try {
            Constructor<?> constuctor = clazz.getConstructor((Class<?>[])null);
            
            return constuctor != null;
        }
        catch (NoSuchMethodException e) {
            /* ignore */
        }
        
        return false;
    }
}
