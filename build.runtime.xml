<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
    
    This file is part of Djigzo email encryption.
    
    Djigzo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License 
    version 3, 19 November 2007 as published by the Free Software 
    Foundation.
    
    Djigzo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public 
    License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
    
    Additional permission under GNU AGPL version 3 section 7
    
    If you modify this Program, or any covered work, by linking or 
    combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
    freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
    spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
    spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
    wsdl4j-1.6.1.jar (or modified versions of these libraries), 
    containing parts covered by the terms of Eclipse Public License, 
    tyrex license, freemarker license, dom4j license, mx4j license,
    Spice Software License, Common Development and Distribution License
    (CDDL), Common Public License (CPL) the licensors of this Program grant 
    you additional permission to convey the resulting work.
-->
<!--
	ANT file for setting up Djigzo
-->
<project name="djigzo" basedir="." default="init">
	<property name="confdir"         value="conf"/>
	<property name="jarfile"         value="djigzo.jar"/>
	<property name="libdir"          value="lib"/>
	<property name="jamesdir"        value="james-2.3.1"/>
	<property name="jamessrcconfdir" value="${confdir}/james"/>
	<property name="jamesappsdir"    value="${jamesdir}/apps"/>
	<property name="sardir"          value="${jamesappsdir}"/>
	<property name="sarfile"         value="${sardir}/james.sar"/>
	<property name="jamessarinfdir"  value="${jamesappsdir}/james/SAR-INF"/>
	<property name="jamesconfdir"    value="${jamesappsdir}/james/conf"/>
	<property name="jameslibdir"     value="${jamessarinfdir}/lib"/>
	<property name="jameslib.d"      value="${jamessarinfdir}/lib.d"/>
	<property name="jamesclassesdir" value="${jamessarinfdir}/classes"/>
	<property name="sunjceprovider"  value="${java.home}/lib/ext/sunjce_provider.jar"/>

	<!-- we need the foreach task from ant-contrib -->
	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="lib/ant/ant-contrib-1.0b3.jar"/>
		</classpath>
	</taskdef>

	<target name="init" description="initializes the environment (must be run after unpacking the distribution)" 
		depends="create-symlinks, make-scripts-executable, create-sunjceprovider-softlink, init-wrapper, create-james-config-symlinks">
	</target>

	<target name="create-symlinks" description="create symlinks to the jars.">
        <!-- 
            we first need to remove any existing jars because if jars are removed with a new upgrade
            we get classpath errors when starting Djigzo
        --> 
        <delete dir="${jameslibdir}"/>

		<mkdir dir="${jameslibdir}"/>

		<!-- create symlinks for the runtime jars -->
		<foreach target="create-symlink" param="jar.file">
			<path>
				<fileset dir="${libdir}">
					<include name="**/*.jar"/>
				</fileset>
				<fileset dir=".">
					<include name="${jarfile}"/>
				</fileset>
			</path>
		</foreach>
		
		<!-- create a symlink to lib.d directory -->
		<symlink link="${jameslib.d}" resource="../../../../lib/lib.d" overwrite="true"/>		
	</target>

	<!-- creates a symlink to the file specified by the ${file} parameter in the james SAR-INF lib dir -->
	<target name="create-symlink">

		<echo message="${jar.file}"/>
		
		<!-- extract the filename from the path -->
		<basename property="jar.filename" file="${jar.file}"/>

		<!-- we need to explicitly set the link filename otherwise overwrite won't work -->
		<symlink link="${jameslibdir}/${jar.filename}" resource="${jar.file}" overwrite="true"/>
	</target>

	<target name="make-scripts-executable" description="make scripts executable.">
		<chmod dir="." perm="755" includes="**/*.sh"/>
		<chmod dir="scripts" perm="755" includes="**/*.sh"/>
		<chmod dir="scripts" perm="755" includes="djigzo"/>
	</target>

	<target name="init-wrapper" description="unpacks the Java wrapper.">
		<ant dir="wrapper" inheritall="false"/>
	</target>

	<target name="create-sunjceprovider-softlink">
		<echo>create symlink to ${sunjceprovider} in ${jamesdir}/lib</echo>

		<!-- we need to delete the symlink first because overwrite seems not to work -->
		<delete failonerror="false" file="${jamesdir}/lib/sunjce_provider.jar"/>
		<symlink link="${jamesdir}/lib/sunjce_provider.jar" resource="${sunjceprovider}" overwrite="true"/>
	</target>

    <target name="symlink-overwrite">
        <!-- somehow overwrite is not enough so we will delete the file first -->
        <delete file="${link}"/>
        <symlink link="${link}" resource="${resource}" overwrite="true"/>
    </target>

    <target name="create-james-config-symlinks" description="Creates links to the James config files">
		<mkdir dir="${jamessarinfdir}"/>
		<mkdir dir="${jamesconfdir}"/>

    	<!-- files from conf/james/conf-->
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamesconfdir}/james-smtphandlerchain.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/conf/james-smtphandlerchain.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamesconfdir}/sqlResources.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/conf/sqlResources.xml"/>
        </antcall>
    	
        <!-- files from conf/james/SAR-INF-->
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/assembly.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/assembly.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/config.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/config.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/custom_processors_config.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/custom_processors_config.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/dlp.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/dlp.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/environment.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/environment.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/internal_remote_delivery_processor.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/internal_remote_delivery_processor.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/smtp_server_config.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/smtp_server_config.xml"/>
        </antcall>
        <antcall target="symlink-overwrite">
            <param name="link" value="${jamessarinfdir}/smtp_transport_config.xml"/>
            <param name="resource" value="../../../../${jamessrcconfdir}/SAR-INF/smtp_transport_config.xml"/>
        </antcall>
    </target>
	
	<!-- 
		imports all certificates from windows-xp-all-roots.p7b and windows-xp-all-intermediates.p7b
		into the certificate store. 
	-->
	<target name="import-ca" description="import roots and intermediates.">
		<java classname="mitm.application.djigzo.tools.manager.Manager" fork="true">
			<classpath>
				<pathelement location="${jarfile}"/>
			</classpath>

			<arg line="-cer resources/certificates/windows-xp-all-roots.p7b"/>
		</java>

		<java classname="mitm.application.djigzo.tools.manager.Manager" fork="true">
			<classpath>
				<pathelement location="${jarfile}"/>
			</classpath>

			<arg line="-cer resources/certificates/windows-xp-all-intermediates.p7b"/>
		</java>
	</target>
</project>
