
    create table Admin (
        id int8 not null,
        builtIn bool,
        enabled bool,
        password varchar(255),
        passwordEncoding int4,
        salt varchar(255),
        username varchar(255) not null unique,
        primary key (id)
    );

    create table Admin_Authority (
        Admin_id int8 not null,
        authorities_id int8 not null,
        primary key (Admin_id, authorities_id)
    );

    create table Authority (
        id int8 not null,
        role varchar(255) not null unique,
        primary key (id)
    );

    create table blob (
        id int8 not null,
        blob bytea,
        primary key (id)
    );

    create table certificateRequest (
        id int8 not null,
        certificateHandlerName varchar(255),
        created timestamp not null,
        crlDistPoint varchar(1024),
        data bytea,
        email varchar(320),
        info varchar(1024),
        iteration int4,
        keyLength int4,
        lastMessage varchar(1024),
        lastUpdated timestamp,
        nextUpdate timestamp,
        privateKey bytea,
        publicKey bytea,
        signatureAlgorithm varchar(255),
        subject bytea,
        validity int4,
        primary key (id)
    );

    create table certificates (
        id int8 not null,
        notBefore timestamp,
        notAfter timestamp,
        issuer text,
        issuerFriendly text,
        serial varchar(1024),
        subjectKeyIdentifier text,
        subject text,
        subjectFriendly text,
        certificate bytea,
        thumbprint varchar(255),
        certPath bytea,
        certPathType varchar(255),
        creationDate timestamp,
        datePathUpdated timestamp,
        keyAlias varchar(1024),
        storeName varchar(255),
        primary key (id),
        unique (storeName, thumbprint)
    );

    create table certificates_email (
        certificates_id int8 not null,
        email varchar(320)
    );

    create table crls (
        id int8 not null,
        creationDate timestamp,
        issuer text,
        nextUpdate timestamp,
        thisUpdate timestamp,
        crlNumber varchar(1024),
        crl bytea,
        thumbprint varchar(255),
        storeName varchar(255),
        primary key (id),
        unique (storeName, thumbprint)
    );

    create table ctl (
        id int8 not null,
        name varchar(255),
        thumbprint varchar(255) not null,
        primary key (id),
        unique (name, thumbprint)
    );

    create table ctl_nameValues (
        ctl_id int8 not null,
        value text,
        name text,
        primary key (ctl_id, name)
    );

    create table keystore (
        id int8 not null,
        alias varchar(1024) not null,
        certificate bytea,
        certificateType varchar(255),
        thumbprint varchar(255),
        certificateChain bytea,
        creationDate timestamp,
        encodedKey bytea,
        storeName varchar(255) not null,
        primary key (id),
        unique (storeName, alias)
    );

    create table mail_repository (
        id varchar(255) not null,
        data bytea,
        created timestamp,
        fromHeader varchar(1024),
        lastUpdated timestamp,
        messageID varchar(1024),
        originator varchar(1024),
        remoteAddress varchar(1024),
        repository varchar(255),
        sender varchar(1024),
        subject varchar(1024),
        mime_id int8,
        primary key (id)
    );

    create table mail_repository_recipients (
        id varchar(255) not null,
        recipients varchar(1024)
    );

    create table named_blob (
        id int8 not null,
        category varchar(255) not null,
        name varchar(255) not null,
        blobEntity_id int8,
        primary key (id),
        unique (category, name)
    );

    create table named_blob_named_blob (
        named_blob_id int8 not null,
        namedBlobs_id int8 not null,
        primary key (named_blob_id, namedBlobs_id)
    );

    create table properties (
        id int8 not null,
        category varchar(320) unique,
        primary key (id)
    );

    create table properties_nameValues (
        properties_id int8 not null,
        value text,
        name text,
        primary key (properties_id, name)
    );

    create table sms (
        id int8 not null,
        data bytea,
        dateCreated timestamp,
        dateLastTry timestamp,
        lastError varchar(1024),
        message varchar(2048) not null,
        phoneNumber varchar(255) not null,
        primary key (id)
    );

    create table userpreferences (
        id int8 not null,
        category varchar(320) not null,
        name varchar(320) not null,
        keyAndCertificateEntry_id int8,
        propertyEntity_id int8,
        primary key (id),
        unique (category, name)
    );

    create table userpreferences_certificates (
        userpreferences_id int8 not null,
        certificates_id int8 not null,
        primary key (userpreferences_id, certificates_id)
    );

    create table userpreferences_inheritedpreferences (
        userpreferences_id int8 not null,
        index int8 not null,
        inherited_preferences_id int8 not null,
        primary key (userpreferences_id, index, inherited_preferences_id)
    );

    create table userpreferences_named_blob (
        userpreferences_id int8 not null,
        namedBlobs_id int8 not null,
        primary key (userpreferences_id, namedBlobs_id)
    );

    create table userpreferences_named_certificates (
        userpreferences_id int8 not null,
        certificateEntry_id int8,
        name varchar(255)
    );

    create table users (
        id int8 not null,
        email varchar(320) not null unique,
        userPreferencesEntity_id int8,
        primary key (id)
    );

    alter table Admin_Authority 
        add constraint FK31EB4613A92DB044 
        foreign key (Admin_id) 
        references Admin;

    alter table Admin_Authority 
        add constraint FK31EB46135DD88A6 
        foreign key (authorities_id) 
        references Authority;

    create index certificateRequest_email_index on certificateRequest (email);

    create index certificateRequest_next_update_index on certificateRequest (nextUpdate);

    create index certificateRequest_created_index on certificateRequest (created);

    create index certificates_issuer_index on certificates (issuer);

    create index certificates_key_alias_index on certificates (keyAlias);

    create index certificates_thumbprint_index on certificates (thumbprint);

    create index certificates_creationdate_index on certificates (creationDate);

    create index certificates_subject_index on certificates (subject);

    create index certificates_store_name_index on certificates (storeName);

    create index certificates_subject_key_id_index on certificates (subjectKeyIdentifier);

    create index certificates_serial_index on certificates (serial);

    alter table certificates_email 
        add constraint FK904263596F91DC04 
        foreign key (certificates_id) 
        references certificates;

    create index crls_thumbprint_index on crls (thumbprint);

    create index crls_issuer_index on crls (issuer);

    create index crls_crlnumber_index on crls (crlNumber);

    create index crls_store_name_index on crls (storeName);

    alter table ctl_nameValues 
        add constraint FKC370B4312C4F7350 
        foreign key (ctl_id) 
        references ctl;

    create index keystore_alias_index on keystore (alias);

    create index keystore_storename_index on keystore (storeName);

    alter table mail_repository 
        add constraint FK9F4A42D2A7662F3 
        foreign key (mime_id) 
        references blob;

    alter table mail_repository_recipients 
        add constraint FK16FD4D076BBA8DE5 
        foreign key (id) 
        references mail_repository;

    alter table named_blob 
        add constraint FKEF3FACE33F4FE1E7 
        foreign key (blobEntity_id) 
        references blob;

    alter table named_blob_named_blob 
        add constraint FKC12079BF50DFF003 
        foreign key (named_blob_id) 
        references named_blob;

    alter table named_blob_named_blob 
        add constraint FKC12079BF9DF0D9C9 
        foreign key (namedBlobs_id) 
        references named_blob;

    alter table properties_nameValues 
        add constraint FK1DE74A19D9423B4C 
        foreign key (properties_id) 
        references properties;

    create index sms_datelasttry_index on sms (dateLastTry);

    create index userpreferences_name_index on userpreferences (name);

    create index userpreferences_category_index on userpreferences (category);

    alter table userpreferences 
        add constraint FK8551364DFE700867 
        foreign key (propertyEntity_id) 
        references properties;

    alter table userpreferences 
        add constraint FK8551364D3F326A0D 
        foreign key (keyAndCertificateEntry_id) 
        references certificates;

    alter table userpreferences_certificates 
        add constraint FKB8B48A6E4E408931 
        foreign key (userpreferences_id) 
        references userpreferences;

    alter table userpreferences_certificates 
        add constraint FKB8B48A6E6F91DC04 
        foreign key (certificates_id) 
        references certificates;

    alter table userpreferences_inheritedpreferences 
        add constraint FK46ABEE904E408931 
        foreign key (userpreferences_id) 
        references userpreferences;

    alter table userpreferences_inheritedpreferences 
        add constraint FK46ABEE90F11D70AB 
        foreign key (inherited_preferences_id) 
        references userpreferences;

    alter table userpreferences_named_blob 
        add constraint FKA77DCA154E408931 
        foreign key (userpreferences_id) 
        references userpreferences;

    alter table userpreferences_named_blob 
        add constraint FKA77DCA159DF0D9C9 
        foreign key (namedBlobs_id) 
        references named_blob;

    alter table userpreferences_named_certificates 
        add constraint FKAAB29434A3567C25 
        foreign key (certificateEntry_id) 
        references certificates;

    alter table userpreferences_named_certificates 
        add constraint FKAAB294344E408931 
        foreign key (userpreferences_id) 
        references userpreferences;

    alter table users 
        add constraint FK6A68E08BA1F7C2E 
        foreign key (userPreferencesEntity_id) 
        references userpreferences;

    create sequence hibernate_sequence;
    create index certificates_email_email_index on certificates_email (email);