<!--
    The DLP mailet/matcher configuration fragment. This fragment will be included by config.xml
-->
<processor name="dlp">
    <mailet match="All" class="Log">
        <comment> dlp state </comment>
    </mailet>

    <mailet match="All" class="SenderRegExpPolicyChecker">
        <log> DLP checking the message </log>
        <warnProcessor> dlpWarn </warnProcessor>                
        <mustEncryptProcessor> dlpMustEncrypt </mustEncryptProcessor>                
        <quarantineProcessor> dlpQuarantine </quarantineProcessor>                
        <blockProcessor> dlpBlock </blockProcessor>                
        <errorProcessor> dlpError </errorProcessor>                
    </mailet>

    <mailet match="All" class="GotoProcessor">
        <processor> postDLP </processor>
    </mailet>
</processor>

<processor name="dlpWarn">
    <mailet match="All" class="Log">
        <comment> A DLP warning rule was activated for the message </comment>
    </mailet>

    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendWarningToOriginator}=='true'" 
            class="MailAttributes">
        <delete>runtime.recipients</delete>
        <add>runtime.recipients=${originator}</add>
    </mailet>

    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendWarningToDLPManagers}=='true'" 
            class="MailAttributes">
        <add>runtime.recipients=#{user.dlp.dlpManagers}</add>
    </mailet>
                
    <mailet match="All" class="Notify">
        <log> Sending a DLP warning notification message </log>
        <template> dlp.warning.ftl </template>
        <templateProperty> user.template.dlp.warning </templateProperty>
        <recipients> ${runtime.recipients} </recipients>
        <ignoreMissingRecipients> true </ignoreMissingRecipients>
        <sender> ${null} </sender>
        <to> ${sameAsRecipients} </to>
        <subject> ${sameAsMessage} </subject>
        <from> postmaster </from>
        <processor> transport-auto-submitted </processor>
        <passThrough> true </passThrough>
    </mailet>
                
    <mailet match="All" class="GotoProcessor">
        <processor> postDLP </processor>
    </mailet>
</processor>

<processor name="dlpMustEncrypt">
    <mailet match="All" class="Log">
        <comment> A DLP must encrypt rule was activated for the message </comment>
    </mailet>
    
    <mailet match="All" class="SetAttributes">
        <!-- message must be encrypted -->
        <runtime.mustEncrypt> true </runtime.mustEncrypt>
    </mailet>

    <mailet match="All" class="GotoProcessor">
        <processor> postDLP </processor>
    </mailet>
</processor>

<processor name="dlpQuarantine">
    <mailet match="All" class="Log">
        <comment> A DLP quarantine rule was activated for the message </comment>
    </mailet>
    
    <mailet match="All" class="Quarantine">
        <log> The message was quarantined </log>
        <errorProcessor> error </errorProcessor>
    </mailet>
    
    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendQuarantineToOriginator}=='true'" 
            class="MailAttributes">
        <delete>runtime.recipients</delete>
        <add>runtime.recipients=${originator}</add>
    </mailet>

    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendQuarantineToDLPManagers}=='true'" 
            class="MailAttributes">
        <add>runtime.recipients=#{user.dlp.dlpManagers}</add>
    </mailet>

    <mailet match="All" class="Notify">
        <log> Sending a DLP quarantine notification message </log>
        <template> dlp.quarantine.ftl </template>
        <templateProperty> user.template.dlp.quarantine </templateProperty>
        <recipients> ${runtime.recipients} </recipients>
        <userProperty>user.dlp.quarantineURL</userProperty>
        <ignoreMissingRecipients> true </ignoreMissingRecipients>
        <sender> ${null} </sender>
        <to> ${sameAsRecipients} </to>
        <subject> ${sameAsMessage} </subject>
        <from> postmaster </from>
        <processor> transport-auto-submitted </processor>
        <passThrough> false </passThrough>
    </mailet>
    
    <!-- should not happen. Can happen if template contains errors -->
    <mailet match="All" class="GotoProcessor">
        <processor> error </processor>
    </mailet>            
</processor>

<processor name="dlpBlock">
    <mailet match="All" class="Log">
        <comment> A DLP block rule was activated for the message </comment>
    </mailet>
    
    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendBlockToOriginator}=='true'" 
            class="MailAttributes">
        <delete>runtime.recipients</delete>
        <add>runtime.recipients=${originator}</add>
    </mailet>

    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendBlockToDLPManagers}=='true'" 
            class="MailAttributes">
        <add>runtime.recipients=#{user.dlp.dlpManagers}</add>
    </mailet>
                
    <mailet match="All" class="Notify">
        <log> Sending a DLP blocking notification message </log>
        <template> dlp.block.ftl </template>
        <templateProperty> user.template.dlp.block </templateProperty>
        <recipients> ${runtime.recipients} </recipients>
        <ignoreMissingRecipients> true </ignoreMissingRecipients>
        <sender> ${null} </sender>
        <to> ${sameAsRecipients} </to>
        <subject> ${sameAsMessage} </subject>
        <from> postmaster </from>
        <processor> transport-auto-submitted </processor>
        <passThrough> false </passThrough>
    </mailet>
    
    <!-- should not happen. Can happen if template contains errors -->
    <mailet match="All" class="GotoProcessor">
        <processor> error </processor>
    </mailet>
</processor>

<processor name="dlpError">
    <mailet match="All" class="Log">
        <comment> Error occurred while DLP scanning </comment>
        <logLevel> WARN </logLevel>
    </mailet>
    
    <!-- in case of error, the message will be quarantined if the settings say so -->
    <mailet match="SenderEvaluateUserProperty=matchOnError=true,#{user.dlp.quarantineOnError}!='true'" 
            class="GotoProcessor">
        <log> Quarantine On Error is disabled </log>
        <processor> postDLP </processor>
    </mailet>
    
    <mailet match="All" class="Quarantine">
        <log> The message was quarantined </log>
        <errorProcessor> error </errorProcessor>
    </mailet>
    
    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendErrorToOriginator}=='true'" 
            class="MailAttributes">
        <delete>runtime.recipients</delete>
        <add>runtime.recipients=${originator}</add>
    </mailet>

    <mailet match="SenderEvaluateUserProperty=matchOnError=false,#{user.dlp.sendErrorToDLPManagers}=='true'" 
            class="MailAttributes">
        <add>runtime.recipients=#{user.dlp.dlpManagers}</add>
    </mailet>

    <mailet match="All" class="Notify">
        <log> Sending DLP error notification message </log>
        <template> dlp.error.ftl </template>
        <templateProperty> user.template.dlp.error </templateProperty>
        <recipients> ${runtime.recipients} </recipients>
        <userProperty>user.dlp.quarantineURL</userProperty>
        <ignoreMissingRecipients> true </ignoreMissingRecipients>
        <sender> ${null} </sender>
        <to> ${sameAsRecipients} </to>
        <subject> ${sameAsMessage} </subject>
        <from> postmaster </from>
        <processor> transport-auto-submitted </processor>
        <passThrough> false </passThrough>
    </mailet>
    
    <!-- should not happen. Can happen if template contains errors -->
    <mailet match="All" class="GotoProcessor">
        <processor> error </processor>
    </mailet>            
</processor>

