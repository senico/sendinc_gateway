Name: sendinc-gateway
Version: @VERSION@
Release: @RELEASE@
Summary: Sendinc Email Encryption Gateway
License: AGPLv3
Group: System Environment/Daemons
URL: http://www.sendinc.com
Vendor: Sendinc
Requires: redhat-lsb
Requires: sudo
Requires: postgresql
Requires: postgresql-server
Requires: postfix
Requires: java-1.6.0-openjdk-devel
Requires: ant
Requires: ant-nodeps
Requires: mktemp
Requires: symlinks

%define _rpmdir .
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

%post
#!/bin/bash -e
# postinst script for djigzo

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

DJIGZO_HOME=/usr/share/djigzo
DJIGZO_USER=djigzo
DJIGZO_GROUP=djigzo
DJIGZO_DB_USER=djigzo

DJIGZO_SUDOERS=`cat <<EOF

User_Alias DJIGZO_USERS = djigzo
Cmnd_Alias DJIGZO_COMMANDS = $DJIGZO_HOME/scripts/docopy-postfix-main-config.sh,\ 
    $DJIGZO_HOME/scripts/dosmtp-client-passwd-config.sh,\ 
    $DJIGZO_HOME/scripts/docopy-jce-policy.sh,\ 
    $DJIGZO_HOME/scripts/dopostfix.sh,\ 
    $DJIGZO_HOME/scripts/dobackup.sh,\ 
    $DJIGZO_HOME/scripts/dorestart.sh
DJIGZO_USERS ALL=(ALL) NOPASSWD: DJIGZO_COMMANDS
EOF`

replace_sudoers_entry()
{
    local INPUT=$1
    local ALIAS=$2
    local NAME=$3
    local REPLACE=$4
    
    sed -n --in-place '
    # if the first line copy the pattern to the hold buffer
    1h
    # if not the first line then append the pattern to the hold buffer
    1!H
    # if the last line then ...
    $ {
            # copy from the hold to the pattern buffer
            g
            # do the search and replace.
            # it seems that with the M flag . does not match newlines
            s/^'$ALIAS'[ \t]*'$NAME'[ \t]*=[ \t]*.*\(\\[ \t]*\n.*\)*\n\?/'$REPLACE'/gM
            # print
            p
    }
    ' "$INPUT"
}

edit_sudoers()
{
    # we need to check if visudo is not already running by checking the sudoers.tmp
    SUDOERS_TMP="/etc/sudoers.tmp"
    
    if [ -f "$SUDOERS_TMP" ]; then
            echo "sudoers.tmp exists. Is someone editing sudoers?"
            exit 1
    fi
    
    touch "$SUDOERS_TMP"
    
    SUDOERS_NEW=$(mktemp) || exit 1
    
    cp /etc/sudoers "$SUDOERS_NEW"
    
    # first remove all Djigzo sudoers lines
    replace_sudoers_entry "$SUDOERS_NEW" "User_Alias" "DJIGZO_USERS" ''
    replace_sudoers_entry "$SUDOERS_NEW" "Cmnd_Alias" "DJIGZO_COMMANDS" ''
    replace_sudoers_entry "$SUDOERS_NEW" "DJIGZO_USERS" "ALL" ''
    
    # requiretty must be disabled!
    sed -r "/^Defaults\s+requiretty/s/^/#/g" "$SUDOERS_NEW" --in-place

    # delete all trailing blank lines at end of file
    sed --in-place -e :a -e '/^\n*$/{$d;N;ba' -e '}' "$SUDOERS_NEW"    
    
    # apend new sudoer lines
    echo -n "$DJIGZO_SUDOERS" >> "$SUDOERS_NEW"
    
    # make visudo check if we have a valid sudoers file
    visudo -c -f "$SUDOERS_NEW" > /dev/null
    
    local EXIT_CODE=$?
    
    if [ $EXIT_CODE -eq "0" ]; then
            cp "$SUDOERS_NEW" /etc/sudoers
    fi
}

config_postfix()
{
    cp $DJIGZO_HOME/conf/system/main.cf /etc/postfix/djigzo-main.cf
    cp $DJIGZO_HOME/conf/system/master.cf /etc/postfix/djigzo-master.cf
}

wait_for_postgres_active()
{
    # try for 10 times and wait 2 seconds between every check
    MAX_TRIES=10
    SLEEP=2

    pg_running=-1

    pg_retry=1

    while [ $pg_running -ne 1 ]
    do
        pg_running=1

        service postgresql status || pg_running=0

        if [ $pg_running -ne 1 ]; then
#            echo "not running"
            sleep $SLEEP
        fi

        (( pg_retry++ ))

        if [ $pg_retry -eq $MAX_TRIES ]; then
            echo "Postgresql is unable to start"
            exit 1
        fi
    done
    
    # wait some more time to be 100% certain that postgres is running
    sleep 4
}

cleanup()
{
    if [ -f "$SUDOERS_TMP" ]; then
        rm "$SUDOERS_TMP"
    fi

    if [ -f "$SUDOERS_NEW" ]; then
        rm "$SUDOERS_NEW"
    fi    
}

# disabled since this depends too much on the actual Java version of OpenJDK
#update_java()
#{
#    /usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.6.0-openjdk/bin/java
#}

finish_message()
{
    echo "*****************************************************************************"
    echo "* The installation has finished. Postfix should now be configured.          *"
    echo "* If the current postfix config (the config prior to installation) should   *"
    echo "* not be kept, it's best to move the example postfix config files (/etc/    *"
    echo "* postfix/djigzo-main.cf and /etc/postfix/djigzo-master.cf) to the          *"
    echo "* corresponding postfix config files (main.cf and master.cf). If the        *"
    echo "* current postfix config cannot be overwritten, the relevant changes must   *"
    echo "* be merged manually.                                                       *"
    echo "*****************************************************************************"
}

# create a trap that will cleanup all temporary resources
trap cleanup ABRT EXIT HUP INT QUIT

# add user djigzo if user does not exist
if ! id "$DJIGZO_USER" > /dev/null 2>&1 ; then
    adduser --home-dir "$DJIGZO_HOME" -m  --shell /sbin/nologin "$DJIGZO_USER"
fi

# disabled since this depends too much on the actual Java version of OpenJDK
#update_java

# Make sure that PostgreSQL is started at bootup
chkconfig postgresql on

# Start PostgreSQL
service postgresql start

# By default PostgreSQL does not allow to login with username/password.   
# PostgreSQL must be configured for username/password login

# comment out the line with "ident sameuser" authentication and add "md5" authentication  
sed -r "/^host\s+all\s+all\s+127.0.0.1\/32\s+ident/s/^/#/g" /var/lib/pgsql/data/pg_hba.conf --in-place
# only add md5 authentication for localhost if not already added
if ! grep -E "^host\s*all\s*all\s*127.0.0.1/32\s*md5" /var/lib/pgsql/data/pg_hba.conf > /dev/null; then
    echo "host    all         all         127.0.0.1/32         md5" >> /var/lib/pgsql/data/pg_hba.conf
fi

# restart PostgreSQL
service postgresql restart

# we need to wait for postgres to startup 
wait_for_postgres_active

# Add a database user (username: djigzo, password:djigzo)
echo "CREATE USER "$DJIGZO_DB_USER" NOCREATEUSER NOCREATEDB ENCRYPTED PASSWORD 'md5b720bc9de4ca53d53a4059882a0868b9';" | sudo -u postgres psql

# Create Djigzo database
sudo -u postgres createdb --owner "$DJIGZO_DB_USER" djigzo

# make djigzo home owned by user djigzo
chown -R "$DJIGZO_USER":"$DJIGZO_GROUP" $DJIGZO_HOME

cd $DJIGZO_HOME
sudo -u "$DJIGZO_USER" ant

# import the database schema
sudo -u "$DJIGZO_USER" psql djigzo < $DJIGZO_HOME/conf/djigzo.sql || true

# scripts need to be owned by root because they will be added to the sudoers. Only root should be allowed to change the scripts
chown root:root $DJIGZO_HOME/scripts/*
chmod go-w $DJIGZO_HOME/scripts/*

# make some files containing passwords non readable by other  
chmod 640 $DJIGZO_HOME/conf/djigzo.properties
chmod 640 $DJIGZO_HOME/conf/hibernate.cfg.xml

# fetchmail config jar must be owned by root and 644 because it's used from a script
FETCHMAIL_CONFIG_JAR=$DJIGZO_HOME/lib/djigzo/fetchmail-config.jar
chown root:root $FETCHMAIL_CONFIG_JAR
chmod 644 $FETCHMAIL_CONFIG_JAR

ln -s $DJIGZO_HOME/scripts/djigzo /etc/init.d/djigzo

# make sure that Djigzo starts at boot time
chkconfig  --add djigzo

# RH/CentOS enable SELinux. We should allow text relocation for the Java wrapper lib
chcon -t textrel_shlib_t $DJIGZO_HOME/wrapper/libwrapper.so

# add djigzo dir in etc and create soft links
mkdir /etc/djigzo
mkdir /etc/djigzo/james
mkdir /etc/djigzo/spring

ln -s $DJIGZO_HOME/wrapper/djigzo.wrapper.conf /etc/djigzo/

ln -s $DJIGZO_HOME/conf/djigzo.properties /etc/djigzo/
ln -s $DJIGZO_HOME/conf/log4j.properties /etc/djigzo/
ln -s $DJIGZO_HOME/conf/hibernate.cfg.xml /etc/djigzo/
ln -s $DJIGZO_HOME/conf/mimetypes.xml /etc/djigzo/

ln -s $DJIGZO_HOME/conf/james/SAR-INF/config.xml /etc/djigzo/james
ln -s $DJIGZO_HOME/conf/james/SAR-INF/custom_processors_config.xml /etc/djigzo/james
ln -s $DJIGZO_HOME/conf/james/SAR-INF/dlp.xml /etc/djigzo/james
ln -s $DJIGZO_HOME/conf/james/SAR-INF/internal_remote_delivery_processor.xml /etc/djigzo/james
ln -s $DJIGZO_HOME/conf/james/SAR-INF/smtp_server_config.xml /etc/djigzo/james
ln -s $DJIGZO_HOME/conf/james/SAR-INF/smtp_transport_config.xml /etc/djigzo/james

ln -s $DJIGZO_HOME/conf/spring/backup.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/certificate-request-handlers.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/djigzo.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/dlp.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/extractors.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/general.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/jobs.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/services.xml /etc/djigzo/spring/
ln -s $DJIGZO_HOME/conf/spring/soap.xml /etc/djigzo/spring/

ln -s $DJIGZO_HOME/logs/james.wrapper.log /var/log/djigzo.log

edit_sudoers

config_postfix

finish_message

exit 0

%preun
#!/bin/bash -e

# only continue if last instance is removed
if [ -z $1 ] || [ $1 != '0' ]; then
  exit 0
fi

chkconfig --del djigzo

if [ -x "/etc/init.d/djigzo" ]; then
    /etc/init.d/djigzo stop || exit $?
fi

%postun
#!/bin/bash -e
# postrm script for djigzo
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# only continue if last instance is removed
if [ -z $1 ] || [ $1 != '0' ]; then
  exit 0
fi

DJIGZO_HOME="/usr/share/djigzo"
DJIGZO_USER=djigzo

replace_sudoers_entry()
{
    local INPUT=$1
    local ALIAS=$2
    local NAME=$3
    local REPLACE=$4
    
    sed -n --in-place '
    # if the first line copy the pattern to the hold buffer
    1h
    # if not the first line then append the pattern to the hold buffer
    1!H
    # if the last line then ...
    $ {
            # copy from the hold to the pattern buffer
            g
            # do the search and replace.
            # it seems that with the M flag . does not match newlines
            s/^'$ALIAS'[ \t]*'$NAME'[ \t]*=[ \t]*.*\(\\[ \t]*\n.*\)*\n\?/'$REPLACE'/gM
            # print
            p
    }
    ' "$INPUT"
}

edit_sudoers()
{
    # we need to check if visudo is not already running by checking the sudoers.tmp
    SUDOERS_TMP="/etc/sudoers.tmp"
    
    if [ -f "$SUDOERS_TMP" ]; then
            echo "sudoers.tmp exists. Is someone editing sudoers?"
            exit 1
    fi
    
    touch "$SUDOERS_TMP"
    
    SUDOERS_NEW=$(mktemp) || exit 1
    
    cp /etc/sudoers "$SUDOERS_NEW"
    
    # first remove all Djigzo sudoers lines
    replace_sudoers_entry "$SUDOERS_NEW" "User_Alias" "DJIGZO_USERS" ''
    replace_sudoers_entry "$SUDOERS_NEW" "Cmnd_Alias" "DJIGZO_COMMANDS" ''
    replace_sudoers_entry "$SUDOERS_NEW" "DJIGZO_USERS" "ALL" ''
    
    # delete all trailing blank lines at end of file
    sed --in-place -e :a -e '/^\n*$/{$d;N;ba' -e '}' "$SUDOERS_NEW"    
    
    # make visudo check if we have a valid sudoers file
    visudo -c -f "$SUDOERS_NEW" > /dev/null
    
    local EXIT_CODE=$?
    
    if [ $EXIT_CODE -eq "0" ]; then
            cp "$SUDOERS_NEW" /etc/sudoers
    fi
}

cleanup()
{
    if [ -f "$SUDOERS_TMP" ]; then
        rm "$SUDOERS_TMP"
    fi

    if [ -f "$SUDOERS_NEW" ]; then
        rm "$SUDOERS_NEW"
    fi    
}

finish_message()
{
    echo "*****************************************************************************"
    echo "* Sendinc Gateway has been removed. Don't forget to restore the postfix     *"
    echo "* settings to those prior to installation, if needed. The database (djigzo) *"
    echo "* has not been removed; the database should be manually removed.            *"
    echo "*****************************************************************************"
}

# create a trap that will cleanup all temporary resources
trap cleanup ABRT EXIT HUP INT QUIT

edit_sudoers

# remove some symlinks which created in post install
rm /var/log/djigzo.log
rm /etc/init.d/djigzo

# remove djigzo home and /etc/djigzo
rm -r $DJIGZO_HOME
rm -r /etc/djigzo

finish_message

%description

Sendinc Email Encryption ateway is a mail server (MTA) that encrypts your
outgoing email.

%files
%dir "/"
%dir "/usr/"
%dir "/usr/share/"
%dir "/usr/share/djigzo/"
%dir "/usr/share/djigzo/lib/"
%dir "/usr/share/djigzo/logs/"
%dir "/usr/share/djigzo/scripts/"
%dir "/usr/share/djigzo/wrapper/"

%config "/etc/default/djigzo"
%config "/usr/share/djigzo/wrapper/djigzo.wrapper.conf"
%config "/usr/share/djigzo/conf/james/conf/james-smtphandlerchain.xml"
%config "/usr/share/djigzo/conf/james/conf/sqlResources.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/assembly.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/config.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/custom_processors_config.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/dlp.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/environment.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/internal_remote_delivery_processor.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/smtp_server_config.xml"
%config "/usr/share/djigzo/conf/james/SAR-INF/smtp_transport_config.xml"
%config "/usr/share/djigzo/conf/spring/backup.xml"
%config "/usr/share/djigzo/conf/spring/certificate-request-handlers.xml"
%config "/usr/share/djigzo/conf/spring/certificate-request-resolvers.xml"
%config "/usr/share/djigzo/conf/spring/djigzo.xml"
%config "/usr/share/djigzo/conf/spring/dlp.xml"
%config "/usr/share/djigzo/conf/spring/extractors.xml"
%config "/usr/share/djigzo/conf/spring/general.xml"
%config "/usr/share/djigzo/conf/spring/jobs.xml"
%config "/usr/share/djigzo/conf/spring/services.xml"
%config "/usr/share/djigzo/conf/spring/soap.xml"
%config "/usr/share/djigzo/conf/spring/spring.d/README.txt"
%config "/usr/share/djigzo/conf/charset-aliases.properties"
%config "/usr/share/djigzo/conf/djigzo.properties"
"/usr/share/djigzo/conf/djigzo.sql"
"/usr/share/djigzo/conf/djigzo-drop-tables.sql"
%config "/usr/share/djigzo/conf/error-logging.log4j.properties"
%config "/usr/share/djigzo/conf/hibernate.cfg.xml"
%config "/usr/share/djigzo/conf/log4j.properties"
%config "/usr/share/djigzo/conf/mimetypes.xml"
%config "/usr/share/djigzo/conf/skip-words.txt"
%config "/usr/share/djigzo/conf/tools.log4j.properties"

"/usr/share/djigzo/conf/system/fetchmailrc"
"/usr/share/djigzo/conf/system/main.cf"
"/usr/share/djigzo/conf/system/master.cf"
"/usr/share/djigzo/conf/system/master-2.6.cf"
"/usr/share/djigzo/conf/system/sudoers"

"/usr/share/djigzo/james-2.3.1/apps/dummy.txt"
"/usr/share/djigzo/james-2.3.1/apps/james.sar"
"/usr/share/djigzo/james-2.3.1/tools/lib/msv-20020414.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/phoenix-tools.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/isorelax-20020414.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/xsdlib.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/qdox.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/qdox-1.1.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/excalibur-i18n-1.0.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/isorelax.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/relaxngDatatype.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/xsdlib-20020414.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/relaxngDatatype-20020414.jar"
"/usr/share/djigzo/james-2.3.1/tools/lib/msv.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/spice.LICENSE"
"/usr/share/djigzo/james-2.3.1/tools/lib/spice-configkit-1.1.2.jar"
"/usr/share/djigzo/james-2.3.1/NOTICE.txt"
"/usr/share/djigzo/james-2.3.1/bin/Wrapper.dll"
"/usr/share/djigzo/james-2.3.1/bin/run.sh"
"/usr/share/djigzo/james-2.3.1/bin/Wrapper.LICENSE"
"/usr/share/djigzo/james-2.3.1/bin/sendmail.py"
"/usr/share/djigzo/james-2.3.1/bin/Wrapper.exe"
"/usr/share/djigzo/james-2.3.1/bin/derby.properties"
"/usr/share/djigzo/james-2.3.1/bin/run.bat"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-io-1.1.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/spice-salt-0.8.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/phoenix-engine.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-monitor-2.1.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-cli-1.0.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-instrument-manager-1.0.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/spice-classman-1.0.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-extension-1.0a.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/excalibur-logger-2.1.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/mx4j-tools.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/jakarta-oro-2.0.8.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/mx4j-jmx.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/spice-loggerstore-0.5.jar"
"/usr/share/djigzo/james-2.3.1/bin/lib/spice.LICENSE"
"/usr/share/djigzo/james-2.3.1/bin/lib/mx4j.LICENSE"
"/usr/share/djigzo/james-2.3.1/bin/lib/spice-xmlpolicy-1.0.jar"
"/usr/share/djigzo/james-2.3.1/bin/phoenix-loader.jar"
"/usr/share/djigzo/james-2.3.1/bin/phoenix.sh"
"/usr/share/djigzo/james-2.3.1/bin/wrapper.jar"
"/usr/share/djigzo/james-2.3.1/ext/README.txt"
"/usr/share/djigzo/james-2.3.1/logs/readme.txt"
"/usr/share/djigzo/james-2.3.1/LICENSE.txt"
"/usr/share/djigzo/james-2.3.1/lib/commons-collections.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-io-1.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/phoenix-client.jar"
"/usr/share/djigzo/james-2.3.1/lib/avalon-framework-api-4.3.jar"
"/usr/share/djigzo/james-2.3.1/lib/spice-salt-0.8.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-thread-impl-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-thread-api-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/phoenix-engine.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-monitor-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-cli-1.0.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-instrument-manager-1.0.jar"
"/usr/share/djigzo/james-2.3.1/lib/spice-classman-1.0.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-instrument-api-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/concurrent-1.3.4.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-extension-1.0a.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-logger-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/mx4j-tools.jar"
"/usr/share/djigzo/james-2.3.1/lib/avalon-framework-impl-4.3.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-pool-impl-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/jakarta-oro-2.0.8.jar"
"/usr/share/djigzo/james-2.3.1/lib/mx4j-jmx.jar"
"/usr/share/djigzo/james-2.3.1/lib/spice-loggerstore-0.5.jar"
"/usr/share/djigzo/james-2.3.1/lib/avalon-logkit-2.1.jar"
"/usr/share/djigzo/james-2.3.1/lib/spice.LICENSE"
"/usr/share/djigzo/james-2.3.1/lib/mx4j.LICENSE"
"/usr/share/djigzo/james-2.3.1/lib/spice-xmlpolicy-1.0.jar"
"/usr/share/djigzo/james-2.3.1/lib/excalibur-pool-api-2.1.jar"
"/usr/share/djigzo/james-2.3.1/conf/wrapper.conf"
"/usr/share/djigzo/james-2.3.1/conf/kernel.xml"
"/usr/share/djigzo/james-2.3.1/conf/phoenix-service.xml"
"/usr/share/djigzo/james-2.3.1/UPGRADE.txt"
"/usr/share/djigzo/james-2.3.1/README.txt"

"/usr/share/djigzo/lib/ant/ant-contrib-1.0b3.jar"

"/usr/share/djigzo/lib/apache/commons-cli-1.1.jar"
"/usr/share/djigzo/lib/apache/commons-codec-1.3.jar"
"/usr/share/djigzo/lib/apache/commons-collections-3.2.1.jar"
"/usr/share/djigzo/lib/apache/commons-compress-1.1.jar"
"/usr/share/djigzo/lib/apache/commons-discovery-0.4.jar"
"/usr/share/djigzo/lib/apache/commons-httpclient-3.1.jar"
"/usr/share/djigzo/lib/apache/commons-io-1.4.jar"
"/usr/share/djigzo/lib/apache/commons-lang-2.4.jar"
"/usr/share/djigzo/lib/apache/commons-logging-1.1.1.jar"
"/usr/share/djigzo/lib/apache/jdkim.jar"
"/usr/share/djigzo/lib/apache/log4j-1.2.15.jar"
"/usr/share/djigzo/lib/apache/neethi-2.0.4.jar"
"/usr/share/djigzo/lib/apache/wss4j-1.5.8.jar"
"/usr/share/djigzo/lib/apache/xmlbeans-2.3.0.jar"
"/usr/share/djigzo/lib/apache/xml-resolver-1.2.jar"
"/usr/share/djigzo/lib/apache/XmlSchema-1.3.2.jar"
"/usr/share/djigzo/lib/apache/xmlsec-1.3.0.jar"

"/usr/share/djigzo/lib/aspectj/aspectjrt.jar"
"/usr/share/djigzo/lib/aspectj/aspectjweaver.jar"

"/usr/share/djigzo/lib/avalon/avalon-framework-api-4.3.jar"

"/usr/share/djigzo/lib/bouncycastle/bcmail.jar"
"/usr/share/djigzo/lib/bouncycastle/bcpkix.jar"
"/usr/share/djigzo/lib/bouncycastle/bcprov.jar"

"/usr/share/djigzo/lib/charset/djigzo-charset.jar"

"/usr/share/djigzo/lib/cxf/asm-2.2.3.jar"
"/usr/share/djigzo/lib/cxf/cxf-2.1.10.jar"
"/usr/share/djigzo/lib/cxf/FastInfoset-1.2.2.jar"

"/usr/share/djigzo/lib/database/postgresql.jdbc4.jar"

"/usr/share/djigzo/lib/djigzo/fetchmail-config.jar"

"/usr/share/djigzo/lib/freemarker/freemarker.jar"

"/usr/share/djigzo/lib/geronimo/geronimo-annotation_1.0_spec-1.1.1.jar"
"/usr/share/djigzo/lib/geronimo/geronimo-stax-api_1.0_spec-1.0.1.jar"
"/usr/share/djigzo/lib/geronimo/geronimo-ws-metadata_2.0_spec-1.1.2.jar"

"/usr/share/djigzo/lib/hibernate/antlr.jar"
"/usr/share/djigzo/lib/hibernate/c3p0.jar"
"/usr/share/djigzo/lib/hibernate/dom4j.jar"
"/usr/share/djigzo/lib/hibernate/ejb3-persistence.jar"
"/usr/share/djigzo/lib/hibernate/hibernate3.jar"
"/usr/share/djigzo/lib/hibernate/hibernate-annotations.jar"
"/usr/share/djigzo/lib/hibernate/hibernate-commons-annotations.jar"
"/usr/share/djigzo/lib/hibernate/jta.jar"

"/usr/share/djigzo/lib/icu/icu4j-core.jar"

"/usr/share/djigzo/lib/itext/iText.jar"
"/usr/share/djigzo/lib/itext/iTextAsian.jar"
"/usr/share/djigzo/lib/itext/iTextAsianCmaps.jar"

"/usr/share/djigzo/lib/james/cornerstone-store-api-2.1.jar"

"/usr/share/djigzo/lib/javassist/javassist.jar"

"/usr/share/djigzo/lib/jaxb/jaxb-impl-2.1.7.jar"
"/usr/share/djigzo/lib/jaxb/jaxb-api-2.1.jar"

"/usr/share/djigzo/lib/jaxws/jaxws-api-2.1-1.jar"

"/usr/share/djigzo/lib/jetty/jetty-util-6.1.21.jar"
"/usr/share/djigzo/lib/jetty/jetty-6.1.21.jar"

"/usr/share/djigzo/lib/jeval/jeval.jar"

"/usr/share/djigzo/lib/jline/jline.jar"

"/usr/share/djigzo/lib/jutf7/jutf7-1.0.0.jar"

"/usr/share/djigzo/lib/mail/dsn.jar"
"/usr/share/djigzo/lib/mail/mail.jar"

"/usr/share/djigzo/lib/opencsv/opencsv-2.2.jar"

"/usr/share/djigzo/lib/opensymphony/quartz-1.6.4.jar"


"/usr/share/djigzo/lib/samba/jcifs-1.3.15.jar"

"/usr/share/djigzo/lib/servlet/servlet-api.jar"

"/usr/share/djigzo/lib/slf4j/slf4j-api.jar"
"/usr/share/djigzo/lib/slf4j/slf4j-log4j.jar"

"/usr/share/djigzo/lib/spring/cglib-nodep-2.1_3.jar"
"/usr/share/djigzo/lib/spring/spring-2.5.6.jar"

"/usr/share/djigzo/lib/subetha/subethasmtp-3.1.2.jar"

"/usr/share/djigzo/lib/tagsoup/tagsoup-1.2-p1.jar"

"/usr/share/djigzo/lib/tyrex/tyrex-1.0.3.jar"

"/usr/share/djigzo/lib/woodstox/wstx-asl-3.2.4.jar"

"/usr/share/djigzo/lib/wsdl4j/wsdl4j-1.6.1.jar"

"/usr/share/djigzo/lib/lib.d/README.txt"

"/usr/share/djigzo/licenses/agpl.txt"
"/usr/share/djigzo/licenses/antlr.txt"
"/usr/share/djigzo/licenses/apache-v2.txt"
"/usr/share/djigzo/licenses/bouncycastle.txt"
"/usr/share/djigzo/licenses/bsd.txt"
"/usr/share/djigzo/licenses/cddl.txt"
"/usr/share/djigzo/licenses/cpl.txt"
"/usr/share/djigzo/licenses/dom4j.txt"
"/usr/share/djigzo/licenses/epl.txt"
"/usr/share/djigzo/licenses/freemarker.txt"
"/usr/share/djigzo/licenses/icu4j-license.html"
"/usr/share/djigzo/licenses/mx4j.txt"
"/usr/share/djigzo/licenses/objectweb.txt"
"/usr/share/djigzo/licenses/slf4j.txt"
"/usr/share/djigzo/licenses/spice.txt"
"/usr/share/djigzo/licenses/tyrex.txt"

"/usr/share/djigzo/resources/certificates/windows-xp-all-intermediates.p7b"
"/usr/share/djigzo/resources/certificates/windows-xp-all-roots.p7b"

"/usr/share/djigzo/resources/fonts/dejavu-fonts-ttf-2.33.tar.bz2"
"/usr/share/djigzo/resources/fonts/gw-fonts-ttf_1.0.orig.tar.gz"
"/usr/share/djigzo/resources/fonts/README.txt"

"/usr/share/djigzo/resources/templates/blackberry-smime-adapter.ftl"
"/usr/share/djigzo/resources/templates/dlp.block.ftl"
"/usr/share/djigzo/resources/templates/dlp.delete.notification.ftl"
"/usr/share/djigzo/resources/templates/dlp.error.ftl"
"/usr/share/djigzo/resources/templates/dlp.expire.notification.ftl"
"/usr/share/djigzo/resources/templates/dlp.quarantine.ftl"
"/usr/share/djigzo/resources/templates/dlp.release.notification.ftl"
"/usr/share/djigzo/resources/templates/dlp.warning.ftl"
"/usr/share/djigzo/resources/templates/encrypted-pdf-otp-invite.ftl"
"/usr/share/djigzo/resources/templates/encrypted-pdf-otp.ftl"
"/usr/share/djigzo/resources/templates/encrypted-pdf-sms.ftl"
"/usr/share/djigzo/resources/templates/encrypted-pdf.ftl"
"/usr/share/djigzo/resources/templates/encryption-failed-notification.ftl"
"/usr/share/djigzo/resources/templates/encryption-notification.ftl"
"/usr/share/djigzo/resources/templates/mail-pfx.ftl"
"/usr/share/djigzo/resources/templates/passwords-notification.ftl"
"/usr/share/djigzo/resources/templates/relay-bounce.ftl"
"/usr/share/djigzo/resources/templates/sms.ftl"
"/usr/share/djigzo/resources/templates/sms-pfx.ftl"

"/usr/share/djigzo/scripts/backup.sh"
%config "/usr/share/djigzo/scripts/backup_files"
"/usr/share/djigzo/scripts/check_backend.sh"
"/usr/share/djigzo/scripts/copy-jce-policy.sh"
"/usr/share/djigzo/scripts/copy-postfix-main-config.sh"
"/usr/share/djigzo/scripts/daemonized-restart.sh"
"/usr/share/djigzo/scripts/djigzo"
"/usr/share/djigzo/scripts/dobackup.sh"
"/usr/share/djigzo/scripts/docopy-jce-policy.sh"
"/usr/share/djigzo/scripts/docopy-postfix-main-config.sh"
"/usr/share/djigzo/scripts/dofetchmail-configure.sh"
"/usr/share/djigzo/scripts/dopostfix.sh"
"/usr/share/djigzo/scripts/dorestart.sh"
"/usr/share/djigzo/scripts/dosmtp-client-passwd-config.sh"
"/usr/share/djigzo/scripts/fetchmail-configure.sh"
"/usr/share/djigzo/scripts/install_unicode_font.sh"
"/usr/share/djigzo/scripts/postfix.sh"
"/usr/share/djigzo/scripts/post-install.sh"
"/usr/share/djigzo/scripts/restart.sh"
"/usr/share/djigzo/scripts/smtp-client-passwd-config.sh"

"/usr/share/djigzo/wrapper/build.xml"
"/usr/share/djigzo/wrapper/wrapper.dist.Linux.amd64.properties"
"/usr/share/djigzo/wrapper/wrapper.dist.Linux.i386.properties"
"/usr/share/djigzo/wrapper/wrapper.dist.Mac OS X.i386.properties"
"/usr/share/djigzo/wrapper/wrapper-linux-x86-32-3.4.1.tar.gz"
"/usr/share/djigzo/wrapper/wrapper-linux-x86-64-3.4.1.tar.gz"
"/usr/share/djigzo/wrapper/wrapper-macosx-universal-32-3.4.1.tar.gz"

"/usr/share/djigzo/build.xml"
"/usr/share/djigzo/djigzo.jar"
"/usr/share/djigzo/LIBRARIES.txt"
"/usr/share/djigzo/LICENSE.txt"
"/usr/share/djigzo/README.txt"
"/usr/share/djigzo/start-djigzo.sh"
