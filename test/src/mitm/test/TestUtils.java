/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.DefaultPKISecurityServicesFactory;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.PKISecurityServicesFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.validator.PKISMIMETrustCheckCertificateValidatorFactory;
import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import mitm.common.security.certpath.CertStoreTrustAnchorBuilder;
import mitm.common.security.certpath.CertificatePathBuilderFactory;
import mitm.common.security.certpath.DefaultCertificatePathBuilderFactory;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.crl.CRLPathBuilderFactory;
import mitm.common.security.crl.CRLUtils;
import mitm.common.security.crl.DefaultCRLPathBuilderFactory;
import mitm.common.security.crl.PKIXRevocationChecker;
import mitm.common.security.crl.RevocationChecker;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.time.DateUtils;

public class TestUtils
{    
	
    /*
     * Calculate the number of mitm*.tmp files in the temp folder. This is used to detect if we have any 
     * temp file leakage.
     */
    public static int getTempFileCount(final String prefix, final String postfix)
    {
        String[] files = SystemUtils.getJavaIoTmpDir().list(new FilenameFilter()
        {
            @Override
            public boolean accept(File file, String s)
            {
                return StringUtils.startsWith(s, prefix) && 
                        StringUtils.endsWith(s, postfix);
            }
        });
        
        return files != null ? files.length : 0;
    }
    
    public static boolean isEqual(MimeMessage message1, MimeMessage message2)
    throws IOException, MessagingException
    {
        ByteArrayOutputStream bos1 = new ByteArrayOutputStream();

        MailUtils.writeMessage(message1, bos1);

        ByteArrayOutputStream bos2 = new ByteArrayOutputStream();

        MailUtils.writeMessage(message2, bos2);
        
        return Arrays.equals(bos2.toByteArray(), bos1.toByteArray());
    }
    
    public static void saveMessages(File tempDir, String baseName, List<MimeMessage> messages)
    throws IOException, MessagingException
    {
        for (int i = 0; i < messages.size(); i++)
        {
          File file = new File(tempDir, baseName + "_" + i + ".eml");
            
          MailUtils.writeMessage(messages.get(i), file);
        }
    }

    public static void saveMessages(File tempDir, String baseName, MimeMessage... messages)
    throws IOException, MessagingException
    {
        for (int i = 0; i < messages.length; i++)
        {
          File file = new File(tempDir, baseName + "_" + i + ".eml");
            
          MailUtils.writeMessage(messages[i], file);
        }
    }

    public static X509Certificate loadCertificate(File file)
    throws CertificateException, NoSuchProviderException, FileNotFoundException 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        if (certificates != null && certificates.size() > 0) {
            return (X509Certificate)certificates.iterator().next();
        }
        
        return null;
    }
    
    public static X509Certificate loadCertificate(String filename)
    throws CertificateException, NoSuchProviderException, FileNotFoundException 
    {
        File file = new File(filename);
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        if (certificates != null && certificates.size() > 0) {
            return (X509Certificate)certificates.iterator().next();
        }
        
        return null;
    }
    
    public static X509CRL loadX509CRL(File file) 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, FileNotFoundException
    {
        Collection<? extends CRL> crls = CRLUtils.readCRLs(file);
        
        if (crls.size() > 0) {
            return (X509CRL) crls.iterator().next();
        }
        
        return null;
    }
    
    public static Date parseDate(String date) 
    throws ParseException 
    {
    	return new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z").parse(date);
    }
    
    public static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }
    
    public static PKISecurityServices createDefaultPKISecurityServices(KeyAndCertStore keyAndCertStore,
    		X509CertStoreExt rootStore, X509CRLStoreExt crlStore) 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        TrustAnchorBuilder trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStore, 30 * DateUtils.MILLIS_PER_SECOND); 
        
        CertificatePathBuilderFactory certificatePathBuilderFactory = new DefaultCertificatePathBuilderFactory(trustAnchorBuilder, keyAndCertStore);
        
        Encryptor encryptor = new PasswordBasedEncryptor("djigzo");

        RevocationChecker revocationChecker = new PKIXRevocationChecker(crlStore);
        
        CRLPathBuilderFactory crlPathBuilderFactory = new DefaultCRLPathBuilderFactory(certificatePathBuilderFactory);
        
        PKITrustCheckCertificateValidatorFactory validatorFactory = 
        		new PKISMIMETrustCheckCertificateValidatorFactory(certificatePathBuilderFactory, revocationChecker, null);
        
        PKISecurityServicesFactory pkiSecurityServicesFactory = new DefaultPKISecurityServicesFactory(
        		keyAndCertStore, rootStore, crlStore, trustAnchorBuilder, revocationChecker, encryptor, 
        		certificatePathBuilderFactory, crlPathBuilderFactory, validatorFactory);
        
        return pkiSecurityServicesFactory.createPKISecurityServices();
    }
}
