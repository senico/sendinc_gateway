/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.xml;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import mitm.application.djigzo.xml.XMLStore;
import mitm.application.djigzo.xml.XMLStoreFactory;
import mitm.application.djigzo.xml.XMLUser;

import org.junit.Test;

public class XMLStoreFactoryTest 
{
    private static final File tempDir = new File("test/tmp");
	
	@Test
	public void testMarshall() 
	throws JAXBException, IOException
	{
		XMLStore xmlStore = new XMLStore();
		
		XMLUser user = new XMLUser();
		
		user.email = "m.brinkers@pobox.com";
		
		xmlStore.getUsers().add(user);
		
		File xmlFile = new File(tempDir, "testMarshall.xml");
		
		FileOutputStream fos = new FileOutputStream(xmlFile);
		
		XMLStoreFactory.marshall(xmlStore, fos);
		
		fos.close();
		
		FileInputStream fis = new FileInputStream(xmlFile);
		
		XMLStore xmlStore2 = XMLStoreFactory.unmarshall(fis);
		
		assertEquals(1, xmlStore2.getUsers().size());
	}
}
