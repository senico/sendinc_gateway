/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.xml.bind.JAXBException;

import mitm.application.djigzo.xml.XMLProperty;
import mitm.application.djigzo.xml.XMLStore;
import mitm.application.djigzo.xml.XMLStoreFactory;
import mitm.application.djigzo.xml.XMLUser;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

public class GenerateTestData 
{
    private static final File tempDir = new File("test/tmp");
	
    private static final String[] TLDs = {".com", ".nl", ".net", ".org"};
    
    private static final Random random = new Random();
    
    private void addUser(XMLStore xmlStore, String email)
    {
		XMLUser user = new XMLUser();
		
		user.email = email;
		
		XMLProperty property = new XMLProperty();
		
		property.setName("user.smsPhoneNumber");
		property.setValue(Long.toString(System.currentTimeMillis()));
		
        user.getProperties().add(property);
        
        property = new XMLProperty();
        
        property.setName("user.password");
        property.setValue(Long.toString(System.currentTimeMillis()));
        property.setEncrypted(true);
        
        user.getProperties().add(property);
		
		xmlStore.getUsers().add(user);    	
    }
    
    private String generateEmail() {
    	return RandomStringUtils.randomAlphabetic(15) + "@" + 
    			RandomStringUtils.randomAlphabetic(10) + TLDs[random.nextInt(TLDs.length)];
    }

    private void addDomain(XMLStore xmlStore, String domain)
    {
        XMLDomain xmlDomain = new XMLDomain();
        
        xmlDomain.setName(domain);
        
        XMLProperty property = new XMLProperty();
        
        property.setName("user.password");
        property.setValue("test");
        property.setEncrypted(true);

        xmlDomain.getProperties().add(property);

        property = new XMLProperty();

        property.setName("user.locality");
        property.setValue("internal");

        xmlDomain.getProperties().add(property);
        
        xmlStore.getDomains().add(xmlDomain);      
    }
    
	@Test
	public void generateTestData() 
	throws JAXBException, IOException
	{
		XMLStore xmlStore = new XMLStore();
		
		for (int i = 0; i < 5; i++) {
			addUser(xmlStore, generateEmail());
		}
		
        addDomain(xmlStore, "example.com");
        addDomain(xmlStore, "sub.example.com");
		
		File xmlFile = new File(tempDir, "testData.xml");
		
		FileOutputStream fos = new FileOutputStream(xmlFile);
		
		XMLStoreFactory.marshall(xmlStore, fos);
		
		fos.close();
	}
}
