/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import mitm.application.djigzo.james.mock.MockMail;

import org.junit.Test;


public class MailAttributesUtilsTest
{
    @Test
    public void testGetAttributeValue()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", "value");
        
        assertEquals("value", MailAttributesUtils.getAttributeValue(mail, "name", null, String.class));
        assertEquals("default", MailAttributesUtils.getAttributeValue(mail, "other", "default", String.class));
        assertNull(MailAttributesUtils.getAttributeValue(mail, "other", null, String.class));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeValueInvalid()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", "value");
        
        MailAttributesUtils.getAttributeValue(mail, "name", null, Integer.class);
    }    
    
    @Test
    public void testGetAttributeAsCollection()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", "value");
        
        Collection<String> strings = MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
        
        assertEquals(1, strings.size());
        assertEquals("value", strings.iterator().next());
        
        mail.setAttribute("name", new String[]{"a", "b", null});
        
        strings = MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
        
        assertEquals(3, strings.size());
        
        Iterator<String> strIt = strings.iterator();
        assertEquals("a", strIt.next());
        assertEquals("b", strIt.next());
        assertEquals(null, strIt.next());

        mail.setAttribute("name", new Object[]{"a", 1, null});
        
        Collection<Object> objects = MailAttributesUtils.getAttributeAsList(mail, "name", Object.class);  

        assertEquals(3, objects.size());
        
        Iterator<Object> objIt = objects.iterator();
        assertEquals("a", objIt.next());
        assertEquals(1, objIt.next());
        assertEquals(null, objIt.next());
        
        ArrayList<String> list = new ArrayList<String>();
        
        list.add("a");
        list.add(null);
        list.add("b");
        
        mail.setAttribute("name", list);
        
        strings = MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
        
        assertEquals(3, strings.size());
        
        strIt = strings.iterator();
        assertEquals("a", strIt.next());
        assertEquals(null, strIt.next());
        assertEquals("b", strIt.next());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeAsCollectionInvalid1()
    {
        MockMail mail = new MockMail();

        ArrayList<Object> list = new ArrayList<Object>();
        
        list.add("a");
        list.add(1);
        
        mail.setAttribute("name", list);
                
        MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeAsCollectionInvalid2()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", new Object[]{"a", 1});
        
        MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
    }    
 
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeAsCollectionInvalid3()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", 1);
        
        MailAttributesUtils.getAttributeAsList(mail, "name", String.class);  
    }
    
    @Test
    public void testGetAttributeAsArray()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", "value");
        
        String[] strings = MailAttributesUtils.getAttributeAsArray(mail, "name", String.class);  
        
        assertEquals(1, strings.length);
        assertEquals("value", strings[0]);
        
        mail.setAttribute("name", new String[]{"a", "b", null});
        
        strings = MailAttributesUtils.getAttributeAsArray(mail, "name", String.class);  
        
        assertEquals(3, strings.length);
        
        assertEquals("a", strings[0]);
        assertEquals("b", strings[1]);
        assertEquals(null, strings[2]);

        mail.setAttribute("name", new Object[]{"a", 1, null});
        
        Object[] objects = MailAttributesUtils.getAttributeAsArray(mail, "name", Object.class);  

        assertEquals(3, objects.length);
        
        assertEquals("a", objects[0]);
        assertEquals(1, objects[1]);
        assertEquals(null, objects[2]);
        
        ArrayList<String> list = new ArrayList<String>();
        
        list.add("a");
        list.add(null);
        list.add("b");
        
        mail.setAttribute("name", list);
        
        strings = MailAttributesUtils.getAttributeAsArray(mail, "name", String.class);  
        
        assertEquals(3, strings.length);
        
        assertEquals("a", strings[0]);
        assertEquals(null, strings[1]);
        assertEquals("b", strings[2]);
    }        
    
    @Test
    public void testGetBoolean()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", true);
        
        assertTrue(MailAttributesUtils.getBoolean(mail, "name", null));
        
        mail.setAttribute("name", false);
        
        assertFalse(MailAttributesUtils.getBoolean(mail, "name", null));  

        // check default        
        assertFalse(MailAttributesUtils.getBoolean(mail, "other", false));
        assertTrue(MailAttributesUtils.getBoolean(mail, "other", true));
        assertNull(MailAttributesUtils.getBoolean(mail, "other", null));

        // check string conversion
        mail.setAttribute("name", "true");
        
        assertTrue(MailAttributesUtils.getBoolean(mail, "name", null));

        mail.setAttribute("name", "yes");
        
        assertTrue(MailAttributesUtils.getBoolean(mail, "name", null));
        
        mail.setAttribute("name", "false");
        
        assertFalse(MailAttributesUtils.getBoolean(mail, "name", null));

        mail.setAttribute("name", "no");
        
        assertFalse(MailAttributesUtils.getBoolean(mail, "name", null));        
    }    

    @Test
    public void testGetInteger()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", 1);
        
        assertEquals((Integer)1, MailAttributesUtils.getInteger(mail, "name", null));
        
        // check default        
        assertEquals((Integer)2, MailAttributesUtils.getInteger(mail, "other", 2));
        assertNull(MailAttributesUtils.getInteger(mail, "other", null));

        // check string conversion
        mail.setAttribute("name", "3");
        
        assertEquals((Integer)3, MailAttributesUtils.getInteger(mail, "name", null));
    }    

    @Test
    public void testGetLong()
    {
        MockMail mail = new MockMail();
        
        mail.setAttribute("name", 1L);
        
        assertEquals((Long)1L, MailAttributesUtils.getLong(mail, "name", null));
        
        // check default        
        assertEquals((Long)2L, MailAttributesUtils.getLong(mail, "other", 2L));
        assertNull(MailAttributesUtils.getLong(mail, "other", null));

        // check string conversion
        mail.setAttribute("name", "3");
        
        assertEquals((Long)3L, MailAttributesUtils.getLong(mail, "name", null));
        
        // check Integer conversion
        mail.setAttribute("name", (Integer) 4);
        
        assertEquals((Long)4L, MailAttributesUtils.getLong(mail, "name", null));
    }    

    @Test
    public void testBinary()
    {
        MockMail mail = new MockMail();
        
        MailAttributesUtils.setBinary(mail, "name", new byte[]{1,2,3});
        
        assertTrue(Arrays.equals(new byte[]{1,2,3}, MailAttributesUtils.getBinary(mail, "name", null)));

        // test default
        assertTrue(Arrays.equals(new byte[]{4,5,6}, MailAttributesUtils.getBinary(mail, "other", new byte[]{4,5,6})));
    }
}
