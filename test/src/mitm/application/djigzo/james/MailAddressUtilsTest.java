/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;

import mitm.application.djigzo.james.MailAddressUtils;

import org.apache.james.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Test;

public class MailAddressUtilsTest 
{
	@Test
	public void testGetRecipients() 
	throws ParseException
	{
		Collection<MailAddress> recipients = new LinkedList<MailAddress>();
		
		recipients.add(new MailAddress("test1@example.com"));
		recipients.add(new MailAddress("test2@example.com"));
		
		Mail mail = new MailImpl("test", null, recipients);
		
		Collection<MailAddress> result = MailAddressUtils.getRecipients(mail);
		
		assertEquals(result, recipients);
	}

    @Test
    public void testFromAddressArrayToMailAddressList() 
    throws ParseException
    {
        List<MailAddress> addresses = MailAddressUtils.fromAddressArrayToMailAddressList(new InternetAddress("test1@example.com"), 
                new InternetAddress("test2@example.com"));
        
        assertEquals(2, addresses.size());
        assertEquals("test1@example.com", addresses.get(0).toInternetAddress().getAddress());
        assertEquals("test2@example.com", addresses.get(1).toInternetAddress().getAddress());
    }
    
    @Test
    public void testToMailAddressList() 
    throws ParseException
    {
        Set<String> emails = new HashSet<String>();
        
        emails.add("test1@example.com");
        emails.add("test2@example.com");
        
        List<MailAddress> addresses = MailAddressUtils.toMailAddressList(emails);
        
        assertEquals(2, addresses.size());
        assertEquals("test1@example.com", addresses.get(0).toInternetAddress().getAddress());
        assertEquals("test2@example.com", addresses.get(1).toInternetAddress().getAddress());
    }
}
