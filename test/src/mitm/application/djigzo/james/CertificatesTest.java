/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collections;

import mitm.common.util.MiscStringUtils;
import mitm.test.TestUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.SerializationUtils;
import org.junit.Test;

public class CertificatesTest
{
    private static String BASE64_ENCODED_CERTS = 
        "rO0ABXNyACptaXRtLmFwcGxpY2F0aW9uLmRqaWd6by5qYW1lcy5DZXJ0aWZpY2F0ZXO86FqtfXiR" +
        "6AMAAUwADGNlcnRpZmljYXRlc3QAFkxqYXZhL3V0aWwvQ29sbGVjdGlvbjt4cHoAAANqvOharX14" +
        "kegAAAABAAADWjCCA1YwggK/oAMCAQICDlLcAAEAArK7dHSUAfR1MA0GCSqGSIb3DQEBBAUAMIG8" +
        "MQswCQYDVQQGEwJERTEQMA4GA1UECBMHSGFtYnVyZzEQMA4GA1UEBxMHSGFtYnVyZzE6MDgGA1UE" +
        "ChMxVEMgVHJ1c3RDZW50ZXIgZm9yIFNlY3VyaXR5IGluIERhdGEgTmV0d29ya3MgR21iSDEiMCAG" +
        "A1UECxMZVEMgVHJ1c3RDZW50ZXIgQ2xhc3MgMSBDQTEpMCcGCSqGSIb3DQEJARYaY2VydGlmaWNh" +
        "dGVAdHJ1c3RjZW50ZXIuZGUwHhcNMDUwOTI0MjAxOTQzWhcNMDYwOTI0MjAxOTQzWjBNMQswCQYD" +
        "VQQGEwJOTDEZMBcGA1UEAxMQTWFydGlqbiBCcmlua2VyczEjMCEGCSqGSIb3DQEJARYUbS5icmlu" +
        "a2Vyc0Bwb2JveC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBANhdVm1jp1DDyeF/swrU" +
        "tCxk+q1KkSoJZ2+ElTVVV+EWxljdqd0K4Tth68BmjwVlrxhdpmC90tgdU+f/J2jB2Wa5K5UD2XuB" +
        "MGKNywSMkgCRUHWZwg7OvMQcFyYpakaAnzR3lfOoVUJFOKjd4OgoHZaw8qFGhT8wC8kb+/T/39gr" +
        "AgMBAAGjgcgwgcUwDAYDVR0TAQH/BAIwADAOBgNVHQ8BAf8EBAMCBeAwMwYJYIZIAYb4QgEIBCYW" +
        "JGh0dHA6Ly93d3cudHJ1c3RjZW50ZXIuZGUvZ3VpZGVsaW5lczARBglghkgBhvhCAQEEBAMCBaAw" +
        "XQYJYIZIAYb4QgEDBFAWTmh0dHBzOi8vd3d3LnRydXN0Y2VudGVyLmRlL2NnaS1iaW4vY2hlY2st" +
        "cmV2LmNnaS81MkRDMDAwMTAwMDJCMkJCNzQ3NDk0MDFGNDc1PzANBgkqhkiG9w0BAQQFAAOBgQCL" +
        "G+rS9ZwNK2cxH3/pUKZISMrXtHRphUq6urd+E/dkJfO11utxHjC6yWZdP9ewdY2p3LYQG0s1f4fN" +
        "l32JoyNiPeSfOidWcheh5PODd+0AO5gIIb/x5YYy+cfJEOyoDkOipm3CnytrG2JkYGd6u/+hAynl" +
        "yyLvf3OArMKis1nZW3g=";
            
    @Test
    public void testSerialize()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(
            "test/resources/testdata/certificates/testcertificate.cer"));

        Certificates certificates = new Certificates(Collections.singleton(certificate));
        
        assertEquals(1, certificates.getCertificates().size());
        
        byte[] serialized = SerializationUtils.serialize(certificates);
        
        Certificates deserialized = (Certificates) SerializationUtils.deserialize(serialized);
        
        assertEquals(certificates.getCertificates(), deserialized.getCertificates());
    }
    
    @Test
    public void testDeserialize()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(
            "test/resources/testdata/certificates/testcertificate.cer"));
        
        Certificates certificates = (Certificates) SerializationUtils.deserialize(Base64.decodeBase64(
                MiscStringUtils.toAsciiBytes(BASE64_ENCODED_CERTS)));
        
        assertEquals(1, certificates.getCertificates().size());
        assertEquals(certificate, certificates.getCertificates().iterator().next());
    }
}
