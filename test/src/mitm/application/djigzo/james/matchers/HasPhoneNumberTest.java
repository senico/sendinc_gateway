/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.PhoneNumbers;
import mitm.application.djigzo.james.matchers.HasPhoneNumber;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.MailSession;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;

public class HasPhoneNumberTest
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        addUser();
    }

    private static void addUser()
    throws AddressException, CertificateException, NoSuchProviderException, FileNotFoundException, DatabaseException 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        addUserAction(session, "test1@example.com", "1000000000");
                        addUserAction(session, "test2@example.com", "2000000000");
                        addUserAction(session, "test3@example.com", null);
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
     
    public static void addUserAction(Session session, String email, String phoneNumber)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
     
            UserProperties userProperties = user.getUserPreferences().getProperties();  
            
            if (userProperties != null) {
                userProperties.setSMSPhoneNumber(phoneNumber);
            }
            
            userWorkflow.makePersistent(user);            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }

    @Test
    public void testSingleMatchCaseInsensitive()
    throws MessagingException 
    {
        HasPhoneNumber matcher = new HasPhoneNumber();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@EXAMPLE.com")));
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        PhoneNumbers phoneNumbers = attributes.getPhoneNumbers();
        
        assertNotNull(phoneNumbers);
        assertEquals(1, phoneNumbers.keySet().size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("1000000000", phoneNumbers.get("test1@example.com").getNumber());
    }
    
    @Test
    public void testSingleMatch()
    throws MessagingException 
    {
        HasPhoneNumber matcher = new HasPhoneNumber();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        PhoneNumbers phoneNumbers = attributes.getPhoneNumbers();
        
        assertNotNull(phoneNumbers);
        assertEquals(1, phoneNumbers.keySet().size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("1000000000", phoneNumbers.get("test1@example.com").getNumber());
    }

    @Test
    public void testMultipleMatch()
    throws MessagingException 
    {
        HasPhoneNumber matcher = new HasPhoneNumber();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        recipients.add(new MailAddress("test3@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        PhoneNumbers phoneNumbers = attributes.getPhoneNumbers();
        
        assertNotNull(phoneNumbers);
        assertEquals(2, phoneNumbers.keySet().size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("1000000000", phoneNumbers.get("test1@example.com").getNumber());
        assertNotNull(phoneNumbers.get("test2@example.com"));
        assertEquals("2000000000", phoneNumbers.get("test2@example.com").getNumber());
    }

    @Test
    public void testNoMatchMissingPhoneNumber()
    throws MessagingException 
    {
        HasPhoneNumber matcher = new HasPhoneNumber();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test3@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }    
    
    @Test
    public void testNoMatchUnknownUser()
    throws MessagingException 
    {
        HasPhoneNumber matcher = new HasPhoneNumber();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("unknown@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
}
