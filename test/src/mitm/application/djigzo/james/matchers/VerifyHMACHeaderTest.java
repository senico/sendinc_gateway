/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Matcher;
import org.junit.BeforeClass;
import org.junit.Test;


public class VerifyHMACHeaderTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    @Test
    public void verifyHeaderTest()
    throws Exception
    {
        Matcher matcher = new VerifyHMACHeader();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, X-HMAC, test, 123456", 
                mailetContext);
        
        matcher.init(matcherConfig);
        
        MockMail mail = new MockMail();

        MimeMessage message = loadMessage("hmac-header.eml");
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }

    @Test
    public void verifyMismatchValue()
    throws Exception
    {
        Matcher matcher = new VerifyHMACHeader();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, X-HMAC, ***, 123456", 
                mailetContext);
        
        matcher.init(matcherConfig);
        
        MockMail mail = new MockMail();

        MimeMessage message = loadMessage("hmac-header.eml");
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void verifyMismatchSecret()
    throws Exception
    {
        Matcher matcher = new VerifyHMACHeader();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, X-HMAC, test, abc", 
                mailetContext);
        
        matcher.init(matcherConfig);
        
        MockMail mail = new MockMail();

        MimeMessage message = loadMessage("hmac-header.eml");
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void verifyMissingHeader()
    throws Exception
    {
        Matcher matcher = new VerifyHMACHeader();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, X-HMAC, test, 123456", 
                mailetContext);
        
        matcher.init(matcherConfig);
        
        MockMail mail = new MockMail();

        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }    
}
