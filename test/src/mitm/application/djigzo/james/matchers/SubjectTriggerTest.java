/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.BeforeClass;
import org.junit.Test;

public class SubjectTriggerTest
{
    private static MockMailetContext mailetContext;
    
    private final static File testBase = new File("test/resources/testdata");
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
    }
    
    @Test
    public void testSubjectTriggerUnknownEncoding()
    throws Exception 
    {
        SubjectTrigger matcher = new SubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/unknown-content-transfer-encoding.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("simple message", mail.getMessage().getSubject());
        
        MailUtils.validateMessage(mail.getMessage());
    }
    
    @Test
    public void testSubjectTrigger()
    throws Exception 
    {
        SubjectTrigger matcher = new SubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,(?i)\\[\\s*encrypt\\s*\\]", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject("some subject [  encrypt]12234");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("some subject 12234", mail.getMessage().getSubject());
        
        MailUtils.validateMessage(mail.getMessage());
    }

    @Test
    public void testSubjectTriggerEndPattern()
    throws Exception 
    {
        SubjectTrigger matcher = new SubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,(?i)\\[\\s*encrypt\\s*\\]", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject("test[encrypt]");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        
        MailUtils.validateMessage(mail.getMessage());
    }
    
    @Test
    public void testSubjectTriggerNoMatch()
    throws Exception 
    {
        SubjectTrigger matcher = new SubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,(?i)\\[\\s*encrypt\\s*\\]", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject("some subject [ xx ]12234");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);

        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("some subject [ xx ]12234", mail.getMessage().getSubject());
        
        MailUtils.validateMessage(mail.getMessage());
    }
}
