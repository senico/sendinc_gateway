/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailSession;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SenderSubjectTriggerTest 
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
    }

    @Before
    public void setup()
    throws Exception
    {
    	AutoTransactDelegator.createProxy().deleteAllUsers();
    }

    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
    	@StartTransaction
    	public void addUserWithTrigger(String email, String trigger, boolean regExpr, boolean removePattern) 
    	throws AddressException, HierarchicalPropertiesException
    	{
    		UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

    		User user = userWorkflow.addUser(email);

    		UserProperties userProperties = user.getUserPreferences().getProperties();

    		userProperties.setSubjectTrigger(trigger);
    		userProperties.setSubjectTriggerIsRegExpr(regExpr);
    		userProperties.setSubjectTriggerRemovePattern(removePattern);

    		userWorkflow.makePersistent(user);            
    	}
    }
    
    private void addUser(final String email, final String trigger, final boolean regExpr, final boolean removePattern) 
    throws AddressException, HierarchicalPropertiesException, ProxyFactoryException, NoSuchMethodException 
    {
    	ExtendedAutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class).addUserWithTrigger(email, 
    			trigger, regExpr, removePattern);
    }
         
    private Mail createMail(String from, String subject, String... recipients) 
    throws MessagingException
    {
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        
        Set<MailAddress> recipientAddresses = new HashSet<MailAddress>();
        
        for (String recipient : recipients)
        {
        	recipientAddresses.add(new MailAddress(recipient));
        }
        
        mail.setRecipients(recipientAddresses);
        
        return mail;
    }

    /*
     * Test nested triggers max recursion depth.
     * 
     * example:
     * 
     * to test that enencryptcrypt does not result in encrypt after remove 
     */
    @Test
    public void testSubjectTriggerNestedMaxRecursion()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "encrypt", false, true);
        
        Mail mail = createMail("test@example.com", "test enenenencryptcryptcryptcrypt 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals(SubjectTrigger.MAX_SUBJECT_RECURSIVE_DEPTH_REACHED, mail.getMessage().getSubject());
    }
    
    /*
     * Test nested triggers.
     * 
     * example:
     * 
     * to test that enencryptcrypt does not result in encrypt after remove 
     */
    @Test
    public void testSubjectTriggerNested()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "encrypt", false, true);
        
        Mail mail = createMail("test@example.com", "test enencryptcrypt 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());
        
        // test whether the original subject mail attribute is set and that it's correct
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        assertEquals("test enencryptcrypt 123", mailAttributes.getOriginalSubject());
    }
    
    @Test
    public void testSubjectNullOriginator()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true", mailetContext);
        
        matcher.init(matcherConfig);
        
        /*
         * A null originator should not result in a NPE
         */
        Mail mail = createMail(null, "", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testSubjectTriggerRegExprRemoveMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "(?i)\\[\\s*encrypt\\s*\\]", true, true);
        
        Mail mail = createMail("test@example.com", "test [encrypt] 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());
        
        // test whether the original subject mail attribute is set and that it's correct
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        assertEquals("test [encrypt] 123", mailAttributes.getOriginalSubject());
    }

    @Test
    public void testSubjectTriggerRegExprNotRemoveMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "(?i)\\[\\s*encrypt\\s*\\]", true, false);
        
        Mail mail = createMail("test@example.com", "test [encrypt] 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test@example.com", "test [encrypt] 123", mail.getMessage().getSubject());
        
        // test whether the original subject mail attribute is set and that it's correct
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        assertNull(mailAttributes.getOriginalSubject());
    }
    
    @Test
    public void testSubjectTriggerRegExprNoMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "(?i)\\[\\s*encrypt\\s*\\]", true, true);
        
        Mail mail = createMail("test@example.com", "test no match 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("test no match 123", mail.getMessage().getSubject());
        
        // test whether the original subject mail attribute is set and that it's correct
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        assertNull(mailAttributes.getOriginalSubject());
    }

    @Test
    public void testSubjectTriggerNoUserNoMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test no match 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("test no match 123", mail.getMessage().getSubject());
    }

    @Test
    public void testSubjectTriggerNoRegExprRemoveMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "ENcRypT", false, true);
        
        Mail mail = createMail("test@example.com", "test [encrypt] 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test [] 123", mail.getMessage().getSubject());
    }

    @Test
    public void testSubjectTriggerNoRegExprNoRemoveMatch()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "encrypt", false, false);
        
        Mail mail = createMail("test@example.com", "test [encrypt] 123", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test [encrypt] 123", mail.getMessage().getSubject());
    }

    @Test
    public void testSubjectTriggerInvalidPattern()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "123 (", true, true);
        
        Mail mail = createMail("test@example.com", "test", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test
    public void testSubjectTriggerInvalidPatternButNoRegExpr()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "123 (", false, true);
        
        Mail mail = createMail("test@example.com", "test123 (xxx", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("testxxx", mail.getMessage().getSubject());
    }
    
    @Test
    public void testSubjectTriggerNoSubject()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "123 (", true, true);
        
        Mail mail = createMail("test@example.com", null, "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }    
    
    @Test
    public void testSubjectTriggerRegMultipleRecipients()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "(?i)\\[\\s*encrypt\\s*\\]", true, true);
        
        Mail mail = createMail("test@example.com", "test [encrypt] 123", "someuser1@example.com", "someuser2@example.com",
        		"someuser3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(3, result.size());
        assertTrue(result.contains(new MailAddress("someuser1@example.com")));
        assertTrue(result.contains(new MailAddress("someuser2@example.com")));
        assertTrue(result.contains(new MailAddress("someuser3@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());
    }
    
    @Test
    public void testSubjectTriggerNullPattern()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", null, true, true);
        
        Mail mail = createMail("test@example.com", "test", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testSubjectEmptyPattern()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "", true, true);
        
        Mail mail = createMail("test@example.com", "", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testSubjectTriggerQuoteRegExpr()
    throws Exception 
    {
        SenderSubjectTrigger matcher = new SenderSubjectTrigger(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        addUser("test@example.com", "\\Q|^@*test\\E", false, true);
        
        Mail mail = createMail("test@example.com", "test \\Q|^@\\Q|^@*test\\E*test\\E 123 ", "someuser@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());
    }
}
