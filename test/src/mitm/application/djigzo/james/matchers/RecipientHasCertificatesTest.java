/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.Certificates;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.test.TestUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RecipientHasCertificatesTest
{
    private static MockMatcherConfig matcherConfig;
    
    private static ExtendedAutoTransactDelegator proxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
        
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        MockMailetContext mailetContext = new MockMailetContext();
        
        matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        KeyAndCertificateWorkflow keyAndCertificateWorkflow = SystemServices.getKeyAndCertificateWorkflow();
        
        CertificateWorkflow rootWorkflow = SystemServices.getKeyAndRootCertificateWorkflow();
        
        importKeyStore(keyAndCertificateWorkflow, new File("test/resources/testdata/keys/testCertificates.p12"));
        importCertificates(rootWorkflow, new File("test/resources/testdata/certificates/mitm-test-root.cer"));
        
        proxy = AutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class); 
    }
    
    @Before
    public void setup()
    throws Exception
    {
        proxy.cleanCTL();
        proxy.deleteAllUsers();
        proxy.deleteAllDomains();
        proxy.addUser("m.brinkers@pobox.com", CertificateUtils.readX509Certificates(new File(
            "test/resources/testdata/certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer")));
    }
    
    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator
    {
        @StartTransaction
        public void addUser(String email, Collection<X509Certificate> certificates)
        throws DatabaseException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                    
            try {
                User user = userWorkflow.addUser(email);
                
                userWorkflow.makePersistent(user);
                
                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        user.getUserPreferences().getCertificates().add((X509Certificate) certificate);
                    }
                }            
            }
            catch(Exception e) {
                throw new DatabaseException(e);
            }
        }
    }
    
    private static void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile) 
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(pfxFile), "test".toCharArray());
        
        keyAndCertificateWorkflow.importKeyStore(keyStore, KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);        
    }

    private static void importCertificates(CertificateWorkflow rootWorkflow, File certificateFile) 
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate) {
                rootWorkflow.addCertificateTransacted((X509Certificate) certificate);
            }
        }
    }
        
    @Test
    public void testSingleRecipientWithMatchCaseInsensitiveEmail() 
    throws Exception
    {
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("teST@EXAmple.com"));
        
        mail.setRecipients(recipients);

        assertEquals(1, proxy.getUserCount());
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        
        
        assertEquals(1, proxy.getUserCount());
    }

    @Test
    public void testSingleRecipientWithMatch() 
    throws Exception
    {
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(new MockMatcherConfig("test", "matchOnError=false,false", new MockMailetContext()));
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);

        assertEquals(1, proxy.getUserCount());
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        

        assertEquals(1, proxy.getUserCount());
    }

    @Test
    public void testSingleRecipientWithMatchPersist() 
    throws Exception
    {
        proxy.setDomainProperty("example.com", "user.sMIMEAddUser", "true");
        
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(new MockMatcherConfig("test", "matchOnError=false,false", new MockMailetContext()));
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);

        assertEquals(1, proxy.getUserCount());
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        

        assertEquals(2, proxy.getUserCount());
    }
    
    @Test
    public void testSingleRecipientWithMatchNoPersist() 
    throws Exception
    {
        proxy.setDomainProperty("example.com", "user.sMIMEAddUser", "false");
        
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(new MockMatcherConfig("test", "matchOnError=false,false", new MockMailetContext()));
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);

        assertEquals(1, proxy.getUserCount());
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        

        assertEquals(1, proxy.getUserCount());
    }
    
    @Test
    public void testMultipleRecipientsOneMatch() 
    throws Exception
    {
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(new MockMatcherConfig("test", "matchOnError=false,true", new MockMailetContext()));
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("qqq@example.com"));
        
        mail.setRecipients(recipients);

        assertEquals(1, proxy.getUserCount());

        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("test@example.com", ((MailAddress) result.iterator().next()).toInternetAddress().toString());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        

        assertEquals(1, proxy.getUserCount());
    }

    @Test
    public void testMultipleRecipientsBlackListed() 
    throws Exception
    {
        AutoTransactDelegator.createProxy().addToCTL("E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4", 
                CTLEntryStatus.BLACKLISTED, false /* allow expired */);
        
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(12, certificates.getCertificates().size());        
    }
    
    @Test
    public void testMultipleRecipientsMultipleMatchesWhiteListed() 
    throws Exception
    {
        File file = new File("test/resources/testdata/certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer");
        
        AutoTransactDelegator.createProxy().addToCTL(TestUtils.loadCertificate(file), CTLEntryStatus.WHITELISTED, true /* allow expired */);
        
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(14, certificates.getCertificates().size());        
    }

    @Test
    public void testMultipleRecipientsMultipleMatchesWhiteListedExpiredNotAllowed() 
    throws Exception
    {
        File file = new File("test/resources/testdata/certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer");
        
        AutoTransactDelegator.createProxy().addToCTL(TestUtils.loadCertificate(file), CTLEntryStatus.WHITELISTED, false /* not allow expired */);
        
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNotNull(certificates);
        
        assertEquals(13, certificates.getCertificates().size());        
    }
    
    @Test
    public void testSingleRecipientNoMatch() 
    throws Exception
    {
        RecipientHasCertificates matcher = new RecipientHasCertificates();
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("testxxx@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = mailAttributes.getCertificates();
        
        assertNull(certificates);
    }
}
