/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RecipientIsSkipCalendarTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
    }

    @Before
    public void setup()
    throws Exception
    {
        AutoTransactDelegator.createProxy().deleteAllUsers();
    }

    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
        @StartTransaction
        public void setSMIMESkipCalendar(String userEmail, boolean skip) 
        throws AddressException, HierarchicalPropertiesException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

            User user = userWorkflow.getUser(userEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);

            UserProperties userProperties = user.getUserPreferences().getProperties();

            userProperties.setSMIMESkipCalendar(skip);

            userWorkflow.makePersistent(user);            
        }

        @StartTransaction
        public void setSMIMESkipSigningCalendar(String userEmail, boolean skip) 
        throws AddressException, HierarchicalPropertiesException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

            User user = userWorkflow.getUser(userEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
            
            UserProperties userProperties = user.getUserPreferences().getProperties();

            userProperties.setSMIMESkipSigningCalendar(skip);

            userWorkflow.makePersistent(user);            
        }
    }
    
    private void setSMIMESkipCalendar(String user, boolean skip) 
    throws AddressException, HierarchicalPropertiesException, ProxyFactoryException, NoSuchMethodException 
    {
        ExtendedAutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class).
                setSMIMESkipCalendar(user, skip); 
    }

    private void setSMIMESkipSigningCalendar(String user, boolean skip) 
    throws AddressException, HierarchicalPropertiesException, ProxyFactoryException, NoSuchMethodException 
    {
        ExtendedAutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class).
                setSMIMESkipSigningCalendar(user, skip); 
    }
    
    private Mail createMail(String from, File messageFile, String... recipients) 
    throws MessagingException, FileNotFoundException
    {
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(messageFile);

        message.setFrom(from != null ? new InternetAddress(from) : null);
        
        message.saveChanges();
        
        mail.setMessage(message);
                
        Set<MailAddress> recipientAddresses = new HashSet<MailAddress>();
        
        for (String recipient : recipients)
        {
            recipientAddresses.add(new MailAddress(recipient));
        }
        
        mail.setRecipients(recipientAddresses);
        
        return mail;
    }
    
    
    @Test
    public void testTextCalendarBoth()
    throws Exception 
    {
        RecipientIsSkipCalendar matcher = new RecipientIsSkipCalendar();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,skipSMIMEBoth", mailetContext);
        
        matcher.init(matcherConfig);
        
        setSMIMESkipSigningCalendar("1@example.com", true);
        setSMIMESkipCalendar("2@example.com", true);
        setSMIMESkipSigningCalendar("3@example.com", true);
        
        Mail mail = createMail("test@example.com", new File(testBase, "mail/outlook-meeting-request.eml"), 
                "1@example.com", "2@example.com", "3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(3, result.size());
        assertTrue(result.contains(new MailAddress("1@example.com")));
        assertTrue(result.contains(new MailAddress("2@example.com")));
        assertTrue(result.contains(new MailAddress("3@example.com")));
    }    
    
    @Test
    public void testTextCalendarSMIMESkipCalendarMultipart()
    throws Exception 
    {
        RecipientIsSkipCalendar matcher = new RecipientIsSkipCalendar();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,skipSMIME", mailetContext);
        
        matcher.init(matcherConfig);
        
        setSMIMESkipCalendar("2@example.com", true);
        setSMIMESkipCalendar("3@example.com", true);
        
        Mail mail = createMail("test@example.com", new File(testBase, "mail/multipart_appointment.eml"), 
                "1@example.com", "2@example.com", "3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("2@example.com")));
        assertTrue(result.contains(new MailAddress("3@example.com")));
    }    

    @Test
    public void testTextCalendarSMIMESkipCalendarMultipartNoMatch()
    throws Exception 
    {
        RecipientIsSkipCalendar matcher = new RecipientIsSkipCalendar();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,skipSMIME", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", new File(testBase, "mail/multipart_appointment.eml"), 
                "1@example.com", "2@example.com", "3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testNoCalendar()
    throws Exception 
    {
        RecipientIsSkipCalendar matcher = new RecipientIsSkipCalendar();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,skipSMIME", mailetContext);
        
        matcher.init(matcherConfig);
        
        setSMIMESkipCalendar("2@example.com", true);
        setSMIMESkipCalendar("3@example.com", true);

        Mail mail = createMail("test@example.com", new File(testBase, "mail/signed-encrypt-oe.eml"), 
                "1@example.com", "2@example.com", "3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }        
}
