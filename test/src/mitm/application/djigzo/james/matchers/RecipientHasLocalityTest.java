/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.james.matchers.RecipientHasLocality;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;

public class RecipientHasLocalityTest
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        addUser();
    }

    private static void addUser() 
    throws AddressException, CertificateException, NoSuchProviderException, FileNotFoundException, DatabaseException 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        addUserAction(session, "m.brinkers@pobox.com", UserLocality.INTERNAL);
                        addUserAction(session, "m.brinkers2@pobox.com", UserLocality.INTERNAL);
                        addUserAction(session, "test@example.com", UserLocality.EXTERNAL);
                        addUserAction(session, "test2@example.com", null);
                        addUserAction(session, "m.brinkers3@pobox.com", UserLocality.INTERNAL);
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
     
    public static void addUserAction(Session session, String email, UserLocality userLocality)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.addUser(email);
     
            user.getUserPreferences().getProperties().setUserLocality(userLocality);
            
            userWorkflow.makePersistent(user);            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }
    
    @Test
    public void testInternalCaseInsensitiveEmail() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, internal", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinKERS2@POBox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("m.brinKERS2@POBox.com", ((MailAddress)result.iterator().next()).toInternetAddress().toString());
    }

    @Test
    public void testExternal() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, external", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("test@example.com", ((MailAddress)result.iterator().next()).toInternetAddress().toString());
    }

    @Test
    public void testExternalMultipleMatches() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, external", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("m.brinkers2@pobox.com"));
        recipients.add(new MailAddress("m.brinkers3@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
    }
    
    @Test
    public void testInternal() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, internal", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("m.brinkers@pobox.com", ((MailAddress)result.iterator().next()).toInternetAddress().toString());
    }
    
    @Test
    public void testInternalMultipleMatches() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, internal", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("m.brinkers2@pobox.com"));
        recipients.add(new MailAddress("m.brinkers3@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(3, result.size());
    }    
    
    @Test(expected = IllegalArgumentException.class)
    public void testMissingCondition() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", null, mailetContext);
        
        matcher.init(matcherConfig);
    }    

    @Test(expected = IllegalArgumentException.class)
    public void testUnknownCondition() 
    throws MessagingException 
    {
        RecipientHasLocality matcher = new RecipientHasLocality();

        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, xxx", mailetContext);
        
        matcher.init(matcherConfig);
    }    
}
