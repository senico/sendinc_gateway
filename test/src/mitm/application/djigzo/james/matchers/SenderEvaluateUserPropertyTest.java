/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.matchers.SenderEvaluateUserProperty;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.MailSession;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;

public class SenderEvaluateUserPropertyTest
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        addUser();
    }

    private static void addUser()
    throws AddressException, CertificateException, NoSuchProviderException, FileNotFoundException, DatabaseException 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        addUserAction(session, "m.brinkers@pobox.com", "p1", "v1");
                        addUserAction(session, "m.brinkers@pobox.com", "p2", "v2");
                        addUserAction(session, "m.brinkers@pobox.com", "nullValue", null);
                        addUserAction(session, "000@example.com", "p1", "v1");
                        addUserAction(session, "test@example.com", "p2=", "v2 = x");
                        addUserAction(session, "m.brinkers@pobox.com", "user.test", "123");
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
     
    public static void addUserAction(Session session, String email, String propertyName, String value)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
     
            user.getUserPreferences().getProperties().setProperty(propertyName, value, false);
            
            userWorkflow.makePersistent(user);            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }
    
    @Test
    public void testInvalidFrom()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("zzz"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testMatchCaseInsensitiveEmail()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkERS@POBox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@EXAMple.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@EXAMple.com")));
    }

    @Test
    public void testMatch()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
    }

    @Test
    public void testMatchVariableWithDot()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{user.test}=='123'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
    }
    
    @Test
    public void testNoMatch()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='123456' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test
    public void testMatchNullValue()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        /*
         * Check if it maches on a non existing property when matching agains 'null' 
         */
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{nonexisting}=='null'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
    }
     
    @Test
    public void testMatchNonNull()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}!='null'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        
        
        // now test if matching for non-null does not match
        matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{nullValue}!='null'", mailetContext);
        
        matcher.init(matcherConfig);
        
        mail = new MockMail();
        
        message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testMatchOnError()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, #{'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
    }
    
    @Test
    public void testNoMatchOnError()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInvalidCondition()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=xxx", mailetContext);
        
        matcher.init(matcherConfig);        
    }
    
    @Test
    public void testMailAttributes()
    throws MessagingException 
    {
        SenderEvaluateUserProperty matcher = new SenderEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", 
                "matchOnError=false, #{mail.attribute.attr}!='true'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
        
        mail.setAttribute("attr", true);

        result = matcher.match(mail);

        assertEquals(0, result.size());
        
        mail.setAttribute("attr", false);

        result = matcher.match(mail);

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
    }    
}
