/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.ClientSecrets;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.EncryptedContainer;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.common.mail.MailSession;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class HasClientSecretTest
{
    private static MockMailetContext mailetContext;
    
    private static AutoTransactDelegator proxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();        

        proxy = AutoTransactDelegator.createProxy();
    }

    @Before
    public void setup()
    throws Exception 
    {
        mailetContext = new MockMailetContext();
        
        proxy.deleteAllUsers();
        
        proxy.addUser("test1@example.com");
        proxy.addUser("test2@example.com");
        
        proxy.setProperty("test1@example.com", "user.clientSecret", "secret1", true);
        proxy.setProperty("test2@example.com", "user.clientSecret", "secret2", true);        
    }
    
    @Test
    public void testSingleMatch()
    throws Exception 
    {
        HasClientSecret matcher = new HasClientSecret();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("nomatch@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        ClientSecrets clientSecrets = attributes.getClientSecrets();
        
        assertNotNull(clientSecrets);
        
        assertEquals(1, clientSecrets.size());
        
        EncryptedContainer<String> encryptedClientSecret = clientSecrets.get("test1@example.com");

        assertNotNull(encryptedClientSecret);
        
        assertEquals("secret1", encryptedClientSecret.get());
    }    
    
    @Test
    public void testMultipleMatch()
    throws Exception 
    {
        HasClientSecret matcher = new HasClientSecret();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        ClientSecrets clientSecrets = attributes.getClientSecrets();
        
        assertNotNull(clientSecrets);
        
        assertEquals(2, clientSecrets.size());
        
        EncryptedContainer<String> encryptedClientSecret = clientSecrets.get("test1@example.com");

        assertNotNull(encryptedClientSecret);
        
        assertEquals("secret1", encryptedClientSecret.get());
        
        encryptedClientSecret = clientSecrets.get("test2@example.com");

        assertNotNull(encryptedClientSecret);
        
        assertEquals("secret2", encryptedClientSecret.get());        
    }        
    
    @Test
    public void testNoMatch()
    throws Exception 
    {
        HasClientSecret matcher = new HasClientSecret();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("nomatch@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        ClientSecrets clientSecrets = attributes.getClientSecrets();
        
        assertNotNull(clientSecrets);
        
        assertEquals(0, clientSecrets.size());
    }        
}
