/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.matchers.RecipientEvaluateUserProperty;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.MailSession;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;

public class RecipientEvaluateUserPropertyTest
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        addUser();
    }

    private static void addUser()
    throws AddressException, CertificateException, NoSuchProviderException, FileNotFoundException, DatabaseException 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        addUserAction(session, "m.brinkers@pobox.com", "p1", "v1");
                        addUserAction(session, "m.brinkers@pobox.com", "p2", "v2");
                        addUserAction(session, "000@example.com", "p1", "v1");
                        addUserAction(session, "test@example.com", "p2", "v2");
                        addUserAction(session, "test@example.com", "p3", "<");
                        addUserAction(session, "test@example.com", "number", "100");
                        addUserAction(session, "test@example.com", "largenumber", "9999");
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
     
    public static void addUserAction(Session session, String email, String propertyName, String value)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
     
            user.getUserPreferences().getProperties().setProperty(propertyName, value, false);
            
            userWorkflow.makePersistent(user);            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }

    @Test
    public void testIsMIMETypeFunctionNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, isMIMEType('#{mail.message.contentType}', 'application/binary')", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "application/octet-stream");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testIsMIMETypeFunctionMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, isMIMEType('#{mail.message.contentType}', 'text/*')", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/xml");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }
    
    @Test
    public void testContentTypeVariable()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, startsWith('#{mail.message.contentType}', 'text/xml', 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/xml");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }

    @Test
    public void testContentTypeVariableNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, startsWith('#{mail.message.contentType}', 'abc', 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/xml");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testMailSizeExceed()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat(#{number}, 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }

    @Test
    public void testMailSizeUnknownLimit()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat(#{unknown_var}, 5)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }

    @Test
    public void testMailSizeUnknownLimitNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat(#{unknown_var}, 999999)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testMailSizeLargeNumber()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}<toFloat(#{largenumber}, 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }

    @Test
    public void testMailSizeLargeNumberNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat(#{largenumber}, 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testIllegalNumberMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat('abc', 5)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }
    
    
    @Test
    public void testMailSizeNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}<toFloat(#{number}, 0)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test
    public void testIllegalNumberNoMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{mail.size}>toFloat('abc', 99999999)", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test123456", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testMatchCaseInsensitiveEmail()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brINKers@POBOX.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("m.brINKers@POBOX.com")));
    }

    @Test
    public void testMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("m.brinkers@pobox.com")));
    }

    @Test
    public void testMatchMultipleRecipientsSingleMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' && #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("m.brinkers@pobox.com")));
    }

    @Test
    public void testMatchMultipleRecipientsMultipleMatch()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p1}=='v1' || #{p2}=='v2'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("123456@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("m.brinkers@pobox.com")));
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }
    
    @Test
    public void testMatchOnError()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, #{'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("123456@example.com")));
    }
    
    @Test
    public void testNoMatchOnError()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123456@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInvalidCondition()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=xxx", mailetContext);
        
        matcher.init(matcherConfig);        
    }
    
    @Test
    public void testXmlEscape()
    throws MessagingException 
    {
        RecipientEvaluateUserProperty matcher = new RecipientEvaluateUserProperty();
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false, #{p3}=='&lt;'", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("m.brinkers@pobox.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
    }    
}
