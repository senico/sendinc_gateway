package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailSession;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class SubjectPhoneNumberTest
{
    private static MockMailetContext mailetContext;
        
    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
        @StartTransaction
        public void assertPhoneNumber(String email, String phoneNumber) 
        throws AddressException, HierarchicalPropertiesException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

            User user = userWorkflow.getUser(email, GetUserMode.NULL_IF_NOT_EXIST);

            assertNotNull(user);
            
            UserProperties userProperties = user.getUserPreferences().getProperties();

            assertEquals(phoneNumber, userProperties.getSMSPhoneNumber());
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
    }

    @Before
    public void setup()
    throws Exception
    {
        AutoTransactDelegator.createProxy().deleteAllUsers();
    }

    private Mail createMail(String from, String subject, String... recipients) 
    throws MessagingException
    {
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        
        Set<MailAddress> recipientAddresses = new HashSet<MailAddress>();
        
        for (String recipient : recipients)
        {
            recipientAddresses.add(new MailAddress(recipient));
        }
        
        mail.setRecipients(recipientAddresses);
        
        return mail;
    }
    
    private void assertPhoneNumber(String email, String phoneNumber)
    throws AddressException, HierarchicalPropertiesException, ProxyFactoryException, NoSuchMethodException
    {
        ExtendedAutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class).assertPhoneNumber(email, phoneNumber);
    }
    
    @Test
    public void testNumberNotAtEnd()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "+31123456 some text", "test2@example.com");
        
        AutoTransactDelegator delegator = AutoTransactDelegator.createProxy();
        
        assertFalse(delegator.isUser("test2@example.com"));
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("+31123456 some text", mail.getMessage().getSubject());
        assertFalse(AutoTransactDelegator.createProxy().isUser("test2@example.com"));
    }
    
    @Test
    public void testMultipleRecipientsMatch()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test 316123456", "test@example.com", "test2@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertFalse(AutoTransactDelegator.createProxy().isUser("test@example.com"));
        assertFalse(AutoTransactDelegator.createProxy().isUser("test2@example.com"));
    }

    @Test
    public void testMultipleRecipientsNoMatch()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test", "test@example.com", "test2@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("test", mail.getMessage().getSubject());
        assertFalse(AutoTransactDelegator.createProxy().isUser("test@example.com"));
        assertFalse(AutoTransactDelegator.createProxy().isUser("test2@example.com"));
    }
    
    @Test
    public void testOnlyNumber()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "+31123456", "test2@example.com");
        
        AutoTransactDelegator delegator = AutoTransactDelegator.createProxy();
        
        assertFalse(delegator.isUser("test2@example.com"));
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("", mail.getMessage().getSubject());        
        assertPhoneNumber("test2@example.com", "31123456");
        assertTrue(delegator.isUser("test2@example.com"));
    }

    @Test
    public void testOnlyNumberNoPlus()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "31123456", "test2@example.com");
        
        AutoTransactDelegator delegator = AutoTransactDelegator.createProxy();
        
        assertFalse(delegator.isUser("test2@example.com"));
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("", mail.getMessage().getSubject());        
        assertPhoneNumber("test2@example.com", "31123456");
        assertTrue(delegator.isUser("test2@example.com"));
    }
    
    @Test
    public void testNoNumber()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test test", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("test test", mail.getMessage().getSubject());        
    }
    
    @Test
    public void testNullSubject()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", null, "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test
    public void testNullSender()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail(null, "test 31123456", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());        
        assertPhoneNumber("test@example.com", "31123456");
    }
    
    @Test
    public void testNullDefaultCountryCode()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test 06123456", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }

    @Test
    public void testDefaultCountryCode()
    throws Exception 
    {
        AutoTransactDelegator.createProxy().setProperty("test@example.com", "user.phoneDefaultCountryCode", "31");
        
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test 06123456", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());        
        assertPhoneNumber("test@example.com", "316123456");
    }
    
    @Test
    public void testSubjectPhoneNumber()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test +31123456", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "31123456");
        
        mail = createMail("test@example.com", "test 31 1 23456", "test@example.com");

        result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());        
        assertPhoneNumber("test@example.com", "31123456");

        mail = createMail("test@example.com", "test 31-1 (2)3456", "test@example.com");

        result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());        
        assertPhoneNumber("test@example.com", "31123456");
    }    
    
    @Test
    public void testAltRegExpression()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false,\\d{6,25}", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test 316123456", "test@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());        
        assertPhoneNumber("test@example.com", "316123456");
        
        mail = createMail("test@example.com", "test 3161 23456", "test@example.com");
        
        result = matcher.match(mail);
        
        assertEquals(0, result.size());
        assertEquals("test 3161 23456", mail.getMessage().getSubject());        
    }    

    @Test
    public void testMutipleNumbers()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "+31123456 test +31123456", "test2@example.com");
        
        AutoTransactDelegator delegator = AutoTransactDelegator.createProxy();
        
        assertFalse(delegator.isUser("test2@example.com"));
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("+31123456 test", mail.getMessage().getSubject());        
        assertPhoneNumber("test2@example.com", "31123456");
        assertTrue(delegator.isUser("test2@example.com"));
    }

    @Test
    public void testOriginalNumberInMailAttributes()
    throws Exception 
    {
        SubjectPhoneNumber matcher = new SubjectPhoneNumber(); 
                
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=false", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("test@example.com", "test 123 31123456", "test2@example.com");
        
        DjigzoMailAttributes mailAttr = new DjigzoMailAttributesImpl(mail);
        
        mailAttr.setOriginalSubject("test 123 [encrypt] 31123456");
        
        AutoTransactDelegator delegator = AutoTransactDelegator.createProxy();
        
        assertFalse(delegator.isUser("test2@example.com"));
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("test 123", mail.getMessage().getSubject());        
        assertPhoneNumber("test2@example.com", "31123456");
        assertTrue(delegator.isUser("test2@example.com"));
    }    
}
