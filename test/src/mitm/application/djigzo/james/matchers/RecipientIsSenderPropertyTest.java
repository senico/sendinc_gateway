/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetContext;
import mitm.application.djigzo.james.mock.MockMatcherConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.mail.MailSession;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class RecipientIsSenderPropertyTest
{
    private static MockMailetContext mailetContext;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
    }

    @Before
    public void setup()
    throws Exception
    {
        AutoTransactDelegator.createProxy().deleteAllUsers();
    }

    private Mail createMail(String from, String subject, String... recipients) 
    throws MessagingException
    {
        Mail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        
        Set<MailAddress> recipientAddresses = new HashSet<MailAddress>();
        
        for (String recipient : recipients)
        {
            recipientAddresses.add(new MailAddress(recipient));
        }
        
        mail.setRecipients(recipientAddresses);
        
        return mail;
    }
    
    @Test
    public void testMatch()
    throws Exception 
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        proxy.setProperty("sender@example.com", "test", "server@example.com");
                
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("sender@example.com", "some subject", "server@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("server@example.com", result.iterator().next().toString());
    }

    @Test
    public void testMatchMiltipleRecipients()
    throws Exception 
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        proxy.setProperty("sender@example.com", "test", "server@example.com");
                
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("sender@example.com", "some subject", "server@example.com", "r2@example.com", "r3@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(1, result.size());
        assertEquals("server@example.com", result.iterator().next().toString());
    }
    
    @Test
    public void testNoEmailMatch()
    throws Exception 
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        proxy.setProperty("sender@example.com", "test", "server@example.com");
                
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("sender@example.com", "some subject", "other@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }    

    @Test
    public void testNoProperty()
    throws Exception 
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        proxy.setProperty("sender@example.com", "123456", "server@example.com");
                
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("sender@example.com", "some subject", "other@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }
    
    @Test
    public void testNoUser()
    throws Exception 
    {
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("sender@example.com", "some subject", "other@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }        
    
    @Test
    public void testNullSender()
    throws Exception 
    {
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail(null, "some subject", "other@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }            

    @Test
    public void testInvalidSender()
    throws Exception 
    {
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true, test", mailetContext);
        
        matcher.init(matcherConfig);
        
        Mail mail = createMail("xxx", "some subject", "other@example.com");
        
        Collection<?> result = matcher.match(mail);
        
        assertEquals(0, result.size());
    }            

    @Test(expected = MessagingException.class)
    public void testMissingInitProperty()
    throws Exception 
    {
        RecipientIsSenderProperty matcher = new RecipientIsSenderProperty(); 
        
        MockMatcherConfig matcherConfig = new MockMatcherConfig("test", "matchOnError=true", mailetContext);
        
        matcher.init(matcherConfig);
    }            
}
