/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseActionRetryEvent;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.mailet.MailAddress;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.BeforeClass;
import org.junit.Test;


public class MailAddressMatcherTest
{
	private static SessionManager sessionManager;
	private static UserWorkflow userWorkflow;
	private static DatabaseActionExecutor actionExecutor;
	
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        sessionManager = SystemServices.getSessionManager();
        userWorkflow = SystemServices.getUserWorkflow();
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);
    }

    
    @Test
    public void mailAddressMatcherNoRetry()
    throws Exception
    {
    	final int retries = 0;
    	
    	final MutableInt count = new MutableInt();

    	MailAddressMatcher.HasMatchEventHandler handler = new MailAddressMatcher.HasMatchEventHandler()
    	{
            @Override
			public boolean hasMatch(User user)
			throws MessagingException
			{
				count.increment();
				
				throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
			}
    	};
    	
    	MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, actionExecutor, handler, retries);
    	
    	Collection<MailAddress> recipients = MailAddressUtils.fromAddressArrayToMailAddressList(
    			new InternetAddress("test@example.com"));

    	try {
    		matcher.getMatchingMailAddresses(recipients);
    		
    		fail();
    	}
    	catch(MessagingException e)
    	{
    		/*
    		 * Must be caused by ConstraintViolationException
    		 */
    		assertTrue(ExceptionUtils.getRootCause(e) instanceof ConstraintViolationException);
    	}
    	
    	assertEquals(1, count.intValue());
    }
    
    @Test
    public void mailAddressMatcherRetrySuccess()
    throws Exception
    {
    	final int retries = 4;
    	
    	final MutableInt count = new MutableInt();

    	MailAddressMatcher.HasMatchEventHandler handler = new MailAddressMatcher.HasMatchEventHandler()
    	{
            @Override
			public boolean hasMatch(User user)
			throws MessagingException
			{
				count.increment();
				
				if (count.intValue() <= retries) {
					throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
				}
				
				return true;
			}
    	};
    	
    	MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, actionExecutor, handler, retries);
    	
    	Collection<MailAddress> recipients = MailAddressUtils.fromAddressArrayToMailAddressList(
    			new InternetAddress("test@example.com"));
    	
    	Collection<MailAddress> result = matcher.getMatchingMailAddresses(recipients);
    	
    	assertEquals(retries + 1, count.intValue());
    	assertEquals(1, result.size());
    	assertEquals("test@example.com", result.iterator().next().toString());
    }
    
    @Test
    public void mailAddressMatcherRetryEvent()
    throws Exception
    {
    	final int retries = 4;
    	
    	final MutableInt count = new MutableInt();

    	final MutableInt eventCount = new MutableInt();

    	DatabaseActionRetryEvent retryEvent = new DatabaseActionRetryEvent()
    	{
            @Override
			public void onRetry() {
				eventCount.increment();
			}
    	};
    	
    	MailAddressMatcher.HasMatchEventHandler handler = new MailAddressMatcher.HasMatchEventHandler()
    	{
            @Override
			public boolean hasMatch(User user)
			throws MessagingException
			{
				count.increment();
				
				throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
			}
    	};
    	
    	MailAddressMatcher matcher = new MailAddressMatcher(sessionManager, userWorkflow, actionExecutor, handler, retries, retryEvent);
    	
    	Collection<MailAddress> recipients = MailAddressUtils.fromAddressArrayToMailAddressList(
    			new InternetAddress("test@example.com"));
    	
    	try {
    		matcher.getMatchingMailAddresses(recipients);
    	}
    	catch(MessagingException e) {
    		/*
    		 * Must be caused by ConstraintViolationException
    		 */
    		assertTrue(ExceptionUtils.getRootCause(e) instanceof ConstraintViolationException);
    	}
    	
    	assertEquals(retries + 1, count.intValue());
    	assertEquals(retries, eventCount.intValue());
    }
}
