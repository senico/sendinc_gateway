/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.mailet.MailAddress;
import org.junit.Test;


public class DefaultMessageOriginatorIdentifierTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
        
    @Test
    public void testGetOriginator()
    throws Exception
    {
        assertEquals("wulungcheung3007@yahoo.com.hk", new DefaultMessageOriginatorIdentifier().getOriginator(
                loadMessage("chinese-encoded-from.eml")).getAddress());
    }
    
    @Test
    public void testGetOriginatorNullSenderFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                message).getAddress());
    }
    
    @Test
    public void testGetOriginatorInvalidFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("ABC", false));
        
        message.saveChanges();
        
        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                message).getAddress());
    }
    
    @Test
    public void testGetOriginatorMailSender()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        mail.setSender(new MailAddress("test@example.com"));
        
        assertEquals("test@example.com", new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }
    
    @Test
    public void testGetOriginatorMailNullSenderNoFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        mail.setSender(null);
        
        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }    
    
    @Test
    public void testGetOriginatorMailSenderInvalidFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setFrom(new InternetAddress("ABC", false));
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        mail.setSender(new MailAddress("test@example.com"));
        
        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }
}
