/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.PasswordContainer;
import mitm.application.djigzo.james.Passwords;
import mitm.application.djigzo.james.PhoneNumber;
import mitm.application.djigzo.james.PhoneNumbers;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.mail.MailUtils;
import mitm.common.sms.DummySMSTransport;
import mitm.common.sms.SMSGateway;

import org.apache.commons.io.FileUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SenderTemplatePropertySMSPasswordTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static DummySMSTransport smsTransport;
    
	private AutoTransactDelegator autoTransactDelegator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();        
    }
    
    @Before
    public void setup()
    throws Exception
    {
    	autoTransactDelegator = AutoTransactDelegator.createProxy();
    	
    	autoTransactDelegator.deleteAllUsers();

    	// we will use the dummy transport
        SMSGateway smsGateway = SystemServices.getSMSGateway();
        
        smsTransport = new DummySMSTransport();
        
        smsGateway.setTransport(smsTransport);

        smsGateway.start();        
    }
        
    @Test
    public void testSendSMS()
    throws Exception
    {
        String template = FileUtils.readFileToString(new File("test/resources/templates/sms.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "smsTemplate", template);

        MockMailetConfig mailetConfig = new MockMailetConfig("test");
        
        Mailet mailet = new SenderTemplatePropertySMSPassword();
        
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "smsTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);

        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = new Passwords();
        
        PasswordContainer container = new PasswordContainer("test1", "test ID 1");
        
        passwords.put("m.brinkers@pobox.com", container);

        container = new PasswordContainer("test2", "test ID 2");

        passwords.put("123@example.com", container);

        attributes.setPasswords(passwords);
        
        PhoneNumbers phoneNumbers = new PhoneNumbers();
        
        phoneNumbers.put("m.brinkers@pobox.com", new PhoneNumber("1234567890"));
        phoneNumbers.put("123@example.com", new PhoneNumber("0987654321"));
        
        attributes.setPhoneNumbers(phoneNumbers);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/normal-message-with-attach.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        // wait some time for the SMS messages to be transported
        Thread.sleep(5000);
        
        assertEquals(2, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));
        assertTrue(smsTransport.getPhoneNumbers().contains("0987654321"));
        assertEquals(2, smsTransport.getMessages().size());
        
        String sms0 = smsTransport.getMessages().get(0); 
        String sms1 = smsTransport.getMessages().get(1); 
        
        assertTrue(sms0.startsWith("TEST"));
        assertTrue(sms0.contains("test2"));
        assertTrue(sms0.contains("test ID 2"));

        assertTrue(sms1.startsWith("TEST"));
        assertTrue(sms1.contains("test1"));
        assertTrue(sms1.contains("test ID 1"));
    }
}
