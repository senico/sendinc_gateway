/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.mail.MailUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MailAttributesTest
{
    private AutoTransactDelegator autoTransactDelegator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        autoTransactDelegator = AutoTransactDelegator.createProxy();
        
        autoTransactDelegator.deleteAllUsers();
    }

    private MimeMessage createMessage() 
    throws FileNotFoundException, MessagingException
    {
        return MailUtils.loadMessage(new File("test/resources/testdata/mail/simple-text-message.eml"));    
    }
    
    private void assertNrOfAttributes(int nrOfAttributes, Mail mail)
    {
        Iterator<?> it = mail.getAttributeNames();
        
        int i = 0;
        
        while (it.hasNext())
        {
            i++;
            it.next();
        }
        
        assertEquals(nrOfAttributes, i);
    }
    
    @Test
    public void testDelete()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("delete", "todelete");
        mailetConfig.setInitParameter("delete", "anothertodelete");
        mailetConfig.setInitParameter("delete", "nonexistingtodelete");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setAttribute("todelete", "123");
        mail.setAttribute("anothertodelete", "abc");
        mail.setAttribute("nottodelete", "567");
        
        assertEquals("123", mail.getAttribute("todelete"));
        assertEquals("abc", mail.getAttribute("anothertodelete"));
        assertEquals("567", mail.getAttribute("nottodelete"));
        
        mail.setMessage(createMessage());
        
        assertNrOfAttributes(3, mail);
        
        mailet.service(mail);
        
        assertNull(mail.getAttribute("todelete"));
        assertNull(mail.getAttribute("anothertodelete"));
        assertEquals("567", mail.getAttribute("nottodelete"));        

        assertNrOfAttributes(1, mail);
    }
    
    @Test
    public void testSetSimple()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("set", "toset1=123");
        mailetConfig.setInitParameter("set", "toset2=abc");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        assertNrOfAttributes(0, mail);
        
        mail.setAttribute("toset1", "***");
        
        assertEquals("***", mail.getAttribute("toset1"));
        
        mail.setMessage(createMessage());
        
        mailet.service(mail);
        
        assertEquals("123", mail.getAttribute("toset1"));        
        assertEquals("abc", mail.getAttribute("toset2"));
        
        assertNrOfAttributes(2, mail);
        
        assertEquals("root", mail.getState());
    }

    @Test
    public void testAddSimple()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("add", "c1=123");
        mailetConfig.setInitParameter("add", "c1= 456");
        mailetConfig.setInitParameter("add", "c2 = abc");
        mailetConfig.setInitParameter("add", "existing_no_collection=789");
        mailetConfig.setInitParameter("add", "existing_collection=abc");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setAttribute("existing_no_collection", "***");
        mail.setAttribute("existing_collection", new ArrayList<String>(Arrays.asList(new String[]{"a1", "a2"})));
        
        assertEquals("***", mail.getAttribute("existing_no_collection"));
        assertTrue(mail.getAttribute("existing_collection") instanceof ArrayList);
        
        assertNrOfAttributes(2, mail);
        
        mail.setMessage(createMessage());
        
        mailet.service(mail);
        
        Collection<?> attr = (Collection<?>) mail.getAttribute("c1");
        
        assertTrue(attr instanceof LinkedList);
        assertTrue(attr.contains("123"));
        assertTrue(attr.contains("456"));

        attr = (Collection<?>) mail.getAttribute("c2");
        
        assertTrue(attr instanceof LinkedList);
        assertTrue(attr.contains("abc"));

        attr = (Collection<?>) mail.getAttribute("existing_collection");
        
        assertTrue(attr instanceof ArrayList);
        assertTrue(attr.contains("a1"));
        assertTrue(attr.contains("a2"));
        assertTrue(attr.contains("abc"));

        attr = (Collection<?>) mail.getAttribute("existing_no_collection");
        
        assertTrue(attr instanceof LinkedList);
        assertTrue(attr.contains("789"));

        assertNrOfAttributes(4, mail);
    }
    
    @Test
    public void testSetUserProp()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("set", "p1=#{p1}");
        mailetConfig.setInitParameter("set", "p2=#{p2}");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setMessage(createMessage());

        autoTransactDelegator.setProperty("test@example.com", "p1", "123");
        
        assertNull(mail.getAttribute("p1"));
        assertNull(mail.getAttribute("p2"));
        
        mailet.service(mail);
        
        assertEquals("123", mail.getAttribute("p1"));        
        assertNull(mail.getAttribute("p2"));  
        
        assertNrOfAttributes(1, mail);
    }
    
    @Test
    public void testAddUserProp()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("add", "p1=#{p1}");
        mailetConfig.setInitParameter("add", "p2=#{p2}");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setMessage(createMessage());

        autoTransactDelegator.setProperty("test@example.com", "p1", "123");
        
        assertNull(mail.getAttribute("p1"));
        assertNull(mail.getAttribute("p2"));
        
        mailet.service(mail);
        
        Collection<?> attr = (Collection<?>) mail.getAttribute("p1");
        
        assertTrue(attr instanceof LinkedList);        
        assertTrue(attr.contains("123"));        
        assertEquals(1, attr.size());        

        
        attr = (Collection<?>) mail.getAttribute("p2");
        
        assertNull(attr);        
    }
    
    @Test
    public void testSetSpecialAddress()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("set", "p1=${originator}");
        mailetConfig.setInitParameter("set", "p2=${replyTo}");
        mailetConfig.setInitParameter("set", "p3=${sender}");
        mailetConfig.setInitParameter("set", "p4=${from}");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = createMessage();

        mail.setSender(new MailAddress("sender@example.com"));
        message.setReplyTo(new Address[]{new InternetAddress("reply@example.com")});
        
        message.saveChanges();
        
        mail.setMessage(message);

        mailet.service(mail);
        
        assertEquals("test@example.com", mail.getAttribute("p1"));        
        assertEquals("reply@example.com", mail.getAttribute("p2"));        
        assertEquals("sender@example.com", mail.getAttribute("p3"));        
        assertEquals("test@example.com", mail.getAttribute("p4"));        
        
        assertNrOfAttributes(4, mail);
    }
    
    @Test
    public void testSerialized()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("serialized",
            "  djigzo.policyViolations =\n" +
            "  rO0ABXNyABRqYXZhLnV0aWwuTGlua2VkTGlzdAwpU11KYIgiAwAAeHB3BAAAAAFzcgAobWl0bS5j  \n" +
            "  b21tb24uZGxwLmltcGwuUG9saWN5VmlvbGF0aW9uSW1wbHSPxQFH6a6eAgAETAAFbWF0Y2h0ABJM  \n" +
            "  amF2YS9sYW5nL1N0cmluZztMAAZwb2xpY3lxAH4AA0wACHByaW9yaXR5dAApTG1pdG0vY29tbW9u  \n" +
            "  L2RscC9Qb2xpY3lWaW9sYXRpb25Qcmlvcml0eTtMAARydWxlcQB+AAN4cHB0AAhidWlsdCBpbn5y  \n" +
            "  ACdtaXRtLmNvbW1vbi5kbHAuUG9saWN5VmlvbGF0aW9uUHJpb3JpdHkAAAAAAAAAABIAAHhyAA5q  \n" +
            "  YXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AApRVUFSQU5USU5FdAAiVGhlIG1lc3NhZ2UgY291  \n" +
            "  bGQgbm90IGJlIGVuY3J5cHRlZHg=");        

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = createMessage();

        mail.setSender(new MailAddress("sender@example.com"));
        message.setReplyTo(new Address[]{new InternetAddress("reply@example.com")});
        
        message.saveChanges();
        
        mail.setMessage(message);

        mailet.service(mail);

        DjigzoMailAttributes mailAttr = new DjigzoMailAttributesImpl(mail);
        
        Collection<PolicyViolation> violations = mailAttr.getPolicyViolations(); 
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("built in", violation.getPolicy());                
        assertEquals("The message could not be encrypted", violation.getRule());                
        assertNull(violation.getMatch());                
        assertEquals(PolicyViolationPriority.QUARANTINE, violation.getPriority());                
    }
    
    @Test
    public void testSetProcessor()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("set", "toset1=123");
        mailetConfig.setInitParameter("processor", "next");

        Mailet mailet = new MailAttributes();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        assertNrOfAttributes(0, mail);
        
        mail.setAttribute("toset1", "***");
        
        assertEquals("***", mail.getAttribute("toset1"));
        
        mail.setMessage(createMessage());
        
        mailet.service(mail);
        
        assertEquals("123", mail.getAttribute("toset1"));        
        
        assertNrOfAttributes(1, mail);
        
        assertEquals("next", mail.getState());
    }    
}
