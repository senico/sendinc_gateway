/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class RemoveHeadersTest
{
    private final static File testBase = new File("test/resources/testdata");
    private static final File tempDir = new File("test/tmp");

    private static String log4jConfig = "conf/log4j.properties";

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        PropertyConfigurator.configure(log4jConfig);        
    }
        
    @Test
    public void testInvalidContentEncoding()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new RemoveHeaders();

        mailetConfig.setInitParameter("pattern", "(?i)^Subject$");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/unknown-content-transfer-encoding.eml"));
        
        mail.setMessage(message);

        assertNotNull(mail.getMessage().getSubject());

        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        TestUtils.saveMessages(tempDir, "testInvalidContentEncoding", mail.getMessage());
        
        assertNull(mail.getMessage().getSubject());
        
        MailUtils.validateMessage(mail.getMessage());
    }    
    
    @Test
	public void testRemoveHeader()
	throws Exception
	{
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new RemoveHeaders();

        mailetConfig.setInitParameter("pattern", "(?i)^X-Evolution-.*");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/unicode.eml"));

        mail.setMessage(message);

        assertNotNull(mail.getMessage().getHeader("X-Evolution-Account"));
        assertNotNull(mail.getMessage().getHeader("X-Evolution-Format"));
        assertNotNull(mail.getMessage().getHeader("X-Mailer"));

        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        TestUtils.saveMessages(tempDir, "testRemoveHeader", mail.getMessage());
        
        assertNull(mail.getMessage().getHeader("X-Evolution-Account"));
        assertNull(mail.getMessage().getHeader("X-Evolution-Format"));
        assertNotNull(mail.getMessage().getHeader("X-Mailer"));
	}
    
    @Test
	public void testRemoveHeaderCorruptMessage()
	throws Exception
	{
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new RemoveHeaders();

        mailetConfig.setInitParameter("pattern", "(?i)^Content-Disposition$");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/corrupt-base64.eml"));
        
        mail.setMessage(message);

        assertNotNull(mail.getMessage().getHeader("Content-Disposition"));

        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        TestUtils.saveMessages(tempDir, "testRemoveHeader", mail.getMessage());
        
        assertNull(mail.getMessage().getHeader("Content-Disposition"));
        
        MailUtils.validateMessage(mail.getMessage());
	}    
}
