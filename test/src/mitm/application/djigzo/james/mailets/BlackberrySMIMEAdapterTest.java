/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.james.mock.SendMailEventListenerImpl;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;
import mitm.test.TestUtils;

import org.apache.commons.io.FileUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BlackberrySMIMEAdapterTest
{
    private final static File testBase = new File("test/resources/testdata");
    private static final File tempDir = new File("test/tmp");

	private AutoTransactDelegator autoTransactDelegator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
    }
    
    @Before
    public void setup()
    throws Exception
    {
    	autoTransactDelegator = AutoTransactDelegator.createProxy();
    	
    	autoTransactDelegator.deleteAllUsers();
    }

    /*
     * Test what happens when an invalid message (a message that fails on writeTo) is used. 
     */
    @Test
    public void testCorruptMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("resources/templates/blackberry-smime-adapter.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        MimeBodyPart emptyPart = new MimeBodyPart();
        
        message.setContent(emptyPart, "text/plain");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        assertEquals(message, mail.getMessage());
        
        try {
            MailUtils.validateMessage(message);

            fail();
        }
        catch(IOException e) {
            // expected. The message should be corrupt
        }        
    }
    
    /*
     * This test tests whether a email template message with unknown content-transfer-encoding can be used. Javamail
     * prior to 1.4.4 (i.e. <= 1.4.3) would throw exceptions when handling unknown content-transfer-encoding's. Since
     * 1.4.4 Javamail silently ignores the unkown encoding.
     * 
     * This test has been modified to reflect the new behavior since 1.4.4
     */
    @Test
    public void testInvalidEncoding()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("test/resources/templates/blackberry-smime-adapter-invalid-encoding.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertFalse(mail.getMessage() == message);
        
        MimeMultipart mp = (MimeMultipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        BodyPart bp = mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("text/plain"));

        bp = mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("application/octet-stream"));
        assertEquals("x-rimdevicesmime.p7m", bp.getFileName());
    }
    
    @Test
    public void testSMIMEAdaptMessageNoMultipartNoMarker()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("test/resources/templates/blackberry-smime-adapter-no-multipart-no-marker.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        mailetConfig.setInitParameter("directSizeLimit", "100");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptMessageNoMultipartNoMarker", mail.getMessage());
        
        assertTrue(mail.getMessage().isMimeType("application/octet-stream"));
        assertFalse(mail.getMessage() == message);
        
        assertEquals("test 1,test 2", mail.getMessage().getHeader("X-Test", ","));
    }
    
    @Test
    public void testSMIMEAdaptMessageNoMultipart()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("test/resources/templates/blackberry-smime-adapter-no-multipart.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        mailetConfig.setInitParameter("directSizeLimit", "100");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptMessageNoMultipart", mail.getMessage());
        
        assertTrue(mail.getMessage().isMimeType("application/octet-stream"));
        assertFalse(mail.getMessage() == message);
        
        assertEquals("test 1,test 2", mail.getMessage().getHeader("X-Test", ","));
    }
        
    @Test
    public void testSMIMEAdaptMessageOverDirectLimit()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("resources/templates/blackberry-smime-adapter.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        mailetConfig.setInitParameter("directSizeLimit", "100");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptMessageOverDirectLimit", mail.getMessage());
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertFalse(mail.getMessage() == message);
        
        assertEquals("test 1,test 2", mail.getMessage().getHeader("X-Test", ","));
        
        MimeMultipart mp = (MimeMultipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        BodyPart bp = mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("text/plain"));

        bp = mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("application/octet-stream"));
        assertEquals("attachment.smime", bp.getFileName());
    }
    
    @Test
    public void testSMIMEAdaptCorruptMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("resources/templates/blackberry-smime-adapter.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-corrupt.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptCorruptMessage", mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("application/pkcs7-mime"));
        assertTrue(mail.getMessage() == message);
        
        assertEquals("test 3", mail.getMessage().getHeader("X-Test-a", ","));
    }

    @Test
    public void testSMIMEAdaptCorruptBase64Message()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("resources/templates/blackberry-smime-adapter.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/corrupt-base64.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptCorruptBase64Message", mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("text/plain"));
        assertTrue(mail.getMessage() == message);
    }
    
    @Test
    public void testSMIMEAdaptMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("resources/templates/blackberry-smime-adapter.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptMessage", mail.getMessage());
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertFalse(mail.getMessage() == message);
        
        MimeMultipart mp = (MimeMultipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        BodyPart bp = mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("text/plain"));

        bp = mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("application/octet-stream"));
        assertEquals("x-rimdevicesmime.p7m", bp.getFileName());
    }
    
    @Test
    public void testSMIMEAdaptMessageNoMarker()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new BlackberrySMIMEAdapter();

        String template = FileUtils.readFileToString(new File("test/resources/templates/blackberry-smime-adapter-no-marker.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "pdfTemplate", template);
        
        mailetConfig.setInitParameter("log", "starting");
        /* use some dummy template because we must specify the default template */
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "pdfTemplate");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("123@EXAMPLE.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        TestUtils.saveMessages(tempDir, "testSMIMEAdaptMessageNoMarker", mail.getMessage());
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertFalse(mail.getMessage() == message);
    }    
}
