/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;

import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class SenderDKIMVerifyTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private final static String KEY_PAIR_PEM = 
        "-----BEGIN RSA PRIVATE KEY-----\n" + 
        "MIICXgIBAAKBgQCrUoJ16657mK99O8WYddgMFJPmXDIO49SaJzOp1/sohNd1T6Xh\n" + 
        "WWZVGHEfioxz+HmUFLnekyw51NE64O9cpaiVoZTga9fn5WjtmQNglvcbnhWedeUZ\n" + 
        "PgTkuhbUHgZNtkwvtKyRnTlh7ZxthF6W33q2Bj1CETuS2WdRs/4AA4CNCwIDAQAB\n" + 
        "AoGAaOu3ChC0Yu03TDL26FADaCKSEVoVLhlJcr7fXPzwy/fPHAETTdc6XJMDdJWd\n" + 
        "PsjFbHLlAfKP+zriiHSJIuwxObFocxleeTDKil3QlqXMjqFfF4PUP1XIiZREgDlE\n" + 
        "Gq8nqhFgHeUfV8U7kBejvuhF6r4FfWO5fjgLBbh8u0dTZkECQQDaMPdbNV84daV6\n" + 
        "5nkzYUgWBRn0NjRuIsVY+ugyhNgUexmreGFlpcWrDGMQDU2b2xSR2FEAILM/pxag\n" + 
        "NzxrTFu3AkEAyQJryIY/tl3OD1jvAJLJmDQT0kf0AUcbrL4bL0rWABlH4dk2db6Y\n" + 
        "MyGi88HcJNLkoadBHqJWIBiFWXaGvrzBTQJBAJbgYUt6vpuGDqXLlWfID1batDXA\n" + 
        "/cRi2uBKsCGu5tRSw09k8MSfOu6qpB3HdTEe7zxivrA97HVJj0W+rFLt/EUCQQCy\n" + 
        "2w6gzKOgV3NkwJNZhUMPxTbl4tRA1s7PNBDoUcR9LgGB+k61EjRHOuTN1G9X7Lc3\n" + 
        "B6Wv5m6P/IGbCxX2Xen5AkEAs/mUhmBH3T7Xd1PyeGk6ycIGdu4t+mZc0BhhKfN8\n" + 
        "z3KElieQ+Jy4cXrI5cdPJZwdxDdD0BQMsGnMVNSxz9j8zg==\n" + 
        "-----END RSA PRIVATE KEY-----\n";
    
    private static AutoTransactDelegator proxy;
        
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();        

        proxy = AutoTransactDelegator.createProxy();
    }

    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    @Before
    public void before()
    throws Exception
    {
        proxy.setGlobalProperty("user.dkim.keyPair", KEY_PAIR_PEM);
    }
    
    @Test
    public void testVerifyRelaxedRelaxedTextMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMVerify();

        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-text-plain.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("verified", mail.getAttribute("djigzo.dkim.result"));
    }
    
    @Test
    public void testVerifyRelaxedRelaxedFailWrongKey()
    throws Exception
    {
        String OtherKey =
            "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIICXAIBAAKBgQDMyeGqS+rYHbGv31jeaN5jDdTgfeMqeCVLIfaOWZ96CIPu0w5j\n" +
            "Qup4wf5Cdhz4uRmLv+YRFzovyoYbsvCDKojkGRtIDPoihwwzSzF0MbGtpTN/AP/i\n" +
            "M1pNv2SflqEsXYdrTrUY+SR/RY3qgdCkD+B5Jc1vWcFQ/neImUPiWvgF+QIDAQAB\n" +
            "AoGAG7KycvYRrWlWvxLWUj6c1YjpYfOk5fmaEa9mSZqVl0vPjF2/dG14iRyz5wlS\n" +
            "odFnQx+RXn5lwFEEEnsBTLxUB0fo4HGE8jsOK6UKUxZsNNlOqXi2DwfLHxPhGtC6\n" +
            "A1SztOTPIRtzIyxTHNsHKWsdzIj4+8CIVSMIue9YzfJwEIECQQDq5xjWr27koSq4\n" +
            "mBNEqx6bOLDWTWqd5NP0vM5Z8rnTyxYMOeO5hE19RQytpSNHPs7bh+7Ogn+Bewzs\n" +
            "cwI6KzzXAkEA3y5l5qfvtOPomG8X5LpzJ3PLLmhO+0nXKddzYQwiogX9rZIkTsCU\n" +
            "r5yz1Ppkh7rcAC1WN/NVUZx0gq+u0k0prwJBAOciudgVC0LAKu80BFGfJyCI6cgU\n" +
            "qQHwNXctiMYNBjiWLn2dQKw8uJq4pL8ALPRfot90o9Bjq97WG2NVzy05ekUCQFyd\n" +
            "KHQ7JGHwYck/8K5eIQMyLhKn/n6Q+dTHL34KRyTtT4QDrUVw7UKiyI2NCsK4bCs2\n" +
            "pRy6waEIR+EjfmyO0MkCQD6yCgN3KO/903aRA/axPVMk/bDRvukRsY1685OJAbaI\n" +
            "fkcrtGKoND9Q/w93+/OQ4YkEMG3ddjWSnS3Dyz1TwJM=\n" +
            "-----END RSA PRIVATE KEY-----";
        
        proxy.setGlobalProperty("user.dkim.keyPair", OtherKey);
        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMVerify();

        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-text-plain.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("failed", mail.getAttribute("djigzo.dkim.result"));
    }
}
