/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class SetHMACHeaderTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static File saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
        
        return file;
    }
    
    @Test
    public void setHeaderTest()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SetHMACHeader();

        mailetConfig.setInitParameter("header", "X-HMAC");
        mailetConfig.setInitParameter("value", "test");
        mailetConfig.setInitParameter("secret", "123456");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "setHeaderTest.eml");

        MimeMessage result = MailUtils.loadMessage(resultFile);
        
        assertEquals("9D2BB116E1DF997FFE8A5139FC1D187F976C19579A138414A112BC2E39020EBA", 
                result.getHeader("X-HMAC", ", "));
    }

    @Test
    public void replaceExistingHeaderTest()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SetHMACHeader();

        mailetConfig.setInitParameter("header", "from");
        mailetConfig.setInitParameter("value", "test");
        mailetConfig.setInitParameter("secret", "123456");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");

        assertEquals("test@example.com", message.getHeader("from", ", "));

        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "replaceExistingHeaderTest.eml");

        MimeMessage result = MailUtils.loadMessage(resultFile);
        
        assertEquals("9D2BB116E1DF997FFE8A5139FC1D187F976C19579A138414A112BC2E39020EBA", 
                result.getHeader("from", ", "));
    }
    
    @Test
    public void setHeaderTestMissingSecret()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SetHMACHeader();

        mailetConfig.setInitParameter("header", "X-HMAC");
        mailetConfig.setInitParameter("value", "test");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "setHeaderTestMissingSecret.eml");

        MimeMessage result = MailUtils.loadMessage(resultFile);
        
        assertNull(result.getHeader("X-HMAC", ", "));
    }    
}
