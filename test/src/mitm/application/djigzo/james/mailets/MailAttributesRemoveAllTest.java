/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.util.Iterator;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailSession;

import org.apache.log4j.PropertyConfigurator;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class MailAttributesRemoveAllTest
{
    private static String log4jConfig = "conf/log4j.properties";

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        PropertyConfigurator.configure(log4jConfig);        
    }
        
    @Test
    public void testRemoveAttributes()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new MailAttributesRemoveAll();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
                
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        mail.setAttribute("a", "1");
        mail.setAttribute("b", "2");
        
        assertEquals("1", mail.getAttribute("a"));
        assertEquals("2", mail.getAttribute("b"));

        mailet.service(mail);

        assertNull(mail.getAttribute("a"));
        assertNull(mail.getAttribute("b"));

        Iterator<?> it = mail.getAttributeNames();
        
        assertFalse(it.hasNext());
    }
}
