/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.james.jdkim.DKIMCommon;
import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class SenderDKIMSignTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    private final static String KEY_PAIR_PEM = 
        "-----BEGIN RSA PRIVATE KEY-----\n" + 
        "MIICXgIBAAKBgQCrUoJ16657mK99O8WYddgMFJPmXDIO49SaJzOp1/sohNd1T6Xh\n" + 
        "WWZVGHEfioxz+HmUFLnekyw51NE64O9cpaiVoZTga9fn5WjtmQNglvcbnhWedeUZ\n" + 
        "PgTkuhbUHgZNtkwvtKyRnTlh7ZxthF6W33q2Bj1CETuS2WdRs/4AA4CNCwIDAQAB\n" + 
        "AoGAaOu3ChC0Yu03TDL26FADaCKSEVoVLhlJcr7fXPzwy/fPHAETTdc6XJMDdJWd\n" + 
        "PsjFbHLlAfKP+zriiHSJIuwxObFocxleeTDKil3QlqXMjqFfF4PUP1XIiZREgDlE\n" + 
        "Gq8nqhFgHeUfV8U7kBejvuhF6r4FfWO5fjgLBbh8u0dTZkECQQDaMPdbNV84daV6\n" + 
        "5nkzYUgWBRn0NjRuIsVY+ugyhNgUexmreGFlpcWrDGMQDU2b2xSR2FEAILM/pxag\n" + 
        "NzxrTFu3AkEAyQJryIY/tl3OD1jvAJLJmDQT0kf0AUcbrL4bL0rWABlH4dk2db6Y\n" + 
        "MyGi88HcJNLkoadBHqJWIBiFWXaGvrzBTQJBAJbgYUt6vpuGDqXLlWfID1batDXA\n" + 
        "/cRi2uBKsCGu5tRSw09k8MSfOu6qpB3HdTEe7zxivrA97HVJj0W+rFLt/EUCQQCy\n" + 
        "2w6gzKOgV3NkwJNZhUMPxTbl4tRA1s7PNBDoUcR9LgGB+k61EjRHOuTN1G9X7Lc3\n" + 
        "B6Wv5m6P/IGbCxX2Xen5AkEAs/mUhmBH3T7Xd1PyeGk6ycIGdu4t+mZc0BhhKfN8\n" + 
        "z3KElieQ+Jy4cXrI5cdPJZwdxDdD0BQMsGnMVNSxz9j8zg==\n" + 
        "-----END RSA PRIVATE KEY-----\n";
    
    private static AutoTransactDelegator proxy;
        
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();        

        proxy = AutoTransactDelegator.createProxy();
    }

    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    private static File saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
        
        return file;
    }
    
    private boolean verify(File file, String dkimHeader)
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        mailetConfig.setInitParameter("dkimHeader", dkimHeader);
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(file);
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        return "verified".equals(mail.getAttribute("djigzo.dkim.result"));
    }

    private boolean verify(File file)
    throws Exception
    {
        return verify(file, DKIMCommon.DEFAULT_DKIM_HEADER);
    }

    @Before
    public void before()
    throws Exception
    {
        proxy.setGlobalProperty("user.dkim.keyPair", KEY_PAIR_PEM);
    }
    
    @Test
    public void testSign()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMSign();

        mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                "h=from:to; a=rsa-sha256; bh=; b=;");
        mailetConfig.setInitParameter("foldSignature", "true");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "testSignSenderDKIMSignTest.eml");
        
        assertTrue(verify(resultFile));
    }

    @Test
    public void testSignNoFromSigned()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMSign();

        mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=djigzo; " +
                "h=X-Unknown-Header; a=rsa-sha256; bh=; b=;");
        mailetConfig.setInitParameter("foldSignature", "true");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        // now change from. Should not change the signature
        mail.getMessage().setFrom(new InternetAddress("test@unkown.tld"));
        
        File resultFile = saveMessage(mail.getMessage(), "testSignNoFromSigned.eml");
        
        assertTrue(verify(resultFile));
    }    
    
    @Test
    public void testSignCustomHeader()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMSign();

        mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                "h=from:to; a=rsa-sha256; bh=; b=;");
        mailetConfig.setInitParameter("foldSignature", "true");
        mailetConfig.setInitParameter("dkimHeader", "X-X");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "testSignCustomHeader.eml");
        
        assertTrue(verify(resultFile, "X-X"));
    }
    
    @Test
    public void testSignShouldFailMissingKey()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMSign();

        mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                "h=from:to; a=rsa-sha256; bh=; b=;");
        mailetConfig.setInitParameter("foldSignature", "true");
        mailetConfig.setInitParameter("keyProperty", "***");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("simple-text-message.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "testSignShouldFailMissingKey.eml");
        
        assertFalse(verify(resultFile));
    }
    
    @Test
    public void testSignNoFromInMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new SenderDKIMSign();

        mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=djigzo; " +
                "h=X-Unknown-Header; a=rsa-sha256; bh=; b=;");
        mailetConfig.setInitParameter("foldSignature", "true");
        mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setSubject("test");
        message.setContent("test", "text/plain");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        File resultFile = saveMessage(mail.getMessage(), "testSignNoFromInMessage.eml");
        
        assertTrue(verify(resultFile));
    }    
    
    @Test
    public void testSpeed()
    throws Exception
    {
        int repeat = 100;
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < repeat; i++)
        {
            MockMailetConfig mailetConfig = new MockMailetConfig("test");

            Mailet mailet = new SenderDKIMSign();

            mailetConfig.setInitParameter("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                    "h=from:to; a=rsa-sha256; bh=; b=;");
            mailetConfig.setInitParameter("foldSignature", "true");
            mailetConfig.setInitParameter("keyProperty", "user.dkim.keyPair");
            
            mailet.init(mailetConfig);
            
            MockMail mail = new MockMail();
            
            MimeMessage message = loadMessage("simple-text-message.eml");
            
            mail.setMessage(message);
            
            mailet.service(mail);
        }
        
        double signsPerSec = repeat * 1000.0 / (System.currentTimeMillis() - start);
        
        System.out.println("Signs/second: " + signsPerSec);
        
        assertTrue("can fail in slower systems.", signsPerSec > 30);
    }
    
    /*
     * Test if parsing a PEM keypair is fast enough
     */
    @Test
    public void testSpeedParsePEM()
    throws Exception
    {
        int repeat = 10000;
        
        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++) {
            assertNotNull(SenderDKIMSign.parseKey(KEY_PAIR_PEM));
        }
        
        double perSec = repeat * 1000.0 / (System.currentTimeMillis() - start);
        
        System.out.println("parse/second: " + perSec);
        
        assertTrue("can fail in slower systems.", perSec > 3000);
    }
}
