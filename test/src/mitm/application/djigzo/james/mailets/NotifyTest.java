/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.MailAddressUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.james.mock.SendMailEventListenerImpl;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;
import mitm.test.TestUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.smtpserver.SMTPSession;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class NotifyTest
{
    private final static File testBase = new File("test/resources/testdata");

    private static final File tempDir = new File("test/tmp");
    
	private AutoTransactDelegator autoTransactDelegator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
    }
    
    @Before
    public void setup()
    throws Exception
    {
    	autoTransactDelegator = AutoTransactDelegator.createProxy();
    	
    	autoTransactDelegator.deleteAllUsers();
    }
    
    @Test
    public void testNotificationInvalidFromToEtc()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "${originator}, invalid@invalid.tld, a@b.c, ${invalid}");
        mailetConfig.setInitParameter("passThroughProcessor", "nextProcessor");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setAttribute("invalid", new Object[]{"**", EmailAddressUtils.INVALID_EMAIL, 
                new MailAddress(EmailAddressUtils.INVALID_EMAIL), 
                new InternetAddress(EmailAddressUtils.INVALID_EMAIL),
                new InternetAddress("***", false)});

        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/invalid-from-to-cc-reply-to.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("a@b.c", StringUtils.join(listener.getRecipients().get(0), ","));
        assertEquals("nextProcessor", mail.getState());

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testNotificationPassThroughProcessor()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "${originator}");
        mailetConfig.setInitParameter("passThroughProcessor", "nextProcessor");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("test@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertEquals("nextProcessor", mail.getState());

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testInvalidEncoding()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/pdf-attachment-invalid-encoding.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "notifyTemplate", template);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${originator}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("test@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertNull(mail.getState());
        assertNotNull(listener.getMessages().get(0).getHeader("X-pdf-template-test"));

        MailUtils.validateMessage(listener.getMessages().get(0));

        MailUtils.writeMessage(listener.getMessages().get(0), new File(tempDir, "testNotifyInvalidEncoding.eml"));        
    }    
    
    @Test
    public void testEncryptionNotification()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "aap@example.com, noot@example.com");
        mailetConfig.setInitParameter("to", "abc@example.com, def@example.com");
        mailetConfig.setInitParameter("from", "from@example.com");
        mailetConfig.setInitParameter("replyTo", "reply@example.com");
        mailetConfig.setInitParameter("subject", "some subject");
        mailetConfig.setInitParameter("sender", "sender@example.com");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("aap@example.com,noot@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertEquals("sender@example.com", listener.getSenders().get(0).toString());
        assertEquals("<from@example.com>", listener.getMessages().get(0).getHeader("from", ","));
        assertEquals("<abc@example.com>, <def@example.com>", listener.getMessages().get(0).getHeader("to", ","));
        assertEquals("The message has been encrypted", listener.getMessages().get(0).getSubject());
        assertNull(mail.getState());

        MailUtils.validateMessage(listener.getMessages().get(0));
    }    

    @Test
    public void testNotificationCloneAttributes()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "aap@example.com, noot@example.com");
        mailetConfig.setInitParameter("to", "abc@example.com, def@example.com");
        mailetConfig.setInitParameter("from", "from@example.com");
        mailetConfig.setInitParameter("replyTo", "reply@example.com");
        mailetConfig.setInitParameter("subject", "some subject");
        mailetConfig.setInitParameter("sender", "sender@example.com");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setAttribute("XXX", "123");
        mail.setAttribute(SMTPSession.SMTP_MAIL_EXTENSIONS_ATTRIBUTE_NAME, "AAP=NOOT");
        assertEquals("AAP=NOOT", mail.getAttribute(SMTPSession.SMTP_MAIL_EXTENSIONS_ATTRIBUTE_NAME));
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("aap@example.com,noot@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertEquals("sender@example.com", listener.getSenders().get(0).toString());
        assertEquals("<from@example.com>", listener.getMessages().get(0).getHeader("from", ","));
        assertEquals("<abc@example.com>, <def@example.com>", listener.getMessages().get(0).getHeader("to", ","));
        assertEquals("The message has been encrypted", listener.getMessages().get(0).getSubject());
        assertNull(mail.getState());
        
        Mail check = listener.getMails().get(0);
        
        assertEquals("123", check.getAttribute("XXX"));
        assertNull(check.getAttribute(SMTPSession.SMTP_MAIL_EXTENSIONS_ATTRIBUTE_NAME));
        assertEquals("AAP=NOOT", mail.getAttribute(SMTPSession.SMTP_MAIL_EXTENSIONS_ATTRIBUTE_NAME));

        MailUtils.validateMessage(listener.getMessages().get(0));
    }    
    
    @Test
    public void testEncryptionNotificationOriginator()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "${originator}");
        mailetConfig.setInitParameter("to", "${sameAsRecipients}");
        mailetConfig.setInitParameter("from", "${null}");
        mailetConfig.setInitParameter("subject", "${sameAsMessage}");
        mailetConfig.setInitParameter("processor", "testProcessor");
        mailetConfig.setInitParameter("passThrough", "false");
        mailetConfig.setInitParameter("sender", "${ null}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals("testProcessor", listener.getStates().get(0));
        assertEquals("test@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertNull(listener.getSenders().get(0));
        assertEquals("<>", listener.getMessages().get(0).getHeader("from", ","));
        assertEquals("The message has been encrypted", listener.getMessages().get(0).getSubject());
        assertEquals(Mail.GHOST, mail.getState());
        assertNull(listener.getMessages().get(0).getHeader("X-pdf-template-test"));
        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testEncryptionNotificationInvalidRecipients()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "xxx");
        mailetConfig.setInitParameter("to", "sameAsRecipients");
        mailetConfig.setInitParameter("from", "null");
        mailetConfig.setInitParameter("subject", "sameAsMessage");
        mailetConfig.setInitParameter("processor", "testProcessor");
        mailetConfig.setInitParameter("passThrough", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(0, listener.getSenders().size());
        assertEquals(0, listener.getRecipients().size());
        assertEquals(0, listener.getStates().size());
        assertEquals(0, listener.getMessages().size());
        assertEquals(0, listener.getMails().size());
        
        assertNull(mail.getState());
    }
    
    @Test
    public void testNotificationUserTemplateProperty()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/pdf-attachment.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "notifyTemplate", template);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${originator}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("test@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertNull(mail.getState());
        assertNotNull(listener.getMessages().get(0).getHeader("X-pdf-template-test"));

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testNotificationUserTemplatePropertyUnknown()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/pdf-attachment.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "notifyTemplate", template);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "${originator}");
        mailetConfig.setInitParameter("to", "abc@example.com, def@example.com");
        mailetConfig.setInitParameter("from", "from@example.com");
        mailetConfig.setInitParameter("replyTo", "reply@example.com");
        mailetConfig.setInitParameter("subject", "some subject");
        mailetConfig.setInitParameter("sender", "sender@example.com");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("test@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertNull(mail.getState());
        assertEquals("sender@example.com", listener.getSenders().get(0).toString());
        assertEquals("<from@example.com>", listener.getMessages().get(0).getHeader("from", ","));
        assertEquals("<abc@example.com>, <def@example.com>", listener.getMessages().get(0).getHeader("to", ","));
        assertEquals("The message has been encrypted", listener.getMessages().get(0).getSubject());
        assertNull(listener.getMessages().get(0).getHeader("X-pdf-template-test"));

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testUserPropertyInMailetConfig()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@example.com", "test.user.property.1", "TEST #{test } VALUE");
        autoTransactDelegator.setProperty("test@example.com", "test.user.property.2", "TEST 2");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${originator}");
        mailetConfig.setInitParameter("userProperty", "test.user.property.1");
        mailetConfig.setInitParameter("userProperty", "test.user.property.2");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals("[test@example.com]", listener.getRecipients().get(0).toString());
        
        MimeMessage notification = listener.getMessages().get(0);

        MailUtils.validateMessage(notification);
        
        String content = (String) notification.getContent();
        
        assertEquals("TEST #{test } VALUE\nTEST 2\nnon existing property not set\n", content);
    }    
    
    @Test
    public void testSpecialUserPropertyRecipient()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "prop.recipient", 
                "recipient.from.property@example.com, Martijn Brinkers <martijn@djigzo.com>");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${originator}, #{prop.recipient}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        assertEquals("test@example.com,recipient.from.property@example.com,martijn@djigzo.com", 
                StringUtils.join(listener.getRecipients().get(0), ","));
        
        MimeMessage notification = listener.getMessages().get(0);

        MailUtils.validateMessage(notification);
        
        String content = (String) notification.getContent();
        
        assertEquals("property 1 not set\nproperty 2 not set\nnon existing property not set\n", content);
    }
    
    @Test
    public void testSpecialUserPropertyRecipientUnknown()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${replyTo}, #{prop.recipient}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/chinese-text.eml"));
        
        message.setFrom(new InternetAddress("test@example.com"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals("[123@example.com]", MailAddressUtils.getRecipients(mail).toString());
        assertTrue(TestUtils.isEqual(message, mail.getMessage()));
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        assertEquals("[users@tapestry.apache.org]", listener.getRecipients().get(0).toString());
        
        MimeMessage notification = listener.getMessages().get(0);

        MailUtils.validateMessage(notification);
        
        String content = (String) notification.getContent();
        
        assertEquals("property 1 not set\nproperty 2 not set\nnon existing property not set\n", content);
    }    
    
    @Test
    public void testSpecialUserPropertyRecipientInvalidEmail()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "prop.recipient", "INVALID,     martijn@djigzo.com");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${originator}, #{prop.recipient}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());

        assertEquals("[123@example.com, m.brinkers@pobox.com]", MailAddressUtils.getRecipients(mail).toString());
        assertTrue(TestUtils.isEqual(message, mail.getMessage()));
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        assertEquals("[test@example.com, martijn@djigzo.com]", 
                listener.getRecipients().get(0).toString());
        
        MimeMessage notification = listener.getMessages().get(0);

        MailUtils.validateMessage(notification);
        
        String content = (String) notification.getContent();
        
        assertEquals("property 1 not set\nproperty 2 not set\nnon existing property not set\n", content);
    }  
    
    @Test
    public void testSpecialUserPropertyRecipientNoRecipients()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "prop.recipient", "INVALID,  ***");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "#{prop.recipient}");
        mailetConfig.setInitParameter("passThrough", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(0, listener.getSenders().size());
        assertEquals(0, listener.getRecipients().size());
        assertEquals(0, listener.getStates().size());
        assertEquals(0, listener.getMessages().size());
        assertEquals(0, listener.getMails().size());
        
        assertTrue(TestUtils.isEqual(message, mail.getMessage()));
        assertNull(mail.getState());
    }
    
    @Test
    public void testNoRecipientsIgnore()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test-user-property.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "prop.recipient", "INVALID,  ***");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "#{prop.recipient}");
        mailetConfig.setInitParameter("passThrough", "false");
        mailetConfig.setInitParameter("ignoreMissingRecipients", "true");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(0, listener.getSenders().size());
        assertEquals(0, listener.getRecipients().size());
        assertEquals(0, listener.getStates().size());
        assertEquals(0, listener.getMessages().size());
        assertEquals(0, listener.getMails().size());
        
        assertTrue(TestUtils.isEqual(message, mail.getMessage()));
        assertEquals(Mail.GHOST, mail.getState());
    }    
    
    @Test
    public void testNotificationToCc()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);

        String template = FileUtils.readFileToString(new File("test/resources/templates/test.ftl"));
        
        autoTransactDelegator.setProperty("test@example.com", "notifyTemplate", template);
        autoTransactDelegator.setProperty("test@EXAMPLE.com", "prop.recipient", "INVALID,     martijn@djigzo.com");
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "sms.ftl");
        mailetConfig.setInitParameter("templateProperty", "notifyTemplate");
        mailetConfig.setInitParameter("recipients", "${sender}, a@b.c");
        mailetConfig.setInitParameter("to", "${sameAsMessage},  #{prop.recipient}");
        mailetConfig.setInitParameter("cc", "${sameAsRecipients}, other@other.com, #{prop.recipient}, #{NONEXISTING}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        recipients.add(new MailAddress("123@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("a-bc@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        assertEquals("a-bc@example.com,a@b.c", StringUtils.join(listener.getRecipients().get(0), ","));
        assertEquals("test@example.com, martijn@djigzo.com", listener.getMessages().get(0).getHeader("to", ","));
        assertEquals("a-bc@example.com, a@b.c, other@other.com, martijn@djigzo.com", listener.getMessages().get(0).getHeader("Cc", ","));
        assertNull(mail.getState());

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testMailAttr()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "${attr1}, ${attr2}, ${non_exsisting}, direct@example.com,   ${attr3}");
        mailetConfig.setInitParameter("to", "${sameAsRecipients}");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        mail.setMessage(message);
        
        mail.setAttribute("attr1", new Object[]{"aap@example.com", new MailAddress("noot@example.com")});
        mail.setAttribute("attr2", new Object[]{"mies@example.com", new InternetAddress("teun@example.com")});
        mail.setAttribute("attr3", "schapen@example.com");
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("m.brinkers@pobox.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(1, listener.getSenders().size());
        assertEquals(1, listener.getRecipients().size());
        assertEquals(1, listener.getStates().size());
        assertEquals(1, listener.getMessages().size());
        assertEquals(1, listener.getMails().size());
        
        assertEquals(Mail.DEFAULT, listener.getStates().get(0));
        
        assertEquals("aap@example.com,noot@example.com,mies@example.com,teun@example.com,direct@example.com," +
        		"schapen@example.com", StringUtils.join(listener.getRecipients().get(0), ","));
        assertNull(listener.getSenders().get(0));
        assertEquals("<>", listener.getMessages().get(0).getHeader("from", ","));
        assertEquals("The message has been encrypted", listener.getMessages().get(0).getSubject());
        assertNull(mail.getState());

        MailUtils.validateMessage(listener.getMessages().get(0));
    }
    
    @Test
    public void testInvalidOriginator()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Notify mailet = new Notify();
        
        mailetConfig.setInitParameter("template", "encryption-notification.ftl");
        mailetConfig.setInitParameter("recipients", "originator");
        mailetConfig.setInitParameter("processor", "testProcessor");
        mailetConfig.setInitParameter("passThrough", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test\n", "text/plain");
        message.setHeader("From", "!@#$%^&*(");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("recipient@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(0, listener.getSenders().size());
        assertEquals(0, listener.getRecipients().size());
        assertEquals(0, listener.getStates().size());
        assertEquals(0, listener.getMessages().size());
        assertEquals(0, listener.getMails().size());
        
        assertNull(mail.getState());
    }    
}
