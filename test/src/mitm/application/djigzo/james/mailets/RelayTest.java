/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.NamedCertificateCategories;
import mitm.application.djigzo.NamedCertificatesUserPreferencesManager;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.james.mock.SendMailEventListener;
import mitm.application.djigzo.james.mock.SendMailEventListenerImpl;
import mitm.application.djigzo.relay.RelayBounceMode;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certificate.CertificateUtils;
import mitm.test.TestUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RelayTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static CertificateWorkflow rootWorkflow;

    private static KeyAndCertificateWorkflow keyAndCertificateWorkflow;
    
    private static KeyAndCertStore certStore;

    private static ExtendedAutoTransactDelegator autoTransactDelegator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();   

        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        rootWorkflow = SystemServices.getKeyAndRootCertificateWorkflow();
        keyAndCertificateWorkflow = SystemServices.getKeyAndCertificateWorkflow();
        certStore = SystemServices.getKeyAndCertStore();
    }

    @Before
    public void setup()
    throws Exception
    {
        autoTransactDelegator = AutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class);
        
        autoTransactDelegator.deleteAllUsers();
        autoTransactDelegator.cleanKeyAndCertStore();
        autoTransactDelegator.cleanKeyAndRootStore();
    }
    
    private static void importCertificates(CertificateWorkflow rootWorkflow, File certificateFile) 
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate) {
                rootWorkflow.addCertificateTransacted((X509Certificate) certificate);
            }
        }
    }

    private static void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password) 
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

        keyAndCertificateWorkflow.importKeyStore(keyStore, KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);        
    }
    
    private static void addDefaultTestCertificates()
    throws Exception
    {
        importCertificates(rootWorkflow, new File(testBase, "certificates/mitm-test-root.cer"));
        importCertificates(keyAndCertificateWorkflow, new File(testBase, "certificates/mitm-test-ca.cer"));        
        importCertificates(keyAndCertificateWorkflow, new File(testBase, "certificates/testCertificates.p7b"));        
    }
    
    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
        @StartTransaction
        public void addUser(String email, Collection<X509Certificate> relayCertificates, boolean relayAllowed, 
                long relayValidityInterval, RelayBounceMode bounceMode) 
        throws AddressException, HierarchicalPropertiesException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

            User user = userWorkflow.addUser(email);

            UserPreferences preferences = user.getUserPreferences(); 

            UserProperties properties = preferences.getProperties();

            properties.setRelayAllowed(relayAllowed);
            properties.setRelayBounceMode(bounceMode);
            properties.setRelayValidityInterval(relayValidityInterval);

            NamedCertificatesUserPreferencesManager namedCertificatesManager = 
                new NamedCertificatesUserPreferencesManager(NamedCertificateCategories.RELAY);
            
            namedCertificatesManager.setNamedCertificates(preferences, relayCertificates);
            
            userWorkflow.makePersistent(user);            
        }
    }    

    @Test
    public void testRelayReplayPrevention()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        mailet.service(mail);
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(1, sendMessages.size());
    }
    
    @Test(expected = MessagingException.class)
    public void testRelayInvalidSender()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("sender", "$");
        
        mailet.init(mailetConfig);
    }
    
    @Test
    public void testRelaySenderNotSpecialAddress()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("sender", "someother@example.com");
        mailetConfig.setInitParameter("preventReplay", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        Mail sendMail = listener.getMails().get(0);
        
        assertEquals("someother@example.com", sendMail.getSender().toString());
    }
    
    @Test
    public void testRelaySameAsRelay()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("sender", "sameAsRelay");
        mailetConfig.setInitParameter("preventReplay", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        Mail sendMail = listener.getMails().get(0);
        
        assertEquals("m.brinkers@pobox.com", sendMail.getSender().toString());
    }
    
    @Test
    public void testRelayNullSender()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("sender", "null");
        mailetConfig.setInitParameter("preventReplay", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        Mail sendMail = listener.getMails().get(0);
        
        assertEquals(null, sendMail.getSender());
    }
    
    @Test
    public void testRelayCertificateNotValidForSigning()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayCertificateNotForSigning.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
    
    @Test
    public void testRelayCertificateNotTrusted()
    throws Exception
    {        
        importCertificates(rootWorkflow, new File(testBase, "certificates/mitm-test-root.cer"));
        importCertificates(keyAndCertificateWorkflow, new File(testBase, "certificates/testCertificates.p7b"));        
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
    
    @Test
    public void testExpiredBounce()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();

        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                1, RelayBounceMode.ON_RELAYING_DENIED);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertEquals("bounce", mail.getState());
    }
    
    @Test
    public void testRelayInvalidRecipientBounceTest()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.ON_INVALID_RECIPIENT);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayInvalidRecipient.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertEquals("bounce", mail.getState());
    }
    
    @Test
    public void testRelayInvalidRecipient()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayInvalidRecipient.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
    
    @Test
    public void testNoRelayEmail()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();

        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", null, true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-djigzo.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }    
    
    @Test
    public void testNoRelayCertificate()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();

        X509Certificate relayCert = TestUtils.loadCertificate(new File(testBase, "certificates/intel-corp-basic.cer"));
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", Collections.singleton(relayCert), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
    
    @Test
    public void testRelayNotAllowed()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), false /* not allowed */, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
        
    @Test
    public void testRelay()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("preventReplay", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(1, sendMessages.size());
        
        message = sendMessages.get(0);
        
        assertTrue(message.isMimeType("text/plain"));
        
        Mail sendMail = listener.getMails().get(0);
        
        assertEquals("relay", sendMail.getState());
        assertEquals("martijn_brinkers@mobileemail.vodafone.nl", sendMail.getSender().toString());

        assertEquals(Mail.GHOST, mail.getState());
    }
    
    @Test
    public void testRelayPassThrough()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        mailetConfig.setInitParameter("passThrough", "true");
        mailetConfig.setInitParameter("preventReplay", "false");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState("123");
        
        autoTransactDelegator.addUser("martijn_brinkers@mobileemail.vodafone.nl", certStore.getCertificates(null), true, 
                0, RelayBounceMode.NEVER);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("ca@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(1, sendMessages.size());
        
        message = sendMessages.get(0);
        
        assertTrue(message.isMimeType("text/plain"));
        
        Mail sendMail = listener.getMails().get(0);
        
        assertEquals("relay", sendMail.getState());
        assertEquals("123", mail.getState());
    }
    
    @Test
    public void testRelayEncryptedMessageNoDecryptionKeyAvailable()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-encrypted-rfc822.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
    
    @Test
    public void testRelayEncryptedMessageDecryptionKeyAvailable()
    throws Exception
    {        
        addDefaultTestCertificates();
        
        importKeyStore(keyAndCertificateWorkflow, new File("test/resources/testdata/keys/testCertificates.p12"), "test");

        MockMailetConfig mailetConfig = new MockMailetConfig();

        SendMailEventListener listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Relay mailet = new Relay();
        
        mailetConfig.setInitParameter("relayProcessor", "relay");
        mailetConfig.setInitParameter("bounceProcessor", "bounce");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-encrypted-rfc822.eml"));
        
        Set<MailAddress> recipients = new HashSet<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setMessage(message);
        mail.setRecipients(recipients);
        mail.setSender(new MailAddress("m.brinkers@pobox.com"));
        
        mailet.service(mail);
        
        List<MimeMessage> sendMessages = listener.getMessages();
        
        assertEquals(0, sendMessages.size());
        
        assertEquals(message, mail.getMessage());
        assertNull(mail.getState());
    }
}
