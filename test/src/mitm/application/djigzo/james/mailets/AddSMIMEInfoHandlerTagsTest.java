/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.SecurityInfoTags;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;

import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class AddSMIMEInfoHandlerTagsTest
{
    private AutoTransactDelegator autoTransactDelegator;
    
    private static MimeMessage message;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
        
        message = MailUtils.loadMessage(new File("test/resources/testdata/mail/simple-text-message.eml"));
    }
    
    @Before
    public void setup()
    throws Exception
    {
        autoTransactDelegator = AutoTransactDelegator.createProxy();
        
        autoTransactDelegator.deleteAllUsers();
    }

    @Test
    public void testUserProperties()
    throws Exception
    {
        autoTransactDelegator.setProperty("test@example.com", "user.securityInfo.decryptedTag", "[Xdecrypted]");
        autoTransactDelegator.setProperty("test@example.com", "user.securityInfo.signedValidTag", "[Xvalid]");
        autoTransactDelegator.setProperty("test@example.com", "user.securityInfo.signedByValidTag", "[Xsigned By: %s]");
        autoTransactDelegator.setProperty("test@example.com", "user.securityInfo.signedInvalidTag", "[Xinvalid]");
        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new AddSMIMEInfoHandlerTags();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("recipient@example.com"));
        
        mail.setRecipients(recipients);
        
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        SecurityInfoTags tags = DjigzoMailAttributesImpl.getInstance(mail).getSecurityInfoTags();
        
        assertNotNull(tags);
        
        assertEquals("[Xdecrypted]", tags.getDecryptedTag());
        assertEquals("[Xvalid]", tags.getSignedValidTag());
        assertEquals("[Xsigned By: %s]", tags.getSignedByValidTag());
        assertEquals("[Xinvalid]", tags.getSignedInvalidTag());
    }
    
    @Test
    public void testInherited()
    throws Exception
    {
        autoTransactDelegator.setProperty("test@example.com", "user.securityInfo.decryptedTag", "[123]");
        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new AddSMIMEInfoHandlerTags();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("recipient@example.com"));
        
        mail.setRecipients(recipients);
        
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        SecurityInfoTags tags = DjigzoMailAttributesImpl.getInstance(mail).getSecurityInfoTags();
        
        assertNotNull(tags);
        
        assertEquals("[123]", tags.getDecryptedTag());
        assertEquals("[signed]", tags.getSignedValidTag());
        assertEquals("[signed by: %s]", tags.getSignedByValidTag());
        assertEquals("[invalid signature!]", tags.getSignedInvalidTag());
    }
    
    @Test
    public void testNoUser()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new AddSMIMEInfoHandlerTags();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("recipient@example.com"));
        
        mail.setRecipients(recipients);
        
        mail.setSender(new MailAddress("test@example.com"));
        
        mailet.service(mail);
        
        SecurityInfoTags tags = DjigzoMailAttributesImpl.getInstance(mail).getSecurityInfoTags();
        
        assertNotNull(tags);
        
        assertEquals("[decrypted]", tags.getDecryptedTag());
        assertEquals("[signed]", tags.getSignedValidTag());
        assertEquals("[signed by: %s]", tags.getSignedByValidTag());
        assertEquals("[invalid signature!]", tags.getSignedInvalidTag());
    }        
}
