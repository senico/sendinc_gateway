/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.util.CRLFInputStream;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class DKIMVerifyTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private final static String KEY_PAIR_PEM = 
        "-----BEGIN RSA PRIVATE KEY-----\n" + 
        "MIICXgIBAAKBgQCrUoJ16657mK99O8WYddgMFJPmXDIO49SaJzOp1/sohNd1T6Xh\n" + 
        "WWZVGHEfioxz+HmUFLnekyw51NE64O9cpaiVoZTga9fn5WjtmQNglvcbnhWedeUZ\n" + 
        "PgTkuhbUHgZNtkwvtKyRnTlh7ZxthF6W33q2Bj1CETuS2WdRs/4AA4CNCwIDAQAB\n" + 
        "AoGAaOu3ChC0Yu03TDL26FADaCKSEVoVLhlJcr7fXPzwy/fPHAETTdc6XJMDdJWd\n" + 
        "PsjFbHLlAfKP+zriiHSJIuwxObFocxleeTDKil3QlqXMjqFfF4PUP1XIiZREgDlE\n" + 
        "Gq8nqhFgHeUfV8U7kBejvuhF6r4FfWO5fjgLBbh8u0dTZkECQQDaMPdbNV84daV6\n" + 
        "5nkzYUgWBRn0NjRuIsVY+ugyhNgUexmreGFlpcWrDGMQDU2b2xSR2FEAILM/pxag\n" + 
        "NzxrTFu3AkEAyQJryIY/tl3OD1jvAJLJmDQT0kf0AUcbrL4bL0rWABlH4dk2db6Y\n" + 
        "MyGi88HcJNLkoadBHqJWIBiFWXaGvrzBTQJBAJbgYUt6vpuGDqXLlWfID1batDXA\n" + 
        "/cRi2uBKsCGu5tRSw09k8MSfOu6qpB3HdTEe7zxivrA97HVJj0W+rFLt/EUCQQCy\n" + 
        "2w6gzKOgV3NkwJNZhUMPxTbl4tRA1s7PNBDoUcR9LgGB+k61EjRHOuTN1G9X7Lc3\n" + 
        "B6Wv5m6P/IGbCxX2Xen5AkEAs/mUhmBH3T7Xd1PyeGk6ycIGdu4t+mZc0BhhKfN8\n" + 
        "z3KElieQ+Jy4cXrI5cdPJZwdxDdD0BQMsGnMVNSxz9j8zg==\n" + 
        "-----END RSA PRIVATE KEY-----\n";
    
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        BasicConfigurator.configure();

        InitializeBouncycastle.initialize();
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(new CRLFInputStream(new FileInputStream(mail)));
        
        return message;
    }

    @Test
    public void testVerifyRelaxedRelaxedTextMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-text-plain.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("verified", mail.getAttribute("djigzo.dkim.result"));
    }
    
    @Test
    public void testVerifySimpleSimpleTextMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-simple-simple-text-plain.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("verified", mail.getAttribute("djigzo.dkim.result"));
    }    
    
    @Test
    public void testVerifyRelaxedRelaxedTextWhitespaceAdded()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-text-plain-whitespace-added.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("verified", mail.getAttribute("djigzo.dkim.result"));
    }
    
    @Test
    public void testVerifyRelaxedRelaxedTampered()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-text-tampered.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("failed", mail.getAttribute("djigzo.dkim.result"));
    } 
    
    @Test
    public void testVerifyRelaxedRelaxedFailMissingHeader()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-fail-missing-header.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("failed", mail.getAttribute("djigzo.dkim.result"));
    }
    
    @Test
    public void testVerifyRelaxedRelaxedFailHeaderChanged()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new DKIMVerify();

        mailetConfig.setInitParameter("publicKey", KEY_PAIR_PEM);
        mailetConfig.setInitParameter("resultAttribute", "djigzo.dkim.result");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("dkim-relaxed-relaxed-fail-changed-header.eml");
        
        mail.setMessage(message);
        
        mailet.service(mail);
        
        assertEquals("failed", mail.getAttribute("djigzo.dkim.result"));
    } 
}
