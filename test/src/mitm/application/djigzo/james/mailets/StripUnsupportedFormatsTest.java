/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.james.mock.SendMailEventListenerImpl;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class StripUnsupportedFormatsTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    @BeforeClass
    public static void setUpBeforeClass() 
    {
        BasicConfigurator.configure();
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }

    /*
     * Test what happens when an invalid message (a message that fails on writeTo) is used. 
     */
    @Test
    public void testCorruptMessage()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "32768");
        mailetConfig.setInitParameter("contentWildcard.1", "application/octet-stream");
        mailetConfig.setInitParameter("filenameWildcard.1", "*.blabla");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        MimeBodyPart emptyPart = new MimeBodyPart();
        
        message.setContent(emptyPart, "text/plain");
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        assertEquals(message, mail.getMessage());
        
        try {
            MailUtils.validateMessage(message);

            fail();
        }
        catch(IOException e) {
            // expected. The message should be corrupt
        }        
    }    
    
    /*
     * This test tests whether a email template message with unknown content-transfer-encoding can be used. Javamail
     * prior to 1.4.4 (i.e. <= 1.4.3) would throw exceptions when handling unknown content-transfer-encoding's. Since
     * 1.4.4 Javamail silently ignores the unkown encoding.
     * 
     * This test has been modified to reflect the new behavior since 1.4.4
     */    
    @Test
    public void testAttachmentInvalidEncoding()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "32768");
        mailetConfig.setInitParameter("contentWildcard.1", "application/octet-stream");
        mailetConfig.setInitParameter("filenameWildcard.1", "*.blabla");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = loadMessage("message-with-attach-invalid-encoding.eml");
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        saveMessage(mail.getMessage(), "testAttachmentInvalidEncoding.eml");
        
        assertFalse(message == mail.getMessage());
    }
    
    @Test
    public void testImageAttachment()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "65535");
        mailetConfig.setInitParameter("contentWildcard.3", "*");
        mailetConfig.setInitParameter("filenameWildcard.3", "*.jpg, *.jpeg, *.wav, *.zip, *.png");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = loadMessage("image-attachement.eml");
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        saveMessage(mail.getMessage(), "testImageAttachment.eml");

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("text/plain"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("image/png"));
        assertEquals("gmail-pdf-sms.png", part.getFileName());        
    }
    
    @Test
    public void testAttachmentWithMatchMultipleFilenameMatchers()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "32768");
        mailetConfig.setInitParameter("contentWildcard.3", "*");
        mailetConfig.setInitParameter("filenameWildcard.3", "*.jpg, *.jpeg, *.wav, *.zip");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        saveMessage(mail.getMessage(), "testAttachmentWithMatch.eml");

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertEquals("normal message with attachment", mail.getMessage().getSubject());
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("multipart/alternative"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("application/octet-stream"));
        assertEquals("cityinfo.zip", part.getFileName());        
    }

    @Test
    public void testAttachmentWithMatch()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "32768");
        mailetConfig.setInitParameter("contentWildcard.1", "application/octet-stream");
        mailetConfig.setInitParameter("filenameWildcard.1", "*.zip");
        mailetConfig.setInitParameter("contentWildcard.2", "*");
        mailetConfig.setInitParameter("filenameWildcard.2", "*.wav");
        mailetConfig.setInitParameter("contentWildcard.3", "*");
        mailetConfig.setInitParameter("filenameWildcard.3", "*.jpg, *.jpeg, *.wav");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        saveMessage(mail.getMessage(), "testAttachmentWithMatch.eml");

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        assertEquals("normal message with attachment", mail.getMessage().getSubject());
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("multipart/alternative"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("application/octet-stream"));
        assertEquals("cityinfo.zip", part.getFileName());        
    }
    
    @Test
    public void testKeepSMIME()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        SendMailEventListenerImpl listener = new SendMailEventListenerImpl();
        
        mailetConfig.getMailetContext().setSendMailEventListener(listener);
        
        Mailet mailet = new StripUnsupportedFormats();

        mailetConfig.setInitParameter("keepSMIMEParts", "true");
        mailetConfig.setInitParameter("maxMessageSize", "32768");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = loadMessage("encrypted-aes.eml");
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        saveMessage(mail.getMessage(), "testKeepSMIME.eml");

        assertTrue(mail.getMessage().isMimeType("application/pkcs7-mime"));        
    }    
}
