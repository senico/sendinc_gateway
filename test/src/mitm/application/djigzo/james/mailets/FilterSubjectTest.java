/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class FilterSubjectTest
{
    private final static File testBase = new File("test/resources/testdata");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
    }

    @Test
    public void testStaticFilter()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailetConfig.setInitParameter("filter", "/e$/ %^&*");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test simple messag%^&*", message.getSubject());        
    }
    
    @Test
    public void testAttribute()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailetConfig.setInitParameter("attribute", "attribute");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setAttribute("attribute", "/(simple)/ *$1$1*");
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test *simplesimple* message", message.getSubject());        
    }    
    
    @Test
    public void testEncodedSubject()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailetConfig.setInitParameter("filter", "/(^(.*)$)/ $1 !@#\\$%^&*()");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encoded-subject.eml"));

        assertEquals("test encoded subject äöü ÄÖÜ", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test encoded subject äöü ÄÖÜ !@#$%^&*()", message.getSubject());
        
        /*
         * Check if subject is encoded
         */
        assertEquals("=?UTF-8?Q?test_encoded_subjec?=\r\n =?UTF-8?Q?t_=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=9C_!@#$%^&*()?=", 
                StringUtils.join(message.getHeader("subject"), ','));
    }
    
    @Test
    public void testMissingSubject()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailetConfig.setInitParameter("filter", "/(^(.*)$)/ test");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/test-5K.eml"));

        assertNull(message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertNull(message.getSubject());        
    }    
    
    @Test
    public void testNullFilter()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test simple message", message.getSubject());        
    }
    
    @Test
    public void testInvalidFilter()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("attribute", "attribute");
        
        Mailet mailet = new FilterSubject();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();

        mail.setAttribute("attribute", "invalid");
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test simple message", message.getSubject());        
    }
    
    @Test
    public void testEmptyReplace()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new FilterSubject();

        mailetConfig.setInitParameter("filter", "/simple/");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test  message", message.getSubject());        
    }
    
    @Test
    public void testInvalidRegExp()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("attribute", "attribute");
        
        Mailet mailet = new FilterSubject();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();

        mail.setAttribute("attribute", "/[x/ test");
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));

        assertEquals("test simple message", message.getSubject());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        mailet.service(mail);
        
        message = mail.getMessage();
        
        assertEquals("test simple message", message.getSubject());        
    }    
}
