/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.NamedCertificateCategories;
import mitm.application.djigzo.NamedCertificatesUserPreferencesManager;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.james.Certificates;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailSession;
import mitm.common.security.certificate.CertificateUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class RecipientAddAdditionalCertificatesTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static ExtendedAutoTransactDelegator proxy;
    
    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
        @StartTransaction
        public void setAdditionalCertificatesForUser(String email, Collection<X509Certificate> certificates)
        throws Exception
        {
            User user = getUser(email);
            
            if (user == null) {
                user = addUser(email);
            }
            
            NamedCertificatesUserPreferencesManager namedCertificatesManager = 
                    new NamedCertificatesUserPreferencesManager(NamedCertificateCategories.ADDITIONAL);
            
            namedCertificatesManager.setNamedCertificates(user.getUserPreferences(), certificates);
            
            SystemServices.getUserWorkflow().makePersistent(user);            
        }

        @StartTransaction
        public void setAdditionalCertificatesForDomain(String domain, Collection<X509Certificate> certificates)
        throws Exception
        {
            UserPreferences domainPrefs = SystemServices.getDomainManager().getDomainPreferences(domain);
            
            if (domainPrefs == null) {
                domainPrefs = SystemServices.getDomainManager().addDomain(domain);
            }
            
            NamedCertificatesUserPreferencesManager namedCertificatesManager = 
                    new NamedCertificatesUserPreferencesManager(NamedCertificateCategories.ADDITIONAL);
            
            namedCertificatesManager.setNamedCertificates(domainPrefs, certificates);
        }
        
        @StartTransaction
        public void setAdditionalCertificatesForGlobal(Collection<X509Certificate> certificates)
        throws Exception
        {
            GlobalPreferencesManager manager = SystemServices.getGlobalPreferencesManager();

            UserPreferences preferences = manager.getGlobalUserPreferences();
            
            NamedCertificatesUserPreferencesManager namedCertificatesManager = 
                    new NamedCertificatesUserPreferencesManager(NamedCertificateCategories.ADDITIONAL);
            
            namedCertificatesManager.setNamedCertificates(preferences, certificates);
        }

        @StartTransaction
        public void clearAdditionalCertificatesForGlobal()
        throws Exception
        {
            GlobalPreferencesManager manager = SystemServices.getGlobalPreferencesManager();

            UserPreferences preferences = manager.getGlobalUserPreferences();
            
            NamedCertificatesUserPreferencesManager namedCertificatesManager = 
                    new NamedCertificatesUserPreferencesManager(NamedCertificateCategories.ADDITIONAL);
            
            namedCertificatesManager.setNamedCertificates(preferences, null);
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();   

        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        proxy = AutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class);
    }
    
    @Before
    public void setup()
    throws Exception
    {
        proxy.deleteAllUsers();
        proxy.deleteAllDomains();
        proxy.clearAdditionalCertificatesForGlobal();
        proxy.cleanKeyAndCertStore();
    }
    
    @Test
    public void testAddCertificate()
    throws Exception
    {        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("validateCertificates", "false");

        RecipientAddAdditionalCertificates mailet = new RecipientAddAdditionalCertificates();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("sender@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("teST1@EXAMple.com"));
        recipients.add(new MailAddress("teST2@EXAMple.com"));
        recipients.add(new MailAddress("teST3@EXAMple.com"));
        recipients.add(new MailAddress("teST4@EXAMple.com"));
        
        mail.setRecipients(recipients);

        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = new Certificates(new HashSet<X509Certificate>(
                CertificateUtils.readX509Certificates(new File(testBase, "certificates/testcertificate.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        mailAttributes.setCertificates(certificates);
        
        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/multipleemail.cer"));
        
        assertEquals(1, additionalCerts.size());
        
        proxy.setAdditionalCertificatesForUser("test1@example.com", additionalCerts);

        
        additionalCerts = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/rim.cer"));
        
        assertEquals(1, additionalCerts.size());
        
        proxy.setAdditionalCertificatesForUser("test2@example.com", additionalCerts);
        proxy.setAdditionalCertificatesForUser("test3@example.com", additionalCerts);
        
        mailet.service(mail);
        
        mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        certificates = mailAttributes.getCertificates();
        
        assertEquals(3, certificates.getCertificates().size());        
    }
    
    @Test
    public void testAddCertificateDomain()
    throws Exception
    {        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        RecipientAddAdditionalCertificates mailet = new RecipientAddAdditionalCertificates();
        
        mailetConfig.setInitParameter("validateCertificates", "false");

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("sender@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("teST@EXAMple.com"));
        recipients.add(new MailAddress("teST@other.com"));
        
        mail.setRecipients(recipients);

        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        Certificates certificates = new Certificates(new HashSet<X509Certificate>(
                CertificateUtils.readX509Certificates(new File(testBase, "certificates/testcertificate.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        mailAttributes.setCertificates(certificates);
        
        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/multipleemail.cer"));
        
        assertEquals(1, additionalCerts.size());
        
        proxy.setAdditionalCertificatesForDomain("*.other.com", additionalCerts);
        
        additionalCerts = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/rim.cer"));
        
        assertEquals(1, additionalCerts.size());
        
        proxy.setAdditionalCertificatesForUser("test@example.com", additionalCerts);
        
        mailet.service(mail);
        
        mailAttributes = new DjigzoMailAttributesImpl(mail);
        
        certificates = mailAttributes.getCertificates();
        
        assertEquals(3, certificates.getCertificates().size());        
    }    
}
