/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.mail.repository.MailStorer;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailUtils;
import mitm.common.mail.repository.MailRepository;
import mitm.common.mail.repository.MailRepositoryItem;
import mitm.common.mail.repository.hibernate.MailRepositoryDAO;
import mitm.common.reflection.ProxyFactoryException;
import mitm.test.TestUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class QuarantineTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private static AutoTransactDelegator proxy;

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();   

        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        proxy = AutoTransactDelegator.createProxy();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        proxy.deleteAll();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
        }

        private static SessionManager getSessionManager() {
            return SystemServices.getSessionManager();
        }
        
        private static MailRepository getQuarantine() {
            return SystemServices.getService("quarantineMailRepository", MailRepository.class);
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, getSessionManager()).createProxy();
            
            return autoTransactDelegator;
        }
        
        private MailRepositoryDAO getDAO()
        {
            SessionAdapter session = SessionAdapterFactory.create(getSessionManager().getSession());
            
            return new MailRepositoryDAO(session);
        }    

        @StartTransaction
        public void deleteAll() {
            getDAO().deleteAll();
        }

        @StartTransaction
        public List<? extends MailRepositoryItem> getItems()
        {
            return getQuarantine().getItems(0, Integer.MAX_VALUE);
        }
        
        @StartTransaction
        public void assertMessage(String id, MimeMessage expected)
        throws MessagingException, IOException
        {
            MailRepositoryItem item = getQuarantine().getItem(id);

            assertTrue(TestUtils.isEqual(expected, item.getMimeMessage()));
        }        
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    @Test
    public void testQuarantine()
    throws Exception
    {   
        MimeMessage message = loadMessage("html-alternative.eml");
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        Quarantine mailet = new Quarantine();
        
        mailetConfig.setInitParameter("errorProcessor", "error");
        
        mailet.init(mailetConfig);
        
        Mail mail = new MailImpl("test", new MailAddress("sender@example.com"), 
                Arrays.asList(new MailAddress("test1@example.com"), new MailAddress("test2@example.com")), message);

        assertEquals("root", mail.getState());
        
        assertEquals(0, proxy.getItems().size());

        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        assertNull(attributes.getMailRepositoryID());        

        assertTrue(TestUtils.isEqual(message, mail.getMessage()));

        MimeMessage org = mail.getMessage();
        
        mailet.service(mail);

        assertTrue(org == mail.getMessage());
        
        List<? extends MailRepositoryItem> items = proxy.getItems();
        
        assertEquals(1, items.size());
        
        proxy.assertMessage(items.get(0).getID(), message);
        
        attributes = new DjigzoMailAttributesImpl(mail);

        assertNotNull(attributes.getMailRepositoryID());        

        assertEquals("root", mail.getState());
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        assertEquals("test1@example.com,test2@example.com", StringUtils.join(mail.getRecipients(), ","));
    }
    
    @Test
    public void testError()
    throws Exception
    {   
        MimeMessage message = loadMessage("html-alternative.eml");
        
        MockMailetConfig mailetConfig = new MockMailetConfig();

        Quarantine mailet = new Quarantine();
        
        mailetConfig.setInitParameter("errorProcessor", "error");
        
        mailet.init(mailetConfig);

        // override the MailStorer with one that always fails
        mailet.setQuarantineMailStorer(new MailStorer() {
            @Override
            public void store(Mail mail)
            throws MessagingException, IOException
            {
                throw new OutOfMemoryError("Expected error");
            }
        });
        
        Mail mail = new MailImpl("test", new MailAddress("sender@example.com"), 
                Arrays.asList(new MailAddress("test1@example.com"), new MailAddress("test2@example.com")), message);

        assertEquals("root", mail.getState());
        
        assertEquals(0, proxy.getItems().size());

        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        MimeMessage org = mail.getMessage();
        
        mailet.service(mail);

        assertTrue(org == mail.getMessage());
        
        List<? extends MailRepositoryItem> items = proxy.getItems();
        
        assertEquals(0, items.size());
        
        attributes = new DjigzoMailAttributesImpl(mail);

        assertNull(attributes.getMailRepositoryID());        

        assertEquals("error", mail.getState());
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        assertEquals("test1@example.com,test2@example.com", StringUtils.join(mail.getRecipients(), ","));
    }        
}
