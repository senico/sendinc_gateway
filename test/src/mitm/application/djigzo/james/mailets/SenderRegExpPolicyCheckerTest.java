/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.dlp.PolicyPatternManager;
import mitm.application.djigzo.dlp.PolicyPatternNode;
import mitm.application.djigzo.dlp.UserPreferencesPolicyPatternManager;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.dlp.impl.PolicyPatternImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailUtils;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests for SenderRegExpPolicyChecker
 * 
 * @author Martijn Brinkers
 *
 */
public class SenderRegExpPolicyCheckerTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static ExtendedAutoTransactProxy autoTransactProxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();        

        Logger.getLogger(SenderRegExpPolicyChecker.class).setLevel(Level.DEBUG);
        
        autoTransactProxy = AutoTransactDelegator.createProxy(ExtendedAutoTransactProxy.class);
        
        autoTransactProxy.init();
    }
    
    public static class ExtendedAutoTransactProxy extends AutoTransactDelegator
    {
        private PolicyPatternManager policyPatternManager;
        private UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;
        
        public ExtendedAutoTransactProxy()
        {
            policyPatternManager = SystemServices.getPolicyPatternManager();
            userPreferencesPolicyPatternManager = SystemServices.getUserPreferencesPolicyPatternManager();
        }
        
        public void init()
        {
            addPattern("WARN", "test", PolicyViolationPriority.WARN);
            addPattern("MUST_ENCRYPT", "test", PolicyViolationPriority.MUST_ENCRYPT);
            addPattern("QUARANTINE", "test", PolicyViolationPriority.QUARANTINE);
            addPattern("BLOCK", "test", PolicyViolationPriority.BLOCK);
            addPattern("TES", "tes", PolicyViolationPriority.WARN);
            addPattern("WARN_MATCH_ALL", ".*", PolicyViolationPriority.WARN);
            
            addGroup("GROUP");
            addChild("GROUP", "CHILD", ".*", PolicyViolationPriority.WARN);
            
            addGroup("RECURSIVE1");
            addGroup("RECURSIVE2");
            addChild("RECURSIVE1", "RECURSIVE2");
            addChild("RECURSIVE2", "RECURSIVE1");
            addChild("RECURSIVE2", "CHILD2", ".*", PolicyViolationPriority.WARN);
        }
        
        @StartTransaction
        public void addPattern(String name, String regExp, PolicyViolationPriority priority)
        {
            PolicyPatternNode node = policyPatternManager.createPattern(name);
            
            PolicyPatternImpl pattern = new PolicyPatternImpl(name, Pattern.compile(regExp));
            
            pattern.setPriority(priority);
            
            node.setPolicyPattern(pattern);
        }

        @StartTransaction
        public void addGroup(String name)
        {
            policyPatternManager.createPattern(name);
        }
        
        @StartTransaction
        public void addChild(String parentName, String childName, String regExp, PolicyViolationPriority priority)
        {
            PolicyPatternNode parent = policyPatternManager.getPattern(parentName);
            
            PolicyPatternImpl pattern = new PolicyPatternImpl(childName, Pattern.compile(regExp));
            
            pattern.setPriority(priority);

            PolicyPatternNode childNode = policyPatternManager.createPattern(childName);
            
            childNode.setPolicyPattern(pattern);
            
            parent.getChilds().add(childNode);
        }

        @StartTransaction
        public void addChild(String parentName, String childName)
        {
            PolicyPatternNode child = policyPatternManager.getPattern(childName);
            PolicyPatternNode parent = policyPatternManager.getPattern(parentName);
            
            parent.getChilds().add(child);
        }
        
        @StartTransaction
        public void addUserPatterns(String email, String... patternNames)
        throws AddressException, HierarchicalPropertiesException
        {
            User user = getUser(email);
        
            if (user == null) {
                user = addUser(email);
            }
            
            Collection<PolicyPatternNode> nodes = userPreferencesPolicyPatternManager.getPatterns(
                    user.getUserPreferences());
            
            for (String patternName : patternNames) {
                nodes.add(policyPatternManager.getPattern(patternName));
            }
            
            userPreferencesPolicyPatternManager.setPatterns(user.getUserPreferences(), nodes);
        }
    }
    
    @Before
    public void setup()
    throws Exception
    {
        autoTransactProxy.deleteAllUsers();
    }
    
    private SenderRegExpPolicyChecker createDefaultSenderRegExpPolicyChecker()
    throws MessagingException
    {
        SenderRegExpPolicyChecker mailet = new SenderRegExpPolicyChecker();
        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");
        
        mailetConfig.setInitParameter("warnProcessor", "warnProcessor");
        mailetConfig.setInitParameter("mustEncryptProcessor", "mustEncryptProcessor");
        mailetConfig.setInitParameter("quarantineProcessor", "quarantineProcessor");
        mailetConfig.setInitParameter("blockProcessor", "blockProcessor");
        mailetConfig.setInitParameter("errorProcessor", "errorProcessor");
        
        mailet.init(mailetConfig);
        
        return mailet;
    }
    
    public MockMail testViolation(String policyName)
    throws Exception
    {
        SenderRegExpPolicyChecker mailet = createDefaultSenderRegExpPolicyChecker();
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        autoTransactProxy.addUserPatterns("test@example.com", policyName);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/normal-message-with-attach.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test2@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));

        mailet.service(mail);

        return mail;
    }
    
    @Test
    public void testViolateWARN()
    throws Exception
    {
        MockMail mail = testViolation("WARN");

        assertEquals("warnProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, " +
        		"test, test, test, test, test, test", violation.toString());
    }

    @Test
    public void testViolateWARNMatchAll()
    throws Exception
    {
        MockMail mail = testViolation("WARN_MATCH_ALL");

        assertEquals("warnProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        /*
         * Somehow the end of line seems to be matched separately
         */
        assertEquals("Policy: RegExp, Rule: WARN_MATCH_ALL, Priority: WARN, Match: to: <test@example.com> " +
        		"subject: normal message attachment dat..., , test , , test , ", violation.toString());
    }
    
    @Test
    public void testViolateMUST_ENCRYPT()
    throws Exception
    {
        MockMail mail = testViolation("MUST_ENCRYPT");

        assertEquals("mustEncryptProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("Policy: RegExp, Rule: MUST_ENCRYPT, Priority: MUST_ENCRYPT, Match: test, test, test, test, " +
        		"test, test, test, test, test, test, test", violation.toString());
    }
    
    @Test
    public void testViolateQUARANTINE()
    throws Exception
    {
        MockMail mail = testViolation("QUARANTINE");

        assertEquals("quarantineProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("Policy: RegExp, Rule: QUARANTINE, Priority: QUARANTINE, Match: test, test, test, test, test, " +
        		"test, test, test, test, test, test", violation.toString());
    }
    
    @Test
    public void testViolateBLOCK()
    throws Exception
    {
        autoTransactProxy.addUserPatterns("test@example.com", "WARN");
        
        MockMail mail = testViolation("BLOCK");

        assertEquals("blockProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(2, violations.size());
        
        Iterator<PolicyViolation> it = violations.iterator();
        
        PolicyViolation violation = it.next();
        
        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, test, " +
        		"test, test, test, test, test", violation.toString());

        violation = it.next();
        
        assertEquals("Policy: RegExp, Rule: BLOCK, Priority: BLOCK, Match: test, test, test, test, test, test, " +
        		"test, test, test, test, test", violation.toString());
    }
    
    @Test
    public void testMaxStringLength()
    throws Exception
    {
        SenderRegExpPolicyChecker mailet = new SenderRegExpPolicyChecker();
        
        MockMailetConfig mailetConfig = new MockMailetConfig("test");
        
        mailetConfig.setInitParameter("warnProcessor", "warnProcessor");
        mailetConfig.setInitParameter("mustEncryptProcessor", "mustEncryptProcessor");
        mailetConfig.setInitParameter("quarantineProcessor", "quarantineProcessor");
        mailetConfig.setInitParameter("blockProcessor", "blockProcessor");
        mailetConfig.setInitParameter("errorProcessor", "errorProcessor");
        mailetConfig.setInitParameter("maxStringLength", "3");
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        autoTransactProxy.addUserPatterns("test@example.com", "TES");
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/normal-message-with-attach.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test2@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("test@example.com"));

        mailet.service(mail);

        assertEquals("warnProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
                
        /*
         * tes is matched multiple times because of partial and overlap
         */
        assertEquals("Policy: RegExp, Rule: TES, Priority: WARN, Match: tes, tes, tes, tes, tes, tes, tes, tes, " +
        		"tes, tes, tes, tes, tes, tes", violation.toString());
    }
    
    @Test
    public void testChildPattern()
    throws Exception
    {
        MockMail mail = testViolation("GROUP");

        assertEquals("warnProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("Policy: RegExp, Rule: CHILD, Priority: WARN, Match: to: <test@example.com> subject: " +
        		"normal message attachment dat..., , test , , test , ", violation.toString());
    }
    
    @Test
    public void testRecursive()
    throws Exception
    {
        MockMail mail = testViolation("RECURSIVE1");

        assertEquals("warnProcessor", mail.getState());
        
        DjigzoMailAttributes mailAttributes = new DjigzoMailAttributesImpl(mail);

        assertNull(mailAttributes.getPolicyViolationErrorMessage());
        
        Collection<PolicyViolation> violations = mailAttributes.getPolicyViolations();
        
        assertEquals(1, violations.size());
        
        PolicyViolation violation = violations.iterator().next();
        
        assertEquals("Policy: RegExp, Rule: CHILD2, Priority: WARN, Match: to: <test@example.com> subject: " +
        		"normal message attachment dat..., , test , , test , ", violation.toString());
    }    
}
