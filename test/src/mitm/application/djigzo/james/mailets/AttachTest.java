/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.Mailet;
import org.junit.BeforeClass;
import org.junit.Test;


public class AttachTest
{
    private final static File testBase = new File("test/resources/testdata");
    private static final File tempDir = new File("test/tmp");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception
    {
        DjigzoTestUtils.initialize();
    }

    @Test
    public void testSinglePart()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new Attach();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        assertFalse(mail.getMessage().isMimeType("multipart/mixed"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        MailUtils.writeMessage(mail.getMessage(), new File(tempDir, "testSinglePart.eml"));
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(1, mp.getCount());
    }
    
    @Test
    public void testMultipart()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        Mailet mailet = new Attach();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-validcertificate.eml"));
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        assertFalse(mail.getMessage().isMimeType("multipart/mixed"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        MailUtils.writeMessage(mail.getMessage(), new File(tempDir, "Multipart.eml"));
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(1, mp.getCount());
    }    
    
    @Test
    public void testSinglePartWithFilename()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("filename", "test.txt");
        
        Mailet mailet = new Attach();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/test-5K.eml"));
     
        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", message.getMessageID());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        assertTrue(mail.getMessage().isMimeType("text/plain"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        MailUtils.writeMessage(mail.getMessage(), new File(tempDir, "testSinglePartWithFilename.eml"));
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(1, mp.getCount());
        
        assertEquals("test.txt", mail.getMessage().getFileName());
        
        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", mail.getMessage().getMessageID());        
    }    
    
    @Test
    public void testDoNotRetainMessageID()
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("retainMessageID", "false");
        
        Mailet mailet = new Attach();

        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
                
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/test-5K.eml"));
     
        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", message.getMessageID());
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));
        
        assertTrue(mail.getMessage().isMimeType("text/plain"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        MailUtils.writeMessage(mail.getMessage(), new File(tempDir, "testDoNotRetainMessageID.eml"));
        
        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) mail.getMessage().getContent();
        
        assertEquals(1, mp.getCount());
        
        assertFalse("<6d5a.0003.fffffffd@martijn-desktop>".equals(mail.getMessage().getMessageID()));        
    }        
}
