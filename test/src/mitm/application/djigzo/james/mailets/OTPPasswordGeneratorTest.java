/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.james.ClientSecrets;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.EncryptedContainer;
import mitm.application.djigzo.james.PasswordContainer;
import mitm.application.djigzo.james.Passwords;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class OTPPasswordGeneratorTest
{
    private static AutoTransactDelegator proxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
        
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        proxy = AutoTransactDelegator.createProxy();        
    }
    
    @Before
    public void setup()
    throws Exception
    {
        proxy.deleteAllUsers();
        
        proxy.addUser("test1@example.com");

        proxy.setProperty("test1@example.com", "user.clientSecret", "secret1", true);
    }

    @Test
    public void testUserProperties() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        OTPPasswordGenerator mailet = new OTPPasswordGenerator();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("teST1@EXAMple.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("teST1@EXAMple.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
                
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(16, (int) passwordContainer.getPasswordLength());
        
        System.out.println("Password: " + passwordContainer.getPassword() + 
                ", PasswordID: " + passwordContainer.getPasswordID());
        
        mailet.service(mail);
        
        Passwords newPasswords = attributes.getPasswords();

        PasswordContainer newPasswordContainer = newPasswords.get("test1@example.com");

        assertFalse(newPasswordContainer.getPassword().equals(passwordContainer.getPassword()));

        System.out.println("Password: " + newPasswordContainer.getPassword() + 
                ", PasswordID: " + newPasswordContainer.getPasswordID());
    }    
    
    @Test
    public void testUserPropertiesNonDefaultPasswordLength() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        OTPPasswordGenerator mailet = new OTPPasswordGenerator();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("teST1@EXAMple.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        proxy.setProperty("test1@example.com", "user.passwordLength", "100", false);
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("teST1@EXAMple.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
                
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(100, (int) passwordContainer.getPasswordLength());
        
        System.out.println("Password: " + passwordContainer.getPassword() + 
                ", PasswordID: " + passwordContainer.getPasswordID());
        
        mailet.service(mail);
        
        Passwords newPasswords = attributes.getPasswords();

        PasswordContainer newPasswordContainer = newPasswords.get("test1@example.com");

        assertFalse(newPasswordContainer.getPassword().equals(passwordContainer.getPassword()));

        System.out.println("Password: " + newPasswordContainer.getPassword() + 
                ", PasswordID: " + newPasswordContainer.getPasswordID());
    }    
    
    @Test
    public void testDefaultSecret() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        OTPPasswordGenerator mailet = new OTPPasswordGenerator();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("nosecret@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNull(passwords);
        
        /*
         * Now set a default password
         */
        mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("defaultSecret", "secret");
        
        mailet = new OTPPasswordGenerator();
        
        mailet.init(mailetConfig);

        mail.setMessage(message);

        mail.setRecipients(recipients);
        
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);

        passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        assertEquals(1, passwords.size());
        assertNotNull(passwords.get("nosecret@example.com"));
    }        
    
    @Test
    public void testSecretMailAttributes()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        OTPPasswordGenerator mailet = new OTPPasswordGenerator();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("recipient@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);

        ClientSecrets clientSecrets = new ClientSecrets();
        
        clientSecrets.put("nonmatch@example.com", new EncryptedContainer<String>("secret"));
        
        attributes.setClientSecrets(clientSecrets);
        
        mailet.service(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNull(passwords);

        clientSecrets.put("recipient@example.com", new EncryptedContainer<String>("secret"));

        attributes.setClientSecrets(clientSecrets);
        
        mailet.service(mail);
        
        passwords = attributes.getPasswords();

        assertNotNull(passwords);
        
        assertEquals(1, passwords.size());
        assertNotNull(passwords.get("recipient@example.com"));
    }
}
