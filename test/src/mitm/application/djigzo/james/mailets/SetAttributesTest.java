/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.BeforeClass;
import org.junit.Test;


public class SetAttributesTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
    }
    
    private Mail createMail()
    throws Exception 
    {
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        return mail;
    }
    
    @Test
    public void testSetAttributes() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("test", "123");
        
        SetAttributes mailet = new SetAttributes();
        
        mailet.init(mailetConfig);

        Mail mail = createMail();

        assertNull(mail.getAttribute("test"));

        mailet.service(mail);

        assertEquals("123", mail.getAttribute("test"));
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("test@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
    }    
    
    @Test
    public void testWithProcessor()
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("processor", "nextState");
        
        SetAttributes mailet = new SetAttributes();
        
        mailet.init(mailetConfig);

        Mail mail = createMail();

        assertNull(mail.getAttribute("processor"));

        mailet.service(mail);

        assertNull(mail.getAttribute("processor"));
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals("nextState", mail.getState());
    }    
}
