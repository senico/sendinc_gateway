/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.james.DjigzoMailAttributes;
import mitm.application.djigzo.james.DjigzoMailAttributesImpl;
import mitm.application.djigzo.james.PasswordContainer;
import mitm.application.djigzo.james.Passwords;
import mitm.application.djigzo.james.mock.MockMail;
import mitm.application.djigzo.james.mock.MockMailetConfig;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.MailSession;
import mitm.common.mail.MailUtils;
import mitm.common.properties.HierarchicalPropertiesException;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneratePasswordTest
{
    private final static Logger logger = LoggerFactory.getLogger(GeneratePasswordTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        addUser();
    }

    private static void addUser()
    throws Exception 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        addUserAction(session, "test1@example.com");
                        addUserAction(session, "test2@example.com");
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
     
    public static void addUserAction(Session session, String email)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
     
            userWorkflow.makePersistent(user);            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }

    private static boolean userExists(final String email)
    throws Exception 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        boolean exists = false;
        
        Session previousSession = sessionManager.getSession();
        
        try {
            DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
            		sessionManager);
            
            exists = actionExecutor.executeTransaction(
                new DatabaseAction<Boolean>()
                {
                    @Override
                    public Boolean doAction(Session session)
                    throws DatabaseException
                    {
                    	sessionManager.setSession(session);
                        
                        return userExistsAction(session, email);
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
        
        return exists;
    }
     
    public static boolean userExistsAction(Session session, String email)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
     
            return user != null;            
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }
    
    @Test
    public void testGeneratePasswordSingleRecipientCaseInsensitive() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("teST1@EXAMple.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("teST1@EXAMple.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
    }
    
    @Test
    public void testGeneratePasswordSingleRecipient() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("test1@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
    }
    
    @Test
    public void testGeneratePasswordMultipleRecipient() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(2, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("test1@example.com")));
        assertTrue(mail.getRecipients().contains(new MailAddress("test2@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        
        passwordContainer = passwords.get("test2@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());        
    }

    @Test
    public void testGeneratePasswordMultipleRecipientCaseInsensitive() 
    throws Exception 
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(2, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("TEST1@EXAMPLE.COM")));
        assertTrue(mail.getRecipients().contains(new MailAddress("test2@exAMPLE.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("test1@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        
        passwordContainer = passwords.get("test2@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());        
    }
    
    @Test
    public void testGeneratePasswordNewUser() 
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("randomIDBound", "10000");
        
        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
        
        MockMail mail = new MockMail();
        
        mail.setState(Mail.DEFAULT);
        
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());
        
        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));
        
        message.saveChanges();
        
        mail.setMessage(message);
        
        Collection<MailAddress> recipients = new LinkedList<MailAddress>();
        
        recipients.add(new MailAddress("new@example.com"));
        
        mail.setRecipients(recipients);
     
        mail.setSender(new MailAddress("sender@example.com"));

        // User should not yet exist
        assertFalse(userExists("new@example.com"));
        
        mailet.service(mail);
        
        MailUtils.validateMessage(mail.getMessage());
        
        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("new@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        
        DjigzoMailAttributes attributes = new DjigzoMailAttributesImpl(mail);
        
        Passwords passwords = attributes.getPasswords();
        
        assertNotNull(passwords);
        
        PasswordContainer passwordContainer = passwords.get("new@example.com");
        
        logger.info("Password: " + passwordContainer.getPassword() + "; passwordID: " + passwordContainer.getPasswordID());
        
        assertNotNull(passwordContainer);        
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        
        // check that the user now exists
        assertTrue(userExists("new@example.com"));
    }
    
    /*
     * Checks if setting catchRuntimeExceptions and catchErrors is allowed
     */
    @Test
    public void testSetBaseParameters() 
    throws Exception
    {
        MockMailetConfig mailetConfig = new MockMailetConfig("test");

        mailetConfig.setInitParameter("catchRuntimeExceptions", "true");
        mailetConfig.setInitParameter("catchErrors", "true");

        GeneratePassword mailet = new GeneratePassword();
        
        mailet.init(mailetConfig);
    }
}
