/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mailets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import mitm.common.util.StandardHttpURLBuilder;
import mitm.common.util.URLBuilderException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class PDFReplyURLBuilderTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }

    @Test
    public void testUnicode() 
    throws URLBuilderException
    {
        final String key = "abc";
        final String subject = "Größe";
        
        PDFReplyURLBuilder builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());
        
        builder.setBaseURL("http://www.example.com");
        builder.setUser("user@example.com");
        builder.setFrom("test@example.com");
        builder.setRecipient("test@recipient.example.com");
        builder.setSubject(subject);
        builder.setKey(key);
        builder.setTime(8765L);
        builder.setMessageID("123");

        String url = builder.buildURL();
        
        assertNotNull(url);
        
        PDFReplyURLBuilder.KeyProvider keyProvider = new PDFReplyURLBuilder.KeyProvider()
        {
            @Override
            public String getKey(PDFReplyURLBuilder builder)
            throws URLBuilderException
            {
                return key;
            }
        };
        
        String envelope = StringUtils.substringBetween(url, "env=", "&hmac=");
        String hmac = StringUtils.substringAfter(url, "hmac=");

        System.out.println(url);
        System.out.println(envelope);
        System.out.println(hmac);
        
        builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());
        builder.loadFromEnvelope(envelope, hmac, keyProvider);
        
        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals(subject, builder.getSubject());
    }
    
    @Test
    public void testPDFReplyURLBuilder() 
    throws URLBuilderException
    {
    	PDFReplyURLBuilder builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());
    	
    	builder.setBaseURL("http://www.example.com");
    	builder.setUser("user@example.com");
    	builder.setFrom("test@example.com");
    	builder.setRecipient("test@recipient.example.com");
    	builder.setSubject("test");
    	builder.setKey("1234");

    	String url = builder.buildURL();
    	
    	assertNotNull(url);
    }

    @Test
    public void testPDFReplyURLBuilderVerify() 
    throws URLBuilderException
    {
    	PDFReplyURLBuilder builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());

    	PDFReplyURLBuilder.KeyProvider keyProvider = new PDFReplyURLBuilder.KeyProvider()
    	{
            @Override
			public String getKey(PDFReplyURLBuilder builder)
			throws URLBuilderException
			{
				return "1234";
			}
    	};

    	builder.loadFromEnvelope("eyJpZCI6InNvbWUgaWQiLCJmIjoidGVzdEBleGFtcGxlLmNvbSIsInUiOiJ1c2VyQGV4YW1wbGUuY29tIiwidCI6ODc2NSwicyI6InRlc3QiLCJyIjoidGVzdEByZWNpcGllbnQuZXhhbXBsZS5jb20ifQ==", 
    			"azfl35nzidcqdjr6wgm3fedlfcq5tom4", keyProvider);

    	assertEquals("user@example.com", builder.getUser());
    	assertEquals("test@example.com", builder.getFrom());
    	assertEquals("test@recipient.example.com", builder.getRecipient());
    	/* we need to cast to long to prevent ambigous error */
    	assertEquals(8765L, (long) builder.getTime());
    	assertEquals("some id", builder.getMessageID());
    	assertEquals("test", builder.getSubject());
    }

    @Test(expected = URLBuilderException.class)
    public void testPDFReplyURLBuilderIncorrectMac() 
    throws URLBuilderException
    {
    	PDFReplyURLBuilder builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());

    	PDFReplyURLBuilder.KeyProvider keyProvider = new PDFReplyURLBuilder.KeyProvider()
    	{
            @Override
			public String getKey(PDFReplyURLBuilder builder)
			throws URLBuilderException
			{
				return "1234";
			}
    	};

    	builder.loadFromEnvelope("eyJpZCI6InNvbWUgaWQiLCJmIjoidGVzdEBleGFtcGxlLmNvbSIsInUiOiJ1c2VyQGV4YW1wbGUuY29tIiwidCI6ODc2NSwicyI6InRlc3QiLCJyIjoidGVzdEByZWNpcGllbnQuZXhhbXBsZS5jb20ifQ==", 
    			"abc", keyProvider);
    }

    @Test(expected = URLBuilderException.class)
    public void testPDFReplyURLBuilderIncorrectBase64() 
    throws URLBuilderException
    {
    	PDFReplyURLBuilder builder = new PDFReplyURLBuilder(new StandardHttpURLBuilder());

    	PDFReplyURLBuilder.KeyProvider keyProvider = new PDFReplyURLBuilder.KeyProvider()
    	{
            @Override
			public String getKey(PDFReplyURLBuilder builder)
			throws URLBuilderException
			{
				return "";
			}
    	};

    	builder.loadFromEnvelope("x", "xx", keyProvider);
    }
}
