/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mock;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MimeMessageUtil;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

/**
 * A mockup implementation of {@link Mail} used for testing matchers and mailets.
 * 
 * @author Martijn Brinkers
 *
 */
public class MockMail implements Mail, Cloneable
{
    private static final long serialVersionUID = 7125675246441268829L;

    private String errorMessage;
    private Date lastUpdated;
    private MimeMessage mimeMessage;
    private String name;
    private String remoteAddr;
    
    @SuppressWarnings("rawtypes")
    private Collection recipients = new LinkedList();
    private MailAddress sender;
    private String state;
    private Map<String, byte[]> attributes = new HashMap<String, byte[]>();
    
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
    
    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    @Override
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    @Override
    public MimeMessage getMessage()
    throws MessagingException 
    {
        return mimeMessage;
    }

    @Override
    public void setMessage(MimeMessage mimeMessage) {
        this.mimeMessage = mimeMessage;
    }
    
    @Override
    public long getMessageSize()
    throws MessagingException 
    {
        return mimeMessage != null ? MimeMessageUtil.getMessageSize(mimeMessage) : -1;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public Collection<?> getRecipients() {
        return recipients;
    }

    @Override
    public void setRecipients(@SuppressWarnings("rawtypes") Collection recipients) {
        this.recipients = recipients;
    }
    
    @Override
    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }
    
    @Override
    public String getRemoteHost() {
        return remoteAddr;
    }

    public void setRemoteHost(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }
    
    @Override
    public MailAddress getSender() {
        return sender;
    }

    public void setSender(MailAddress sender) {
        this.sender = sender;
    }
    
    @Override
    public String getState() {
        return state;
    }

    @Override
    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean hasAttributes() 
    {
        return attributes.size() > 0;
    }

    @Override
    public Serializable removeAttribute(String name) {
        return attributes.remove(name);
    }

    @Override
    public Serializable getAttribute(String name) 
    {
        byte[] serialized = attributes.get(name);
        
        Serializable value = null;
        
        if (serialized != null) {
            value = (Serializable) SerializationUtils.deserialize(serialized);
        }
        
        return value;
    }
    
    @Override
    public Serializable setAttribute(String name, Serializable serializable) 
    {
        byte[] serialized = null;
        
        if (serializable != null) 
        {
            /*
             * Serialize immediately so we know that the object is serializable.
             */
            serialized = SerializationUtils.serialize(serializable);
        }
                
        attributes.put(name, serialized);
        
        return serializable;
    }

    @Override
    public Iterator<?> getAttributeNames() {
        return attributes.keySet().iterator();
    }
    
    @Override
    public void removeAllAttributes() {
        attributes.clear();
    }    
    
    public static MockMail clone(Mail mail)
    throws MessagingException
    {
        MockMail clone = new MockMail();
        
        clone.errorMessage = mail.getErrorMessage();
        clone.lastUpdated = mail.getLastUpdated();
        clone.mimeMessage = mail.getMessage();
        clone.name = mail.getName();
        clone.remoteAddr = mail.getRemoteAddr();
        clone.recipients = mail.getRecipients();
        clone.sender = mail.getSender();
        clone.state = mail.getState();
        
        @SuppressWarnings("unchecked")
        Iterator<String> it = mail.getAttributeNames();
            
        while (it.hasNext())
        {
            String name = it.next();
            
            clone.setAttribute(name, mail.getAttribute(name));
        }
        
        return clone;
    }
    
    /*
     * Create a shallow clone
     */
    @Override
    public MockMail clone()
    {
        try {
            return clone(this);
        } 
        catch (MessagingException e) {
            throw new IllegalArgumentException(e);
        }
    }
    
    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder();
        
        sb.append("Recipients: ");
        sb.appendWithSeparators(recipients, ", ");
        sb.appendNewLine();
        sb.append("Remote address: ");
        sb.append(remoteAddr);
        sb.appendNewLine();
        sb.append("Sender: ");
        sb.append(sender);
        sb.appendNewLine();
        
        return sb.toString();
    }
}
