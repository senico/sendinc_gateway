/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Configuration;
import org.apache.mailet.MailetConfig;

/**
 * Mock implementation of MailetConfig for unit testing.
 * 
 * @author Martijn Brinkers
 *
 */
public class MockMailetConfig implements MailetConfig
{
    private final String name;
    private final MockMailetContext mailetContext = new MockMailetContext();
    
    private final Map<String, List<String>> parameters = new HashMap<String, List<String>>();
    
    public MockMailetConfig(String name) {
        this.name = name;
    }

    public MockMailetConfig() {
        this("");
    }
    
    @Override
    public String getInitParameter(String parameterName)
    {
        List<String> values = parameters.get(parameterName);
        
        if (values == null) {
            return null;
        }
        
        return StringUtils.join(values, ',');
    }

    public void setInitParameter(String parameterName, String value) 
    {
        List<String> values = parameters.get(parameterName);
        
        if (values == null)
        {
            values = new LinkedList<String>();
            
            parameters.put(parameterName, values);
        }

        values.add(value);
    }
    
    @Override
    public Iterator<?> getInitParameterNames() {
        return parameters.keySet().iterator();
    }

    @Override
    public MockMailetContext getMailetContext() {
        return mailetContext;
    }

    @Override
    public String getMailetName() {
        return name;
    }

    @Override
    public Configuration getConfiguration() {
        return new MockConfiguration(null, parameters);
    }
}
