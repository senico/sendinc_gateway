/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.james.mock;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.MailetContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockMailetContext implements MailetContext
{
    private final static Logger logger = LoggerFactory.getLogger(MockMailetContext.class);
    
    private Map<String, Object> attributes = new HashMap<String, Object>();
    private MailAddress postmaster;
    
    private SendMailEventListener sendMailEventListener; 
    
    public void setSendMailEventListener(SendMailEventListener sendMailEventListener) {
        this.sendMailEventListener = sendMailEventListener;
    }
    
    @Override
    public void bounce(Mail mail, String message)
    throws MessagingException 
    {
        logger.info("Bouncing mail: " + mail);
    }

    @Override
    public void bounce(Mail mail, String message, MailAddress sender)
    throws MessagingException 
    {
        logger.info("Bouncing mail: " + mail + " , Sender: " + sender);
    }

    @Override
    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    @Override
    public void setAttribute(String name, Object value) {
        attributes.put(name, value);
    }
    
    @Override
    public Iterator<?> getAttributeNames() {
        return attributes.keySet().iterator();
    }

    @Override
    public void removeAttribute(String name) {
        attributes.remove(name);
    }
    
    @Override
    public Collection<?> getMailServers(String host) {
        return null;
    }

    @Override
    public int getMajorVersion() {
        return 1;
    }

    @Override
    public int getMinorVersion() {
        return 2;
    }

    @Override
    public MailAddress getPostmaster() {
        return postmaster;
    }

    public void setPostmaster(MailAddress postmaster) {
        this.postmaster = postmaster;
    }
    
    @Override
    public Iterator<?> getSMTPHostAddresses(String domainName) {
        return null;
    }

    @Override
    public String getServerInfo() {
        return "Mimsecure mock server";
    }

    @Override
    public boolean isLocalServer(String s) {
        return false;
    }

    @Override
    public boolean isLocalUser(String s) {
        return false;
    }

    @Override
    public void log(String message) {
        logger.info(message);
    }

    @Override
    public void log(String message, Throwable throwable) {
        logger.error(message, throwable);
    }

    @Override
    public void sendMail(MimeMessage mimemessage)
    throws MessagingException
    {
        logger.info("sendMail");
    }

    @Override
    public void sendMail(Mail mail)
    throws MessagingException 
    {
        logger.info("sendMail. Sender: " + mail.getSender() + ", State: " + mail.getState() + ", Recipients: " 
            + StringUtils.join(mail.getRecipients(), ", "));        

        if (sendMailEventListener != null) {
            sendMailEventListener.sendMail(mail);
        }
    }

    @Override
    public void sendMail(MailAddress sender, @SuppressWarnings("rawtypes") Collection recipients, 
        MimeMessage mimemessage)
    throws MessagingException 
    {
        this.sendMail(sender, recipients, mimemessage, Mail.DEFAULT);
    }

    @Override
    public void sendMail(MailAddress sender, @SuppressWarnings("rawtypes") Collection recipients, 
        MimeMessage mimemessage, String state)
    throws MessagingException 
    {
        MailImpl mail = new MailImpl("xxx", sender, recipients, mimemessage);

        mail.setState(state);

        this.sendMail(mail);
    }

    @Override
    public void storeMail(MailAddress sender, MailAddress recipient, MimeMessage mimemessage)
    throws MessagingException 
    {
        this.sendMail(sender, Collections.singleton(recipient), mimemessage);
    }
}
