/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.KeyStore;
import java.util.Properties;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.impl.DefaultGlobalPreferencesManager;
import mitm.application.djigzo.impl.hibernate.UserPreferencesCategoryManagerHibernate;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.StandardHierarchicalProperties;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.provider.MITMProvider;

import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

public class DefaultGlobalPreferencesManagerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager;
    
    private static KeyAndCertStore keyAndCertStore;
    private static UserPreferencesCategoryManager userPreferencesCategoryManager;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();

        InitializeBouncycastle.initialize();
     
        SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        KeyStore keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter("keyStore", sessionManager));

        X509CertStoreExt certStore = new X509CertStoreExtHibernate("certStore", sessionManager);
        
        keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, null);
        
        userPreferencesCategoryManager = new UserPreferencesCategoryManagerHibernate(keyAndCertStore, 
        		new PasswordBasedEncryptor("djigzo"), sessionManager);
    }

    @Test
    public void testAddGlobalPreferences()
    throws Exception 
    {
        Properties props = new Properties();
        
        props.setProperty("123", "abc");
        props.setProperty("456", "def");
        
        HierarchicalProperties factoryProperties = new StandardHierarchicalProperties("", null, props, null);
        
        assertEquals("abc", factoryProperties.getProperty("123", false));
        assertEquals("def", factoryProperties.getProperty("456", false));
        
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager, factoryProperties);
                        
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            assertEquals("abc", manager.getGlobalUserPreferences().getProperties().getProperty("123", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("123"));
            assertEquals("def", manager.getGlobalUserPreferences().getProperties().getProperty("456", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("456"));

            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }        
    }

    @Test
    public void testLoadFactoryProperties()
    throws Exception
    {
        Properties props = new Properties();
        
        props.setProperty("123", "abc");
        props.setProperty("456", "def");
        
        HierarchicalProperties factoryProperties = new StandardHierarchicalProperties("", null, props, null);
        
        assertEquals("abc", factoryProperties.getProperty("123", false));
        assertEquals("def", factoryProperties.getProperty("456", false));
        
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager, factoryProperties);
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            assertEquals("abc", manager.getGlobalUserPreferences().getProperties().getProperty("123", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("123"));
            assertEquals("def", manager.getGlobalUserPreferences().getProperties().getProperty("456", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("456"));

            props.clear();
            
            props.setProperty("789", "ghi");
            props.setProperty("abc", "jkl");
            
            manager.loadFactoryProperties(new StandardHierarchicalProperties("", null, props, null));
            
            assertEquals("ghi", manager.getGlobalUserPreferences().getProperties().getProperty("789", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("789"));
            assertEquals("jkl", manager.getGlobalUserPreferences().getProperties().getProperty("abc", false));
            assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("abc"));

            assertNull(manager.getGlobalUserPreferences().getProperties().getProperty("123", false));
            assertNull(manager.getGlobalUserPreferences().getProperties().getProperty("456", false));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }        
    }
    
    @Test
    public void testSetGlobalPreferencesProperties()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager, null);
            
            manager.getGlobalUserPreferences().getProperties().setProperty("prop1", "value", false);
            manager.getGlobalUserPreferences().getProperties().setProperty("prop1", "value2", false);
            manager.getGlobalUserPreferences().getProperties().setProperty("prop2", "value3", false);
            manager.getGlobalUserPreferences().getProperties().setProperty("prop3", "value3", false);
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            assertEquals("value2", manager.getGlobalUserPreferences().getProperties().getProperty("prop1", false));
            assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop1"));
            assertEquals("value3", manager.getGlobalUserPreferences().getProperties().getProperty("prop2", false));
            assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop2"));
            assertEquals("value3", manager.getGlobalUserPreferences().getProperties().getProperty("prop3", false));
            assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop3"));

            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }        
    }    
}
