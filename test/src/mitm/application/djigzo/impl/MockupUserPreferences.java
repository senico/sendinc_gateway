/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.impl.UserPropertiesImpl;
import mitm.common.properties.HierarchicalProperties;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.StandardHierarchicalProperties;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.SecurityFactoryFactoryException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MockupUserPreferences implements UserPreferences 
{
    private final String name;
    private final String category;
    private Set<X509Certificate> certificates = new HashSet<X509Certificate>();
    private Set<NamedCertificate> namedCertificates = new HashSet<NamedCertificate>();
    private Set<X509Certificate> inheritedCertificates = new HashSet<X509Certificate>();
    private Set<NamedCertificate> inheritedNamedCertificates = new HashSet<NamedCertificate>();
    private KeyAndCertificate keyAndCertificate;
    private Set<KeyAndCertificate> inheritedKeyAndCertificates = new HashSet<KeyAndCertificate>();;
    private HierarchicalProperties properties;
    private Set<UserPreferences> inheritedUserPreferences = new LinkedHashSet<UserPreferences>();
    private Set<NamedBlob> namedBlobs = new LinkedHashSet<NamedBlob>();
    
    public MockupUserPreferences(String name, String category) 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        this.name = name;
        this.category = category;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public String getCategory() {
        return category;
    }
    
    @Override
    public Set<X509Certificate> getCertificates() {
        return certificates;
    }

    @Override
    public Set<NamedCertificate> getNamedCertificates() {
        return namedCertificates;
    }
    
    @Override
    public Set<X509Certificate> getInheritedCertificates() {
        return inheritedCertificates;
    }
    
    @Override
    public Set<NamedCertificate> getInheritedNamedCertificates() {
        return inheritedNamedCertificates;
    }
    
    @Override
    public KeyAndCertificate getKeyAndCertificate()
    throws CertStoreException, KeyStoreException
    {
        return keyAndCertificate;
    }

    @Override
    public void setKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        this.keyAndCertificate = keyAndCertificate;
    }

    @Override
    public Set<KeyAndCertificate> getInheritedKeyAndCertificates()
    throws CertStoreException, KeyStoreException
    {
        return inheritedKeyAndCertificates;
    }
    
    @Override
    public UserProperties getProperties() 
    {
        if (properties == null) {
            properties = new StandardHierarchicalProperties(category, null, new Properties(), null);
        }
        
        return new UserPropertiesImpl(properties);
    }
    
    @Override
    public Set<NamedBlob> getNamedBlobs() {
        return namedBlobs;
    }
    
    @Override
    public Set<UserPreferences> getInheritedUserPreferences() {
        return inheritedUserPreferences;
    }
    
    @Override
    public String toString() {
        return getName() + ":" + getCategory();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferences)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        UserPreferences rhs = (UserPreferences) obj;
        
        return new EqualsBuilder()
            .append(getCategory(), rhs.getCategory())
            .append(getName(), rhs.getName())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getCategory())
            .append(getName())
            .toHashCode();    
    }        
}
