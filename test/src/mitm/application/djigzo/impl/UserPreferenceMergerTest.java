/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.UserPreferenceMerger;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.impl.DefaultUserPreferenceMerger;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.bouncycastle.InitializeBouncycastle;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserPreferenceMergerTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
    }
    
    @Test
    public void testAddNew() 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<UserPreferences>();
        
        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.getName()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.getName()));
        targetPreferences.add(new MockupUserPreferences("prefs3", "otherCategory"));
        
        Set<UserPreferences> newPreferences = new LinkedHashSet<UserPreferences>();

        newPreferences.add(new MockupUserPreferences("prefs4", UserPreferencesCategory.GLOBAL.getName()));
        newPreferences.add(new MockupUserPreferences("prefs6", UserPreferencesCategory.DOMAIN.getName()));
        newPreferences.add(new MockupUserPreferences("prefs7", UserPreferencesCategory.CUSTOM.getName()));
        
        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();
        
        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);
    
        assertEquals(6, mergedPreferences.size());

        List<UserPreferences> list = new LinkedList<UserPreferences>(mergedPreferences);
        
        assertEquals("prefs4", list.get(0).getName());;
        assertEquals("prefs6", list.get(1).getName());;
        assertEquals("prefs7", list.get(2).getName());;
        assertEquals("prefs2", list.get(3).getName());;
        assertEquals("prefs3", list.get(4).getName());;
        assertEquals("prefs1", list.get(5).getName());;
    }

    @Test
    public void testAddExisting() 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<UserPreferences>();
        
        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.getName()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.getName()));
        targetPreferences.add(new MockupUserPreferences("prefs3", "otherCategory"));
        targetPreferences.add(new MockupUserPreferences("prefs4", UserPreferencesCategory.USER.getName()));
        
        Set<UserPreferences> newPreferences = new LinkedHashSet<UserPreferences>();

        newPreferences.add(new MockupUserPreferences("prefs5", UserPreferencesCategory.GLOBAL.getName()));
        newPreferences.add(new MockupUserPreferences("prefs7", UserPreferencesCategory.DOMAIN.getName()));
        newPreferences.add(new MockupUserPreferences("prefs8", UserPreferencesCategory.CUSTOM.getName()));
        
        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();
        
        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);
    
        assertEquals(7, mergedPreferences.size());
        
        List<UserPreferences> list = new LinkedList<UserPreferences>(mergedPreferences);
        
        assertEquals("prefs5", list.get(0).getName());;
        assertEquals("prefs7", list.get(1).getName());;
        assertEquals("prefs8", list.get(2).getName());;
        assertEquals("prefs2", list.get(3).getName());;
        assertEquals("prefs3", list.get(4).getName());;
        assertEquals("prefs1", list.get(5).getName());;        
    }

    @Test
    public void testNoMerge() 
    throws NoSuchAlgorithmException, NoSuchProviderException, SecurityFactoryFactoryException
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<UserPreferences>();
        
        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.getName()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.getName()));
        
        Set<UserPreferences> newPreferences = new LinkedHashSet<UserPreferences>();

        newPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.getName()));
        newPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.getName()));
        
        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();
        
        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);
    
        assertNull(mergedPreferences);
    }
}
