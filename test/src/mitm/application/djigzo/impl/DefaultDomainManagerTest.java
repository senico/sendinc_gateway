/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.KeyStore;
import java.util.Properties;
import java.util.Set;

import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategory;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.impl.DefaultDomainManager;
import mitm.application.djigzo.impl.DefaultGlobalPreferencesManager;
import mitm.application.djigzo.impl.hibernate.UserPreferencesCategoryManagerHibernate;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.properties.StandardHierarchicalProperties;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DefaultDomainManagerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager;
    
    private static KeyAndCertStore keyAndCertStore;
    private static UserPreferencesCategoryManager userPreferencesCategoryManager;
    private static DomainManager domainManager;
    private static GlobalPreferencesManager globalPreferencesManager;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        KeyStore keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter("keyStore", sessionManager));

        X509CertStoreExt certStore = new X509CertStoreExtHibernate("certStore", sessionManager);
        
        keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, null);
        
        userPreferencesCategoryManager = new UserPreferencesCategoryManagerHibernate(keyAndCertStore, 
        		new PasswordBasedEncryptor("djigzo"), sessionManager);
        
        globalPreferencesManager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
        		new StandardHierarchicalProperties("", null, new Properties(), null));
        
        domainManager = new DefaultDomainManager(userPreferencesCategoryManager, globalPreferencesManager);
    }

    @Before
    public void setup()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            /* remove all domains */
            CloseableIterator<String> domainIterator = null;
            
            try {
                domainIterator = domainManager.getDomainIterator();
                /*
                 * Clear inherited first before deleting to make sure that we can delete all domains
                 */
                while (domainIterator.hasNext()) {
                    domainManager.getDomainPreferences(domainIterator.next()).getInheritedUserPreferences().clear();
                }

                tx.commit();

                tx = sessionSource.getSession().beginTransaction();

                domainIterator = domainManager.getDomainIterator();
                /*
                 * Delete all domains
                 */
                while (domainIterator.hasNext()) 
                {
                    UserPreferences domainPreferences = domainManager.getDomainPreferences(domainIterator.next());
                    
                    assertTrue(domainManager.deleteDomainPreferences(domainPreferences));
                }
                
                tx.commit();
            }
            finally {
                CloseableIteratorUtils.closeQuietly(domainIterator);
            }
        }
        catch(Exception e) {
            tx.rollback();

            throw e;
        }
        finally {
            sessionSource.getSession().close();            
        }
    }

    @Test
    public void testAddDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            assertNotNull(domainManager.addDomain("exAMPle.com"));

            UserPreferences prefs = domainManager.getDomainPreferences("example.com");
            
            assertNotNull(prefs);
            assertEquals(UserPreferencesCategory.DOMAIN.getName(), prefs.getCategory());
            assertEquals("example.com", prefs.getName());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }

    @Test
    public void testAddWildcardDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferences prefs = domainManager.addDomain("*.example.com"); 
            
            assertNotNull(prefs);
            assertEquals(UserPreferencesCategory.DOMAIN.getName(), prefs.getCategory());
            assertEquals("*.example.com", prefs.getName());
            
            Set<UserPreferences> inherited = prefs.getInheritedUserPreferences();
            
            assertEquals(1, inherited.size());
            
            assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }

    @Test
    public void testAddWildcardDomainAfterDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferences domainPrefs = domainManager.addDomain("example.com"); 
            UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com"); 

            UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com"); 
                        
            Set<UserPreferences> inherited = wildcardPrefs.getInheritedUserPreferences();
            
            assertEquals(1, inherited.size());
            
            assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

            inherited = domainPrefs.getInheritedUserPreferences();
            /*
             * example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();
            /*
             * sub.example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }
    
    @Test
    public void testDomainAfterWildcardDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com"); 

            UserPreferences domainPrefs = domainManager.addDomain("example.com"); 
            UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com"); 
                        
            Set<UserPreferences> inherited = wildcardPrefs.getInheritedUserPreferences();
            
            assertEquals(1, inherited.size());
            
            assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

            inherited = domainPrefs.getInheritedUserPreferences();
            /*
             * example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();
            /*
             * sub.example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }    
    
    @Test
    public void testAddLowerPrioWildcardDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferences wildcardPrefs = domainManager.addDomain("*.sub.Example.com"); 

            UserPreferences domainPrefs = domainManager.addDomain("eXAmple.com"); 
            UserPreferences subDomainPrefs = domainManager.addDomain("sub.examPLE.com"); 
                        
            Set<UserPreferences> inherited = domainPrefs.getInheritedUserPreferences();
            /*
             * example.com should inherit from global
             */
            assertEquals(1, inherited.size());

            assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();            
            /*
             * sub.example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            /*
             * Now add a lower prio wildcard domain
             */
            UserPreferences lowerPrioWildcardPrefs = domainManager.addDomain("*.example.COM"); 

            inherited = domainPrefs.getInheritedUserPreferences();            
            /*
             * example.com should inherit from lower prio wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(lowerPrioWildcardPrefs, inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();            
            /*
             * sub.example.com should still inherit from 'old' wildcard prefs and not the new one
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());
                        
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }        
        
    @Test
    public void testAddHigherPrioWildcardDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com"); 

            UserPreferences domainPrefs = domainManager.addDomain("example.com"); 
            UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com"); 
                        
            Set<UserPreferences> inherited = domainPrefs.getInheritedUserPreferences();
            /*
             * example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();            
            /*
             * sub.example.com should inherit from wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            /*
             * Now add a higher prio wildcard domain
             */
            UserPreferences higherPrioWildcardPrefs = domainManager.addDomain("*.sub.example.com"); 

            inherited = domainPrefs.getInheritedUserPreferences();            
            /*
             * example.com should still inherit from 'old' wildcard
             */
            assertEquals(1, inherited.size());

            assertEquals(wildcardPrefs, inherited.iterator().next());

            inherited = subDomainPrefs.getInheritedUserPreferences();            
            /*
             * sub.example.com should inherit from higher wildcard prefs
             */
            assertEquals(1, inherited.size());

            assertEquals(higherPrioWildcardPrefs, inherited.iterator().next());
                        
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }        
                
    @Test(expected=ConstraintViolationException.class)
    public void testAddExistingDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            domainManager.addDomain("exAMPle.com");

            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            domainManager.addDomain("example.com");
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }    

    @Test
    public void testDeleteDomain()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            domainManager.addDomain("exAMPle.com");
            domainManager.addDomain("123.com");

            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            assertTrue(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("xxx")));
            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
            
            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
            assertNotNull(domainManager.getDomainPreferences("123.com"));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }    
}
