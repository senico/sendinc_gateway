/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl;

import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.impl.hibernate.UserHibernate;
import mitm.common.security.KeyAndCertificate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MockupUser implements User
{
    private final String email;
    private final UserPreferences userPreferences;
    private final Set<X509Certificate> autoSelectEncryptionCertificates;
    private KeyAndCertificate signingKeyAndCertificate;
    
    public MockupUser(String email, UserPreferences userPreferences, Set<X509Certificate> autoSelectEncryptionCertificates) 
    {
        this.email = email;
        this.userPreferences = userPreferences;
        this.autoSelectEncryptionCertificates = autoSelectEncryptionCertificates;
    }
    
    @Override
    public String getEmail() {
        return email;
    }
    
    @Override
    public UserPreferences getUserPreferences() {
        return userPreferences;
    }
    
    @Override
    public Set<X509Certificate> getAutoSelectEncryptionCertificates() {
        return autoSelectEncryptionCertificates;
    }
    
    @Override
    public void setSigningKeyAndCertificate(KeyAndCertificate keyAndCertificate) 
    throws CertStoreException, KeyStoreException
    {
        this.signingKeyAndCertificate = keyAndCertificate;
    }

    @Override
    public KeyAndCertificate getSigningKeyAndCertificate() 
    throws CertStoreException, KeyStoreException
    {
        return signingKeyAndCertificate;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserHibernate)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        UserHibernate rhs = (UserHibernate) obj;
        
        return new EqualsBuilder()
            .append(getEmail(), rhs.getEmail())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getEmail())
            .toHashCode();    
    }    
}