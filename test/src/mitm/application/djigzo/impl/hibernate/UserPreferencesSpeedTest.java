/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import static org.junit.Assert.assertTrue;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Warning: this test uses the non test database!!
 * 
 * This test is used to check the speed of the UserPreferences classes. The test assumes that the database
 * contains specfic users/domains. This test is therefore not added to the test suites.
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesSpeedTest
{
    private static String USE_PRODUCTION_DB_SPRING_CONFIG = "test/resources/spring/djigzo-use-production-db.xml";
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize(USE_PRODUCTION_DB_SPRING_CONFIG);
    }
        
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            SessionManager sessionManager = SystemServices.getSessionManager();

            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public boolean isDomainInUse(String domain)
        {
            return SystemServices.getDomainManager().isDomainInUse(domain);
        }
    }

    @Test
    public void testIsDomainInUse()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        int repeat = 50;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            assertTrue("test assumes the domain is in use", proxy.isDomainInUse("example.com"));
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = repeat * 1000.0 / diff;

        System.out.println("isDomainInUse calls/sec: " + perSecond);

        /*
         * On a Core2 Q8300 with 61000 user in domain example.com  I get about 40 calls/sec in non-debug mode.
         */
    }
}
