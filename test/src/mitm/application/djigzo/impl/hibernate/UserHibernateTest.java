/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.Inherited;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certificate.CertificateUtils;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserHibernateTest
{
    private static SessionManager sessionManager;
    
    private static UserWorkflow userWorkflow;
    
    private static GlobalPreferencesManager globalPreferencesManager;
    
    private static DatabaseActionExecutor actionExecutor;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();        
    }
    
    @Before
    public void setup() 
    throws Exception
    {
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());

        sessionManager = SystemServices.getSessionManager();

        userWorkflow = SystemServices.getUserWorkflow();
        
        globalPreferencesManager = SystemServices.getGlobalPreferencesManager();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);

        addUser("test@example.com");
        addUser("m.brinkers@pobox.com");
        
        loadKeys();
        loadCAs();
        
    }
    
    private static void loadKeys()
    throws Exception
    {
        KeyAndCertificateWorkflow keyAndCertificateWorkflow = SystemServices.getKeyAndCertificateWorkflow();

        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        keyStore.load(new FileInputStream(new File("test/resources/testdata/keys/testCertificates.p12")), 
                "test".toCharArray());

        keyAndCertificateWorkflow.importKeyStore(keyStore, KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);
        
    }
    
    private static void loadCAs() 
    throws Exception
    {
        CertificateWorkflow rootWorkflow = SystemServices.getKeyAndRootCertificateWorkflow();

        importCertificates(rootWorkflow, new File("test/resources/testdata/certificates/mitm-test-root.cer"));
    }
    
    private static void importCertificates(CertificateWorkflow rootWorkflow, File certificateFile) 
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate) {
                rootWorkflow.addCertificateTransacted((X509Certificate) certificate);
            }
        }
    }
    
    private static void addUser(final String email)
    throws Exception 
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        DatabaseActionExecutor actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(
        		sessionManager);

        actionExecutor.executeTransaction(
        new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session)
            throws DatabaseException
            {
                Session previousSession = sessionManager.getSession();
                try {
                	sessionManager.setSession(session);

                    addUserAction(session, email);
                }
                finally {
                	sessionManager.setSession(previousSession);
                }
            }
        });
    }
     
    public static void addUserAction(Session session, String email)
    throws DatabaseException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
                
        try {
            User user = userWorkflow.addUser(email);
            
            userWorkflow.makePersistent(user);
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }
    
    private User getUser(String email) 
    throws AddressException, HierarchicalPropertiesException
    {
        String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, false);
        
        if (filteredEmail == null) {
            throw new AddressException("email address '" + email + "' is invalid.");
        }
        
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
        
        User user = userWorkflow.getUser(filteredEmail, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);
        
        return user;
    }    
    
    @Test
    public void testGetSigningKeyAndCertificate()
    throws Exception
    {
        final String email = "test@example.com";
        
        KeyAndCertificate keyAndCertificate = actionExecutor.executeTransaction(
            new DatabaseAction<KeyAndCertificate>()
            {
                @Override
                public KeyAndCertificate doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        return getKeyAndCertificateAction(email);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());
    }
    
    private KeyAndCertificate getKeyAndCertificateAction(String email)
    throws DatabaseException
    {
        try {
            User user = getUser(email);
            
            return user.getSigningKeyAndCertificate();
        }
        catch (AddressException e) {
            throw new DatabaseException(e);
        }
        catch (CertStoreException e) {
            throw new DatabaseException(e);
        }
        catch (KeyStoreException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }

    @Test
    public void testGetSigningKeyAndCertificateUnknownUser()
    throws Exception
    {
        final String email = "testxxx@example.com";
        
        KeyAndCertificate keyAndCertificate = actionExecutor.executeTransaction(
            new DatabaseAction<KeyAndCertificate>()
            {
                @Override
                public KeyAndCertificate doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        return getKeyAndCertificateAction(email);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        assertNull(keyAndCertificate);
    }

    @Test
    public void testSetSigningKeyAndCertificate()
    throws Exception
    {
        final String email = "m.brinkers@pobox.com";

        actionExecutor.executeTransaction(
            new DatabaseVoidAction()
            {
                @Override
                public void doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        User user = getUser(email);
                        
                        KeyAndCertificate keyAndCertificate = user.getSigningKeyAndCertificate();
                        
                        assertNull(keyAndCertificate);
                        
                        user = userWorkflow.getUser("test@example.com", UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
                        
                        keyAndCertificate = user.getSigningKeyAndCertificate();
                        
                        user = getUser(email);
                        
                        user.setSigningKeyAndCertificate(keyAndCertificate);
                    }
                    catch(Exception e) {
                        throw new DatabaseException(e);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        KeyAndCertificate keyAndCertificate = actionExecutor.executeTransaction(
            new DatabaseAction<KeyAndCertificate>()
            {
                @Override
                public KeyAndCertificate doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        return getKeyAndCertificateAction(email);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        assertNotNull(keyAndCertificate);
        assertFalse(keyAndCertificate instanceof Inherited);
    }
    
    @Test
    public void testInheritSigningKeyAndCertificate()
    throws Exception
    {
        final String email = "m.brinkers@pobox.com";

        actionExecutor.executeTransaction(
            new DatabaseVoidAction()
            {
                @Override
                public void doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        User user = getUser(email);
                        
                        KeyAndCertificate keyAndCertificate = user.getSigningKeyAndCertificate();
                        
                        assertNull(keyAndCertificate);
                        
                        user = userWorkflow.getUser("test@example.com", UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
                        
                        keyAndCertificate = user.getSigningKeyAndCertificate();
                        
                        globalPreferencesManager.getGlobalUserPreferences().setKeyAndCertificate(keyAndCertificate);                        
                    }
                    catch(Exception e) {
                        throw new DatabaseException(e);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        KeyAndCertificate keyAndCertificate = actionExecutor.executeTransaction(
            new DatabaseAction<KeyAndCertificate>()
            {
                @Override
                public KeyAndCertificate doAction(Session session)
                throws DatabaseException
                {
                	sessionManager.setSession(session);
                    
                    try {
                        return getKeyAndCertificateAction(email);
                    }
                    finally {
                    	sessionManager.setSession(null);                            
                    }
                }
            });
        
        assertNotNull(keyAndCertificate);
        assertTrue(keyAndCertificate instanceof Inherited);
    }    
}
