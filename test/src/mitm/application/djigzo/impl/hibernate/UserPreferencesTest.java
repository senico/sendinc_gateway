/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.UserPreferencesCategoryManager;
import mitm.application.djigzo.UserPreferencesManager;
import mitm.application.djigzo.impl.NamedCertificateImpl;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.NamedBlobManager;
import mitm.common.properties.hibernate.NamedBlobManagerImpl;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserPreferencesTest
{
    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesTest.class);
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static final String NAMED_BLOB_CATEGORY = "test_category";
    private static final String NAMED_BLOB_1 = "blob1";
    private static final String NAMED_BLOB_2 = "blob2";
    private static final String NAMED_BLOB_3 = "blob3";
    
    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager; 
    
    private static KeyAndCertStore keyAndCertStore;
    private static UserPreferencesCategoryManager userPreferencesCategoryManager;
    
    private static NamedBlobManager namedBlobManager;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);
        
        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        KeyStore keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter("test key store", sessionManager));

        X509CertStoreExt certStore = new X509CertStoreExtHibernate("test cert store", sessionManager);
        
        keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, null);
        
        userPreferencesCategoryManager = new UserPreferencesCategoryManagerHibernate(keyAndCertStore, 
        		new PasswordBasedEncryptor("djigzo"), sessionManager);
        
        namedBlobManager = new NamedBlobManagerImpl(sessionManager);
        
        addNamedBlobs();
    }

    private static void addNamedBlobs()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            namedBlobManager.deleteAll();
            namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);
            namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);
            namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_3);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();

            throw e;
        }
        finally {
        }
    }
    
    @Before
    public void setup()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            removeAllUserPreferences();
            
            keyAndCertStore.removeAllEntries();
    
            importKeyStore(keyAndCertStore, new File("test/resources/testdata/keys/testCertificates.p12"), "test");
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();

            throw e;
        }
        finally {
        }
    }

    private static void removeAllUserPreferences()
    throws CloseableIteratorException
    {
        CloseableIterator<String> categoryIterator = userPreferencesCategoryManager.getCategoryIterator();
        
        /* 
         * Because user preferences can contain loops we first have to clear the inherited collections before removing.
         */
        try {
            /* first remove all inherited */
            while (categoryIterator.hasNext())
            {
                String category = categoryIterator.next();
                
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
                
                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();
                
                try {
                    while (nameIterator.hasNext())
                    {
                        String name = nameIterator.next();
                        
                        UserPreferences prefs = userPreferencesManager.getUserPreferences(name);
                        
                        prefs.getInheritedUserPreferences().clear();
                    }
                }
                finally {
                    nameIterator.close();
                }
            }
        }
        finally {
            categoryIterator.close();
        }

        categoryIterator = userPreferencesCategoryManager.getCategoryIterator();
        
        try {
            /* now delete all prefs from every category */
            while (categoryIterator.hasNext())
            {
                String category = categoryIterator.next();
                
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
                
                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();
                
                try {
                    while (nameIterator.hasNext())
                    {
                        String name = nameIterator.next();
                        
                        UserPreferences userPreferences = userPreferencesManager.getUserPreferences(name);
                        
                        assertTrue(userPreferencesManager.deleteUserPreferences(userPreferences));
                    }
                }
                finally {
                    nameIterator.close();
                }
            }
        }
        finally {
            categoryIterator.close();
        }
    }
    
    private static void importKeyStore(KeyAndCertStore keyAndCertStore, File pfxfile, String password) 
    throws Exception 
    {        
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(pfxfile), password.toCharArray());
        
        Enumeration<String> aliases = keyStore.aliases();
        
        while (aliases.hasMoreElements()) 
        {
            String alias = aliases.nextElement();

            Certificate certificate = keyStore.getCertificate(alias);
            
            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            try {
                Key key = keyStore.getKey(alias, null);

                if (!(key instanceof PrivateKey)) {
                    continue;
                }
                
                keyAndCertStore.addKeyAndCertificate(new KeyAndCertificateImpl((PrivateKey) key, (X509Certificate) certificate));
            }
            catch (UnrecoverableKeyException e) {
                logger.error("Could not get the key.",e);
            }
            catch (CertStoreException e) {
                logger.error("CertStoreException.",e);
            }
        }
    }
    
    @Test
    public void testAddInheritedNamedCertificates()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");
    
            File file = new File("test/resources/testdata/certificates/equifax.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);        
    
            NamedCertificate namedCertificate = new NamedCertificateImpl("name", certificate);
            
            userPreferences.getNamedCertificates().add(namedCertificate);
                    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");
    
            file = new File("test/resources/testdata/certificates/rim.cer");
            
            X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);        
    
            NamedCertificate inheritedNamedCertificate = new NamedCertificateImpl("inherited", inheritedCertificate);
            
            inheritedPreferences.getNamedCertificates().add(inheritedNamedCertificate);
            
            UserPreferences inheritedInheritedPreferences = userPreferencesManager.addUserPreferences("inherited inherited prefs");
    
            file = new File("test/resources/testdata/certificates/intel-crl-test.cer");
            
            X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);        

            NamedCertificate inheritedInheritedNamedCertificate = new NamedCertificateImpl("inheritedinherited", inheritedInheritedCertificate);
            
            inheritedInheritedPreferences.getNamedCertificates().add(inheritedInheritedNamedCertificate);
            
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);
            
            assertEquals(1, userPreferences.getInheritedUserPreferences().size());
    
            assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());
            
            assertTrue(userPreferences.getNamedCertificates().contains(namedCertificate));
            assertEquals(1, userPreferences.getNamedCertificates().size());
    
            assertTrue(inheritedPreferences.getNamedCertificates().contains(inheritedNamedCertificate));
            assertEquals(1, inheritedPreferences.getNamedCertificates().size());
    
            assertTrue(userPreferences.getInheritedNamedCertificates().contains(inheritedNamedCertificate));
            assertTrue(userPreferences.getInheritedNamedCertificates().contains(inheritedInheritedNamedCertificate));
            assertEquals(2, userPreferences.getInheritedNamedCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }            
    }
    
    @Test
    public void testRemoveNamedCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate certificate = certificates.iterator().next();
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

            assertEquals(0, userPreferences.getNamedCertificates().size());
            
            userPreferences.getNamedCertificates().add(namedCertificate);
            
            assertEquals(1, userPreferences.getNamedCertificates().size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();
            
            userPreferences = userPreferencesManager.getUserPreferences("test");
            
            assertNotNull(userPreferences);

            assertEquals(1, userPreferences.getNamedCertificates().size());

            NamedCertificate toRemove = new NamedCertificateImpl("test", certificate);
            
            userPreferences.getNamedCertificates().remove(toRemove);

            assertEquals(0, userPreferences.getNamedCertificates().size());
            
            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            assertEquals(0, userPreferences.getNamedCertificates().size());

            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddDuplicateCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> allCertificates = keyAndCertStore.getCertificates(null);

            X509Certificate certificate = allCertificates.iterator().next();
            
            Set<X509Certificate> certificates = userPreferences.getCertificates(); 
            
            assertEquals(0, certificates.size());
            
            certificates.add(certificate);
            
            assertEquals(1, certificates.size());
            assertEquals(1, userPreferences.getCertificates().size());

            allCertificates = keyAndCertStore.getCertificates(null);

            X509Certificate duplicateCertificate = allCertificates.iterator().next();
            
            assertEquals(certificate, duplicateCertificate);
            assertFalse(System.identityHashCode(certificate) == System.identityHashCode(duplicateCertificate));
            
            certificates.add(certificate);
            assertEquals(1, certificates.size());
            assertEquals(1, userPreferences.getCertificates().size());
                        
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddNamedCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate certificate = certificates.iterator().next();
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

            assertEquals(0, userPreferences.getNamedCertificates().size());
            
            userPreferences.getNamedCertificates().add(namedCertificate);
            
            assertEquals(1, userPreferences.getNamedCertificates().size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();
            
            userPreferences = userPreferencesManager.getUserPreferences("test");
            
            assertNotNull(userPreferences);

            assertEquals(1, userPreferences.getNamedCertificates().size());

            NamedCertificate retrieved = userPreferences.getNamedCertificates().iterator().next();
            
            assertEquals("test", retrieved.getName());
            assertEquals(certificate, retrieved.getCertificate());
            assertEquals(namedCertificate, retrieved);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }

    @Test
    public void testAddDuplicateNamedCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate certificate = certificates.iterator().next();
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

            certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate duplicateCertificate = certificates.iterator().next();
            
            NamedCertificate duplicateNamedCertificate = new NamedCertificateImpl("test", duplicateCertificate);
            
            assertEquals(0, userPreferences.getNamedCertificates().size());
            
            userPreferences.getNamedCertificates().add(namedCertificate);
            userPreferences.getNamedCertificates().add(duplicateNamedCertificate);
            
            assertEquals(1, userPreferences.getNamedCertificates().size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();
            
            userPreferences = userPreferencesManager.getUserPreferences("test");
            
            assertNotNull(userPreferences);

            assertEquals(1, userPreferences.getNamedCertificates().size());

            NamedCertificate retrieved = userPreferences.getNamedCertificates().iterator().next();
            
            assertEquals("test", retrieved.getName());
            assertEquals(certificate, retrieved.getCertificate());
            assertEquals(namedCertificate, retrieved);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }

    @Test
    public void testAddMultipleNamedCertificates()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            Iterator<X509Certificate> it = certificates.iterator();
            
            X509Certificate certificate = it.next();
            X509Certificate otherCertificate = it.next();
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

            NamedCertificate namedCertificate2 = new NamedCertificateImpl("test2", certificate);

            NamedCertificate namedCertificate3 = new NamedCertificateImpl("test2", otherCertificate);
            
            assertEquals(0, userPreferences.getNamedCertificates().size());
            
            userPreferences.getNamedCertificates().add(namedCertificate);
            userPreferences.getNamedCertificates().add(namedCertificate2);
            userPreferences.getNamedCertificates().add(namedCertificate3);
            
            assertEquals(3, userPreferences.getNamedCertificates().size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();
            
            userPreferences = userPreferencesManager.getUserPreferences("test");
            
            assertNotNull(userPreferences);

            assertEquals(3, userPreferences.getNamedCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddDuplicateNamedCertificateSingleCollection()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category");
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate certificate = certificates.iterator().next();
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

            certificates = keyAndCertStore.getCertificates(null);
            
            X509Certificate duplicateCertificate = certificates.iterator().next();
            
            NamedCertificate duplicateNamedCertificate = new NamedCertificateImpl("test", duplicateCertificate);
            
            Set<NamedCertificate> namedCertificates = userPreferences.getNamedCertificates(); 
            
            assertEquals(0, namedCertificates.size());
            
            namedCertificates.add(namedCertificate);
            namedCertificates.add(duplicateNamedCertificate);
            namedCertificates.contains(namedCertificate);
            namedCertificates.contains(duplicateNamedCertificate);
            
            assertEquals(1, namedCertificates.size());
            
            tx.commit();            
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddSameCertificateOtherPreferences()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("pref1");
    
            File file = new File("test/resources/testdata/certificates/equifax.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);        
    
            userPreferences.getCertificates().add(certificate);

            UserPreferences userPreferences2 = userPreferencesManager.addUserPreferences("pref2");
            
            userPreferences2.getCertificates().add(certificate);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }            
    }
    
    @Test
    public void testInUse()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        try {
            String category = "category-2";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");
    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");
    
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            assertFalse(userPreferencesManager.isInUse(userPreferences));
            assertTrue(userPreferencesManager.isInUse(inheritedPreferences));

            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testInheritedOrder()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));
    
            UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");            
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager.getUserPreferences("test2"));

            UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");            
            assertNotNull(prefs3);
            assertNotNull(userPreferencesManager.getUserPreferences("test3"));

            UserPreferences prefs4 = userPreferencesManager.addUserPreferences("test4");            
            assertNotNull(prefs4);
            assertNotNull(userPreferencesManager.getUserPreferences("test4"));
            
            prefs1.getInheritedUserPreferences().add(prefs2);
            prefs1.getInheritedUserPreferences().add(prefs3);
            prefs1.getInheritedUserPreferences().add(prefs4);
            
            assertEquals(3, prefs1.getInheritedUserPreferences().size());            
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            prefs1 = userPreferencesManager.getUserPreferences("test1");            
            
            Set<UserPreferences> inherited = prefs1.getInheritedUserPreferences();
            
            Iterator<UserPreferences> prefsIt = inherited.iterator();
            
            assertEquals("test2", prefsIt.next().getName());
            assertEquals("test3", prefsIt.next().getName());
            assertEquals("test4", prefsIt.next().getName());
            
            tx.commit();

            // now reorder
            tx = sessionSource.getSession().beginTransaction();

            prefs1 = userPreferencesManager.getUserPreferences("test1");            
            
            inherited = prefs1.getInheritedUserPreferences();
            
            prefsIt = inherited.iterator();
            
            prefs2 = prefsIt.next();
            prefs3 = prefsIt.next();
            prefs4 = prefsIt.next();
            
            prefs1.getInheritedUserPreferences().clear();

            prefs1.getInheritedUserPreferences().add(prefs4);
            prefs1.getInheritedUserPreferences().add(prefs3);
            prefs1.getInheritedUserPreferences().add(prefs2);

            tx.commit();

            tx = sessionSource.getSession().beginTransaction();

            prefs1 = userPreferencesManager.getUserPreferences("test1");            
            
            inherited = prefs1.getInheritedUserPreferences();
            
            prefsIt = inherited.iterator();
            
            assertEquals("test4", prefsIt.next().getName());
            assertEquals("test3", prefsIt.next().getName());
            assertEquals("test2", prefsIt.next().getName());
            
            tx.commit();            
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }    
        
    @Test
    public void testCategoryIterator()
    throws Exception
    {
        Session session = sessionSource.newSession();
        
        sessionManager.setSession(session);
        
        Transaction tx = session.beginTransaction();

        try {
            UserPreferencesManager userPreferencesManager1 = userPreferencesCategoryManager.getUserPreferencesManager("category-1");
            
            UserPreferences prefs1 = userPreferencesManager1.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager1.getUserPreferences("test1"));
            assertEquals("category-1", prefs1.getCategory());
    
            UserPreferences prefs2 = userPreferencesManager1.addUserPreferences("test2");            
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager1.getUserPreferences("test2"));

            UserPreferencesManager userPreferencesManager2 = userPreferencesCategoryManager.getUserPreferencesManager("category-2");
            
            prefs1 = userPreferencesManager2.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager2.getUserPreferences("test1"));
    
            prefs2 = userPreferencesManager2.addUserPreferences("test2");            
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager2.getUserPreferences("test2"));
                        
            tx.commit();

            /*
             * Because getCategoryIterator uses a stateless session it's only valid after the commit.
             */
            tx = sessionSource.getSession().beginTransaction();
            
            List<String> names = new LinkedList<String>();
            
            CloseableIterator<String> nameIterator = userPreferencesCategoryManager.getCategoryIterator();

            try {
                while (nameIterator.hasNext())
                {
                    names.add(nameIterator.next());
                }
            }
            finally {
                nameIterator.close();
            }
            
            assertEquals(2, names.size());

            names.clear();
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager("category-1");

            nameIterator = userPreferencesManager.getNameIterator();

            try {
                while (nameIterator.hasNext())
                {
                    names.add(nameIterator.next());
                }
            }
            finally {
                nameIterator.close();
            }
            
            assertEquals(2, names.size());
            
            tx.commit();            
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        	sessionManager.setSession(null);

            sessionSource.closeSession(session);
        }
    }        

    @Test
    public void testUserPreferencesLoop()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));
    
            UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");            
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager.getUserPreferences("test2"));

            UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");            
            assertNotNull(prefs3);
            assertNotNull(userPreferencesManager.getUserPreferences("test3"));
            
            prefs1.getInheritedUserPreferences().add(prefs2);
            prefs2.getInheritedUserPreferences().add(prefs3);
            prefs3.getInheritedUserPreferences().add(prefs1);
            
            assertEquals(1, prefs1.getInheritedUserPreferences().size());
            
            prefs2.getProperties().setProperty("prop", "123", false);
            prefs3.getProperties().setProperty("prop", "456", false);

            assertEquals("123", prefs1.getProperties().getProperty("prop", false));
            
            assertEquals(0, prefs1.getInheritedCertificates().size());

            assertEquals(0, prefs1.getInheritedKeyAndCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }    
    
    @Test
    public void testUserPreferencesLoopDirect()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-2";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));
    
            prefs1.getInheritedUserPreferences().add(prefs1);

            assertEquals(1, prefs1.getInheritedUserPreferences().size());
            
            assertEquals(0, prefs1.getInheritedCertificates().size());
            assertEquals(0, prefs1.getCertificates().size());

            assertEquals(0, prefs1.getInheritedKeyAndCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }    
    
    @Test
    public void testReorderUserPreferences()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";

            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");            
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));
    
            UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");            
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager.getUserPreferences("test2"));

            UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");            
            assertNotNull(prefs3);
            assertNotNull(userPreferencesManager.getUserPreferences("test3"));
            
            prefs1.getInheritedUserPreferences().add(prefs2);
            prefs1.getInheritedUserPreferences().add(prefs3);
            
            assertEquals(2, prefs1.getInheritedUserPreferences().size());
            
            prefs2.getProperties().setProperty("prop", "123", false);
            prefs3.getProperties().setProperty("prop", "456", false);

            assertEquals("456", prefs1.getProperties().getProperty("prop", false));
            assertTrue(prefs1.getProperties().isInherited("prop"));
            
            // now change order
            prefs1.getInheritedUserPreferences().clear();
            prefs1.getInheritedUserPreferences().add(prefs3);
            prefs1.getInheritedUserPreferences().add(prefs2);
            
            assertEquals("123", prefs1.getProperties().getProperty("prop", false));
            assertTrue(prefs1.getProperties().isInherited("prop"));            
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddInheritedProperties()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        try {
            String category = "category-2";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");
    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");
    
            UserPreferences inheritedInheritedPreferences = userPreferencesManager.addUserPreferences("inherited inherited prefs");
            
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);
            
            assertEquals(1, userPreferences.getInheritedUserPreferences().size());
    
            assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());        
            
            userPreferences.getProperties().setProperty("prop", "123", false);        
            inheritedPreferences.getProperties().setProperty("inherited prop", "456", false);
            inheritedInheritedPreferences.getProperties().setProperty("inherited inherited prop", "789", false);
            
            assertEquals("123", userPreferences.getProperties().getProperty("prop", false));
            assertEquals("456", userPreferences.getProperties().getProperty("inherited prop", false));
            assertEquals("789", userPreferences.getProperties().getProperty("inherited inherited prop", false));
            
            assertFalse(userPreferences.getProperties().isInherited("prop"));
            assertTrue(userPreferences.getProperties().isInherited("inherited prop"));
            assertTrue(userPreferences.getProperties().isInherited("inherited inherited prop"));
            assertTrue(userPreferences.getProperties().isInherited("non existing property"));
    
            assertFalse(inheritedPreferences.getProperties().isInherited("inherited prop"));
            assertTrue(inheritedPreferences.getProperties().isInherited("inherited inherited prop"));
    
            assertFalse(inheritedInheritedPreferences.getProperties().isInherited("inherited inherited prop"));
            
            // set property with same name on inherited inherited
            inheritedInheritedPreferences.getProperties().setProperty("prop", "ABC", false);
            assertEquals("123", userPreferences.getProperties().getProperty("prop", false));
            assertFalse(userPreferences.getProperties().isInherited("prop"));
            
            userPreferences.getProperties().deleteProperty("prop");
            assertEquals("ABC", userPreferences.getProperties().getProperty("prop", false));
            assertTrue(userPreferences.getProperties().isInherited("prop"));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
        
    @Test
    public void testAddInheritedCertificates()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");
    
            File file = new File("test/resources/testdata/certificates/equifax.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);        
    
            userPreferences.getCertificates().add(certificate);
                    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");
    
            file = new File("test/resources/testdata/certificates/rim.cer");
            
            X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);        
    
            inheritedPreferences.getCertificates().add(inheritedCertificate);
            
            UserPreferences inheritedInheritedPreferences = userPreferencesManager.addUserPreferences("inherited inherited prefs");
    
            file = new File("test/resources/testdata/certificates/intel-crl-test.cer");
            
            X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);        
    
            inheritedInheritedPreferences.getCertificates().add(inheritedInheritedCertificate);
            
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);
            
            assertEquals(1, userPreferences.getInheritedUserPreferences().size());
    
            assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());
            
            assertTrue(userPreferences.getCertificates().contains(certificate));
            assertEquals(1, userPreferences.getCertificates().size());
    
            assertTrue(inheritedPreferences.getCertificates().contains(inheritedCertificate));
            assertEquals(1, inheritedPreferences.getCertificates().size());
    
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedCertificate));
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedInheritedCertificate));
            assertEquals(2, userPreferences.getInheritedCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }            
    }
    
    /*
     * almost equal to testAddInheritedCertificates but now inherit at a different level
     */
    @Test
    public void testAddInheritedCertificates2()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");
    
            File file = new File("test/resources/testdata/certificates/equifax.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);        
    
            userPreferences.getCertificates().add(certificate);
                    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");
    
            file = new File("test/resources/testdata/certificates/rim.cer");
            
            X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);        
    
            inheritedPreferences.getCertificates().add(inheritedCertificate);
            
            UserPreferences inheritedInheritedPreferences = userPreferencesManager.addUserPreferences("inherited inherited prefs");
    
            file = new File("test/resources/testdata/certificates/intel-crl-test.cer");
            
            X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);        
    
            inheritedInheritedPreferences.getCertificates().add(inheritedInheritedCertificate);
            
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            userPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);
            
            assertEquals(2, userPreferences.getInheritedUserPreferences().size());
    
            assertEquals(0, inheritedPreferences.getInheritedUserPreferences().size());
            
            assertTrue(userPreferences.getCertificates().contains(certificate));
            assertEquals(1, userPreferences.getCertificates().size());
    
            assertTrue(inheritedPreferences.getCertificates().contains(inheritedCertificate));
            assertEquals(1, inheritedPreferences.getCertificates().size());
    
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedCertificate));
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedInheritedCertificate));
            assertEquals(2, userPreferences.getInheritedCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testSetProperty()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        try {
            String preferencesName = "Test";
            
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);
    
            userPreferences.getProperties().setProperty("testProperty", "123", false);
            
            assertEquals("123", userPreferences.getProperties().getProperty("testProperty", false));
            
            tx.commit();
    
            sessionSource.getSession().close();
            
            tx = sessionSource.getSession().beginTransaction();
    
            assertEquals("123", userPreferences.getProperties().getProperty("testProperty", false));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }    
    
    @Test
    public void testAddUserPreferences()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String preferencesName = "Test";
            
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);
            
            assertNull(userPreferences.getKeyAndCertificate());
            assertNotNull(userPreferences.getProperties());
            assertEquals(userPreferences.toString(), userPreferences.getProperties().getCategory());
            assertEquals(0, userPreferences.getProperties().getProperyNames(false).size());
            assertEquals(0, userPreferences.getProperties().getProperyNames(true).size());
            assertEquals(0, userPreferences.getCertificates().size());
            assertEquals(0, userPreferences.getInheritedCertificates().size());
            assertEquals(0, userPreferences.getInheritedKeyAndCertificates().size());
            
            assertNotNull(userPreferencesManager.getUserPreferences(preferencesName));
    
            // because the name iterator uses a stateless session we need to commit the transaction otherwise
            // the changes are not noticed by the iterator. This is kind of a pain but we need to use a 
            // stateless session to make things scalable
            tx.commit();
    
            tx = sessionSource.getSession().beginTransaction();
            
            CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();
            
            int count = 0;
            
            while (nameIterator.hasNext()) 
            {
                String name = nameIterator.next();
                
                assert(name != null);
                
                count++;
            }
            
            assertEquals(1, count);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }

    @Test
    public void testAddMultipleUserPreferences()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            userPreferencesManager.addUserPreferences("test1");
            
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));
    
            userPreferencesManager.addUserPreferences("test2");
            
            assertNotNull(userPreferencesManager.getUserPreferences("test2"));
    
            // because the name iterator uses a stateless session we need to commit the transaction otherwise
            // the changes are not noticed by the iterator. This is kind of a pain but we need to use a 
            // stateless session to make things scalable
            tx.commit();
    
            tx = sessionSource.getSession().beginTransaction();
            
            CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();
            
            int count = 0;
            
            while (nameIterator.hasNext()) 
            {
                String name = nameIterator.next();
                
                assert(name != null);
                
                count++;
            }
            
            assertEquals(2, count);
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }
    
    @Test
    public void testAddCertificates()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String preferencesName = "Test";
            
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);
            
            Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);
            
            for (X509Certificate certificate : certificates)
            {
                userPreferences.getCertificates().add(certificate);
            }
            
            assertEquals(certificates.size(), userPreferences.getCertificates().size());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }

    @Test
    public void testAddNewCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String preferencesName = "Test";

            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);
            
            File file = new File("test/resources/testdata/certificates/rim.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);        
            
            assertFalse(keyAndCertStore.contains(certificate));
            
            userPreferences.getCertificates().add(certificate);
            
            assertEquals(1, userPreferences.getCertificates().size());
    
            assertTrue(keyAndCertStore.contains(certificate));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
        finally {
        }
    }

    @Test
    public void testAddNamedBlobs()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String preferencesName = "Test";
            
            String category = "category";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

            Set<NamedBlob> blobs = userPreferences.getNamedBlobs(); 

            assertEquals(0, blobs.size()); 
            
            NamedBlob blob1 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);

            blobs.add(blob1);
            
            assertEquals(1, blobs.size()); 

            NamedBlob blob2 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);

            blobs.add(blob2);
            
            tx.commit();
            
            sessionSource.getSession().close();
            
            tx = sessionSource.getSession().beginTransaction();

            userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

            blobs = userPreferences.getNamedBlobs(); 

            assertEquals(2, blobs.size()); 
            
            assertTrue(blobs.contains(blob1));
            assertTrue(blobs.contains(blob2));
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }
    
    @Test
    public void testAddInheritedNamedBlobs()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String category = "category";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main");
    
            NamedBlob blob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);

            userPreferences.getNamedBlobs().add(blob);
                    
            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited");

            NamedBlob inheritedBlob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);
            
            inheritedPreferences.getNamedBlobs().add(inheritedBlob);
                
            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
    
            tx.commit();
            
            sessionSource.getSession().close();
            
            tx = sessionSource.getSession().beginTransaction();

            userPreferences = userPreferencesManager.getUserPreferences("main");

            Set<NamedBlob> blobs = userPreferences.getNamedBlobs();
            
            assertEquals(1, blobs.size());

            assertEquals(blob, blobs.iterator().next());
            
            assertEquals(1, userPreferences.getInheritedUserPreferences().size());
            
            Set<NamedBlob> inheritedBlobs = userPreferences.getInheritedUserPreferences().iterator().next().getNamedBlobs();

            assertEquals(1, inheritedBlobs.size());
            
            assertEquals(inheritedBlob, inheritedBlobs.iterator().next());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }
    
    @Test
    public void testAddkeyAndCertificate()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            String preferencesName = "Test";
            
            String category = "category-1";
            
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(category);
            
            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);
    
            String thumbprint = "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69" + 
                        "FB45FF52A34049AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B";
            
            X509Certificate certificate = keyAndCertStore.getByThumbprint(thumbprint).getCertificate();
            
            X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(certificate);
            
            KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);
            
            assertNotNull(keyAndCertificate);
            
            userPreferences.setKeyAndCertificate(keyAndCertificate);
            
            tx.commit();
            
            sessionSource.getSession().close();
            
            tx = sessionSource.getSession().beginTransaction();
    
            userPreferences = userPreferencesManager.getUserPreferences(preferencesName);
    
            assertNotNull(userPreferences);
            assertNotNull(userPreferences.getKeyAndCertificate());
            assertEquals(certificate, userPreferences.getKeyAndCertificate().getCertificate());
            assertNotNull(userPreferences.getKeyAndCertificate().getPrivateKey());

            userPreferences.setKeyAndCertificate(null);
            
            tx.commit();
            
            sessionSource.getSession().close();
            
            tx = sessionSource.getSession().beginTransaction();
    
            userPreferences = userPreferencesManager.getUserPreferences(preferencesName);
    
            assertNotNull(userPreferences);
            assertNull(userPreferences.getKeyAndCertificate());
            
            tx.commit();
        }
        catch(Exception e) {
            tx.rollback();
            
            throw e;
        }
    }
}
