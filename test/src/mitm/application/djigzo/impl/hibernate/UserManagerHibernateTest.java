/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.impl.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserManager;
import mitm.application.djigzo.impl.hibernate.UserManagerHibernate;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import mitm.common.security.provider.MITMProvider;
import mitm.common.security.smime.selector.EncryptionCertificateSelector;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorUtils;
import mitm.test.TestUtils;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManagerHibernateTest
{
    private static final Logger logger = LoggerFactory.getLogger(UserManagerHibernateTest.class);
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    
    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager; 
    
    private static PKISecurityServices pKISecurityServices;

    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    
    private static KeyStore keyStore;
    
    private static UserManager userManager;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();

        InitializeBouncycastle.initialize();
                
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtHibernate("certificates", sessionManager);
        rootStore = new X509CertStoreExtHibernate("roots", sessionManager);
        crlStore = new X509CRLStoreExtHibernate("crls", sessionManager);
        
        keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        
        keyStore.load(null);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, "");
        
        pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);
        
        userManager = new UserManagerHibernate(pKISecurityServices, new EncryptionCertificateSelector(pKISecurityServices),
        		sessionManager);
        
        importKeyStore(pKISecurityServices.getKeyAndCertStore(),
                new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), pKISecurityServices.getRootStore());
        
    }
    
    private static void importKeyStore(KeyAndCertStore keyAndCertStore, File pfxfile, String password) 
    throws Exception 
    {        
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(pfxfile), password.toCharArray());
        
        Enumeration<String> aliases = keyStore.aliases();
        
        Session session = sessionSource.getSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            while (aliases.hasMoreElements()) 
            {
                String alias = aliases.nextElement();
    
                Certificate certificate = keyStore.getCertificate(alias);
                
                if (!(certificate instanceof X509Certificate)) {
                    // only X509Certificates are supported
                    continue;
                }
    
                try {
                    Key key = keyStore.getKey(alias, null);
    
                    if (!(key instanceof PrivateKey)) {
                        key = null;
                    }
                    
                    keyAndCertStore.addKeyAndCertificate(new KeyAndCertificateImpl((PrivateKey) key, 
                            (X509Certificate) certificate));
                }
                catch (UnrecoverableKeyException e) {
                    logger.error("Could not get the key.",e);
                }
                catch (CertStoreException e) {
                    logger.error("CertStoreException.",e);
                }
            }
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
    }
    
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws Exception 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws Exception
    {
        Session session = sessionSource.getSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate)
                {
                    if (!certStore.contains((X509Certificate) certificate))
                    {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
    }
    
    @Before
    public void setup()
    throws Exception
    {
        Session session = sessionSource.getSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            CloseableIterator<String> emailIterator = userManager.getEmailIterator();
            
            try {
                while (emailIterator.hasNext())
                {
                    String email = emailIterator.next();
                    
                    User user = userManager.getUser(email);
                    
                    userManager.deleteUser(user);
                }
            }
            finally {
                emailIterator.close();
            }
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
    }
    
    @Test
    public void testAddUserCaseInsensitive()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("tESt@exAMPle.com", UserManager.AddMode.PERSISTENT);
            
            assertTrue(userManager.isPersistent(user));
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("Test@examplE.com");
            
            assertNotNull(user);
            
            tx.commit();                        
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }

    @Test
    public void testAutoSelectEncryptionCertsOff()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.PERSISTENT);
            
            assertTrue(userManager.isPersistent(user));
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();

            assertEquals(0, certificates.size());

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("Test@examplE.com");
            
            assertNotNull(user);
            
            tx.commit();                        
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test(expected=AddressException.class)
    public void testAddInvalidEmail()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            userManager.addUser("xxx", UserManager.AddMode.PERSISTENT);
            
            tx.commit();                        
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test
    public void testAddUser()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.PERSISTENT);
            
            assertTrue(userManager.isPersistent(user));
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("test@example.com");
            
            assertNotNull(user);
            
            tx.commit();                        
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test
    public void testAddUserNonPersistent()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

            assertFalse(userManager.isPersistent(user));
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("test@example.com");
            
            assertNull(user);
            
            tx.commit();            
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }

    @Test
    public void testAddUserNonPersistentMakePersistent()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

            assertFalse(userManager.isPersistent(user));
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            userManager.makePersistent(user);
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("test@example.com");
            
            assertNotNull(user);
            
            tx.commit();            
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }    

    @Test
    public void testMakePersistentTwice()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

            assertFalse(userManager.isPersistent(user));
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);

            user.getUserPreferences().getProperties().setAutoSelectEncryptionCerts(true);
            
            Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();
            
            assertEquals(13, certificates.size());
            
            userManager.makePersistent(user);
            
            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            user = userManager.getUser("test@example.com");
            
            assertNotNull(user);

            /*
             * Should not fail
             */
            userManager.makePersistent(user);
            
            tx.commit();            
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }    
        
    @Test
    public void testAddProperty()
    throws Exception
    {
        Session session = sessionSource.getSession();
        
        Transaction tx = session.beginTransaction();

        try {
            User user = userManager.addUser("test@example.com", UserManager.AddMode.PERSISTENT);
            
            user.getUserPreferences().getProperties().setProperty("test", "123", false);
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
        
        /* create a new session to make sure the data does not come from the session cache */
        
        session = sessionSource.newSession();
        
        tx = null;
        
        try {
            /* need to inject session */
        	sessionManager.setSession(session);
            
            tx = session.beginTransaction();

            User user = userManager.getUser("test@example.com");
            
            assertNotNull(user);
            
            assertEquals("123", user.getUserPreferences().getProperties().getProperty("test", false));
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
        finally {
        	sessionManager.setSession(null);

            sessionSource.closeSession(session);
        }
    }
    
    @Test
    public void testGetEmailIterator()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);            
            userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);

            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            CloseableIterator<String> it = userManager.getEmailIterator();
            
            List<String> emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(4, emails.size());
            
            assertEquals("test1@example.com", emails.get(0));
            assertEquals("test1@xxx.com", emails.get(1));
            assertEquals("test2@example.com", emails.get(2));
            assertEquals("test2@xxx.com", emails.get(3));

            it = userManager.getEmailIterator(0, 2, SortDirection.ASC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(2, emails.size());
            
            assertEquals("test1@example.com", emails.get(0));
            assertEquals("test1@xxx.com", emails.get(1));

            it = userManager.getEmailIterator(0, 2, SortDirection.DESC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(2, emails.size());
            
            assertEquals("test2@xxx.com", emails.get(0));
            assertEquals("test2@example.com", emails.get(1));
            
            
            tx.commit();            
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test
    public void testSearchEmail()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);            
            userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);

            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            CloseableIterator<String> it = userManager.searchEmail("%%", 0, 100, SortDirection.ASC);
            
            List<String> emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(4, emails.size());
            
            assertEquals("test1@example.com", emails.get(0));
            assertEquals("test1@xxx.com", emails.get(1));
            assertEquals("test2@example.com", emails.get(2));
            assertEquals("test2@xxx.com", emails.get(3));

            it = userManager.searchEmail("%%", 0, 100, SortDirection.DESC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(4, emails.size());
            
            assertEquals("test2@xxx.com", emails.get(0));
            assertEquals("test2@example.com", emails.get(1));
            assertEquals("test1@xxx.com", emails.get(2));
            assertEquals("test1@example.com", emails.get(3));

            it = userManager.searchEmail("%%", 1, 2, SortDirection.DESC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(2, emails.size());
            
            assertEquals("test2@example.com", emails.get(0));
            assertEquals("test1@xxx.com", emails.get(1));

            it = userManager.searchEmail("%xxx%", 0, 100, SortDirection.ASC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(2, emails.size());
            
            assertEquals("test1@xxx.com", emails.get(0));
            assertEquals("test2@xxx.com", emails.get(1));

            it = userManager.searchEmail("non-existing", 0, 100, SortDirection.ASC);
            
            emails = CloseableIteratorUtils.toList(it);
            
            assertEquals(0, emails.size());
            
            tx.commit();            
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test
    public void testGetSearchEmailCount()
    throws Exception
    {
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);            
            userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
            userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);

            tx.commit();
            
            tx = sessionSource.getSession().beginTransaction();

            int count = userManager.getSearchEmailCount("%%");
            
            assertEquals(4, count);

            count = userManager.getSearchEmailCount("%xxx%");
            
            assertEquals(2, count);

            count = userManager.getSearchEmailCount("non-existing");
            
            assertEquals(0, count);
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
}
