package mitm.application.djigzo.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.AutoTransactDelegator;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.test.TestUtils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class DefaultEncryptionRecipientSelectorTest
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static ExtendedAutoTransactDelegator autoTransactDelegator;
    
    public static class ExtendedAutoTransactDelegator extends AutoTransactDelegator 
    {
        @StartTransaction
        public void addUser(String email, Collection<X509Certificate> certificates) 
        throws AddressException, HierarchicalPropertiesException
        {
            UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

            User user = userWorkflow.getUser(email, GetUserMode.CREATE_IF_NOT_EXIST);
            
            UserPreferences preferences = user.getUserPreferences(); 

            preferences.getCertificates().addAll(certificates);
            
            userWorkflow.makePersistent(user);            
        }
        
        @StartTransaction
        public Set<User> select(DefaultEncryptionRecipientSelector selector, Collection<X509Certificate> certificates, String... emails)
        throws AddressException, HierarchicalPropertiesException
        {
            Set<User> users = new HashSet<User>();
            
            for (String email : emails)
            {
                User user = getUser(email);
                
                if (user == null) {
                    user = addUser(email);
                }
                
                users.add(user);
            }
            
            return selector.select(users, certificates);
        }
    }    
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.recreateTables();
        
        DjigzoTestUtils.initialize();
        
        autoTransactDelegator = AutoTransactDelegator.createProxy(ExtendedAutoTransactDelegator.class);
    }

    @Before
    public void setup()
    throws Exception
    {
        clearAll();
    }
    
    private void clearAll() 
    throws Exception
    {        
        autoTransactDelegator.deleteAllUsers();
        autoTransactDelegator.cleanKeyAndCertStore();
        autoTransactDelegator.cleanKeyAndRootStore();
        autoTransactDelegator.cleanCRLStore();
        autoTransactDelegator.cleanCTL();
    }
    
    @Test
    public void testSelectOneUserAutoSelectWhiteListedExpired()
    throws Exception
    {
        X509Certificate expiredCert = TestUtils.loadCertificate(new File(testBase, "certificates/chinesechars.cer"));
        
        autoTransactDelegator.addRootCertificate(expiredCert);

        Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                true /* check validity */);

        Set<User> users = autoTransactDelegator.select(selector, certificates, "liu_shuhong@itrus.com.cn");
        
        assertEquals(0, users.size());
        assertEquals(0, certificates.size());

        certificates.clear();
        
        /*
         * now whitelist the certificate
         */
        autoTransactDelegator.addToCTL(expiredCert, CTLEntryStatus.WHITELISTED, true);
        
        selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                true /* check validity */);

        users = autoTransactDelegator.select(selector, certificates, "liu_shuhong@itrus.com.cn");
        
        /*
         * Although the certificate was whitelisted it is not automatically selected when expired
         */
        assertEquals(0, users.size());
        assertEquals(0, certificates.size());
        
        autoTransactDelegator.addUser("liu_shuhong@itrus.com.cn", Collections.singleton(expiredCert));
        
        selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                true /* check validity */);

        users = autoTransactDelegator.select(selector, certificates, "liu_shuhong@itrus.com.cn");
        
        /*
         * The certificate is now explicitly added to the user so it's now used because it's whitelisted
         */
        assertEquals(1, users.size());
        assertEquals(1, certificates.size());
        assertEquals(expiredCert, certificates.iterator().next());
    }
    
    @Test
    public void testSelectOneUserAutoSelect()
    throws Exception
    {
        autoTransactDelegator.addCertificates(CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/testCertificates.p7b")));
        autoTransactDelegator.addCertificate(TestUtils.loadCertificate(
                new File(testBase, "certificates/mitm-test-ca.cer")));

        autoTransactDelegator.addRootCertificate(TestUtils.loadCertificate(
                new File(testBase, "certificates/mitm-test-root.cer")));

        Set<X509Certificate> certificates = new HashSet<X509Certificate>();
        
        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                true /* check validity */);

        Set<User> users = autoTransactDelegator.select(selector, certificates, "test@example.com");
        
        assertEquals(1, users.size());
        assertEquals(13, certificates.size());

        certificates.clear();
        
        selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                false /* do not check validity */);

        users = autoTransactDelegator.select(selector, certificates, "test@example.com");
        
        assertEquals(1, users.size());
        assertEquals(13, certificates.size());
    }

    @Test
    public void testSelectOneUserManualSelect()
    throws Exception
    {
        Collection<X509Certificate> manualCertificates = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/testCertificates.p7b"));

        manualCertificates.add(TestUtils.loadCertificate(
                new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer")));
        
        assertEquals(21, manualCertificates.size());
        
        autoTransactDelegator.addCertificate(TestUtils.loadCertificate(
                new File(testBase, "certificates/mitm-test-ca.cer")));

        autoTransactDelegator.addRootCertificate(TestUtils.loadCertificate(
                new File(testBase, "certificates/mitm-test-root.cer")));
        
        autoTransactDelegator.addCertificates(manualCertificates);

        autoTransactDelegator.addUser("someoneelse@example.com", manualCertificates);
        
        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                true /* check validity */);

        Set<X509Certificate> certificates = new HashSet<X509Certificate>();

        Set<User> users = autoTransactDelegator.select(selector, certificates, "someoneelse@example.com");

        assertEquals(1, users.size());
        assertEquals(15, certificates.size());
    }
    
    @Test
    public void testSelectOneUserNoCheck()
    throws Exception
    {
        Collection<X509Certificate> manualCertificates = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/testCertificates.p7b"));

        manualCertificates.add(TestUtils.loadCertificate(
                new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer")));
        
        assertEquals(21, manualCertificates.size());
                
        autoTransactDelegator.addCertificates(manualCertificates);

        autoTransactDelegator.addUser("someoneelse@example.com", manualCertificates);
        
        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(SystemServices.getPKISecurityServices(), 
                false /* no check validity */);

        Set<X509Certificate> certificates = new HashSet<X509Certificate>();

        Set<User> users = autoTransactDelegator.select(selector, certificates, "someoneelse@example.com");

        assertEquals(1, users.size());
        assertEquals(21, certificates.size());
    }    
}
