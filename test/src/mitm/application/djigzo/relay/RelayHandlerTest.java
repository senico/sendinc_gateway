/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.relay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.MailUtils;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class RelayHandlerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");
    
    private static HibernateSessionSource sessionSource;
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    private static PKISecurityServices pkiSecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        rootStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create();
        crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "crls").create();
    }

    @Before
    public void setup()
    throws Exception
    {
        certStore.removeAllEntries();
        rootStore.removeAllEntries();
        crlStore.removeAllEntries();
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        pkiSecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
                rootStore, crlStore);
    }
    
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException, CertStoreException 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws CertStoreException
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }        
    }
    
    private static void addTestCertificates() 
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);        
    }
    
    private static RelayUserInfoProvider createDefaultRelayUserInfoProvider()
    {
        RelayUserInfoProvider infoProvider = new RelayUserInfoProvider()
        {
            @Override
            public RelayUserInfoImpl getRelayUserInfo(MimeMessage message)
            throws MessagingException
            {
                try {
                    return new RelayUserInfoImpl("test@example.com", certStore.getCertificates(null), true, 
                            0, RelayBounceMode.NEVER);
                }
                catch (CertStoreException e) {
                    throw new MessagingException("Error", e);
                }
            }            
        };

        return infoProvider;
    }

    @Test
    public void testRelayCertificateExpired()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayCertificateExpired.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.NO_VALID_CERTIFICATES, e.getMessage());
        }
    }
    
    @Test
    public void testRelayCertificateNotAllowedForSigning()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayCertificateNotForSigning.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.NO_VALID_CERTIFICATES, e.getMessage());
        }
    }
    
    @Test
    public void testRelayCertificateNotTrusted()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);        
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.NO_VALID_CERTIFICATES, e.getMessage());
        }
    }
    
    @Test
    public void testNotExpired()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayUserInfoProvider infoProvider = new RelayUserInfoProvider()
        {
            @Override
            public RelayUserInfoImpl getRelayUserInfo(MimeMessage message)
            throws MessagingException
            {
                try {
                    return new RelayUserInfoImpl("test@example.com", certStore.getCertificates(null), true, 
                            Long.MAX_VALUE, RelayBounceMode.NEVER);
                }
                catch (CertStoreException e) {
                    throw new MessagingException("Error", e);
                }
            }            
        };
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, infoProvider);
        
        MimeMessage relayMessage = handler.handleRelayMessage(message);
        
        File file = new File(tempDir, "testRelayMessage.eml");
        
        MailUtils.writeMessage(relayMessage, file);
        
        assertEquals("martijn_brinkers@mobileemail.vodafone.nl", relayMessage.getFrom()[0].toString());
        assertTrue(relayMessage.isMimeType("text/plain"));
        
        String body = (String) relayMessage.getContent();
        
        assertEquals("Test", body.trim());
        
        assertEquals(1, handler.getRecipients().size());
        assertEquals("test@example.com", handler.getRecipients().iterator().next());
    }
    
    @Test
    public void testExpired()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayUserInfoProvider infoProvider = new RelayUserInfoProvider()
        {
            @Override
            public RelayUserInfoImpl getRelayUserInfo(MimeMessage message)
            throws MessagingException
            {
                try {
                    return new RelayUserInfoImpl("test@example.com", certStore.getCertificates(null), true, 1, 
                            RelayBounceMode.NEVER);
                }
                catch (CertStoreException e) {
                    throw new MessagingException("Error", e);
                }
            }            
        };
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, infoProvider);
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.MESSAGE_EXPIRED, e.getRelayStep());
            assertEquals(RelayHandler.MESSAGE_EXPIRED, e.getMessage());
        }
    }

    @Test
    public void testInvalidRecipient()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayInvalidRecipient.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e)
        {
            assertEquals(RelayStep.INVALID_RECIPIENT, e.getRelayStep());
            assertEquals("test is not a valid email address.", e.getMessage());
        }
    }
    
    @Test
    public void testInvalidXML()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayInvalidXML.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e)
        {
            assertEquals(RelayStep.INVALID, e.getRelayStep());
            assertEquals(RelayHandler.META_XML_IS_INVALID, e.getMessage());
        }
    }
    
    @Test
    public void testNoRelayCertificates()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e)
        {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.RELAY_CERTS_MISSING, e.getMessage());
        }
    }
    
    @Test
    public void testRelayNotAllowed()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayUserInfoProvider infoProvider = new RelayUserInfoProvider()
        {
            @Override
            public RelayUserInfoImpl getRelayUserInfo(MimeMessage message)
            throws MessagingException
            {
                try {
                    return new RelayUserInfoImpl("test@example.com", certStore.getCertificates(null), false, 0, 
                            RelayBounceMode.NEVER);
                }
                catch (CertStoreException e) {
                    throw new MessagingException("Error", e);
                }
            }            
        };
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, infoProvider);
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.RELAY_NOT_ALLOWED, e.getMessage());
        }
    }
    
    @Test
    public void testNoRelay()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayNoRelay.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.INVALID, e.getRelayStep());
            assertEquals(RelayHandler.RELAY_MESSAGE_NOT_FOUND, e.getMessage());
        }
    }        
    
    @Test
    public void testNoMeta()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-evolution.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.INVALID, e.getRelayStep());
            assertEquals(RelayHandler.META_PART_NOT_FOUND, e.getMessage());
        }
    }        
    
    @Test
    public void testSMIMEButNotSigned()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/compressed.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.INVALID, e.getRelayStep());
            assertEquals(RelayHandler.MESSAGE_IS_NOT_SIGNED, e.getMessage());
        }
    }        
    
    @Test
    public void testInvalidSMIME()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-corrupt.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals("S/MIME message cannot be handled.", e.getMessage());
        }
    }        
    
    @Test
    public void testNoSMIME()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/simple-text-message.eml"));
        
        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.INVALID, e.getRelayStep());
            assertEquals(RelayHandler.MESSAGE_IS_NOT_SMIME, e.getMessage());
        }
    }        
        
    @Test
    public void testRelayMessage()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
                
        MimeMessage relayMessage = handler.handleRelayMessage(message);
        
        RelayUserInfo relayUserInfo = handler.getUserInfo();
        
        assertNotNull(relayUserInfo);
        assertEquals("test@example.com", relayUserInfo.getEmail());
        
        File file = new File(tempDir, "testRelayMessage.eml");
        
        MailUtils.writeMessage(relayMessage, file);
        
        assertEquals("martijn_brinkers@mobileemail.vodafone.nl", relayMessage.getFrom()[0].toString());
        assertTrue(relayMessage.isMimeType("text/plain"));
        
        String body = (String) relayMessage.getContent();
        
        assertEquals("Test", body.trim());
        
        assertEquals(1, handler.getRecipients().size());
        assertEquals("test@example.com", handler.getRecipients().iterator().next());
    }
    
    @Test
    public void testRelayMessageNoValidRelayCertificate()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelay.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.RELAYING_NOT_ALLOWED, e.getRelayStep());
            assertEquals(RelayHandler.NO_VALID_CERTIFICATES, e.getMessage());
        }
    }
    
    @Test
    public void testRelayMessageTampered()
    throws Exception
    {
        addTestCertificates();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/blackBerryRelayTampered.eml"));
        
        message = BodyPartUtils.searchForRFC822(message);

        RelayHandler handler = new RelayHandler(pkiSecurityServices, createDefaultRelayUserInfoProvider());
        
        try {
            handler.handleRelayMessage(message);
            
            fail();
        }
        catch(RelayException e) {
            assertEquals(RelayStep.INVALID_SIGNATURE, e.getRelayStep());
            assertEquals(RelayHandler.INVALID_SIGNATURE, e.getMessage());
        }
    }        
}
