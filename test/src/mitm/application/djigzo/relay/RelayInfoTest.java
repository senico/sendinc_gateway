/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.relay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileInputStream;

import mitm.application.djigzo.relay.RelayInfo;
import mitm.application.djigzo.relay.RelayInfoFactory;

import org.junit.Test;


public class RelayInfoTest
{
    @Test
    public void testRelayInfo()
    throws Exception
    {
        File xmlFile = new File("test/resources/testdata/other/blackBerryRelayTest.xml");
        
        FileInputStream fis = new FileInputStream(xmlFile);
        
        RelayInfo blackBerryRelay = RelayInfoFactory.unmarshall(fis);
        
        assertEquals("1", blackBerryRelay.getVersion());        
        assertEquals((Object) 1252004807545L, blackBerryRelay.getTimestamp());        
        assertEquals(2, blackBerryRelay.recipients.size());
        assertEquals("264330215", blackBerryRelay.getNonce());
        assertEquals("test@example.com", blackBerryRelay.recipients.get(0).getEmail());        
        assertEquals("test2@example.com", blackBerryRelay.recipients.get(1).getEmail());        
        assertNull(blackBerryRelay.getFrom());        
    }

    @Test
    public void testRelayInfoWithFrom()
    throws Exception
    {
        File xmlFile = new File("test/resources/testdata/other/blackBerryRelayTestWithFrom.xml");
        
        FileInputStream fis = new FileInputStream(xmlFile);
        
        RelayInfo blackBerryRelay = RelayInfoFactory.unmarshall(fis);
        
        assertEquals("1", blackBerryRelay.getVersion());        
        assertEquals((Object) 1267096735368L, blackBerryRelay.getTimestamp());        
        assertEquals(3, blackBerryRelay.recipients.size());
        assertEquals("1715829656553648138", blackBerryRelay.getNonce());
        assertEquals("ALBERT.MCDONALD@seat.com", blackBerryRelay.recipients.get(0).getEmail());        
        assertEquals("CAROLYN.ROSS@virgin.com", blackBerryRelay.recipients.get(1).getEmail());        
        assertEquals("FRANCES.HOFFMAN@aol.com", blackBerryRelay.recipients.get(2).getEmail());        
        assertEquals("martijn@djigzo.com", blackBerryRelay.getFrom());        
    }
}
