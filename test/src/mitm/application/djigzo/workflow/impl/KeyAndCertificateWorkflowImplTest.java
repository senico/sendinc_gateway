/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.KeyAndCertificateWorkflow;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.X509CertStoreExt;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KeyAndCertificateWorkflowImplTest 
{
	private static KeyAndCertificateWorkflow keyAndCertificateWorkflow;
	
	private static X509CertStoreExt certStore;
	
    private static X509CertStoreExt rootStore;
    
	private static AutoTransactDelegator proxy;
	
	private static KeyStore keyStore;
	
	private static SessionManager sessionManager;
	
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
        
        keyAndCertificateWorkflow = SystemServices.getKeyAndCertificateWorkflow();
        
        certStore = SystemServices.getKeyAndCertStore();
        
        rootStore = SystemServices.getKeyAndRootCertStore();
        
        sessionManager = SystemServices.getSessionManager();
        
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = securityFactory.createKeyStore("PKCS12");

        File keyFile = new File("test/resources/testdata/keys/testCertificates.p12");
        
        keyStore.load(new FileInputStream(keyFile), "test".toCharArray());
    }
    
    public static class AutoTransactDelegator
    {
    	public AutoTransactDelegator() {
    		// required by AutoCommitProxyFactory
    	}
    	
    	@StartTransaction
    	public void deleteAllCerts() 
    	throws CertStoreException 
    	{
    	    rootStore.removeAllEntries();
            certStore.removeAllEntries();
    	}

        @StartTransaction
        public Collection<X509Certificate> getAllCerts() 
        throws CertStoreException
        {
            return certStore.getCertificates(null);
        }

        @StartTransaction
        public void addRootCertificates(String file) 
        throws CertStoreException, CertificateException, NoSuchProviderException, FileNotFoundException
        {
            Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(new File(file));
            
            for (Certificate certificate : certificates) {
                rootStore.addCertificate((X509Certificate) certificate);
            }
        }
    }
    
    @Before
    public void setup() 
    throws Exception
    {
    	proxy = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
    			AutoTransactDelegator.class, sessionManager).createProxy();
    	
    	proxy.deleteAllCerts();
    }
    
    @Test
    public void testImportKeyStore() 
    throws KeyStoreException
    {
    	int imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
    			KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);
    	
    	assertEquals(22, imported);
    }

    @Test
    public void testImportKeyStoreSkipMissingKey() 
    throws KeyStoreException
    {
    	int imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
    			KeyAndCertificateWorkflow.MissingKey.SKIP_CERTIFICATE);
    	
    	assertEquals(22, imported);
    }

    @Test
    public void testImportKeyStoreRepeat() 
    throws KeyStoreException
    {
        int imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
                KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);
        
        assertEquals(22, imported);

        imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
                KeyAndCertificateWorkflow.MissingKey.ADD_CERTIFICATE);
        
        /*
         * everything was already imported
         */
        assertEquals(0, imported);
    }

    /*
     * Test if a PFX can be retrieved when a chain cannot be build
     */
    @Test
    public void testGetPFXNoChain() 
    throws Exception
    {
        char[] password = "test".toCharArray();
        
        int imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
                KeyAndCertificateWorkflow.MissingKey.SKIP_CERTIFICATE);
        
        assertEquals(22, imported);
        
        Collection<X509Certificate> certificates = proxy.getAllCerts();

        assertEquals(22, certificates.size());
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        keyAndCertificateWorkflow.getPFX(certificates, password, bos);
    }
    
    @Test
    public void testGetPFX() 
    throws Exception
    {
        proxy.addRootCertificates("test/resources/testdata/certificates/mitm-test-root.cer");
        
    	char[] password = "test".toCharArray();
    	
    	int imported = keyAndCertificateWorkflow.importKeyStore(keyStore, 
    			KeyAndCertificateWorkflow.MissingKey.SKIP_CERTIFICATE);
    	
    	assertEquals(22, imported);
    	
    	Collection<X509Certificate> certificates = proxy.getAllCerts();

    	assertEquals(22, certificates.size());
    	
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	
    	keyAndCertificateWorkflow.getPFX(certificates, password, bos);
    	
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        keyStore.load(new ByteArrayInputStream(bos.toByteArray()), "test".toCharArray());
    	
        assertEquals(22, keyStore.size());

        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection(password);
        
        for (X509Certificate certificate : certificates)
        {
        	String alias = X509CertificateInspector.getThumbprint(certificate);
        	
        	if (keyStore.isKeyEntry(alias))
        	{
            	KeyStore.Entry entry = keyStore.getEntry(alias, passwd);
            	
            	assertTrue(entry instanceof PrivateKeyEntry);
            	
            	PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) entry;
            	
            	assertNotNull(privateKeyEntry.getPrivateKey());
            	assertNotNull(privateKeyEntry.getCertificate());
            	assertEquals(certificate, privateKeyEntry.getCertificate());
        	}
        }
        
        Enumeration<String> aliases = keyStore.aliases();
        
        int keyCount = 0;
        int certCount = 0;
        
        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();
            
            if (keyStore.isKeyEntry(alias))
            {
                keyCount++;
                
                PrivateKeyEntry entry = (PrivateKeyEntry) keyStore.getEntry(alias, passwd);
                
                Certificate[] chain = entry.getCertificateChain();
                
                assertEquals(3, chain.length);
            }
            else {
                certCount++;
            }
        }
        
        assertEquals(20, keyCount);
        assertEquals(2, certCount);
    }
}
