/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.CloseableIteratorException;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserWorkflowImplTest
{
    private static SessionManager sessionManager;
    
    private static HibernateSessionSource sessionSource;
    
    private static UserWorkflow userWorkflow;
    
    private static DatabaseActionExecutor actionExecutor;
        
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();

        sessionSource = SystemServices.getHibernateSessionSource();
        userWorkflow = SystemServices.getUserWorkflow();
        sessionManager = SystemServices.getSessionManager();
        
        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager); 
    }

    @Before
    public void setup()
    {
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
    }
    
    public boolean isExistingUserAction(Session session, String email) 
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
            
            return user != null;
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }    

    public void hasOnlyGlobalUserPreferencesAction(Session session, String email) 
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
            
            assertNotNull(user);
            
            assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());
            
            UserPreferences pref = user.getUserPreferences().getInheritedUserPreferences().iterator().next();
            
            assertEquals("global", pref.getCategory());
            assertEquals("preferences", pref.getName());
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }    
    
    
    @Test
    public void addUser()
    throws DatabaseException
    {
        Session previousSession = sessionManager.getSession();
        
        try {
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addUserAction(session);
                    }
                });
            
            assertTrue(actionExecutor.executeTransaction(
                new DatabaseAction<Boolean>()
                {
                    @Override
                    public Boolean doAction(Session session)
                    throws DatabaseException
                    {
                        return isExistingUserAction(session, "test@example.com");
                    }
                }));
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        hasOnlyGlobalUserPreferencesAction(session, "test@example.com");
                    }
                });            
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
    
    public void addUserAction(Session session)
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.addUser("test@example.com");
            
            assertFalse(userWorkflow.isPersistent(user));
            
            assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());
            
            userWorkflow.makePersistent(user);
    
            assertTrue(userWorkflow.isPersistent(user));
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }

    @Test
    public void addUserNotPersistent()
    throws DatabaseException
    {
        Session previousSession = sessionManager.getSession();
        
        try {
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addUserNotPersistentAction(session);
                    }
                });

            assertFalse(actionExecutor.executeTransaction(
                new DatabaseAction<Boolean>()
                {
                    @Override
                    public Boolean doAction(Session session)
                    throws DatabaseException
                    {
                        return isExistingUserAction(session, "test@example.com");
                    }
                }));            
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
    
    public void addUserNotPersistentAction(Session session) 
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.addUser("test@example.com");
            
            assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());
            
            UserPreferences pref = user.getUserPreferences().getInheritedUserPreferences().iterator().next();
            
            assertEquals("global", pref.getCategory());
            assertEquals("preferences", pref.getName());            
            
            assertFalse(userWorkflow.isPersistent(user));
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}
    }
    
    @Test
    public void addUserAndDomainPrefs()
    throws DatabaseException
    {
        Session previousSession = sessionManager.getSession();
        
        try {
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addDomainAction(session, "example.com");
                    }
                });            

            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addUserAction(session);
                    }
                });            

            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addDomainCheckInheritedPreferencesAction(session);
                    }
                });
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addDomainAction(session, "*.example.com");
                    }
                });            
            
            actionExecutor.executeTransaction(
                new DatabaseVoidAction()
                {
                    @Override
                    public void doAction(Session session)
                    throws DatabaseException
                    {
                        addWildcardDomainCheckInheritedPreferencesAction(session);
                    }
                });
        }
        finally {
        	sessionManager.setSession(previousSession);
        }
    }
    
    public void addDomainAction(Session session, String domain)
    throws DatabaseException
    {
    	sessionManager.setSession(session);
        
        DomainManager domainManager = SystemServices.getDomainManager();
        
        try {
        	UserPreferences domainPreferences = domainManager.addDomain(domain);
        
			domainPreferences.getProperties().setProperty("test", "456", false);
		} 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		} 
        catch (CloseableIteratorException e) {
            throw new DatabaseException(e);
        }
    }    
    
    public void addDomainCheckInheritedPreferencesAction(Session session)
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.getUser("test@example.com", UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
            
            assertNotNull(user);

            assertEquals(2, user.getUserPreferences().getInheritedUserPreferences().size());
            
            Iterator<UserPreferences> it = user.getUserPreferences().getInheritedUserPreferences().iterator();
            
            UserPreferences pref = it.next();
            
            assertEquals("global", pref.getCategory());
            assertEquals("preferences", pref.getName());            

            pref = it.next();
            
            assertEquals("domain", pref.getCategory());
            assertEquals("example.com", pref.getName());            
            
            assertEquals("456", user.getUserPreferences().getProperties().getProperty("test", false));
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}        
    }
    
    public void addWildcardDomainCheckInheritedPreferencesAction(Session session)
    throws DatabaseException
    {
    	sessionManager.setSession(session);

        try {
            User user = userWorkflow.getUser("test@example.com", UserWorkflow.GetUserMode.NULL_IF_NOT_EXIST);
            
            assertNotNull(user);

            assertEquals(3, user.getUserPreferences().getInheritedUserPreferences().size());
            
            Iterator<UserPreferences> it = user.getUserPreferences().getInheritedUserPreferences().iterator();
            
            UserPreferences pref = it.next();
            
            assertEquals("global", pref.getCategory());
            assertEquals("preferences", pref.getName());            

            pref = it.next();
            
            assertEquals("domain", pref.getCategory());
            assertEquals("*.example.com", pref.getName());            

            pref = it.next();
            
            assertEquals("domain", pref.getCategory());
            assertEquals("example.com", pref.getName());            
            
            assertEquals("456", user.getUserPreferences().getProperties().getProperty("test", false));
        }
        catch(AddressException e) {
            throw new DatabaseException(e);
        } 
        catch (HierarchicalPropertiesException e) {
            throw new DatabaseException(e);
		}        
    }        
}
