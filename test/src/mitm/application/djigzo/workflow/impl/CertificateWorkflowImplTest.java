/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.User;
import mitm.application.djigzo.impl.NamedCertificateImpl;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.test.TestUtils;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CertificateWorkflowImplTest
{
    private final static File testBaseDir = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;
        
	private static SessionManager sessionManager;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();

        sessionSource = SystemServices.getHibernateSessionSource();
        
        sessionManager = SystemServices.getSessionManager();
    }

    @Before
    public void setup()
    {
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
    }

    public static class AutoTransactDelegator
    {
    	public AutoTransactDelegator() {
    		// required by AutoCommitProxyFactory
    	}
    	
    	@StartTransaction
    	public boolean isInUse(X509Certificate certificate) 
    	throws CertStoreException 
    	{
    		CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();
    		
    		return workFlow.isInUse(certificate);
    	}
    	
    	@StartTransaction
    	public void addUserWithCertificate(String email, X509Certificate certificate) 
    	throws AddressException, HierarchicalPropertiesException 
    	{
    		User user = SystemServices.getUserWorkflow().addUser(email);
    		
    		SystemServices.getUserWorkflow().makePersistent(user);
    		
    		user.getUserPreferences().getCertificates().add(certificate);
    	}

        @StartTransaction
        public void addUserWithNamedCertificate(String email, X509Certificate certificate) 
        throws AddressException, HierarchicalPropertiesException 
        {
            User user = SystemServices.getUserWorkflow().addUser(email);
            
            SystemServices.getUserWorkflow().makePersistent(user);
            
            NamedCertificate namedCertificate = new NamedCertificateImpl("name", certificate);
            
            user.getUserPreferences().getNamedCertificates().add(namedCertificate);
        }
    	
    	@StartTransaction
    	public void addUserWithSigningCertificate(String email, KeyAndCertificate keyAndCertificate) 
    	throws AddressException, HierarchicalPropertiesException, CertStoreException, KeyStoreException 
    	{
    		User user = SystemServices.getUserWorkflow().addUser(email);
    		
    		SystemServices.getUserWorkflow().makePersistent(user);
    		
    		user.getUserPreferences().setKeyAndCertificate(keyAndCertificate);
    	}
    }

    @Test
    public void testInUseNamedCertificate()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                AutoTransactDelegator.class, sessionManager).createProxy();
        
        assertFalse(autoTransactDelegator.isInUse(certificate));

        assertFalse(autoTransactDelegator.isInUse(certificate));
        
        autoTransactDelegator.addUserWithNamedCertificate("test@example.com", certificate);
        
        assertTrue(autoTransactDelegator.isInUse(certificate));
    }
    
    @Test
    public void testInUseKeyAndCertificate()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
    			AutoTransactDelegator.class, sessionManager).createProxy();
        
        assertFalse(autoTransactDelegator.isInUse(certificate));
        
        autoTransactDelegator.addUserWithSigningCertificate("test@example.com", 
        		new KeyAndCertificateImpl(null, certificate));
        
        assertTrue(autoTransactDelegator.isInUse(certificate));
    }
    
    @Test
    public void testInUse()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
    			AutoTransactDelegator.class, sessionManager).createProxy();
        
        assertFalse(autoTransactDelegator.isInUse(certificate));
        
        autoTransactDelegator.addUserWithCertificate("test@example.com", certificate);
        
        assertTrue(autoTransactDelegator.isInUse(certificate));
    }

    @Test
    public void testNotInUse() 
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
    			AutoTransactDelegator.class, sessionManager).createProxy();
        
        assertFalse(autoTransactDelegator.isInUse(certificate));
    }
        
    @Test
    public void testAddCertificate() 
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
    }

    @Test
    public void testAddExistingCertificate() 
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        assertFalse(workFlow.addCertificateTransacted(certificate));
    }
    
    @Test
    public void testAddCertificateSessionSave() 
    throws Exception
    {
        Session session = sessionSource.newSession();
        
        SessionManager sessionManager = SystemServices.getSessionManager();

        sessionManager.setSession(session);
        
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        assertEquals(session, sessionManager.getSession());
    }    
}
