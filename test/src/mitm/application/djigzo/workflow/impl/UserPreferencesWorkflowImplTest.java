/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.workflow.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.NamedCertificate;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.impl.NamedCertificateImpl;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.CertificateWorkflow;
import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.NamedBlob;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.util.Check;
import mitm.test.TestUtils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class UserPreferencesWorkflowImplTest
{
    private final static File testBaseDir = new File("test/resources/testdata");
    
    private static AutoTransactDelegator proxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();

        proxy = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                AutoTransactDelegator.class, SystemServices.getSessionManager()).createProxy();
    }

    @Before
    public void setup()
    {
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
    }

    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        @StartTransaction
        public void deleteUser(String email) 
        throws Exception 
        {
            User user = SystemServices.getUserWorkflow().getUser(email, GetUserMode.NULL_IF_NOT_EXIST);
            
            if (user != null) {
                SystemServices.getUserWorkflow().deleteUser(user);
            }
        }
        
        @StartTransaction
        public void checkReferencedFromKeyAndCertificate(X509Certificate certificate, Integer firstResult,
                Integer maxResults, String... names)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromKeyAndCertificate(certificate, firstResult, maxResults);
            
            assertEquals(names.length, prefs.size());
            
            Set<String> foundNames = new HashSet<String>();  
            
            for (UserPreferences pref : prefs) {
                foundNames.add(pref.getName());
            }
            
            for (String name : names) {
                assertTrue(foundNames.contains(name));
            }
        }

        @StartTransaction
        public void checkNotReferencedFromKeyAndCertificate(X509Certificate certificate)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromKeyAndCertificate(certificate, null, null);
            
            assertEquals(0, prefs.size());
        }

        @StartTransaction
        public void checkReferencedFromCertificates(X509Certificate certificate, Integer firstResult,
                Integer maxResults, String... names)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromCertificates(certificate, firstResult, maxResults);
            
            assertEquals(names.length, prefs.size());
            
            Set<String> foundNames = new HashSet<String>();  
            
            for (UserPreferences pref : prefs) {
                foundNames.add(pref.getName());
            }
            
            for (String name : names) {
                assertTrue(foundNames.contains(name));
            }
        }

        @StartTransaction
        public void checkNotReferencedFromCertificates(X509Certificate certificate)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromCertificates(certificate, null, null);
            
            assertEquals(0, prefs.size());
        }

        @StartTransaction
        public void checkReferencedFromNamedCertificates(X509Certificate certificate, Integer firstResult,
                Integer maxResults, String... names)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromNamedCertificates(certificate, firstResult, maxResults);
            
            assertEquals(names.length, prefs.size());
            
            Set<String> foundNames = new HashSet<String>();  
            
            for (UserPreferences pref : prefs) {
                foundNames.add(pref.getName());
            }
            
            for (String name : names) {
                assertTrue(foundNames.contains(name));
            }
        }

        @StartTransaction
        public void checkNotReferencedFromNamedCertificates(X509Certificate certificate)
        throws CertStoreException 
        {
            UserPreferencesWorkflow workflow = SystemServices.getUserPreferencesWorkflow();
            
            List<UserPreferences> prefs = workflow.getReferencingFromNamedCertificates(certificate, null, null);
            
            assertEquals(0, prefs.size());
        }
        
        @StartTransaction
        public void addUsersWithCertificate(X509Certificate certificate, String... emails) 
        throws Exception 
        {
            for (String email : emails)
            {
                User user = SystemServices.getUserWorkflow().addUser(email);
                
                SystemServices.getUserWorkflow().makePersistent(user);
                
                user.getUserPreferences().getCertificates().add(certificate);
            }
        }

        @StartTransaction
        public void addUsersWithNamedCertificate(X509Certificate certificate, String... emails) 
        throws Exception 
        {
            for (String email : emails)
            {
                User user = SystemServices.getUserWorkflow().addUser(email);
                
                SystemServices.getUserWorkflow().makePersistent(user);
                
                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);
                
                user.getUserPreferences().getNamedCertificates().add(namedCertificate);
            }
        }
        
        @StartTransaction
        public void addUsersWithSigningCertificate(KeyAndCertificate keyAndCertificate, String... emails) 
        throws Exception 
        {
            for (String email : emails)
            {
                User user = SystemServices.getUserWorkflow().addUser(email);
                
                SystemServices.getUserWorkflow().makePersistent(user);
                
                user.getUserPreferences().setKeyAndCertificate(keyAndCertificate);
            }
        }
        
        @StartTransaction
        public NamedBlob createNamedBlob(String category, String name)
        {
            return SystemServices.getNamedBlobManager().createNamedBlob(category, name);
        }
        
        @StartTransaction
        public void addUsersWithNamedBlob(String category, String name, String... emails) 
        throws Exception 
        {
            NamedBlob namedBlob = SystemServices.getNamedBlobManager().getNamedBlob(category, name);
            
            Check.notNull(namedBlob, "namedBlob");
            
            for (String email : emails)
            {
                User user = SystemServices.getUserWorkflow().addUser(email);
                
                SystemServices.getUserWorkflow().makePersistent(user);
                
                user.getUserPreferences().getNamedBlobs().add(namedBlob);
            }
        }
        
        @StartTransaction
        public long getReferencingFromNamedBlobsCount(String category, String name)
        {
            NamedBlob namedBlob = SystemServices.getNamedBlobManager().getNamedBlob(category, name);
            
            Check.notNull(namedBlob, "namedBlob");
            
            return SystemServices.getUserPreferencesWorkflow().getReferencingFromNamedBlobsCount(namedBlob);
        }
        
        @StartTransaction
        public List<UserPreferences> getReferencingFromNamedBlobs(String category, String name, Integer firstResult,
                Integer maxResults)
        {
            List<UserPreferences> result = new LinkedList<UserPreferences>();
            
            NamedBlob namedBlob = SystemServices.getNamedBlobManager().getNamedBlob(category, name);
            
            Check.notNull(namedBlob, "namedBlob");
            
            List<UserPreferences> prefs = SystemServices.getUserPreferencesWorkflow().
                    getReferencingFromNamedBlobs(namedBlob, firstResult, maxResults);
            
            /*
             * copy to force load
             */
            for (UserPreferences pref : prefs) {
                result.add(pref);
            }
            
            return result;
        }
    }

    @Test
    public void testInUseNamedCertificates()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        String[] emails = new String[]{"test@example.com", "test2@example.com"};
        
        proxy.checkNotReferencedFromNamedCertificates(certificate);
        
        proxy.addUsersWithNamedCertificate(certificate, emails);
        
        proxy.checkReferencedFromNamedCertificates(certificate, null, null, emails);
    }
    
    @Test
    public void testInUseCertificates()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        String[] emails = new String[]{"test@example.com", "test2@example.com"};
        
        proxy.checkNotReferencedFromCertificates(certificate);
        
        proxy.addUsersWithCertificate(certificate, emails);
        
        proxy.checkReferencedFromCertificates(certificate, null, null, emails);
    }
    
    @Test
    public void testInUseKeyAndCertificate()
    throws Exception
    {
        File file = new File(testBaseDir, "certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertNotNull(certificate);
        
        CertificateWorkflow workFlow = SystemServices.getKeyAndCertificateWorkflow();

        assertNotNull(workFlow);
        
        assertTrue(workFlow.addCertificateTransacted(certificate));
        
        String[] emails = new String[]{"test@example.com", "test2@example.com"};
        
        proxy.checkNotReferencedFromKeyAndCertificate(certificate);
        
        proxy.addUsersWithSigningCertificate(new KeyAndCertificateImpl(null, certificate), emails);
        
        proxy.checkReferencedFromKeyAndCertificate(certificate, null, null, emails);
    }
    
    @Test
    public void testReferencingFromNamedBlobs()
    throws Exception
    {
        String category = "category";
        String name = "name";
        
        proxy.createNamedBlob(category, name);
        
        proxy.addUsersWithNamedBlob(category, name, "test1@example.com", "test2@example.com");
        
        assertEquals(2, proxy.getReferencingFromNamedBlobsCount(category, name));
        
        List<UserPreferences> prefs = proxy.getReferencingFromNamedBlobs(category, name, null, null);
        
        assertEquals(2, prefs.size());
        
        UserPreferences pref = prefs.get(0);
        
        assertEquals("test1@example.com", pref.getName());
        assertEquals("user", pref.getCategory());

        pref = prefs.get(1);
        
        assertEquals("test2@example.com", pref.getName());
        assertEquals("user", pref.getCategory());        

        prefs = proxy.getReferencingFromNamedBlobs(category, name, 1, 1);
        
        assertEquals(1, prefs.size());
        
        pref = prefs.get(0);
        
        assertEquals("test2@example.com", pref.getName());
        
        prefs = proxy.getReferencingFromNamedBlobs(category, name, 0, 1);
        
        assertEquals(1, prefs.size());
        
        pref = prefs.get(0);
        
        assertEquals("test1@example.com", pref.getName());
        
        proxy.deleteUser("test1@example.com");
        
        assertEquals(1, proxy.getReferencingFromNamedBlobsCount(category, name));

        prefs = proxy.getReferencingFromNamedBlobs(category, name, null, null);
        
        assertEquals(1, prefs.size());
        
        pref = prefs.get(0);
        
        assertEquals("test2@example.com", pref.getName());
        assertEquals("user", pref.getCategory());
                
        proxy.deleteUser("test2@example.com");
        
        assertEquals(0, proxy.getReferencingFromNamedBlobsCount(category, name));
        
        prefs = proxy.getReferencingFromNamedBlobs(category, name, null, null);
        
        assertEquals(0, prefs.size());
    }
}
