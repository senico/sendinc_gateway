/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import mitm.application.djigzo.workflow.UserPreferencesWorkflow;
import mitm.application.djigzo.workflow.impl.UserPreferencesWorkflowImpl;
import mitm.common.cache.SimpleMemoryPatternCache;
import mitm.common.dlp.MatchFilter;
import mitm.common.dlp.MatchFilterRegistry;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyPatternMarshaller;
import mitm.common.dlp.impl.MatchFilterRegistryImpl;
import mitm.common.dlp.impl.PolicyPatternImpl;
import mitm.common.dlp.impl.PolicyPatternMarshallerImpl;
import mitm.common.dlp.impl.matchfilter.MaskingFilter;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.NamedBlobManager;
import mitm.common.properties.hibernate.NamedBlobManagerImpl;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;
import mitm.common.util.Check;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternManagerImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static SessionManager sessionManager;
    private static NamedBlobManager namedBlobManager;
    private static PolicyPatternManager policyPatternManager;
    private static AutoTransactDelegator autoTransactProxy;
    
    
    private final static String XML_PATTERNS = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<policyPatterns>\n" +
        "    <pattern>\n" +
        "        <name>test2</name>\n" +
        "        <regExp>.*&lt;test&gt;</regExp>\n" +
        "        <threshold>0</threshold>\n" +
        "    </pattern>\n" +
        "    <pattern>\n" +
        "        <name>test1</name>\n" +
        "        <regExp>.*&amp;</regExp>\n" +
        "        <threshold>0</threshold>\n" +
        "    </pattern>\n" +
        "</policyPatterns>\n";
    
    private final static String XML_PATTERNS_WITH_CHILDS = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<policyPatterns>\n" +
        "    <pattern>\n" +
        "        <name>node3</name>\n" +
        "        <regExp>.*</regExp>\n" +
        "        <threshold>0</threshold>\n" +
        "    </pattern>\n" +
        "    <pattern>\n" +
        "        <name>node4</name>\n" +
        "        <regExp>.*</regExp>\n" +
        "        <threshold>0</threshold>\n" +
        "    </pattern>\n" +
        "    <pattern>\n" +
        "        <matchFilterName>Mask</matchFilterName>\n" +
        "        <name>node2</name>\n" +
        "        <regExp>.*</regExp>\n" +
        "        <threshold>0</threshold>\n" +
        "    </pattern>\n" +
        "    <group>\n" +
        "        <child>node4</child>\n" +
        "        <child>node2</child>\n" +
        "        <child>node3</child>\n" +
        "        <name>node1</name>\n" +
        "    </group>\n" +
        "</policyPatterns>\n";
        
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());       
        
        namedBlobManager = new NamedBlobManagerImpl(sessionManager);
        
        MatchFilterRegistry matchFilterRegistry = new MatchFilterRegistryImpl(null);
        
        matchFilterRegistry.addMatchFilter(new MaskingFilter());
        
        PolicyPatternMarshaller patternMarshaller = new PolicyPatternMarshallerImpl(new SimpleMemoryPatternCache(),
                matchFilterRegistry);

        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        UserPreferencesWorkflow userPreferencesWorkflow = new UserPreferencesWorkflowImpl(keyAndCertStore, 
                sessionManager, new PasswordBasedEncryptor("test"));
        
        policyPatternManager = new PolicyPatternManagerImpl(namedBlobManager, patternMarshaller, 
                userPreferencesWorkflow);
        
        autoTransactProxy = AutoTransactDelegator.createProxy();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        autoTransactProxy.deleteAll();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }

        @StartTransaction
        public PolicyPatternNode createPattern(String name) {
            return policyPatternManager.createPattern(name);
        }

        @StartTransaction
        public PolicyPatternNode createPattern(String name, PolicyPattern pattern)
        {
            PolicyPatternNode node = policyPatternManager.createPattern(name);
            
            node.setPolicyPattern(pattern);
            
            return node;
        }
        
        @StartTransaction
        public PolicyPatternNode getPattern(String name) {
            return policyPatternManager.getPattern(name);
        }

        @StartTransaction
        public void deletePattern(String name) {
            policyPatternManager.deletePattern(name);
        }

        @StartTransaction
        public void assertNumberOfPatterns(int size) {
            assertEquals(size, policyPatternManager.getNumberOfPatterns());
        }

        @StartTransaction
        public void assertChildSize(int size, String name) {
            assertEquals(size, policyPatternManager.getPattern(name).getChilds().size());
        }
        
        @StartTransaction
        public void addChild(String parent, String child)
        {
            PolicyPatternNode node = policyPatternManager.getPattern(parent);
            
            Check.notNull(node, "node");
            
            PolicyPatternNode childNode = policyPatternManager.getPattern(child);
            
            Check.notNull(childNode, "childNode");
            
            node.getChilds().add(childNode);
        }
        
        @StartTransaction
        public void testAddChilds()
        {
            PolicyPatternNode node1 = policyPatternManager.createPattern("node1");
            PolicyPatternNode node2 = policyPatternManager.createPattern("node2");
            PolicyPatternNode node3 = policyPatternManager.createPattern("node3");
            PolicyPatternNode node4 = policyPatternManager.createPattern("node4");
            
            PolicyPatternImpl pattern = new PolicyPatternImpl("node2", Pattern.compile(".*"));
            pattern.setMatchFilterName("Mask");
            
            node2.setPolicyPattern(pattern);
            node3.setPolicyPattern(new PolicyPatternImpl("node3", Pattern.compile(".*")));
            node4.setPolicyPattern(new PolicyPatternImpl("node4", Pattern.compile(".*")));
                        
            Set<PolicyPatternNode> childs = node1.getChilds();
            childs.add(node2);
            childs.add(node3);
            
            node1.getChilds().add(node4);
        }

        @StartTransaction
        public boolean containsChild(String name, String childName)
        {
            PolicyPatternNode node = policyPatternManager.getPattern(name);

            assertNotNull(node);

            PolicyPatternNode childNode = policyPatternManager.getPattern(childName);
            
            assertNotNull(childNode);
            
            Set<PolicyPatternNode> childs = node.getChilds();
         
            return childs.contains(childNode);
        }

        @StartTransaction
        public void setPolicyPattern(String name, PolicyPattern pattern) {
            policyPatternManager.getPattern(name).setPolicyPattern(pattern);
        }

        @StartTransaction
        public PolicyPattern getPolicyPattern(String name) {
            return policyPatternManager.getPattern(name).getPolicyPattern();
        }

        @StartTransaction
        public MatchFilter getMatchFilter(String name) {
            return policyPatternManager.getPattern(name).getPolicyPattern().getMatchFilter();
        }

        @StartTransaction
        public String exportToXML(String... name)
        throws IOException
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            policyPatternManager.exportToXML(Arrays.asList(name), bos);
            
            return new String(bos.toByteArray(), "UTF-8");
        }

        @StartTransaction
        public void importFromXML(String xml)
        throws IOException
        {
            policyPatternManager.importFromXML(IOUtils.toInputStream(xml, "UTF-8"), false);
        }

        @StartTransaction
        public void importFromXML(String xml, boolean ignoreDuplicates)
        throws IOException
        {
            policyPatternManager.importFromXML(IOUtils.toInputStream(xml, "UTF-8"), ignoreDuplicates);
        }
        
        @StartTransaction
        public void deleteAll()
        {
            namedBlobManager.deleteAll();
        }
        
        @StartTransaction
        public boolean isInUse(String name) {
            return policyPatternManager.isInUse(name);
        }
        
        @StartTransaction
        public List<String> getInUseInfo(String name) {
            return policyPatternManager.getInUseInfo(name);
        }
    }    

    @Test
    public void testCreateNew()
    {
        assertNull(autoTransactProxy.getPattern("test"));

        PolicyPatternNode node = autoTransactProxy.createPattern("test");
        
        assertNotNull(node);
        
        assertEquals("test", node.getName());
        assertNull(node.getPolicyPattern());
        assertEquals(0, node.getChilds().size());
        
        node = autoTransactProxy.getPattern("test");

        assertNotNull(node);

        assertEquals("test", node.getName());
        assertNull(node.getPolicyPattern());
        autoTransactProxy.assertChildSize(0, "test");
    }

    @Test
    public void testAddChilds()
    {
        assertNotNull(autoTransactProxy.createPattern("other"));
        autoTransactProxy.testAddChilds();
        autoTransactProxy.assertChildSize(3, "node1");
        
        assertTrue(autoTransactProxy.containsChild("node1", "node2"));
        assertTrue(autoTransactProxy.containsChild("node1", "node3"));
        assertTrue(autoTransactProxy.containsChild("node1", "node4"));
        assertFalse(autoTransactProxy.containsChild("node1", "other"));
    }

    @Test
    public void testSetPattern()
    {
        assertNotNull(autoTransactProxy.createPattern("test"));
        
        PolicyPatternImpl pattern = new PolicyPatternImpl("name", Pattern.compile(".*"));
        
        pattern.setDescription("description");
        pattern.setThreshold(12);
        
        autoTransactProxy.setPolicyPattern("test", pattern);
        
        PolicyPattern stored = autoTransactProxy.getPolicyPattern("test");
        assertEquals("name", stored.getName());
        assertEquals(".*", stored.getPattern().pattern());
        assertEquals("description", stored.getDescription());
        assertEquals(12, stored.getThreshold());
    }
    
    @Test
    public void testDelete()
    {
        assertNull(autoTransactProxy.getPattern("test"));

        assertNotNull(autoTransactProxy.createPattern("test"));

        assertNotNull(autoTransactProxy.getPattern("test"));
        
        autoTransactProxy.deletePattern("test");

        assertNull(autoTransactProxy.getPattern("test"));
    }    
    
    @Test(expected = ConstraintViolationException.class)
    public void testCreateExisting()
    {
        assertNotNull(autoTransactProxy.createPattern("test"));
        
        autoTransactProxy.createPattern("test");
    }        
    
    @Test
    public void testExportToXML()
    throws IOException
    {
        PolicyPatternImpl pattern1 = new PolicyPatternImpl("test1", Pattern.compile(".*&"));
        PolicyPatternImpl pattern2 = new PolicyPatternImpl("test2", Pattern.compile(".*<test>"));

        autoTransactProxy.createPattern("test1", pattern1);
        autoTransactProxy.createPattern("test2", pattern2);

        String xml = autoTransactProxy.exportToXML("test1", "test2");
        
        assertEquals(XML_PATTERNS, xml);
    }
    
    @Test
    public void testExportToXMLChilds()
    throws IOException
    {
        autoTransactProxy.testAddChilds();

        String xml = autoTransactProxy.exportToXML("node1", "node2", "node3", "node4");
        
        assertEquals(XML_PATTERNS_WITH_CHILDS, xml);
    }
    
    @Test
    public void testImportFromXML()
    throws IOException
    {
        autoTransactProxy.importFromXML(XML_PATTERNS);

        autoTransactProxy.assertNumberOfPatterns(2);
        
        PolicyPattern pattern = autoTransactProxy.getPolicyPattern("test1");
        assertEquals(".*&", pattern.getPattern().pattern());
        
        pattern = autoTransactProxy.getPolicyPattern("test2");
        assertEquals(".*<test>", pattern.getPattern().pattern());
    }
    
    @Test
    public void testImportFromXMChilds()
    throws IOException
    {
        autoTransactProxy.importFromXML(XML_PATTERNS_WITH_CHILDS);
        
        autoTransactProxy.assertNumberOfPatterns(4);
        
        PolicyPattern pattern = autoTransactProxy.getPolicyPattern("node2");
        assertEquals(".*", pattern.getPattern().pattern());
        
        pattern = autoTransactProxy.getPolicyPattern("node3");
        assertEquals(".*", pattern.getPattern().pattern());

        pattern = autoTransactProxy.getPolicyPattern("node4");
        assertEquals(".*", pattern.getPattern().pattern());
        
        autoTransactProxy.assertChildSize(3, "node1");
        
        assertTrue(autoTransactProxy.containsChild("node1", "node2"));
        assertTrue(autoTransactProxy.containsChild("node1", "node3"));
        assertTrue(autoTransactProxy.containsChild("node1", "node4"));
        
        MatchFilter filter = autoTransactProxy.getMatchFilter("node2");
        
        assertEquals("Mask", filter.getName());
    }
    
    @Test
    public void testImportFromXMLDuplicate()
    throws IOException
    {
        autoTransactProxy.importFromXML(XML_PATTERNS);

        autoTransactProxy.assertNumberOfPatterns(2);

        try {
            autoTransactProxy.importFromXML(XML_PATTERNS);
        }
        catch(IOException e) {
            assertEquals("IOException: Pattern with name test2 already exists.", ExceptionUtils.getRootCauseMessage(e));
        }
    }
    
    @Test
    public void testImportFromXMLIgnoreDuplicates()
    throws IOException
    {
        assertNotNull(autoTransactProxy.createPattern("node1"));
        assertNotNull(autoTransactProxy.createPattern("node2"));

        autoTransactProxy.assertNumberOfPatterns(2);
        
        autoTransactProxy.importFromXML(XML_PATTERNS_WITH_CHILDS, true /* ignore duplicates */);

        autoTransactProxy.assertNumberOfPatterns(4);
        
        autoTransactProxy.assertChildSize(0, "node1");
        autoTransactProxy.assertChildSize(0, "node2");
        autoTransactProxy.assertChildSize(0, "node3");
        autoTransactProxy.assertChildSize(0, "node4");
    }    
    
    @Test
    public void testInUseGroupPattern()
    {
        autoTransactProxy.createPattern("parent1");
        autoTransactProxy.createPattern("parent2");
        autoTransactProxy.createPattern("child1");
        autoTransactProxy.createPattern("child2");
        autoTransactProxy.createPattern("child3");
        
        autoTransactProxy.addChild("parent1", "child1");
        autoTransactProxy.addChild("parent1", "child2");

        autoTransactProxy.addChild("parent2", "child1");
        autoTransactProxy.addChild("parent1", "parent2");
        
        assertTrue(autoTransactProxy.isInUse("child1"));
        assertTrue(autoTransactProxy.isInUse("child2"));
        assertTrue(autoTransactProxy.isInUse("parent2"));
        assertFalse(autoTransactProxy.isInUse("child3"));
        assertFalse(autoTransactProxy.isInUse("parent1"));
        
        assertEquals("[pattern,group,parent1, pattern,group,parent2]", 
                autoTransactProxy.getInUseInfo("child1").toString());
        assertEquals("[pattern,group,parent1]", 
                autoTransactProxy.getInUseInfo("child2").toString());
        assertEquals("[pattern,group,parent1]", 
                autoTransactProxy.getInUseInfo("parent2").toString());
        assertEquals("[]", 
                autoTransactProxy.getInUseInfo("child3").toString());
        assertEquals("[]", 
                autoTransactProxy.getInUseInfo("parent1").toString());
    }
}
