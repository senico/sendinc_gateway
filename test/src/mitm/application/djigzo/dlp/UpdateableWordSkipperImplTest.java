/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.properties.NamedBlobManager;
import mitm.common.properties.hibernate.NamedBlobManagerImpl;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class UpdateableWordSkipperImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static SessionManager sessionManager;
    private static NamedBlobManager namedBlobManager;
    private static UpdateableWordSkipper updateableWordSkipper;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());       
        
        namedBlobManager = new NamedBlobManagerImpl(sessionManager);
        
        updateableWordSkipper = new UpdateableWordSkipperImpl(namedBlobManager, sessionManager, 
                new File("conf/skip-words.txt"));
    }
    
    @Test
    public void testSkip()
    throws IOException
    {
        assertTrue(updateableWordSkipper.isSkip("the"));
        assertTrue(updateableWordSkipper.isSkip("us"));
        assertFalse(updateableWordSkipper.isSkip("nonexistingword"));
        /*
         * Check is case insensitive
         */
        assertTrue(updateableWordSkipper.isSkip("US"));
        /*
         * Input is not trimmed
         */
        assertFalse(updateableWordSkipper.isSkip(" us "));
    }

    @Test
    public void testGetSkipList()
    throws IOException
    {
        String wordList = updateableWordSkipper.getSkipList();
        
        assertTrue(wordList.startsWith("the is be to "));
        assertTrue(wordList.endsWith("give day most us"));
    }
    
    @Test
    public void testsetSkipList()
    throws IOException
    {
        updateableWordSkipper.setSkipList("aap NOOT mies");

        assertTrue(updateableWordSkipper.isSkip("aAp"));
        assertFalse(updateableWordSkipper.isSkip("schapen"));
        assertTrue(updateableWordSkipper.isSkip("mies"));
        
        String wordList = updateableWordSkipper.getSkipList();
        assertEquals("aap noot mies", wordList);
    }    
}
