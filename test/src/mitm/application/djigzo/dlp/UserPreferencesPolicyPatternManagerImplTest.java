/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.dlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.DomainManager;
import mitm.application.djigzo.GlobalPreferencesManager;
import mitm.application.djigzo.User;
import mitm.application.djigzo.UserPreferences;
import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.dlp.impl.PolicyPatternImpl;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesPolicyPatternManagerImplTest
{
    private static UserPreferencesPolicyPatternManager userPrefsPatternManager;   
    private static PolicyPatternManager policyPatternManager;
    private static GlobalPreferencesManager globalPreferencesManager;
    private static DomainManager domainManager;
    private static UserWorkflow userWorkflow;
    
    private static AutoTransactDelegator autoTransactProxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
        
        userPrefsPatternManager = SystemServices.getService("userPreferencesPolicyPatternManager", 
                UserPreferencesPolicyPatternManager.class);

        policyPatternManager = SystemServices.getService("policyPatternManager", 
                PolicyPatternManager.class);
        
        globalPreferencesManager = SystemServices.getGlobalPreferencesManager();
            
        domainManager = SystemServices.getDomainManager();
        
        userWorkflow = SystemServices.getUserWorkflow();
        
        autoTransactProxy = AutoTransactDelegator.createProxy();
        
        autoTransactProxy.setup();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, SystemServices.getSessionManager()).createProxy();
            
            return autoTransactDelegator;
        }

        @StartTransaction
        public void setup()
        throws Exception
        {
            PolicyPatternNode node1 = policyPatternManager.createPattern("pattern1");
            node1.setPolicyPattern(new PolicyPatternImpl("pattern1", Pattern.compile(".*")));

            PolicyPatternNode node2 = policyPatternManager.createPattern("pattern2");
            node2.setPolicyPattern(new PolicyPatternImpl("pattern2", Pattern.compile("123")));
            
            node1.getChilds().add(node2);
            
            PolicyPatternNode node3 = policyPatternManager.createPattern("pattern3");
            node3.setPolicyPattern(new PolicyPatternImpl("pattern3", Pattern.compile("abc.*")));

            PolicyPatternNode node4 = policyPatternManager.createPattern("pattern4");
            node4.setPolicyPattern(new PolicyPatternImpl("pattern4", Pattern.compile("\\d")));

            PolicyPatternNode node5 = policyPatternManager.createPattern("pattern5");
            node5.setPolicyPattern(new PolicyPatternImpl("pattern5", Pattern.compile("5")));
            
            userPrefsPatternManager.setPatterns(globalPreferencesManager.getGlobalUserPreferences(),
                    Collections.singleton(node1));
            
            Set<PolicyPatternNode> globalPatterns = userPrefsPatternManager.getPatterns(
                    globalPreferencesManager.getGlobalUserPreferences());
            
            globalPatterns.add(node3);

            userPrefsPatternManager.setPatterns(globalPreferencesManager.getGlobalUserPreferences(),
                    globalPatterns);

            UserPreferences domainPrefs = domainManager.addDomain("example1.com");

            userPrefsPatternManager.setPatterns(domainPrefs, Collections.singleton(node4));

            domainPrefs = domainManager.addDomain("*.sub.example.com");

            userPrefsPatternManager.setPatterns(domainPrefs, Collections.singleton(node5));
            
            domainManager.addDomain("example2.com");
            
            User user = userWorkflow.addUser("user@example1.com");

            userWorkflow.makePersistent(user);
            
            user = userWorkflow.addUser("user@example2.com");

            userWorkflow.makePersistent(user);

            user = userWorkflow.addUser("user@example3.com");

            userWorkflow.makePersistent(user);

            user = userWorkflow.addUser("user@example4.com");

            userWorkflow.makePersistent(user);

            userPrefsPatternManager.setPatterns(user.getUserPreferences(),
                    Collections.singleton(node5));
            
            user = userWorkflow.addUser("user@sub.example.com");

            userWorkflow.makePersistent(user);

            user = userWorkflow.addUser("user@sub.sub.example.com");

            userWorkflow.makePersistent(user);
        }
        
        private boolean containsPolicyPatternNode(Collection<PolicyPatternNode> patterns, final String name)
        {
            return CollectionUtils.exists(patterns, new Predicate()
            {
                @Override
                public boolean evaluate(Object obj) {
                     return name.equals(((PolicyPatternNode) obj).getName());
                }
            });
        }

        private PolicyPatternNode getPolicyPatternNode(Collection<PolicyPatternNode> patterns, final String name)
        {
            return (PolicyPatternNode) CollectionUtils.find(patterns, new Predicate()
            {
                @Override
                public boolean evaluate(Object obj) {
                     return name.equals(((PolicyPatternNode) obj).getName());
                }
            });
        }
        
        private boolean isInherited(Collection<PolicyPatternNode> patterns)
        {
            for (PolicyPatternNode pattern : patterns)
            {
                if (!(pattern instanceof UserPolicyPatternNode)) {
                    fail("Should be a UserPolicyPatternNode");
                }
                
                if (!((UserPolicyPatternNode)pattern).isInherited()) {
                    return false;
                }
            }
            
            return true;
        }
        
        @StartTransaction
        public void testGlobalSettings()
        throws Exception
        {
            Set<PolicyPatternNode> patterns = userPrefsPatternManager.getPatterns(
                    globalPreferencesManager.getGlobalUserPreferences());
            
            assertNotNull(patterns);
            assertEquals(2, patterns.size());
            
            assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
            assertTrue(containsPolicyPatternNode(patterns, "pattern3"));
            
            assertFalse(isInherited(patterns));
        }

        @StartTransaction
        public void testDomainSettings()
        throws Exception
        {
            /*
             * example1.com has explicitly set policy patterns so not inherited from global
             */
            Set<PolicyPatternNode> patterns = userPrefsPatternManager.getPatterns(
                    domainManager.getDomainPreferences("example1.com"));
            assertNotNull(patterns);
            assertEquals(1, patterns.size());
            assertFalse(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern4"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));

            
            /*
             * example2.com inherits from Global
             */
            patterns = userPrefsPatternManager.getPatterns(
                    domainManager.getDomainPreferences("example2.com"));
            assertNotNull(patterns);
            assertEquals(2, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
            assertTrue(containsPolicyPatternNode(patterns, "pattern3"));
            
            PolicyPatternNode node1 = getPolicyPatternNode(patterns, "pattern1");
            
            assertNotNull(node1);
            assertEquals(".*", node1.getPolicyPattern().getPattern().pattern());
        }
        
        @StartTransaction
        public void testUserSettings()
        throws Exception
        {
            /*
             * user@example1.com inherits from example1.com
             */
            User user = userWorkflow.getUser("user@example1.com", GetUserMode.NULL_IF_NOT_EXIST);
            assertNotNull(user);
            
            Set<PolicyPatternNode> patterns = userPrefsPatternManager.getPatterns(user.getUserPreferences());
         
            assertNotNull(patterns);
            assertEquals(1, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern4"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));

            /*
             * user@example2.com inherits from example2.com which inherits from global
             */
            user = userWorkflow.getUser("user@example2.com", GetUserMode.NULL_IF_NOT_EXIST);
            assertNotNull(user);
            
            patterns = userPrefsPatternManager.getPatterns(user.getUserPreferences());
         
            assertEquals(2, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
            assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

            /*
             * user@example3.com inherits from global
             */
            user = userWorkflow.getUser("user@example3.com", GetUserMode.NULL_IF_NOT_EXIST);
            assertNotNull(user);
            
            patterns = userPrefsPatternManager.getPatterns(user.getUserPreferences());
         
            assertEquals(2, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
            assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

            /*
             * user@example4.com has explicitly set policy patterns
             */
            user = userWorkflow.getUser("user@example4.com", GetUserMode.NULL_IF_NOT_EXIST);
            assertNotNull(user);
            
            patterns = userPrefsPatternManager.getPatterns(user.getUserPreferences());
         
            assertEquals(1, patterns.size());
            assertFalse(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern5"));
        }
        
        @StartTransaction
        public void testWildcard()
        throws Exception
        {
            /*
             * user@sub.example.com inherits from *.sub.example.com
             */
            User user = userWorkflow.getUser("user@sub.example.com", GetUserMode.NULL_IF_NOT_EXIST);
            assertNotNull(user);

            Set<PolicyPatternNode> patterns = userPrefsPatternManager.getPatterns(user.getUserPreferences());
            
            assertNotNull(patterns);
            assertEquals(1, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern5"));
        }
    }
    
    @Test
    public void testGlobalSettings()
    throws Exception
    {
        autoTransactProxy.testGlobalSettings();
    }

    @Test
    public void testDomainSettings()
    throws Exception
    {
        autoTransactProxy.testDomainSettings();
    }

    @Test
    public void testUserSettings()
    throws Exception
    {
        autoTransactProxy.testUserSettings();
    }

    @Test
    public void testWildcard()
    throws Exception
    {
        autoTransactProxy.testWildcard();
    }
}
