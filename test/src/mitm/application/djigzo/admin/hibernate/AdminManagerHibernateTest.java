/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.AdminManager;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AdminManagerHibernateTest 
{
	private static AdminManager adminManager;
	private static AuthorityManager authorityManagerAutoCommit;
	private static AuthorityManager authorityManager;
	private static AdminManager adminManagerAutoCommit;
    private static SessionManager sessionManager;
    private static DatabaseActionExecutor actionExecutor;
	
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        authorityManagerAutoCommit = SystemServices.getService("authorityManagerAutoCommit", AuthorityManager.class);
        adminManagerAutoCommit = SystemServices.getService("adminManagerAutoCommit", AdminManager.class);
        adminManager = SystemServices.getService("adminManager", AdminManager.class);
        authorityManager = SystemServices.getService("authorityManager", AuthorityManager.class);
        sessionManager = SystemServices.getSessionManager();

        actionExecutor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
    }

    @Before
    public void setup()
    {
    	adminManagerAutoCommit.deleteAll();
    	authorityManagerAutoCommit.deleteAll();
    }

    public boolean hasRoles(final String username, final String... roles) 
    throws DatabaseException
    {
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        boolean has = actionExecutor.executeTransaction(
	        new DatabaseAction<Boolean>()
	        {
	            @Override
                public Boolean doAction(Session session)
	            throws DatabaseException
	            {
	                Session previousSession = sessionManager.getSession();
	                try {
	                	sessionManager.setSession(session);
	
	                	return hasRolesAction(username, roles);
	                }
	                finally {
	                	sessionManager.setSession(previousSession);
	                }
	            }
	        });
        
        return has;
    }

    public boolean hasRolesAction(String username, String... roles)
    {
    	Admin admin = adminManager.getAdmin(username);
    	
    	assertNotNull(admin);
    	
    	Set<Authority> authorities = admin.getAuthorities();
    	
    	Set<String> hasRoles = new HashSet<String>();
    	
    	for (Authority authority : authorities) 
    	{
    		hasRoles.add(authority.getRole());
    	}
    	
    	for (String role : roles) 
    	{
    		if (!hasRoles.contains(role)) {
    			return false;
    		}
    	}
    	
    	if (roles.length != authorities.size()) {
			return false;
    	}
    	
    	return true;
    }

    public void addRoleAction(String username, String role)
    {
    	Authority authority = authorityManager.getAuthority(role);
    	
    	assertNotNull(authority);
    	
    	Admin admin = adminManager.getAdmin(username);

    	assertNotNull(admin);
    	
    	admin.getAuthorities().add(authority);
    }
    
    @Test
    public void testAddAuthorities() 
    throws DatabaseException
    {
    	adminManagerAutoCommit.addAdmin("admin1", true);
    	adminManagerAutoCommit.addAdmin("admin2", true);
    	
    	authorityManagerAutoCommit.addAuthority("TEST");
    	
        final SessionManager sessionManager = SystemServices.getSessionManager();
        
        actionExecutor.executeTransaction(
	        new DatabaseVoidAction()
	        {
	            @Override
	            public void doAction(Session session)
	            throws DatabaseException
	            {
	                Session previousSession = sessionManager.getSession();
	                try {
	                	sessionManager.setSession(session);
	
	                	addRoleAction("admin1", "TEST");
	                }
	                finally {
	                	sessionManager.setSession(previousSession);
	                }
	            }
	        });

        actionExecutor.executeTransaction(
    	        new DatabaseVoidAction()
    	        {
    	            @Override
    	            public void doAction(Session session)
    	            throws DatabaseException
    	            {
    	                Session previousSession = sessionManager.getSession();
    	                try {
    	                	sessionManager.setSession(session);
    	
    	                	addRoleAction("admin2", "TEST");
    	                }
    	                finally {
    	                	sessionManager.setSession(previousSession);
    	                }
    	            }
    	        });
    	
        hasRoles("admin1", "TEST");
        hasRoles("admin2", "TEST");
    }
    
    
    @Test
    public void testAddAdmin() 
    throws DatabaseException
    {
    	adminManagerAutoCommit.addAdmin("admin", true);
    	
    	Admin admin = adminManagerAutoCommit.getAdmin("admin");

    	assertEquals("admin", admin.getUsername());
    	assertNull(admin.getPassword());
    	assertTrue(admin.isBuiltIn());
    	assertTrue(hasRoles("admin", new String[]{}));
    	assertFalse(admin.isEnabled());
    	assertNull(admin.getPasswordEncoding());
    	assertNull(admin.getSalt());
    	
    	
    	admin.setEnabled(true);
    	admin.setPassword("test");
    	admin.setPasswordEncoding(PasswordEncoding.ENCODED);
    	admin.setSalt("salt");
    	
    	assertTrue(admin.isEnabled());
    	assertEquals("test", admin.getPassword());
    	assertEquals(PasswordEncoding.ENCODED, admin.getPasswordEncoding());
    	assertEquals("salt", admin.getSalt());
    	
    	adminManagerAutoCommit.addAdmin("admin2", false);
    	
    	admin = adminManagerAutoCommit.getAdmin("admin2");

    	assertEquals("admin2", admin.getUsername());
    	assertNull(admin.getPassword());
    	assertFalse(admin.isBuiltIn());    	
    	assertTrue(hasRoles("admin", new String[]{}));
    }
    
    @Test
    public void testAddDefaultAdminsAndAuthorities() 
    throws DatabaseException
    {
    	SystemServices.getDefaultAdmins().addDefaultAdminsAndAuthorities();
    	
    	Admin admin = adminManagerAutoCommit.getAdmin("admin");
    	
    	assertEquals("admin", admin.getUsername());
    	assertEquals("admin", admin.getPassword());
    	assertTrue(admin.isBuiltIn());
    	assertTrue(hasRoles("admin", new String[]{FactoryRoles.ROLE_LOGIN, 
    			FactoryRoles.ROLE_ADMIN}));
    }
}
