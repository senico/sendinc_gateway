/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.admin.Authority;
import mitm.application.djigzo.admin.AuthorityManager;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SortDirection;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AuthorityManagerHibernateTest 
{
	private static AuthorityManager authorityManager;
	
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        DjigzoTestUtils.initialize();
                
        HibernateUtils.recreateTables(SystemServices.getHibernateSessionSource().getHibernateConfiguration());
        
        authorityManager = SystemServices.getService("authorityManagerAutoCommit", AuthorityManager.class);
    }

    @Before
    public void setup()
    {
    	authorityManager.deleteAll();
    }
    
    @Test
    public void testAddAuthority()
    {
    	Authority authority = authorityManager.getAuthority("TEST");

    	assertNull(authority);
    	
    	authority = authorityManager.addAuthority("TEST");

    	assertNotNull(authority);
    	assertEquals("TEST", authority.getRole());
    	
    	authority = authorityManager.getAuthority("TEST");
    	
    	assertNotNull(authority);
    	assertEquals("TEST", authority.getRole());
    }

    @Test
    public void testDelete()
    {
    	Authority authority = authorityManager.addAuthority("TEST");

    	assertNotNull(authority);
    	assertEquals("TEST", authority.getRole());
    	
    	assertTrue(authorityManager.deleteAuthority("TEST"));
    	assertFalse(authorityManager.deleteAuthority("TEST"));
    }
    
    @Test
    public void testCount()
    {
    	assertEquals(0, authorityManager.getAuthorityCount());
    	
    	authorityManager.addAuthority("TEST1");

    	assertEquals(1, authorityManager.getAuthorityCount());
    	
    	authorityManager.addAuthority("TEST2");

    	assertEquals(2, authorityManager.getAuthorityCount());
    }

    @Test
    public void testAddExisting()
    {
    	authorityManager.addAuthority("TEST");

    	try {
    		authorityManager.addAuthority("TEST");
    		
    		fail();
    	}
    	catch(ConstraintViolationException e) {
    		// expected
    	}
    }
    
    @Test
    public void testGetAuthority()
    {
    	authorityManager.addAuthority("TEST");

    	Authority authority = authorityManager.getAuthority("TEST");
    	
    	assertEquals("TEST", authority.getRole());
    }
    
    @Test
    public void testGetAuthories()
    {
    	authorityManager.addAuthority("TEST1");
    	authorityManager.addAuthority("TEST2");
    	authorityManager.addAuthority("TEST3");
    	authorityManager.addAuthority("TEST4");

    	List<? extends Authority> authorities = authorityManager.getAuthorities(
    			null, null, null);
    	
    	assertEquals("TEST1", authorities.get(0).getRole());
    	assertEquals("TEST2", authorities.get(1).getRole());
    	assertEquals("TEST3", authorities.get(2).getRole());
    	assertEquals("TEST4", authorities.get(3).getRole());

    	authorities = authorityManager.getAuthorities(
    			1, 2, null);
    	
    	assertEquals("TEST2", authorities.get(0).getRole());
    	assertEquals("TEST3", authorities.get(1).getRole());

    	authorities = authorityManager.getAuthorities(
    			1, 2, SortDirection.DESC);
    	
    	assertEquals("TEST3", authorities.get(0).getRole());
    	assertEquals("TEST2", authorities.get(1).getRole());

    	authorities = authorityManager.getAuthorities(
    			100, 100, SortDirection.DESC);
    	
    	assertEquals(0, authorities.size());
    }
}
