/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.admin.hibernate;

import java.util.List;

import mitm.application.djigzo.admin.Admin;
import mitm.application.djigzo.admin.hibernate.AdminManagerHibernate;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;

/**
 * Auto commit extension of AdminManagerHibernate for testing purposes.
 * 
 * @author Martijn Brinkers
 *
 */
public class AdminManagerHibernateAutoCommit extends AdminManagerHibernate 
{
	public AdminManagerHibernateAutoCommit(SessionManager sessionManager)
	{
		super(sessionManager);
	}

	@StartTransaction
    @Override
	public List<? extends Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection) {
		return super.getAdmins(firstResult, maxResults, sortDirection);
	}

	@StartTransaction
    @Override
	public Admin addAdmin(String username, boolean builtIn) {
		return super.addAdmin(username, builtIn);
	}

	@StartTransaction
    @Override
	public Admin getAdmin(String username) {
		return super.getAdmin(username);
	}
	
	@StartTransaction
    @Override
	public boolean deleteAdmin(String username) {
		return super.deleteAdmin(username);
	}

	@StartTransaction
    @Override
	public void deleteAll() {
		super.deleteAll();
	}
	
	@StartTransaction
    @Override
	public int getAdminCount() {
		return super.getAdminCount();
	}
}
