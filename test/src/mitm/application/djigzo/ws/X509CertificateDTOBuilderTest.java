/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.test.TestUtils;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class X509CertificateDTOBuilderTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        BasicConfigurator.configure();
    }

    @Test
    public void testValidCertificate()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(
            "test/resources/testdata/certificates/ldap-crl.cer"));
        
        X509CertificateDTOBuilderImpl factory = new X509CertificateDTOBuilderImpl(null, false);
        
        X509CertificateDTO dto = factory.buildCertificateDTO(certificate, "123");
        
        assertEquals("D883B8E711DDD02F04CF1EA77140CF6C3A49CB909C02FEC548E4A4ECA160D29B6EC413B95983F428EBA4133E4" + 
                "F6F1F9D62E5293A67F8DFD43585370C40BC068E", dto.getThumbprint());

        assertEquals("7D071BBCA20003217313FE535D7E9E7AF18A698B", dto.getThumbprintSHA1());
        
        assertEquals("123", dto.getKeyAlias());
        assertEquals("1787A41300030000C620", dto.getSerialNumberHex());
        assertEquals("CN=ukspkca01, DC=corp, DC=hds, DC=com", dto.getIssuerFriendly());
        assertEquals("CN=Dick Hoogendoorn, OU=Users, OU=NL, OU=EMEA, DC=corp, DC=hds, DC=com", dto.getSubjectFriendly());
        assertEquals("C042D9AA873BB62FAEE9362CEB14C1C6B00F4598", dto.getSubjectKeyIdentifier());
        assertEquals("Fri Jan 06 16:57:25 CET 2006", dto.getNotBefore().toString());
        assertEquals("Sat Jan 06 16:57:25 CET 2007", dto.getNotAfter().toString());
        assertTrue(dto.isExpired());
        assertEquals("[]", dto.getEmailFromAltNames().toString());
        assertEquals("[]", dto.getEmailFromDN().toString());
        
        Set<String> keyUsage = dto.getKeyUsage();
        
        assertEquals(2, keyUsage.size());
        assertTrue(keyUsage.contains("digitalSignature"));
        assertTrue(keyUsage.contains("keyEncipherment"));

        Set<String> extendedKeyUsage = dto.getExtendedKeyUsage();

        assertEquals(2, extendedKeyUsage.size());
        assertTrue(extendedKeyUsage.contains("clientAuth"));
        assertTrue(extendedKeyUsage.contains("emailProtection"));
        
        assertFalse(dto.isCA());

        assertNull(dto.getPathLengthConstraint());

        assertEquals(1024, dto.getPublicKeyLength());
        assertEquals("RSA", dto.getPublicKeyAlgorithm());
        assertEquals("SHA1WITHRSA", dto.getSignatureAlgorithm());
        
        Set<String> uris = dto.getURIDistributionPointNames();
        
        assertNotNull(uris);
        assertEquals(2, uris.size());
        assertTrue(uris.contains("ldap:///CN=ukspkca01,CN=UKSPKCA01,CN=CDP,CN=Public%20Key%20Services," + 
                "CN=Services,CN=Configuration,DC=hds,DC=com?certificateRevocationList?base?" + 
                "objectClass=cRLDistributionPoint"));
            
        assertTrue(uris.contains("http://ukspkca01.corp.hds.com/CertEnroll/ukspkca01.crl"));
    }
    
    @Test
    public void testValidCRLDistPoint()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(
            "test/resources/testdata/certificates/dod-mega-crl.cer"));
        
        X509CertificateDTOBuilderImpl factory = new X509CertificateDTOBuilderImpl(null, false);
        
        X509CertificateDTO dto = factory.buildCertificateDTO(certificate, null);
        
        assertEquals("[KeenerPB@mcnosc.usmc.mil]", dto.getEmailFromAltNames().toString());
        assertEquals("[]", dto.getEmailFromDN().toString());
        
        Set<String> uris = dto.getURIDistributionPointNames();
        
        assertNotNull(uris);

        assertEquals(1, uris.size());
        
        assertTrue(uris.contains("ldap://email-ds-3.c3pki.chamb.disa.mil/cn%3dDOD%20CLASS%203%20EMAIL%20CA-9%2cou%3d" +
        		"PKI%2cou%3dDoD%2co%3dU.S.%20Government%2cc%3dUS?certificaterevocationlist;binary"));
    }
    
    @Test
    public void testIllegalCRLDistPoint()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(
            "test/resources/testdata/certificates/certeurope_root_ca_illegal_crl_dist_point.crt"));
        
        X509CertificateDTOBuilderImpl factory = new X509CertificateDTOBuilderImpl(null, false);
        
        X509CertificateDTO dto = factory.buildCertificateDTO(certificate, null);

        Set<String> uris = dto.getURIDistributionPointNames();
        
        assertNotNull(uris);

        assertEquals(1, uris.size());
        
        assertTrue(uris.contains(X509CertificateDTOBuilderImpl.ERROR));
        
        assertEquals("91A96B7D6F9DC6C11E5E736E088F8BCEDE4640CB", dto.getThumbprintSHA1());
    }    
}
