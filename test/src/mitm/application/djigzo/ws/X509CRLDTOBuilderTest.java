/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.X509CRL;

import mitm.test.TestUtils;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class X509CRLDTOBuilderTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        BasicConfigurator.configure();
    }

    @Test
    public void testValidCRL()
    throws Exception
    {
        X509CRL crl = TestUtils.loadX509CRL(new File(
                "test/resources/testdata/crls/intel-basic-enterprise-issuing-CA.crl"));
        
        X509CRLDTOBuilder builder = new X509CRLDTOBuilder(crl);
        
        assertEquals("5D4C02A39F7378F43671448F47506F8952F3C809F80DE352FB465DC93BC5A8F0B4BFCB2B24FD8518D533F306" + 
                "FAFBD37CD59E07093D324294A4C3BAA589299188", builder.getThumbprint());
        
        assertEquals("40B88F4BC5B487390A985293C67AB3FCBB3A7314", builder.getThumbprintSHA1());
        assertEquals("CN=Intel Corporation Basic Enterprise Issuing CA 1, OU=Information Technology Enterprise " + 
                "Business Computing, O=Intel Corporation, L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com", 
                builder.getIssuerFriendly());
        assertEquals("Sat Oct 06 02:08:57 CEST 2007", builder.getNextUpdate().toString());
        assertEquals("Fri Sep 21 21:08:57 CEST 2007", builder.getThisUpdate().toString());
        assertEquals(2, builder.getVersion());
        assertEquals("E3", builder.getCrlNumber());
        assertNull(builder.getDeltaIndicator());
        assertFalse(builder.isDeltaCRL());
        assertEquals("SHA1WITHRSA", builder.getSignatureAlgorithm());
    }
    
    @Test
    public void testDeltaCRL()
    throws Exception
    {
        X509CRL crl = TestUtils.loadX509CRL(new File(
                "test/resources/testdata/PKITS/crls/deltaCRLCA1deltaCRL.crl"));
        
        X509CRLDTOBuilder builder = new X509CRLDTOBuilder(crl);
        
        assertEquals("98D9E18739E05D939C0FC6242C5B4829CE0FCD9FB725A11914AFAC87DBD7976D4549FD54A251F639C5339B3566" + 
                "FBA396962DB68E0004DC27847CC7C362771909", builder.getThumbprint());
        
        assertEquals("D596FCC675D8A4A544FB4833F12DB43E799D71A4", builder.getThumbprintSHA1());
        assertEquals("CN=deltaCRL CA1, O=Test Certificates, C=US", builder.getIssuerFriendly());
        assertEquals("Tue Apr 19 16:57:20 CEST 2011", builder.getNextUpdate().toString());
        assertEquals("Wed Jan 01 13:00:00 CET 2003", builder.getThisUpdate().toString());
        assertEquals(2, builder.getVersion());
        assertEquals("2", builder.getCrlNumber());
        assertEquals("1", builder.getDeltaIndicator());
        assertTrue(builder.isDeltaCRL());
        assertEquals("SHA1WITHRSA", builder.getSignatureAlgorithm());
    }    
}
