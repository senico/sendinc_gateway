/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.io.File;
import java.util.Properties;

import mitm.application.djigzo.james.service.DjigzoServiceImpl;
import mitm.application.djigzo.service.DjigzoServiceRegistry;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;

import org.apache.avalon.framework.service.DefaultServiceManager;
import org.apache.avalon.framework.service.ServiceException;
import org.apache.log4j.PropertyConfigurator;

public class DjigzoTestUtils
{
	private static boolean initialized = false;
	
    /*
     * Path to the spring config file.
     */
    public static String DEFAULT_TEST_SPRING_CONFIG = "test/resources/spring/djigzo.xml";

    /*
     * Path to log4j properties. Default value is relative to james/bin
     */
    private static String log4jConfig = "conf/log4j.properties";
        
    private static void initializeLogging()
    {
        PropertyConfigurator.configure(log4jConfig);        
    }

    private static void initializeRegistry(String... springConfig) 
    throws ServiceException
    {
        DjigzoServiceRegistry.initialize(springConfig); 
    }

    private static void initializeAvalonServiceManager()
    throws ServiceException
    {
    	/*
    	 * We need to set the default JNDI factory. For James this is done in james.wrapper.conf
    	 * but for testing we will not use the service wrapper so we will hardcode it.
    	 * 
    	 */
    	Properties systemProperties = new Properties(System.getProperties());
    	
    	systemProperties.put("java.naming.factory.initial", "tyrex.naming.MemoryContextFactory");
    	systemProperties.put("java.naming.provider.url", "djigzo");
    	
        System.setProperties(systemProperties);
        
        /*
         * We need to register at default service manager for testing purposes because 
         * avalonServiceManager requires that a ServiceManager is bound to JNDI. This
         * is normally done in the James DjigzoService but with unit testing James
         * does not run.
         */
        new DjigzoServiceImpl().service(new DefaultServiceManager());
    }

    public static void initialize() 
    throws ServiceException
    {
        initialize(DEFAULT_TEST_SPRING_CONFIG);
    }
    
    public synchronized static void initialize(String... springConfig) 
    throws ServiceException
    {
    	if (!initialized)
    	{
    		initializeAvalonServiceManager();
	        initializeLogging();
	        initializeRegistry(springConfig);
	        
	        initialized = true;
    	}    	
    }
    
    public static void recreateTables()
    {
        HibernateSessionSource sessionSource;
        
        File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());        
    }
}
