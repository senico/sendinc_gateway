/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.ca;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.template.TemplateBuilder;
import mitm.common.template.TemplateBuilderImpl;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class PFXMailBuilderTest
{
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");

    private static final File testPFX = new File(testBase, "keys/testCertificates.p12");
    private static final File templateFile = new File("resources/templates/mail-pfx.ftl");
    
    private static TemplateBuilder templateBuilder;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
        
        templateBuilder = new TemplateBuilderImpl(new File(SystemUtils.USER_DIR));
    }
    
    private byte[] getPFX(MimeMessage message)
    throws Exception
    {
        Multipart mp;

        mp = (Multipart) message.getContent();

        BodyPart pdfPart = null;

        for (int i=0; i < mp.getCount(); i++)
        {
            BodyPart part = mp.getBodyPart(i);

            if (part.isMimeType("application/octet-stream")) {
                pdfPart = part;
            }
        }

        assertNotNull(pdfPart);
        
        Object content = pdfPart.getContent();
        
        assertTrue(content instanceof ByteArrayInputStream);
        
        return IOUtils.toByteArray((ByteArrayInputStream) content);
    }    
        
    @Test
    public void testReplacePFXSendSMSTrue()
    throws Exception
    {
        byte[] pfx = IOUtils.toByteArray(new FileInputStream(testPFX));
        
        PFXMailBuilder builder = new PFXMailBuilder(IOUtils.toString(new FileInputStream(templateFile)), 
                templateBuilder);
        
        String from = "123@test.com";
        
        builder.setFrom(new InternetAddress(from, "test user"));
        builder.setPFX(pfx);
        builder.addProperty("sendSMS", true);
        builder.addProperty("phoneNumberAnonymized", "1234***");
        builder.addProperty("id", "0987");
        
        
        MimeMessage message = builder.createMessage();
        
        assertNotNull(message);
        
        MailUtils.writeMessage(message, new File(tempDir, "testReplacePFXSendSMSTrue.eml"));

        Multipart mp;

        mp = (Multipart) message.getContent();

        BodyPart textPart = mp.getBodyPart(0);
        
        assertTrue(textPart.isMimeType("text/plain"));
        
        String body = (String) textPart.getContent();
        
        assertTrue(body.contains("was sent to you by SMS"));
        
        /*
         * Check if the PFX has really been replaced
         */
        byte[] newPFX = getPFX(message);
        
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        keyStore.load(new ByteArrayInputStream(newPFX), "test".toCharArray());

        assertEquals(22, keyStore.size());
    }

    @Test
    public void testReplacePFXSendSMSFalse()
    throws Exception
    {
        byte[] pfx = IOUtils.toByteArray(new FileInputStream(testPFX));
        
        PFXMailBuilder builder = new PFXMailBuilder(IOUtils.toString(new FileInputStream(templateFile)), 
                templateBuilder);
        
        String from = "123@test.com";
        
        builder.setFrom(new InternetAddress(from, "test user"));
        builder.setPFX(pfx);
        builder.addProperty("sendSMS", false);
                
        MimeMessage message = builder.createMessage();
        
        assertNotNull(message);
        
        MailUtils.writeMessage(message, new File(tempDir, "testReplacePFXSendSMSFalse.eml"));

        Multipart mp;

        mp = (Multipart) message.getContent();

        BodyPart textPart = mp.getBodyPart(0);
        
        assertTrue(textPart.isMimeType("text/plain"));
        
        String body = (String) textPart.getContent();
        
        assertFalse(body.contains("was sent to you by SMS"));
        
        /*
         * Check if the PFX has really been replaced
         */
        byte[] newPFX = getPFX(message);
        
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        keyStore.load(new ByteArrayInputStream(newPFX), "test".toCharArray());

        assertEquals(22, keyStore.size());
    }
    
    @Test
    public void testReplacePFX()
    throws Exception
    {
        byte[] pfx = IOUtils.toByteArray(new FileInputStream(testPFX));
        
        PFXMailBuilder builder = new PFXMailBuilder(IOUtils.toString(new FileInputStream(templateFile)), 
                templateBuilder);
        
        String from = "123@test.com";
        
        builder.setFrom(new InternetAddress(from, "test user"));
        builder.setPFX(pfx);
        
        MimeMessage message = builder.createMessage();
        
        assertNotNull(message);
        
        MailUtils.writeMessage(message, new File(tempDir, "testReplacePFX.eml"));

        /*
         * Check if the PFX has really been replaced
         */
        byte[] newPFX = getPFX(message);
        
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        keyStore.load(new ByteArrayInputStream(newPFX), "test".toCharArray());

        assertEquals(22, keyStore.size());
    }
    
    @Test
    public void testReplacePFXMissingMarker()
    throws Exception
    {
        byte[] pfx = IOUtils.toByteArray(new FileInputStream(testPFX));
        
        PFXMailBuilder builder = new PFXMailBuilder(IOUtils.toString(
                new FileInputStream("test/resources/templates/mail-pfx-no-marker.ftl")), templateBuilder);
        
        String from = "123@test.com";
        
        builder.setFrom(new InternetAddress(from, "test user"));
        builder.setPFX(pfx);
        builder.addProperty("test", "new value");
        
        MimeMessage message = builder.createMessage();
        
        assertNotNull(message);
        
        MailUtils.writeMessage(message, new File(tempDir, "testReplacePFXMissingMarker.eml"));

        assertEquals("new value", message.getHeader("X-TEST", ","));
        
        /*
         * Check if the PFX has really been replaced
         */
        byte[] newPFX = getPFX(message);
        
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        keyStore.load(new ByteArrayInputStream(newPFX), "test".toCharArray());

        assertEquals(22, keyStore.size());
    }    
}
