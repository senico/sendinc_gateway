/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.application.djigzo.mail.MailContainer;
import mitm.common.mail.MailUtils;
import mitm.common.util.HexUtils;

import org.apache.commons.lang.SerializationUtils;
import org.apache.james.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.junit.Test;

public class MailContainerTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    @Test
    public void testSerialize()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");
        
        Mail mail = new MailImpl("test", new MailAddress("sender@example.com"), 
                Arrays.asList(new MailAddress("test1@example.com"), new MailAddress("test1@example.com")), message);
        
        mail.setAttribute("test1", "1234");
        mail.setAttribute("test2", "5678");
        
        MailContainer container = new MailContainer(mail);

        assertNotNull(container.getMail().getMessage());
        
        byte[] serialized = SerializationUtils.serialize(container);
        
        MailContainer deserialized = (MailContainer) SerializationUtils.deserialize(serialized);
        
        assertEquals(mail.getRecipients(), deserialized.getMail().getRecipients());
        assertEquals(mail.getSender(), deserialized.getMail().getSender());
        assertEquals(mail.getState(), deserialized.getMail().getState());
        assertEquals(mail.getErrorMessage(), deserialized.getMail().getErrorMessage());
        assertEquals(mail.getName(), deserialized.getMail().getName());
        assertEquals(mail.getRemoteHost(), deserialized.getMail().getRemoteHost());
        assertEquals(mail.getRemoteAddr(), deserialized.getMail().getRemoteAddr());
        assertEquals("1234", deserialized.getMail().getAttribute("test1"));
        assertEquals("5678", deserialized.getMail().getAttribute("test2"));
        // the message should not be serialized
        assertNull(deserialized.getMail().getMessage());
    }
    
    @Test
    public void testDeserialized()
    throws Exception
    {
        String hex = "ACED00057372002A6D69746D2E6170706C69636174696F6E2E646A69677A6F2E6D61696C2E4D61696C436F6E74616" +
        		"96E657200000000000000010300014C00046D61696C7400184C6F72672F6170616368652F6D61696C65742F4D61696C3B7" +
        		"870770800000000000000017372001E6F72672E6170616368652E6A616D65732E636F72652E4D61696C496D706CC4780DE" +
        		"5BCCFDDAC03000A4C000A617474726962757465737400134C6A6176612F7574696C2F486173684D61703B4C000C6572726" +
        		"F724D6573736167657400124C6A6176612F6C616E672F537472696E673B4C000B6C617374557064617465647400104C6A6" +
        		"176612F7574696C2F446174653B4C00076D6573736167657400214C6A617661782F6D61696C2F696E7465726E65742F4D6" +
        		"96D654D6573736167653B4C00046E616D6571007E00054C000A726563697069656E74737400164C6A6176612F7574696C2" +
        		"F436F6C6C656374696F6E3B4C000A72656D6F74654164647271007E00054C000A72656D6F7465486F737471007E00054C0" +
        		"00673656E64657274001F4C6F72672F6170616368652F6D61696C65742F4D61696C416464726573733B4C0005737461746" +
        		"571007E000578707372001D6F72672E6170616368652E6D61696C65742E4D61696C41646472657373269192846DC77BA40" +
        		"20003490003706F734C0004686F737471007E00054C00047573657271007E000578700000001274000B6578616D706C652" +
        		"E636F6D74000673656E646572737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000" +
        		"473697A6578700000000277040000000A7371007E000B0000001174000B6578616D706C652E636F6D74000574657374317" +
        		"371007E000B0000001174000B6578616D706C652E636F6D740005746573743178740004726F6F747074000474657374740" +
        		"0096C6F63616C686F73747400093132372E302E302E317372000E6A6176612E7574696C2E44617465686A81014B5974190" +
        		"30000787077080000012D94980CFF78737200116A6176612E7574696C2E486173684D61700507DAC1C31660D1030002460" +
        		"00A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000574657" +
        		"3743174000431323334740005746573743274000435363738787878";

        MailContainer deserialized = (MailContainer) SerializationUtils.deserialize(HexUtils.hexDecode(hex));
        
        assertEquals(Arrays.asList(new MailAddress("test1@example.com"), new MailAddress("test1@example.com")),
                deserialized.getMail().getRecipients());
        assertEquals(new MailAddress("sender@example.com"), deserialized.getMail().getSender());
        assertEquals("root", deserialized.getMail().getState());
        assertEquals(null, deserialized.getMail().getErrorMessage());
        assertEquals("test", deserialized.getMail().getName());
        assertEquals("localhost", deserialized.getMail().getRemoteHost());
        assertEquals("127.0.0.1", deserialized.getMail().getRemoteAddr());
        assertEquals("1234", deserialized.getMail().getAttribute("test1"));
        assertEquals("5678", deserialized.getMail().getAttribute("test2"));
        // the message should not be serialized
        assertNull(deserialized.getMail().getMessage());
    }
}
