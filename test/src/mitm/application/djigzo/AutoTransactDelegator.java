/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.application.djigzo;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;

import mitm.application.djigzo.service.SystemServices;
import mitm.application.djigzo.workflow.UserWorkflow;
import mitm.application.djigzo.workflow.UserWorkflow.GetUserMode;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.CertificateAlreadyExistsException;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.ctl.CTL;
import mitm.common.security.ctl.CTLDAO;
import mitm.common.security.ctl.CTLEntry;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.common.security.ctl.CTLException;
import mitm.common.security.ctl.CTLManager;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;

public class AutoTransactDelegator
{
	public AutoTransactDelegator() {
		// required by AutoCommitProxyFactory
	}
	
    @StartTransaction
    public User addUser(String email) 
    throws AddressException, HierarchicalPropertiesException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

        User user = userWorkflow.addUser(email);

        userWorkflow.makePersistent(user);
        
        return user;
    }

    @StartTransaction
    public User getUser(String email) 
    throws AddressException, HierarchicalPropertiesException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

        User user = userWorkflow.getUser(email, GetUserMode.NULL_IF_NOT_EXIST);

        return user;
    }
        
    public boolean isUser(String email) 
    throws AddressException, HierarchicalPropertiesException
    {
        return getUser(email)!= null;
    }

    @StartTransaction
    public int getUserCount() 
    throws AddressException, HierarchicalPropertiesException
    {
        return SystemServices.getUserWorkflow().getUserCount();
    }
    
    @StartTransaction
    public void addUserCertificate(String email, X509Certificate certificate)
    throws AddressException, HierarchicalPropertiesException
    {
        User user = getUser(email);
    
        if (user == null) {
            user = addUser(email);
        }
        
        user.getUserPreferences().getCertificates().add(certificate);
    }

    @StartTransaction
    public void addDomainCertificate(String domain, X509Certificate certificate)
    throws AddressException, HierarchicalPropertiesException, CloseableIteratorException
    {
        UserPreferences domainPrefs = SystemServices.getDomainManager().getDomainPreferences(domain);
    
        if (domainPrefs == null) {
            domainPrefs = SystemServices.getDomainManager().addDomain(domain);
        }
                
        domainPrefs.getCertificates().add(certificate);
    }

    @StartTransaction
    public void setUserSigningKeyAndCertificate(String email, KeyAndCertificate keyAndCertificate)
    throws AddressException, HierarchicalPropertiesException, CertStoreException, KeyStoreException
    {
        User user = getUser(email);
        
        if (user == null) {
            user = addUser(email);
        }
        
        user.setSigningKeyAndCertificate(keyAndCertificate);
    }
    
    @StartTransaction
    public void setProperty(String email, String propertyName, String value) 
    throws AddressException, HierarchicalPropertiesException
    {
        this.setProperty(email, propertyName, value, false);
    }
    
	@StartTransaction
	public void setProperty(String email, String propertyName, String value, boolean encrypt) 
	throws AddressException, HierarchicalPropertiesException
	{
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
        
        User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);

        UserProperties userProperties = user.getUserPreferences().getProperties();

        userProperties.setProperty(propertyName, value, encrypt);
        
        userWorkflow.makePersistent(user);            
	}

    @StartTransaction
    public String getProperty(String email, String propertyName, boolean decrypt) 
    throws AddressException, HierarchicalPropertiesException
    {
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();
        
        User user = userWorkflow.getUser(email, UserWorkflow.GetUserMode.CREATE_IF_NOT_EXIST);

        UserProperties userProperties = user.getUserPreferences().getProperties();

        return userProperties.getProperty(propertyName, decrypt);
    }
	
	@StartTransaction
	public void deleteAllUsers() 
	throws AddressException, CloseableIteratorException, HierarchicalPropertiesException
	{
        UserWorkflow userWorkflow = SystemServices.getUserWorkflow();

        Set<User> users = userWorkflow.getUsers(null, null, SortDirection.ASC); 
        
        for (User user : users)
        {
        	userWorkflow.deleteUser(user);
        }
	}

    @StartTransaction
    public void setDomainProperty(String domain, String propertyName, String value) 
    throws AddressException, HierarchicalPropertiesException, CloseableIteratorException
    {
        this.setDomainProperty(domain, propertyName, value, false);
    }
    
    @StartTransaction
    public void setDomainProperty(String domain, String propertyName, String value, boolean encrypt) 
    throws AddressException, HierarchicalPropertiesException, CloseableIteratorException
    {
        DomainManager manager = SystemServices.getDomainManager();

        UserPreferences preferences = manager.getDomainPreferences(domain);
        
        if (preferences == null) {
            preferences = manager.addDomain(domain);
        }
        
        UserProperties properties = preferences.getProperties();

        properties.setProperty(propertyName, value, encrypt);
    }
	
    @StartTransaction
    public void deleteAllDomains()
    throws CloseableIteratorException 
    {
        DomainManager manager = SystemServices.getDomainManager();
        
        CloseableIterator<String> domainIterator = manager.getDomainIterator();
        
        try {
            while (domainIterator.hasNext())
            {
                String domain = domainIterator.next();
                
                manager.deleteDomainPreferences(manager.getDomainPreferences(domain));
            }
        } 
        finally {
            domainIterator.close();
        }
    }

    @StartTransaction
    public void setGlobalProperty(String propertyName, String value) 
    throws HierarchicalPropertiesException 
    {
        this.setGlobalProperty(propertyName, value, false);
    }
    
    @StartTransaction
    public void setGlobalProperty(String propertyName, String value, boolean encrypt) 
    throws HierarchicalPropertiesException 
    {
        GlobalPreferencesManager manager = SystemServices.getGlobalPreferencesManager();

        UserPreferences preferences = manager.getGlobalUserPreferences();
        
        UserProperties properties = preferences.getProperties();

        properties.setProperty(propertyName, value, encrypt);
    }

    @StartTransaction
    public void syncFactoryProperties()
    throws HierarchicalPropertiesException 
    {
        SystemServices.getGlobalPreferencesManager().syncFactoryProperties();
    }    
    
    @StartTransaction
    public void cleanKeyAndCertStore()
    throws CertStoreException 
    {
        KeyAndCertStore store = SystemServices.getKeyAndCertStore();

        store.removeAllEntries();
    }

    @StartTransaction
    public void cleanKeyAndRootStore()
    throws CertStoreException 
    {
        KeyAndCertStore store = SystemServices.getKeyAndRootCertStore();

        store.removeAllEntries();
    }

    @StartTransaction
    public void cleanCRLStore()
    throws CRLStoreException
    {
        SystemServices.getPKISecurityServices().getCRLStore().removeAllEntries();
    }
    
    @StartTransaction
    public void addCertificate(X509Certificate certificate)
    throws CertificateAlreadyExistsException, CertStoreException
    {
        KeyAndCertStore store = SystemServices.getKeyAndCertStore();
        
        if (!store.contains(certificate)) {
            store.addCertificate(certificate);
        }
    }

    @StartTransaction
    public void addCertificates(Collection<X509Certificate> certificates)
    throws CertificateAlreadyExistsException, CertStoreException
    {
        KeyAndCertStore store = SystemServices.getKeyAndCertStore();
        
        for (X509Certificate certificate : certificates)
        {
            if (!store.contains(certificate)) {
                store.addCertificate(certificate);
            }
        }
    }
    
    @StartTransaction
    public void addRootCertificate(X509Certificate certificate)
    throws CertificateAlreadyExistsException, CertStoreException
    {
        KeyAndCertStore store = SystemServices.getKeyAndRootCertStore();
        
        if (!store.contains(certificate)) {
            store.addCertificate(certificate);
        }
    }

    @StartTransaction
    public void addRootCertificates(Collection<X509Certificate> certificates)
    throws CertificateAlreadyExistsException, CertStoreException
    {
        KeyAndCertStore store = SystemServices.getKeyAndRootCertStore();
        
        for (X509Certificate certificate : certificates)
        {
            if (!store.contains(certificate)) {
                store.addCertificate(certificate);
            }
        }
    }

    @StartTransaction
    public void addKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        SystemServices.getKeyAndCertStore().addKeyAndCertificate(keyAndCertificate);
    }

    @StartTransaction
    public void addRootKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        SystemServices.getKeyAndRootCertStore().addKeyAndCertificate(keyAndCertificate);
    }
    
    @StartTransaction
    public List<KeyAndCertificate> getKeyAndCertificates(CertSelector certSelector)
    throws CertStoreException, CloseableIteratorException, KeyStoreException
    {
        KeyAndCertStore store = SystemServices.getKeyAndCertStore();

        List<? extends X509CertStoreEntry> entries = CloseableIteratorUtils.toList(store.getCertStoreIterator(
                certSelector, MissingKeyAlias.ALLOWED, null, null));
        
        List<KeyAndCertificate> keyAndCertificates = new LinkedList<KeyAndCertificate>();
        
        for (X509CertStoreEntry entry : entries) {
            keyAndCertificates.add(store.getKeyAndCertificate(entry));
        }
        
        return keyAndCertificates;
    }
    
    @StartTransaction
    public long getKeyAndCertStoreSize() {
        return SystemServices.getKeyAndCertStore().size();
    }

    @StartTransaction
    public long getRootKeyAndCertStoreSize() {
        return SystemServices.getKeyAndRootCertStore().size();
    }

    @StartTransaction
    public void addToCTL(X509Certificate certificate, CTLEntryStatus status, boolean allowExpired)
    throws CTLException, CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        addToCTL(X509CertificateInspector.getThumbprint(certificate), status, allowExpired);
    }

    @StartTransaction
    public void addToCTL(String thumbprint, CTLEntryStatus status, boolean allowExpired)
    throws CTLException
    {
        CTL ctl = SystemServices.getCTLManager().getCTL(CTLManager.DEFAULT_CTL);
        
        CTLEntry ctlEntry = ctl.createEntry(thumbprint);
        
        ctlEntry.setStatus(status);
        ctlEntry.setAllowExpired(allowExpired);
        
        ctl.addEntry(ctlEntry);
    }
    
    @StartTransaction
    public void cleanCTL()
    {
        new CTLDAO(SessionAdapterFactory.create(SystemServices.getSessionManager().getSession()), null).deleteAll();
    }
    
	public static AutoTransactDelegator createProxy()
	throws ProxyFactoryException, NoSuchMethodException
	{
        SessionManager sessionManager = SystemServices.getSessionManager();

        AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
    			AutoTransactDelegator.class, sessionManager).createProxy();
        
        return autoTransactDelegator;
	}

	public static <T> T createProxy(Class<T> classOfT)
	throws ProxyFactoryException, NoSuchMethodException
	{
        SessionManager sessionManager = SystemServices.getSessionManager();

        T autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<T>(
        		classOfT, sessionManager).createProxy();
        
        return autoTransactDelegator;
	}
}
