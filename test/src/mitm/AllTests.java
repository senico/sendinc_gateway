/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
 
@RunWith(Suite.class)
@Suite.SuiteClasses({
    mitm.application.djigzo.admin.hibernate.AllTests.class,
    mitm.application.djigzo.ca.AllTests.class,
    mitm.application.djigzo.dlp.AllTests.class,
    mitm.application.djigzo.impl.AllTests.class,
    mitm.application.djigzo.impl.hibernate.AllTests.class,
    mitm.application.djigzo.james.AllTests.class,
    mitm.application.djigzo.james.mailets.AllTests.class,
    mitm.application.djigzo.james.matchers.AllTests.class,
    mitm.application.djigzo.mail.AllTests.class,
    mitm.application.djigzo.mail.repository.AllTests.class,
    mitm.application.djigzo.relay.AllTests.class,
    mitm.application.djigzo.workflow.impl.AllTests.class,
    mitm.application.djigzo.ws.AllTests.class,
    mitm.application.djigzo.xml.AllTests.class,
    mitm.common.cache.AllTests.class,
    mitm.common.charset.AllTests.class,
    mitm.common.cifs.AllTests.class,
    mitm.common.dlp.impl.AllTests.class,
    mitm.common.dlp.impl.matchfilter.AllTests.class,
    mitm.common.extractor.impl.AllTests.class,
    mitm.common.fetchmail.AllTests.class,
    mitm.common.hibernate.AllTests.class,
    mitm.common.mail.AllTests.class,
    mitm.common.mail.filter.AllTests.class,
    mitm.common.mail.handler.AllTests.class,
    mitm.common.mail.repository.AllTests.class,
    mitm.common.mail.repository.hibernate.AllTests.class,
    mitm.common.net.AllTests.class,
    mitm.common.pdf.AllTests.class,
    mitm.common.postfix.AllTests.class,
    mitm.common.properties.AllTests.class,
    mitm.common.properties.hibernate.AllTests.class,
    mitm.common.scheduler.AllTests.class,
    mitm.common.security.AllTests.class,
    mitm.common.security.asn1.AllTests.class,
    mitm.common.security.ca.AllTests.class,
    mitm.common.security.ca.handlers.comodo.AllTests.class,
    mitm.common.security.ca.hibernate.AllTests.class,    
    mitm.common.security.certificate.AllTests.class,
    mitm.common.security.certificate.impl.AllTests.class,
    mitm.common.security.certificate.validator.AllTests.class,
    mitm.common.security.certpath.AllTests.class,
    mitm.common.security.certstore.AllTests.class,
    mitm.common.security.certstore.dao.AllTests.class,
    mitm.common.security.certstore.hibernate.AllTests.class,
    mitm.common.security.certstore.jce.AllTests.class,
    mitm.common.security.cms.AllTests.class,
    mitm.common.security.crl.AllTests.class,
    mitm.common.security.crlstore.hibernate.AllTests.class,
    mitm.common.security.crypto.AllTests.class,
    mitm.common.security.ctl.AllTests.class,
    mitm.common.security.digest.AllTests.class,
    mitm.common.security.keystore.AllTests.class,
    mitm.common.security.keystore.dao.AllTests.class,
    mitm.common.security.keystore.hibernate.AllTests.class,    
    mitm.common.security.otp.AllTests.class,
    mitm.common.security.password.AllTests.class,
    mitm.common.security.provider.AllTests.class,
    mitm.common.security.smime.AllTests.class,
    mitm.common.security.smime.handler.AllTests.class,
    mitm.common.security.smime.selector.AllTests.class,
    mitm.common.sms.AllTests.class,
    mitm.common.sms.hibernate.AllTests.class,
    mitm.common.template.AllTests.class,
    mitm.common.tools.AllTests.class,
    mitm.common.util.AllTests.class
})
public class AllTests {
    // the class remains completely empty, 
    // being used only as a holder for the above annotations
}