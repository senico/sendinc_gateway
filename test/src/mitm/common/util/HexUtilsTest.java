/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.DecoderException;
import org.junit.Test;

public class HexUtilsTest
{
    @Test
    public void testIsHex()
    {
        assertTrue(HexUtils.isHex("F"));
        assertTrue(HexUtils.isHex("    AF  "));
        assertTrue(HexUtils.isHex("1"));
        assertTrue(HexUtils.isHex("1F"));
        assertFalse(HexUtils.isHex(""));
        assertFalse(HexUtils.isHex(null));
        assertFalse(HexUtils.isHex("  "));
        assertFalse(HexUtils.isHex("XXX"));
        assertFalse(HexUtils.isHex("F F"));
    }
    
    @Test
    public void testHexEncode()
    {
        assertEquals("FF", HexUtils.hexEncode(new byte[]{(byte)255}));
        assertEquals(null, HexUtils.hexEncode(null));
        assertEquals("", HexUtils.hexEncode(new byte[]{}));
        assertEquals("xxx", HexUtils.hexEncode(null, "xxx"));
    }
    
    @Test
    public void testHexDecode() 
    throws DecoderException
    {
        assertArrayEquals(new byte[]{(byte)255}, HexUtils.hexDecode("FF"));
        assertArrayEquals(new byte[]{(byte)255}, HexUtils.hexDecode("ff"));
        assertArrayEquals(new byte[]{(byte)255}, HexUtils.hexDecode("Ff"));
        assertArrayEquals(null, HexUtils.hexDecode(null));
        assertArrayEquals(new byte[]{(byte)123}, new byte[]{(byte)123});
    }
}
