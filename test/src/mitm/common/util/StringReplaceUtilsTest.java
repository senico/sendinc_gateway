/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class StringReplaceUtilsTest
{
    @Test
    public void testReplaceTabsWithSpaces()
    {
        assertEquals("    ", StringReplaceUtils.replaceTabsWithSpaces("\t", 4));
        assertEquals(" a   b ", StringReplaceUtils.replaceTabsWithSpaces(" a\tb ", 3));
    }

    @Test
    public void testReplaceNonXML()
    {
        assertEquals("xx", StringReplaceUtils.replaceNonXML("\u000c", "xx"));
        assertEquals(" test 123 ", StringReplaceUtils.replaceNonXML(" test 123 ", "xx"));
        assertEquals(" test\r\t\n123 ", StringReplaceUtils.replaceNonXML(" test\r\t\n123 ", "xx"));
        assertEquals(" test\rxx\t\n123 ", StringReplaceUtils.replaceNonXML(" test\r\b\t\n123 ", "xx"));
        assertNull(StringReplaceUtils.replaceNonXML((String)null, ""));
        assertEquals(" test\r\t\n123 ", StringReplaceUtils.replaceNonXML(" test\r\b\t\n123 ", ""));
        assertEquals("test\u00FFunicode", StringReplaceUtils.replaceNonXML("test\u00FFunicode", "xx"));
        assertEquals("testxxunicode", StringReplaceUtils.replaceNonXML("test\uEFFFunicode", "xx"));
        assertEquals("\t\r\n 12abyzABYZ!@#$%&*()+=\\|~", StringReplaceUtils.replaceNonXML("\t\r\n 12abyzABYZ!@#$%&*()+=\\|~", ""));
        assertEquals("xxaxxbxxc", StringReplaceUtils.replaceNonXML("\ba\bb\bc", "xx"));
    }

    @Test
    public void testReplaceNonXMLList()
    {
        List<String> lines = new LinkedList<String>();
        
        lines.add("123");
        lines.add("test\uEFFFunicode");
        
        assertEquals(2, lines.size());

        lines = StringReplaceUtils.replaceNonXML(lines, "#");
        
        assertEquals(2, lines.size());
        assertEquals("123", lines.get(0));
        assertEquals("test#unicode", lines.get(1));
    }

    @Test
    public void testReplaceNonXMLSet()
    {
        Set<String> lines = new LinkedHashSet<String>();
        
        lines.add("123");
        lines.add("test\uEFFFunicode");
        
        assertEquals(2, lines.size());

        lines = StringReplaceUtils.replaceNonXML(lines, "#");
        
        assertEquals(2, lines.size());
        
        Iterator<String> it = lines.iterator();
        
        assertEquals("123", it.next());
        assertEquals("test#unicode", it.next());
    }
    
    @Test
    public void testReplaceNonASCII()
    {
        assertEquals("ab", StringReplaceUtils.replaceNonASCII("ab", "xx"));
        assertEquals("axxb", StringReplaceUtils.replaceNonASCII("a\u00AAb", "xx"));
        assertEquals("axxbxx", StringReplaceUtils.replaceNonASCII("a\u00AAb\u00FF", "xx"));
    }
}
