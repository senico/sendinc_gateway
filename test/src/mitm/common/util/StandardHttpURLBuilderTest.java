/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import mitm.common.security.SecurityFactoryFactory;

import org.junit.Test;

public class StandardHttpURLBuilderTest
{
	@Test
	public void testHttpURLBuilder() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com/");
		
		builder.addParameter("param", "one");
		
		String url = builder.buildURL();
		
		assertEquals("http://example.com?param=one", url);
	}

	@Test
	public void testHttpURLBuilderSpaceInName() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com/");
		
		builder.addParameter("param ", "one");
		
		String url = builder.buildURL();
		
		assertEquals("http://example.com?param=one", url);
	}
	
	@Test
	public void testHttpURLBuilderSpace() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com/");
		
		builder.addParameter("param", "one two");
		
		String url = builder.buildURL();
		
		assertEquals("http://example.com?param=one+two", url);
	}
	
	@Test
	public void testHttpURLBuilderNoParameters() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com/");
		
		String url = builder.buildURL();
		
		assertEquals("http://example.com", url);
	}
	
	@Test
	public void testHttpURLBuilderHMac() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com");
		
		builder.addParameter("param1", "one");
		builder.addParameter("param2", "two");
		builder.addParameter("param3", "*&/%?");
		
		Mac mac = SecurityFactoryFactory.getSecurityFactory().createMAC("HmacSHA256");
		
		assertNotNull(mac);
		
		SecretKeySpec key = new SecretKeySpec(new byte[]{1, 2, 3}, "hmac");
		
		mac.init(key);
		
		builder.addHMAC("mac", mac);
		
		String url = builder.buildURL();
		
		assertEquals("http://example.com?param1=one&param2=two&param3=*%26%2F%25%3F&mac=xi3htpn4j3efs3msgl4avqopuxjqp6vz2l4dt4sfqj75moiqzmpa", url);
	}
	
	@Test
	public void testHttpURLBuilderBuildQuery() 
	throws Exception
	{
		URLBuilder builder = new StandardHttpURLBuilder();
		
		builder.setBaseURL("http://example.com");
		
		builder.addParameter("param1", "one");
		builder.addParameter("param2", "two");
		builder.addParameter("param3", "*&/%?");
		
		Mac mac = SecurityFactoryFactory.getSecurityFactory().createMAC("HmacSHA256");
		
		assertNotNull(mac);
		
		SecretKeySpec key = new SecretKeySpec(new byte[]{1, 2, 3}, "hmac");
		
		mac.init(key);
		
		builder.addHMAC("mac", mac);
		
		String query = builder.buildQueury();
		
		assertEquals("param1=one&param2=two&param3=*%26%2F%25%3F&mac=xi3htpn4j3efs3msgl4avqopuxjqp6vz2l4dt4sfqj75moiqzmpa", query);
	}	
}
