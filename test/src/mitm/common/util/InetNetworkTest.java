package mitm.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;

import org.junit.Test;


public class InetNetworkTest
{
	@Test
	public void testMask()
	throws UnknownHostException
	{
		InetNetwork network = InetNetwork.getFromString("192.168.*");
		
		assertTrue(network.contains("192.168.2.2"));
		assertFalse(network.contains("192.169.2.2"));
	}

	@Test
	public void testAcceptAll()
	throws UnknownHostException
	{
		InetNetwork network = InetNetwork.getFromString("*");
		
		assertTrue(network.contains("192.168.2.2"));
		assertTrue(network.contains("192.169.2.2"));
		assertTrue(network.contains("1.2.3.4"));
	}
	
	@Test
	public void testCIDR()
	throws UnknownHostException
	{
		InetNetwork network = InetNetwork.getFromString("192.168.1.0/24");
		
		assertTrue(network.contains("192.168.1.2"));
		assertFalse(network.contains("192.168.9.2"));
	}

	@Test
	public void testSingleIP()
	throws UnknownHostException
	{
		InetNetwork network = InetNetwork.getFromString("127.1.1.1");
		
		assertTrue(network.contains("127.1.1.1"));
		assertFalse(network.contains("127.1.1.2"));
	}
}
