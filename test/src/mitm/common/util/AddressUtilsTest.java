/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AddressUtilsTest
{
	@Test
	public void testIsIPv4Address()
	{
        assertTrue(AddressUtils.isValidIPv4Address("192.168.0.1"));
        assertTrue(AddressUtils.isValidIPv4Address("1.1.1.1"));
        assertTrue(AddressUtils.isValidIPv4Address("0.0.0.0"));
		assertTrue(AddressUtils.isValidIPv4Address("255.255.255.255"));
		assertFalse(AddressUtils.isValidIPv4Address("xx"));
		assertFalse(AddressUtils.isValidIPv4Address(null));
		assertFalse(AddressUtils.isValidIPv4Address("google.com"));
	}

	@Test
	public void testIsIPv6Address()
	{
		assertTrue(AddressUtils.isValidIPv6Address("2001:0db8:85a3:0000:0000:8a2e:0370:7334"));
		assertTrue(AddressUtils.isValidIPv6Address("2001:0db8:85a3:0000:0000:8a2e:0370:7334"));
		assertTrue(AddressUtils.isValidIPv6Address("2001:db8:85a3:0:0:8a2e:370:7334"));
		assertTrue(AddressUtils.isValidIPv6Address("2001:db8:85a3::8a2e:370:7334"));
		assertTrue(AddressUtils.isValidIPv6Address("0000:0000:0000:0000:0000:0000:0000:0001"));
		assertTrue(AddressUtils.isValidIPv6Address("::1"));
		assertFalse(AddressUtils.isValidIPv6Address("xx"));
	}
	
	@Test
	public void testIsValidNetwork()
	{
		assertTrue(AddressUtils.isValidNetwork("2001:0db8:85a3:0000:0000:8a2e:0370:7334"));
		assertTrue(AddressUtils.isValidNetwork("[2001:0db8:85a3:0000:0000:8a2e:0370:7334]/128"));
		assertTrue(AddressUtils.isValidNetwork("2001:0db8:85a3:0000:0000:8a2e:0370:7334/10"));
		assertTrue(AddressUtils.isValidNetwork("2001:0db8:85a3:0000:0000:8a2e:0370:7334/10  "));
		assertTrue(AddressUtils.isValidNetwork("192.168.0.1"));
		assertTrue(AddressUtils.isValidNetwork("192.168.0.1/8"));
		assertFalse(AddressUtils.isValidNetwork("192.168.0.1/"));
		assertFalse(AddressUtils.isValidNetwork("192.168.0.1/aa"));
		assertFalse(AddressUtils.isValidNetwork("XX/aa"));
		assertFalse(AddressUtils.isValidNetwork("2001:0db8:85a3:0000:0000:8a2e:0370:7334/"));
		assertFalse(AddressUtils.isValidNetwork("2001:0db8:85a3:0000:0000:8a2e:0370:7334/aa"));
	}
	
	/*
	 * Test for https://jira.djigzo.com/browse/GATEWAY-18
	 */
	@Test
	public void testIsValid0Network()
	{
        assertTrue(AddressUtils.isValidNetwork("0.0.0.0/0"));
	}
}
