/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MiscStringUtilsTest
{
    @Test
    public void testIsPrintableAscii()
    {
        assertTrue(MiscStringUtils.isPrintableAscii(""));
        assertTrue(MiscStringUtils.isPrintableAscii(" 12 zZ"));
        assertTrue(MiscStringUtils.isPrintableAscii(null));
        assertTrue(MiscStringUtils.isPrintableAscii("\n\r\t"));
        assertFalse(MiscStringUtils.isPrintableAscii("\0007"));
        assertFalse(MiscStringUtils.isPrintableAscii("öäüßÖÄÜ"));
    }
    
    @Test
    public void testToAscii() 
    {
        assertEquals("123", MiscStringUtils.toAscii("123"));
        assertNull(MiscStringUtils.toAscii(null));
        assertEquals("???", MiscStringUtils.toAscii("刘书洪"));
    }
    
    @Test
    public void testToAsciiBytes() 
    {
        byte[] bytes = MiscStringUtils.toAsciiBytes("ABC\n");
        
        assertArrayEquals(new byte[]{ 'A', 'B', 'C', '\n'}, bytes);
        
        assertNull(MiscStringUtils.toAsciiBytes(null));
    }

    @Test
    public void testToAsciiString() 
    {
        String s = MiscStringUtils.toAsciiString(new byte[]{ 'A', 'B', 'C', '\n'});
        
        assertEquals("ABC\n", s);

        assertNull(MiscStringUtils.toAsciiString((byte[])null));
    }

    @Test
    public void testToUTF8String() 
    {
        String s = MiscStringUtils.toUTF8String(new byte[]{ 'A', 'B', 'C', '\n'});
        
        assertEquals("ABC\n", s);

        assertNull(MiscStringUtils.toUTF8String(null));
    }
    
    @Test
    public void testRemoveControlChars()
    {
        assertNull(MiscStringUtils.removeControlChars(null));
        assertEquals("123", MiscStringUtils.removeControlChars("123"));
        assertEquals(" 123 ", MiscStringUtils.removeControlChars(" 123 "));
        assertEquals("123", MiscStringUtils.removeControlChars("\00\r1\n2\t3\00"));
        assertNull(MiscStringUtils.removeControlChars(null));
    }
    
    @Test
    public void testUnquote() 
    {
        assertEquals("test@example.com", MiscStringUtils.unquote("test@example.com", '"'));
        assertEquals("test@example.com", MiscStringUtils.unquote("'test@example.com'", '\''));
        assertEquals("test@example.com", MiscStringUtils.unquote("\"test@example.com\"", '"'));
        assertEquals("test@example.com", MiscStringUtils.unquote("#test@example.com#", '#'));
        assertEquals("\"test@example.com", MiscStringUtils.unquote("\"test@example.com", '"'));
        assertEquals("", MiscStringUtils.unquote("", '"'));
        assertNull(MiscStringUtils.unquote(null, '"'));
    }
    
    @Test
    public void testRestrictLength() 
    {
        String s = MiscStringUtils.restrictLength("123", 3);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 4);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 1);
        assertEquals("1", s);

        s = MiscStringUtils.restrictLength("123", 0);
        assertEquals("", s);

        s = MiscStringUtils.restrictLength(null, 0);
        assertEquals(null, s);
        
        s = MiscStringUtils.restrictLength("123", -1);
        assertEquals("", s);

        assertNull(MiscStringUtils.restrictLength(null, 1));
    }
    
    @Test
    public void testRestrictLengthDots() 
    {
        String s = MiscStringUtils.restrictLength("123", 3, true);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 3, false);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("1234", 3, true);
        assertEquals("...", s);

        s = MiscStringUtils.restrictLength("1234", 3, false);
        assertEquals("123", s);
        
        s = MiscStringUtils.restrictLength("12345678", 5, true);
        assertEquals("12...", s);
        
        s = MiscStringUtils.restrictLength("12", 1, true);
        assertEquals("...", s);
    }

    @Test
    public void testRestrictLengthDotsSetDots() 
    {
        String s = MiscStringUtils.restrictLength("1234", 3, true, "***");
        assertEquals("***", s);

        s = MiscStringUtils.restrictLength("12345678", 5, true, "**");
        assertEquals("12**", s);
        
        s = MiscStringUtils.restrictLength("12", 1, true, "long string");
        assertEquals("long string", s);

        s = MiscStringUtils.restrictLength("1234", 3, true, null);
        assertEquals("", s);
    }
    
    @Test
    public void testEnsureEndsWith()
    {
    	assertEquals("test/", MiscStringUtils.ensureEndsWith("test/", "/"));
    	assertEquals("test/", MiscStringUtils.ensureEndsWith("test", "/"));
    	assertEquals("123", MiscStringUtils.ensureEndsWith(null, "123"));
    }
    
    @Test
    public void testReplaceLastChars() 
    {
        String s = MiscStringUtils.replaceLastChars("abc123", 3, "mn");        
        assertEquals("abcmnmnmn", s);

        s = MiscStringUtils.replaceLastChars("abc123", 4, "*");        
        assertEquals("ab****", s);

        s = MiscStringUtils.replaceLastChars("abc123", 100, "*");        
        assertEquals("******", s);

        s = MiscStringUtils.replaceLastChars("abc123", 1, null);        
        assertEquals("abc12", s);

        s = MiscStringUtils.replaceLastChars(null, 1, null);        
        assertEquals("", s);
        
        s = MiscStringUtils.replaceLastChars("abc123", 0, "**");        
        assertEquals("abc123", s);

        s = MiscStringUtils.replaceLastChars("abc123", -100, "**");        
        assertEquals("abc123", s);

        s = MiscStringUtils.replaceLastChars("", 100, "**");        
        assertEquals("", s);
    }
}
