package mitm.common.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class MiscFilenameUtilsTest
{
    @Test
    public void testRemoveIllegalFilenameChars()
    {
        assertEquals("test", MiscFilenameUtils.removeIllegalFilenameChars("test"));
        assertEquals("test.123.456", MiscFilenameUtils.removeIllegalFilenameChars("test.123.456"));
        assertEquals("t.A es_t-123", MiscFilenameUtils.removeIllegalFilenameChars("t.A es_t-123"));
        assertEquals("123456", MiscFilenameUtils.removeIllegalFilenameChars("123$4/5\\6"));
    }
    
    @Test
    public void testReplaceIllegalFilenameChars()
    {
        assertEquals("test", MiscFilenameUtils.replaceIllegalFilenameChars("test", '_'));
        assertEquals("test.123.456", MiscFilenameUtils.replaceIllegalFilenameChars("test.123.456", '_'));
        assertEquals("t.A es_t-123", MiscFilenameUtils.replaceIllegalFilenameChars("t.A es_t-123", '_'));
        assertEquals("123?4?5?6", MiscFilenameUtils.replaceIllegalFilenameChars("123$4/5\\6", '?'));
        assertEquals("123_4_5_6", MiscFilenameUtils.replaceIllegalFilenameChars("123$4/5\\6", '_'));
    }    
}
