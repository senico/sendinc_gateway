/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class DateTimeUtilsTest
{
    @Test
    public void testMillisecondsToSeconds()
    {
        assertEquals(1, DateTimeUtils.millisecondsToSeconds(1000));
        assertEquals(1, DateTimeUtils.millisecondsToSeconds(1001));
        assertEquals(0, DateTimeUtils.millisecondsToSeconds(999));
        assertEquals(0, DateTimeUtils.millisecondsToSeconds(0));
        assertEquals(0, DateTimeUtils.millisecondsToSeconds(-1));
        assertEquals(1, (long)DateTimeUtils.millisecondsToSeconds(new Long(1000)));
        assertEquals(null, DateTimeUtils.millisecondsToSeconds(null));
    }
    
    @Test
    public void testMillisecondsToMinutes()
    {
        assertEquals(0, DateTimeUtils.millisecondsToMinutes(1000));
        assertEquals(1, DateTimeUtils.millisecondsToMinutes(60000));
        assertEquals(0, DateTimeUtils.millisecondsToMinutes(59999));
        assertEquals(10, DateTimeUtils.millisecondsToMinutes(600000));
        assertEquals(0, DateTimeUtils.millisecondsToMinutes(-1));
        assertEquals(0, DateTimeUtils.millisecondsToMinutes(0));
        assertEquals(1, (long)DateTimeUtils.millisecondsToMinutes(new Long(60000)));
        assertEquals(null, DateTimeUtils.millisecondsToMinutes(null));
    }
    @Test
    public void testMinutesToMilliseconds()
    {
        assertEquals(60000, DateTimeUtils.minutesToMilliseconds(1));
        assertEquals(120000, DateTimeUtils.minutesToMilliseconds(2));
        assertEquals(0, DateTimeUtils.minutesToMilliseconds(0));
        assertEquals(-60000, DateTimeUtils.minutesToMilliseconds(-1));
        assertEquals(120000, (long)DateTimeUtils.minutesToMilliseconds(new Long(2)));
        assertEquals(null, DateTimeUtils.minutesToMilliseconds(null));
    }
}
