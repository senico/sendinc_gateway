/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.junit.Test;

public class PageReaderTest 
{
	@Test
	public void testPage() 
	throws IOException
	{
		String input = "0\n1\n2";
		
		PageReader pageReader = new PageReader(new StringReader(input), Integer.MAX_VALUE);
		
		assertEquals(-1, pageReader.getCurrentPage());
		
		pageReader.getPage(Integer.MAX_VALUE);
		
		assertEquals(0, pageReader.getCurrentPage());
	}
	
	@Test
	public void testPageOnePage() 
	throws IOException
	{
		String input = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12";
		
		PageReader pageReader = new PageReader(new StringReader(input), Integer.MAX_VALUE);
		
		List<String> page = pageReader.getPage(0);

		assertEquals(13, page.size());

		assertEquals(0, pageReader.getCurrentPage());
	}

	@Test
	public void testStartPageTooLarge() 
	throws IOException
	{
		String input = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12";
		
		PageReader pageReader = new PageReader(new StringReader(input), 42);
		
		List<String> page = pageReader.getPage(Integer.MAX_VALUE);

		assertEquals(13, page.size());
		assertEquals(0, pageReader.getCurrentPage());
	}
	
	@Test
	public void testPageUseSamePageReader() 
	throws IOException
	{
		String input = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12";
		
		PageReader pageReader = new PageReader(new StringReader(input), 5);
		
		List<String> page = pageReader.getPage(0);

		assertEquals(5, page.size());
		
		assertEquals("0", page.get(0));
		assertEquals("1", page.get(1));
		assertEquals("2", page.get(2));
		assertEquals("3", page.get(3));
		assertEquals("4", page.get(4));

		assertEquals(0, pageReader.getCurrentPage());
		
		page = pageReader.getPage(1);

		assertEquals(5, page.size());
		
		assertEquals("5", page.get(0));
		assertEquals("6", page.get(1));
		assertEquals("7", page.get(2));
		assertEquals("8", page.get(3));
		assertEquals("9", page.get(4));

		assertEquals(1, pageReader.getCurrentPage());
		
		page = pageReader.getPage(2);

		assertEquals(3, page.size());
		
		assertEquals("10", page.get(0));
		assertEquals("11", page.get(1));
		assertEquals("12", page.get(2));
		
		assertEquals(2, pageReader.getCurrentPage());		
	}
	
	@Test
	public void testPageNewPageReader() 
	throws IOException
	{
		String input = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12";
		
		PageReader pageReader = new PageReader(new StringReader(input), 5);
		
		List<String> page = pageReader.getPage(0);

		assertEquals(5, page.size());
		
		assertEquals("0", page.get(0));
		assertEquals("1", page.get(1));
		assertEquals("2", page.get(2));
		assertEquals("3", page.get(3));
		assertEquals("4", page.get(4));
		
		assertEquals(0, pageReader.getCurrentPage());
		
		pageReader = new PageReader(new StringReader(input), 5);
		
		page = pageReader.getPage(1);

		assertEquals(5, page.size());
		
		assertEquals("5", page.get(0));
		assertEquals("6", page.get(1));
		assertEquals("7", page.get(2));
		assertEquals("8", page.get(3));
		assertEquals("9", page.get(4));
		
		assertEquals(1, pageReader.getCurrentPage());

		pageReader = new PageReader(new StringReader(input), 5);
		
		page = pageReader.getPage(2);

		assertEquals(3, page.size());
		
		assertEquals("10", page.get(0));
		assertEquals("11", page.get(1));
		assertEquals("12", page.get(2));

		assertEquals(2, pageReader.getCurrentPage());
	}
	
}
