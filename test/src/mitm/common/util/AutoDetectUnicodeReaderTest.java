/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class AutoDetectUnicodeReaderTest
{
    private InputStream getFileInputStream(String file)
    throws FileNotFoundException
    {
        return new FileInputStream("test/resources/testdata/documents/" + file);
    }
    
    private void checkText(String text, String header)
    {
        String expected = header + 
            "\r\n<note>\r\n" +
            "   <from>Santa Claus</from>\r\n" +
            "   <to>Donald Duck</to>\r\n" +
            "\r\n" +
            "   <body1>Norwegian: æø ÆØ</body1>\r\n" +
            "   <body2>French: êèé ÊÈÉ</body2>\r\n" +
            "   <body3>Finnish: åäö ÅÄÖ</body3>\r\n" +
            "   <body4>Greek: ΧΨΩ</body4>\r\n" +
            "   <body5>Cyrillic: ЖЗИ</body5>\r\n" +
            "   <body6>Arabic: فقك</body6>\r\n" +
            "   <body7>Hebrew: אבג</body7>\r\n" +
            "   <body8>Symbols: №™Ω℮</body8>\r\n" +
            "   <body9>Gujarati: ઊઋઍ</body9>\r\n" +
            "   <body10>Tamil: ௫௬௮</body10>\r\n" +
            "   <body11>Malayalam: ൪൫൬</body11>\r\n" +
            "   <body12>More symbols: ☊☋☑☒☓☢☣☮</body12>\r\n" +
            "   <body13>Arrows: ↺↻↹⇑⇗⇒⇘⇓⇙⇐⇖⇑</body13>\r\n" +
            "   <body14>Circled digits: ①②③</body14>\r\n" +
            "   <body15>Circled letters: ⒾⓉ ⓌⓄⓇⓀⓈ</body15>\r\n" +
            "   <body16>Chess: ♔♕♖♗♘♙♚♛♜♝♞♟</body16>\r\n" +
            "   <body17>Flowers: ❀❁❂❃❄❅</body17>\r\n" +
            "   <body18>Units: ㎅㎆㎇㎈㎉㎏㎐㎑㎒㎓</body18>\r\n" +
            "   <body19>CJK Ideograph: 亼亽亾仈仌</body19>\r\n" +
            "   <body20>Katakana: ズセゼソゾタ</body20>\r\n" +
            "\r\n" +
            "</note>\r\n";

        assertEquals(expected, text);
    }
    
    @Test
    public void testUTF8Bom()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("xmlUTF8.xml"));
        
        String text = IOUtils.toString(reader);

        checkText(text, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    }

    @Test
    public void testUTF8NoBom()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("xmlUTF8_NOBOM.xml"));
        
        String text = IOUtils.toString(reader);

        assertTrue(text.contains("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertTrue(text.contains("This does not work due to a missing BOM marker"));
        assertTrue(text.contains("French: êèé ÊÈÉ"));
        assertTrue(text.contains("Units: ㎅㎆㎇㎈㎉㎏㎐㎑㎒㎓"));
    }
    
    @Test
    public void testExplicitEncoding()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("xmlUTF8_NOBOM.xml"), "US-ASCII");
        
        String text = IOUtils.toString(reader);

        assertTrue(text.contains("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertTrue(text.contains("This does not work due to a missing BOM marker"));
        // this should not be found because encoding is explicitly set to US-ASCII
        assertFalse(text.contains("Units: ㎅㎆㎇㎈㎉㎏㎐㎑㎒㎓"));
    }

    @Test
    public void testUTF16BE()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("xmlUTF16BE.xml"));
        
        String text = IOUtils.toString(reader);
        
        checkText(text, "<?xml version=\"1.0\" encoding=\"UTF-16BE\"?>");
    }    

    @Test
    public void testUTF16LE()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("xmlUTF16LE.xml"));
        
        String text = IOUtils.toString(reader);
        
        checkText(text, "<?xml version=\"1.0\" encoding=\"UTF-16LE\"?>");
    }    
    
    @Test
    public void testUSASCIITextNoBOM()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("test.txt"));
        
        String text = IOUtils.toString(reader);
        
        assertEquals("this is just a text file with us-ascii characters\n", text);
    }    

    @Test
    public void testBinaryFileUnsupportedEncoding()
    throws IOException
    {
        /*
         * The first encoding found is unknown charset (IBM424_rtl). This test tests that that charset is skipped.
         */
        Reader reader = new AutoDetectUnicodeReader(getFileInputStream("testJAR.jar"));
        
        IOUtils.toString(reader);
    }    
    
    @Test
    public void testEmptyInput()
    throws IOException
    {
        Reader reader = new AutoDetectUnicodeReader(new ByteArrayInputStream(new byte[]{}));
        
        String text = IOUtils.toString(reader);
        
        assertEquals("", text);
    }        
}
