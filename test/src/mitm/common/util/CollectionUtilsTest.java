/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.junit.Test;

public class CollectionUtilsTest
{
    @Test
    public void testIsEmpty()
    {
        assertTrue(CollectionUtils.isEmpty(null));
        assertTrue(CollectionUtils.isEmpty(Collections.emptyList()));
        assertFalse(CollectionUtils.isEmpty(Collections.singleton("test")));
        assertFalse(CollectionUtils.isEmpty(Arrays.asList(1,2,3,4)));
    }

    @Test
    public void testGetSize()
    {
        assertEquals(0, CollectionUtils.getSize(null));
        assertEquals(0, CollectionUtils.getSize(Collections.emptyList()));
        assertEquals(1, CollectionUtils.getSize(Collections.singleton("test")));
        assertEquals(4, CollectionUtils.getSize(Arrays.asList(1,2,3,4)));
    }
    
    @Test
    public void testCopyCollectionFiltered()
    {
       Collection<Integer> integers = new LinkedList<Integer>();
       
       integers.add(0);
       integers.add(1);
       
       Collection<Integer> target = new LinkedList<Integer>();
       
       CollectionUtils.copyCollectionFiltered(integers, target, Integer.class);
       
       assertEquals(integers, target);
    }

    @Test
    public void testCopyCollectionFilteredNoMatch()
    {
       Collection<Integer> integers = new LinkedList<Integer>();
       
       integers.add(0);
       integers.add(1);
       
       Collection<Integer> target = new LinkedList<Integer>();
       
       CollectionUtils.copyCollectionFiltered(integers, target, Double.class);
       
       assertEquals(0, target.size());
    }

    @Test
    public void testCopyCollectionFilteredNoMatchMultipleClasses()
    {
       Collection<Integer> integers = new LinkedList<Integer>();
       
       integers.add(0);
       integers.add(1);
       
       Collection<Integer> target = new LinkedList<Integer>();
       
       CollectionUtils.copyCollectionFiltered(integers, target, Integer.class, Double.class);
       
       assertEquals(0, target.size());
    }

    @Test
    public void testCopyCollectionFilteredMatchMultipleClasses()
    {
       Collection<Integer> integers = new LinkedList<Integer>();
       
       integers.add(0);
       integers.add(1);
       
       Collection<Integer> target = new LinkedList<Integer>();
       
       CollectionUtils.copyCollectionFiltered(integers, target, Integer.class, Comparable.class);
       
       assertEquals(integers, target);
    }
    
    @Test
    public void testToStringList()
    {
        assertNotNull(CollectionUtils.toStringList(null, ""));
        assertEquals(0, CollectionUtils.toStringList(null, "").size());
        assertEquals(1, CollectionUtils.toStringList(Collections.singleton(new Integer(1)), "").size());
        assertEquals("1", CollectionUtils.toStringList(Collections.singleton(new Integer(1)), "").get(0));
    }
    
    @Test
    public void testToStringArray()
    {
        Object[] in = new Object[]{"a", "b", null, new Integer(1)};
        
        String[] out = CollectionUtils.toStringArray(Arrays.asList(in), null);
        
        assertEquals(4, out.length);
        assertEquals("a", out[0]);
        assertEquals("b", out[1]);
        assertEquals(null, out[2]);
        assertEquals("1", out[3]);
        
        in = new Object[]{null};
        
        out = CollectionUtils.toStringArray(Arrays.asList(in), "<null>");
        
        assertEquals(1, out.length);
        assertEquals("<null>", out[0]);
        
        out = CollectionUtils.toStringArray(null, "<null>");
        
        assertNull(out);
    }
}
