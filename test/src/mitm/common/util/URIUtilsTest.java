/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import mitm.common.util.URIUtils.URIType;

import org.junit.Test;

public class URIUtilsTest
{
    @Test
    public void testURLs()
    {
        assertTrue(URIUtils.URL_PATTERN.matcher("http://www.example.com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://www.example.com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://www.example.com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://www.example.com?var=value").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://www.exa-mple.com/x?").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?page=com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=c-om").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com").matches()); 
        assertTrue(URIUtils.URL_PATTERN.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com." +
        		"atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action" +
        		"_12580213").matches()); 
    }

    @Test
    public void testHttpURLs()
    {
        Pattern pattern = Pattern.compile(URIUtils.HTTP_URL_REG_EXPR);
        
        assertTrue(pattern.matcher("http://www.example.com").matches()); 
        assertTrue(pattern.matcher("http://www.example.com").matches()); 
        assertTrue(pattern.matcher("http://www.example.com").matches()); 
        assertTrue(pattern.matcher("http://www.example.com?var=value").matches()); 
        assertTrue(pattern.matcher("http://www.exa-mple.com/x?").matches()); 
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?page=com").matches()); 
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=com").matches()); 
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=c-om").matches()); 
        assertTrue(pattern.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com").matches()); 
        assertTrue(pattern.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian.jira." +
        		"plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213").matches()); 
        assertTrue(pattern.matcher("www.example.com").matches());
        assertTrue(pattern.matcher("WWW.example.com").matches());
        assertTrue(pattern.matcher("hTTps://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian.jira." +
        		"plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213").matches()); 
    }
        
    @Test
    public void testURLsNoMatch()
    {
        assertFalse(URIUtils.URL_PATTERN.matcher("http://www.example.com xxx").matches()); 
    }

    @Test
    public void testHttpNoMatch()
    {
        Pattern pattern = Pattern.compile(URIUtils.HTTP_URL_REG_EXPR);

        assertFalse(pattern.matcher("test@sub.example.com").matches()); 
        assertFalse(pattern.matcher("sub.example.com").matches()); 
        assertFalse(pattern.matcher("xxx://sub.example.com").matches()); 
    }
    
    @Test
    public void testToURI()
    throws URISyntaxException
    {
        assertNotNull(URIUtils.toURI("http://www.example.com", false)); 
        assertNotNull(URIUtils.toURI("hTTps://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian." +
                "jira.plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213", false)); 
        assertNotNull(URIUtils.toURI("http://www.example.com ", false)); 
        assertNotNull(URIUtils.toURI(" http://www.example.com", false)); 
        
        // URI contains spaces that are not encoded which is not valid
        try {
            URIUtils.toURI("ldap://dirsys.rootca.or.kr:389/CN=ROOT-RSA-CRL, OU=ROOTCA, O=KISA, C=KR", false);
            fail();
        }
        catch(URISyntaxException e) {
            // expected
        }
        
        URI uri = URIUtils.toURI("ldap://dirsys.rootca.or.kr:389/CN=ROOT-RSA-CRL, OU=ROOTCA, O=KISA, C=KR", true);
        assertEquals("ldap://dirsys.rootca.or.kr:389/CN=ROOT-RSA-CRL,%20OU=ROOTCA,%20O=KISA,%20C=KR", uri.toString());
    }
    
    @Test
    public void testURIValidatorFULL()
    {
        assertTrue(URIUtils.isValidURI("http://www.example.com", URIType.FULL));
        assertTrue(URIUtils.isValidURI("http://www.example.com:80/?", URIType.FULL));
        assertTrue(URIUtils.isValidURI("hTTps://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian." +
        		"jira.plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213", 
        		URIType.FULL));
        assertTrue(URIUtils.isValidURI("xxp://test.nl#1234", URIType.FULL));
        assertTrue(URIUtils.isValidURI("xxp://test:xx@test.nl?x=1#1234", URIType.FULL));
        assertTrue(URIUtils.isValidURI("xxp://test:xx@test.nl?x=1#1234", null));
        assertFalse(URIUtils.isValidURI("www.example.com", URIType.FULL));
        assertFalse(URIUtils.isValidURI("<script>alert(1)</script>", URIType.FULL));
        assertFalse(URIUtils.isValidURI("http://<script>", URIType.FULL));
        
        assertFalse(URIUtils.isValidURI("", URIType.BASE));
    }
    
    @Test
    public void testURIValidatorBASE()
    {
        assertTrue(URIUtils.isValidURI("http://www.example.com", URIType.BASE));
        assertTrue(URIUtils.isValidURI("http://test:123@www.example.com:80", URIType.BASE));
        assertTrue(URIUtils.isValidURI("http://test:123@www.example.com:80?", URIType.BASE));
        assertTrue(URIUtils.isValidURI("http://test:123@www.example.com:80/?", URIType.BASE));
        assertFalse(URIUtils.isValidURI("http://test:123@www.example.com:80/?a", URIType.BASE));
        assertFalse(URIUtils.isValidURI("www.example.com", URIType.BASE));
        assertFalse(URIUtils.isValidURI("http://www.example.com#123", URIType.BASE));
    }
    
    @Test
    public void testURIValidatorRELATIVE()
    {
        assertTrue(URIUtils.isValidURI("/test", URIType.RELATIVE));
        assertTrue(URIUtils.isValidURI("/test?x#10", URIType.RELATIVE));
        assertTrue(URIUtils.isValidURI("test", URIType.RELATIVE));
        assertFalse(URIUtils.isValidURI("http://www.example.com", URIType.RELATIVE));
        assertFalse(URIUtils.isValidURI("http://www.example.com?x", URIType.RELATIVE));
        assertFalse(URIUtils.isValidURI("http://www.example.com#x", URIType.RELATIVE));
        assertFalse(URIUtils.isValidURI("http://www.example.com/123", URIType.RELATIVE));
    }    
}
