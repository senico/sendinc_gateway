/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ObservableListTest
{
    private static Integer added;
    private static Integer deleted;
    private static Integer set;
    private static int currentIndex;
    
    private Collection<Integer> addedItems;
    private Collection<Integer> deletedItems;
    private Collection<Integer> setItems;
    
    private List<Integer> integerList;
    private ObservableList<Integer> observableList;
    
    
    private class AddEvent implements ObservableList.Event<Integer> 
    {
        @Override
        public boolean event(int index, Integer item) 
        {            
            added = item;
            
            currentIndex = index; 

            addedItems.add(item);
            
            return true;
        }        
    }

    private class RemoveEvent implements ObservableList.Event<Object>
    {
        @Override
        public boolean event(int index, Object item) 
        {
            deleted = (Integer) item;

            currentIndex = index; 

            deletedItems.add((Integer) item);
            
            return true;            
        }        
    }

    private class SetEvent implements ObservableList.Event<Integer> 
    {
        @Override
        public boolean event(int index, Integer item) 
        {            
            set = item;

            currentIndex = index;
            
            setItems.add(item);
            
            return true;            
        }        
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    @Before
    public void setup() 
    {
        added = null;
        deleted = null;
        set = null;
        currentIndex = -1;
        
        addedItems = new LinkedList<Integer>();
        deletedItems = new LinkedList<Integer>();
        setItems = new LinkedList<Integer>();
        
        integerList = new LinkedList<Integer>();
        observableList = new ObservableList<Integer>(integerList);

        observableList.setAddEvent(new AddEvent());
        observableList.setRemoveEvent(new RemoveEvent());
        observableList.setSetEvent(new SetEvent());
    }
    
    @Test
    public void testAdd() 
    {
        assertTrue(observableList.add(1));
        
        assertEquals(1, (int) added);
        assertEquals(-1, currentIndex);
        assertEquals(1, integerList.size());
        assertTrue(integerList.contains(1));
        
        observableList.add(2);
        
        assertEquals(2, (int) added);
        assertEquals(-1, currentIndex);
        assertEquals(2, integerList.size());
        assertTrue(integerList.contains(2));
        
        assertEquals(2, observableList.size());
    }

    @Test
    public void testAddWithIndex() 
    {
        observableList.add(0, 66);
        
        assertEquals(66, (int) added);
        assertEquals(0, currentIndex);
        assertEquals(1, integerList.size());
        assertTrue(integerList.contains(66));

        observableList.add(0, 77);
        
        assertEquals(77, (int) added);
        assertEquals(0, currentIndex);
        assertEquals(2, integerList.size());
        assertTrue(integerList.contains(77));
        
        assertEquals(66, (int) observableList.get(1));
        
        assertEquals(2, observableList.size());
    }
    
    @Test
    public void testRemove()
    {
        observableList.add(1);
        observableList.add(2);
        
        assertTrue(observableList.remove((Object)2));
        
        assertEquals(2, (int) deleted);
        assertEquals(-1, currentIndex);
        assertEquals(1, integerList.size());
        assertTrue(!integerList.contains(2));

        assertFalse(observableList.remove((Object)3));
        
        assertEquals(3, (int) deleted);
        assertEquals(-1, currentIndex);
        assertEquals(1, integerList.size());
        assertTrue(!integerList.contains(3));
        
        assertTrue(observableList.remove((Object)1));
        
        assertEquals(1, (int) deleted);
        assertEquals(-1, currentIndex);
    }

    @Test
    public void testClear() 
    {
        observableList.add(1);
        observableList.add(2);
        observableList.add(3);
        
        assertEquals(3, observableList.size());
        assertEquals(3, integerList.size());
        
        observableList.clear();
        
        assertEquals(0, integerList.size());
        assertEquals(0, observableList.size());
        
        assertEquals(3, deletedItems.size());

        assertTrue(deletedItems.contains(1));
        assertTrue(deletedItems.contains(2));
        assertTrue(deletedItems.contains(3));
    }

    @Test
    public void testAddAll()
    {
        Collection<Integer> otherIntegers = new LinkedList<Integer>();
        
        otherIntegers.add(1);
        otherIntegers.add(2);
        otherIntegers.add(3);
        
        assertEquals(0, observableList.size());
        assertEquals(0, integerList.size());
        
        assertTrue(observableList.addAll(otherIntegers));
        
        assertEquals(3, observableList.size());
        assertEquals(3, integerList.size());

        assertTrue(observableList.contains(1));
        assertTrue(observableList.contains(2));
        assertTrue(observableList.contains(3));

        assertTrue(integerList.contains(1));
        assertTrue(integerList.contains(2));
        assertTrue(integerList.contains(3));
    }

    @Test
    public void testRemoveAll()
    {
        observableList.add(1);
        observableList.add(2);
        observableList.add(3);
        
        Collection<Integer> removeIntegers = new LinkedList<Integer>();
        
        removeIntegers.add(2);
        removeIntegers.add(3);
        
        assertEquals(3, observableList.size());
        assertEquals(3, integerList.size());
        
        assertTrue(observableList.removeAll(removeIntegers));
        
        assertEquals(1, observableList.size());
        assertEquals(1, integerList.size());

        assertTrue(observableList.contains(1));
        assertTrue(integerList.contains(1));
    }

    @Test
    public void testRetainAll()
    {
        observableList.add(1);
        observableList.add(2);
        observableList.add(3);
        
        Collection<Integer> retainIntegers = new LinkedList<Integer>();
        
        retainIntegers.add(2);
        retainIntegers.add(3);
        
        assertEquals(3, observableList.size());
        assertEquals(3, integerList.size());
        
        assertTrue(observableList.retainAll(retainIntegers));
        
        assertEquals(2, observableList.size());
        assertEquals(2, integerList.size());

        assertTrue(observableList.contains(2));
        assertTrue(observableList.contains(3));

        assertTrue(integerList.contains(2));
        assertTrue(integerList.contains(3));
    }
    
    @Test
    public void testRemoveWithIndex()
    {
        observableList.add(8);
        observableList.add(9);
        
        assertEquals(9, (int) observableList.remove(1));
        
        assertEquals(null, deleted);
        assertEquals(1, currentIndex);
        assertEquals(1, observableList.size());
        assertEquals(1, integerList.size());
                
        assertFalse(observableList.contains(9));
        assertFalse(integerList.contains(9));

        assertEquals(8, (int) observableList.remove(0));
        
        assertEquals(null, deleted);
        assertEquals(0, currentIndex);
        assertEquals(0, observableList.size());
        assertEquals(0, integerList.size());

        assertFalse(observableList.contains(8));
        assertFalse(integerList.contains(8));
    }

    @Test
    public void testGet()
    {
        observableList.add(66);
        observableList.add(77);
        
        assertEquals(77, (int) observableList.get(1));
        assertEquals(66, (int) observableList.get(0));        

        assertEquals(77, (int) integerList.get(1));
        assertEquals(66, (int) integerList.get(0));        
    }
    
    @Test
    public void testIndexOf()
    {
        observableList.add(66);
        observableList.add(77);
        
        assertEquals(0, observableList.indexOf(66));
        assertEquals(1, observableList.indexOf(77));        

        assertEquals(0, integerList.indexOf(66));
        assertEquals(1, integerList.indexOf(77));        
    }
    
    @Test
    public void testSet()
    {
        observableList.add(66);
        assertEquals(66, (int) observableList.get(0));
        assertEquals(66, (int) integerList.get(0));

        observableList.add(77);
        assertEquals(77, (int) observableList.get(1));
        assertEquals(77, (int) integerList.get(1));

        assertEquals(66, (int) observableList.get(0));
        assertEquals(66, (int) integerList.get(0));
        
        observableList.set(0, 77);
        assertEquals(77, (int) observableList.get(0));
        assertEquals(77, (int) integerList.get(0));
        assertEquals(0, currentIndex);
        assertEquals(77, (int) set);
    }
    
    @Test
    public void testSublist()
    {
        observableList.add(66);
        observableList.add(77);
        observableList.add(88);
        observableList.add(99);

        List<Integer> subList = observableList.subList(1, 1);
        assertEquals(0, subList.size());

        subList = observableList.subList(1, 2);
        assertEquals(1, subList.size());
        assertEquals(77, (int) subList.get(0));

        subList = observableList.subList(1, 3);
        assertEquals(2, subList.size());
        assertEquals(77, (int) subList.get(0));
        assertEquals(88, (int) subList.get(1));

        assertEquals(88, (int) observableList.get(2));
        
        subList.remove(1);
        assertEquals(null, deleted);
        assertEquals(1, currentIndex);
        assertEquals(3, observableList.size());
        assertEquals(99, (int) observableList.get(2));
        assertEquals(3, integerList.size());
        assertEquals(99, (int) integerList.get(2));
    }
}
