/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.Test;

public class FilteredFileLineNumberReaderTest 
{
	private final static File base = new File("test/resources/testdata");
	
	@Test
	public void testReadLinesPattern() 
	throws IOException
	{
		File file = new File(base, "other/ends-with-text.txt");
		
		Pattern pattern = Pattern.compile(".*s$");
		
		FileLineNumberReader reader = new FilteredFileLineNumberReader(file, pattern);
		
		assertEquals(2, reader.getLineCount());
		
		List<String> lines = reader.readLines(0, 100);
		
		assertEquals(2, lines.size());

		assertEquals("this", lines.get(0));
		assertEquals("is", lines.get(1));
		
		lines = reader.readLines(1, 10);
		
		assertEquals(1, lines.size());

		assertEquals("is", lines.get(0));
	}

	@Test
	public void testReadLinesNullPattern() 
	throws IOException
	{
		File file = new File(base, "other/ends-with-text.txt");
		
		FileLineNumberReader reader = new FilteredFileLineNumberReader(file, null);
		
		assertEquals(7, reader.getLineCount());
		
		List<String> lines = reader.readLines(3, 3);
		
		assertEquals(3, lines.size());

		assertEquals("", lines.get(0));
		assertEquals("test", lines.get(1));
		assertEquals("", lines.get(2));
	}
}
