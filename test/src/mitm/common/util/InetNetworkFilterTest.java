package mitm.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;

import org.junit.Test;


public class InetNetworkFilterTest
{
	@Test
	public void testSingle()
	throws UnknownHostException
	{
		InetNetworkFilter filter = new InetNetworkFilter("192.168.*");
		
		assertTrue(filter.isAccepted("192.168.2.2"));
		assertFalse(filter.isAccepted("127.0.0.1"));
	}

	@Test
	public void testMultiple()
	throws UnknownHostException
	{
		InetNetworkFilter filter = new InetNetworkFilter("192.168.*, 127.*");
		
		assertTrue(filter.isAccepted("192.168.2.2"));
		assertTrue(filter.isAccepted("127.0.0.1"));
	}

	@Test
	public void testMultipleCIDR()
	throws UnknownHostException
	{
		InetNetworkFilter filter = new InetNetworkFilter("192.168.*, 127.*, 222.0.0.0/8");
		
		assertTrue(filter.isAccepted("192.168.2.2"));
		assertTrue(filter.isAccepted("127.0.0.1"));
		assertTrue(filter.isAccepted("222.1.2.3"));
	}

	@Test
	public void testWithEmpty()
	throws UnknownHostException
	{
		InetNetworkFilter filter = new InetNetworkFilter("192.168.*,,, 127.*,");
		
		assertTrue(filter.isAccepted("192.168.2.2"));
		assertTrue(filter.isAccepted("127.0.0.1"));
	}
}
