/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ObservableSetTest
{
    private static Integer added;
    private static Integer deleted;
    
    private Collection<Integer> addedItems;
    private Collection<Integer> deletedItems;
    
    private Set<Integer> integers;
    private ObservableSet<Integer> observedSet;
    
    private class AddEvent implements ObservableCollection.Event<Integer> 
    {
        @Override
        public boolean event(Integer item) 
        {
            added = item;
            
            addedItems.add(item);
            
            return true;
        }        
    }

    private class RemoveEvent implements ObservableCollection.Event<Object> 
    {
        @Override
        public boolean event(Object item) 
        {
            deleted = (Integer) item;
            
            deletedItems.add((Integer) item);
            
            return true;
        }        
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    @Before
    public void setup() 
    {
        added = null;
        deleted = null;
        
        addedItems = new LinkedList<Integer>();
        deletedItems = new LinkedList<Integer>();
        
        integers = new HashSet<Integer>();
        
        observedSet = new ObservableSet<Integer>(integers);
        
        observedSet.setAddEvent(new AddEvent());
        observedSet.setRemoveEvent(new RemoveEvent());
        
    }
    
    @Test
    public void testAdd() 
    {        
        assertTrue(observedSet.add(1));
        
        assertEquals(1, (int) added);
        assertEquals(1, integers.size());        
        
        observedSet.add(2);
        
        assertEquals(2, (int) added);
        assertEquals(2, integers.size());        
        
        assertEquals(2, observedSet.size());
    }

    @Test
    public void testRemove()
    {
        observedSet.add(1);
        observedSet.add(2);
        
        assertEquals(2, integers.size());        
        
        assertTrue(observedSet.remove(2));
        
        assertEquals(1, integers.size());                
        
        assertEquals(2, (int) deleted);

        assertFalse(observedSet.remove(3));
        
        assertEquals(3, (int) deleted);
        assertEquals(1, integers.size());        
        assertTrue(integers.contains(1));        
        
        observedSet.remove(1);
        
        assertEquals(1, (int) deleted);
        assertEquals(0, integers.size());        
    }

    @Test
    public void testClear() 
    {
        observedSet.add(1);
        observedSet.add(2);
        observedSet.add(3);
        
        assertEquals(3, observedSet.size());
        
        observedSet.clear();
        
        assertEquals(3, deletedItems.size());

        assertTrue(deletedItems.contains(1));
        assertTrue(deletedItems.contains(2));
        assertTrue(deletedItems.contains(3));
    }

    @Test
    public void testAddAll()
    {
        Collection<Integer> otherIntegers = new LinkedList<Integer>();
        
        otherIntegers.add(1);
        otherIntegers.add(2);
        otherIntegers.add(3);
        
        assertEquals(0, observedSet.size());
        
        assertTrue(observedSet.addAll(otherIntegers));
        
        assertEquals(3, observedSet.size());

        assertTrue(observedSet.contains(1));
        assertTrue(observedSet.contains(2));
        assertTrue(observedSet.contains(3));
    }

    @Test
    public void testRemoveAll()
    {
        observedSet.add(1);
        observedSet.add(2);
        observedSet.add(3);
        
        Collection<Integer> removeIntegers = new LinkedList<Integer>();
        
        removeIntegers.add(2);
        removeIntegers.add(3);
        
        assertEquals(3, observedSet.size());
        
        assertTrue(observedSet.removeAll(removeIntegers));
        
        assertEquals(1, observedSet.size());

        assertTrue(observedSet.contains(1));
    }

    @Test
    public void testRetainAll()
    {
        observedSet.add(1);
        observedSet.add(2);
        observedSet.add(3);
        
        Collection<Integer> retainIntegers = new LinkedList<Integer>();
        
        retainIntegers.add(2);
        retainIntegers.add(3);
        
        assertEquals(3, observedSet.size());
        
        assertTrue(observedSet.retainAll(retainIntegers));
        
        assertEquals(2, observedSet.size());

        assertTrue(observedSet.contains(2));
        assertTrue(observedSet.contains(3));
    }
}
