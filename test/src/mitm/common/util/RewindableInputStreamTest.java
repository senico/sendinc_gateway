/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import mitm.test.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Martijn Brinkers
 *
 */
public class RewindableInputStreamTest
{
    private static int tempFileCount;

    @Before
    public void before() {
        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");
    }

    @After
    public void after()
    throws IOException
    {
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    @Test
    public void testMarkResetMarkInMem()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3,4};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        assertNull(delegate.getFile());

        byte[] read2 = new byte[2];
        byte[] read5 = new byte[5];

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        int i = 0;

        delegate.mark(0);
        i = delegate.read(read5);
        assertEquals(4, i);
        assertTrue(Arrays.equals(new byte[]{1,2,3,4,0}, read5));
        delegate.reset();

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        delegate.mark(0);
        i = delegate.read(read2);
        assertEquals(2, i);
        assertTrue(Arrays.equals(new byte[]{1,2}, read2));
        delegate.reset();

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        delegate.mark(0);
        i = delegate.read(read5);
        assertEquals(4, i);
        assertTrue(Arrays.equals(new byte[]{1,2,3,4,0}, read5));
        delegate.reset();

        assertNull(delegate.getFile());

        delegate.close();
    }

    @Test
    public void testMarkResetMarkOnDisk()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3,4};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, 0);

        assertNull(delegate.getFile());

        byte[] read2 = new byte[2];
        byte[] read5 = new byte[5];

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        int i = 0;

        delegate.mark(0);
        i = delegate.read(read5);
        assertEquals(4, i);
        assertTrue(Arrays.equals(new byte[]{1,2,3,4,0}, read5));
        delegate.reset();

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        delegate.mark(0);
        i = delegate.read(read2);
        assertEquals(2, i);
        assertTrue(Arrays.equals(new byte[]{1,2}, read2));
        delegate.reset();

        Arrays.fill(read2, (byte)0);
        Arrays.fill(read5, (byte)0);

        delegate.mark(0);
        i = delegate.read(read5);
        assertEquals(4, i);
        assertTrue(Arrays.equals(new byte[]{1,2,3,4,0}, read5));
        delegate.reset();

        assertNotNull(delegate.getFile());

        delegate.close();
    }

    @Test
    public void testMarkInMem()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3,4};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        assertTrue(delegate.markSupported());

        assertEquals(4, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertEquals(2, delegate.available());

        delegate.mark(1000);

        assertEquals(3, delegate.read());
        assertEquals(4, delegate.read());
        assertEquals(-1, delegate.read());

        delegate.reset();

        assertEquals(2, delegate.available());

        assertEquals(3, delegate.read());
        assertEquals(4, delegate.read());
        assertEquals(-1, delegate.read());

        assertNull(delegate.getFile());

        delegate.close();
    }

    @Test
    public void testMarkOnDisk()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3,4};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, 1);

        assertTrue(delegate.markSupported());

        assertEquals(4, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertEquals(2, delegate.available());

        delegate.mark(1000);

        assertEquals(3, delegate.read());
        assertEquals(4, delegate.read());
        assertEquals(-1, delegate.read());

        delegate.reset();

        assertEquals(2, delegate.available());

        assertEquals(3, delegate.read());
        assertEquals(4, delegate.read());
        assertEquals(-1, delegate.read());

        File file = delegate.getFile();

        assertTrue(file.exists());

        delegate.close();

        assertFalse(file.exists());
    }

    @Test
    public void testResetInMem()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        assertEquals(3, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertEquals(1, delegate.available());

        delegate.reset();

        assertEquals(3, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());
        assertEquals(3, delegate.read());
        assertEquals(-1, delegate.read());

        assertNull(delegate.getFile());

        delegate.close();
    }

    @Test
    public void testResetOnDisk()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, 2);

        assertEquals(3, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertEquals(1, delegate.available());

        delegate.reset();

        assertEquals(3, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());
        assertEquals(3, delegate.read());
        assertEquals(-1, delegate.read());

        assertTrue(delegate.getFile().exists());

        delegate.close();

        assertFalse(delegate.getFile().exists());
    }

    @Test
    public void testResetLargeInMem()
    throws IOException
    {
        byte[] data = new byte[65535];

        Arrays.fill(data, (byte)0xff);

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        assertEquals(data.length, delegate.available());

        for (int i = 0; i < 64000; i++) {
            assertEquals(0xff, delegate.read());
        }

        assertEquals(data.length - 64000, delegate.available());

        delegate.reset();

        assertEquals(data.length, delegate.available());

        for (int i = 0; i < data.length; i++) {
            assertEquals(0xff, delegate.read());
        }

        assertEquals(-1, delegate.read());

        assertNull(delegate.getFile());

        delegate.close();
    }

    @Test
    public void testResetLargeOnDisk()
    throws IOException
    {
        byte[] data = new byte[65535];

        Arrays.fill(data, (byte)0xff);

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, 100);

        assertEquals(data.length, delegate.available());

        for (int i = 0; i < 64000; i++) {
            assertEquals(0xff, delegate.read());
        }

        assertEquals(data.length - 64000, delegate.available());

        delegate.reset();

        assertEquals(data.length, delegate.available());

        for (int i = 0; i < data.length; i++) {
            assertEquals(0xff, delegate.read());
        }

        assertEquals(-1, delegate.read());

        assertTrue(delegate.getFile().exists());

        delegate.close();

        assertFalse(delegate.getFile().exists());
    }

    @Test
    public void testRead()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3};

        InputStream input = new ByteArrayInputStream(data);

        InputStream delegate = new RewindableInputStream(input, 1024);

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());
        assertEquals(3, delegate.read());
        assertEquals(-1, delegate.read());
        delegate.close();
    }

    @Test
    public void testClose()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3};

        InputStream input = new CheckCloseInputStream(new ByteArrayInputStream(data));

        InputStream delegate = new RewindableInputStream(input, 1024);

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());
        assertEquals(3, delegate.read());
        assertEquals(-1, delegate.read());

        delegate.close();

        try {
            delegate.read();

            fail("input should be closed.");
        }
        catch(IOException e) {
            // expected
        }

        try {
            input.read();

            fail("input should be closed.");
        }
        catch(IOException e) {
            // expected
        }
    }

    @Test
    public void testThresholdExceeded()
    throws IOException
    {
        byte[] data = new byte[]{1,2,3};

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, 2);

        assertEquals(3, delegate.available());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertNull(delegate.getFile());

        assertEquals(1, delegate.available());

        delegate.reset();

        assertEquals(3, delegate.available());

        assertNull(delegate.getFile());

        assertEquals(1, delegate.read());
        assertEquals(2, delegate.read());

        assertNull(delegate.getFile());

        assertEquals(3, delegate.read());

        assertNotNull(delegate.getFile());

        assertEquals(-1, delegate.read());

        assertNotNull(delegate.getFile());
        assertTrue(delegate.getFile().exists());

        delegate.close();

        assertFalse(delegate.getFile().exists());
    }

    @Test
    public void testBufferSize()
    throws IOException
    {
        byte[] data = new byte[RewindableInputStream.INITIAL_BUF_SIZE + 1];

        Arrays.fill(data, (byte)0xff);

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        for (int i = 0; i < RewindableInputStream.INITIAL_BUF_SIZE; i++)
        {
            delegate.read();

            assertEquals(RewindableInputStream.INITIAL_BUF_SIZE, delegate.getByteBuffer().getBuffer().length);
        }

        delegate.read();

        assertEquals(RewindableInputStream.INITIAL_BUF_SIZE * 2, delegate.getByteBuffer().getBuffer().length);

        delegate.close();
    }

    @Test
    public void testPosition()
    throws IOException
    {
        byte[] data = new byte[16384];

        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) i;
        }

        InputStream input = new ByteArrayInputStream(data);

        RewindableInputStream delegate = new RewindableInputStream(input, Integer.MAX_VALUE);

        assertEquals(0, delegate.getPosition());

        for (int i = 0; i < 100; i++) {
            assertEquals((byte) i, (byte) delegate.read());
        }

        assertEquals(100, delegate.getPosition());

        delegate.setPosition(20);

        for (int i = 0; i < 100; i++) {
            assertEquals((byte) (i + 20), (byte) delegate.read());
        }

        delegate.close();
    }

    @Test
    public void testLargeFileSpeed()
    throws IOException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(
                "test/resources/testdata/documents/excel-very-large.xls"));

        long start = System.currentTimeMillis();

        RewindableInputStream delegate = new RewindableInputStream(input, 1048576);

        int i = 0;

        while (delegate.read() != -1) {
            i++;
        }

        assertEquals(16572416, i);

        delegate.setPosition(0);

        i = 0;

        while (delegate.read() != -1) {
            i++;
        }

        assertEquals(16572416, i);

        System.out.println("testLargeFileSpeed secs:" +
                DateTimeUtils.millisecondsToSeconds(System.currentTimeMillis() - start));

        delegate.close();
    }
}
