/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import mitm.common.security.crypto.RandomGenerator;
import mitm.common.security.crypto.impl.RandomGeneratorImpl;

import org.junit.BeforeClass;
import org.junit.Test;


public class UniqueIDGeneratorTest
{
    private static RandomGenerator randomGenerator;
    
    @BeforeClass
    public static void beforeClass()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        randomGenerator = new RandomGeneratorImpl();
    }
    
    @Test
    public void testCreateID()
    {
        IDGenerator idCreator = new UniqueIDGenerator(randomGenerator, 16);
        
        String id1 = idCreator.createID();
        String id2 = idCreator.createID();
        
        assertFalse(id1.equals(id2));
        assertEquals(40, id1.length());
        assertEquals(40, id2.length());
    }

    @Test
    public void testCreateIDWithBase()
    {
        IDGenerator idCreator = new UniqueIDGenerator(randomGenerator, 16, "test");
        
        String id = idCreator.createID();

        assertEquals(44, id.length());
        assertTrue(id.startsWith("test"));
    }
}
