/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collection;

import mitm.common.security.certificate.CertificateUtils;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class SizeLimitedInputStreamTest
{
    private File testBase = new File("test/resources/testdata/");

    @Test
    public void testTestMaxReached()
    throws IOException
    {
        InputStream input = new FileInputStream(new File(testBase, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        byte[] buf = new byte[maxBytes];

        limitedInput.read(buf);

        try {
            limitedInput.read(buf, 0, 1);

            fail();
        }
        catch(IOException e) {
            // should be thrown
            assertEquals(SizeLimitedInputStream.MAXIMUM_REACHED, e.getMessage());
        }
        limitedInput.close();
    }

    @Test
    public void testTestMaxNotReached()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(testBase, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(limitedInput);

        assertEquals(1000, certificates.size());
    }

    @Test
    public void testTestMaxNotReachedMaxBytesNull()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(testBase, "certificates/random-self-signed-10.p7b"));

        final int maxBytes = 0;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(limitedInput);

        assertEquals(10, certificates.size());
    }

    @Test
    public void testTestMaxNotReachedMaxBytesNegative()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(testBase, "certificates/random-self-signed-10.p7b"));

        final int maxBytes = -1;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(limitedInput);

        assertEquals(10, certificates.size());
    }

    @Test
    public void testLimitNoException()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(testBase, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes, false);

        byte[] data = IOUtils.toByteArray(limitedInput);

        /*
         * Actual read is 4096 because the buffer size is 4096
         */
        assertEquals(4096, data.length);

        assertEquals(0, limitedInput.available());
        assertEquals(-1, limitedInput.read());
        assertEquals(-1, limitedInput.read(data));
        assertEquals(-1, limitedInput.read(data, 0, 100));
        assertEquals(0, limitedInput.skip(10));
    }
}
