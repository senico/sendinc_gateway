/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

public class FileLineNumberReaderTest 
{
	private final static File base = new File("test/resources/testdata");
	
	@Test
	public void testLineCount() 
	throws IOException
	{
		File file = new File(base, "other/ends-with-lf.txt");
		
		FileLineNumberReader reader = new FileLineNumberReader(file);
		
		assertEquals(8, reader.getLineCount());
		assertEquals(8, reader.getLineCount());

		file = new File(base, "other/ends-with-text.txt");
		
		reader = new FileLineNumberReader(file);
		
		assertEquals(7, reader.getLineCount());
		assertEquals(7, reader.getLineCount());
	}

	@Test
	public void testReadSingleLine() 
	throws IOException
	{
		File file = new File(base, "mail/simple-text-message.eml");
		
		FileLineNumberReader reader = new FileLineNumberReader(file);
		
		List<String> lines = reader.readLines(0, 1);
		
		assertEquals(1, lines.size());
		assertEquals("Content-Type: text/plain", lines.get(0));

		lines = reader.readLines(2, 1);
		
		assertEquals(1, lines.size());
		assertEquals("From: test@example.com", lines.get(0));
	}

	@Test
	public void testReadLines() 
	throws IOException
	{
		File file = new File(base, "other/ends-with-text.txt");
		
		FileLineNumberReader reader = new FileLineNumberReader(file);
		
		List<String> lines = reader.readLines(3, 3);
		
		assertEquals(3, lines.size());

		assertEquals("", lines.get(0));
		assertEquals("test", lines.get(1));
		assertEquals("", lines.get(2));
	}
	
	@Test
	public void testReadLinesToEnd() 
	throws IOException
	{
		File file = new File(base, "other/ends-with-text.txt");
		
		FileLineNumberReader reader = new FileLineNumberReader(file);
		
		List<String> lines = reader.readLines(0, Integer.MAX_VALUE);
		
		assertEquals(7, lines.size());

		lines = reader.readLines(5, 99);
		
		assertEquals(2, lines.size());
		assertEquals("", lines.get(0));
		assertEquals("123", lines.get(1));
		
		file = new File(base, "other/ends-with-lf.txt");
		
		reader = new FileLineNumberReader(file);
		
		lines = reader.readLines(0, Integer.MAX_VALUE);
		
		assertEquals(8, lines.size());

		lines = reader.readLines(5, 99);
		
		assertEquals(3, lines.size());
		assertEquals("", lines.get(0));
		assertEquals("123", lines.get(1));
		assertEquals("", lines.get(2));
	}
}
