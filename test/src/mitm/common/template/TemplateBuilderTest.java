/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.template;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.commons.lang.SystemUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import freemarker.template.SimpleHash;

public class TemplateBuilderTest
{    
    private static TemplateBuilder templateBuilder;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        templateBuilder = new TemplateBuilderImpl(new File(SystemUtils.USER_DIR));
    }
    
    @Test
    public void testAbbreviate()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        properties.put("value", "qwerty");
        
        String result = templateBuilder.buildTemplate("${abbreviate(value, 5)}", properties);
        
        assertEquals("qw...", result);

        result = templateBuilder.buildTemplate("${abbreviate(unknown!\"\",5)}", properties);
        
        assertEquals("", result);
    }

    @Test(expected = TemplateBuilderException.class)
    public void testAbbreviateNotEnoughParams()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        properties.put("value", "qwerty");
        
        templateBuilder.buildTemplate("${abbreviate(value)}", properties);
    }
    
    @Test
    public void testBasic()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        properties.put("value", "123");
        
        String result = templateBuilder.buildTemplate("name=${value}", properties);
        
        assertEquals("name=123", result);
    }

    @Test(expected = TemplateBuilderException.class)
    public void testQuotedPrintableNotEnoughParams()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        templateBuilder.buildTemplate("${qp()}", properties);
    }
    
    @Test
    public void testQuotedPrintable()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        properties.put("value", "=");
        
        String result = templateBuilder.buildTemplate("${qp(value)}", properties);
        
        assertEquals("=3D", result);

        result = templateBuilder.buildTemplate("${qp(value, 'US-ASCII')}", properties);
        
        assertEquals("=3D", result);

        result = templateBuilder.buildTemplate("${qp('äöüÄÖÜ', 'UTF-8')}", properties);
        
        assertEquals("=C3=A4=C3=B6=C3=BC=C3=84=C3=96=C3=9C", result);
    }
    
    @Test
    public void testQuotedPrintableUnknownVar()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        String result = templateBuilder.buildTemplate("${qp(value!\"\")}", properties);
        
        assertEquals("", result);
    }
    
    @Test
    public void testUnicode()
    throws TemplateBuilderException
    {
        SimpleHash properties = new SimpleHash();
        
        properties.put("value", "äöüÄÖÜ");
        
        String result = templateBuilder.buildTemplate("${value} äöüÄÖÜ", properties);
        
        assertEquals("äöüÄÖÜ äöüÄÖÜ", result);
    }
}
