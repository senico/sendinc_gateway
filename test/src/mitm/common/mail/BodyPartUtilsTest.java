/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.junit.Ignore;
import org.junit.Test;

public class BodyPartUtilsTest
{
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");

    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }

    @Test
    public void testsearchForRFC822() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/attached-encrypted-rfc822.eml");
        
        message = BodyPartUtils.searchForRFC822(message);
        
        assertNotNull(message);
        
        assertTrue(message.isMimeType("application/pkcs7-mime"));
    }
    
    @Test
    public void testToRFC822() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/folded-header.eml");
        
        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "testtorfc822.eml");
        
        assertTrue(rfc822Part.isMimeType("message/rfc822"));
        
        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);
        
        saveMessage(rfc822Message, "testtorfc822.eml");
        
        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }

    @Test
    public void testToRFC822UnknownCharset() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/unknown-charset-xxx.eml");
        
        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "unknown-charset-xxx.eml");
        
        assertTrue(rfc822Part.isMimeType("message/rfc822"));

        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);
        
        saveMessage(rfc822Message, "testtorfc822unknowncharset.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }

    @Test
    public void testToRFC822UnknownContentTransferEncoding() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/unknown-content-transfer-encoding.eml");
        
        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "unknown-content-transfer-encoding.eml");
        
        assertTrue(rfc822Part.isMimeType("message/rfc822"));
        
        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);
        
        saveMessage(rfc822Message, "testtorfc822unknowncontenttransferencoding.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }
    
    @Test
    public void testExtractFromRFC822() 
    throws MessagingException, IOException
    {
        MimeMessage rfc822Message = loadMessage("mail/rfc822.eml");
        
        assertTrue(rfc822Message.isMimeType("message/rfc822"));
        
        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);
        
        assertTrue(message.isMimeType("text/plain"));
        
        Object content = message.getContent();
        assertTrue(content instanceof String);
        assertEquals("test", ((String) content).trim());
    }

    @Test
    public void testExtractFromRFC822UnknownContentType() 
    throws MessagingException, IOException
    {
        MimeMessage rfc822Message = loadMessage("mail/rfc822-unknown-content-transfer-encoding-attached-message.eml");
        
        assertTrue(rfc822Message.isMimeType("message/rfc822"));
        
        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);
        
        assertTrue(message.isMimeType("text/plain"));
        assertEquals("test simple message", message.getSubject());
        assertEquals("xxx", message.getHeader("Content-Transfer-Encoding", ","));        
    }

    /*
     * Note: The content type is corrupt. If -Dmail.mime.parameters.strict is set to false, the content
     * type is detected. However calling getContent() does not return a MimeMessage
     */
    @Ignore
    @Test
    public void testExtractFromRFC822CorruptContentType() 
    throws MessagingException, IOException
    {
        System.setProperty("mail.mime.parameters.strict", "false");
                
        MimeMessage rfc822Message = loadMessage("mail/rfc822-corrupt-content-type.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
        
        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);
        
        assertTrue(message.isMimeType("text/plain"));
        assertEquals("test simple message", message.getSubject());
    }
    
    @Test
    public void testExtractFromRFC822UnknownCharset() 
    throws MessagingException, IOException
    {
        MimeMessage rfc822Message = loadMessage("mail/rfc822-unknown-charset.eml");
        
        assertTrue(rfc822Message.isMimeType("message/rfc822"));
        
        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);
        
        assertTrue(message.isMimeType("text/plain"));
    }
    
    @Test
    public void testGetBodyAndAttachments()
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/normal-message-with-attach.eml");
        
        Collection<Part> attachments = new LinkedList<Part>();
        
        String body = BodyPartUtils.getPlainBodyAndAttachments(message, attachments);
        
        assertEquals(1, attachments.size());
        assertEquals("Test", body);
    }

    @Test
    public void testGetBodyAndAttachmentsSigned()
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("mail/clear-signed-validcertificate.eml");
        
        Collection<Part> attachments = new LinkedList<Part>();
        
        String body = BodyPartUtils.getPlainBodyAndAttachments(message, attachments);
        
        assertEquals(1, attachments.size());
        assertEquals("Test", body);
    }
}
