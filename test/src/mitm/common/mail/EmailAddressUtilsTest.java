/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;

import org.apache.mailet.MailAddress;
import org.junit.Test;

public class EmailAddressUtilsTest
{
    private final static File testBase = new File("test/resources/testdata");

    @Test
    public void testStripQuotes() 
    {
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("test@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("'test@example.com'"));
        assertEquals("testexample.com", EmailAddressUtils.stripQuotes("testexample.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("\"test\"@example.com"));
        assertEquals("te\"st@example.com", EmailAddressUtils.stripQuotes("\"te\"st\"@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("\"test@example.com\""));
    }

    @Test
    public void testValidate() 
    {
        assertEquals("test@example.com", EmailAddressUtils.validate("test@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com"));
        assertEquals("\"test\"@example.com", EmailAddressUtils.validate("\"test\"@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com"));
        assertEquals(null, EmailAddressUtils.validate("invalid"));
        assertEquals(null, EmailAddressUtils.validate("<script>test.@example.com"));
        assertEquals(null, EmailAddressUtils.validate("<test.@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.validate("  test@example.com  "));
        assertEquals(null, EmailAddressUtils.validate("test @example.com"));
        assertEquals("\"test \"@example.com", EmailAddressUtils.validate("\"test \"@example.com"));
        assertEquals(null, EmailAddressUtils.validate("test@ex_ample.com"));
        assertEquals("\"test,123\"@example.com", EmailAddressUtils.validate("\"test,123\"@example.com"));
        assertEquals(null, EmailAddressUtils.validate("test,123@example.com"));
    }

    @Test
    public void testCanonicalize() 
    {
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("TEST.@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("'TEST.@example.com'"));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("\"TEST.\"@example.com"));
    }

    @Test
    public void testNormalize() 
    {
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("TEST.@example.com", false));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("\"TEST.\"@example.com", false));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("'TEST.@example.com'", false));
        assertEquals("test@example.com", EmailAddressUtils.canonicalizeAndValidate("\"test@example.com\"", false));
        assertEquals(null, EmailAddressUtils.canonicalizeAndValidate("123", true));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate("123", false));
        assertEquals(null, EmailAddressUtils.canonicalizeAndValidate((String)null, true));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate(EmailAddressUtils.INVALID_EMAIL, false));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate(EmailAddressUtils.INVALID_EMAIL, true));
    }
    
    @Test
    public void testInvalidEmail() 
    throws ParseException
    {
    	/*
    	 * Make sure that INVALID_EMAIL can be used to create an InternetAdddress
    	 */
    	InternetAddress address = new InternetAddress(EmailAddressUtils.INVALID_EMAIL);
    	
    	assertEquals(EmailAddressUtils.INVALID_EMAIL, address.getAddress());
    	
    	MailAddress mailAddress = new MailAddress(address);

    	assertEquals(EmailAddressUtils.INVALID_EMAIL, mailAddress.toString());
    }
    
    @Test
    public void testNormalizeCollectionSingle() 
    {
        List<String> emails = new LinkedList<String>();
        
        emails.add("  test@example.com  ");
        emails.add(" 'test@example.com' ");
        emails.add(" \"TESt\"@example.com ");

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);
        
        assertEquals(1, normalized.size());
        assertEquals("test@example.com", normalized.iterator().next());
    }   

    @Test
    public void testNormalizeCollectionMultiple() 
    {
        List<String> emails = new LinkedList<String>();
        
        emails.add("  test@example.com  ");
        emails.add(" xxx ");
        emails.add(" test2@example.com ");
        emails.add("  ");
        emails.add(" test @ example.com  ");

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);
        
        assertEquals(2, normalized.size());
        
        Iterator<String> it = normalized.iterator();
        
        assertEquals("test@example.com", it.next());
        assertEquals("test2@example.com", it.next());
        
        normalized = EmailAddressUtils.canonicalizeAndValidate(emails, false);
        
        assertEquals(3, normalized.size());
        
        assertTrue(normalized.contains("test@example.com"));
        assertTrue(normalized.contains("test2@example.com"));
        assertTrue(normalized.contains(EmailAddressUtils.INVALID_EMAIL));
    }

    @Test
    public void testNormalizeArrayMultiple() 
    {
        String[] emails = new String[]{"  test@example.com  ", " xxx ", " test2@example.com ", "  ", 
                " test @ example.com  "};

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);
        
        assertEquals(2, normalized.size());
        
        Iterator<String> it = normalized.iterator();
        
        assertEquals("test@example.com", it.next());
        assertEquals("test2@example.com", it.next());
        
        normalized = EmailAddressUtils.canonicalizeAndValidate(emails, false);
        
        assertEquals(3, normalized.size());
        
        assertTrue(normalized.contains("test@example.com"));
        assertTrue(normalized.contains("test2@example.com"));
        assertTrue(normalized.contains(EmailAddressUtils.INVALID_EMAIL));
    }
    
    @Test
    public void testNormalizeCollectionNull() 
    {
        Collection<String> emails = null;
        
        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);
        
        assertEquals(0, normalized.size());
    }

    @Test
    public void testNormalizeArrayNull() 
    {
        String[] emails = null;
        
        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);
        
        assertEquals(0, normalized.size());
    }
    
    @Test
    public void testGetDomain()
    throws AddressException
    {
        assertEquals("example.com", EmailAddressUtils.getDomain("test@example.com"));
        assertEquals(null, EmailAddressUtils.getDomain("example.com"));
        assertEquals("example.com", EmailAddressUtils.getDomain("@example.com"));
        assertNull(EmailAddressUtils.getDomain((String)null));
        assertEquals(null, EmailAddressUtils.getDomain(""));
        assertEquals("example.com", EmailAddressUtils.getDomain(new InternetAddress("test@example.com")));
        assertEquals(null, EmailAddressUtils.getDomain((InternetAddress)null));
    }
    
    @Test
    public void testGetDomainWithAt()
    {
        assertEquals("test.com", EmailAddressUtils.getDomain("\"x@example.com\"@test.com"));
    }

    @Test
    public void testGetLocalPart()
    throws AddressException
    {
        assertEquals("test", EmailAddressUtils.getLocalPart("test@example.com"));
        assertEquals(null, EmailAddressUtils.getLocalPart("@example.com"));
        assertEquals("123", EmailAddressUtils.getLocalPart("123"));
        assertNull(EmailAddressUtils.getLocalPart((String)null));
        assertEquals(null, EmailAddressUtils.getLocalPart(""));
        assertEquals("\"x@example.com\"", EmailAddressUtils.getLocalPart("\"x@example.com\"@test.com"));
        assertEquals("test", EmailAddressUtils.getLocalPart(new InternetAddress("test@example.com")));
        assertEquals(null, EmailAddressUtils.getLocalPart((InternetAddress)null));
    }
        
    @Test
    public void testQuoteLocalPart()
    {
        assertEquals("test@example.com", EmailAddressUtils.quoteLocalPart("test@example.com"));
        assertEquals("\"te,st\"@example.com", EmailAddressUtils.quoteLocalPart("te,st@example.com"));
        assertEquals("\"te,st\"@example.com", EmailAddressUtils.quoteLocalPart("\"te,st\"@example.com"));
        assertEquals("\"\"te,st\"@example.com", EmailAddressUtils.quoteLocalPart("\"te,st@example.com"));
        assertEquals("test.123@example.com", EmailAddressUtils.quoteLocalPart("test.123@example.com"));
        assertEquals("test_123@example.com", EmailAddressUtils.quoteLocalPart("test_123@example.com"));
    }
    
    @Test
    public void testEmailPattern()
    {
        String replaceWith = "123";

        String s = "test@example.com"; 
        assertEquals("123", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  test@example.com  "; 
        assertEquals("  123  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  test@example.com>  "; 
        assertEquals("  123>  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));
        
        s = "  <test@example.com>  "; 
        assertEquals("  <123>  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  &lt;test@example.com&gt;  "; 
        assertEquals("  &lt;123&gt;  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));
    }
    
    @Test
    public void testContainsEmail()
    {
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", Arrays.asList("test@example.com")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", Arrays.asList("   test@eXAmple.com ")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", Arrays.asList("a@b.c", "test@eXAmple.com")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", Arrays.asList("***", "test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("test2@example.com", Arrays.asList("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail(null, Arrays.asList("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("**", Arrays.asList("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("test@example.com", null));
        assertFalse(EmailAddressUtils.containsEmail(null, null));
    }
    
    @Test
    public void testAddressesToStrings()
    throws AddressException
    {
        Address[] addresses = new Address[]{new InternetAddress("test@example.com", false),
                new InternetAddress("test user<test@example.com>", false),
                new InternetAddress("\"test user \" <test@example.com>", false),
                new InternetAddress("\"=?big5?q?=AFQ=C0s=B8=E9@=BD=DE=C0Y=20j?=\"<test@example.com>", false)};
        
        String[] s = EmailAddressUtils.addressesToStrings(addresses, true);
        
        assertEquals(4, s.length);
        assertEquals("test@example.com", s[0]);
        assertEquals("test user <test@example.com>", s[1]);
        assertEquals("test user  <test@example.com>", s[2]);
        // Note: this only works with -Dmail.mime.decodetext.strict=false system setting
        // It's therefore disabled for now
        //assertEquals("\"烏龍賊@豬頭 j\" <test@example.com>", s[3]);
    }
        
    @Test
    public void testGetAddress()
    throws Exception
    {
        assertEquals("test@example.com", EmailAddressUtils.getAddress(new Address[]{
                new InternetAddress("test@example.com")}).toString());

        assertEquals("test@example.com", EmailAddressUtils.getAddress(new Address[]{
                new InternetAddress("test@example.com"), new InternetAddress("test2@example.com")}).toString());

        assertNull(EmailAddressUtils.getAddress(new Address[]{}));
        assertNull(EmailAddressUtils.getAddress(null));
    }
    
    @Test
    public void testGetReplyToQuietly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/invalid-from-to-cc-reply-to.eml"));
        
        Address[] replyTo = EmailAddressUtils.getReplyToQuietly(message);
        
        assertNull(replyTo);

        message = MailUtils.loadMessage(new File(testBase, "mail/embedded-clear-signed.eml"));
        
        replyTo = EmailAddressUtils.getReplyToQuietly(message);
        
        assertNotNull(replyTo);
        assertEquals(1, replyTo.length);
        assertEquals("\"Ubuntu user technical support, not for general discussions\" <ubuntu-users@lists.ubuntu.com>",
        		replyTo[0].toString());
        
        assertNull(EmailAddressUtils.getReplyToQuietly(null));
    }
    
    @Test
    public void testGetFromQuietly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/invalid-from-to-cc-reply-to.eml"));
        
        Address[] from = EmailAddressUtils.getFromQuietly(message);
        
        assertNull(from);

        message = MailUtils.loadMessage(new File(testBase, "mail/embedded-clear-signed.eml"));
        
        from = EmailAddressUtils.getFromQuietly(message);
        
        assertNotNull(from);
        assertEquals(1, from.length);
        assertEquals("James Gray <james@gray.net.au>", from[0].toString());
        
        assertNull(EmailAddressUtils.getFromQuietly(null));
    }    
}
