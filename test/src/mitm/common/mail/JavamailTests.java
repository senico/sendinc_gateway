/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/*
 * Test cases to test certain aspects of Javamail to learn how it actually works.
 */
public class JavamailTests
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    @BeforeClass
    public static void setUpBeforeClass() 
    {
        System.setProperty("mail.mime.parameters.strict", "false");
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }

    @Test
    public void testUnfolding() 
    throws FileNotFoundException, MessagingException
    {
        MimeMessage message = loadMessage("folded-header.eml");
        
        String header = message.getHeader("X-Folded-Header", ",");
        
        assertEquals("Test of a \r\n\tfolded header", header);
        
        String unfolded = MimeUtility.unfold(header);

        assertEquals("Test of a  folded header", unfolded);
    }

    @Test
    public void testFolding()
    throws FileNotFoundException, MessagingException
    {
        String unfolded = 
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa ";

        assertTrue(unfolded.indexOf('\n') == -1);
        
        String folded = MimeUtility.fold(0, unfolded);
        
        assertTrue(folded.indexOf('\n') != -1);
    }

    @Test
    public void testFoldingMessage()
    throws MessagingException, IOException
    {
        String unfolded = 
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa ";

        MimeMessage message = loadMessage("folded-header.eml");
        
        message.setHeader("X-Test", unfolded);
        
        // Javamail does not fold the message for you when you use setHeader or addHeader 
        saveMessage(message, "testfoldingmessage.eml");
        
        MimeMessage savedMessage = MailUtils.loadMessage(new File(tempDir, "testfoldingmessage.eml"));
        
        assertEquals(unfolded, savedMessage.getHeader("X-Test", ","));
    }
    
    
    @Test
    public void testNonASCII() 
    throws FileNotFoundException, MessagingException, UnsupportedEncodingException
    {
        MimeMessage message = loadMessage("chinese-encoded-from.eml");
        
        String header = message.getHeader("from", ",");
        
        // this decoding works
        String decoded = MimeUtility.decodeWord("=?big5?q?=AFQ=C0s=B8=E9@=BD=DE=C0Y=20j?=");
        
        assertEquals("烏龍賊@豬頭 j", decoded);
        
        // this decoding only works when the following system property is set
        // mail.mime.decodetext.strict=false.
        // the header is non-standard encoded so it only works when strict is off
        // see MimeUtility for more info
        decoded = MimeUtility.decodeText(header);
    }
    
    @Test
    public void testEncoding() 
    throws UnsupportedEncodingException
    {
        String header = "test";
        
        header = MimeUtility.encodeText(header);
        
        assertEquals("test", header);
        
        header = MimeUtility.encodeText("烏龍賊@豬頭 j");
        
        assertEquals("烏龍賊@豬頭 j", MimeUtility.decodeText(header));
    }
    
    @Test
    public void testSetNullHeaderValue() 
    throws FileNotFoundException, MessagingException
    {
        MimeMessage message = loadMessage("folded-header.eml");

        assertNotNull(message.getSubject());
        
        message.setHeader("subject", null);
        
        assertEquals("null", message.getHeader("subject", ","));
        
        message.removeHeader("subject");
        assertNull(message.getHeader("subject"));
    }

    @Test
    @Ignore 
    // XXX test ignored because it requires the system property mail.mime.parameters.strict to be false
    // setting this property only works when set for this test class but now when set on the main 
    // AllTest class. Somehow the system property is not set on the forked java process during unit testing
    public void testAttachmentFilenameSpacesNotQuoted() 
    throws FileNotFoundException, MessagingException
    {
        /*
         * Apple sometimes adds a filename with spaces without quotes. Setting the system property 
         * mail.mime.parameters.strict to true allows Javamail to read the filename correctly (workaround)
         */
        MimeMessage message = loadMessage("message-attach-filename-with-no-quotes.eml");

        String filename = message.getFileName();

        assertEquals("city info.zip", filename);
    }
}
