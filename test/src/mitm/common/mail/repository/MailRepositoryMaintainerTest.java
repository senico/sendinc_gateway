/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionAdapter;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.mail.MailUtils;
import mitm.common.mail.repository.hibernate.MailRepositoryDAO;
import mitm.common.mail.repository.hibernate.MailRepositoryImpl;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.crypto.impl.RandomGeneratorImpl;
import mitm.common.util.IDGenerator;
import mitm.common.util.UniqueIDGenerator;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class MailRepositoryMaintainerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static final File testBase = new File("test/resources/testdata/mail");
    
    private static final String DEFAULT_REPOSITORY = "test-quarantine";
    
    private static SessionManager sessionManager;
    private static AutoTransactDelegator proxy;
    private static IDGenerator idGenerator; 
    private static MimeMessage message;
    
    private static class MailRepositoryEventListenerTester implements MailRepositoryEventListener
    {
        List<MailRepositoryItem> expired = new LinkedList<MailRepositoryItem>();
        
        @Override
        public void onDeleted(String repository, MailRepositoryItem item) {
            fail();
        }

        @Override
        public void onReleased(String repository, MailRepositoryItem item) {
            fail();
        }

        @Override
        public void onExpired(String repository, MailRepositoryItem item) {
            expired.add(item);
        }
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        LogManager.getLogger(MailRepositoryMaintainer.class).setLevel(Level.DEBUG);
        
        HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        proxy = AutoTransactDelegator.createProxy();
        
        idGenerator = new UniqueIDGenerator(new RandomGeneratorImpl(), 16);
        
        message = loadMessage("html-alternative.eml");
    }
    
    @Before
    public void setup()
    throws Exception
    {
        proxy.deleteAll();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        private MailRepositoryDAO getDAO()
        {
            SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());
            
            return new MailRepositoryDAO(session);
        }    

        private MailRepository getRepository(String repositoryName) {
            return new MailRepositoryImpl(idGenerator, sessionManager, repositoryName);
        }
        
        @StartTransaction
        public void deleteAll() {
            getDAO().deleteAll();
        }

        @StartTransaction
        public MailRepositoryItem addItemWithDate(String repositoryName, MimeMessage message, Date created)
        throws MessagingException, IOException
        {
            MailRepository repository = getRepository(repositoryName);
            
            MailRepositoryItem item = ((MailRepositoryImpl) repository).createItem(message, created);
            
            repository.addItem(item);
            
            return item;
        }

        @StartTransaction
        public MailRepositoryItem getItem(String repositoryName, String id)
        throws MessagingException, IOException
        {
            return getRepository(repositoryName).getItem(id);
        }
        
        @StartTransaction
        public int getItemCount(String repositoryName) {
            return getRepository(repositoryName).getItemCount();
        }
        
        @StartTransaction
        public List<? extends MailRepositoryItem> getItems(String repositoryName, Integer firstResult, 
                Integer maxResults)
        {
            return getRepository(repositoryName).getItems(firstResult, maxResults);
        }
    }
    
    @Test
    public void testExpire()
    throws Exception
    {
        Date expiredDate = DateUtils.addDays(new Date(), -6);
        
        proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        proxy.addItemWithDate(DEFAULT_REPOSITORY, message, new Date());
        
        assertEquals(4, proxy.getItemCount(DEFAULT_REPOSITORY));
        
        MailRepositoryEventListenerTester listener = new MailRepositoryEventListenerTester();
        
        MailRepositoryMaintainer maintainer = new MailRepositoryMaintainer(Collections.singleton(
            proxy.getRepository(DEFAULT_REPOSITORY)), listener, sessionManager);
        
        maintainer.setSettleTime(0);
        maintainer.setSleepTime(10);
        maintainer.start();
        
        try {
            Thread.sleep(2000);
            
            assertEquals(1, proxy.getItemCount(DEFAULT_REPOSITORY));
    
            assertEquals(3, listener.expired.size());
        }
        finally {
            maintainer.stop();
        }
        
    }
    
    @Test
    public void testBatchSize()
    throws Exception
    {
        Date expiredDate = DateUtils.addDays(new Date(), -6);
        
        int nrToAdd = 500;
        
        for (int i = 0; i < nrToAdd; i++)
        {
            proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            proxy.addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            proxy.addItemWithDate(DEFAULT_REPOSITORY, message, new Date());
        }
        
        assertEquals(nrToAdd * 4, proxy.getItemCount(DEFAULT_REPOSITORY));
        
        MailRepositoryEventListenerTester listener = new MailRepositoryEventListenerTester();
        
        MailRepositoryMaintainer maintainer = new MailRepositoryMaintainer(Collections.singleton(
            proxy.getRepository(DEFAULT_REPOSITORY)), listener, sessionManager);
        
        maintainer.setSettleTime(0);
        maintainer.setSleepTime(10);

        try {
            maintainer.start();
        
            do {
                Thread.sleep(1000);
            }
            while (proxy.getItemCount(DEFAULT_REPOSITORY) != nrToAdd);

            // wait some extra
            Thread.sleep(2000);

            assertEquals(nrToAdd, proxy.getItemCount(DEFAULT_REPOSITORY));
            
            assertEquals(nrToAdd * 3, listener.expired.size());
        }
        finally {
            maintainer.stop();
        }        
    }
}
