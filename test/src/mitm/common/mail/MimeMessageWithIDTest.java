/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.Test;


public class MimeMessageWithIDTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    @Test
    public void testSetMessageID()
    throws Exception
    {
        MimeMessage message = loadMessage("clear-signed-evolution.eml");
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));
        
        message.saveChanges();

        assertFalse("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));
        
        message = loadMessage("clear-signed-evolution.eml");
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));
        
        MimeMessage clone = new MimeMessage(message);
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(clone.getMessageID()));

        clone.saveChanges();

        assertFalse("<1195030544.6663.27.camel@ubuntu>".equals(clone.getMessageID()));
        
        MimeMessage cloneWithID = new MimeMessageWithID(message, "abc");
        
        assertTrue("abc".equals(cloneWithID.getMessageID()));

        cloneWithID.saveChanges();

        assertTrue("abc".equals(cloneWithID.getMessageID()));        
    }
    
    @Test
    public void testSetMessageIDInputStream()
    throws Exception
    {
        MimeMessage message = loadMessage("clear-signed-evolution.eml");
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        message.writeTo(bos);
        
        MimeMessage clone = new MimeMessage(MailSession.getDefaultSession(), 
                new ByteArrayInputStream(bos.toByteArray()));
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(clone.getMessageID()));
        
        clone.saveChanges();

        assertFalse("<1195030544.6663.27.camel@ubuntu>".equals(clone.getMessageID()));
        
        MimeMessage cloneWithID = new MimeMessageWithID(MailSession.getDefaultSession(), 
                new ByteArrayInputStream(bos.toByteArray()), "123");
        
        assertTrue("123".equals(cloneWithID.getMessageID()));
        
        cloneWithID.saveChanges();

        assertTrue("123".equals(cloneWithID.getMessageID()));        
    }

    @Test
    public void testSetMessageIDNull()
    throws Exception
    {
        MimeMessage message = loadMessage("clear-signed-evolution.eml");
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));

        MimeMessage cloneWithID = new MimeMessageWithID(message, null);
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(cloneWithID.getMessageID()));        
        
        cloneWithID.saveChanges();

        assertNotNull(cloneWithID.getMessageID());
        assertFalse("<1195030544.6663.27.camel@ubuntu>".equals(cloneWithID.getMessageID()));        
    }

    @Test
    public void testSetMessageIDNullStream()
    throws Exception
    {
        MimeMessage message = loadMessage("clear-signed-evolution.eml");
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(message.getMessageID()));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        message.writeTo(bos);
        
        MimeMessage cloneWithID = new MimeMessageWithID(MailSession.getDefaultSession(), 
                new ByteArrayInputStream(bos.toByteArray()), null);
        
        assertTrue("<1195030544.6663.27.camel@ubuntu>".equals(cloneWithID.getMessageID()));        
        
        cloneWithID.saveChanges();

        assertNotNull(cloneWithID.getMessageID());
        assertFalse("<1195030544.6663.27.camel@ubuntu>".equals(cloneWithID.getMessageID()));        
    }
    
    @Test
    public void testWithSession()
    throws Exception
    {
        MimeMessage message = new MimeMessageWithID(MailSession.getDefaultSession(), "123");
        
        message.setText("test");
        
        message.saveChanges();
        
        assertEquals("123", message.getMessageID());
    }
}
