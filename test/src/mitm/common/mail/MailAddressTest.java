/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;

import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.SerializationUtils;
import org.apache.mailet.MailAddress;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test for James MailAddress. Strictly speaking this test doesn't belong here because it's not the correct package.
 * However because we have modified MailAddress to make it accept more email addresses we decided to put the test in
 * this package and not in the James source. 
 * 
 * @author Martijn Brinkers
 *
 */
public class MailAddressTest
{
    @Test
    public void testValid()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");
        assertEquals("test", address.getUser());
        assertEquals("example.com", address.getHost());

        address = new MailAddress("  test@example.com  ");
        assertEquals("test", address.getUser());
        assertEquals("example.com", address.getHost());

        address = new MailAddress("\"test.\"@example.com  ");
        assertEquals("\"test.\"", address.getUser());
        assertEquals("example.com", address.getHost());

        // source routing
        address = new MailAddress(" @xxx:test@example.com  ");
        assertEquals("test", address.getUser());
        assertEquals("example.com", address.getHost());

        // dot num ([])
        address = new MailAddress("test@[1.2.3.4]");
        assertEquals("test", address.getUser());
        assertEquals("1.2.3.4", address.getHost());
        
        assertEquals("test@exa-mple.com", new InternetAddress("test@exa-mple.com").getAddress());
    }

    /*
     * Note: this test is disabled because it currently fails with the current version of James MailAddress. MailAddress
     * is strict RFC compliant. Postfix however is not so sometimes email will be accepted by Postfix which is then 
     * blocked by James. I tried to make MailAddress less strict but then toInternetAddress() fails because 
     * Java InternetAddress does not accept _ for a domain. 
     */
    @Ignore
    @Test
    public void testInvalidButShouldBeAccepted()
    throws Exception
    {
        MailAddress address = new MailAddress("test.@example.com");
        assertEquals("test.", address.getUser());
        assertEquals("example.com", address.getHost());

        address = new MailAddress("test@exa_mple.com");
        assertEquals("test", address.getUser());
        assertEquals("exa_mple.com", address.getHost());

        address = new MailAddress("test@e_xample.com");
        assertEquals("test@e_xample.com", address.toInternetAddress().toString());
    }
    
    @Test
    public void testToInternetAddress()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");
        assertEquals("test@example.com", address.toInternetAddress().toString());
    }
    
    private void shouldFail(String address)
    {
        try {
            new MailAddress(address);
            
            fail();
        }
        catch(ParseException e) {
            // expected
        }
    }
    
    @Test
    public void testInvalid()
    throws Exception
    {
        shouldFail("test@-example.com");
    }
    
    @Test
    public void testSerialize()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");
        
        byte[] serialized = SerializationUtils.serialize(address);
        
        MailAddress copy = (MailAddress) SerializationUtils.deserialize(serialized);
        
        assertEquals(address, copy);
    }
    
    /*
     * Check to make sure that old serialized versions of MailAddress still can be deserialized 
     */
    @Test
    public void testDeserialize()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");

        String base64Serialized = 
            "rO0ABXNyAB1vcmcuYXBhY2hlLm1haWxldC5NYWlsQWRkcmVzcyaRkoRtx3ukAgADSQADcG9zTAAE" +
            "aG9zdHQAEkxqYXZhL2xhbmcvU3RyaW5nO0wABHVzZXJxAH4AAXhwAAAAEHQAC2V4YW1wbGUuY29t" +
            "dAAEdGVzdA==";

        MailAddress copy = (MailAddress) SerializationUtils.deserialize(
                Base64.decodeBase64(MiscStringUtils.toAsciiBytes(base64Serialized)));
        
        assertEquals(address, copy);
    }
}
