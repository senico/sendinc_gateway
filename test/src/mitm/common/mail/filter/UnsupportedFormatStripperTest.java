/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.mail.filter.UnsupportedFormatStripper.MatchingPair;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class UnsupportedFormatStripperTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    @BeforeClass
    public static void setUpBeforeClass() 
    {
        BasicConfigurator.configure();
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }

    @Test
    public void testAttachmentWithMatchUppercaseExtension()
    throws Exception
    {
        MimeMessage message = loadMessage("normal-message-with-attach-uppercase-extension.eml");
        
        List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();
        
        attachmentMatchers.add(new MatchingPair("application/octet-stream", "*.zip"));
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper(attachmentMatchers);
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }
    
    @Test
    public void testAttachmentWithMatchUppercaseExtensionStrip()
    throws Exception
    {
        MimeMessage message = loadMessage("normal-message-with-attach-uppercase-extension.eml");
        
        List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper(attachmentMatchers);
        
        MimeMessage converted = stripper.strip(message);
        
        assertNotNull(converted);
        
        saveMessage(converted, "testAttachmentWithMatchUppercaseExtensionStrip.eml");
        
        assertTrue(converted.isMimeType("multipart/mixed"));
        assertEquals("normal message with attachment", converted.getSubject());
        
        Multipart mp = (Multipart) converted.getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("multipart/alternative"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("text/plain"));
        
        assertEquals("Attachment with filename cityinfo.ZiP was removed.", part.getContent());
    }
    
    @Test
    public void testMultipleAttachmentToTooLarge()
    throws Exception
    {
        MimeMessage message = loadMessage("multiple-attachments.eml");
        
        List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();
        
        attachmentMatchers.add(new MatchingPair("*", "*.png"));
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper(attachmentMatchers);
        
        stripper.setMaxMessageSize(200000);
        
        MimeMessage converted = stripper.strip(message);
        
        assertNotNull(converted);
        
        saveMessage(converted, "testMultipleAttachmentToTooLarge.eml");
        
        assertTrue(converted.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) converted.getContent();
        
        assertEquals(6, mp.getCount());
        
        Part part = mp.getBodyPart(0);

        assertTrue(part.isMimeType("text/plain"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("image/png"));

        part = mp.getBodyPart(2);

        assertTrue(part.isMimeType("image/png"));

        for (int i = 3; i < 6; i++)
        {
            part = mp.getBodyPart(i);
            
            assertTrue(part.isMimeType("text/plain"));
        }
    }
    
    @Test
    public void testKeepSMIME()
    throws Exception
    {
        MimeMessage message = loadMessage("encrypted-aes.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }

    @Test
    public void testRemoveSMIME()
    throws Exception
    {
        MimeMessage message = loadMessage("encrypted-aes.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        stripper.setKeepSMIMEParts(false);
        
        MimeMessage converted = stripper.strip(message);
        
        assertNotNull(converted);

        saveMessage(converted, "testRemoveSMIME.eml");
        
        assertTrue(converted.isMimeType("text/plain"));
        assertEquals("normal message with attachment", converted.getSubject());
    }
    
    @Test
    public void testAttachedMessage()
    throws Exception
    {
        MimeMessage message = loadMessage("attached-message.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }
    
    @Test
    public void testAttachmentTooLarge()
    throws Exception
    {
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();
        
        attachmentMatchers.add(new MatchingPair("application/octet-stream", "*.zip"));
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper(attachmentMatchers);
        
        stripper.setMaxMessageSize(500);
        
        MimeMessage converted = stripper.strip(message);
        
        assertNotNull(converted);
        
        saveMessage(converted, "testAttachmentTooLarge.eml");
        
        assertTrue(converted.isMimeType("multipart/mixed"));
        assertEquals("normal message with attachment", converted.getSubject());
        
        Multipart mp = (Multipart) converted.getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("multipart/alternative"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("text/plain"));
        assertEquals("Attachment with filename cityinfo.zip was removed.", part.getContent());        
    }

    @Test
    public void testNoContentType()
    throws Exception
    {
        MimeMessage message = loadMessage("text-message-no-content-type.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }
    
    @Test
    public void testAlternative()
    throws Exception
    {
        MimeMessage message = loadMessage("unicode.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }
    
    @Test
    public void testHTMLOnlyMail()
    throws Exception
    {
        MimeMessage message = loadMessage("html-only.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }
    
    @Test
    public void testAttachedRFC822()
    throws Exception
    {
        MimeMessage message = loadMessage("attached-message.eml");
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper();
        
        MimeMessage converted = stripper.strip(message);
        
        assertNull(converted);
    }    

    @Test
    public void testAttachedRFC822Strip()
    throws Exception
    {
        MimeMessage message = loadMessage("attached-message.eml");
        
        List<MatchingPair> attachmentMatchers = new LinkedList<MatchingPair>();

        attachmentMatchers.add(new MatchingPair("application/octet-stream", "*.zip"));
        
        UnsupportedFormatStripper stripper = new UnsupportedFormatStripper(attachmentMatchers);
        
        MimeMessage converted = stripper.strip(message);

        assertNotNull(converted);

        saveMessage(converted, "testAttachedRFC822Strip.eml");
        
        assertTrue(converted.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) converted.getContent();
        
        assertEquals(2, mp.getCount());
        
        Part part = mp.getBodyPart(0);
        
        assertTrue(part.isMimeType("text/plain"));

        part = mp.getBodyPart(1);
        
        assertTrue(part.isMimeType("text/plain"));
        assertEquals("Attachment with filename <unknown> was removed.", part.getContent());        
    }    
}
