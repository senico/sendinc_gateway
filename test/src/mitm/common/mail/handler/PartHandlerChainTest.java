/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.MailUtils;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.HeaderNameMatcher;

import org.junit.Test;

public class PartHandlerChainTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }
    
    @Test
    public void testChainUnknownContentType() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("unknown-content-type.eml");
        
        HeaderMatcher matcher = new HeaderNameMatcher("date", "SUBJECT");
        
        PartHandlerChain decorator = new PartHandlerChain(new RemoveHeadersHandler(matcher));
        
        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"), new Header("test", "456")));
        
        message = BodyPartUtils.toMessage(decorator.handlePart(message));
                
        MailUtils.validateMessage(message);
        
        assertNull(message.getSubject());
        assertNull(message.getHeader("datE"));
        assertEquals("123,456", message.getHeader("test", ","));
        
        saveMessage(message, "testchainunknowncontenttype.eml");
    }
    
    @Test
    public void testChainOrderTest() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("unknown-content-type.eml");
        
        HeaderMatcher matcher = new HeaderNameMatcher("test");
        
        PartHandlerChain decorator = new PartHandlerChain();
        
        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"), new Header("test", "456")));
        
        // this handler should remove the headers that were just added
        decorator.addMessageHandlers(new RemoveHeadersHandler(matcher));
        
        message = BodyPartUtils.toMessage(decorator.handlePart(message));
                
        MailUtils.validateMessage(message);
        
        assertNull(message.getHeader("test", ","));
        
        saveMessage(message, "testchainordertest.eml");
    }

    @Test
    public void testChainOrderTestOtherOrder() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("unknown-content-type.eml");
        
        HeaderMatcher matcher = new HeaderNameMatcher("test");
        
        PartHandlerChain decorator = new PartHandlerChain();
        
        decorator.addMessageHandlers(new RemoveHeadersHandler(matcher));
        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"), new Header("test", "456")));
                
        message = BodyPartUtils.toMessage(decorator.handlePart(message));
                
        MailUtils.validateMessage(message);
        
        assertEquals("123,456", message.getHeader("test", ","));
        
        saveMessage(message, "testchainordertestotherorder.eml");
    }
}
