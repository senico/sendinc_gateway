/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail.handler;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.BodyPartUtils;
import mitm.common.mail.MailUtils;
import mitm.common.mail.PartHandler;
import mitm.common.mail.handler.AddHeadersHandler;

import org.junit.Test;

public class AddHeadersHandlerTest
{
    private static final File testBase = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");

    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    private static void saveMessage(MimeMessage message, String filename) 
    throws IOException, MessagingException
    {
        File file = new File(tempDir, filename);
        
        FileOutputStream output = new FileOutputStream(file);
        
        MailUtils.writeMessage(message, output);
    }
    
    @Test
    public void testAddHeaderUnknownContentType() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("unknown-content-type-multipart.eml");
        
        PartHandler decorator = new AddHeadersHandler(new Header("test", "123"));
        
        message = BodyPartUtils.toMessage(decorator.handlePart(message));
                
        MailUtils.validateMessage(message);
        
        assertEquals("123", message.getHeader("test", ","));
        
        saveMessage(message, "testaddheaderunknowncontenttype.eml");
    }

    @Test
    public void testAddHeaderUnknownContentTransferEncoding() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("unknown-content-transfer-encoding.eml");
        
        PartHandler decorator = new AddHeadersHandler(new Header("test", "123"));
        
        message = BodyPartUtils.toMessage(decorator.handlePart(message));
                
        MailUtils.validateMessage(message);
        
        assertEquals("123", message.getHeader("test", ","));
                
        saveMessage(message, "testaddheaderunknowncontenttransferencoding.eml");
    }
}
