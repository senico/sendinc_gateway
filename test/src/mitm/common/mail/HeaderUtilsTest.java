/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Enumeration;
import java.util.regex.Pattern;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.junit.Test;

import mitm.common.mail.matcher.ContentHeaderNameMatcher;
import mitm.common.mail.matcher.HeaderMatcher;
import mitm.common.mail.matcher.NotHeaderNameMatcher;
import mitm.common.mail.matcher.RegExHeaderNameMatcher;

public class HeaderUtilsTest
{
    private static final File testBase = new File("test/resources/testdata/mail");

    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    private int getHeaderCount(Enumeration<Header> headerEnum)
    {
        int count = 0;
        
        while (headerEnum.hasMoreElements()) 
        {
            headerEnum.nextElement();
            count++; 
        }

        return count;
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testCopyHeaders() 
    throws FileNotFoundException, MessagingException 
    {
        MimeBodyPart destination = new MimeBodyPart();
        
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        HeaderMatcher matcher = new ContentHeaderNameMatcher();
        
        HeaderUtils.copyHeaders(message, destination, matcher);
        
        assertEquals(1, getHeaderCount(destination.getAllHeaders()));
        assertEquals(message.getContentType(), destination.getContentType());
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testCopyHeadersRegEx() 
    throws FileNotFoundException, MessagingException 
    {
        MimeBodyPart destination = new MimeBodyPart();
        
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);
        
        HeaderUtils.copyHeaders(message, destination, matcher);
        
        assertEquals(4, getHeaderCount(destination.getAllHeaders()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCopyHeadersNotMatcher() 
    throws FileNotFoundException, MessagingException 
    {
        MimeBodyPart destination = new MimeBodyPart();
        
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);

        HeaderMatcher notMatcher = new NotHeaderNameMatcher(matcher); 
        
        HeaderUtils.copyHeaders(message, destination, notMatcher);
        
        assertEquals(10, getHeaderCount(destination.getAllHeaders()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testRemoveHeaders() 
    throws FileNotFoundException, MessagingException 
    {
        MimeMessage message = loadMessage("normal-message-with-attach.eml");
        
        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);

        HeaderUtils.removeHeaders(message, matcher);
        
        assertEquals(10, getHeaderCount(message.getAllHeaders()));
    }
    
    @Test
    public void testPrependHeaderLine() 
    throws Exception 
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertNull(message.getHeader("X-Djigzo-Test"));
        
        HeaderUtils.prependHeaderLine(message, "X-Djigzo-Test: test");
        
        /*
         * Don't call message.saveChanges() because that result in resorting 
         */

        File file = new File("test/tmp/testPrependHeaderLine.eml");
        
        MailUtils.writeMessage(message, file);        
        
        assertEquals("test", message.getHeader("X-Djigzo-Test", ", "));
        
        Enumeration<?> headerEnum = message.getAllHeaderLines();
        
        assertEquals("X-Djigzo-Test: test", (String) (headerEnum.nextElement()));
    }
}
