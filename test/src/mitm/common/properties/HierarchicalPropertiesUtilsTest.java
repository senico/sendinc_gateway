/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Pattern;

import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.crypto.Encryptor;
import mitm.common.security.crypto.EncryptorException;
import mitm.common.security.crypto.impl.PasswordBasedEncryptor;
import mitm.common.util.MiscStringUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HierarchicalPropertiesUtilsTest
{
    private static HierarchicalProperties properties;
    
    private static Encryptor encryptor;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
        
        encryptor = new PasswordBasedEncryptor("djigzo");
        
        properties = new StandardHierarchicalProperties("properties", null, new Properties(), encryptor);
    }

    @Before
    public void setup() 
    throws HierarchicalPropertiesException 
    {
        properties.deleteAll();
    }

    @Test
    public void testCopyPropertiesRegEx() 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123, false);
        properties.setProperty("someProp", "someValue", false);
        properties.setProperty("someOtherProp", "someOtherValue", false);
        properties.setProperty("protected.test", "protectedValue", false);

        assertEquals(4, properties.getProperyNames(false).size());
        assertEquals("123", properties.getProperty("int", false));
        assertEquals("someValue", properties.getProperty("someProp", false));
        assertEquals("someOtherValue", properties.getProperty("someOtherProp", false));
        assertEquals("protectedValue", properties.getProperty("protected.test", false));
        
        HierarchicalProperties other = new StandardHierarchicalProperties("other", null, new Properties(), encryptor);
        
        HierarchicalPropertiesUtils.copyProperties(properties, other, Pattern.compile("(?i)^protected\\..*"));

        assertEquals(3, other.getProperyNames(false).size());
        assertEquals("123", other.getProperty("int", false));
        assertEquals("someValue", other.getProperty("someProp", false));
        assertEquals("someOtherValue", other.getProperty("someOtherProp", false));
    }
    
    @Test
    public void testCopyProperties() 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123, false);
        properties.setProperty("someProp", "someValue", false);
        properties.setProperty("someOtherProp", "someOtherValue", false);
        properties.setProperty("protected.test", "protectedValue", false);

        assertEquals(4, properties.getProperyNames(false).size());
        assertEquals("123", properties.getProperty("int", false));
        assertEquals("someValue", properties.getProperty("someProp", false));
        assertEquals("someOtherValue", properties.getProperty("someOtherProp", false));
        assertEquals("protectedValue", properties.getProperty("protected.test", false));
        
        HierarchicalProperties other = new StandardHierarchicalProperties("other", null, new Properties(), encryptor);
        
        HierarchicalPropertiesUtils.copyProperties(properties, other);

        assertEquals(4, other.getProperyNames(false).size());
        assertEquals("123", other.getProperty("int", false));
        assertEquals("someValue", other.getProperty("someProp", false));
        assertEquals("someOtherValue", other.getProperty("someOtherProp", false));
        assertEquals("protectedValue", other.getProperty("protected.test", false));
    }
    
    @Test
    public void testIntegerProperty() 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123, false);
       
        assertEquals((int) 123, (int) HierarchicalPropertiesUtils.getInteger(properties, "int", 456, false));
        assertNull(HierarchicalPropertiesUtils.getInteger(properties, "xxx", null, false));
        assertEquals((int) 456, (int) HierarchicalPropertiesUtils.getInteger(properties, "xxx", 456, false));
    }

    @Test
    public void testLongProperty() 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(properties, "long", 123L, false);
       
        assertEquals(123L, (long) HierarchicalPropertiesUtils.getLong(properties, "long", 890L, false));
        assertNull(HierarchicalPropertiesUtils.getLong(properties, "xxx", null, false));
        assertEquals(890L, (long) HierarchicalPropertiesUtils.getLong(properties, "xxx", 890L, false));
    }

    @Test
    public void testBooleanProperty() 
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(properties, "boolean", true, false);
       
        assertEquals(true, HierarchicalPropertiesUtils.getBoolean(properties, "boolean", true, false));
        assertNull(HierarchicalPropertiesUtils.getBoolean(properties, "xxx", null, false));
        assertEquals(true, HierarchicalPropertiesUtils.getBoolean(properties, "xxx", true, false));
    }

    @Test
    public void testBinaryProperty() 
    throws HierarchicalPropertiesException
    {
        byte[] bin = new byte[]{1, 2, 3};
        
        HierarchicalPropertiesUtils.setBinary(properties, "bin", bin, false);
       
        assertArrayEquals(bin, HierarchicalPropertiesUtils.getBinary(properties, "bin", null, false));
        assertNull(HierarchicalPropertiesUtils.getBinary(properties, "xxx", null, false));
        assertArrayEquals(bin, HierarchicalPropertiesUtils.getBinary(properties, "xxx", bin, false));
    }
    
    @Test
    public void testEncryptedProperties() 
    throws HierarchicalPropertiesException, NoSuchAlgorithmException, NoSuchProviderException, 
    SecurityFactoryFactoryException, EncryptorException
    {
        properties.setProperty("encrypted", "123", true);
        assertEquals("123", properties.getProperty("encrypted", true));
        assertNotNull(equals(properties.getProperty("encrypted", false)));
        assertFalse("123".equals(properties.getProperty("encrypted", false)));
        assertTrue(properties.getProperty("encrypted", false).length() > 3);
        
        Encryptor encryptor = new PasswordBasedEncryptor("djigzo");
        
        assertEquals("123",  MiscStringUtils.toAsciiString(encryptor.decryptBase64(
        		properties.getProperty("encrypted", false))));
    }
    
    @Test
    public void testEncryptedPropertiesMappedHierarchicalProperties() 
    throws HierarchicalPropertiesException, NoSuchAlgorithmException, NoSuchProviderException, 
    SecurityFactoryFactoryException, EncryptorException
    {
        properties = new MappedHierarchicalProperties("properties", null, new HashMap<String, String>(), encryptor);
    	
        properties.setProperty("encrypted", "123", true);
        assertEquals("123", properties.getProperty("encrypted", true));
        assertNotNull(equals(properties.getProperty("encrypted", false)));
        assertFalse("123".equals(properties.getProperty("encrypted", false)));
        assertTrue(properties.getProperty("encrypted", false).length() > 3);
        
        Encryptor encryptor = new PasswordBasedEncryptor("djigzo");
        
        assertEquals("123",  MiscStringUtils.toAsciiString(encryptor.decryptBase64(
        		properties.getProperty("encrypted", false))));
    }
}
