/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import mitm.common.properties.hibernate.NamedBlobEntity;

import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class NamedBlobUtilsTest
{
    @Test
    public void testSelectEmpty()
    {
        List<NamedBlob> selected = NamedBlobUtils.select(null, null);
        
        assertNull(selected);

        NamedBlobEntity namedBlob = new NamedBlobEntity();
        namedBlob.setCategory("category");
        
        selected = NamedBlobUtils.select(Collections.singleton((NamedBlob) namedBlob), null);
        assertNull(selected);

        
        NamedBlobSelectorImpl selector = new NamedBlobSelectorImpl();
        selector.setCategory("category");
        
        selected = NamedBlobUtils.select(null, selector);
        assertNull(selected);
    }
    
    @Test
    public void testSelect()
    {
        Collection<NamedBlob> all = new LinkedList<NamedBlob>();
        
        NamedBlobEntity namedBlob = new NamedBlobEntity();
        namedBlob.setName("name");
        namedBlob.setCategory("category");
        namedBlob.setBlob(new byte[]{1});
        all.add(namedBlob);
        
        namedBlob = new NamedBlobEntity();
        namedBlob.setCategory("category2");
        namedBlob.setBlob(new byte[]{2});
        all.add(namedBlob);

        namedBlob = new NamedBlobEntity();
        namedBlob.setName("name");
        namedBlob.setBlob(new byte[]{3});
        all.add(namedBlob);
        
        namedBlob = new NamedBlobEntity();
        namedBlob.setCategory("category");
        namedBlob.setBlob(new byte[]{4});
        all.add(namedBlob);
        
        assertEquals(4, all.size());
        
        NamedBlobSelectorImpl selector = new NamedBlobSelectorImpl();
        selector.setCategory("category");
        
        List<NamedBlob> selected = NamedBlobUtils.select(all, selector);
        
        assertEquals(2, selected.size());
        
        assertArrayEquals(new byte[]{1}, selected.get(0).getBlob());
        assertArrayEquals(new byte[]{4}, selected.get(1).getBlob());
    }
}
