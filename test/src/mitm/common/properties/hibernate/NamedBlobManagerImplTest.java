/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.properties.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.NamedBlob;
import mitm.common.properties.NamedBlobManager;
import mitm.common.reflection.ProxyFactoryException;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class NamedBlobManagerImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static SessionManager sessionManager;
    private static NamedBlobManager namedBlobManager;
    private static AutoTransactDelegator namedBlobManagerProxy;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());       
        
        namedBlobManager = new NamedBlobManagerImpl(sessionManager);
        
        namedBlobManagerProxy = AutoTransactDelegator.createProxy();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        namedBlobManagerProxy.deleteAll();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public void deleteAll()
        {
            namedBlobManager.deleteAll();
        }

        @StartTransaction
        public NamedBlob createNamedBlob(String category, String name)
        {
            return namedBlobManager.createNamedBlob(category, name);
        }

        @StartTransaction
        public NamedBlob getNamedBlob(String category, String name)
        {
            return namedBlobManager.getNamedBlob(category, name);
        }

        @StartTransaction
        public void deleteNamedBlob(String category, String name)
        {
            namedBlobManager.deleteNamedBlob(category, name);
        }

        @StartTransaction
        public List<? extends NamedBlob> getByCategory(String category, Integer firstResult, Integer maxResults)
        {
            return namedBlobManager.getByCategory(category, firstResult, maxResults);
        }
        
        @StartTransaction
        public int getByCategoryCount(String category)
        {
            return namedBlobManager.getByCategoryCount(category);
        }

        @StartTransaction
        public void addChild(String parentCategory, String parentName, String childCategory, String childName)
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);
            
            assertNotNull(parent);

            NamedBlob child = namedBlobManager.getNamedBlob(childCategory, childName);
            
            assertNotNull(child);
            
            parent.getNamedBlobs().add(child);
        }

        @StartTransaction
        public void deleteChild(String parentCategory, String parentName, String childCategory, String childName)
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);
            
            assertNotNull(parent);

            NamedBlob child = namedBlobManager.getNamedBlob(childCategory, childName);
            
            assertNotNull(child);
            
            parent.getNamedBlobs().remove(child);
        }
        
        @StartTransaction
        public void assertChildSize(String parentCategory, String parentName, int size)
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);
            
            assertNotNull(parent);
            
            assertEquals(size, parent.getNamedBlobs().size());
        }
        
        @StartTransaction
        public long getReferencedByCount(String parentCategory, String name)
        {
            NamedBlob namedBlob = namedBlobManager.getNamedBlob(parentCategory, name);
            
            assertNotNull(namedBlob);
            
            return namedBlobManager.getReferencedByCount(namedBlob);
        }
        
        @StartTransaction
        public List<NamedBlob> getReferencedBy(String parentCategory, String name, Integer firstResult, 
                Integer maxResults)
        {
            NamedBlob namedBlob = namedBlobManager.getNamedBlob(parentCategory, name);
            
            assertNotNull(namedBlob);
            
            List<? extends NamedBlob> blobs = namedBlobManager.getReferencedBy(namedBlob, firstResult, maxResults);
            
            List<NamedBlob> result = new LinkedList<NamedBlob>();
            
            // copy to force them to be loaded (except the binary blob and childs)
            for (NamedBlob blob : blobs) {
                result.add(blob);
            }
            
            return result;
        }
    }    
    
    @Test
    public void testCreateNamedBlob()
    {
        String category = "category";
        String name = "name";
        
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category));
        
        NamedBlob blob = namedBlobManagerProxy.createNamedBlob(category, name);
        
        assertEquals(0, namedBlobManagerProxy.getReferencedByCount(category, name));
        
        assertNotNull(blob);
        assertEquals(category, blob.getCategory());
        assertEquals(name, blob.getName());
        assertEquals(0, blob.getNamedBlobs().size());
        
        assertEquals(1, namedBlobManagerProxy.getByCategoryCount(category));
    }

    @Test
    public void testCreateDuplicateNamedBlob()
    {
        String category = "category";
        String name = "name";
        
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category));
        
        namedBlobManagerProxy.createNamedBlob(category, name);
        
        try {
            namedBlobManagerProxy.createNamedBlob(category, name);
            
            fail("ConstraintViolationException should have been thrown.");
        }
        catch(ConstraintViolationException e) {
            // expected
        }
    }
    
    @Test
    public void testGetNamedBlob()
    {
        String category = "category";
        String name = "name";
        
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category));
        
        namedBlobManagerProxy.createNamedBlob(category, name);

        assertEquals(1, namedBlobManagerProxy.getByCategoryCount(category));
        
        NamedBlob blob = namedBlobManagerProxy.getNamedBlob(category, name);
        
        assertNotNull(blob);
        assertEquals(category, blob.getCategory());
        assertEquals(name, blob.getName());

        assertEquals(1, namedBlobManagerProxy.getByCategoryCount(category));
    }
    
    @Test
    public void testDeleteNamedBlob()
    {
        String category = "category";
        String name = "name";
        String name2 = "name2";
        
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category));
        
        namedBlobManagerProxy.createNamedBlob(category, name);
        namedBlobManagerProxy.createNamedBlob(category, name2);

        assertEquals(2, namedBlobManagerProxy.getByCategoryCount(category));
        
        NamedBlob blob = namedBlobManagerProxy.getNamedBlob(category, name);
        
        assertNotNull(blob);
        
        namedBlobManagerProxy.deleteNamedBlob(category, name);

        assertEquals(1, namedBlobManagerProxy.getByCategoryCount(category));

        assertNull(namedBlobManagerProxy.getNamedBlob(category, name));
        assertNotNull(namedBlobManagerProxy.getNamedBlob(category, name2));
    }    

    @Test
    public void testGetByCategory()
    {
        String category1 = "category1";
        String category2 = "category2";
        
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category1));
        assertEquals(0, namedBlobManagerProxy.getByCategoryCount(category2));

        int toAdd = 9;
        
        for (int i = toAdd; i > 0; i--)
        {
            namedBlobManagerProxy.createNamedBlob(category1, "name" + i);
            namedBlobManagerProxy.createNamedBlob(category2, "other" + i);
        }
        
        assertEquals(toAdd, namedBlobManagerProxy.getByCategoryCount(category1));
        assertEquals(toAdd, namedBlobManagerProxy.getByCategoryCount(category2));
        
        List<? extends NamedBlob> blobs1 = namedBlobManagerProxy.getByCategory(category1, null, null);
        
        assertEquals(toAdd, blobs1.size());

        for (int i = 0; i < toAdd; i++)
        {
            assertEquals(category1, blobs1.get(i).getCategory());
            assertEquals("name" + (i + 1), blobs1.get(i).getName());
        }
        
        List<? extends NamedBlob> blobs2 = namedBlobManagerProxy.getByCategory(category2, null, null);
        
        assertEquals(toAdd, blobs2.size());

        for (int i = 0; i < toAdd; i++)
        {
            assertEquals(category2, blobs2.get(i).getCategory());
            assertEquals("other" + (i + 1), blobs2.get(i).getName());
        }
        
        // now test with only a few results
        int firstResult = 2;
        int maxResults = 3;
        
        blobs1 = namedBlobManagerProxy.getByCategory(category1, firstResult, maxResults);
        
        assertEquals(maxResults, blobs1.size());
        
        for (int i = 0; i < maxResults; i++)
        {
            assertEquals(category1, blobs1.get(i).getCategory());
            assertEquals("name" + (i + 1 + firstResult), blobs1.get(i).getName());
        }
    }        
    
    @Test
    public void testAddChildBlobs()
    {
        String parentCategory = "parentCategory";
        String parentName = "parentName";
        String childCategory = "childCategory";
        String childName = "childName";
        
        namedBlobManagerProxy.createNamedBlob(parentCategory, parentName);
        namedBlobManagerProxy.createNamedBlob(childCategory, childName);
        
        namedBlobManagerProxy.addChild(parentCategory, parentName, childCategory, childName);
        
        namedBlobManagerProxy.assertChildSize(parentCategory, parentName, 1);
        
        assertEquals(1, namedBlobManagerProxy.getReferencedByCount(childCategory, childName));
    }
    
    @Test
    public void testDeleteChildInUse()
    {
        String category = "category";
        String parentName = "parentName";
        String childName = "childName";
        
        namedBlobManagerProxy.createNamedBlob(category, parentName);
        namedBlobManagerProxy.createNamedBlob(category, childName);
        
        namedBlobManagerProxy.addChild(category, parentName, category, childName);
        
        namedBlobManagerProxy.assertChildSize(category, parentName, 1);
        
        assertEquals(1, namedBlobManagerProxy.getReferencedByCount(category, childName));
        
        try {
            namedBlobManagerProxy.deleteNamedBlob(category, childName);
            
            fail("should fail.");
        }
        catch (ConstraintViolationException e) {
            // expected 
        }
        
        namedBlobManagerProxy.deleteChild(category, parentName, category, childName);
        namedBlobManagerProxy.deleteNamedBlob(category, childName);
        namedBlobManagerProxy.assertChildSize(category, parentName, 0);
    }
    
    @Test
    public void testReferencedBy()
    {
        String category = "category";
        String parent1 = "parent1";
        String parent2 = "parent2";
        String child = "childName";
        
        namedBlobManagerProxy.createNamedBlob(category, parent1);
        namedBlobManagerProxy.createNamedBlob(category, parent2);
        namedBlobManagerProxy.createNamedBlob(category, child);
        
        namedBlobManagerProxy.addChild(category, parent1, category, child);
        namedBlobManagerProxy.addChild(category, parent2, category, child);
        
        assertEquals(2, namedBlobManagerProxy.getReferencedByCount(category, child));
        
        List<NamedBlob> blobs = namedBlobManagerProxy.getReferencedBy(category, child, null, null);
        
        assertEquals(2, blobs.size());
        
        NamedBlob blob = blobs.get(0);
        
        assertEquals(parent1, blob.getName());

        blob = blobs.get(1);
        
        assertEquals(parent2, blob.getName());

        blobs = namedBlobManagerProxy.getReferencedBy(category, child, 1, 1);

        assertEquals(1, blobs.size());
        
        blob = blobs.get(0);
        
        assertEquals(parent2, blob.getName());
                
        blobs = namedBlobManagerProxy.getReferencedBy(category, child, 1, null);

        assertEquals(1, blobs.size());
        
        blob = blobs.get(0);
        
        assertEquals(parent2, blob.getName());
 
        blobs = namedBlobManagerProxy.getReferencedBy(category, child, 0, 1);

        assertEquals(1, blobs.size());
        
        blob = blobs.get(0);
        
        assertEquals(parent1, blob.getName());
        
        namedBlobManagerProxy.deleteChild(category, parent1, category, child);
        
        assertEquals(1, namedBlobManagerProxy.getReferencedByCount(category, child));
        
        blobs = namedBlobManagerProxy.getReferencedBy(category, child, null, null);

        assertEquals(1, blobs.size());
        
        blob = blobs.get(0);
        
        assertEquals(parent2, blob.getName());
        
        namedBlobManagerProxy.deleteChild(category, parent2, category, child);
        
        assertEquals(0, namedBlobManagerProxy.getReferencedByCount(category, child));
        
        blobs = namedBlobManagerProxy.getReferencedBy(category, child, null, null);
        
        assertEquals(0, blobs.size());
    }
}
