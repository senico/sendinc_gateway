/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBException;

import mitm.common.cache.SimpleMemoryPatternCache;
import mitm.common.dlp.MatchFilter;
import mitm.common.dlp.MatchFilterRegistry;
import mitm.common.dlp.PolicyPatternMarshaller;
import mitm.common.dlp.impl.matchfilter.MaskingFilter;
import mitm.common.util.MarshallException;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternMarshallerTest
{
    private static PolicyPatternMarshaller marshaller;
    
    @BeforeClass
    public static void beforeClass()
    throws JAXBException
    {
        MatchFilterRegistry matchFilterRegistry = new MatchFilterRegistryImpl(Collections.singletonList(
                (MatchFilter) new MaskingFilter()));
        
        marshaller = new PolicyPatternMarshallerImpl(new SimpleMemoryPatternCache(), matchFilterRegistry);
    }
    
    @Test
    public void testMarshall()
    throws UnsupportedEncodingException, MarshallException
    {
        PolicyPatternImpl policyPattern = new PolicyPatternImpl();
        
        policyPattern.setName("test name");
        policyPattern.setDescription("test & pattern");
        policyPattern.setMatchFilter(new MaskingFilter());
        policyPattern.setPattern(Pattern.compile(".*"));
        policyPattern.setThreshold(10);
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        marshaller.marshall(policyPattern, bos);
        
        String xml = new String(bos.toByteArray(), "US-ASCII");
        
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><PolicyPattern>" + 
                "<description>test &amp; pattern</description><matchFilterName>Mask</matchFilterName>" + 
                "<name>test name</name><regExp>.*</regExp><threshold>10</threshold></PolicyPattern>", xml);
    }
    
    @Test
    public void testUnmarshall()
    throws UnsupportedEncodingException, MarshallException
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><PolicyPattern>" + 
            "<description>test &amp; pattern</description><matchFilterName>Mask</matchFilterName>" + 
            "<name>test name</name><regExp>.*</regExp><threshold>10</threshold></PolicyPattern>";
        
        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                xml.getBytes("US-ASCII")));
        
        assertNotNull(policyPattern);
        assertEquals("test name", policyPattern.getName());
        assertEquals("test & pattern", policyPattern.getDescription());
        assertEquals("Mask", policyPattern.getMatchFilterName());
        assertEquals(".*", policyPattern.getRegExp());
        assertEquals(10, policyPattern.getThreshold());
        assertEquals(".*", policyPattern.getPattern().pattern());
        
        MatchFilter matchFilter = policyPattern.getMatchFilter(); 
        assertNotNull(matchFilter);
        assertEquals("Mask", matchFilter.getName());
    }
    
    @Test
    public void testUnmarshallNoRegExpNoMatchFilter()
    throws UnsupportedEncodingException, MarshallException
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><PolicyPattern>" + 
            "<description>test &amp; pattern</description>" + 
            "<name>test name</name><threshold>10</threshold></PolicyPattern>";
        
        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                xml.getBytes("US-ASCII")));
        
        assertNotNull(policyPattern);
        assertNull(policyPattern.getPattern());
        assertNull(policyPattern.getMatchFilter());
    }    
    
    @Test
    public void testUnmarshallSpeedTest()
    throws UnsupportedEncodingException, MarshallException
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><PolicyPattern>" + 
            "<description>test &amp; pattern</description><matchFilterName>Mask</matchFilterName>" + 
            "<name>test name</name><regExp>.*</regExp><threshold>10</threshold></PolicyPattern>";

        final int repeat = 5000;
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < repeat; i++)
        {
            PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                    xml.getBytes("US-ASCII")));
            
            assertNotNull(policyPattern);
        }
        
        double perSec = repeat * 1000.0 / (System.currentTimeMillis() - start);
        
        System.out.println("per/second: " + perSec);
        
        assertTrue("can fail in slower systems.", perSec > 2000);
    }
}
