/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import mitm.common.dlp.TextNormalizer;
import mitm.common.dlp.WordSkipper;
import mitm.common.dlp.impl.FileWordSkipper;
import mitm.common.dlp.impl.TextNormalizerImpl;

import org.junit.BeforeClass;
import org.junit.Test;


public class TextNormalizerImplTest
{
    private static WordSkipper wordSkipper;
    
    @BeforeClass
    public static void setUpBeforeClass()
    throws IOException 
    {
        wordSkipper = new FileWordSkipper(new File("conf/skip-words.txt"));
    }
    
    @Test
    public void testNormalize()
    throws IOException
    {
        String input = " teSt 123\u00A0\u0000AND\r45,67 \t\n    NOT\np\n";
        
        TextNormalizer normalizer = new TextNormalizerImpl(wordSkipper);
        
        StringWriter sw = new StringWriter();
        
        normalizer.normalize(new StringReader(input), sw);
        
        assertEquals("test 123 45,67 p ", sw.toString());
    }

    @Test
    public void testNormalizeUnicode()
    throws IOException
    {
        String input = " teSt 123 \u0041\u0301 456";
        
        TextNormalizer normalizer = new TextNormalizerImpl(wordSkipper);
        
        StringWriter sw = new StringWriter();
        
        normalizer.normalize(new StringReader(input), sw);
        
        assertEquals("test 123 á 456 ", sw.toString());
    }
}
