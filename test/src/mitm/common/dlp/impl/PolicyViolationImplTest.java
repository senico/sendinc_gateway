/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;

import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.util.Base64Utils;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.SerializationUtils;
import org.junit.Test;

/**
 * Test whether PolicyViolationImpl can be correctly serialized/deserialized.
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyViolationImplTest
{
    
    private static final String SERIALIZED = 
        "rO0ABXNyAChtaXRtLmNvbW1vbi5kbHAuaW1wbC5Qb2xpY3lWaW9sYXRpb25JbXBsdI/FAUfprp4CAARMAAVtYXRjaHQ" +
        "AEkxqYXZhL2xhbmcvU3RyaW5nO0wABnBvbGljeXEAfgABTAAIcHJpb3JpdHl0AClMbWl0bS9jb21tb24vZGxwL1BvbGl" +
        "jeVZpb2xhdGlvblByaW9yaXR5O0wABHJ1bGVxAH4AAXhwdAAFbWF0Y2h0AAZwb2xpY3l+cgAnbWl0bS5jb21tb24uZGx" +
        "wLlBvbGljeVZpb2xhdGlvblByaW9yaXR5AAAAAAAAAAASAAB4cgAOamF2YS5sYW5nLkVudW0AAAAAAAAAABIAAHhwdAA" +
        "KUVVBUkFOVElORXQABHJ1bGU=";
    
    private static final String SERIALIZED_NULLS = 
        "rO0ABXNyAChtaXRtLmNvbW1vbi5kbHAuaW1wbC5Qb2xpY3lWaW9sYXRpb25JbXBsdI/FAUfprp4CAARMAAVtYXRjaHQAE" +
        "kxqYXZhL2xhbmcvU3RyaW5nO0wABnBvbGljeXEAfgABTAAIcHJpb3JpdHl0AClMbWl0bS9jb21tb24vZGxwL1BvbGljeV" +
        "Zpb2xhdGlvblByaW9yaXR5O0wABHJ1bGVxAH4AAXhwcHBwcA==";
    
    @Test
    public void testSerialize()
    {
        PolicyViolation violation = new PolicyViolationImpl("policy", "rule", "match", PolicyViolationPriority.QUARANTINE);
     
        byte[] serialized = SerializationUtils.serialize(violation);
        
        assertEquals(SERIALIZED, MiscStringUtils.toAsciiString((Base64.encodeBase64(serialized))));
        
        violation = (PolicyViolation) SerializationUtils.deserialize(serialized);
    }

    /*
     * Not really a test. Is ues to generate the base64 encoded blob used for config.xml so a 
     * PolicyViolation collection can be added
     */
    @Test
    public void testSerializeCollection()
    {
        LinkedList<PolicyViolation> violations = new LinkedList<PolicyViolation>();
        
        violations.add(new PolicyViolationImpl("built in", "The message could not be encrypted", null, 
                PolicyViolationPriority.QUARANTINE));
     
        System.out.println(Base64Utils.encodeChunked(SerializationUtils.serialize(violations)));
    }
    
    @Test
    public void testDeserialize()
    {
        PolicyViolation violation = (PolicyViolation) SerializationUtils.deserialize(Base64.decodeBase64(
                MiscStringUtils.toAsciiBytes(SERIALIZED)));
        
        assertEquals("policy", violation.getPolicy());
        assertEquals("rule", violation.getRule());
        assertEquals("match", violation.getMatch());
        assertEquals(PolicyViolationPriority.QUARANTINE, violation.getPriority());
    }
    
    @Test
    public void testSerializeNulls()
    {
        PolicyViolation violation = new PolicyViolationImpl(null, null, null, null);
     
        byte[] serialized = SerializationUtils.serialize(violation);
        
        assertEquals(SERIALIZED_NULLS, MiscStringUtils.toAsciiString((Base64.encodeBase64(serialized))));
        
        violation = (PolicyViolation) SerializationUtils.deserialize(serialized);
    }
    
    @Test
    public void testDeserializeNulls()
    {
        PolicyViolation violation = (PolicyViolation) SerializationUtils.deserialize(Base64.decodeBase64(
                MiscStringUtils.toAsciiBytes(SERIALIZED_NULLS)));
        
        assertNull(violation.getPolicy());
        assertNull(violation.getRule());
        assertNull(violation.getMatch());
        assertNull(violation.getPriority());
    }
}
