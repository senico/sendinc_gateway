/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.dlp.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import mitm.common.dlp.PolicyChecker;
import mitm.common.dlp.PolicyCheckerContext;
import mitm.common.dlp.PolicyCheckerPipeline;
import mitm.common.dlp.PolicyPattern;
import mitm.common.dlp.PolicyViolation;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.dlp.PolicyViolationException;
import mitm.common.mail.EmailAddressUtils;

import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class PolicyCheckerPipelineImplTest
{
    private static class AlwaysFailPolicyChecker implements PolicyChecker
    {
        @Override
        public String getName() {
            return null;
        }

        @Override
        public void init(PolicyCheckerContext context) {
        }

        @Override
        public void update(PolicyCheckerContext context) {
        }
        
        @Override
        public void finish(PolicyCheckerContext context)
        throws PolicyViolationException
        {
            PolicyViolationException e = new PolicyViolationException("Always fail");
            
            e.addViolation(new PolicyViolationImpl("AlwaysFail", "AlwaysFailRule", "AlwaysFailMatch", 
                    PolicyViolationPriority.BLOCK));
            
            throw e;
        }
    }
    
    @Test
    public void testPipeline()
    {
        PolicyCheckerPipeline pipeline = new PolicyCheckerPipelineImpl(Collections.singleton((PolicyChecker) 
                new RegExpPolicyChecker()));
                
        PolicyCheckerContext context = new PolicyCheckerContextImpl();
        
        Collection<PolicyPattern> patterns = new LinkedList<PolicyPattern>();

        PolicyPatternImpl pattern = new PolicyPatternImpl("email", Pattern.compile(EmailAddressUtils.EMAIL_REG_EXPR)); 
        
        pattern.setPriority(PolicyViolationPriority.MUST_ENCRYPT);
        
        patterns.add(pattern);
        
        context.setContent("test1@example.com");
        context.setPatterns(patterns);
        
        pipeline.init(context);
        
        pipeline.update(context);
        context.setContent(" test2@example.com");
        context.setPartial(true);
        
        pipeline.update(context);
        
        try {
            pipeline.finish(context);
            
            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());
            
            List<PolicyViolation> violations = e.getViolations();
            
            assertNotNull(violations);
            
            assertEquals(1, violations.size());
            
            // because of overlap, test1@example.com will be matched two times
            assertEquals("Policy: RegExp, Rule: email, Priority: MUST_ENCRYPT, Match: test1@example.com, " +
            		"test1@example.com, test2@example.com", violations.get(0).toString());
        }
    }
    
    @Test
    public void testSlowFailPipeline()
    {
        PolicyCheckerPipeline pipeline = new PolicyCheckerPipelineImpl();
        
        assertFalse(pipeline.isFastFail());
        pipeline.addPolicyChecker(new RegExpPolicyChecker());
        pipeline.addPolicyChecker(new AlwaysFailPolicyChecker());
        
        PolicyCheckerContext context = new PolicyCheckerContextImpl();
        
        Collection<PolicyPattern> patterns = new LinkedList<PolicyPattern>();

        PolicyPatternImpl pattern = new PolicyPatternImpl("email", Pattern.compile(EmailAddressUtils.EMAIL_REG_EXPR)); 
        
        patterns.add(pattern);
        
        context.setContent("test1@example.com");
        context.setPatterns(patterns);
        
        pipeline.init(context);
        
        pipeline.update(context);
        
        try {
            pipeline.finish(context);
            
            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());
            
            List<PolicyViolation> violations = e.getViolations();
            
            assertNotNull(violations);
            
            assertEquals(2, violations.size());
            
            assertEquals("Policy: RegExp, Rule: email, Priority: , Match: test1@example.com", violations.get(0).toString());
            assertEquals("Policy: AlwaysFail, Rule: AlwaysFailRule, Priority: BLOCK, Match: AlwaysFailMatch", 
                    violations.get(1).toString());
        }
    }
    
    @Test
    public void testFastFailPipeline()
    {
        PolicyCheckerPipeline pipeline = new PolicyCheckerPipelineImpl();
        
        assertFalse(pipeline.isFastFail());
        pipeline.setFastFail(true);
        pipeline.addPolicyChecker(new RegExpPolicyChecker());
        pipeline.addPolicyChecker(new AlwaysFailPolicyChecker());
        
        PolicyCheckerContext context = new PolicyCheckerContextImpl();
        
        Collection<PolicyPattern> patterns = new LinkedList<PolicyPattern>();

        PolicyPatternImpl pattern = new PolicyPatternImpl("email", Pattern.compile(EmailAddressUtils.EMAIL_REG_EXPR)); 
        
        patterns.add(pattern);
        
        context.setContent("test1@example.com");
        context.setPatterns(patterns);
        
        pipeline.init(context);
        
        pipeline.update(context);
        
        try {
            pipeline.finish(context);
            
            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());
            
            List<PolicyViolation> violations = e.getViolations();
            
            assertNotNull(violations);
            
            assertEquals(1, violations.size());
            
            assertEquals("Policy: RegExp, Rule: email, Priority: , Match: test1@example.com", violations.get(0).toString());
        }
    }    
}
