/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.pdf;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessagePDFBuilderTest
{
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");

    private static FontProvider fontProvider;
    
    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        BasicConfigurator.configure();
        
        fontProvider = new FileFontProvider(new File("resources/fonts/"));
    }
    
    private MessagePDFBuilder createMessagePDFBuilder()
    {
        MessagePDFBuilder pdfBuilder = new MessagePDFBuilder();

        pdfBuilder.setFontProvider(fontProvider);
        
        return pdfBuilder;
    }

    @Test
    public void testEncodedFilename()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "message-with-attach-encoded-filename.pdf"));

        String filename = "mail/message-with-attach-encoded-filename.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testISO8859_9()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "test8859-9.pdf"));

        String filename = "mail/8859-9.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testUmlautsISO885915()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testUmlautsISO885915.pdf"));

        String filename = "mail/email-with-umlauts-iso-8859-15.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testUnicode()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testUnicode.pdf"));

        String filename = "mail/unicode.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }

    @Test
    public void testKoi8()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testKoi8.pdf"));

        String filename = "mail/message-koi8-r.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testMessageWithLinks()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testMessageWithLinks.pdf"));

        String filename = "mail/message-with-links.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }    

    @Test
    public void testAttachedMessage()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testAttachedMessage.pdf"));

        String filename = "mail/attached-message.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }    
    
    @Test
    public void testMessageChineseSubject()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testMessageChineseSubject.pdf"));

        String filename = "mail/chinese-text.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }

    @Test
    public void testMessageChineseFrom()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testMessageChineseFrom.pdf"));

        String filename = "mail/chinese-encoded-from.eml";

        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testMessageWithAttachment()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "testSimpleMessage.pdf"));

        String filename = "mail/normal-message-with-attach.eml";
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }
    
    @Test
    public void testMessageWithMultipleAttachments()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "multiple-attachments.pdf"));

        String filename = "mail/multiple-attachments.eml";
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, "http://www.example.com", pdfOutput);
        
        pdfOutput.close();
    }

    @Test
    public void testHtmlOnly()
    throws Exception
    {
        MessagePDFBuilder pdfBuilder = createMessagePDFBuilder();
        
        OutputStream pdfOutput = new FileOutputStream(new File(tempDir, "HtmlOnly.pdf"));

        String filename = "mail/html-only.eml";
        File mail = new File(testBase, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
                
        pdfBuilder.buildPDF(message, null, pdfOutput);
        
        pdfOutput.close();
    }

    @Test
    public void speedTest()
    throws Exception
    {
        // call one time to init fonts etc.
        testISO8859_9();

        int repeat = 1000;
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < repeat; i++) {
            testISO8859_9();
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = repeat * 1000.0 / diff;
        
        System.out.println("builds/sec: " + perSecond);
        
        /*
         * NOTE: !!! can fail on a slower system
         * 
         * On my Quad CPU Q8300, 175 /sec without extra fonts
         */
        assertTrue("MessagePDFBuilder too slow. !!! this can fail on a slower system !!!", perSecond > 100);        
    }
}
