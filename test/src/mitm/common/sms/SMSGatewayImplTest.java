/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;

import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.sms.hibernate.SMSGatewayImpl;
import mitm.common.sms.hibernate.SMSImpl;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMSGatewayImplTest
{
    private static HibernateSessionSource sessionSource;
    
    private static DummySMSTransport transport = new DummySMSTransport();
    
    private static SMSGatewayImpl gateway;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static SMS expired;

    private static class SMSExpiredListenerImpl implements SMSExpiredListener
    {
        @Override
        public void expired(SMS sms) {
            expired = sms;
        }        
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws InstantiationException, IllegalAccessException, ClassNotFoundException, DatabaseException, SMSGatewayException 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        gateway = new SMSGatewayImpl(transport, new SMSExpiredListenerImpl(), sessionManager);
        
        gateway.setExpirationTime(DateUtils.MILLIS_PER_SECOND * 5);
        
        // UpdateInterval must be 0 for this test
        gateway.setUpdateInterval(0);
        
        gateway.start();
    }

    @Test
    public void testGateway() 
    throws SMSGatewayException, InterruptedException
    {
        assertEquals(0, transport.getPhoneNumbers().size());

        SMS sms1 = new SMSImpl("123", "test 1", null);
        SMS sms2 = new SMSImpl("456", "test 2", null);
        
        
        gateway.sendSMS(sms1);
        gateway.sendSMS(sms2);
        
        // wait some time for the message to be sent
        Thread.sleep(1000);
        
        assertEquals(2, transport.getPhoneNumbers().size());
        
        transport.getPhoneNumbers().clear();
        transport.getMessages().clear();
        
        SMS smsFail = new SMSImpl("789", "fail", null);

        assertNull(expired);
        
        gateway.sendSMS(sms1);
        gateway.sendSMS(sms2);
        gateway.sendSMS(smsFail);
        
        // wait some time for the message to be sent
        Thread.sleep(1000);

        assertEquals(2, transport.getPhoneNumbers().size());

        // wait some time for the sms to expire
        Thread.sleep(5000);
        
        assertNotNull(expired);
        assertEquals("789", expired.getPhoneNumber());
        assertEquals("fail", expired.getMessage());
    }
}
