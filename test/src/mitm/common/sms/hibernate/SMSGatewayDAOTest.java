/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.sms.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Date;
import java.util.List;

import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.SortDirection;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.sms.SMS;
import mitm.common.sms.SortColumn;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMSGatewayDAOTest
{
    private static HibernateSessionSource sessionSource;
    private static DatabaseActionExecutor executor;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws InstantiationException, IllegalAccessException, ClassNotFoundException, DatabaseException 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        executor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());        
    }

    @Before
    public void setup() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                deleteAllAction(session);
            }
        });
    }
    
    private static void deleteAllAction(Session session)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            dao.deleteAll();
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    private static Long addSMSAction(Session session, SMS sms, Date dateLastTry)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            SMSGatewayEntity entity = dao.addSMS(sms);
            
            if (dateLastTry != null) {
                entity.setDateLastTry(dateLastTry);
            }
            
            return (Long) session.getIdentifier(entity);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    private static int getSMSCountAction(Session session)
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            
            return dao.findAll().size();
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    private static SMSGatewayEntity loadByIDAction(Session session, Long id) 
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            SMSGatewayEntity entity = dao.findById(id);
            
            return entity;
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }        
    }

    private static SMSGatewayEntity getNextAvailableAction(Session session, Date notAfter) 
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            SMSGatewayEntity entity = dao.getNextAvailable(notAfter);
            
            return entity;
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }        
    }

    private static int getAvailableCountAction(Session session, Date notAfter) 
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            return dao.getAvailableCount(notAfter);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }        
    }
    
    private static List<SMSGatewayEntity> getExpiredAction(Session session, Date olderThan) 
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            List<SMSGatewayEntity> entities = dao.getExpired(olderThan);
            
            return entities;
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }        
    }
    
    private static List<SMS> getAllAction(Session session, Integer firstResult, Integer maxResults,
    		SortColumn sortColumn, SortDirection sortDirection) 
    throws DatabaseException
    {
        try {
            SMSGatewayDAO dao = new SMSGatewayDAO(SessionAdapterFactory.create(session));
            
            return dao.getAll(firstResult, maxResults, sortColumn, sortDirection);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }        
    }
    
    @Test
    public void testGetAllSMS()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -1));
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("1111", "asasadsa", null);
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        List<SMS> all = executor.executeTransaction(new DatabaseAction<List<SMS>>()
        {
            @Override
            public List<SMS> doAction(Session session) 
            throws DatabaseException 
            {
                return getAllAction(session, null, null, SortColumn.PHONENUMBER, SortDirection.ASC);
            }
        });
        
        assertEquals(3, all.size());
        assertEquals("1111", all.get(0).getPhoneNumber());
        assertEquals("123456", all.get(1).getPhoneNumber());
        assertEquals("789", all.get(2).getPhoneNumber());
        
        all = executor.executeTransaction(new DatabaseAction<List<SMS>>()
        {
            @Override
            public List<SMS> doAction(Session session) 
            throws DatabaseException 
            {
                return getAllAction(session, null, null, SortColumn.PHONENUMBER, SortDirection.DESC);
            }
        });

        assertEquals(3, all.size());
        assertEquals("789", all.get(0).getPhoneNumber());
        assertEquals("123456", all.get(1).getPhoneNumber());
        assertEquals("1111", all.get(2).getPhoneNumber());

        all = executor.executeTransaction(new DatabaseAction<List<SMS>>()
        {
            @Override
            public List<SMS> doAction(Session session) 
            throws DatabaseException 
            {
                return getAllAction(session, 1, 2, SortColumn.PHONENUMBER, SortDirection.DESC);
            }
        });

        assertEquals(2, all.size());
        assertEquals("123456", all.get(0).getPhoneNumber());
        assertEquals("1111", all.get(1).getPhoneNumber());

        all = executor.executeTransaction(new DatabaseAction<List<SMS>>()
        {
            @Override
            public List<SMS> doAction(Session session) 
            throws DatabaseException 
            {
                return getAllAction(session, 1, 2, SortColumn.PHONENUMBER, SortDirection.ASC);
            }
        });

        assertEquals(2, all.size());
        assertEquals("123456", all.get(0).getPhoneNumber());
        assertEquals("789", all.get(1).getPhoneNumber());
    }
    
    @Test
    public void testGetAvailableCount()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -1));
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        
        final Date notAfter = new Date();
        
        int count = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getAvailableCountAction(session, notAfter);
            }
        });
        
        assertEquals(2, count);
    }

    @Test
    public void testGetAvailableCountNullDate()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -1));
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        
        final Date notAfter = null;
        
        int count = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getAvailableCountAction(session, notAfter);
            }
        });
        
        assertEquals(2, count);
    }
    
    @Test
    public void testGetAvailableCountOneMatch()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), 1));
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        
        final Date notAfter = new Date();
        
        int count = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getAvailableCountAction(session, notAfter);
            }
        });
        
        assertEquals(1, count);
    }

    @Test
    public void testGetAvailableCountNoRecords()
    throws DatabaseException 
    {
        final Date notAfter = new Date();
        
        int count = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getAvailableCountAction(session, notAfter);
            }
        });
        
        assertEquals(0, count);
    }
        
    @Test
    public void testGetExpired()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, new Date());
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, new Date());
            }
        });
        
        
        final Date olderThan = new Date();
        
        List<SMSGatewayEntity> entities = executor.executeTransaction(new DatabaseAction<List<SMSGatewayEntity>>()
        {
            @Override
            public List<SMSGatewayEntity> doAction(Session session) 
            throws DatabaseException 
            {
                return getExpiredAction(session, olderThan);
            }
        });
        
        assertNotNull(entities);
        assertEquals(2, entities.size());
    }

    @Test
    public void testGetExpiredNoExpired()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, new Date());
            }
        });

        final Date olderThan = DateUtils.addHours(new Date(), -1);
        
        List<SMSGatewayEntity> entities = executor.executeTransaction(new DatabaseAction<List<SMSGatewayEntity>>()
        {
            @Override
            public List<SMSGatewayEntity> doAction(Session session) 
            throws DatabaseException 
            {
                return getExpiredAction(session, olderThan);
            }
        });
        
        assertNotNull(entities);
        assertEquals(0, entities.size());
    }
    
    @Test
    public void testGetAvailableOldest()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -1));
            }
        });

        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("789", "qwerty", "111");
                
                return addSMSAction(session, sms, DateUtils.addHours(new Date(), -2));
            }
        });
        
        
        final Date notAfter = new Date();
        
        SMSGatewayEntity entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return getNextAvailableAction(session, notAfter);
            }
        });
        
        assertNotNull(entity);
        assertEquals("789", entity.getPhoneNumber());
    }

    @Test
    public void testGetAvailableNotYetValid()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, new Date());
            }
        });
        
        final Date notAfter1 = DateUtils.addHours(new Date(), -1);
        
        SMSGatewayEntity entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return getNextAvailableAction(session, notAfter1);
            }
        });
        
        assertNull(entity);
        
        final Date notAfter2 = DateUtils.addHours(new Date(), 1);
        
        entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return getNextAvailableAction(session, notAfter2);
            }
        });

        assertNotNull(entity);
        assertEquals("123456", entity.getPhoneNumber());
    }
    
    @Test
    public void testGetAvailableNullTryDate()
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, null);
            }
        });
        
        final Date notAfter = new Date();
        
        SMSGatewayEntity entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return getNextAvailableAction(session, notAfter);
            }
        });
        
        assertNotNull(entity);
        assertEquals("123456", entity.getPhoneNumber());
    }
    
    @Test
    public void testGetAvailableNoRecords() 
    throws DatabaseException 
    {
        final Date notAfter = new Date();
        
        SMSGatewayEntity entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return getNextAvailableAction(session, notAfter);
            }
        });
        
        assertNull(entity);
    }
    
    @Test
    public void testAddSMS() 
    throws DatabaseException 
    {
        final Long id = executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", "some extra data");
                
                return addSMSAction(session, sms, null);
            }
        });
        
        assertNotNull(id);
        
        SMSGatewayEntity entity = executor.executeTransaction(new DatabaseAction<SMSGatewayEntity>()
        {
            @Override
            public SMSGatewayEntity doAction(Session session) 
            throws DatabaseException 
            {
                return loadByIDAction(session, id);
            }
        });

        SMS sms = SMSGatewayDAO.entityToSMS(entity);
        
        assertEquals(id, sms.getID());
        
        assertEquals("123456", entity.getPhoneNumber());
        assertEquals("sms body", entity.getMessage());
        
        assertNotNull(entity.getData());
        Object deserialized = SerializationUtils.deserialize(entity.getData());
        assertTrue(deserialized instanceof String);
        assertEquals("some extra data", (String) deserialized);
    }
    
    @Test
    public void testDeleteAll() 
    throws DatabaseException 
    {
        int size = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getSMSCountAction(session);
            }
        });

        assertEquals(0, size);
        
        executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                SMS sms = new SMSImpl("123456", "sms body", null);
                
                return addSMSAction(session, sms, null);
            }
        });

        size = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getSMSCountAction(session);
            }
        });
        
        assertEquals(1, size);
        
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                deleteAllAction(session);
            }
        });

        size = executor.executeTransaction(new DatabaseAction<Integer>()
        {
            @Override
            public Integer doAction(Session session) 
            throws DatabaseException 
            {
                return getSMSCountAction(session);
            }
        });
        
        assertEquals(0, size);
    }
}
