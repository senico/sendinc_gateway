/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.certificate.impl.StandardSerialNumberGenerator;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Generates a large PFX file containing certificates and keys. 
 * 
 * Note: This test is just for generating the PFX file and should not be added to the AllTests 
 */
public class GenerateBulkPFX
{
    private static SecurityFactory securityFactory;
    
    private static X509Certificate rootCertificate;
    private static X509Certificate caCertificate;
    private static PrivateKey caPrivateKey;
    
    private static SecureRandom randomSource;
    private static KeyPairGenerator keyPairGenerator;
    private static KeyStore keyStore;

    private static SerialNumberGenerator serialNumberGenerator;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        randomSource = securityFactory.createSecureRandom();
        
        keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
        keyPairGenerator.initialize(1024, randomSource);

        serialNumberGenerator = new StandardSerialNumberGenerator();
        
        keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(null);
        
        loadCA();
    }

    private static void loadCA() 
    throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, 
    CertificateException, IOException, UnrecoverableKeyException 
    {
        KeyStore caKeyStore = securityFactory.createKeyStore("PKCS12");
        
        File file = new File("test/resources/testdata/keys/testCA.p12");
        
        FileInputStream input = new FileInputStream(file);
        
        caKeyStore.load(input, "test".toCharArray());
        
        rootCertificate = (X509Certificate) caKeyStore.getCertificate("root");
        caCertificate = (X509Certificate) caKeyStore.getCertificate("ca");
        caPrivateKey = (PrivateKey) caKeyStore.getKey("ca", null);
        
        assertNotNull(caCertificate);
        assertNotNull(caPrivateKey);
    }

    /*
     * Generates a valid certificate
     */
    private void generateCertificate(String email, String commonName, String alias) 
    throws Exception 
    {
        X509CertificateBuilder certificateBuilder = securityFactory.createX509CertificateBuilder();

        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        
        X500PrincipalBuilder subjectBuilder = new X500PrincipalBuilder();
                
        subjectBuilder.setCommonName(commonName);
        subjectBuilder.setEmail(email);
        subjectBuilder.setCountryCode("NL");
        subjectBuilder.setLocality("Amsterdam");
        subjectBuilder.setState("NH");
                
        AltNamesBuilder altNamesBuider = new AltNamesBuilder();        
        altNamesBuider.setRFC822Names(email);
        
        X500Principal subject = subjectBuilder.buildPrincipal();
        GeneralNames altNames = altNamesBuider.buildAltNames();

        Set<KeyUsageType> keyUsage = new TreeSet<KeyUsageType>();
        
        keyUsage.add(KeyUsageType.DIGITALSIGNATURE);
        keyUsage.add(KeyUsageType.KEYENCIPHERMENT);
        keyUsage.add(KeyUsageType.NONREPUDIATION);
        
        Set<ExtendedKeyUsageType> extendedKeyUsage = new TreeSet<ExtendedKeyUsageType>();
        
        extendedKeyUsage.add(ExtendedKeyUsageType.CLIENTAUTH);
        extendedKeyUsage.add(ExtendedKeyUsageType.EMAILPROTECTION);
        
        BigInteger serialNumber = serialNumberGenerator.generate();

        Date now = new Date();
        
        certificateBuilder.setSubject(subject);
        certificateBuilder.setAltNames(altNames, true);
        certificateBuilder.setKeyUsage(keyUsage, true);        
        certificateBuilder.setExtendedKeyUsage(extendedKeyUsage, false);
        certificateBuilder.setNotBefore(now);
        certificateBuilder.setNotAfter(DateUtils.addYears(now, 20));
        certificateBuilder.setPublicKey(keyPair.getPublic());
        certificateBuilder.setSerialNumber(serialNumber);
        certificateBuilder.setSignatureAlgorithm("SHA1WithRSAEncryption");
        certificateBuilder.addSubjectKeyIdentifier(true);
        
        X509Certificate certificate = certificateBuilder.generateCertificate(caPrivateKey, caCertificate);
     
        assertNotNull(certificate);
        
        Certificate[] chain = new Certificate[] {certificate, caCertificate, rootCertificate};
        
        keyStore.setKeyEntry(alias, keyPair.getPrivate(), null, chain);
    }
    
    @Test
    public void generatePFX() 
    throws Exception 
    {
        int nrOfCertificates = 10000;
        
        for (int i = 0; i < nrOfCertificates; i++)
        {
            String email = "bulk" + i + "@example.com";
            String commonName = "test cert bulk " + i;
            
            generateCertificate(email, commonName, email);
        }
        
        File p12File = new File("test/tmp/bulkCertificates.p12");
        
        FileOutputStream output = new FileOutputStream(p12File);
        
        keyStore.store(output, "test".toCharArray());
    }
}
