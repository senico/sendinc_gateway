/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collection;

import mitm.common.security.asn1.ObjectEncoding;

import org.junit.Ignore;
import org.junit.Test;

public class CertificateEncoderTest
{
	/* 
	 * Test is ignored because it is just for testing the speed difference between (takes long time)
	 * two ways to encode
	 */
	@Ignore
    @Test
    public void testEncodingSpeed() 
    throws CertificateException, NoSuchProviderException, IOException 
    {
        File file = new File("test/resources/testdata/certificates/random-self-signed-10000.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        long startTime = System.currentTimeMillis();
        
        System.out.println("Start direct version.");
        
        // test direct version
        CertificateEncoder.encode(certificates, ObjectEncoding.DER);
        
        System.out.println("Duration: " + (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        
        System.out.println("Start CertPath version.");
        
        // test direct version
        CertificateEncoder.encode(certificates, "PKCS7");
        
        System.out.println("Duration: " + (System.currentTimeMillis() - startTime));
    }
	
	public void testEncodePKCS7()
	throws CertificateException, NoSuchProviderException, IOException
	{
        File file = new File("test/resources/testdata/certificates/random-self-signed-10.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
		
        byte[] encoded = CertificateEncoder.encode(certificates, ObjectEncoding.DER);
        
        assertNotNull(encoded);
     
        Collection<? extends Certificate> fromBytes = CertificateUtils.readCertificates(
        		new ByteArrayInputStream(encoded));
        
        assertEquals(10, fromBytes.size());
        
        assertTrue(certificates.containsAll(fromBytes));
	}

	public void testEncodePEM()
	throws CertificateException, NoSuchProviderException, IOException
	{
        File file = new File("test/resources/testdata/certificates/random-self-signed-10.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
		
        byte[] encoded = CertificateEncoder.encode(certificates, ObjectEncoding.PEM);
        
        assertNotNull(encoded);
     
        Collection<? extends Certificate> fromBytes = CertificateUtils.readCertificates(
        		new ByteArrayInputStream(encoded));
        
        assertEquals(10, fromBytes.size());
        
        assertTrue(certificates.containsAll(fromBytes));
	}
}
