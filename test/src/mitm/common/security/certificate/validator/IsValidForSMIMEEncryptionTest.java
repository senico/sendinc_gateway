/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import mitm.test.TestUtils;

import org.junit.Test;

public class IsValidForSMIMEEncryptionTest
{
    @Test
    public void testNoExtKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/rim.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertTrue(validator.isValid(certificate));
    }

    @Test
    public void testNullCertificate() 
    throws CertificateException
    {
        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertFalse(validator.isValid(null));
    }
    
    @Test
    public void testNoSMIMEExtKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/missingsmimekeyusage.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertFalse(validator.isValid(certificate));
    }

    @Test
    public void testNoKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/No_key_usage_MITM_Test_CA.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertTrue(validator.isValid(certificate));
    }

    @Test
    public void testIncorrectKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/mitm-test-root.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertFalse(validator.isValid(certificate));
    }

    @Test
    public void testIncorrectExtKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/gmail-ssl.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertFalse(validator.isValid(certificate));
    }

    @Test
    public void testAnyExtKeyUsage() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/any-ext-key-usage.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator validator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        
        assertTrue(validator.isValid(certificate));
    }
}
