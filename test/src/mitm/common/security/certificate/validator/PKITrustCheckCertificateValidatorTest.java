/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.ctl.CTL;
import mitm.common.security.ctl.CTLEntry;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.common.security.ctl.CTLException;
import mitm.common.security.ctl.CTLManager;
import mitm.common.util.BigIntegerUtils;
import mitm.test.TestUtils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PKITrustCheckCertificateValidatorTest 
{
    private final static File testBase = new File("test/resources/testdata");
    
    private static PKISecurityServices pKISecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        
        // TODO: do not use DjigzoTestUtils because otherwise this test depends on Djigzo packages
        DjigzoTestUtils.initialize();

        pKISecurityServices = SystemServices.getPKISecurityServices();
    }
        
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            SessionManager sessionManager = SystemServices.getSessionManager();

            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        private CTL getCTL() {
            return SystemServices.getCTLManager().getCTL(CTLManager.DEFAULT_CTL);
        }
        
        @StartTransaction
        public void clearAll() 
        throws Exception
        {
            pKISecurityServices.getKeyAndCertStore().removeAllEntries();
            pKISecurityServices.getRootStore().removeAllEntries();
            pKISecurityServices.getCRLStore().removeAllEntries();
            getCTL().deleteAll();
        }
        
        @StartTransaction
        public void addCertificateFile(X509CertStoreExt certStore, File file) 
        throws Exception
        {
            Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate)
                {
                    if (!certStore.contains((X509Certificate) certificate))
                    {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
        }

        @StartTransaction
        public void addCertificates(X509CertStoreExt certStore, File... files) 
        throws Exception
        {
            for (File file : files) {
                addCertificateFile(certStore, file);
            }
        }

        @StartTransaction
        public void addCRL(X509CRLStoreExt crlStore, File file) 
        throws Exception
        {
            X509CRL crl = TestUtils.loadX509CRL(file);

            crlStore.addCRL(crl);
        }
        
        @StartTransaction
        public PKITrustCheckCertificateValidator getValidator(X509Certificate certificate, Date date) 
        throws CertificateException
        {
            PKITrustCheckCertificateValidator validator = pKISecurityServices.
                    getPKITrustCheckCertificateValidatorFactory().createValidator(null);
            
            validator.setDate(date);
            
            validator.isValid(certificate);
            
            return validator;
        }
        
        @StartTransaction
        public X509Certificate getCertificate(X509CertStoreExt certStore, X509CertSelector selector) 
        throws CertStoreException
        {
            Collection<X509Certificate> certificates = certStore.getCertificates(selector);

            assertEquals(1, certificates.size());

            return certificates.iterator().next();
        }        
        
        @StartTransaction
        public void addCTLEntry(X509Certificate certificate, CTLEntryStatus status, boolean allowExpired)
        throws CTLException
        {
            CTL ctl = SystemServices.getCTLManager().getCTL(CTLManager.DEFAULT_CTL);
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            entry.setStatus(status);
            entry.setAllowExpired(allowExpired);
            
            ctl.addEntry(entry);
        }
    }
    
    @Before
    public void setup() 
    throws Exception
    {
        AutoTransactDelegator.createProxy().clearAll();
    }

    @Test
    public void testIntermediateBlackListed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();

        proxy.addCertificates(pKISecurityServices.getRootStore(), 
                new File(testBase, "certificates/mitm-test-root.cer"));

        proxy.addCertificates(pKISecurityServices.getKeyAndCertStore(), 
                new File(testBase, "certificates/mitm-test-ca.cer"),
                new File(testBase, "certificates/testCertificates.p7b"));
        
        File file = new File(testBase, "certificates/mitm-test-ca.cer");
        
        X509Certificate blacklistedCertificate = TestUtils.loadCertificate(file);        

        proxy.addCTLEntry(blacklistedCertificate, CTLEntryStatus.BLACKLISTED, false);
        
        Collection<X509Certificate> certificates = CertificateUtils.readX509Certificates(
                new File(testBase, "certificates/testCertificates.p7b"));

        PKITrustCheckCertificateValidator validator = proxy.getValidator(blacklistedCertificate, new Date()); 
        assertFalse(validator.isValid());
        assertTrue(validator.isBlackListed());
        assertEquals("Certificate found in CTL and is blacklisted.", validator.getFailureMessage());
        
        /*
         * All certs should be invalid because intermediate is blacklisted
         */
        for (X509Certificate certificate : certificates)
        {
            validator = proxy.getValidator(certificate, new Date()); 
            
            assertFalse(validator.isValid());
            
            if (validator.isBlackListed()) {
                assertEquals("Intermediate Certificate found in CTL and is blacklisted.", validator.getFailureMessage());
            }
        }
    }
    
    @Test
    public void testBlackListed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();

        proxy.addCertificates(pKISecurityServices.getRootStore(), 
                new File(testBase, "certificates/mitm-test-root.cer"));

        proxy.addCertificates(pKISecurityServices.getKeyAndCertStore(), 
                new File(testBase, "certificates/mitm-test-ca.cer"),
                new File(testBase, "certificates/testCertificates.p7b"));
        
        File file = new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        

        proxy.addCTLEntry(certificate, CTLEntryStatus.BLACKLISTED, false);
        
        PKITrustCheckCertificateValidator validator = proxy.getValidator(certificate, new Date()); 
        
        assertFalse(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertTrue(validator.isBlackListed());
    }
    
    @Test
    public void testNoChainWhiteListed()
    throws Exception
    {
        File file = new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        

        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();

        proxy.addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, false);
        
        PKITrustCheckCertificateValidator validator = proxy.getValidator(certificate, new Date()); 
        
        assertTrue(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertTrue(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testNoChainWhiteListedExpiredAllowed()
    throws Exception
    {
        File file = new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        

        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();

        proxy.addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, true);
        
        PKITrustCheckCertificateValidator validator = proxy.getValidator(certificate, new Date()); 
        
        assertTrue(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertTrue(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testNoChainWhiteListedExpiredNotAllowed()
    throws Exception
    {
        File file = new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        

        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();

        proxy.addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, false);
        
        PKITrustCheckCertificateValidator validator = proxy.getValidator(certificate, new Date()); 
        
        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }
    
    @Test
    public void testSelfSigned() 
    throws Exception
    {
        File file = new File(testBase, "certificates/equifax.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        
    	
        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, new Date()); 
        
        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }

    @Test
    public void testRevoked()
    throws Exception
    {
        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getRootStore(), 
        		new File(testBase, "certificates/mitm-test-root.cer"));

        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getKeyAndCertStore(), 
        		new File(testBase, "certificates/mitm-test-ca.cer"),
        		new File(testBase, "certificates/testCertificates.p7b"));
        
        AutoTransactDelegator.createProxy().addCRL(pKISecurityServices.getCRLStore(), new File(testBase, "crls/test-ca.crl"));
        
        File file = new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        
        
        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, new Date()); 
                
        assertFalse(validator.isValid());
        assertTrue(validator.isTrusted());
        assertTrue(validator.isRevoked());
    }
    
    @Test
    public void testValidCertificate()
    throws Exception
    {
        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getRootStore(), 
        		new File(testBase, "certificates/mitm-test-root.cer"));

        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getKeyAndCertStore(), 
        		new File(testBase, "certificates/mitm-test-ca.cer"),
        		new File(testBase, "certificates/testCertificates.p7b"));
        
        File file = new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        
        
        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, new Date()); 
        
        assertTrue(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }
    
    @Test
    public void testExpiredCertificate()
    throws Exception
    {
        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getRootStore(), 
        		new File(testBase, "certificates/mitm-test-root.cer"));

        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getKeyAndCertStore(), 
        		new File(testBase, "certificates/mitm-test-ca.cer"),
        		new File(testBase, "certificates/testCertificates.p7b"));
        
        File file = new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        
        
        Date future = TestUtils.parseDate("01-Nov-2040 07:39:35 GMT");

        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, future); 
        
        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }
    
    @Test
    public void testNotValidForSMIME()
    throws Exception
    {
        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getRootStore(), 
        		new File(testBase, "certificates/mitm-test-root.cer"));

        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getKeyAndCertStore(), 
        		new File(testBase, "certificates/mitm-test-ca.cer"),
        		new File(testBase, "certificates/testCertificates.p7b"));
        

        final X509CertSelector selector = new X509CertSelector();
    	
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
    	selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD035BA042503BCC6CA44680F9F8"));

        X509Certificate certificate = AutoTransactDelegator.createProxy().getCertificate(pKISecurityServices.getKeyAndCertStore(), 
                selector);
    	
        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, new Date()); 
    	        
        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }
    
    /*
     * Verisign adds a critcal Authority Key Identifier to the CRL. 
     * 
     * RFC 3280 explicitly says: 
     * 
     * 4.2.1.1  Authority Key Identifier
     * ....
     * This extension MUST NOT be marked critical.
     * 
     * We will therefore ignore this extension if it's critical. This test checks whether
     * the extension is ignored
     */
    @Test
    public void testCriticalCRLBug()
    throws Exception
    {
        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getRootStore(), 
                new File(testBase, "certificates/verisign-root-critical-crl-bug.cer"));

        AutoTransactDelegator.createProxy().addCertificates(pKISecurityServices.getKeyAndCertStore(), 
                new File(testBase, "certificates/verisign-intm-critical-crl-bug.cer"));
        
        AutoTransactDelegator.createProxy().addCRL(pKISecurityServices.getCRLStore(), 
                new File(testBase, "crls/verisign-critical-crl-bug.crl"));
        
        File file = new File(testBase, "certificates/verisign-end-critical-crl-bug.cer");
        
        final X509Certificate certificate = TestUtils.loadCertificate(file);        
        
        Date date = TestUtils.parseDate("17-Jun-2011 07:39:35 GMT");

        
        PKITrustCheckCertificateValidator validator = AutoTransactDelegator.createProxy().getValidator(certificate, date); 
                
        assertTrue(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }
}
