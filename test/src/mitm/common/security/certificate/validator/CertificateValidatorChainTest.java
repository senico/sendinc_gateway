/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import mitm.test.TestUtils;

import org.junit.Test;

public class CertificateValidatorChainTest
{
    @Test
    public void testCertificateValidatorChain() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/rim.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMESigning");
        
        CertificateValidatorChain validator = new CertificateValidatorChain("CertificateValidatorChain");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);
        
        
        assertTrue(validator.isValid(certificate));
    }

    @Test
    public void testCertificateValidatorChainInvalid() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/not-for-signing.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMESigning");
        
        CertificateValidatorChain validator = new CertificateValidatorChain("CertificateValidatorChain");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);
        
        assertFalse(validator.isValid(certificate));
        assertTrue(signingValidator == validator.getFailingValidator());
    }

    @Test
    public void testCertificateValidatorEncryptionChainInvalid() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/not-for-encryption.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMEEncryption");
        
        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);
        
        assertFalse(validator.isValid(certificate));
        assertTrue(encryptionValidator == validator.getFailingValidator());
    }
    
    @Test
    public void testCertificateValidatorNoValidators() 
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/certificates/not-for-encryption.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        assertFalse(validator.isValid(certificate));
        assertNull(validator.getFailingValidator());
    }   

    @Test
    public void testCertificateValidatorNullCertificate() 
    throws CertificateException
    {
        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMEEncryption");
        
        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);
        
        assertFalse(validator.isValid(null));
        assertTrue(encryptionValidator == validator.getFailingValidator());
    }
}
