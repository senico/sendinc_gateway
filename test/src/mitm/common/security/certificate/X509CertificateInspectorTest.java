/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.common.security.digest.Digest;
import mitm.common.util.HexUtils;
import mitm.test.TestUtils;

import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.junit.Test;

public class X509CertificateInspectorTest
{
    @Test
    public void testEmailTest() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/multipleemail.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
     
        assertEquals(10, inspector.getEmail().size());
        assertEquals(5, inspector.getEmailFromDN().size());
        assertEquals(5, inspector.getEmailFromAltNames().size());
    }
    
    @Test
    public void testSHA1Thumbprint() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertEquals("1A6C675F9D8AF742219D9DBE7C65003944A7766B", inspector.getThumbprint(Digest.SHA1));
    }

    @Test
    public void testSHA1HashStarts0() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/hash-starts-with-0.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertEquals("05818393AAE1765AD32F1D78C157220147469091E9B1BA92E6C94E687A679BEB5FA85CC10585FC5090283602EEAD6595A91C993A3D4ED99D6F517EF2ED7DC73E", 
                inspector.getThumbprint());
    }
    
    
    @Test
    public void testGetSerialNumber() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/dod-mega-crl.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertEquals("30929E", inspector.getSerialNumberHex());
    }

    @Test
    public void testGetSerialNumber2() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertEquals("52DC00010002B2BB74749401F475", inspector.getSerialNumberHex());
    }
    
    @Test
    public void testSubjectKeyIdentifier() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertNull(inspector.getSubjectKeyIdentifierHex());

        file = new File("test/resources/testdata/certificates/subjectkeyident.cer");
        
        certificate = TestUtils.loadCertificate(file);
        
        inspector = new X509CertificateInspector(certificate);
        
        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", inspector.getSubjectKeyIdentifierHex());

        file = new File("test/resources/testdata/certificates/equifax.cer");
        
        certificate = TestUtils.loadCertificate(file);
        
        inspector = new X509CertificateInspector(certificate);
        
        assertEquals("48E668F92BD2B295D747D82320104F3398909FD4", inspector.getSubjectKeyIdentifierHex());
    }
    
    @Test
    public void testKeyUsageEmpty() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/No_key_usage_MITM_Test_CA.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
     
        Set<KeyUsageType> keyUsage = inspector.getKeyUsage();
        
        assertNull(keyUsage);
    }
    
    @Test
    public void testKeyUsage2()
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/chinesechars.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
     
        Set<KeyUsageType> keyUsage = inspector.getKeyUsage();
        
        assertEquals(2, keyUsage.size());
        
        assertTrue(keyUsage.contains(KeyUsageType.DIGITALSIGNATURE));
        assertTrue(keyUsage.contains(KeyUsageType.KEYENCIPHERMENT));
    }

    @Test
    public void testExtendedKeyUsage()
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/mitm-test-ca.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
     
        Set<ExtendedKeyUsageType> keyUsage = inspector.getExtendedKeyUsage();
        
        assertEquals(2, keyUsage.size());
        
        assertTrue(keyUsage.contains(ExtendedKeyUsageType.EMAILPROTECTION));
        assertTrue(keyUsage.contains(ExtendedKeyUsageType.OCSPSIGNING));
    }

    @Test
    public void testExtendedKeyUsageEmpty()
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/rim.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
     
        Set<ExtendedKeyUsageType> keyUsage = inspector.getExtendedKeyUsage();
        
        assertNull(keyUsage);
    }
    
    @Test
    public void testExpired() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/mitm-test-ca.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        assertFalse(X509CertificateInspector.isExpired(certificate));

        file = new File("test/resources/testdata/certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer");
        
        certificate = TestUtils.loadCertificate(file);
        
        assertTrue(X509CertificateInspector.isExpired(certificate));
    }
    
    @Test
    public void testCalculateSubjectKeyIdentifier() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/subjectkeyident.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);
        
        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", inspector.getSubjectKeyIdentifierHex());

        SubjectKeyIdentifier subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifier();
        
        String skiHex = HexUtils.hexEncode(subjectKeyIdentifier.getKeyIdentifier());
        
        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", skiHex);
        
        file = new File("test/resources/testdata/certificates/AC_MINEFI_DPMA.cer");
        
        certificate = TestUtils.loadCertificate(file);
        
        inspector = new X509CertificateInspector(certificate);

        subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifier();
        
        skiHex = HexUtils.hexEncode(subjectKeyIdentifier.getKeyIdentifier());
        
        assertEquals(skiHex, inspector.getSubjectKeyIdentifierHex());
    }

    /*
     * tests the Microsoft invented alternative way to calculate the subjectKeyIdentifier
     */
    @Test
    public void testCalculateEncodedSubjectKeyIdentifier2() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/outlook2010_cert_missing_subjkeyid.pem");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        byte[] subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifierMicrosoft();
        
        String skiHex = HexUtils.hexEncode(subjectKeyIdentifier);
        
        assertEquals("2219E504D5750B37D20CC930B14129E1A2E583B1", skiHex);
    }
}
