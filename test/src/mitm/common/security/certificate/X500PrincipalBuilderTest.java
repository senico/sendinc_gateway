/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import javax.security.auth.x500.X500Principal;

import org.junit.Test;

public class X500PrincipalBuilderTest
{
    @Test
    public void testAllProperties() 
    throws IOException 
    {
        X500PrincipalBuilder builder = new X500PrincipalBuilder();
        
        builder.setCommonName("Martijn Brinkers");
        builder.setCountryCode("NL");
        builder.setEmail("test@example.com");
        builder.setGivenName("Martijn");
        builder.setSurname("Brinkers");
        builder.setLocality("Amsterdam");
        builder.setOrganisation("Example INC.");
        builder.setOrganisationalUnit("Test Unit");
        builder.setState("NH");
        X500Principal principal = builder.buildPrincipal();
        
        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);
        
        assertEquals("[Martijn Brinkers]", inspector.getCommonName().toString());
        assertEquals("[NL]", inspector.getCountryCode().toString());
        assertEquals("[test@example.com]", inspector.getEmail().toString());
        assertEquals("[Martijn]", inspector.getGivenName().toString());
        assertEquals("[Brinkers]", inspector.getSurname().toString());
        assertEquals("[Amsterdam]", inspector.getLocality().toString());
        assertEquals("[Example INC.]", inspector.getOrganisation().toString());
        assertEquals("[Test Unit]", inspector.getOrganisationalUnit().toString());
        assertEquals("[NH]", inspector.getState().toString());
    }
    
    @Test
    public void testUsingCollection() 
    throws IOException 
    {
        X500PrincipalBuilder builder = new X500PrincipalBuilder();
        
        builder.setCommonName(Arrays.asList(new String[]{"Martijn Brinkers"}));
        builder.setCountryCode(Arrays.asList(new String[]{"NL"}));
        builder.setEmail(Arrays.asList(new String[]{"test@example.com"}));
        builder.setGivenName(Arrays.asList(new String[]{"Martijn"}));
        builder.setSurname(Arrays.asList(new String[]{"Brinkers"}));
        builder.setLocality(Arrays.asList(new String[]{"Amsterdam"}));
        builder.setOrganisation(Arrays.asList(new String[]{"Example INC."}));
        builder.setOrganisationalUnit(Arrays.asList(new String[]{"Test Unit"}));
        builder.setState(Arrays.asList(new String[]{"NH"}));
        X500Principal principal = builder.buildPrincipal();
        
        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);
        
        assertEquals("[Martijn Brinkers]", inspector.getCommonName().toString());
        assertEquals("[NL]", inspector.getCountryCode().toString());
        assertEquals("[test@example.com]", inspector.getEmail().toString());
        assertEquals("[Martijn]", inspector.getGivenName().toString());
        assertEquals("[Brinkers]", inspector.getSurname().toString());
        assertEquals("[Amsterdam]", inspector.getLocality().toString());
        assertEquals("[Example INC.]", inspector.getOrganisation().toString());
        assertEquals("[Test Unit]", inspector.getOrganisationalUnit().toString());
        assertEquals("[NH]", inspector.getState().toString());
    }    
}
