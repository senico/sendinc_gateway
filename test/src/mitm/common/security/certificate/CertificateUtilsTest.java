/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.util.BigIntegerUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class CertificateUtilsTest
{
    /*
     * A test certificate which is not an X509Certificaate
     */
    private static class DummyCertificate extends Certificate
    {
        public DummyCertificate() {
            super("dummy-certificate");
        }
        
        @Override
        public byte[] getEncoded() {return null;}

        @Override
        public void verify(PublicKey key) {}

        @Override
        public void verify(PublicKey key, String sigProvider) {}

        @Override
        public String toString() {return null;}

        @Override
        public PublicKey getPublicKey() {return null;}        
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }

    @Test
    public void testLoadP7B() 
    throws Exception 
    {
        // Somehow this fails when used with the SUN provider. There must be a certificate that is not
        // accepted by the SUN provider
        File file = new File("test/resources/testdata/certificates/windows-xp-all-intermediates.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        assertEquals(123, certificates.size());
    }
    
    
    @Test
    public void testLoadCertificatesBase64File() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificateBase64.cer");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        assertEquals(1, certificates.size());
    }

    @Test
    public void testLoadCertificatesBinaryFile() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        assertEquals(1, certificates.size());
    }
    
    @Test
    public void testWriteCertificates() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        file = new File("test/resources/testdata/certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<Certificate>();
        
        allCerts.addAll(certificates);
        
        allCerts.addAll(CertificateUtils.readCertificates(file));
        
        assertEquals(2, allCerts.size());

        File p7b = new File("test/tmp/testwritecertificates.p7b");
        
        CertificateUtils.writeCertificates(allCerts, p7b);
        
        Collection<? extends Certificate> loaded = CertificateUtils.readCertificates(p7b);
        
        assertEquals(2, loaded.size());
    }

    @Test
    public void testWriteCertificatesPEM() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        file = new File("test/resources/testdata/certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<Certificate>();
        
        allCerts.addAll(certificates);
        
        allCerts.addAll(CertificateUtils.readCertificates(file));
        
        assertEquals(2, allCerts.size());

        File pemFile = new File("test/tmp/testwritecertificates.pem");
        
        CertificateUtils.writeCertificates(allCerts, pemFile, ObjectEncoding.PEM);
        
        Collection<? extends Certificate> loaded = CertificateUtils.readCertificates(pemFile);
        
        assertEquals(2, loaded.size());
    }
    
    
    @Test
    public void testGetMatchingCertificates() 
    throws Exception 
    {        
        File file = new File("test/resources/testdata/certificates/testcertificate.cer");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        file = new File("test/resources/testdata/certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<Certificate>();
        
        allCerts.addAll(certificates);
        
        allCerts.addAll(CertificateUtils.readCertificates(file));
        
        assertEquals(2, allCerts.size());
        
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115fd035ba042503bcc6ca44680f9f8"));
        
        Collection<? extends Certificate> matching = CertificateUtils.getMatchingCertificates(allCerts, selector);
        
        assertEquals(1, matching.size());
    }
    
    @Test
    public void testGetX509CertificatesCollection() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/windows-xp-all-intermediates.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        List<X509Certificate> x509Certificates = CertificateUtils.getX509Certificates(certificates); 
        
        assertEquals(123, x509Certificates.size());

        x509Certificates = CertificateUtils.getX509Certificates((Collection<? extends Certificate>)null); 

        assertEquals(0, x509Certificates.size());        

        x509Certificates = CertificateUtils.getX509Certificates(new ArrayList<X509Certificate>()); 

        assertEquals(0, x509Certificates.size());
        
        Collection<Certificate> certificates2 = new ArrayList<Certificate>();
        
        certificates2.add(null);
        certificates2.add((Certificate)new DummyCertificate());
        certificates2.addAll(certificates);
        
        assertEquals(125, certificates2.size());        
        
        x509Certificates = CertificateUtils.getX509Certificates(certificates2); 

        assertEquals(123, x509Certificates.size());        
    }
    
    @Test
    public void testGetX509CertificatesArray() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/windows-xp-all-intermediates.p7b");
        
        Certificate[] certificates = new ArrayList<Certificate>(CertificateUtils.readCertificates(file)).
                toArray(new Certificate[]{});
        
        X509Certificate[] x509Certificates = CertificateUtils.getX509Certificates(certificates); 
        
        assertEquals(123, x509Certificates.length);

        x509Certificates = CertificateUtils.getX509Certificates((Certificate[])null); 

        assertEquals(0, x509Certificates.length);        

        x509Certificates = CertificateUtils.getX509Certificates(new Certificate[]{}); 

        assertEquals(0, x509Certificates.length);
        
        certificates = new Certificate[]{null, new DummyCertificate(), certificates[0]};
        
        assertEquals(3, certificates.length);        
        
        x509Certificates = CertificateUtils.getX509Certificates(certificates); 

        assertEquals(1, x509Certificates.length);        
    }    
}
