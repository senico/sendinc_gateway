/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import mitm.test.TestUtils;


public class FreshestCertificateComparatorTest
{
    private final static File testBase = new File("test/resources/testdata/certificates/");

    @Test
    public void testCompare() 
    throws Exception
    {
        X509Certificate may_18_2006 = TestUtils.loadCertificate(new File(testBase, "dod-mega-crl.cer"));

        X509Certificate nov_1_2007 = TestUtils.loadCertificate(new File(testBase, "valid_certificate_mitm_test_ca.cer"));
        
        assertTrue(new FreshestCertificateComparator().compare(may_18_2006, nov_1_2007) < 0);
        assertTrue(new FreshestCertificateComparator().compare(nov_1_2007, may_18_2006) > 0);
        assertTrue(new FreshestCertificateComparator().compare(nov_1_2007, nov_1_2007) == 0);
        assertTrue(new FreshestCertificateComparator().compare(may_18_2006, may_18_2006) == 0);
        assertTrue(new FreshestCertificateComparator().compare(null, may_18_2006) < 0);
        assertTrue(new FreshestCertificateComparator().compare(may_18_2006, null) > 0);
        assertTrue(new FreshestCertificateComparator().compare(null, null) == 0);
    }
    
    @Test
    public void testSort() 
    throws Exception
    {
        List<X509Certificate> certs = new LinkedList<X509Certificate>();

        X509Certificate may_18_2006 = TestUtils.loadCertificate(new File(testBase, "dod-mega-crl.cer"));

        X509Certificate nov_1_2007 = TestUtils.loadCertificate(new File(testBase, "valid_certificate_mitm_test_ca.cer"));

        certs.add(nov_1_2007);
        certs.add(may_18_2006);
        certs.add(null);
        
        assertEquals(nov_1_2007, certs.get(0));
        assertEquals(may_18_2006, certs.get(1));
        assertNull(certs.get(2));
        
        Collections.sort(certs, new FreshestCertificateComparator());
        
        assertNull(certs.get(0));
        assertEquals(may_18_2006, certs.get(1));
        assertEquals(nov_1_2007, certs.get(2));
    }
}
