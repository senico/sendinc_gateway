/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CertStoreTrustAnchorBuilderTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static X509CertStoreExt certStore;    
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create();
    }

    @Before
    public void setup() 
    throws CertStoreException, CRLException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException
    {
        certStore.removeAllEntries();
    }    
    
    private static void addRoot(String filename, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        File file = new File("test/resources/testdata/certificates", filename);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    @Test
    public void testInitialBuild() 
    throws CertStoreException, CertificateException, NoSuchProviderException, IOException
    {
        addRoot("rim.cer", certStore);

        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 10 * DateUtils.MILLIS_PER_SECOND);
        
        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
        
        assertEquals(1, trustAnchors.size());
    }

    
    @Test
    public void testEmptyStore() 
    throws CertStoreException
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 10 * DateUtils.MILLIS_PER_SECOND);
        
        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
        
        assertEquals(0, trustAnchors.size());
    }

    @Test
    public void testAddCertificate() 
    throws CertStoreException, CertificateException, NoSuchProviderException, IOException, InterruptedException
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 0 * DateUtils.MILLIS_PER_SECOND);

        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
        
        assertEquals(0, trustAnchors.size());

        addRoot("rim.cer", certStore);

        assertEquals(0, trustAnchors.size());
        
        trustAnchors = builder.getTrustAnchors();
        
        assertEquals(1, trustAnchors.size());
    }

    @Test
    public void testAddExtraCertificate() 
    throws CertStoreException, CertificateException, NoSuchProviderException, IOException
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 0 * DateUtils.MILLIS_PER_SECOND);

        File file = new File("test/resources/testdata/certificates", "gmail-ssl.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        builder.addCertificates(certificates);
        
        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

        assertEquals(1, trustAnchors.size());
        
        addRoot("rim.cer", certStore);
        
        trustAnchors = builder.getTrustAnchors();
        
        assertEquals(2, trustAnchors.size());
        
        file = new File("test/resources/testdata/certificates", "chinesechars.cer");

        certificates = CertificateUtils.readCertificates(file);

        builder.addCertificates(certificates);
        
        trustAnchors = builder.getTrustAnchors();
        
        assertEquals(3, trustAnchors.size());
    }
    
    @Test(expected=UnsupportedOperationException.class)
    public void testUnmodifiableCollection() 
    throws CertStoreException, CertificateException, NoSuchProviderException, IOException
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 0 * DateUtils.MILLIS_PER_SECOND);

        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
        
        trustAnchors.add(null);
    }
    
    @Test
    public void testNoChange() 
    throws CertStoreException, CertificateException, NoSuchProviderException, IOException
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(certStore, 999999 * DateUtils.MILLIS_PER_SECOND);

        addRoot("rim.cer", certStore);

        Set<TrustAnchor> trustAnchors1 = builder.getTrustAnchors();
        Set<TrustAnchor> trustAnchors2 = builder.getTrustAnchors();

        assertSame(trustAnchors1, trustAnchors2);
    }
}
