/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certpath;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.certstore.jce.X509CertStoreParameters;
import mitm.common.security.provider.MITMProvider;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrustAnchorBuilderTest
{
    private static final Logger logger = LoggerFactory.getLogger(TrustAnchorBuilderTest.class);
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static CertStore rootStore;
    private static X509CertStoreParameters rootStoreParams;    
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        rootStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create());
        rootStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, rootStoreParams, "mitm");
        
        rootStoreParams.getCertStore().removeAllEntries();
        
        addRoots();
    }

    private static void addRoot(String filename, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        File file = new File("test/resources/testdata/certificates", filename);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                try {
                    certStore.addCertificate((X509Certificate) certificate);
                }
                catch(CertStoreException e) {
                    logger.error("Error adding certificate.", e);
                }
            }
        }
    }
    
    private static void addRoots() 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        addRoot("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
    }
    
    @Test
    public void testloadTrustAnchorsCollection()
    throws Exception
    {        
        TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();

        X509CertSelector selector = new X509CertSelector();
        
        selector.setIssuer("OU=VeriSign Trust Network, OU=\"(c) 1998 VeriSign, Inc. - For authorized use only\", " + 
                "OU=Class 1 Public Primary Certification Authority - G2, O=\"VeriSign, Inc.\", C=US");
        
        Collection<? extends Certificate> certificates = rootStore.getCertificates(selector);

        builder.addCertificates(certificates);
        
        Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

        assertEquals(2, trustAnchors.size());
    }

    @Test
    public void testloadTrustAnchorsCollectionTiming()
    throws Exception
    {        
        long start = System.currentTimeMillis();

        final int tries = 100;
        
        for (int i = 0; i < tries; i++) 
        {
            TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();
    
            X509CertSelector selector = new X509CertSelector();
            
            selector.setIssuer("OU=VeriSign Trust Network, OU=\"(c) 1998 VeriSign, Inc. - For authorized use only\", " + 
                    "OU=Class 1 Public Primary Certification Authority - G2, O=\"VeriSign, Inc.\", C=US");
            
            Collection<? extends Certificate> certificates = rootStore.getCertificates(selector);
    
            builder.addCertificates(certificates);
            
            Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
    
            assertEquals(2, trustAnchors.size());
        }
        
        System.out.println("testloadTrustAnchorsCollectionTiming. Seconds / try: " + 
                (System.currentTimeMillis() - start) * 0.001 / tries);
    }

    @Test
    public void testloadTrustAnchors()
    throws Exception
    {
        
        long start = System.currentTimeMillis();

        final int tries = 10;
        
        for (int i = 0; i < tries; i++) 
        {
            TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();
    
            builder.addCertificates(rootStore);
                
            Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();
            
            assertEquals(246, trustAnchors.size());
        }
        
        System.out.println("testloadTrustAnchors. Seconds / try: " + 
                (System.currentTimeMillis() - start) * 0.001 / tries);
    }    
}
