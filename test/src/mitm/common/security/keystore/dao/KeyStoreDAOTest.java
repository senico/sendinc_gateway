/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import mitm.common.hibernate.DatabaseAction;
import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.keystore.hibernate.KeyStoreEntryHibernate;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.BeforeClass;
import org.junit.Test;

public class KeyStoreDAOTest
{
    private static HibernateSessionSource sessionSource;
    private static DatabaseActionExecutor executor;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static Long testID;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws InstantiationException, IllegalAccessException, ClassNotFoundException, DatabaseException 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        executor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
        
        insertEntry();
    }

    public static void insertEntry() 
    throws DatabaseException
    {
        testID = executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                return insertAction("test", session, "alias");
            }
        });

        testID = executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                return insertAction("test", session, "alias2");
            }
        });

        testID = executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                return insertAction("other store", session, "alias2");
            }
        });
    }
    
    private static Long insertAction(String storeName, Session session, String alias)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate(storeName, session);
            
            File file = new File("test/resources/testdata/certificates/testcertificate.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);

            file = new File("test/resources/testdata/certificates/rim.cer");
            
            X509Certificate rimCertificate = TestUtils.loadCertificate(file);
            
            Certificate[] chain = new Certificate[] {certificate, rimCertificate};
            
            KeyStoreEntryHibernate entry = new KeyStoreEntryHibernate(storeName, certificate, chain, null, alias, new Date());

            dao.makePersistent(entry);
            
            return (Long) session.getIdentifier(entry);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void testAliasDulication()
    throws DatabaseException
    {
        // should result in exception
        testID = executor.executeTransaction(new DatabaseAction<Long>()
        {
            @Override
            public Long doAction(Session session) 
            throws DatabaseException 
            {
                return insertAction("test", session, "alias");
            }
        });
    }
    
    @Test
    public void testLoadById() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                loadByIdAction(session, testID);
            }
        });
    }
    
    private void loadByIdAction(Session session, Long id)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate("test", session);
            
            KeyStoreEntryHibernate entry = dao.findById(id);

            assertTrue(entry.getCertificate() instanceof X509Certificate);
            
            // test whether the certificate is not created using the default provider but using the
            // bc provider. The BC need to be installed and used for this check to work
            assertTrue(entry.getCertificate() instanceof org.bouncycastle.jce.provider.X509CertificateObject);
            
            assertEquals(2, entry.getChain().length);
            
            for (Certificate certificate : entry.getChain()) 
            {
                // test whether the certificate is not created using the default provider but using the
                // bc provider. The BC need to be installed and used for this check to work
                assertTrue(certificate instanceof org.bouncycastle.jce.provider.X509CertificateObject);
            }
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    
    @Test
    public void testGetAliases() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                getAliasesAction(session);
            }
        });
    }
    
    private void getAliasesAction(Session session)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate("test", session);
            
            List<String> aliases = dao.getAliases();
            
            assertEquals(2, aliases.size());
            assertTrue(aliases.contains("alias"));
            assertTrue(aliases.contains("alias2"));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    @Test
    public void testGetByAliasAction() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                getByAliasAction(session);
            }
        });
    }    
    
    private void getByAliasAction(Session session)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate("test", session);
            
            KeyStoreEntryHibernate entry = dao.getEntryByAlias("alias");
            
            assertNotNull(entry);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    @Test
    public void testGetByCertificateAction() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                getByCertificateAction(session);
            }
        });
    }    
    
    private void getByCertificateAction(Session session)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate("test", session);

            File file = new File("test/resources/testdata/certificates/testcertificate.cer");
            
            X509Certificate certificate = TestUtils.loadCertificate(file);
            
            KeyStoreEntryHibernate entry = dao.getEntryByCertificate(certificate);
            
            assertNotNull(entry);
            assertEquals("alias", entry.getKeyAlias());
            assertEquals(2, entry.getChain().length);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    
    @Test
    public void testGetEntryCountAction() 
    throws DatabaseException 
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                getEntryCountAction(session);
            }
        });
    }    
    
    private void getEntryCountAction(Session session)
    throws DatabaseException
    {
        try {
            KeyStoreDAO dao = new KeyStoreDAOHibernate("test", session);

            Long count = dao.getEntryCount();
            
            assertEquals(2L, (long) count);            
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
}
