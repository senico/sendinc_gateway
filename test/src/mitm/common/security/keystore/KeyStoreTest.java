/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Eclipse Public License,
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.crypto.KeyGenerator;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateInspector;
import mitm.common.security.digest.Digest;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeyStoreTest
{
    private static final Logger logger = LoggerFactory.getLogger(KeyStoreTest.class);

    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static KeyStore databaseKeyStore;
    private static KeyStore databaseKeyStore2;

    private static Key aesSecretKey;
    private static Key desSecretKey;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);

        MITMProvider.initialize(sessionManager);

        databaseKeyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        databaseKeyStore.load(null);

        databaseKeyStore2 = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        databaseKeyStore2.load(new DatabaseKeyStoreLoadStoreParameter("other"));

        addSecretKeys();

        loadPKCS12(new File("test/resources/testdata/keys/testCertificates.p12"), "test", databaseKeyStore, true);
        loadPKCS12(new File("test/resources/testdata/keys/testCertificates.p12"), "test", databaseKeyStore2, false);
    }

    private static void loadPKCS12(File file, String password, KeyStore destination, boolean storeKey)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        Enumeration<String> aliases = keyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            try {
                Key key = keyStore.getKey(alias, null);
                Certificate certificate = keyStore.getCertificate(alias);
                Certificate[] chain = keyStore.getCertificateChain(alias);

                if (storeKey && key != null)
                {
                    destination.setKeyEntry(alias, key, null, chain);

                    // again but now password protected
                    destination.setKeyEntry("protected." + alias, key, "test".toCharArray(), chain);
                }
                else {
                    destination.setCertificateEntry(alias, certificate);
                }
            }
            catch (UnrecoverableKeyException e) {
                logger.error("Could not get the key.", e);
            }
        }
    }

    private static void addSecretKeys()
    throws Exception
    {
        aesSecretKey = KeyGenerator.getInstance("AES").generateKey();
        desSecretKey = KeyGenerator.getInstance("DES").generateKey();

        databaseKeyStore.setKeyEntry("aes", aesSecretKey, null, null);
        databaseKeyStore.setKeyEntry("protected.aes", aesSecretKey, "test".toCharArray(), null);

        databaseKeyStore.setKeyEntry("des", desSecretKey, null, null);
        databaseKeyStore.setKeyEntry("protected.des", desSecretKey, "test".toCharArray(), null);
    }


    @Test
    public void testSecretKeys()
    throws Exception
    {
        Key key = databaseKeyStore.getKey("aes", null);
        assertEquals(aesSecretKey, key);

        key = databaseKeyStore.getKey("protected.aes", "test".toCharArray());
        assertEquals(aesSecretKey, key);

        key = databaseKeyStore.getKey("des", null);
        assertEquals(desSecretKey, key);

        key = databaseKeyStore.getKey("protected.des", "test".toCharArray());
        assertEquals(desSecretKey, key);
    }

    @Test
    public void testAliases()
    throws Exception
    {
        Enumeration<String> aliasesEnum = databaseKeyStore.aliases();

        Set<String> aliases = new HashSet<String>();

        //int size = 0;

        while (aliasesEnum.hasMoreElements())
        {
            String alias = aliasesEnum.nextElement();

            aliases.add(alias);

            //size++;
        }

        // 20 * 2 (key entries also get a protect.* alias) + 2 (root&ca) + 4 secret keys =  46
        assertEquals(46, aliases.size());
    }

    @Test
    public void testContainsAlias()
    throws Exception
    {
        assertTrue(databaseKeyStore.containsAlias("ca"));
        assertTrue(databaseKeyStore.containsAlias("rsa2048"));
        assertFalse(databaseKeyStore.containsAlias("non-existing-alias"));
        assertTrue(databaseKeyStore.containsAlias("protected.rsa2048"));
        assertFalse(databaseKeyStore.containsAlias(null));
    }

    @Test
    public void testGetCertificateByAlias()
    throws Exception
    {
        final String alias = "ValidCertificate";

        Certificate certificate = databaseKeyStore.getCertificate(alias);

        CertificateInspector inspector = new CertificateInspector(certificate);

        assertNotNull(certificate);
        assertTrue(certificate instanceof X509Certificate);
        assertEquals("F9DD44188EB18B5B91BF4C459A6A40A183A5DD43", inspector.getThumbprint(Digest.SHA1));
    }

    @Test
    public void testGetCertificateByNonExistingAlias()
    throws Exception
    {
        final String alias = "non-existing";

        Certificate certificate = databaseKeyStore.getCertificate(alias);

        assertNull(certificate);
    }

    @Test
    public void testGetCertificateByNullAlias()
    throws Exception
    {
        Certificate certificate = databaseKeyStore.getCertificate(null);

        assertNull(certificate);
    }


    @Test
    public void testGetCertificateAlias()
    throws Exception
    {
        Certificate certificate = databaseKeyStore.getCertificate("protected.UppercaseEmail");

        String alias = databaseKeyStore.getCertificateAlias(certificate);

        // the protected. or without prefix alias can be returned because they both
        // contain the same certificate
        assertTrue(alias.endsWith("UppercaseEmail"));
    }


    @Test
    public void testGetCertificateAliasNonExisting()
    throws Exception
    {
        File file = new File("test/resources/testdata/certificates/dod-mega-crl.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        String alias = databaseKeyStore.getCertificateAlias(certificate);

        assertNull(alias);
    }


    @Test
    public void testGetCertificateChain()
    throws Exception
    {
        Certificate[] chain = databaseKeyStore.getCertificateChain("protected.NoExtendedKeyUsage");

        assertNotNull(chain);
        assertEquals(chain.length, 3);

        chain = databaseKeyStore.getCertificateChain("root");

        assertNull(chain);
    }


    @Test
    public void testGetCertificateChainNonExistingAlias()
    throws Exception
    {
        Certificate[] chain = databaseKeyStore.getCertificateChain("non-existing");

        assertNull(chain);
    }

    @Test
    public void testGetCreationDate()
    throws Exception
    {
        Enumeration<String> aliasesEnum = databaseKeyStore.aliases();

        while (aliasesEnum.hasMoreElements())
        {
            String alias = aliasesEnum.nextElement();

            Date date = databaseKeyStore.getCreationDate(alias);

            assertNotNull(date);

            assertTrue(new Date().getTime() - date.getTime() < 3600 * 1000);
        }
    }

    @Test
    public void testGetCreationdateNonExistingAlias()
    throws Exception
    {
        Date date = databaseKeyStore.getCreationDate("non-existing");

        assertNull(date);
    }


    @Test
    public void testGetEntry()
    throws Exception
    {
        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

        KeyStore.Entry entry = databaseKeyStore.getEntry("protected.ValidCertificate", passwd);

        assertNotNull(entry);
    }


    @Test(expected=UnrecoverableEntryException.class)
    public void testGetEntryWrongPassword()
    throws Exception
    {
        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("wrong-password".toCharArray());

        // this should throw an UnrecoverableEntryException
        databaseKeyStore.getEntry("protected.ValidCertificate", passwd);
    }

    @Test
    public void testGetEntryNonExistingAlias()
    throws Exception
    {
        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

        KeyStore.Entry entry = databaseKeyStore.getEntry("non-existing", passwd);

        assertNull(entry);
    }

    @Test
    public void testGetKey()
    throws Exception
    {
        Key key1 = databaseKeyStore.getKey("ValidCertificate", null);

        assertNotNull(key1);
        assertTrue(key1 instanceof PrivateKey);

        Key key2 = databaseKeyStore.getKey("protected.ValidCertificate", "test".toCharArray());

        assertNotNull(key2);
        assertTrue(key2 instanceof PrivateKey);

        assertEquals(key1, key2);
    }

    @Test(expected=UnrecoverableKeyException.class)
    public void testGetKeyPasswordWithNonProtectedEntry()
    throws Exception
    {
        // key is not a protected key so an exception should be thrown
        databaseKeyStore.getKey("ValidCertificate", "test".toCharArray());
    }

    @Test(expected=UnrecoverableKeyException.class)
    public void testGetKeyWrongPassword()
    throws Exception
    {
        // incorrect password
        databaseKeyStore.getKey("protected.ValidCertificate", "incorrect password".toCharArray());
    }

    @Test
    public void testGetIsCertificateEntry()
    throws Exception
    {
        assertTrue(databaseKeyStore.isCertificateEntry("ca"));
        assertFalse(databaseKeyStore.isCertificateEntry("ValidCertificate"));
        assertFalse(databaseKeyStore.isCertificateEntry("protected.ValidCertificate"));
        assertFalse(databaseKeyStore.isCertificateEntry("non-existing"));
    }

    @Test
    public void testGetIsKeyEntry()
    throws Exception
    {
        assertFalse(databaseKeyStore.isKeyEntry("ca"));
        assertTrue(databaseKeyStore.isKeyEntry("ValidCertificate"));
        assertTrue(databaseKeyStore.isKeyEntry("protected.ValidCertificate"));
        assertFalse(databaseKeyStore.isKeyEntry("non-existing"));
    }

    @Test
    public void testGetSize()
    throws Exception
    {
        // 20 * 2 (key entries also get a protect.* alias) + 2 (root&ca) + 4 secret keys =  46
        assertEquals(46, databaseKeyStore.size());
    }
}
