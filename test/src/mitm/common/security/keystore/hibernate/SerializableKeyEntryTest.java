/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.keystore.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.password.PBEncryption;
import mitm.common.security.password.PBEncryptionImpl;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class SerializableKeyEntryTest
{
    private final static String ENCODED_PUBLIC_KEY =
        "30820122300d06092a864886f70d01010105000382010f003082010a0282" +
        "010100b7c0b5ac2250390fe9da9b800bdb0a1b7f21614d1e7c5db67438a9" +
        "d1f793c0cf0403a3523981f17885e145dbc0e4c927a184c0246e7a79ae29" +
        "f7ad4e1a36db1db567c793547b96a8275e93d37ad17006ca516f3ed4bfbf" +
        "f9883e2a351d8c5c76a24502344e8711e76843b3ba05f7a5515287dba46b" +
        "05033c7756b531512034845fc7ae350ebc720ed83e7df7ebd8ade16b0d96" +
        "1c15277fd5802b724c3d9392a7e99d2d06bf55b5d2516152b1d9c249561e" +
        "3887f075e2bebe8e331cdc3fe5de60af36285e514b14163b34af15e6bb48" +
        "e6b232366f05d73ad261714854833305ba06f1430d5b0c83837c49c9408c" +
        "8e4d674b9a9494a6b0dc66e0b4d2107d78f26f0203010001";

    private final static String SERIALIZED_BASE64 =
        "rO0ABXNyADxtaXRtLmNvbW1vbi5zZWN1cml0eS5rZXlzdG9yZS5oaWJlcm5"+
        "hdGUuU2VyaWFsaXphYmxlS2V5RW50cnlhZJu6tBscNwIABUwACWFsZ29yaX"+
        "RobXQAEkxqYXZhL2xhbmcvU3RyaW5nO0wABmZvcm1hdHEAfgABTAAHa2V5V"+
        "HlwZXQARkxtaXRtL2NvbW1vbi9zZWN1cml0eS9rZXlzdG9yZS9oaWJlcm5h"+
        "dGUvU2VyaWFsaXphYmxlS2V5RW50cnkkS2V5VHlwZTtMAApwcm90ZWN0aW9"+
        "udABJTG1pdG0vY29tbW9uL3NlY3VyaXR5L2tleXN0b3JlL2hpYmVybmF0ZS"+
        "9TZXJpYWxpemFibGVLZXlFbnRyeSRQcm90ZWN0aW9uO1sABnJhd0tleXQAA"+
        "ltCeHB0AANSU0F0AAVYLjUwOX5yAERtaXRtLmNvbW1vbi5zZWN1cml0eS5r"+
        "ZXlzdG9yZS5oaWJlcm5hdGUuU2VyaWFsaXphYmxlS2V5RW50cnkkS2V5VHl"+
        "wZQAAAAAAAAAAEgAAeHIADmphdmEubGFuZy5FbnVtAAAAAAAAAAASAAB4cH"+
        "QABlBVQkxJQ35yAEdtaXRtLmNvbW1vbi5zZWN1cml0eS5rZXlzdG9yZS5oa"+
        "WJlcm5hdGUuU2VyaWFsaXphYmxlS2V5RW50cnkkUHJvdGVjdGlvbgAAAAAA"+
        "AAAAEgAAeHEAfgAJdAAJRU5DUllQVEVEdXIAAltCrPMX+AYIVOACAAB4cAA"+
        "AAVSWSV/HpJbEiQAAABATPpH027yrlCaARCuh4GOTAAAIAAAAATA5xzE6HL"+
        "Sanxzo1xDOMSv5gSLBm+UAnvp2BlpBpEW+kJNDgSTWPOPfl9FKpFxlHFPbW"+
        "nf8dNG6XjyObAiUGtCkwv2YfyRyuCE/I0/pWAYybLoQHThehu1b8jcYONSQ"+
        "zMJgHuaFwI0hJ0h9SkeOHxTSbMTddTDr7iHf9OaPgHEdvEKQCIWdAmP0Q5O"+
        "d6ny6hT6P6/qeUxggVqY186M36B0kivqFHWWDZ+c/lEcp/TvPdn6FqZ5OF5"+
        "Zs32cNjiMHG02BPlQZC7XGBOCRSyB+9Yu2jXWURZpJWIhHR/oUbxrDCsvIC"+
        "uPMgijbzC6vpFKXEmz2jJClyd11DhyHL7qQ/UYaIOGfEJKnFKD6skPHX49O"+
        "OAOuB24cV1LfaCglRdxS62vnI6nM/DI3/qHUczNvMpG5";
            
    private final static String PASSWORD = "test";
    
    private static SecurityFactory securityFactory;
    private static KeyFactory keyFactory;
    private static PBEncryption encryptor;
    private static PublicKey publicKey;    

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
       
        keyFactory = securityFactory.createKeyFactory("RSA");
        
        publicKey = decodePublicKey(ENCODED_PUBLIC_KEY);
        
        encryptor = new PBEncryptionImpl();
    }
    
    private static PublicKey decodePublicKey(String encoded) 
    throws DecoderException, InvalidKeySpecException
    {
        byte[] rawKey = Hex.decodeHex(encoded.toCharArray());
        
        KeySpec keySpec = new X509EncodedKeySpec(rawKey);
        
        return keyFactory.generatePublic(keySpec);
    }
    
    @Test
    public void testSerializeKey()
    throws Exception
    {
        SerializableKeyEntry serializer = new SerializableKeyEntry(publicKey, PASSWORD.toCharArray(), encryptor);
        
        byte[] serialized = serializer.serialize();
                
        assertNotNull(serialized);
        
        SerializableKeyEntry deSerialized = SerializableKeyEntry.deserialize(serialized);
        
        Key key = deSerialized.getKey(PASSWORD.toCharArray(), encryptor);
        
        assertEquals(publicKey, key);
    }
    
    @Test
    public void testDeSerializeKey()
    throws Exception
    {
        byte[] serialized = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(SERIALIZED_BASE64));
        
        SerializableKeyEntry deSerialized = SerializableKeyEntry.deserialize(serialized);
        
        Key key = deSerialized.getKey(PASSWORD.toCharArray(), encryptor);
        
        assertEquals(publicKey, key);        
    }
}
