package mitm.common.security.ctl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.List;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;
import mitm.test.TestUtils;

import org.apache.commons.lang.NullArgumentException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CTLImplTest
{
    private static final File testBase = new File("test/resources/testdata/");
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        // TODO: do not use DjigzoTestUtils because otherwise this test depends on Djigzo packages
        DjigzoTestUtils.initialize();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        AutoTransactDelegator.createProxy().deleteAll();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            SessionManager sessionManager = SystemServices.getSessionManager();

            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public void deleteAll()
        {
            new CTLDAO(SessionAdapterFactory.create(SystemServices.getSessionManager().getSession()), null).deleteAll();
        }

        @StartTransaction
        public void testAddEntry()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            ctl.addEntry(entry);
            
            CTLEntry added = ctl.getEntry(certificate);
            
            assertEquals(entry, added);
            assertEquals(1, ctl.size());
            
            // default is white listed
            assertEquals(CTLEntryStatus.WHITELISTED, added.getStatus());
            
            // default is false
            assertFalse(added.isAllowExpired());            
        }

        @StartTransaction
        public void testAddDuplicateEntry()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            ctl.addEntry(entry);

            CTLEntry duplicate = ctl.createEntry(certificate);
            
            ctl.addEntry(duplicate);

            assertEquals(1, ctl.size());
        }

        @StartTransaction
        public void testAddSameCertDifferentCTL()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl1 = manager.getCTL("global1");
            CTL ctl2 = manager.getCTL("global2");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
            
            CTLEntry entry1 = ctl1.createEntry(certificate);
            
            ctl1.addEntry(entry1);

            CTLEntry entry2 = ctl2.createEntry(certificate);
            
            ctl2.addEntry(entry2);

            assertEquals(1, ctl1.size());
            assertEquals(1, ctl2.size());
        }

        @StartTransaction
        public void testDeleteAll()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl1 = manager.getCTL("global1");
            CTL ctl2 = manager.getCTL("global2");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
            
            CTLEntry entry1 = ctl1.createEntry(certificate);
            
            ctl1.addEntry(entry1);

            CTLEntry entry2 = ctl2.createEntry(certificate);
            
            ctl2.addEntry(entry2);

            assertEquals(1, ctl1.size());
            assertEquals(1, ctl2.size());
            
            ctl1.deleteAll();
            
            assertEquals(0, ctl1.size());
            assertEquals(1, ctl2.size());
            assertNull(ctl1.getEntry(certificate));
            assertEquals(entry2, ctl2.getEntry(certificate));
        }
        
        @StartTransaction
        public void testAddMulitpleEntries()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
            
            CTLEntry entry1 = ctl.createEntry(certificate);
            
            ctl.addEntry(entry1);

            CTLEntry entry2 = ctl.createEntry("abc");
            
            ctl.addEntry(entry2);
            
            CTLEntry entry3 = ctl.createEntry("def");
            
            ctl.addEntry(entry3);

            assertEquals(3, ctl.size());
            assertEquals(entry1, ctl.getEntry(certificate));
            assertEquals(entry2, ctl.getEntry("abc"));
            assertEquals(entry3, ctl.getEntry("def"));
            
            List<? extends CTLEntry> entries = ctl.getEntries(0, Integer.MAX_VALUE);

            assertEquals(3, entries.size());
            assertTrue(entries.contains(entry1));
            assertTrue(entries.contains(entry2));
            assertTrue(entries.contains(entry3));
            
            entries = ctl.getEntries(0, 1);
            assertEquals(1, entries.size());
            assertTrue(entries.contains(entry1));
            
            entries = ctl.getEntries(1, 1);
            assertEquals(1, entries.size());
            assertTrue(entries.contains(entry2));            

            entries = ctl.getEntries(10, 1);
            assertEquals(0, entries.size());
        }
        
        @StartTransaction
        public void testValidateWhiteListed()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            entry.setStatus(CTLEntryStatus.WHITELISTED);
            entry.setAllowExpired(false);
            
            ctl.addEntry(entry);

            assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());
            
            CTLValidityResult result = ctl.checkValidity(certificate);
            
            assertEquals(CTLValidity.VALID, result.getValidity());
            assertEquals(CTLImpl.WHITELISTED_MESSAGE, result.getMessage());
        }
        
        @StartTransaction
        public void testValidateWhiteListedButExpired()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            entry.setStatus(CTLEntryStatus.WHITELISTED);
            entry.setAllowExpired(false);
            
            ctl.addEntry(entry);

            assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());
            
            CTLValidityResult result = ctl.checkValidity(certificate);
            
            assertEquals(CTLValidity.INVALID, result.getValidity());
            assertEquals(CTLImpl.EXPIRED_MESSAGE, result.getMessage());
        }
        
        @StartTransaction
        public void testValidateWhiteListedExpiredButAllowed()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/thawte-freemail-valid-till-091108.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            entry.setStatus(CTLEntryStatus.WHITELISTED);
            entry.setAllowExpired(true);
            
            ctl.addEntry(entry);

            assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());
            
            CTLValidityResult result = ctl.checkValidity(certificate);
            
            assertEquals(CTLValidity.VALID, result.getValidity());
            assertEquals(CTLImpl.WHITELISTED_MESSAGE, result.getMessage());
        }

        @StartTransaction
        public void testValidateBlackListed()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer"));
            
            CTLEntry entry = ctl.createEntry(certificate);
            
            entry.setStatus(CTLEntryStatus.BLACKLISTED);
            
            ctl.addEntry(entry);

            assertEquals(CTLEntryStatus.BLACKLISTED, ctl.getEntry(certificate).getStatus());
            
            CTLValidityResult result = ctl.checkValidity(certificate);
            
            assertEquals(CTLValidity.INVALID, result.getValidity());
            assertEquals(CTLImpl.BLACKLISTED_MESSAGE, result.getMessage());
        }

        @StartTransaction
        public void testValidateNotListed()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer"));
            
            CTLValidityResult result = ctl.checkValidity(certificate);
            
            assertEquals(CTLValidity.NOT_LISTED, result.getValidity());
            assertEquals(CTLImpl.NOT_LISTED_MESSAGE, result.getMessage());
        }

        @StartTransaction
        public void testValidateNullCertificate()
        throws Exception
        {
            CTLManager manager = SystemServices.getCTLManager();
            
            CTL ctl = manager.getCTL("global");
            
            ctl.checkValidity(null);
        }
    }
    
    @Test
    public void testAddEntry()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testAddEntry();
    }
    
    @Test(expected = ConstraintViolationException.class)
    public void testAddDuplicateEntry()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testAddDuplicateEntry();
    }

    @Test
    public void testAddSameCertDifferentCTL()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testAddSameCertDifferentCTL();
    }    

    @Test
    public void testDeleteAll()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testDeleteAll();
    }    
    
    @Test
    public void testAddMulitpleEntries()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testAddMulitpleEntries();
    }    

    @Test
    public void testValidateWhiteListed()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateWhiteListed();
    }    

    @Test
    public void testValidateWhiteListedButExpired()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateWhiteListedButExpired();
    }    

    @Test
    public void testValidateWhiteListedExpiredButAllowed()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateWhiteListedExpiredButAllowed();
    }    

    @Test
    public void testValidateBlackListed()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateBlackListed();
    }
    
    @Test
    public void testValidateNotListed()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateNotListed();
    }    

    @Test(expected = NullArgumentException.class)
    public void testValidateNullCertificate()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testValidateNullCertificate();
    }    
}
