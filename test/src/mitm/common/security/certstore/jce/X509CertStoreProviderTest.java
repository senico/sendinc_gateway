/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.jce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;

import javax.security.auth.x500.X500Principal;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.cms.SignerIdentifier;
import mitm.common.security.cms.SignerIdentifierImpl;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.BigIntegerUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class X509CertStoreProviderTest
{
    private static final Logger logger = LoggerFactory.getLogger(X509CertStoreProviderTest.class);

    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static KeyStore keyStore;
    private static CertStore certStore;
    private static CertStore caCertStore;
    private static X509CertStoreParameters certStoreParams;    
    private static X509CertStoreParameters caCertStoreParams;    
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create());
        
        certStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, certStoreParams, "mitm");

        caCertStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "ca").create());
        
        caCertStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, caCertStoreParams, "mitm");
        
        certStoreParams.getCertStore().removeAllEntries();
        caCertStoreParams.getCertStore().removeAllEntries();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        initCAStore();
    }

    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
            
            Enumeration<String> aliases = keyStore.aliases();
            
            while (aliases.hasMoreElements()) 
            {
                String alias = aliases.nextElement();
    
                Certificate certificate = keyStore.getCertificate(alias);
                
                if (!(certificate instanceof X509Certificate)) {
                    // only X509Certificates are supported
                    continue;
                }
    
                try {
                    boolean hasPrivateKey = keyStore.isKeyEntry(alias) && 
                            (keyStore.getKey(alias, null) instanceof PrivateKey);
    
                    alias = hasPrivateKey ? alias : null;
    
                    certStoreParams.getCertStore().addCertificate((X509Certificate) certificate, alias);
                }
                catch (UnrecoverableKeyException e) {
                    logger.error("Could not get the key.",e);
                }
                catch (CertStoreException e) {
                    logger.error("CertStoreException.",e);
                }
            }
            
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }
    
    private static void initCAStore() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");    
        assertNotNull(certificate);
        caCertStoreParams.getCertStore().addCertificate(certificate);
        
        certificate = (X509Certificate) keyStore.getCertificate("ca");
        assertNotNull(certificate);
        caCertStoreParams.getCertStore().addCertificate(certificate);
        
        assertEquals(2, caCertStore.getCertificates(new X509CertSelector()).size());
    }
    
    @Test
    public void testGetCertificatesIssuerSerial() 
    throws KeyStoreException, CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();
        
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");
        
        selector.setIssuer(certificate.getIssuerX500Principal());
        selector.setSerialNumber(certificate.getSerialNumber());
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesSignerIdentifier() 
    throws KeyStoreException, IOException, CertStoreException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        SignerIdentifier identifier = new SignerIdentifierImpl(certificate);

        CertSelector selector = identifier.getSelector();

        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesSubjectKeyIdentifier() 
    throws KeyStoreException, IOException, CertStoreException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        SignerIdentifier identifier = new SignerIdentifierImpl(null, null, 
                X509CertificateInspector.getSubjectKeyIdentifier(certificate));

        CertSelector selector = identifier.getSelector();

        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesUnoptimized() 
    throws KeyStoreException, CertStoreException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        X509CertSelector selector = new X509CertSelector();

        selector.setKeyUsage(certificate.getKeyUsage());
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(17, certificates.size());
    }
    
    @Test
    public void testGetCertificatesNoMatch() 
    throws CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(new BigInteger("123", 10));
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(0, certificates.size());
    }
    
    @Test
    public void testGetCertificatesValidOnly() 
    throws CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setCertificateValid(new Date());
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(21, certificates.size());
    }
    
    @Test
    public void testGetCertificatesByCertificate() 
    throws KeyStoreException, CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        selector.setCertificate(certificate);
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesNoRestriction() 
    throws CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(22, certificates.size());
    }
    
    @Test
    public void testGetCertificatesSerial() 
    throws CertStoreException, KeyStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesIssuer() 
    throws CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

        // should find root and ca certificate
        assertEquals(2, certificates.size());
    }
    
    @Test
    public void testGetCertificatesSubject() 
    throws CertStoreException, KeyStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSubject(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

        // should find root
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    @Test
    public void testGetCertificatesEmailAltNameUsingSelector() 
    throws IOException, CertStoreException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.addSubjectAlternativeName(1, "TEST3@exAmple.com");
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

        assertEquals(1, certificates.size());
    }
}
