/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.jce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.security.cert.CRL;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class X509CertStoreCRLProviderTest
{
    private static final File testBase = new File("test/resources/testdata/crls");
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static CertStore certStore;
    private static CertStore otherCertStore;
    private static X509CertStoreParameters certStoreParams;    
    private static X509CertStoreParameters otherCertStoreParams;    
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();

        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStoreParams = new X509CertStoreParameters(new X509CRLStoreExtAutoCommitFactory(sessionSource, "crls").create());        
        certStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, certStoreParams, "mitm");

        otherCertStoreParams = new X509CertStoreParameters(new X509CRLStoreExtAutoCommitFactory(sessionSource, "otherCRLs").create());
        otherCertStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, otherCertStoreParams, "mitm");
    }

    @Before
    public void setup() 
    throws Exception
    {
        certStoreParams.getCRLStore().removeAllEntries();

        otherCertStoreParams.getCRLStore().removeAllEntries();
        
        addCRLs();
    }
    
    private static void addCRL(String filename, X509CRLStoreExt crlStore) 
    throws CRLStoreException 
    {
        try {
            File crlFile = new File(testBase, filename);
           
            X509CRL crl = TestUtils.loadX509CRL(crlFile);
            
            crlStore.addCRL(crl);
        }
        catch(Exception e) {
            throw new CRLStoreException(e);
        }
    }
    
    private static void addCRLs() 
    throws CRLStoreException
    {
        addCRL("intel-basic-enterprise-issuing-CA.crl", certStoreParams.getCRLStore());
        addCRL("test-ca-no-next-update.crl", certStoreParams.getCRLStore());
        addCRL("itrus.com.cn.crl", certStoreParams.getCRLStore());
        addCRL("itrus.com.cn.crl", otherCertStoreParams.getCRLStore());
        addCRL("test-ca.crl", otherCertStoreParams.getCRLStore());
        addCRL("ThawteSGCCA.crl", otherCertStoreParams.getCRLStore());
    }


    @Test
    public void testContains() 
    throws Exception
    {
        File crlFile = new File(testBase, "itrus.com.cn.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);
        assertNotNull(crl);
        
        assertTrue(certStoreParams.getCRLStore().contains(crl));
        assertFalse(certStoreParams.getCRLStore().contains(null));

        crlFile = new File(testBase, "test-ca.crl");
        
        crl = TestUtils.loadX509CRL(crlFile);
        assertNotNull(crl);

        assertFalse(certStoreParams.getCRLStore().contains(crl));
    }
    
    
    @Test
    public void testAddDuplicate() 
    throws Exception
    {
        try {
            addCRL("intel-basic-enterprise-issuing-CA.crl", certStoreParams.getCRLStore());
            
            fail();
        }
        catch(CRLStoreException e) {
            
            Throwable cause = e.getCause();
            
            assertTrue(cause instanceof ConstraintViolationException);
        }
    }

    @Test
    public void testGetCRLDate() 
    throws CertStoreException, IOException, ParseException
    {
        X509CRLSelector selector = new X509CRLSelector();

        Date now = TestUtils.parseDate("28-Nov-2007 11:38:35 GMT");
        
        selector.setDateAndTime(now);

        Collection<? extends CRL> crls = certStore.getCRLs(selector);
        assertEquals(1, crls.size());

        crls = otherCertStore.getCRLs(selector);
        assertEquals(2, crls.size());
    }
    
    
    @Test
    public void testGetCRLNullDate() 
    throws CertStoreException, IOException, ParseException
    {
        X509CRLSelector selector = new X509CRLSelector();

        Date now = TestUtils.parseDate("30-Nov-2026 11:38:35 GMT");
        
        selector.setDateAndTime(now);

        Collection<? extends CRL> crls = certStore.getCRLs(selector);
        // should be 0 because the crl did not have a next date
        assertEquals(0, crls.size());

        crls = otherCertStore.getCRLs(selector);
        assertEquals(1, crls.size());
    }
    
    
    @Test
    public void testGetAllCRLs() 
    throws CertStoreException
    {
        X509CRLSelector selector = new X509CRLSelector();
        
        Collection<? extends CRL> crls = certStore.getCRLs(selector);        
        assertEquals(3, crls.size());

        crls = otherCertStore.getCRLs(selector);        
        assertEquals(3, crls.size());
    }

    @Test
    public void testGetCRLIssuer() 
    throws CertStoreException, IOException
    {
        X509CRLSelector selector = new X509CRLSelector();

        selector.addIssuerName("CN=Intel Corporation Basic Enterprise Issuing CA 1, " + 
                "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " + 
                "L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com");

        Collection<? extends CRL> crls = certStore.getCRLs(selector);        
        assertEquals(1, crls.size());
    }

    @Test
    public void testGetCRLMultipleIssuers() 
    throws CertStoreException, IOException
    {
        X509CRLSelector selector = new X509CRLSelector();

        selector.addIssuerName("CN=Intel Corporation Basic Enterprise Issuing CA 1, " + 
                "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " + 
                "L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com");

        selector.addIssuerName("CN=Thawte SGC CA, O=Thawte Consulting (Pty) Ltd., C=ZA");
        
        selector.addIssuerName("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        Collection<? extends CRL> crls = otherCertStore.getCRLs(selector);        
        assertEquals(2, crls.size());
    }

    @Test
    public void testGetCRLNoMatch() 
    throws CertStoreException, IOException
    {
        X509CRLSelector selector = new X509CRLSelector();

        selector.addIssuerName("CN=Intel Corporation Basic Enterprise Issuing CA 1, " + 
                "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " + 
                "L=Folsom, ST=CA, C=US");

        Collection<? extends CRL> crls = certStore.getCRLs(selector);        
        assertEquals(0, crls.size());
    }    
}
