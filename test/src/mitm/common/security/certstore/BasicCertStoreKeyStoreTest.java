/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.List;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.util.BigIntegerUtils;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class BasicCertStoreKeyStoreTest
{
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;
    private static X509BasicCertStore certStore; 
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        certStore = new BasicCertStoreKeyStore(keyStore);
    }

    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
            
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }

    @Test
    public void testCertificateIterator() 
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();
        
        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(22, certificates.size());
    }

    @Test
    public void testCertificateIteratorNullSelector() 
    throws CertStoreException, CloseableIteratorException
    {
        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(null);
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(22, certificates.size());
    }

    @Test
    public void testCertificateIteratorNoMatch() 
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(new BigInteger("123", 10));

        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(0, certificates.size());
    }

    @Test
    public void testCertificateIteratorSerialMatch() 
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));

        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(1, certificates.size());
    }
}
