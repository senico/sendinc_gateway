/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.security.auth.x500.X500Principal;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.cms.SignerIdentifier;
import mitm.common.security.cms.SignerIdentifierImpl;
import mitm.common.util.BigIntegerUtils;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class X509CertStoreExtHibernateTest
{
    private static final Logger logger = LoggerFactory.getLogger(X509CertStoreExtHibernateTest.class);
    
    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static KeyStore keyStore;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);
    }

    
    @Before
    public void setup() 
    throws Exception 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
  
        certStore.removeAllEntries();
        
        keyStore = loadKeyStore(certStore, new File("test/resources/testdata/keys/testCertificates.p12"), "test");        
    }
    
    private static KeyStore loadKeyStore(X509CertStoreExt certStore, File file, String password) 
    throws Exception 
    {        
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());
        
        Enumeration<String> aliases = keyStore.aliases();
        
        while (aliases.hasMoreElements()) 
        {
            String alias = aliases.nextElement();

            Certificate certificate = keyStore.getCertificate(alias);
            
            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            try {
                boolean hasPrivateKey = keyStore.isKeyEntry(alias) && 
                        (keyStore.getKey(alias, null) instanceof PrivateKey);

                alias = hasPrivateKey ? alias : null;

                certStore.addCertificate((X509Certificate)certificate, alias);
            }
            catch (UnrecoverableKeyException e) {
                logger.error("Could not get the key.",e);
            }
            catch (CertStoreException e) {
                logger.error("CertStoreException.",e);
            }
        }
        
        return keyStore;
    }

    @Test
    public void testGetCertificatesIssuerIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(selector);

        // should find root and ca certificate
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);
        
        assertEquals(2, certificates.size());
    }

    
    @Test
    public void testGetCertificatesAllNullSelectorIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(null);

        // should find root and ca certificate
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);
        
        assertEquals(22, certificates.size());
    }

    @Test
    public void testGetCertificatesAllIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(selector);

        // should find root and ca certificate
        
        List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);
        
        assertEquals(22, certificates.size());
    }
    
    @Test
    public void testGetCertStoreEntryIssuerIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        // we must now use the auto commit version because we need to keep the session longer because we
        // will use a iterator over the CertStore entries and this requires a live scrollable cursor 
        X509CertStoreExt certStore = new X509CertStoreExtInjectSessionFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(selector,
        		MissingKeyAlias.ALLOWED, null, null);

        // should find root and ca certificate
        
        List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(2, certificates.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }

    @Test
    public void testGetCertStoreEntryAllNullSelectorIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        // we must now use the auto commit version because we need to keep the session longer because we
        // will use a iterator over the CertStore entries and this requires a live scrollable cursor 
        X509CertStoreExt certStore = new X509CertStoreExtInjectSessionFactory(sessionSource, "test").create();

        CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(null,
        		MissingKeyAlias.ALLOWED, null, null);

        // should find root and ca certificate
        
        List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(22, certificates.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }

    @Test
    public void testGetCertStoreEntryAllIterator() 
    throws CertStoreException, CloseableIteratorException 
    {
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        // we must now use the auto commit version because we need to keep the session longer because we
        // will use a iterator over the CertStore entries and this requires a live scrollable cursor 
        X509CertStoreExt certStore = new X509CertStoreExtInjectSessionFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();
        
        CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(selector, 
        		MissingKeyAlias.ALLOWED, null, null);

        // should find root and ca certificate
        
        List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);
        
        assertEquals(22, certificates.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }
    
    
    @Test
    public void testGetSize() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        long size = certStore.size();
        
        assertEquals(22, size);
    }

    @Test
    public void testGetSizeMissingKeyAliasNotAllowed() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        long size = certStore.size(null, MissingKeyAlias.NOT_ALLOWED);
        
        assertEquals(20, size);
    }

    @Test
    public void testGetSizeExpiredNotAllowed() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        long size = certStore.size(Expired.NOT_ALLOWED, null);
        
        assertEquals(21, size);
    }
    
    @Test
    public void testGetLatest() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        X509CertStoreEntry entry = certStore.getLatest();
        
        assertNotNull(entry);
        
        X509Certificate certificate = entry.getCertificate();

        assertNotNull(certificate);
        
        assertEquals("1178C3B653829E895ACB7100EB1F627", X509CertificateInspector.getSerialNumberHex(certificate));
    }
    
    
    @Test
    public void testRemoveCertificate() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        assertTrue(certStore.contains(certificate));
        
        certStore.removeCertificate(certificate);
        
        assertFalse(certStore.contains(certificate));
        
        assertEquals(21, certStore.size());
    }


    @Test
    public void testRemoveAll() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();
        
        assertEquals(22, certStore.getCertificates(selector).size());
        
        certStore.removeAllEntries();
        
        assertEquals(0, certStore.getCertificates(selector).size());

        assertEquals(0, certStore.size());
    }
    
    
    @Test
    public void testAddDuplicate() 
    throws KeyStoreException, CertStoreException, IOException
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        try {
            certStore.addCertificate(certificate);
            
            fail();
        }
        catch(CertStoreException e) {
            // should be thrown
            assertEquals("Certificate is already stored.", e.getMessage());
        }
    }
    
    @Test
    public void testGetCertificatesIssuerSerial() 
    throws CertStoreException, KeyStoreException
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        X509CertSelector selector = new X509CertSelector();
        
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");
        
        selector.setIssuer(certificate.getIssuerX500Principal());
        selector.setSerialNumber(certificate.getSerialNumber());
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
   
    
    @Test
    public void testGetCertificatesSignerIdentifier() 
    throws CertStoreException, KeyStoreException, IOException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();
            
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        SignerIdentifier identifier = new SignerIdentifierImpl(certificate);

        CertSelector selector = identifier.getSelector();

        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    
    @Test
    public void testGetCertificatesSubjectKeyIdentifier() 
    throws CertStoreException, KeyStoreException, IOException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        SignerIdentifier identifier = new SignerIdentifierImpl(null, null, 
                X509CertificateInspector.getSubjectKeyIdentifier(certificate));

        CertSelector selector = identifier.getSelector();

        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    
    
    @Test
    public void testGetCertificatesUnoptimized() 
    throws CertStoreException, KeyStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        X509CertSelector selector = new X509CertSelector();

        selector.setKeyUsage(certificate.getKeyUsage());
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(17, certificates.size());
    }
    
    
    @Test
    public void testGetCertificatesNoMatch() 
    throws CertStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(new BigInteger("123", 10));
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(0, certificates.size());
    }
    
    
    @Test
    public void testGetCertificatesValidOnly() 
    throws CertStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setCertificateValid(new Date());
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(21, certificates.size());
    }
    

    @Test
    public void testGetCertificatesByCertificate() 
    throws CertStoreException, KeyStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

        selector.setCertificate(certificate);
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(1, certificates.size());
        
        assertEquals(certificate, certificates.iterator().next());
    }
    

    @Test
    public void testGetCertificatesNoRestriction() 
    throws CertStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        Collection<X509Certificate> certificates = certStore.getCertificates(selector);
        
        assertEquals(22, certificates.size());
    }

    @Test
    public void testGetCertificatesNoRestrictionNullSelector() 
    throws CertStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        Collection<X509Certificate> certificates = certStore.getCertificates(null);
        
        assertEquals(22, certificates.size());
    }
    

    @Test
    public void testGetCertificatesSerial() 
    throws CertStoreException, KeyStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    

    @Test
    public void testGetCertificatesIssuer() 
    throws CertStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);

        // should find root and ca certificate
        assertEquals(2, certificates.size());
    }
    

    @Test
    public void testGetCertificatesSubject() 
    throws CertStoreException, KeyStoreException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.setSubject(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);

        // should find root
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
        
        assertEquals(1, certificates.size());
        assertEquals(certificate, certificates.iterator().next());
    }
    

    @Test
    public void testGetCertificatesEmailAltNameUsingSelector() 
    throws CertStoreException, IOException 
    {
        X509CertStoreExt certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "test").create();

        X509CertSelector selector = new X509CertSelector();

        selector.addSubjectAlternativeName(1, "TEST3@exAmple.com");
        
        Collection<X509Certificate> certificates = certStore.getCertificates(selector);

        assertEquals(1, certificates.size());
    }    
}
