/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.hibernate;

import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.annotations.InjectHibernateSession;
import mitm.common.hibernate.annotations.InjectHibernateSessionSource;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.certstore.CertificateAlreadyExistsException;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.X509StoreEventListener;
import mitm.common.util.CloseableIterator;

import org.hibernate.Session;

/**
 * Implementation of X509CertStoreExt for auto commit X509CertStoreExt store. 
 * 
 * @author Martijn Brinkers
 *
 */
public class AnnotatedX509CertStoreExtHibernate implements X509CertStoreExt
{
	private String storeName;

	private SessionManager sessionManager;
	
    public AnnotatedX509CertStoreExtHibernate(String storeName)
    {
    	this.storeName = storeName;
    }
    
    /* should have a default constructor for Auto commit proxy */
    public AnnotatedX509CertStoreExtHibernate()
    {
    }

    public void setStoreName(String storeName) {
    	this.storeName = storeName;
    }
    
    private SessionManager getSessionManager()
    {
    	if (sessionManager == null) {
    		throw new IllegalStateException("sessionManager is not valid.");
    	}
    	
    	return sessionManager;
    }

    @InjectHibernateSession
    public void setSession(Session session) {
        getSessionManager().setSession(session);
    }
        
    @InjectHibernateSessionSource
    public void setSessionSource(HibernateSessionSource sessionSource) 
    {
        sessionManager = new SessionManagerImpl(sessionSource);
    }

    private X509CertStoreExtHibernate getDelegate() {
    	return new X509CertStoreExtHibernate(storeName, getSessionManager());
    }
    
    @Override
    @StartTransaction
    public X509CertStoreEntryHibernate addCertificate(X509Certificate certificate, String keyAlias) 
    throws CertStoreException, CertificateAlreadyExistsException
    {
        return getDelegate().addCertificate(certificate, keyAlias);
    }
    

    @Override
    @StartTransaction
    public X509CertStoreEntryHibernate addCertificate(X509Certificate certificate)
    throws CertStoreException 
    {
        return getDelegate().addCertificate(certificate);
    }
    
    @Override
    @StartTransaction
    public void removeCertificate(X509Certificate certificate) 
    throws CertStoreException
    {
    	getDelegate().removeCertificate(certificate);
    }
    
    @Override
    @StartTransaction
    public void removeAllEntries() 
    throws CertStoreException
    {
    	getDelegate().removeAllEntries();
    }
     
    @Override
    @StartTransaction
    public X509CertStoreEntryHibernate getLatest()
    {
        return getDelegate().getLatest();
    }

    @Override
    @StartTransaction
    public long size() {
        return getDelegate().size();
    }

    @Override
    @StartTransaction
    public long size(Expired expired, MissingKeyAlias missingKeyAlias) {
    	return getDelegate().size(expired, missingKeyAlias);
    }
    
    @Override
    @StartTransaction
    public X509CertStoreEntryHibernate getByCertificate(X509Certificate certificate)
    throws CertStoreException 
    {
        return getDelegate().getByCertificate(certificate);
    }    

    @Override
    @StartTransaction
    public boolean contains(X509Certificate certificate)
    throws CertStoreException
    {
        return getDelegate().contains(certificate);
    }

    @Override
    @StartTransaction
    public X509CertStoreEntryHibernate getByThumbprint(String thumbprint) {
        return getDelegate().getByThumbprint(thumbprint);
    }

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CertStoreEntry> getByEmail(String email, Match match, Expired expired,
			MissingKeyAlias missingKeyAlias) 
	throws CertStoreException 
	{
		return getDelegate().getByEmail(email, match, expired, missingKeyAlias);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CertStoreEntry> getByEmail(String email, Match match, Expired expired,
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().getByEmail(email, match, expired, missingKeyAlias, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public long getByEmailCount(String email, Match match, Expired expired,	MissingKeyAlias missingKeyAlias) 
	throws CertStoreException 
	{
		return getDelegate().getByEmailCount(email, match, expired, missingKeyAlias);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CertStoreEntry> getCertStoreIterator(CertSelector certSelector, 
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().getCertStoreIterator(certSelector, missingKeyAlias, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector) 
	throws CertStoreException 
	{
		return getDelegate().getCertificateIterator(certSelector);
	}

    @StartTransaction
	public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector, 
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().getCertificateIterator(certSelector, missingKeyAlias, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public Collection<X509Certificate> getCertificates(CertSelector certSelector)
	throws CertStoreException 
	{
		return getDelegate().getCertificates(certSelector);
	}

    @StartTransaction
	public Collection<X509Certificate> getCertificates(CertSelector certSelector, 
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().getCertificates(certSelector, missingKeyAlias, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public long getSearchByIssuerCount(String issuer, Expired expired, MissingKeyAlias missingKeyAlias) 
	throws CertStoreException 
	{
		return getDelegate().getSearchByIssuerCount(issuer, expired, missingKeyAlias);
	}

    @Override
    @StartTransaction
	public long getSearchBySubjectCount(String subject, Expired expired, MissingKeyAlias missingKeyAlias) 
	throws CertStoreException 
	{
		return getDelegate().getSearchBySubjectCount(subject, expired, missingKeyAlias);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CertStoreEntry> searchByIssuer(String issuer, Expired expired, 
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().searchByIssuer(issuer, expired, missingKeyAlias, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CertStoreEntry> searchBySubject(String subject, Expired expired, 
			MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults) 
	throws CertStoreException 
	{
		return getDelegate().searchBySubject(subject, expired, missingKeyAlias, firstResult, maxResults);
	}

    @Override
	public void setStoreEventListener(X509StoreEventListener eventListener) 
	{
		getDelegate().setStoreEventListener(eventListener);
	}
}
