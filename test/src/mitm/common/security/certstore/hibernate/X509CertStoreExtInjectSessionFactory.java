/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.hibernate;

import java.security.cert.CertStoreException;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.InjectSessionProxyFactory;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.certstore.X509CertStoreExt;

public class X509CertStoreExtInjectSessionFactory implements X509CertStoreFactory
{
    private final HibernateSessionSource sessionSource;
    private final String storeName;   
    
    public X509CertStoreExtInjectSessionFactory(HibernateSessionSource sessionSource, String storeName)
    {
        this.sessionSource = sessionSource;
        this.storeName = storeName;
    }
    
    /**
     * Creates a proxied instance of X509CertStoreExtHibernate which will inject a session and session source
     * @param sessionSource
     * @param storeName
     * @return
     * @throws CertStoreException
     */
    @Override
    public X509CertStoreExt create() 
    throws CertStoreException
    {
        InjectSessionProxyFactory<AnnotatedX509CertStoreExtHibernate> proxyFactory;
        
        try {
            proxyFactory = new InjectSessionProxyFactory<AnnotatedX509CertStoreExtHibernate>(
                    AnnotatedX509CertStoreExtHibernate.class, sessionSource);
        }
        catch (NoSuchMethodException e) {
            throw new CertStoreException(e);
        }

        AnnotatedX509CertStoreExtHibernate certStore;
        
        try {
            certStore = proxyFactory.createProxy();
        }
        catch (ProxyFactoryException e) {
            throw new CertStoreException(e);
        }
        catch (IllegalArgumentException e) {
            throw new CertStoreException(e);
        }

        certStore.setStoreName(storeName);
        
        return certStore;
    }
}
