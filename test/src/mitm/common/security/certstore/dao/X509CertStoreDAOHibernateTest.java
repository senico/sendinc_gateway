/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.certstore.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mitm.common.hibernate.DatabaseActionExecutor;
import mitm.common.hibernate.DatabaseActionExecutorBuilder;
import mitm.common.hibernate.DatabaseException;
import mitm.common.hibernate.DatabaseVoidAction;
import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionAdapterFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.KeyUsageType;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.hibernate.X509CertStoreEntryHibernate;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class X509CertStoreDAOHibernateTest
{
    private static final Logger logger = LoggerFactory.getLogger(X509CertStoreDAOHibernateTest.class);
    
    private static SecurityFactory securityFactory;
    private static HibernateSessionSource sessionSource;
    private static DatabaseActionExecutor executor;
    private static KeyStore keyStore;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        executor = DatabaseActionExecutorBuilder.createDatabaseActionExecutor(sessionManager);
    }

    @Before
    public void setup() 
    throws Exception 
    {
        removeAll();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");        
    }
    
    private static KeyStore loadKeyStore(File file, String password) 
    throws Exception 
    {        
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());
        
        Enumeration<String> aliases = keyStore.aliases();
        
        while (aliases.hasMoreElements()) 
        {
            String alias = aliases.nextElement();

            Certificate certificate = keyStore.getCertificate(alias);
            
            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            try {
                boolean hasPrivateKey = keyStore.isKeyEntry(alias) && 
                        (keyStore.getKey(alias, null) instanceof PrivateKey);

                alias = hasPrivateKey ? alias : null;

                addCertificate((X509Certificate) certificate, alias);
            }
            catch (UnrecoverableKeyException e) {
                logger.error("Could not get the key.",e);
            }
            catch (DatabaseException e) {
                logger.error("Some Database problem.",e);
            }
        }
        
        return keyStore;
    }

    private int getEntryCount(CloseableIterator<?> entryIterator) 
    throws CloseableIteratorException
    {
        int i = 0;

        assertFalse(entryIterator.isClosed());
        
        while(entryIterator.hasNext()) 
        {
            Object entry = entryIterator.next();
            
            assertNotNull(entry);
            
            i++;
        }
        
        assertTrue(entryIterator.isClosed());
        
        // closing it again should be no problem
        entryIterator.close();
        
        return i;
    }
    
    
    private static void addCertificate(final X509Certificate certificate, final String alias) 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                addCertificateAction(session, certificate, alias);
            }
        });
    }

    private static void addCertificateAction(Session session, X509Certificate certificate, String alias) 
    throws DatabaseException
    {
        X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

        try {
            dao.addCertificate(certificate, alias);
        }
        catch (CertStoreException e) {
            throw new DatabaseException(e);
        }
    }

    private static void removeAll() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                removeAllAction(session);
            }
        });
    }
    
    private static void removeAllAction(Session session) 
    throws DatabaseException
    {
        X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

        try {
            dao.removeAllEntries();
        }
        catch (CertStoreException e) {
            throw new DatabaseException(e);
        }
        
        assertEquals(0, dao.getRowCount());
    }

    @Test
    public void testSearchByIssuerCount()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
            	try {
            		searchByIssuerCountAction(session);
				} 
            	catch (Exception e) {
            		throw new DatabaseException(e);
				}
            }
        });
    }
    
    private void searchByIssuerCountAction(Session session) 
    throws CloseableIteratorException, ParseException
    {
    	X509CertStoreDAOHibernate dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
        
    	Date now = new Date();
    	
        int count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.ALLOWED, 
        		MissingKeyAlias.ALLOWED, now);
        
        assertEquals(20, count);

        Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

        count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, future);
        
        assertEquals(1, count);

        now = new Date();
        
        count = dao.getSearchByIssuerCount("%CN=MITM Test Root%", Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now);
        
        assertEquals(2, count);

        count = dao.getSearchByIssuerCount("%CN=MITM Test Root%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now);
        
        assertEquals(0, count);
    }
    
    @Test
    public void testSearchBySubjectCount()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
            	try {
            		searchBySubjectCountAction(session);
				} 
            	catch (Exception e) {
            		throw new DatabaseException(e);
				}
            }
        });
    }
    
    private void searchBySubjectCountAction(Session session) 
    throws CloseableIteratorException, ParseException
    {
    	X509CertStoreDAOHibernate dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
        
    	Date now = new Date();

        Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");
    	
        int count = dao.getSearchBySubjectCount("%MITM Test CA%", Expired.ALLOWED, 
        		MissingKeyAlias.ALLOWED, now);
        
        assertEquals(1, count);

        count = dao.getSearchBySubjectCount("%MITM Test CA%", Expired.ALLOWED, MissingKeyAlias.NOT_ALLOWED, now);
        
        assertEquals(0, count);
        
        count = dao.getSearchBySubjectCount("%Not yet valid%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, future);
        
        assertEquals(1, count);

        count = dao.getSearchBySubjectCount("%CN=%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now);
        
        assertEquals(18, count);
    }
    
    @Test
    public void testGetByEmailCount()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
            	try {
            		getByEmailCountAction(session);
				} 
            	catch (Exception e) {
            		throw new DatabaseException(e);
				}
            }
        });
    }
    
    private void getByEmailCountAction(Session session) 
    throws CloseableIteratorException, ParseException
    {
    	X509CertStoreDAOHibernate dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
        
    	Date now = new Date();
    	
        Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");
    	
    	long count = dao.getByEmailCount("test@example.com", Match.EXACT, Expired.ALLOWED, MissingKeyAlias.ALLOWED, now);
    	
    	assertEquals(18, count);

    	count = dao.getByEmailCount("%@%", Match.LIKE, Expired.ALLOWED, MissingKeyAlias.ALLOWED, future);
    	
    	assertEquals(20, count);

    	count = dao.getByEmailCount("%@%", Match.LIKE, Expired.ALLOWED, MissingKeyAlias.ALLOWED, now);
    	
    	assertEquals(20, count);

    	count = dao.getByEmailCount("%@%", Match.LIKE, Expired.ALLOWED, MissingKeyAlias.NOT_ALLOWED, now);
    	
    	assertEquals(18, count);

    	count = dao.getByEmailCount("%@%", Match.LIKE, Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now);
    	
    	assertEquals(17, count);
    	
    	count = dao.getByEmailCount("%@%", Match.LIKE, Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, future);
    	
    	assertEquals(1, count);
    	
    	/*
    	 * Should return the entries without an email address or without a valid email address
    	 */
    	count = dao.getByEmailCount(null, Match.LIKE, Expired.ALLOWED, MissingKeyAlias.ALLOWED, now);
    	
    	assertEquals(2, count);
    }
    
    @Test
    public void testSearchBySubject()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
            	try {
            		searchBySubjectAction(session);
				} 
            	catch (Exception e) {
            		throw new DatabaseException(e);
				}
            }
        });
    }

    private void searchBySubjectAction(Session session) 
    throws CloseableIteratorException, ParseException
    {
    	X509CertStoreDAOHibernate dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
        
    	Date now = new Date();

        Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");
    	
        CloseableIterator<X509CertStoreEntryHibernate> iterator = dao.searchBySubject("%MITM Test CA%", Expired.ALLOWED, 
        		MissingKeyAlias.ALLOWED, now, null, null);
        
        assertEquals(1, getEntryCount(iterator));

        iterator = dao.searchBySubject("%MITM Test CA%", Expired.ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, null, null);
        
        assertEquals(0, getEntryCount(iterator));
        
        iterator = dao.searchBySubject("%Not yet valid%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, future, null, null);
        
        assertEquals(1, getEntryCount(iterator));

        iterator = dao.searchBySubject("%CN=%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, null, null);
        
        assertEquals(18, getEntryCount(iterator));

        iterator = dao.searchBySubject("%CN=%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, 0, 100);
        
        assertEquals(18, getEntryCount(iterator));

        iterator = dao.searchBySubject("%CN=%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, 0, 4);
        
        assertEquals(4, getEntryCount(iterator));
    }
    
    @Test
    public void testSearchByIssuer()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
            	try {
            		searchByIssuerAction(session);
				} 
            	catch (Exception e) {
            		throw new DatabaseException(e);
				}
            }
        });
    }

    private void searchByIssuerAction(Session session) 
    throws CloseableIteratorException, ParseException
    {
    	X509CertStoreDAOHibernate dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
        
    	Date now = new Date();
    	
        CloseableIterator<X509CertStoreEntryHibernate> iterator = dao.searchByIssuer("%MITM Test CA%", Expired.ALLOWED, 
        		MissingKeyAlias.ALLOWED, now, null, null);
        
        assertEquals(20, getEntryCount(iterator));

        Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

        iterator = dao.searchByIssuer("%MITM Test CA%", Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, future, null, null);
        
        assertEquals(1, getEntryCount(iterator));

        now = new Date();
        
        iterator = dao.searchByIssuer("%CN=MITM Test Root%", Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null);
        
        assertEquals(2, getEntryCount(iterator));

        iterator = dao.searchByIssuer("%CN=MITM Test Root%", Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, null, null);
        
        assertEquals(0, getEntryCount(iterator));

        iterator = dao.searchByIssuer("%MITM Test CA%", Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, 0, 100);
        
        assertEquals(20, getEntryCount(iterator));

        iterator = dao.searchByIssuer("%MITM Test CA%", Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, 0, 5);
        
        assertEquals(5, getEntryCount(iterator));
    }
    
    @Test
    public void testGetCertStoreIterator() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetCertStoreIteratorAction(session);
            }
        });
    }
    
    private void testGetCertStoreIteratorAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

            X509CertSelector selector = new X509CertSelector();
            
            Set<KeyUsageType> keyUsages = new HashSet<KeyUsageType>();
            
            keyUsages.add(KeyUsageType.CRLSIGN);
            
            selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

            CloseableIterator<X509CertStoreEntryHibernate> iterator = dao.getCertStoreIterator(selector, 
                    MissingKeyAlias.ALLOWED, null, null);
            
            assertEquals(3, getEntryCount(iterator));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    
    @Test
    public void testGetCertStoreIteratorHasNextMultiple() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetCertStoreIteratorHasNextMultipleAction(session);
            }
        });
    }
    
    private void testGetCertStoreIteratorHasNextMultipleAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

            X509CertSelector selector = new X509CertSelector();
            
            Set<KeyUsageType> keyUsages = new HashSet<KeyUsageType>();
            
            keyUsages.add(KeyUsageType.CRLSIGN);
            
            selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

            CloseableIterator<X509CertStoreEntryHibernate> iterator = dao.getCertStoreIterator(selector, 
                    MissingKeyAlias.ALLOWED, null, null);
            
            // check that hasNext does not advance to the next entry
            iterator.hasNext();
            iterator.hasNext();
            iterator.hasNext();
            
            assertEquals(3, getEntryCount(iterator));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    
    @Test
    public void testGetCertificateIterator() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetCertificateIteratorAction(session);
            }
        });
    }
    
    private void testGetCertificateIteratorAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

            X509CertSelector selector = new X509CertSelector();
            
            Set<KeyUsageType> keyUsages = new HashSet<KeyUsageType>();
            
            keyUsages.add(KeyUsageType.CRLSIGN);
            
            selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

            CloseableIterator<X509Certificate> iterator = dao.getCertificateIterator(selector, 
                    MissingKeyAlias.ALLOWED, null, null);
            
            assertEquals(3, getEntryCount(iterator));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    

    @Test
    public void testGetCertificateIteratorHasNextMultiple() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetCertificateIteratorHasNextMultipleAction(session);
            }
        });
    }
    
    private void testGetCertificateIteratorHasNextMultipleAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

            X509CertSelector selector = new X509CertSelector();
            
            Set<KeyUsageType> keyUsages = new HashSet<KeyUsageType>();
            
            keyUsages.add(KeyUsageType.CRLSIGN);
            
            selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

            CloseableIterator<X509Certificate> iterator = dao.getCertificateIterator(selector,
                    MissingKeyAlias.ALLOWED, null, null);
            
            // check that hasNext does not advance to the next entry
            iterator.hasNext();
            iterator.hasNext();
            iterator.hasNext();
            iterator.hasNext();
            
            assertEquals(3, getEntryCount(iterator));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    
    @Test
    public void testGetCertificates() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetCertificatesAction(session);
            }
        });
    }
    
    private void testGetCertificatesAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

            X509CertSelector selector = new X509CertSelector();
            
            Set<KeyUsageType> keyUsages = new HashSet<KeyUsageType>();
            
            keyUsages.add(KeyUsageType.CRLSIGN);
            
            selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));
            
            Collection<X509Certificate> certificates = dao.getCertificates(selector, MissingKeyAlias.ALLOWED,
                    null, null);
            
            assertEquals(3, certificates.size());
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    
    @Test
    public void testRowCount() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testRowCountAction(session);
            }
        });
    }
    
    private void testRowCountAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            assertEquals(22, dao.getRowCount());
            
            X509CertStoreDAO otherStoreDAO = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), 
                    "some_non_existing_store");
            
            assertEquals(0, otherStoreDAO.getRowCount());            
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    @Test
    public void testGetLatest() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                testGetLatestAction(session);
            }
        });
    }
    
    private void testGetLatestAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            X509CertStoreEntryHibernate entry = dao.getLatest();
            
            assertNotNull(entry);
            
            assertEquals("1178C3B653829E895ACB7100EB1F627", X509CertificateInspector.getSerialNumberHex(entry.getCertificate()));
            
            X509CertStoreDAO otherStoreDAO = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), 
                    "some_non_existing_store");
            
            entry = otherStoreDAO.getLatest();
            
            assertNull(entry);
            
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
            assertNotNull(certificate);
            
            otherStoreDAO.addCertificate(certificate, null);

            entry = otherStoreDAO.getLatest();
            assertNotNull(entry);
            
            assertEquals(certificate, entry.getCertificate());
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    
    @Test(expected = ConstraintViolationException.class)
    public void testAddExistingCertificateSameStore()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                try {
                    addExistingCertificateSameStoreAction(session);
                }
                catch (Exception e) {
                    fail();
                }
            }
        });
    }
    
    private void addExistingCertificateSameStoreAction(Session session) 
    throws KeyStoreException, CertStoreException 
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
        assertNotNull(certificate);
        
        X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");

        dao.addCertificate(certificate, null);
    }

    
    @Test
    public void testAddExistingCertificateDifferentStore() 
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                try {
                    addExistingCertificateDifferentStoreAction(session);
                }
                catch (Exception e) {
                    fail();
                }
            }
        });
    }
    
    private void addExistingCertificateDifferentStoreAction(Session session) 
    throws KeyStoreException, CertStoreException 
    {
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
        assertNotNull(certificate);
        
        X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "other store");

        dao.addCertificate(certificate, null);
    }
    
    @Test
    public void testAddEmail()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                addEmailAction(session);
            }
        });
    }
    
    private void addEmailAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate("multipleEmail");
            assertNotNull(certificate);
            
            X509CertStoreEntryHibernate entry = dao.getByCertificate(certificate);
            
            assertEquals(certificate, entry.getCertificate());

            assertTrue(entry.getEmail().contains("test@example.com"));
            assertTrue(entry.getEmail().contains("test2@example.com"));
            assertTrue(entry.getEmail().contains("test3@example.com"));

            // It should be possible to add an email address
            entry.getEmail().add("newAddress@example.com");
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    
    @Test
    public void testFindByEmail()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                findByEmailAction(session);
            }
        });
    }

    private void findByEmailAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            X509CertStoreEntryHibernate entry;
    
            Date now = new Date();
            
            List<X509CertStoreEntryHibernate> entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", 
                    Match.EXACT, Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, 
                            null, null));
    
            assertEquals(18, entries.size());
            
            // again but now without expired certificates
            entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(17, entries.size());
            
            // ca@example.com should have no alias. Find with missing alias allowed
            entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(1, entries.size());
            
            entry = entries.get(0);
            assertEquals(null, entry.getKeyAlias());
            assertEquals("115FCAD6B536FD8D49E72922CD1F0DA", new X509CertificateInspector(entry.getCertificate()).getSerialNumberHex());
            
            // now missing alias not allowed
            entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.NOT_ALLOWED, now, null, null));
    
            assertEquals(0, entries.size());
            
            // no match on email
            entries = CloseableIteratorUtils.toList(dao.getByEmail("xxx", Match.EXACT,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(0, entries.size());
            
            // null email. We should now get the entries without an (valid) email
            entries = CloseableIteratorUtils.toList(dao.getByEmail(null, Match.EXACT,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(2, entries.size());       

            // match with spaces begin and end
            entries = CloseableIteratorUtils.toList(dao.getByEmail("  test@Example.COM  ", Match.EXACT,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(18, entries.size());
            
            // set the date to a different date
            now = TestUtils.parseDate("17-Oct-2006 07:38:35 GMT");

            // expired allowed
            entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(18, entries.size());
            
            // expired not allowed
            entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(0, entries.size());
    
            // ca is one second valid
            now = TestUtils.parseDate("01-Nov-2007 07:39:35 GMT");
            
            entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(1, entries.size());

            // ca is one second not yet valid
            now = TestUtils.parseDate("01-Nov-2007 06:37:35 GMT");
            
            entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                    Expired.NOT_ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
    
            assertEquals(0, entries.size());           

            now = new Date();
            
            // test like
            entries = CloseableIteratorUtils.toList(dao.getByEmail("test%", Match.LIKE,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
            
            assertEquals(18, entries.size());           

            // test like
            entries = CloseableIteratorUtils.toList(dao.getByEmail("test3%", Match.LIKE,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
            
            assertEquals(1, entries.size());           

            // test like
            entries = CloseableIteratorUtils.toList(dao.getByEmail("%", Match.LIKE,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
            
            assertEquals(20, entries.size());
            
            // test like case insensitive
            entries = CloseableIteratorUtils.toList(dao.getByEmail("TEST3%", Match.LIKE,
                    Expired.ALLOWED, MissingKeyAlias.ALLOWED, now, null, null));
            
            assertEquals(1, entries.size());           
            
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    @Test
    public void findByThumbprint()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                findByThumbprintAction(session);
            }
        });
    }
    
    private void findByThumbprintAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate("NotYetValid");
            assertNotNull(certificate);
            
            X509CertificateInspector inspector = new X509CertificateInspector(certificate);

            X509CertStoreEntryHibernate entry = dao.getByThumbprint(inspector.getThumbprint());
            
            assertEquals(certificate, entry.getCertificate());
            
            // uppercase
            entry = dao.getByThumbprint("8DD973D3B38C0A8CBB055FA41F20CB36041041C9BB70662D93B54" + 
                "B4F34FE6D0CF03B70E70DFAE8EC567D9122C43E74CEBD0E8DB0D421CD3DE8245CF6D6102945");
            
            assertNotNull(entry);
            assertNotNull(entry.getCertificate());

            // lowercase
            entry = dao.getByThumbprint("8dd973d3b38c0a8cbb055fa41f20cb36041041c9bb70662d93b54" + 
                "b4f34fe6d0cf03b70e70dfae8ec567d9122c43e74cebd0e8db0d421cd3de8245cf6d6102945");
            
            assertNotNull(entry);
            assertNotNull(entry.getCertificate());

            // non existing 
            entry = dao.getByThumbprint("xxx");
            
            assertNull(entry);
            
            // null
            entry = dao.getByThumbprint(null);
            
            assertNull(entry);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }

    @Test
    public void findByCertificate()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                findByCertificateAction(session);
            }
        });
    }
    
    private void findByCertificateAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate("multipleEmail");
            assertNotNull(certificate);
            
            X509CertStoreEntryHibernate entry = dao.getByCertificate(certificate);
            
            assertEquals(certificate, entry.getCertificate());

            assertTrue(entry.getEmail().contains("test@example.com"));
            assertTrue(entry.getEmail().contains("test2@example.com"));
            assertTrue(entry.getEmail().contains("test3@example.com"));

            // no email test
            certificate = (X509Certificate) keyStore.getCertificate("noEmail");
            assertNotNull(certificate);
            
            entry = dao.getByCertificate(certificate);
            
            assertEquals(certificate, entry.getCertificate());

            assertEquals("[]", entry.getEmail().toString());
            
            // null test
            entry = dao.getByCertificate(null);
            
            assertNull(entry);
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
    
    @Test
    public void findAll()
    throws DatabaseException
    {
        executor.executeTransaction(new DatabaseVoidAction()
        {
            @Override
            public void doAction(Session session) 
            throws DatabaseException 
            {
                findAllAction(session);
            }
        });
    }

    
    private void findAllAction(Session session)
    throws DatabaseException
    {
        try {
            X509CertStoreDAO dao = new X509CertStoreDAOHibernate(SessionAdapterFactory.create(session), "test");
            
            Date now = new Date();

            // allow expired and missing
            X509CertSelector selector = new X509CertSelector();
            
            CloseableIterator<X509CertStoreEntryHibernate> entryIterator = dao.getCertStoreIterator(selector, 
            		MissingKeyAlias.ALLOWED, null, null);            
            assertEquals(22, getEntryCount(entryIterator));

            // not allow expired, allow missing
            selector = new X509CertSelector();
            selector.setCertificateValid(now);
            
            entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.ALLOWED, null, null);
            assertEquals(21, getEntryCount(entryIterator));

            // not allow expired, not allow missing
            selector = new X509CertSelector();
            selector.setCertificateValid(now);

            entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
            assertEquals(19, getEntryCount(entryIterator));

            // a date in the past so everything is expired
            now = TestUtils.parseDate("01-Oct-2006 00:00:00 GMT");
            
            // not allow expired, not allow missing
            selector = new X509CertSelector();
            selector.setCertificateValid(now);
            
            entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
            assertEquals(0, getEntryCount(entryIterator));

            // a date in the future so everything is not yet valid
            now = TestUtils.parseDate("01-Oct-2080 00:00:00 GMT");

            // not allow expired, not allow missing
            selector = new X509CertSelector();
            selector.setCertificateValid(now);
            
            entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
            assertEquals(0, getEntryCount(entryIterator));
        }
        catch(Exception e) {
            if (e instanceof DatabaseException) {
                throw (DatabaseException)e;
            }
            throw new DatabaseException(e);
        }
    }
}
