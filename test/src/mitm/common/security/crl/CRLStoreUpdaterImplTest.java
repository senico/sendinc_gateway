/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CRLStoreUpdaterImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        rootStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create();
        crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "crls").create();
    }

    @Before
    public void setup() 
    throws Exception 
    {
        crlStore.removeAllEntries();
    }
    
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }
    }

    @Test
    public void testUpdateCRLStoreIllegalCRLDistPoint() 
    throws Exception 
    {
        addCertificates(new File(testBase, "certificates/windows-xp-all-roots.p7b"), rootStore);
        addCertificates(new File(testBase, "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"), rootStore);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        PKISecurityServices pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
                rootStore, crlStore);

        CRLDownloadParameters downloadParameters = new CRLDownloadParameters(
                1 * DateUtils.MILLIS_PER_MINUTE,  /* total timeout */  
                30 * DateUtils.MILLIS_PER_SECOND, /* connect timeout */
                30 * DateUtils.MILLIS_PER_SECOND, /* read timeout */
                25 * 1024 * 1024);                /* max bytes */
        
        CRLDownloader crlDownloader = new CRLDownloaderImpl(downloadParameters);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(pKISecurityServices.getCRLStore(),
                pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        DefaultCRLStoreUpdaterParametersBuilder paramsBuilder = new DefaultCRLStoreUpdaterParametersBuilder(
                pKISecurityServices, crlDownloader, crlStoreMaintainer);
        
        CRLStoreUpdaterParameters updaterParams = paramsBuilder.createCRLStoreUpdaterParameters();
        
        CRLStoreUpdater crlUpdater = new CRLStoreUpdaterImpl(updaterParams);
        
        // this should not result in an exception
        crlUpdater.update();
        
        // should normally not fail if a valid internet connection is available
        assertTrue("This might fail if non of the downloaded CRLs are accessible.", crlStore.size() > 0);
    }
    
    @Test
    public void testUpdateCRLStore() 
    throws Exception 
    {
        addCertificates(new File(testBase, "certificates/windows-xp-all-roots.p7b"), rootStore);
        addCertificates(new File(testBase, "certificates/windows-xp-all-intermediates.p7b"), certStore);
        addCertificates(new File(testBase, "certificates/some-public.p7b"), certStore);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        PKISecurityServices pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);

        CRLDownloadParameters downloadParameters = new CRLDownloadParameters(
                1 * DateUtils.MILLIS_PER_MINUTE,  /* total timeout */  
                30 * DateUtils.MILLIS_PER_SECOND, /* connect timeout */
                30 * DateUtils.MILLIS_PER_SECOND, /* read timeout */
                25 * 1024 * 1024);                /* max bytes */
        
        CRLDownloader crlDownloader = new CRLDownloaderImpl(downloadParameters);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(pKISecurityServices.getCRLStore(),
                pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        DefaultCRLStoreUpdaterParametersBuilder paramsBuilder = new DefaultCRLStoreUpdaterParametersBuilder(
        		pKISecurityServices, crlDownloader, crlStoreMaintainer);
        
        CRLStoreUpdaterParameters updaterParams = paramsBuilder.createCRLStoreUpdaterParameters();
        
        CRLStoreUpdater crlUpdater = new CRLStoreUpdaterImpl(updaterParams);
        
        crlUpdater.update();
    }
}
