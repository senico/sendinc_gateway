/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;

import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.test.TestUtils;

import org.apache.log4j.BasicConfigurator;
import org.bouncycastle.asn1.x509.CRLReason;
import org.junit.BeforeClass;
import org.junit.Test;


public class X509CRLBuilderImplTest
{
    private static SecurityFactory securityFactory;
    
    private static X509Certificate rootCertificate;
    private static PrivateKey rootPrivateKey;

    private static X509Certificate caCertificate;
    private static PrivateKey caPrivateKey;

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        BasicConfigurator.configure();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        loadCA();
    }

    private static void loadCA() 
    throws Exception 
    {
        KeyStore caKeyStore = securityFactory.createKeyStore("PKCS12");
        
        File file = new File("test/resources/testdata/keys/testCA.p12");
        
        FileInputStream input = new FileInputStream(file);
        
        caKeyStore.load(input, "test".toCharArray());
        
        caCertificate = (X509Certificate) caKeyStore.getCertificate("ca");
        caPrivateKey = (PrivateKey) caKeyStore.getKey("ca", null);

        rootCertificate = (X509Certificate) caKeyStore.getCertificate("root");
        rootPrivateKey = (PrivateKey) caKeyStore.getKey("root", null);

        assertNotNull(caCertificate);
        assertNotNull(caPrivateKey);
        assertNotNull(rootCertificate);
        assertNotNull(rootPrivateKey);
    }
    
    @Test
    public void testGenerateCRL()
    throws Exception
    {
        X509CRLBuilder builder = securityFactory.createX509CRLBuilder();
        
        Date thisDate = TestUtils.parseDate("30-Nov-2007 11:38:35 GMT");

        Date nextDate = TestUtils.parseDate("30-Nov-2027 11:38:35 GMT");
        
        builder.setThisUpdate(thisDate);
        builder.setNextUpdate(nextDate);
        builder.setSignatureAlgorithm("SHA256WithRSAEncryption");

        builder.addCRLEntry(caCertificate.getSerialNumber(), thisDate, CRLReason.cACompromise);

        KeyAndCertificate issuer = new KeyAndCertificateImpl(rootPrivateKey, rootCertificate);
        
        X509CRL crl = builder.generateCRL(issuer);

        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                crl.getIssuerX500Principal().toString());
        assertEquals(thisDate, crl.getThisUpdate());
        assertEquals(nextDate, crl.getNextUpdate());
        assertEquals(1, crl.getRevokedCertificates().size());
        
        assertTrue(crl.isRevoked(caCertificate));

        crl.verify(issuer.getCertificate().getPublicKey());
        
        /*
         * Check if the generated CRL is compatible with JCE X509CRL
         */
        CertificateFactory fac = CertificateFactory.getInstance("X.509");
        
        X509CRL jceCRL = (X509CRL) fac.generateCRL(new ByteArrayInputStream(crl.getEncoded()));
        
        assertTrue(jceCRL.isRevoked(caCertificate));
        
        jceCRL.verify(issuer.getCertificate().getPublicKey());
    }
}
