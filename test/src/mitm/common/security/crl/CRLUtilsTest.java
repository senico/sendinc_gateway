/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.test.TestUtils;

import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.junit.BeforeClass;
import org.junit.Test;


public class CRLUtilsTest
{
    private static final File testBase = new File("test/resources/testdata");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        InitializeBouncycastle.initialize();
    }
    
    @Test
    public void testGetAllDistributionPointURIs()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/ldap-crl.cer"));
        
        CRLDistPoint distPoint = X509CertificateInspector.getCRLDistibutionPoints(certificate);
        
        assertNotNull(distPoint);
        
        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);
        
        assertEquals(2, uris.size());
    }

    @Test
    public void testGetAllDistributionPointURIsStartWithSpace()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, 
                "certificates/crl-dist-point-starts-with-space.cer"));
        
        CRLDistPoint distPoint = X509CertificateInspector.getCRLDistibutionPoints(certificate);
        
        assertNotNull(distPoint);
        
        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);
        
        assertEquals(1, uris.size());
        assertEquals("http://test-crl.geotrust.com/crls/preprodca2.crl", uris.iterator().next().toString());
    }

    @Test
    public void testGetAllDistributionPointURIsInvalidURIEncoding()
    throws Exception
    {
        // the cert contains a crl dist point with spaces in the url which should have been URL encoded
        X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, 
                "certificates/invalid-uri-encoded-crl-dist-point.cer"));
        
        CRLDistPoint distPoint = X509CertificateInspector.getCRLDistibutionPoints(certificate);
        
        assertNotNull(distPoint);
        
        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);
        
        assertEquals(1, uris.size());
        // check if the URI is now properly encoded
        assertEquals("ldap://directory.d-trust.net/CN=D-TRUST%20Test%20CA%202003%201:PN,O=D-Trust%20GmbH,C=DE", 
                uris.iterator().next().toString());
    }
    
    @Test
    public void testCompare()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);
        
        assertNotNull(crl);

        crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        X509CRL otherCRL = TestUtils.loadX509CRL(crlFile);

        assertTrue(CRLUtils.compare(crl, otherCRL) < 0);
        assertTrue(CRLUtils.compare(otherCRL, crl) > 0);
        assertTrue(CRLUtils.compare(crl, crl) == 0);
    }

    @Test
    public void testIsNewer()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);
        
        assertNotNull(crl);

        crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        X509CRL otherCRL = TestUtils.loadX509CRL(crlFile);

        assertFalse(CRLUtils.isNewer(crl, otherCRL));
        assertTrue(CRLUtils.isNewer(otherCRL, crl));
        assertFalse(CRLUtils.isNewer(crl, crl));
    }    
    
    @Test
    public void testCompareWithCRLNumber()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);
        assertNotNull(crl);

        crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");
        
        X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);
        assertNotNull(newerCRL);
        
        assertTrue(CRLUtils.compare(crl, newerCRL) < 0);
        assertTrue(CRLUtils.compare(newerCRL, crl) > 0);
        assertTrue(CRLUtils.compare(crl, crl) == 0);
        
        assertFalse(CRLUtils.isNewer(crl, newerCRL));
        assertTrue(CRLUtils.isNewer(newerCRL, crl));
        assertFalse(CRLUtils.isNewer(crl, crl));
    }
    
    /*
     * BC147-RC1 seems to fail CRL verification
     */
    @Test
    public void testCRLProblemBC147_RC()
    throws Exception
    {
        // verify CRL with default (JCE) provider
        CertificateFactory jceFac = CertificateFactory.getInstance("X.509");
        
        X509Certificate jceIssuer = (X509Certificate) jceFac.generateCertificate(new FileInputStream(
                new File(testBase, "certificates/ThawteSGCCA.cer")));
        
        X509CRL jceCRL = (X509CRL) jceFac.generateCRL(new FileInputStream(new File(testBase, "crls/ThawteSGCCA.crl")));        
        
        jceCRL.verify(jceIssuer.getPublicKey());        

        
        // verify CRL with BC provider
        CertificateFactory bcFac = CertificateFactory.getInstance("X.509", "BC");
        
        X509Certificate bcIssuer = (X509Certificate) bcFac.generateCertificate(new FileInputStream(
                new File(testBase, "certificates/ThawteSGCCA.cer")));
        
        X509CRL bcCRL = (X509CRL) bcFac.generateCRL(new FileInputStream(new File(testBase, "crls/ThawteSGCCA.crl")));        
        
        bcCRL.verify(bcIssuer.getPublicKey());        
    }    
}
