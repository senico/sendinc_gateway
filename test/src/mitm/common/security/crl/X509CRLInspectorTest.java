/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.util.Enumeration;
import java.util.Set;

import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.util.BigIntegerUtils;
import mitm.test.TestUtils;

import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.junit.Test;

/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
public class X509CRLInspectorTest
{
    
    @Test
    public void testDeltaCRL() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, IOException
    {
        File file = new File("test/resources/testdata/PKITS/crls/deltaCRLCA1deltaCRL.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(file);
       
        assertNotNull(crl);

        assertEquals(1L, X509CRLInspector.getDeltaIndicator(crl).longValue());
    }

    
    @Test
    public void testIssuingDistributionPoint() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, IOException
    {
        File file = new File("test/resources/testdata/PKITS/crls/indirectCRLCA1CRL.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(file);
       
        assertNotNull(crl);

        IssuingDistributionPoint idp = X509CRLInspector.getIssuingDistributionPoint(crl);
        
        assertTrue(idp.isIndirectCRL());
    }

    @Test
    public void testIssuingDistributionPoint2() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, IOException
    {
        File file = new File("test/resources/testdata/PKITS/crls/distributionPoint1CACRL.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(file);
       
        assertNotNull(crl);

        IssuingDistributionPoint idp = X509CRLInspector.getIssuingDistributionPoint(crl);

        assertFalse(idp.isIndirectCRL());
        
        DistributionPointName dp = idp.getDistributionPoint();
        
        GeneralName[] genNames = GeneralNames.getInstance(dp.getName()).getNames();

        assertEquals(1, genNames.length);
        assertEquals(DistributionPointName.FULL_NAME, dp.getType());
    }
    
    @Test
    public void testLargeCRL() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, FileNotFoundException
    {
        //File file = new File("test/resources/testdata/crls/test-root-ca-not-revoked.crl");
        File file = new File("test/resources/testdata/crls/dod-large.crl");
        

        X509CRL crl = null;
        
        final int tries = 5;
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < tries; i++) 
        {
           crl = TestUtils.loadX509CRL(file);

           System.out.println("Issuer: " + crl.getIssuerX500Principal());
           
           assertNull(crl.getRevokedCertificate(new BigInteger("1")));
        }

        double timeSpent = (System.currentTimeMillis() - start) * 0.001 / tries;
        
        System.out.println("Seconds / check: " + timeSpent);

        /* should be faster than 5 seconds on a fast machine */
        assertTrue(timeSpent < 5);
        
        assertNotNull(crl);

        start = System.currentTimeMillis();

        Enumeration<? extends X509CRLEntry> revokedCertificatesEnum = ((OptimizedX509CRLObject) crl).getRevokedCertificatesEnumeration(); 

        int i = 0;
        
        while (revokedCertificatesEnum.hasMoreElements())
        {
            X509CRLEntry crlEntry = revokedCertificatesEnum.nextElement();
            
            assertNotNull(crlEntry);
            
            i++;
        }

        timeSpent = (System.currentTimeMillis() - start) * 0.001;
        
        System.out.println("Stepping through " + i + " entries: " + timeSpent);

        /* should be faster than 2 second on a fast machine */
        assertTrue(timeSpent < 2);
        
        start = System.currentTimeMillis();

        Set<? extends X509CRLEntry> entries = crl.getRevokedCertificates();

        timeSpent = (System.currentTimeMillis() - start) * 0.001;
        
        System.out.println("Seconds to get all entries: " + timeSpent);

        /* should be faster than 15 seconds on a fast machine */
        assertTrue(timeSpent < 15);
        
        assertNotNull(entries);
    }

    @Test
    public void testRevoked() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, FileNotFoundException
    {
        File file = new File("test/resources/testdata/crls/test-ca.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(file);
       
        assertNotNull(crl);
        
        Set<? extends X509CRLEntry> revoked = crl.getRevokedCertificates();
        
        assertEquals(1, revoked.size());        
    }
    
    @Test
    public void testCRLNumber() 
    throws CertificateException, NoSuchProviderException, SecurityFactoryFactoryException, CRLException, IOException
    {
        File file = new File("test/resources/testdata/crls/intel-basic-enterprise-issuing-CA.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(file);
       
        assertNotNull(crl);
        
        BigInteger crlNumber = X509CRLInspector.getCRLNumber(crl);
        
        assertEquals("E3", BigIntegerUtils.hexEncode(crlNumber));
    }
}
