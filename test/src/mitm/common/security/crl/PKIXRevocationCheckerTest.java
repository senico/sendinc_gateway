/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certpath.CertStoreTrustAnchorBuilder;
import mitm.common.security.certpath.CertificatePathBuilder;
import mitm.common.security.certpath.PKIXCertificatePathBuilder;
import mitm.common.security.certpath.SMIMEExtendedKeyUsageCertPathChecker;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.certstore.jce.X509CertStoreParameters;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.common.util.BigIntegerUtils;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PKIXRevocationCheckerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;
    private static CertStore certStore;
    private static X509CertStoreParameters certStoreParams;
    private static X509CertStoreParameters rootStoreParams;
    private static TrustAnchorBuilder trustAnchorBuilder;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create(),
                new X509CRLStoreExtAutoCommitFactory(sessionSource, "certificates").create());
        
        certStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, certStoreParams, "mitm");

        rootStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create());
        
        trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStoreParams.getCertStore(), 0 /* seconds */);        
    }

    @Before
    public void setup() 
    throws Exception 
    {
        certStoreParams.getCertStore().removeAllEntries();
        certStoreParams.getCRLStore().removeAllEntries();

        rootStoreParams.getCertStore().removeAllEntries();
    }
    
    private static void addCRL(File crlFile, X509CRLStoreExt crlStore) 
    throws CRLException
    {
        try {
            X509CRL crl = TestUtils.loadX509CRL(crlFile);
            
            crlStore.addCRL(crl);
        }
        catch(CRLStoreException e) {
            throw new CRLException(e);
        }
        catch (CertificateException e) {
            throw new CRLException(e);
        }
        catch (NoSuchProviderException e) {
            throw new CRLException(e);
        }
        catch (SecurityFactoryFactoryException e) {
            throw new CRLException(e);
        }
        catch (FileNotFoundException e) {
            throw new CRLException(e);
        }
    }
        
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }
    }

    private PKIXCertPathBuilderResult getCertPathBuilderResult(X509CertSelector selector) 
    throws CertPathBuilderException, ParseException
    {
        try {
            CertificatePathBuilder builder = new PKIXCertificatePathBuilder();
            
            builder.setTrustAnchors(trustAnchorBuilder.getTrustAnchors());
            builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
            builder.addCertStore(certStore);
            builder.setRevocationEnabled(false);
            
            Date now = TestUtils.parseDate("24-Mar-2008 16:38:35 GMT");
            
            builder.setDate(now);
            
            CertPathBuilderResult result = builder.buildPath(selector);
            
            assertTrue(result instanceof PKIXCertPathBuilderResult);
            
            return (PKIXCertPathBuilderResult) result;
        }
        catch (CertStoreException e) {
            throw new CertPathBuilderException(e);
        }
    }

    @Test
    public void testNullExtKeyUsage()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/windows-xp-all-roots.p7b"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/windows-xp-all-intermediates.p7b"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/thawte-freemail-valid-till-091108.p7b"), certStoreParams.getCertStore());
        
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("2A0968372BE9522A92577B6F186CBBAA"));
        
        selector.setIssuer("CN=Thawte Personal Freemail Issuing CA, O=Thawte Consulting (Pty) Ltd., C=ZA");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath();
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("24-Mar-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
    }    
    
    @Test
    public void testEndCertRevoked()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath();
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
        assertEquals(RevocationReason.PRIVILEGE_WITHDRAWN, revocationStatus.getReason());
    }    

    @Test
    public void testUnknownMissingEndEntityCRL()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    
    
    @Test
    public void testUnknownMissingIntermediateCRL()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testNotRevoked()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testEndCertRevokedInFuture()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2005 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    
    @Test
    public void testEndCertRevokedCRLOverdue()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("22-Nov-2040 11:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
        assertEquals(RevocationReason.PRIVILEGE_WITHDRAWN, revocationStatus.getReason());
    }    

    @Test
    public void testNotRevokedButCRLOverdue()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2030 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.EXPIRED, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testEndCertRevokedButCertExpiresBeforeCRLValid()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("22-Nov-2028 11:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
        assertEquals(RevocationReason.KEY_COMPROMISE, revocationStatus.getReason());
    }    

    @Test
    public void testEndCertNotRevokedCRLNotYetValid()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("15-Dec-2007 11:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testEndCertNotRevokedCRLNotYetValidButOtherValid()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("15-Dec-2007 11:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testCARevoked()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-root-ca-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
        assertEquals(RevocationReason.CA_COMPROMISE, revocationStatus.getReason());
    }    
    
    @Test
    public void testCARevokedMultipleCRLs()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
        assertEquals(RevocationReason.CA_COMPROMISE, revocationStatus.getReason());
    }    

    @Test
    public void testMissingRootCRL()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }    

    @Test
    public void testIncorrectCACRL()
    throws Exception 
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        // add crls
        addCRL(new File(testBase, "crls/test-ca-signed-incorrect-key.crl"), certStoreParams.getCRLStore());
        addCRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                
        X509CertSelector selector = new X509CertSelector();
        
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
        selector.setIssuer("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL");
        
        PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);
        
        CertPath certPath = result.getCertPath(); 
        
        TrustAnchor trustAnchor = result.getTrustAnchor();
        
        assertNotNull(trustAnchor);
        assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL", 
                trustAnchor.getTrustedCert().getSubjectX500Principal().toString());
        
        PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());
        
        Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
        
        RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);
        
        assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
        assertEquals(null, revocationStatus.getReason());
    }
}
