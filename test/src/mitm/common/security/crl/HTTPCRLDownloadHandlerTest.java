/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.util.Collection;

import mitm.common.net.CipherSuiteOverrideSSLProtocolSocketFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.util.FileConstants;
import mitm.common.util.SizeLimitedInputStream;
import mitm.test.SimpleSocketServer;
import mitm.test.SocketAcceptEvent;
import mitm.test.TestUtils;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HTTPCRLDownloadHandlerTest
{
    private final static int SERVER_PORT = 61113;
    
    private static SimpleSocketServer server;
    
    private static class RunawaySocketHandler implements Runnable
    {
        private final Socket socket;
        
        public RunawaySocketHandler(Socket socket) {
            this.socket = socket; 
        }
        
        @Override
        public void run() 
        {
            try {
                while (true) {
                    OutputStream output = socket.getOutputStream();
    
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e) {
                    }
                    
                    output.write(new byte[]{0});
                    output.flush();
                }

            }
            catch (IOException e) {
                e.printStackTrace();
            }
            
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        CipherSuiteOverrideSSLProtocolSocketFactory.register(
                "TLS_RSA_WITH_AES_128_CBC_SHA," +
                "TLS_RSA_WITH_AES_256_CBC_SHA," +
                "SSL_RSA_WITH_3DES_EDE_CBC_SHA," +
                "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA," +
                "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA," +
                "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA," +
                "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA," +
                "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA," +
                "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA," +
                "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA," +
                "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA," +
                "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA," +
                "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA," +
                "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA," +
                "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA");
        
        org.apache.log4j.Logger logger = LogManager.getLogger(CipherSuiteOverrideSSLProtocolSocketFactory.class);
        
        if (logger != null) {
            logger.setLevel(org.apache.log4j.Level.DEBUG);
        }
        
        InitializeBouncycastle.initialize();
        
        startServer();
    }

    @AfterClass
    public static void setUpAfterClass() 
    {
        server.stop();
    }
    
    private static int tempFileCount;

    @Before
    public void before() {
        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");
    }

    @After
    public void after()
    throws IOException
    {
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }
    
    private static void startServer()
    {        
        server = new SimpleSocketServer(SERVER_PORT);

        Thread serverThread = new Thread(server);
        serverThread.setDaemon(true);
        serverThread.start();

        while(!server.isRunning()) {
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                // ignore
            }
        }        
    }
    
    @Test
    public void testHTTPSCRLDownloadHandler() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        URI uri = new URI("https://www.cacert.org/revoke.crl");

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(1, crls.size());
    }

    @Test
    public void testHTTPSCRLDownloadHandlerLowThreshold() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 10 * 1024 * 1024);
        
        HTTPCRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        handler.setMemThreshold(0);
        
        URI uri = new URI("https://www.cacert.org/revoke.crl");

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(1, crls.size());
    }
    
    @Test(timeout=10000)
    public void testHTTPCRLDownloadHandlerTimeout() 
    throws URISyntaxException, CRLException, IOException
    {
        SocketAcceptEvent event = new SocketAcceptEvent()
        {
            @Override
            public void accept(Socket socket)
            throws IOException 
            {
                Runnable runnable = new RunawaySocketHandler(socket);
                
                new Thread(runnable).start();
            }            
        };

        server.setIncomingEvent(event);
        
        CRLDownloadParameters params = new CRLDownloadParameters(4000, 60000, 60000, 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);

        URI uri = new URI("http://127.0.0.1:" + SERVER_PORT);

        try {
            handler.downloadCRLs(uri);
            
            fail();
        }
        catch(IOException e) {
        }
    }    
    
    @Test
    public void testHTTPCRLDownloadHandlerConnectTimeout() 
    throws URISyntaxException, CRLException, IOException
    {
        SocketAcceptEvent event = new SocketAcceptEvent()
        {
            @Override
            public void accept(Socket socket) {
            }            
        };
        
        server.setIncomingEvent(event);
        
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 1000, 1000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);

        URI uri = new URI("http://127.0.0.1:" + SERVER_PORT);

        try {
            handler.downloadCRLs(uri);
            
            fail();
        }
        catch(IOException e) {
            assertEquals("Read timed out", e.getMessage());
        }
    }    
    
    @Test
    public void testHTTPCRLDownloadHandler() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        URI uri = new URI("http://crl.thawte.com/ThawteSGCCA.crl");

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(1, crls.size());
    }

    @Test
    public void testHTTPCRLDownloadHandlerSizeLimit() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 100);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        URI uri = new URI("http://crl.thawte.com/ThawteSGCCA.crl");

        try {
            handler.downloadCRLs(uri);
            
            fail();
        }
        catch(IOException e) {
            assertEquals(SizeLimitedInputStream.MAXIMUM_REACHED, e.getMessage());
        }
    }
    
    @Test(expected=UnknownHostException.class)
    public void testHTTPCRLDownloadHandlerUnknownURL() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        URI uri = new URI("http://dsfljdsjflsdjflsdjflsdjklsjljflsdj");

        handler.downloadCRLs(uri);
    }
    
    @Test
    public void testHTTPCRLDownloadHandlerNoCRL() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(60000, 60000, 60000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new HTTPCRLDownloadHandler(params);
        
        URI uri = new URI("http://www.google.com");

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(0, crls.size());
    }   
}
