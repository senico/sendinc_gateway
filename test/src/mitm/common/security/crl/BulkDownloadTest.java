/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Extension;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.X509ExtensionInspector;
import mitm.common.util.CollectionUtils;
import mitm.common.util.URIUtils;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BulkDownloadTest
{
    private final static Logger logger = LoggerFactory.getLogger(BulkDownloadTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
        
        // set loglevel of URIUtils to DEBUG so we can see which URIs are invalid
        LogManager.getLogger(URIUtils.class).setLevel(Level.DEBUG);
    }
     
    private Set<URI> getAllDistributionPointURIs(Collection<?> elements)
    throws CRLException 
    {
        Set<URI> uris = new HashSet<URI>();
        
        for (Object element : elements)
        {
            if (!(element instanceof X509Extension)) {
                continue;
            }
            
            try {
                X509Extension extension = (X509Extension) element;
                
                CRLDistPoint crlDistPoint = X509ExtensionInspector.getCRLDistibutionPoints(extension);
    
                Set<URI> fromDisPoint = CRLUtils.getAllDistributionPointURIs(crlDistPoint);
                
                uris.addAll(fromDisPoint);
    
                crlDistPoint = X509ExtensionInspector.getFreshestCRL(extension);
    
                if (crlDistPoint != null) {
                    logger.info("getFreshestCRL distribution point found.");                    
                }
                
                fromDisPoint = CRLUtils.getAllDistributionPointURIs(crlDistPoint);
                
                if (fromDisPoint.size() > 0) {
                    logger.info("Delta CRLs found.");
                }
                
                uris.addAll(fromDisPoint);
            }
            catch(IOException e) {
                logger.error("Error getting distribution points.", e);
            }
        }
        
        return uris;
    }
    
    private Collection<X509CRL> downloadCRLs(Set<URI> uris, CRLDownloader downloader)
    {
        Collection<X509CRL> crls = new LinkedList<X509CRL>();
        
        for (URI uri : uris)
        {
            if (uri.getScheme() == null) {                
                logger.warn("No scheme. " + uri.toString());
            }
            logger.info(uri.toString());
            
            try {
                Collection<? extends CRL> downloadedCrls = downloader.downloadCRLs(uri);
                
                CollectionUtils.copyCollectionFiltered(downloadedCrls, crls, X509CRL.class);
            }
            catch (CRLException e) {
                logger.error("Error reading CRL.", e);
            }
            catch (IOException e) {
                logger.error("IO Exception.", e);
            }
        }

        return crls;
    }
    
    @Test
    public void testBulk() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/windows-xp-all-intermediates.p7b");
        
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        Set<URI> uris = getAllDistributionPointURIs(certificates);

        file = new File("test/resources/testdata/certificates/windows-xp-all-roots.p7b");
        
        certificates = CertificateUtils.readCertificates(file);
        
        uris.addAll(getAllDistributionPointURIs(certificates));

        CRLDownloadParameters params = new CRLDownloadParameters(10 * 60000, 10000, 10000, 10 * 1024 * 1024);
        
        CRLDownloader downloader = new CRLDownloaderImpl(params);
        
        Collection<X509CRL> crls = downloadCRLs(uris, downloader);

        logger.info(crls.size() + " CRLS downloaded");
        
        Set<URI> crlURIs = getAllDistributionPointURIs(crls);
        
        Collection<X509CRL> newCRLs = downloadCRLs(crlURIs, downloader);
        
        logger.info(newCRLs.size() + " new CRLS downloaded");
    }
}
