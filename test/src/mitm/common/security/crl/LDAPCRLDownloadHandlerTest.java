/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.util.Collection;

import mitm.test.SimpleSocketServer;
import mitm.test.SocketAcceptEvent;

import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class LDAPCRLDownloadHandlerTest
{
    private final static int SERVER_PORT = 61111;
    
    private static SimpleSocketServer server;
    
    private static class RunawaySocketHandler implements Runnable
    {
        @Override
        public void run() 
        {
            try {
                Thread.sleep(Integer.MAX_VALUE);
            }
            catch (InterruptedException e) {
            }
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
        
        startServer();
    }

    @AfterClass
    public static void setUpAfterClass() 
    {
        server.stop();
    }
    
    private static void startServer()
    {                
        server = new SimpleSocketServer(SERVER_PORT);

        Thread serverThread = new Thread(server);
        serverThread.start();

        while(!server.isRunning()) {
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                // ignore
            }
        }        
    }
    
    @Test(expected = IOException.class)
    public void testLDAPCRLDownloadHandlerTimeoutTest() 
    throws URISyntaxException, CRLException, IOException
    {
        SocketAcceptEvent event = new SocketAcceptEvent()
        {
            @Override
            public void accept(Socket socket)
            throws IOException 
            {
                Runnable runnable = new RunawaySocketHandler();
                new Thread(runnable).start();
            }            
        };

        server.setIncomingEvent(event);
        
        CRLDownloadParameters params = new CRLDownloadParameters(1000, 10000, 10000, 10 * 1024 * 1024);
        
        CRLDownloadHandler handler = new LDAPCRLDownloadHandler(params);

        URI uri = new URI("ldap://127.0.0.1:" + SERVER_PORT);
        
        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(0, crls.size());
    }
    
    /*
     * Note: This test sometimes fails because of connection problems
     */
    @Test
    public void testLDAPCRLDownloadHandler() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(10000, 10000, 10000, 10 * 1024 * 1024);

        CRLDownloadHandler handler = new LDAPCRLDownloadHandler(params);

        URI uri = new URI("ldap://ds-3.c3pki.chamb.disa.mil/cn%3dDoD%20CLASS%203%20Root%20CA%2cou%3dPKI%2cou" + 
                "%3dDoD%2co%3dU.S.%20Government%2cc%3dUS?certificaterevocationlist;binary");
        
        // alternative
        //URI uri = new URI("ldap://directory.d-trust.net/CN=D-TRUST%20Test%20CA%202003%201:PN,O=D-Trust%20GmbH,C=DE");
            
        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(1, crls.size());
    }

    @Test(expected = IOException.class)
    public void testLDAPCRLDownloadHandlerNoMatch() 
    throws URISyntaxException, CRLException, IOException
    {
        CRLDownloadParameters params = new CRLDownloadParameters(10000, 10000, 10000, 10 * 1024 * 1024);

        CRLDownloadHandler handler = new LDAPCRLDownloadHandler(params);

        URI uri = new URI("ldap:///CN=ukspkca01,CN=AIA,CN=Public%20Key%20Services,CN=Services,CN=Configuration," + 
            "DC=hds,DC=com?cACertificate?base?objectClass=certificationAuthority");
        
        handler.downloadCRLs(uri);
    }    
}
