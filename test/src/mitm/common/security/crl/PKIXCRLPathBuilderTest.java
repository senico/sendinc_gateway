/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certpath.CertStoreTrustAnchorBuilder;
import mitm.common.security.certpath.CertificatePathBuilder;
import mitm.common.security.certpath.PKIXCertificatePathBuilder;
import mitm.common.security.certpath.SMIMEExtendedKeyUsageCertPathChecker;
import mitm.common.security.certpath.TrustAnchorBuilder;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.certstore.jce.X509CertStoreParameters;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PKIXCRLPathBuilderTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;
    private static CertStore certStore;
    private static X509CertStoreParameters certStoreParams;
    private static X509CertStoreParameters rootStoreParams;
    private static TrustAnchorBuilder trustAnchorBuilder;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create(),
                new X509CRLStoreExtAutoCommitFactory(sessionSource, "certificates").create());
        
        certStore = CertStore.getInstance(MITMProvider.DATABASE_CERTSTORE, certStoreParams, "mitm");

        rootStoreParams = new X509CertStoreParameters(new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create());
        
        trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStoreParams.getCertStore(), 0 /* seconds */);        
    }

    @Before
    public void setup() 
    throws Exception
    {
        certStoreParams.getCertStore().removeAllEntries();
        certStoreParams.getCRLStore().removeAllEntries();

        rootStoreParams.getCertStore().removeAllEntries();
    }
            
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, CertStoreException, IOException
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }
    }

    private CertificatePathBuilder getCertPathBuilder() 
    throws NoSuchProviderException, NoSuchAlgorithmException, CertStoreException, CertPathBuilderException, 
    InvalidAlgorithmParameterException
    {
        CertificatePathBuilder builder = new PKIXCertificatePathBuilder();
        
        builder.setTrustAnchors(trustAnchorBuilder.getTrustAnchors());
        builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
        builder.addCertStore(certStore);
        builder.setRevocationEnabled(false);
        
        return builder;
    }

    @Test
    public void testBuildCRLPath()
    throws Exception
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        X509CRL crl = TestUtils.loadX509CRL(new File(testBase, "crls/test-ca.crl"));
        
        CertificatePathBuilder certPathBuilder = getCertPathBuilder();
        
        PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);
        
        CertPathBuilderResult result = crlPathBuilder.buildPath(crl);
        
        assertTrue(result instanceof PKIXCertPathBuilderResult);
        
        PKIXCertPathBuilderResult pkixResult = (PKIXCertPathBuilderResult) result;
        
        CertPath path = pkixResult.getCertPath();
        TrustAnchor trustAnchor = pkixResult.getTrustAnchor();
        
        assertNotNull(path);
        List<? extends Certificate> certificates = path.getCertificates();
        // should only contain the CA cert
        assertEquals(1, certificates.size());
        assertEquals("115FCAD6B536FD8D49E72922CD1F0DA", X509CertificateInspector.getSerialNumberHex(
                (X509Certificate) certificates.get(0)));
        
        assertNotNull(trustAnchor);
        assertEquals("115FCAC409FB2022B7D06920A00FE42", X509CertificateInspector.getSerialNumberHex(
                trustAnchor.getTrustedCert()));
    }
    
    @Test
    public void testBuildCRLPathTrustAnchor()
    throws Exception
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        X509CRL crl = TestUtils.loadX509CRL(new File(testBase, "crls/test-root-ca-not-revoked.crl"));
        
        CertificatePathBuilder certPathBuilder = getCertPathBuilder();
        
        PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);
        
        CertPathBuilderResult result = crlPathBuilder.buildPath(crl);
        
        assertTrue(result instanceof PKIXCertPathBuilderResult);
        
        PKIXCertPathBuilderResult pkixResult = (PKIXCertPathBuilderResult) result;
        
        CertPath path = pkixResult.getCertPath();
        TrustAnchor trustAnchor = pkixResult.getTrustAnchor();
        
        assertNotNull(path);
        List<? extends Certificate> certificates = path.getCertificates();
        assertEquals(0, certificates.size());
        
        assertNotNull(trustAnchor);
        assertEquals("115FCAC409FB2022B7D06920A00FE42", X509CertificateInspector.getSerialNumberHex(
                trustAnchor.getTrustedCert()));
    }

    @Test
    public void testBuildCRLPathNoPath()
    throws Exception
    {
        // add root
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());
        
        // add certificates
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStoreParams.getCertStore());
        
        X509CRL crl = TestUtils.loadX509CRL(new File(testBase, "crls/itrus.com.cn.crl"));
        
        CertificatePathBuilder certPathBuilder = getCertPathBuilder();
        
        PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);
        
        try {
            crlPathBuilder.buildPath(crl);
            
            fail();
        }
        catch(CertPathBuilderException e) {
            assertEquals(PKIXCRLPathBuilder.CRL_ISSUER_NOT_FOUND, e.getMessage());
        }
    }
    
    @Test
    public void testBuildCRLPathNoRootShouldNotBeValid()
    throws Exception
    {
        // add certificates
        addCertificates(new File(testBase, "certificates/user-trust-intermediate.cer"), certStoreParams.getCertStore());
        
        X509CRL crl = TestUtils.loadX509CRL(new File(testBase, "crls/user-trust-intermediate.crl"));
        
        CertificatePathBuilder certPathBuilder = getCertPathBuilder();
        
        PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);
        
        try {
            crlPathBuilder.buildPath(crl);
            
            fail();
        }
        catch(CertPathBuilderException e) {
            assertEquals(PKIXCertificatePathBuilder.NO_ROOTS_ERROR_MESSAGE, e.getMessage());
        }
    }
}
