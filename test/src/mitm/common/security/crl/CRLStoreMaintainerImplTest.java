/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CRLStoreMaintainerImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;

    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    
    private static PKISecurityServices pKISecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        rootStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create();
        crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, "");
        
        pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
                rootStore, crlStore);
    }
    
    @Before
    public void setup() 
    throws Exception 
    {
        certStore.removeAllEntries();
        rootStore.removeAllEntries();
        crlStore.removeAllEntries();
    }
    
    private static X509CRL addCRL(File crlFile, X509CRLStoreExt crlStore) 
    throws CRLException
    {
        try {
            X509CRL crl = TestUtils.loadX509CRL(crlFile);
            
            crlStore.addCRL(crl);
            
            return crl;
        }
        catch(CRLStoreException e) {
            throw new CRLException(e);
        }
        catch (CertificateException e) {
            throw new CRLException(e);
        }
        catch (NoSuchProviderException e) {
            throw new CRLException(e);
        }
        catch (SecurityFactoryFactoryException e) {
            throw new CRLException(e);
        }
        catch (FileNotFoundException e) {
            throw new CRLException(e);
        }
    }

    /*
     * Test that adds two CRLS having the same issuer subject but the CRLs are issued
     * by two different CRLs having similar subjects. The new CRL should not overwrite
     * the 'old' CRL
     */
    @Test
    public void testAddSameSubjectNotSameIssuer()
    throws Exception
    {
        File caFile = new File(testBase, "certificates/ca-same-subject-1.cer");

        X509Certificate ca1 = TestUtils.loadCertificate(caFile);

        caFile = new File(testBase, "certificates/root-same-subject-1.cer");
        
        X509Certificate root1 = TestUtils.loadCertificate(caFile);

        ca1.verify(root1.getPublicKey());
        
        caFile = new File(testBase, "certificates/ca-same-subject-2.cer");

        X509Certificate ca2 = TestUtils.loadCertificate(caFile);

        caFile = new File(testBase, "certificates/root-same-subject-2.cer");
        
        X509Certificate root2 = TestUtils.loadCertificate(caFile);

        ca2.verify(root2.getPublicKey());
        
        assertFalse(ca1.equals(ca2));
        
        File crlFile = new File(testBase, "crls/crl-ca-same-subject-1.crl");
        
        X509CRL crl1 = TestUtils.loadX509CRL(crlFile);

        crl1.verify(ca1.getPublicKey());
        
        try {
            crl1.verify(ca2.getPublicKey());
            
            fail();
        }
        catch(SignatureException e) {
            // expected
        }

        certStore.addCertificate(ca1);
        certStore.addCertificate(ca2);

        rootStore.addCertificate(root1);
        rootStore.addCertificate(root2);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory());

        crlStoreMaintainer.addCRL(crl1);

        assertTrue(crlStore.contains(crl1));

        assertEquals(1, crlStore.getCRLs(null).size());

        crlFile = new File(testBase, "crls/crl-ca-same-subject-2.crl");
        
        X509CRL crl2 = TestUtils.loadX509CRL(crlFile);

        crl2.verify(ca2.getPublicKey());

        try {
            crl2.verify(ca1.getPublicKey());
            
            fail();
        }
        catch(SignatureException e) {
            // expected
        }
        
        assertFalse(crl1.equals(crl2));
        
        crlStoreMaintainer.addCRL(crl2);

        assertTrue(crlStore.contains(crl2));

        assertEquals(2, crlStore.getCRLs(null).size());
    }
    
    
    @Test
    public void testCRLNotTrusted()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/crl-ca-same-subject-1.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory());

        crlStoreMaintainer.addCRL(crl);

        assertEquals(0, crlStore.getCRLs(null).size());

    }
    @Test
    public void testAddDuplicate()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);

        crlStoreMaintainer.addCRL(crl);
        
        assertTrue(crlStore.contains(crl));

        assertEquals(1, crlStore.getCRLs(null).size());
        
        crlStoreMaintainer.addCRL(crl);
        
        assertTrue(crlStore.contains(crl));

        assertEquals(1, crlStore.getCRLs(null).size());
    }
        
    @Test
    public void testUpdateNewerFullCRLNoCRLNumber()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        // load a newer CRL that replaces the other CRL
        crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(newerCRL);
        
        assertFalse(crlStore.contains(crl));

        assertTrue(crlStore.contains(newerCRL));
    }

    @Test
    public void testUpdateOlderFullCRLNoCRLNumber()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        // load an older CRL that should not replace the other CRL
        crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(olderCRL);
        
        assertTrue(crlStore.contains(crl));

        assertFalse(crlStore.contains(olderCRL));
    }
    
    @Test
    public void testUpdateNewerFullCRLCRLNumber()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        // load a newer CRL that replaces the other CRL
        crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");
        
        X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(newerCRL);
        
        assertFalse(crlStore.contains(crl));

        assertTrue(crlStore.contains(newerCRL));
    }

    @Test
    public void testUpdateOlderFullCRLCRLNumber()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        // load an older CRL that should not replace the other CRL
        crlFile = new File(testBase, "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");
        
        X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(olderCRL);
        
        assertTrue(crlStore.contains(crl));

        assertFalse(crlStore.contains(olderCRL));
    }
    
    @Test
    public void testUpdateDifferentReasons()
    throws Exception
    {
        File crlFile = new File(testBase, "PKITS/crls/onlySomeReasonsCA1compromiseCRL.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        crlFile = new File(testBase, "PKITS/crls/onlySomeReasonsCA1otherreasonsCRL.crl");
        
        X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(newerCRL);

        // they must both be in the store because they have a different scope
        assertTrue(crlStore.contains(crl));
        assertTrue(crlStore.contains(newerCRL));
    }

    @Test
    public void testUpdateDeltaAndBase()
    throws Exception
    {
        // first add delta
        File crlFile = new File(testBase, "PKITS/crls/deltaCRLCA1deltaCRL.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        crlFile = new File(testBase, "PKITS/crls/deltaCRLCA1CRL.crl");
        
        // now add the older base 
        X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(olderCRL);

        // they must both be in the store because one is the base and the other the delta
        assertTrue(crlStore.contains(crl));
        assertTrue(crlStore.contains(olderCRL));
    }

    @Test
    public void testUpdateBaseAndDelta()
    throws Exception
    {
        // add base
        File crlFile = new File(testBase, "PKITS/crls/deltaCRLCA1CRL.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        // add delta
        crlFile = new File(testBase, "PKITS/crls/deltaCRLCA1deltaCRL.crl");
        
        X509CRL deltaCRL = TestUtils.loadX509CRL(crlFile);
        
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore, pKISecurityServices.getCRLPathBuilderFactory(), false);
        
        crlStoreMaintainer.addCRL(deltaCRL);

        // they must both be in the store because one is the base and the other the delta
        assertTrue(crlStore.contains(crl));
        assertTrue(crlStore.contains(deltaCRL));
    }
}
