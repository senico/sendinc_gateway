/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Set;

import mitm.common.security.certificate.X509CertificateInspector;
import mitm.test.TestUtils;

import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.junit.Test;

public class CRLDistributionPointsInspectorTest
{
    private static final File testBase = new File("test/resources/testdata");

    @Test(expected = CRLException.class)
    public void testInvalidCRLDistributionPoint() 
    throws Exception
    {
       X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, 
               "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"));
       
       CRLDistributionPointsInspector inspector = new CRLDistributionPointsInspector(
               X509CertificateInspector.getCRLDistibutionPoints(certificate));
       
       inspector.getURIDistributionPointNames();
    }
    
    @Test
    public void testLoadDistributionPointsHTTP() 
    throws Exception
    {
       X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/gmail-ssl.cer"));
       
       CRLDistPoint distPoint = X509CertificateInspector.getCRLDistibutionPoints(certificate);
       
       assertNotNull(distPoint);
       
       Set<String> uris = CRLDistributionPointsInspector.getURIDistributionPointNames(distPoint);
       
       assertNotNull(uris);
       assertEquals(1, uris.size());
       assertEquals("http://crl.thawte.com/ThawteSGCCA.crl", uris.toArray()[0].toString());
    }

    @Test
    public void testLoadDistributionPointsMultiple() 
    throws Exception
    {
       X509Certificate certificate = TestUtils.loadCertificate(new File(testBase, "certificates/ldap-crl.cer"));
       
       CRLDistPoint distPoint = X509CertificateInspector.getCRLDistibutionPoints(certificate);
       
       assertNotNull(distPoint);
       
       Set<String> uris = CRLDistributionPointsInspector.getURIDistributionPointNames(distPoint);
       
       assertNotNull(uris);
       
       assertEquals(2, uris.size());

       assertTrue(uris.contains("ldap:///CN=ukspkca01,CN=UKSPKCA01,CN=CDP,CN=Public%20Key%20Services," + 
           "CN=Services,CN=Configuration,DC=hds,DC=com?certificateRevocationList?base?" + 
           "objectClass=cRLDistributionPoint"));
       
       assertTrue(uris.contains("http://ukspkca01.corp.hds.com/CertEnroll/ukspkca01.crl"));
    }

    @Test
    public void testFreshestCRL() 
    throws Exception
    {
       X509CRL crl = TestUtils.loadX509CRL(new File(testBase, "PKITS/crls/deltaCRLCA1CRL.crl"));
       
       CRLDistPoint distPoint = X509CRLInspector.getFreshestCRL(crl);
       
       assertNotNull(distPoint);
       
       Set<String> uris = CRLDistributionPointsInspector.getURIDistributionPointNames(distPoint);
       
       assertNotNull(uris);
       
       assertEquals(0, uris.size());       
    }
}
