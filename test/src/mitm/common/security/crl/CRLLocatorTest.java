package mitm.common.security.crl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.List;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.SecurityFactoryFactoryException;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class CRLLocatorTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata");
    
    private static HibernateSessionSource sessionSource;
    private static X509CRLStoreExt crlStore;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "certificates").create();
    }

    @Before
    public void setup() 
    throws Exception 
    {
        crlStore.removeAllEntries();
    }
    
    private static X509CRL addCRL(File crlFile, X509CRLStoreExt crlStore) 
    throws CRLException
    {
        try {
            X509CRL crl = TestUtils.loadX509CRL(crlFile);
            
            crlStore.addCRL(crl);
            
            return crl;
        }
        catch(CRLStoreException e) {
            throw new CRLException(e);
        }
        catch (CertificateException e) {
            throw new CRLException(e);
        }
        catch (NoSuchProviderException e) {
            throw new CRLException(e);
        }
        catch (SecurityFactoryFactoryException e) {
            throw new CRLException(e);
        }
        catch (FileNotFoundException e) {
            throw new CRLException(e);
        }
    }
    
    @Test
    public void testMultipleCRLs()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        X509CRL crl = addCRL(crlFile, crlStore);
        
        assertTrue(crlStore.contains(crl));

        crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        X509CRL otherCRL = addCRL(crlFile, crlStore);

        CRLLocator locator = new CRLLocator(crlStore);
        
        X509Certificate issuer = TestUtils.loadCertificate(new File(testBase, "certificates/ThawteSGCCA.cer"));
        
        List<X509CRL> crls = locator.findCRLs(issuer);
        
        assertEquals(2, crls.size());
        
        assertTrue(crls.contains(crl));
        assertTrue(crls.contains(otherCRL));
    }

    @Test
    public void testNoCRLsFound()
    throws Exception
    {
        File crlFile = new File(testBase, "crls/ThawteSGCCA.crl");
        
        addCRL(crlFile, crlStore);

        crlFile = new File(testBase, "crls/ThawteSGCCA-thisupdate-211207.crl");
        
        addCRL(crlFile, crlStore);

        CRLLocator locator = new CRLLocator(crlStore);
        
        X509Certificate issuer = TestUtils.loadCertificate(new File(testBase, "certificates/testcertificate.cer"));
        
        List<X509CRL> crls = locator.findCRLs(issuer);
        
        assertEquals(0, crls.size());
    }
    
    @Test
    public void testEmptyStore()
    throws Exception
    {
        CRLLocator locator = new CRLLocator(crlStore);
        
        X509Certificate issuer = TestUtils.loadCertificate(new File(testBase, "certificates/ThawteSGCCA.cer"));
        
        List<X509CRL> crls = locator.findCRLs(issuer);
        
        assertEquals(0, crls.size());
    }        
}
