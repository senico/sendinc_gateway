/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.asn1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;

import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.util.HexUtils;
import mitm.test.TestUtils;

import org.junit.Test;

public class DERUtilsTest
{
    @Test
    public void testGetOctets() 
    throws CertificateException, NoSuchProviderException, IOException, NoSuchAlgorithmException 
    {
        byte[] encoded = new byte[] {4, 20, -100, -126, -34, 82, -108, 71, -104, 51, 123, 5, 3, 106, 24, 125, -19, 65, 99, 46, -5, -1};
        
        byte[] decoded = DERUtils.getOctets(encoded);
        
        String hex = HexUtils.hexEncode(decoded);
        
        assertEquals("9C82DE52944798337B05036A187DED41632EFBFF", hex);
    }
    
    @Test
    public void testGetOctetsCertificate() 
    throws CertificateException, NoSuchProviderException, IOException, NoSuchAlgorithmException 
    {
        File file = new File("test/resources/testdata/certificates/intel-corp-basic.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);
        
        byte[] subjectKeyId = X509CertificateInspector.getSubjectKeyIdentifier(certificate);
        
        X509CertSelector selector = new X509CertSelector();
        
        // X509CertSelector expects a DER encoded subjectKeyIdentifier
        selector.setSubjectKeyIdentifier(DERUtils.toDEREncodedOctetString(subjectKeyId));

        assertTrue(selector.match(certificate));
    }
}
