/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.List;

import mitm.common.locale.CharacterEncoding;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVWriter;


public class CSVRequestConverterTest
{
    @Test
    public void testDefaultValues()
    throws Exception
    {
        String input = "email, cn\r\n" +
            "test1@example.com,cn1\r\n";
        
        CSVRequestConverter converter = new CSVRequestConverter();
                
        List<RequestParameters> parameters = converter.convertCSV(IOUtils.toInputStream(input, 
                CharacterEncoding.US_ASCII));
        
        assertNotNull(parameters);
        assertEquals(1, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("test1@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test1@example.com, CN=cn1", request.getSubject().toString());
        assertEquals(0, request.getKeyLength());
        assertEquals(0, request.getValidity());
        assertNull(request.getCRLDistributionPoint());
        assertNull(request.getSignatureAlgorithm());
        assertNull(request.getCertificateRequestHandler());
    }
    
    @Test(expected = RequestConverterException.class)
    public void testDuplicateEmails()
    throws Exception
    {
        String input = "email, organisation, COMMONNAME, firstname, lastname\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test
    public void testAllowDuplicateEmails()
    throws Exception
    {
        String input = "email, organisation, COMMONNAME, firstname, lastname\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.setAllowDuplicates(true);
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }
    
    @Test
    public void testEmptyLine()
    throws Exception
    {
        String input = "email, organisation, COMMONNAME, firstname, lastname\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1\r\n\r\n" +
            "test2@example.com,org2,cn2,firstname2,lastname2";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        List<RequestParameters> parameters = converter.convertCSV(IOUtils.toInputStream(input, 
                CharacterEncoding.US_ASCII));
        
        assertNotNull(parameters);
        assertEquals(2, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("test1@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test1@example.com, GIVENNAME=firstname1, SURNAME=lastname1, CN=cn1, O=org", 
                request.getSubject().toString());

        request = parameters.get(1);
        
        assertEquals("test2@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test2@example.com, GIVENNAME=firstname2, SURNAME=lastname2, CN=cn2, O=org2", 
                request.getSubject().toString());
    }    
    
    @Test(expected = RequestConverterException.class)
    public void testExceedMaxLines()
    throws Exception
    {
        String input = "email, organisation, COMMONNAME, firstname, lastname\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1\r\n" +
            "test2@example.com,org,  cn1  , firstname1, lastname1\r\n" +
            "test3@example.com,org2,cn2,firstname2,lastname2";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.setMaxLines(2);
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }
    
    @Test
    public void testUnicodeBOM()
    throws Exception
    {
        String input = "e, cn\r\ntest1@example.com,刘书洪";
        
        CSVRequestConverter converter = new CSVRequestConverter();
     
        byte[] bom = new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
        
        byte[] withBOM = ArrayUtils.addAll(bom, input.getBytes(CharacterEncoding.UTF_8));
        
        List<RequestParameters> parameters = converter.convertCSV(new ByteArrayInputStream(withBOM));
        
        assertNotNull(parameters);
        assertEquals(1, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("EMAILADDRESS=test1@example.com, CN=刘书洪", request.getSubject().toString());
    }
    
    @Test
    public void testUnicodeNoBOM()
    throws Exception
    {
        String input = "e, cn\r\ntest1@example.com,刘书洪";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        List<RequestParameters> parameters = converter.convertCSV(new ByteArrayInputStream(input.getBytes( 
                CharacterEncoding.UTF_8)));
        
        assertNotNull(parameters);
        assertEquals(1, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("EMAILADDRESS=test1@example.com, CN=���������", request.getSubject().toString());
    }
    
    @Test(expected = RequestConverterException.class)
    public void testDuplicateColumn()
    throws Exception
    {
        String input = "e, cn, cn\r\ntest1@example.com,aaa";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testMissingValues()
    throws Exception
    {
        String input = "e, cn, fn\r\ntest1@example.com,aaa";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testEmptyCNValue()
    throws Exception
    {
        String input = "EMail, cn\r\ntest1@example.com,";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testInvalidEmail()
    throws Exception
    {
        String input = "email, cn\r\nexample.com,aaa";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testCNColumnMissing()
    throws Exception
    {
        String input = "EMail, fn\r\ntest1@example.com,aaa";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testEmailColumnMissing()
    throws Exception
    {
        String input = "cn, fn\r\ntest1@example.com,cn1";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testUnknownColumn()
    throws Exception
    {
        String input = "E, X, fn\r\ntest1@example.com,cn1";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test(expected = RequestConverterException.class)
    public void testMaxLineLength()
    throws Exception
    {
        String input = "E, cn, fn\r\n" +
            "test1@example.com,cn1,\r\n" +
            "test2@example.com,cn2,";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.setMaxValueLength(2);
        
        converter.convertCSV(IOUtils.toInputStream(input, CharacterEncoding.US_ASCII));
    }

    @Test
    public void testConvertEmptyFields()
    throws Exception
    {
        String input = "E, cn, fn\r\n" +
            "test1@example.com,cn1,\r\n" +
            "test2@example.com,cn2,";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        List<RequestParameters> parameters = converter.convertCSV(IOUtils.toInputStream(input, 
                CharacterEncoding.US_ASCII));
        
        assertNotNull(parameters);
        assertEquals(2, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("test1@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test1@example.com, GIVENNAME=, CN=cn1", request.getSubject().toString());

        request = parameters.get(1);
        
        assertEquals("test2@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test2@example.com, GIVENNAME=, CN=cn2", request.getSubject().toString());
    }

    @Test
    public void testConvertUSASCII()
    throws Exception
    {
        String input = "email, organisation, COMMONNAME, firstname, lastname\r\n" +
            "test1@example.com,org,  cn1  , firstname1, lastname1\r\n" +
            "test2@example.com,org2,cn2,firstname2,lastname2";
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        List<RequestParameters> parameters = converter.convertCSV(IOUtils.toInputStream(input, 
                CharacterEncoding.US_ASCII));
        
        assertNotNull(parameters);
        assertEquals(2, parameters.size());
        
        RequestParameters request = parameters.get(0);
        
        assertEquals("test1@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test1@example.com, GIVENNAME=firstname1, SURNAME=lastname1, CN=cn1, O=org", 
                request.getSubject().toString());

        request = parameters.get(1);
        
        assertEquals("test2@example.com", request.getEmail());
        assertEquals("EMAILADDRESS=test2@example.com, GIVENNAME=firstname2, SURNAME=lastname2, CN=cn2, O=org2", 
                request.getSubject().toString());
    }
    
    /*
     * Large CSV test
     */
    @Test
    public void createTestCSVFile()
    throws Exception
    {
        int start = 0;
        int nrOfRequests = 1000;
        
        File csvFile = new File("test/tmp/certificate-requests-" + nrOfRequests + ".csv");
        
        CSVWriter writer = new CSVWriter(new FileWriter(csvFile));
        
        /*
         * Write header
         */
        String[] entries = new String[]{"e", "o", "cn", "fn", "ln"};

        writer.writeNext(entries);

        for (int i = start; i < start + nrOfRequests; i++)
        {
            entries[0] = "test" + i + "@example.com";
            entries[1] = "organisation " + i;
            entries[2] = "user " + i;
            entries[3] = "first name " + i;
            entries[4] = "last name " + i;
            
            writer.writeNext(entries);
        }
        writer.close();
        
        CSVRequestConverter converter = new CSVRequestConverter();
        
        converter.setMaxLines(nrOfRequests);
        
        List<RequestParameters> parameters = converter.convertCSV(new FileInputStream(csvFile));
        
        assertEquals(nrOfRequests, parameters.size());
    }
}
