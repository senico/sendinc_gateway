/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertStoreException;
import java.util.List;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.KeyAndCertificate;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SyncMode;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import mitm.common.security.ca.handlers.DelayedBuiltInCertificateRequestHandler;
import mitm.common.security.ca.hibernate.CertificateRequestStoreImpl;
import mitm.common.security.certificate.X500PrincipalBuilder;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.provider.MITMProvider;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class CAImplTest
{
    private static CAImpl ca;
    
    private static final File HIBERNATE_CONFIG = new File("test/resources/hibernate.cfg.xml");
    
    private static final File KEY_STORE_FILE = new File("test/resources/testdata/keys/testCA.p12");

    private static final int INITIAL_KEY_STORE_SIZE = 2;

    private static final int DEFAULT_THREAD_SLEEP_TIME = 1;
    private static final int DEFAULT_EXPIRATION_TIME = 3600;
    private static final int[] DEFAULT_DELAY_TIMES = new int[]{60};
    
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;

    private static SessionManager sessionManager;
    
    private static KeyAndCertStore keyAndCertStore;
        
    private static CertificateRequestStore certificateRequestStore;
    
    private  static CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;
    
    public static class DummyCASettings implements CASettings
    {
        @Override
        public Integer getKeyLength() throws HierarchicalPropertiesException {
            return 1024;
        }

        @Override
        public String getSignerThumbprint()
        throws HierarchicalPropertiesException
        {
            return "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" + 
                "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";
        }

        @Override
        public Integer getValidity()
        throws HierarchicalPropertiesException
        {
            return 365;
        }

        @Override
        public void setKeyLength(Integer length)
        throws HierarchicalPropertiesException
        {
        }

        @Override
        public void setSignerThumbprint(String thumbprint)
        throws HierarchicalPropertiesException
        {
        }

        @Override
        public void setValidity(Integer days)
        throws HierarchicalPropertiesException
        {
        }
        
        @Override
        public void setDefaultCommonName(String commonName) {
        }

        @Override
        public String getDefaultCommonName() {
            return null;
        }
        
        @Override
        public void setSignatureAlgorithm(String signatureAlgorithm) {
        }

        @Override
        public String getSignatureAlgorithm() {
            return null;
        }
        
        @Override
        public void setCAEmail(String email) {
        }

        @Override
        public String getCAEmail() {
            return null;
        }
        
        @Override
        public void setPasswordLength(Integer length)
        {
        }

        @Override
        public Integer getPasswordLength()
        {
            return 16;
        }

        @Override
        public String getCRLDistributionPoint()
        throws HierarchicalPropertiesException
        {
            return null;
        }

        @Override
        public boolean isAddCRLDistributionPoint()
        throws HierarchicalPropertiesException
        {
            return false;
        }

        @Override
        public void setAddCRLDistributionPoint(boolean add)
        throws HierarchicalPropertiesException
        {
        }

        @Override
        public void setCRLDistributionPoint(String url)
        throws HierarchicalPropertiesException
        {
        }
        
        @Override
        public void setStorePFXPassword(boolean store)
        throws HierarchicalPropertiesException
        {
        }

        @Override
        public boolean isStorePFXPassword()
        throws HierarchicalPropertiesException
        {
            return false;
        }

        @Override
        public String getCertificateRequestHandler()
        throws HierarchicalPropertiesException
        {
            return null;
        }

        @Override
        public void setCertificateRequestHandler(String certificateRequestHandler)
        throws HierarchicalPropertiesException
        {
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        LogManager.getLogger(CAImpl.class).setLevel(org.apache.log4j.Level.DEBUG);
                
        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(HIBERNATE_CONFIG);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);       
        
        keyStore = securityFactory.createKeyStore("PKCS12");
        
        keyStore.load(null);
        
        X509CertStoreExt certStore = new X509CertStoreExtHibernate("certificates", sessionManager);
        
        keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, "test");
        
        certificateRequestStore = new CertificateRequestStoreImpl(sessionManager);
        
        certificateRequestHandlerRegistry = new CertificateRequestHandlerRegistryImpl();
        
        CASettingsProvider settingsProvider = new CASettingsProvider()
        {
            @Override
            public CASettings getSettings()
            throws HierarchicalPropertiesException
            {
                return new DummyCASettings();
            }
        };

        CertificateRequestHandler builtInhandler = new BuiltInCertificateRequestHandler(
                certificateRequestHandlerRegistry, keyAndCertStore, settingsProvider, 
                new SMIMEKeyAndCertificateIssuer());
        
        certificateRequestHandlerRegistry.registerHandler(builtInhandler);

        CertificateRequestHandler multiStephandler = new DelayedBuiltInCertificateRequestHandler(
                certificateRequestHandlerRegistry, keyAndCertStore, settingsProvider, 
                new SMIMEKeyAndCertificateIssuer(), 3);
        
        certificateRequestHandlerRegistry.registerHandler(multiStephandler); 
        
        ca = new CAImpl(certificateRequestStore, certificateRequestHandlerRegistry, keyAndCertStore, 
                sessionManager, DEFAULT_THREAD_SLEEP_TIME, DEFAULT_EXPIRATION_TIME, DEFAULT_DELAY_TIMES);
        
        ca.startThread();
    }

    @AfterClass
    public static void setUpAfterClass() 
    throws Exception 
    {
        ca.requestStop();
    }
    
    @Before
    public void setup()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        resetKeyStore();
        proxy.deleteAllRequests();

        proxy.assertKeyAndCertStoreSize(INITIAL_KEY_STORE_SIZE);
        proxy.assertRequestStoreSize(0);
        
        ca.setThreadSleepTime(DEFAULT_THREAD_SLEEP_TIME);
        ca.setExpirationTime(DEFAULT_EXPIRATION_TIME);
        ca.setDelayTimes(DEFAULT_DELAY_TIMES);
    }
        
    private static void resetKeyStore()
    throws Exception 
    {        
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        proxy.clearKeyAndCertStore();

        keyStore.load(new FileInputStream(KEY_STORE_FILE), "test".toCharArray());
        
        proxy.sync();
    }
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }

        @StartTransaction
        public void sync()
        throws Exception
        {
            keyAndCertStore.sync(SyncMode.ALL);
        }
        
        @StartTransaction
        public void deleteAllRequests()
        {
            List<? extends CertificateRequest> all = certificateRequestStore.getAllRequests(null, null);
            
            for (CertificateRequest request : all) {
                certificateRequestStore.deleteRequest(request.getID());
            }
            
            assertEquals(0, certificateRequestStore.getSize());
        }

        @StartTransaction
        public void clearKeyAndCertStore()
        throws CertStoreException
        {
            keyAndCertStore.removeAllEntries();
        }
        
        @StartTransaction
        public void testRequestCertificate(String requestHandler, boolean immediate, String email)
        throws Exception
        {
            X500PrincipalBuilder pb = new X500PrincipalBuilder();
            
            pb.setCommonName("TEST CA");
            pb.setEmail(email);
            
            RequestParameters parameters = new RequestParametersImpl();
            
            parameters.setEmail(email);
            parameters.setSubject(pb.buildPrincipal());
            parameters.setCertificateRequestHandler(requestHandler);
            parameters.setKeyLength(1024);
            parameters.setSignatureAlgorithm("SHA256WithRSAEncryption");
            parameters.setValidity(365);
            
            KeyAndCertificate issued = ca.requestCertificate(parameters);
            
            if (immediate)
            {
                assertNotNull(issued);
    
                X509CertStoreEntry entry = keyAndCertStore.getByCertificate(issued.getCertificate());
                
                assertNotNull(entry);
                assertNotNull(entry.getKeyAlias());
                assertTrue(keyStore.containsAlias(entry.getKeyAlias()));
            }
            else {
                assertNull(issued);
            }
        }
        
        @StartTransaction
        public void assertRequestStoreSize(int size)
        {
            assertEquals(size, certificateRequestStore.getSize());
        }

        @StartTransaction
        public void assertKeyAndCertStoreSize(int size)
        {
            assertEquals(size, keyAndCertStore.size());
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public void assertKeyAndCertWithEmailCount(String email, long count)
        throws CertStoreException
        {
            long found = keyAndCertStore.getByEmailCount(email, Match.EXACT, Expired.NOT_ALLOWED, 
                    MissingKeyAlias.NOT_ALLOWED);
            
            assertEquals(count, found);     
        }
    }

    @Test(expected = CAException.class)
    public void testInvalidEmailAddress()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        proxy.testRequestCertificate(BuiltInCertificateRequestHandler.NAME, true, "&^&^&");
    }
    
    @Test
    public void testIssueCertificateDefaultRequestHandler()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        proxy.testRequestCertificate(BuiltInCertificateRequestHandler.NAME, true, "test1@example.com");
        proxy.testRequestCertificate(null, true, "test@example.com");
        
        proxy.assertKeyAndCertWithEmailCount("non-existing@example.com", 0);
        proxy.assertKeyAndCertWithEmailCount("test1@example.com", 1);
        proxy.assertKeyAndCertWithEmailCount("test@example.com", 1);
    }
    
    @Test
    public void testDelayedIssue()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        ca.setDelayTimes(new int[]{1, 0});
        
        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test2@example.com");
        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test3@example.com");
        proxy.assertRequestStoreSize(2);

        // wait some time to make sure the thread is sleeping
        Thread.sleep(1000);
        
        ca.setThreadSleepTime(0);
        ca.kick();

        Thread.sleep(2000);
        
        proxy.assertRequestStoreSize(0);
        proxy.assertKeyAndCertStoreSize(INITIAL_KEY_STORE_SIZE + 2);

        proxy.assertKeyAndCertWithEmailCount("test2@example.com", 1);
        proxy.assertKeyAndCertWithEmailCount("test3@example.com", 1);
    }
    
    @Test
    public void testExpire()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        ca.setDelayTimes(new int[]{1});
        
        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test4@example.com");
        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test5@example.com");
        proxy.assertRequestStoreSize(2);
        
        ca.setThreadSleepTime(0);
        ca.setExpirationTime(1);
        
        // wait some time to make sure the thread is sleeping
        Thread.sleep(1000);

        ca.kick();

        Thread.sleep(2000);
        proxy.assertRequestStoreSize(0);
        proxy.assertKeyAndCertStoreSize(INITIAL_KEY_STORE_SIZE);
    }
    
    @Test
    public void testPendingRequest()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy(); 

        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test@example.com");
        proxy.testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test@example.com");
        proxy.assertRequestStoreSize(2);
    }
        
    @Test(expected = CAException.class)
    public void testUnknownCertificateRequestHandler()
    throws Exception
    {
        AutoTransactDelegator.createProxy().testRequestCertificate("unknown", true, "test@example.com");
    }    
}
