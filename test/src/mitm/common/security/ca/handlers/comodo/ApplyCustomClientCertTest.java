/*
 * Copyright (c) 2010-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca.handlers.comodo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.X500PrincipalBuilder;
import mitm.common.security.certificate.X500PrincipalUtils;
import mitm.common.util.MiscStringUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Note: These tests do not use valid account settings. Certificates are therefore not requested unless the 
 * relevant account settings are changed. 
 */
public class ApplyCustomClientCertTest
{
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        InitializeBouncycastle.initialize();
        
        LogManager.getLogger(ApplyCustomClientCert.class).setLevel(org.apache.log4j.Level.DEBUG);
    }
    
    private KeyPair generateKeyPair()
    throws NoSuchAlgorithmException
    {
        return KeyPairGenerator.getInstance("RSA").generateKeyPair();   
    }
    
    private static ContentSigner getContentSigner(String signatureAlgorithm, PrivateKey privateKey)
    throws OperatorCreationException
    {
        JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(signatureAlgorithm);
        
        contentSignerBuilder.setProvider("BC");
        
        return contentSignerBuilder.build(privateKey);
    }
    
    @Test
    public void testApply()
    throws Exception
    {
        KeyPair keyPair = generateKeyPair();
        
        X500PrincipalBuilder principalBuilder = new X500PrincipalBuilder();
        
        principalBuilder.setCommonName("Martijn Brinkers");
        principalBuilder.setOrganisation("Djigzo");
        principalBuilder.setEmail("martijn@djigzo.com");
        
        PKCS10CertificationRequestBuilder requestBuilder = new PKCS10CertificationRequestBuilder(
                X500PrincipalUtils.toX500Name(principalBuilder.buildPrincipal()), SubjectPublicKeyInfo.getInstance(
                keyPair.getPublic().getEncoded()));
        
        PKCS10CertificationRequest pkcs10 = requestBuilder.build(getContentSigner("SHA1WithRSA", 
                keyPair.getPrivate()));

        String csr = MiscStringUtils.toAsciiString(Base64.encodeBase64(pkcs10.getEncoded()));
        
        ComodoConnectionSettings connectionSettings = new ComodoConnectionSettingsImpl(60000, null);
        
        ApplyCustomClientCert requestor = new ApplyCustomClientCert(connectionSettings);
        
        requestor.setAP("CHANGE");
        requestor.setDays(365);
        requestor.setPkcs10(csr);
        //requestor.setCACertificateID(1);
        
        assertFalse(requestor.apply());
        assertTrue(requestor.isError());
        assertEquals(CustomClientStatusCode.ARGUMENT_IS_INVALID, requestor.getErrorCode());
        assertEquals("The value of the 'ap' argument is invalid!", requestor.getErrorMessage());
    }
}
