package mitm.common.security.ca.handlers.comodo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Note: These tests do not use valid usernames and passwords. The details are therefore not retrieved.
 */
public class Tier2PartnerDetailsTest
{
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
     
        LogManager.getLogger(Tier2PartnerDetails.class).setLevel(org.apache.log4j.Level.DEBUG);
    }

    @Test
    public void testRetrieveDetails()
    throws Exception
    {
        ComodoConnectionSettings connectionSettings = new ComodoConnectionSettingsImpl(60000, null);
        
        Tier2PartnerDetails requestor = new Tier2PartnerDetails(connectionSettings);
        
        requestor.setLoginName("CHANGE");
        requestor.setLoginPassword("CHANGE");
        
        assertFalse(requestor.retrieveDetails());
        assertTrue(requestor.isError());
        assertEquals(CustomClientStatusCode.ARGUMENT_IS_INVALID, requestor.getErrorCode());
        assertEquals("The value of the 'loginName' argument is invalid!", requestor.getErrorMessage());
    }
}
