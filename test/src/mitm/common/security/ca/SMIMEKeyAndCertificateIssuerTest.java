/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import mitm.common.security.KeyAndCertificate;
import mitm.common.security.KeyAndCertificateImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.X500PrincipalBuilder;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.test.TestUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class SMIMEKeyAndCertificateIssuerTest
{
    private static KeyStore keyStore;
    
    private static KeyAndCertificate signer;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        keyStore = TestUtils.loadKeyStore(new File("test/resources/testdata/keys/testCA.p12"), "test");
        
        signer = new KeyAndCertificateImpl((PrivateKey) keyStore.getKey("ca", null), 
                (X509Certificate) keyStore.getCertificate("ca")); 
    }
    
    @Test
    public void testEmailAddress()
    throws Exception
    {
        RequestParameters request = new RequestParametersImpl();
        
        X500PrincipalBuilder subjectBuilder = new X500PrincipalBuilder();
        
        subjectBuilder.setCommonName("testCN");
        subjectBuilder.setEmail("subject@example.com");
        
        request.setCRLDistributionPoint("http://127.0.0.1");
        request.setEmail("test@example.com");
        request.setKeyLength(1024);
        request.setSignatureAlgorithm("SHA256WithRSAEncryption");
        request.setValidity(365);
        request.setSubject(subjectBuilder.buildPrincipal());
        
        SMIMEKeyAndCertificateIssuer issuer = new SMIMEKeyAndCertificateIssuer();
        
        KeyAndCertificate issued = issuer.issueKeyAndCertificate(request, signer);
        
        assertNotNull(issued);
        assertNotNull(issued.getCertificate());
        assertNotNull(issued.getPrivateKey());
        
        X509CertificateInspector inspector = new X509CertificateInspector(issued.getCertificate());
        
        assertEquals(2, inspector.getEmail().size());
        assertTrue(inspector.getEmail().contains("test@example.com"));
        assertTrue(inspector.getEmail().contains("subject@example.com"));

        assertEquals(1, inspector.getEmailFromDN().size());
        assertEquals(1, inspector.getEmailFromAltNames().size());
        
        assertEquals("subject@example.com", StringUtils.join(inspector.getEmailFromDN(), ","));
        assertEquals("test@example.com", StringUtils.join(inspector.getEmailFromAltNames(), ","));
    }
}
