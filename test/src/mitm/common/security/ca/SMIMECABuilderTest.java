/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.ca;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import mitm.common.security.certificate.X500PrincipalBuilder;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


public class SMIMECABuilderTest
{
    @BeforeClass
    public static void setUpBeforeClass() 
    throws NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    @Test
    public void testBuildCA()
    throws Exception
    {
        CABuilder builder = new SMIMECABuilder();
        
        X500PrincipalBuilder issuerBuilder = new X500PrincipalBuilder();
        
        issuerBuilder.setCommonName("Test Root");
        issuerBuilder.setCountryCode("NL");
        issuerBuilder.setLocality("Amsterdam");
        issuerBuilder.setState("NH");

        X500PrincipalBuilder subjectBuilder = new X500PrincipalBuilder();
        
        issuerBuilder.setCommonName("Test CA");
        issuerBuilder.setCountryCode("NL");
        issuerBuilder.setLocality("Amsterdam");
        issuerBuilder.setState("NH");
        
        CABuilderParameters parameters = new CABuilderParametersImpl();
        
        parameters.setRootSubject(issuerBuilder.buildPrincipal());
        parameters.setIntermediateSubject(subjectBuilder.buildPrincipal());
        
        CABuilderResult result = builder.buildCA(parameters);
        
        assertNotNull(result);
        assertNotNull(result.getIntermediate());
        assertNotNull(result.getIntermediate().getCertificate());
        assertNotNull(result.getIntermediate().getPrivateKey());
        assertNotNull(result.getRoot());
        assertNotNull(result.getRoot().getCertificate());
        assertNotNull(result.getRoot().getPrivateKey());
        assertEquals(parameters.getRootSubject(), result.getRoot().getCertificate().getSubjectX500Principal());
        assertEquals(parameters.getRootSubject(), result.getRoot().getCertificate().getIssuerX500Principal());
        assertEquals(parameters.getIntermediateSubject(), result.getIntermediate().getCertificate().getSubjectX500Principal());
        assertEquals(parameters.getRootSubject(), result.getIntermediate().getCertificate().getIssuerX500Principal());

        /*
         * The intermediate should be signed by the root
         */
        result.getIntermediate().getCertificate().verify(result.getRoot().getCertificate().getPublicKey());
    }
}
