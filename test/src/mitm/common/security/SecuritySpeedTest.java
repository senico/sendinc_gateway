/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import static org.junit.Assert.assertTrue;

import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import mitm.application.djigzo.DjigzoTestUtils;
import mitm.application.djigzo.service.SystemServices;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.certificate.validator.PKITrustCheckCertificateValidator;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.util.CloseableIteratorException;
import mitm.common.util.CloseableIteratorUtils;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Warning: this test uses the non test database!!
 * 
 * This test is used to check the speed of some PKI classes. The test assumes that the database
 * contains specfic certificates. This test is therefore not added to the test suites.
 * 
 * @author Martijn Brinkers
 *
 */
public class SecuritySpeedTest 
{
    private static String USE_PRODUCTION_DB_SPRING_CONFIG = "test/resources/spring/djigzo-use-production-db.xml";
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        // TODO: do not use DjigzoTestUtils because otherwise this test depends on Djigzo packages
        DjigzoTestUtils.initialize(USE_PRODUCTION_DB_SPRING_CONFIG);
    }
        
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            SessionManager sessionManager = SystemServices.getSessionManager();

            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public long getKeyAndCertStoreSize(Expired expired, MissingKeyAlias missingKeyAlias)
        {
            return SystemServices.getPKISecurityServices().getKeyAndCertStore().size(expired, missingKeyAlias);
        }
        
        @StartTransaction
        public List<? extends X509CertStoreEntry> getCertificates(X509CertSelector selector, 
                MissingKeyAlias missingKeyAlias, Integer start, Integer max)
        throws CertStoreException, CloseableIteratorException
        {
            return CloseableIteratorUtils.toList(SystemServices.getPKISecurityServices().getKeyAndCertStore().
                    getCertStoreIterator(selector, missingKeyAlias, start, max));
        }
        
        @StartTransaction
        public Collection<X509Certificate> getCertificates(X509CertSelector selector)
        throws CertStoreException
        {
            return SystemServices.getPKISecurityServices().getKeyAndCertStore().getCertificates(selector);
        }

        @StartTransaction
        public PKITrustCheckCertificateValidator getValidator(X509Certificate certificate, Date date) 
        throws CertificateException
        {
            PKITrustCheckCertificateValidator validator = SystemServices.getPKISecurityServices().
                    getPKITrustCheckCertificateValidatorFactory().createValidator(null);
            
            validator.setDate(date);
            
            validator.isValid(certificate);
            
            return validator;
        }
        
        @StartTransaction
        public boolean isInUse(X509Certificate certificate)
        throws CertStoreException
        {
            return SystemServices.getKeyAndCertificateWorkflow().isInUse(certificate);
        }
    }

    @Test
    public void testInUseSpeed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        X509CertSelector selector = new X509CertSelector();
        
        Collection<X509Certificate> certificates = proxy.getCertificates(selector);
        
        System.out.println("Nr of certs: " + certificates.size());
        
        assertTrue(certificates.size() > 0);

        long start = System.currentTimeMillis();

        int i = 0;
        
        for (X509Certificate certificate : certificates)
        {
            proxy.isInUse(certificate);
            
            if (i % 100 == 0)
            {
                System.out.println("Iteration " + i);

                long diff = System.currentTimeMillis() - start;
                
                double perSecond = i * 1000.0 / diff;
                
                System.out.println("InUse calls/sec: " + perSecond);
            }
            
            i++;
            
            /*
             * break test off to make sure it doesn't run too long
             */
            if (i == 300) {
                break;
            }
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = i * 1000.0 / diff;
        
        System.out.println("InUse calls/sec: " + perSecond);
        
        /*
         * On a Core2 Q8300 with 61000 certificates I get about 35-40 InUse calls/sec in non-debug mode.
         */
    }    
    
    @Test
    public void testGetCertStoreIteratorSpeed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        int repeat = 100;

        long nrOfCerts = proxy.getKeyAndCertStoreSize(Expired.ALLOWED, MissingKeyAlias.ALLOWED);
        
        int max = 25;

        X509CertSelector selector = new X509CertSelector();

        Random ran = new Random();
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < repeat; i++)
        {
            int next = ran.nextInt((int)nrOfCerts - 25);
            
            Collection<? extends X509CertStoreEntry> certificates = proxy.getCertificates(selector, 
                    MissingKeyAlias.ALLOWED, next, max);
            
            assertTrue(certificates.size() == Math.min(max, nrOfCerts));
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = repeat * 1000.0 / diff;

        System.out.println("CertStoreIterator calls/sec: " + perSecond);

        /*
         * On a Core2 Q8300 with 61000 certificates I get about 6 calls/sec in non-debug mode.
         */
    }
    
    @Test
    public void testKeyAndCertStoreSizeSpeed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        int repeat = 100;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            assertTrue(proxy.getKeyAndCertStoreSize(Expired.ALLOWED, MissingKeyAlias.ALLOWED) > 0);
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = repeat * 1000.0 / diff;

        System.out.println("CertStoreSize calls/sec: " + perSecond);

        /*
         * On a Core2 Q8300 with 61000 certificates I get about 7 calls/sec in non-debug mode.
         */
    }
    
    @Test
    public void testPKITrustCheckCertificateValidatorSpeed()
    throws Exception
    {
        AutoTransactDelegator proxy = AutoTransactDelegator.createProxy();
        
        X509CertSelector selector = new X509CertSelector();
        
        Collection<X509Certificate> certificates = proxy.getCertificates(selector);
        
        System.out.println("Nr of certs: " + certificates.size());
        
        assertTrue(certificates.size() > 0);

        long start = System.currentTimeMillis();

        int i = 0;
        
        for (X509Certificate certificate : certificates)
        {
            PKITrustCheckCertificateValidator result = proxy.getValidator(certificate, new Date());
            
            if (!result.isValid()) {
                //System.out.println("Certificate is not valid.");
            }
            
            if (i % 100 == 0)
            {
                System.out.println("Iteration " + i);

                long diff = System.currentTimeMillis() - start;
                
                double perSecond = i * 1000.0 / diff;
                
                System.out.println("PKITrustCheckCertificateValidator Validations/sec: " + perSecond);
            }
            
            i++;
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double perSecond = i * 1000.0 / diff;
        
        System.out.println("PKITrustCheckCertificateValidator Validations/sec: " + perSecond);
        
        /*
         * On a Core2 Q8300 with 61000 certificates I get about 160 validations/second in non-debug mode.
         */
    }    
}
