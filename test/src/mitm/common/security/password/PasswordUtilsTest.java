/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mitm.common.security.digest.Digest;


public class PasswordUtilsTest
{
    @Test
    public void testIsEqualSafe()
    throws Exception
    {
        assertTrue(PasswordUtils.isEqualDigest("a", "a", Digest.SHA1));
        assertTrue(PasswordUtils.isEqualDigest("a", "a", Digest.SHA256));
        assertTrue(PasswordUtils.isEqualDigest(null, null, Digest.SHA1));
        assertTrue(PasswordUtils.isEqualDigest("", "", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("a", null, Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest(null, "a", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest(null, "", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("", null, Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("qwerty", "qwerty ", Digest.SHA1));
    }
    
    @Test
    public void testIsEqualSafeSpeedTest()
    throws Exception
    {
        final int repeat = 100000;
        
        final long start = System.currentTimeMillis();
        
        for (int i = 0; i < repeat; i++)
        {
            String password = "testqwertyu" + Integer.toString(i);
            
            assertTrue(PasswordUtils.isEqualDigest(password, password, Digest.SHA256));
        }
        
        final long diff = System.currentTimeMillis() - start;
        
        double perSecond = repeat * 1000.0 / diff;
        
        System.out.println(perSecond);
        
        assertTrue("Can be slower on slower systems", perSecond > 80000);
    }
}
