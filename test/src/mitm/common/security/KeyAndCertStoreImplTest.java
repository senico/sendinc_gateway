/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;

import javax.security.auth.x500.X500Principal;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.HibernateUtils;
import mitm.common.hibernate.SessionManagedAutoCommitProxyFactory;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.reflection.ProxyFactoryException;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certstore.X509CertStoreEntry;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.cms.KeyTransRecipientIdImpl;
import mitm.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import mitm.common.security.provider.MITMProvider;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class KeyAndCertStoreImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager;
    
    private static KeyAndCertStore keyAndCertStore;
    
    private static KeyStore testKeysKeyStore;
    
    private static KeyStore keyStore;
    
    private static AutoTransactDelegator proxy;
    
    public static class AutoTransactDelegator
    {
        public AutoTransactDelegator() {
            // required by AutoCommitProxyFactory
        }
        
        public static AutoTransactDelegator createProxy()
        throws ProxyFactoryException, NoSuchMethodException
        {
            AutoTransactDelegator autoTransactDelegator = new SessionManagedAutoCommitProxyFactory<AutoTransactDelegator>(
                    AutoTransactDelegator.class, sessionManager).createProxy();
            
            return autoTransactDelegator;
        }
        
        @StartTransaction
        public void cleanKeyAndCertStore()
        throws CertStoreException 
        {
            keyAndCertStore.removeAllEntries();
        }
        
        @StartTransaction
        public void addKeyAndCertificate(KeyAndCertificate keyAndCertificate)
        throws CertStoreException, KeyStoreException
        {
            keyAndCertStore.addKeyAndCertificate(keyAndCertificate);
        }
        
        @StartTransaction
        public long getKeyAndCertStoreSize() {
            return keyAndCertStore.size();
        }
        
        @StartTransaction
        public void importkeyStore(KeyStore keyStore)
        throws Exception
        {
            Enumeration<String> aliases = keyStore.aliases();
            
            while (aliases.hasMoreElements()) 
            {
                String alias = aliases.nextElement();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate(alias);
                
                PrivateKey key = (PrivateKey) keyStore.getKey(alias, null);
                
                KeyAndCertificate keyAndCertificate = new KeyAndCertificateImpl(key, certificate);
                
                proxy.addKeyAndCertificate(keyAndCertificate);
            }
        }
        
        @StartTransaction
        public KeyAndCertificate getKeyAndCertificate(String thumbprint)
        throws CertStoreException, KeyStoreException
        {
            KeyAndCertificate keyAndCertificate = null;
            
            X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);
            
            if (entry != null) {
                keyAndCertificate = keyAndCertStore.getKeyAndCertificate(entry);
            }
            
            return keyAndCertificate;
        }
        
        @StartTransaction
        public void removeCertificate(String thumbprint)
        throws CertStoreException, KeyStoreException
        {
            X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);
            
            if (entry != null) {
                keyAndCertStore.removeCertificate(entry.getCertificate());
            }
        }

        @StartTransaction
        public void removeAll()
        throws CertStoreException, KeyStoreException
        {
            keyAndCertStore.removeAllEntries();
        }

        @StartTransaction
        public Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier)
        throws CertStoreException, KeyStoreException
        {
            return keyAndCertStore.getMatchingKeys(keyIdentifier);
        }

        @StartTransaction
        public Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier, Integer firstResult, 
                Integer maxResults)
        throws CertStoreException, KeyStoreException
        {
            return keyAndCertStore.getMatchingKeys(keyIdentifier, firstResult, maxResults);
        }
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        SecurityFactoryFactory.getSecurityFactory();
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        HibernateUtils.recreateTables(sessionSource.getHibernateConfiguration());
        
        keyStore = KeyStore.getInstance(MITMProvider.DATABASE_KEYSTORE, "mitm");
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter("keyStore", sessionManager));

        X509CertStoreExt certStore = new X509CertStoreExtHibernate("certStore", sessionManager);
        
        keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, null);
        
        proxy = AutoTransactDelegator.createProxy();
        
        testKeysKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        
        testKeysKeyStore.load(new FileInputStream("test/resources/testdata/keys/testCA.p12"), "test".toCharArray());
    }

    @Before
    public void setup()
    throws Exception
    {
        proxy.cleanKeyAndCertStore();

        assertEquals(0, proxy.getKeyAndCertStoreSize());
    }
        
    @Test
    public void testAddKeyAndCertificate()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);
        
        assertEquals(2, proxy.getKeyAndCertStoreSize());

        String thumbprint = "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" + 
            "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";
        
        KeyAndCertificate keyAndCertificate = proxy.getKeyAndCertificate(thumbprint);
        
        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());
        
        /*
         * test if importing the same certificate again but now with a null key does not 
         * remove the existing key alias
         */
        keyAndCertificate = new KeyAndCertificateImpl(null, keyAndCertificate.getCertificate());
        
        proxy.addKeyAndCertificate(keyAndCertificate);

        assertEquals(2, proxy.getKeyAndCertStoreSize());

        keyAndCertificate = proxy.getKeyAndCertificate(thumbprint);
        
        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());
    }
    
    @Test
    public void testRemoveCertificate()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);
        
        assertEquals(2, proxy.getKeyAndCertStoreSize());

        String thumbprint = "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" + 
            "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";
        
        KeyAndCertificate keyAndCertificate = proxy.getKeyAndCertificate(thumbprint);
        
        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());

        assertTrue(keyStore.containsAlias(thumbprint));
        
        proxy.removeCertificate(thumbprint);

        keyAndCertificate = proxy.getKeyAndCertificate(thumbprint);
        
        assertNull(keyAndCertificate);
        
        assertFalse(keyStore.containsAlias(thumbprint));
    }
    
    @Test
    public void testRemoveAll()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);
        
        assertEquals(2, proxy.getKeyAndCertStoreSize());
        assertEquals(2, keyStore.size());

        proxy.removeAll();

        assertEquals(0, proxy.getKeyAndCertStoreSize());
        assertEquals(0, keyStore.size());
    }        
    
    @Test
    public void testGetMatchingKeys()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                new BigInteger("115FCAD6B536FD8D49E72922CD1F0DA", 16),
                null);
        
        Collection<? extends PrivateKey> keys = proxy.getMatchingKeys(recipientInfo);
        
        assertEquals(1, keys.size());
    }
    
    @Test
    public void testGetMatchingKeysMultiple()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);
        
        Collection<? extends PrivateKey> keys = proxy.getMatchingKeys(recipientInfo);
        
        assertEquals(2, keys.size());
    }    

    @Test
    public void testGetMatchingKeysMultipleMaxSize()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);
        
        Collection<? extends PrivateKey> keys = proxy.getMatchingKeys(recipientInfo, 0, 1);
        
        assertEquals(1, keys.size());
    }    

    @Test
    public void testGetMatchingKeysMultipleOutOfRange()
    throws Exception
    {
        proxy.importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);
        
        Collection<? extends PrivateKey> keys = proxy.getMatchingKeys(recipientInfo, 10, 100);
        
        assertEquals(0, keys.size());
    }    
}
