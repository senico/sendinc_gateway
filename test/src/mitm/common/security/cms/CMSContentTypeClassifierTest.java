/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class CMSContentTypeClassifierTest
{
    private static final File testDir = new File("test/resources/testdata/mail");

    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    private static MimeMessage loadMessage(String filename) 
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    
    @Test
    public void testClearSigned() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("clear-signed-validcertificate.eml");
        
        Multipart multiPart = (Multipart) message.getContent();
        
        BodyPart signaturePart = multiPart.getBodyPart(1);
        
        InputStream signatureStream = signaturePart.getInputStream();
        
        CMSContentType contentType = CMSContentTypeClassifier.getContentType(signatureStream);
        
        assertEquals(CMSContentType.SIGNEDDATA, contentType);
    }
    
    @Test
    public void testOpaqueSigned() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("signed-opaque-validcertificate.eml");
        
        InputStream signatureStream = message.getInputStream();
        
        CMSContentType contentType = CMSContentTypeClassifier.getContentType(signatureStream);
        
        assertEquals(CMSContentType.SIGNEDDATA, contentType);
    }

    @Test
    public void testEncrypted() 
    throws MessagingException, IOException 
    {
        MimeMessage message = loadMessage("encrypted-both-recipientid-validcertificate.eml");
        
        InputStream signatureStream = message.getInputStream();
        
        CMSContentType contentType = CMSContentTypeClassifier.getContentType(signatureStream);
        
        assertEquals(CMSContentType.ENVELOPEDDATA, contentType);
    }

    @Test
    public void testCompressed() 
    throws IOException, MessagingException 
    {
        MimeMessage message = loadMessage("compressed.eml");
        
        InputStream signatureStream = message.getInputStream();
        
        CMSContentType contentType = CMSContentTypeClassifier.getContentType(signatureStream);
        
        assertEquals(CMSContentType.COMPRESSEDDATA, contentType);
    }    
}
