/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.security.cert.CertSelector;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

import mitm.common.security.certificate.X509CertificateInspector;
import mitm.test.TestUtils;

import org.junit.Test;

public class KeyTransRecipientIdImplTest
{
    /*
     * Check if the selector returned from KeyTransRecipientIdImpl works on selecting 
     * on subjectKeyIdentifier. X509CertSelector expects the subjectKeyIdentifier to
     * be DER encoded.
     */
    @Test
    public void testX509SelectorSubjectKeyIdentifier() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/subjectkeyident.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        byte[] subjectKeyIdentifier = X509CertificateInspector.getSubjectKeyIdentifier(certificate);
        
        KeyTransRecipientId id = new KeyTransRecipientIdImpl(null, null, subjectKeyIdentifier);
        
        CertSelector selector = id.getSelector();
                
        assertTrue(selector.match(certificate));
    }

    @Test
    public void testX509SelectorAll() 
    throws Exception 
    {
        File file = new File("test/resources/testdata/certificates/subjectkeyident.cer");
        
        X509Certificate certificate = TestUtils.loadCertificate(file);

        byte[] subjectKeyIdentifier = X509CertificateInspector.getSubjectKeyIdentifier(certificate);
        
        KeyTransRecipientId id = new KeyTransRecipientIdImpl(
                new X500Principal("CN=AddTrust Class 1 CA Root, OU=AddTrust TTP Network, O=AddTrust AB, C=SE"), 
                new BigInteger("571D8005CE05222616E82BBAF0CC71D2", 16), 
                subjectKeyIdentifier);
        
        CertSelector selector = id.getSelector();
                
        assertTrue(selector.match(certificate));
    }   
    
    /*
     * Either the subjectKeyIdentifier must be specified or both issuer and serialNumber must be
     * specified.
     */
    @Test(expected = RecipientInfoException.class)
    public void testNullValues()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(null, null, null);
    }    

    @Test
    public void testNullValuesNonStrict()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(null, null, null, false);
    }    
    
    /*
     * Either the subjectKeyIdentifier must be specified or both issuer and serialNumber must be
     * specified.
     */
    @Test(expected = RecipientInfoException.class)
    public void testNullValues2()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(null, new BigInteger(new byte[]{0}), null);
    }    

    @Test
    public void testNullValues2NonStrict()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(null, new BigInteger(new byte[]{0}), null, false);
    }    
    
    /*
     * Either the subjectKeyIdentifier must be specified or both issuer and serialNumber must be
     * specified.
     */
    @Test(expected = RecipientInfoException.class)
    public void testNullValues3()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(new X500Principal(""), null, null);
    }
    
    @Test
    public void testNullValues3NonStrict()
    throws RecipientInfoException 
    {
        new KeyTransRecipientIdImpl(new X500Principal(""), null, null, false);
    }    
}
