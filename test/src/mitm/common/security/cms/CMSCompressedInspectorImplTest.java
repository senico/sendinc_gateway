/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.cms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.bouncycastle.InitializeBouncycastle;

import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.mail.smime.SMIMECompressed;
import org.bouncycastle.mail.smime.SMIMECompressedParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class CMSCompressedInspectorImplTest
{
    private static final File testDir = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws InstantiationException, IllegalAccessException, ClassNotFoundException 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
    }

    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message) 
    throws MessagingException
    {
        // the message should contain the signed from, to and subject
        assertEquals("<test@example.com>", message.getHeader("from", ","));
        assertEquals("<test@example.com>", message.getHeader("to", ","));
        assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }

    @Test
    public void testDecompress() 
    throws FileNotFoundException, MessagingException, CMSException
    {
        MimeMessage compressedMessage = loadMessage("compressed.eml");

        SMIMECompressed compressed = new SMIMECompressed(compressedMessage);
        
        CMSCompressedDataAdapter cmsCompressed = CMSAdapterFactory.createAdapter(compressed);
        
        assertTrue(cmsCompressed instanceof CMSCompressedDataAdapterImpl);
    }

    @Test
    public void testDecompressParser() 
    throws FileNotFoundException, MessagingException, CMSException
    {
        MimeMessage compressedMessage = loadMessage("compressed.eml");

        SMIMECompressedParser compressedParser = new SMIMECompressedParser(compressedMessage);
        
        CMSCompressedDataAdapter cmsCompressed = CMSAdapterFactory.createAdapter(compressedParser);
        
        assertTrue(cmsCompressed instanceof CMSCompressedDataParserAdapterImpl);
    }
    
    public void testDecompress(CMSCompressedDataAdapter compressedParser) 
    throws MessagingException, CryptoMessageSyntaxException, IOException
    {
        CMSCompressedInspector inspector = new CMSCompressedInspectorImpl(compressedParser);
        
        MimeMessage decompressedMessage = MailUtils.loadMessage(inspector.getContentStream());
        
        File file = new File(tempDir, "decompressed.eml");
        
        MailUtils.writeMessage(decompressedMessage, file);
        
        decompressedMessage = MailUtils.loadMessage(file);
        
        assertTrue(decompressedMessage.isMimeType("multipart/mixed"));
        
        checkForEmbeddedHeaders(decompressedMessage);
    }
}
