/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.security.cert.CRL;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collection;
import java.util.List;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreEntry;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.util.CloseableIterator;
import mitm.common.util.CloseableIteratorUtils;
import mitm.test.TestUtils;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class X509CRLStoreExtHibernateTest
{
    private static final File testBase = new File("test/resources/testdata/crls");
    
    private static HibernateSessionSource sessionSource;
    
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
     
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        X509CRLStoreExt someCRLStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "someCRLStore").create();
        
        someCRLStore.removeAllEntries();
        
        addTestCRLs(someCRLStore);
    }

    
    @Before
    public void setup() 
    throws Exception 
    {
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
  
        crlStore.removeAllEntries();
    }

    private static void addCRL(String filename, X509CRLStoreExt crlStore) 
    throws CRLStoreException 
    {
        try {
            File crlFile = new File(testBase, filename);
           
            X509CRL crl = TestUtils.loadX509CRL(crlFile);
            
            crlStore.addCRL(crl);
        }
        catch(Exception e) {
            throw new CRLStoreException(e);
        }
    }
    
    private static void addTestCRLs(X509CRLStoreExt crlStore) 
    throws CRLStoreException
    {
        addCRL("intel-basic-enterprise-issuing-CA.crl", crlStore);
        addCRL("test-ca-no-next-update.crl", crlStore);
        addCRL("itrus.com.cn.crl", crlStore);
        addCRL("test-ca.crl", crlStore);
        addCRL("ThawteSGCCA.crl", crlStore);
    }

    @Test
    public void testSize()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
        
        assertEquals(5, crlStore.size());
    }
    
    @Test
    public void testRemoveCRL()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
        
        Collection<X509CRL> crls = crlStore.getCRLs(null);
        
        assertEquals(5, crls.size());
        
        X509CRL crl = crls.iterator().next();
        
        assertTrue(crlStore.contains(crl));
        
        crlStore.remove(crl);

        assertFalse(crlStore.contains(crl));

        crls = crlStore.getCRLs(null);
        
        assertEquals(4, crls.size());
    }
    
    
    @Test
    public void testUniqueConstraint()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();

        try {
            addCRL("itrus.com.cn.crl", crlStore);
            addCRL("itrus.com.cn.crl", crlStore);
            
            fail();
        }
        catch(CRLStoreException e) {
            Throwable cause = ExceptionUtils.getCause(e);
            
            assertTrue(cause instanceof ConstraintViolationException);
        }
    }
    
    
    @Test
    public void testGetAllCRLsNullSelector()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
        
        Collection<? extends CRL> crls = crlStore.getCRLs(null);
        
        assertEquals(5, crls.size());
    }

    @Test
    public void testGetAllCRLs()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
     
        X509CRLSelector selector = new X509CRLSelector();
        
        Collection<? extends CRL> crls = crlStore.getCRLs(selector);
        
        assertEquals(5, crls.size());
    }

    @Test
    public void testGetCRLIterator()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
     
        X509CRLSelector selector = new X509CRLSelector();
        
        CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(selector);
        
        assertTrue(!iterator.isClosed());
        
        List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);
        
        assertTrue(iterator.isClosed());
        
        assertEquals(5, crls.size());
    }

    @Test
    public void testGetCRLIteratorNulSelector()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        addTestCRLs(crlStore);
     
        CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(null);
        
        assertTrue(!iterator.isClosed());
        
        List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);
        
        assertTrue(iterator.isClosed());
        
        assertEquals(5, crls.size());
    }

    
    @Test
    public void testGetCRLIteratorNoCRLs()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(null);
        
        assertTrue(!iterator.isClosed());
        
        List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);
        
        assertTrue(iterator.isClosed());
        
        assertEquals(0, crls.size());
    }
    
    
    @Test
    public void testRemoveAll()
    throws Exception
    {
        // get CRLS does not have a @StartTransaction so auto commit should make no difference
        X509CRLStoreExt crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "test").create();
        
        Collection<? extends CRL> crls = crlStore.getCRLs(null);
        
        assertEquals(0, crls.size());

        addTestCRLs(crlStore);

        crls = crlStore.getCRLs(null);
        
        assertEquals(5, crls.size());
        
        crlStore.removeAllEntries();

        crls = crlStore.getCRLs(null);
        
        assertEquals(0, crls.size());
    }
    
    @Test
    public void testGetCRLEntryIterator()
    throws Exception
    {
        // we should not use the auto commit because the iterator must have a valid transaction
        X509CRLStoreExt crlStore = new X509CRLStoreExtInjectSessionFactory(sessionSource, "test").create();
        
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        addTestCRLs(crlStore);
     
        X509CRLSelector selector = new X509CRLSelector();

        CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(selector);

        assertTrue(!iterator.isClosed());
        
        List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());
        
        assertEquals(5, crls.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }

    
    @Test
    public void testGetCRLEntryIteratorNullSelector()
    throws Exception
    {
        // we should not use the auto commit because the iterator must have a valid transaction
        X509CRLStoreExt crlStore = new X509CRLStoreExtInjectSessionFactory(sessionSource, "test").create();
        
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        addTestCRLs(crlStore);
     
        CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(null);

        assertTrue(!iterator.isClosed());
        
        List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());
        
        assertEquals(5, crls.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }
    
    @Test
    public void testGetCRLEntryIteratorNoCRLs()
    throws Exception
    {
        // we should not use the auto commit because the iterator must have a valid transaction
        X509CRLStoreExt crlStore = new X509CRLStoreExtInjectSessionFactory(sessionSource, "test").create();
        
        Transaction tx = sessionSource.getSession().beginTransaction();
        
        X509CRLSelector selector = new X509CRLSelector();

        CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(selector);

        assertTrue(!iterator.isClosed());
        
        List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());
        
        assertEquals(0, crls.size());
        
        tx.commit();
        
        sessionSource.getSession().close();
    }
    
}
