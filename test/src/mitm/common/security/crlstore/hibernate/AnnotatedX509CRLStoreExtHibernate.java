/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.crlstore.hibernate;

import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.util.Collection;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.annotations.InjectHibernateSession;
import mitm.common.hibernate.annotations.InjectHibernateSessionSource;
import mitm.common.hibernate.annotations.StartTransaction;
import mitm.common.security.crlstore.CRLStoreException;
import mitm.common.security.crlstore.X509CRLStoreEntry;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.util.CloseableIterator;

import org.hibernate.Session;

/**
 * Extension of X509CRLStoreExtHibernate for auto commit X509CRLStoreExtHibernate store. 
 * 
 * @author Martijn Brinkers
 *
 */
public class AnnotatedX509CRLStoreExtHibernate implements X509CRLStoreExt
{
	private String storeName;

	private SessionManager sessionManager;
	
    public AnnotatedX509CRLStoreExtHibernate(String storeName) 
    {
        this.storeName = storeName;
    }
        
    /* should have a default constructor for Auto commit proxy */    
    public AnnotatedX509CRLStoreExtHibernate()
    {
    }
    
    public void setStoreName(String storeName) {
    	this.storeName = storeName;
    }
    
    private SessionManager getSessionManager()
    {
    	if (sessionManager == null) {
    		throw new IllegalStateException("sessionManager is not valid.");
    	}
    	
    	return sessionManager;
    }

    @InjectHibernateSession
    public void setSession(Session session) {
        getSessionManager().setSession(session);
    }
        
    @InjectHibernateSessionSource
    public void setSessionSource(HibernateSessionSource sessionSource) 
    {
        sessionManager = new SessionManagerImpl(sessionSource);
    }

    private X509CRLStoreExtHibernate getDelegate() {
    	return new X509CRLStoreExtHibernate(storeName, getSessionManager());
    }

    @Override
    @StartTransaction
    public void addCRL(X509CRL crl) 
    throws CRLStoreException 
    {
    	getDelegate().addCRL(crl);
	}

    @Override
    @StartTransaction
    public X509CRL getCRL(String thumbprint)
    throws CRLStoreException
    {
    	return getDelegate().getCRL(thumbprint);
    }

    @Override
    @StartTransaction
	public boolean contains(X509CRL crl) 
    throws CRLStoreException 
    {
		return getDelegate().contains(crl);
	}

    @Override
    @StartTransaction
	public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector)
	throws CRLStoreException 
	{
		return getDelegate().getCRLIterator(crlSelector);
	}

    @Override
    @StartTransaction
	public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector,
			Integer firstResult, Integer maxResults) 
	throws CRLStoreException 
	{
		return getDelegate().getCRLIterator(crlSelector, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CRLStoreEntry> getCRLStoreIterator(
			CRLSelector crlSelector) 
	throws CRLStoreException 
	{
		return getDelegate().getCRLStoreIterator(crlSelector);
	}

    @Override
    @StartTransaction
	public CloseableIterator<? extends X509CRLStoreEntry> getCRLStoreIterator(
			CRLSelector crlSelector, Integer firstResult, Integer maxResults)
	throws CRLStoreException 
	{
		return getDelegate().getCRLStoreIterator(crlSelector, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public Collection<X509CRL> getCRLs(CRLSelector crlSelector)
	throws CRLStoreException 
	{
		return getDelegate().getCRLs(crlSelector);
	}

    @Override
    @StartTransaction
	public Collection<X509CRL> getCRLs(CRLSelector crlSelector,
			Integer firstResult, Integer maxResults) 
	throws CRLStoreException 
	{
		return getDelegate().getCRLs(crlSelector, firstResult, maxResults);
	}

    @Override
    @StartTransaction
	public void remove(X509CRL crl) 
    throws CRLStoreException 
    {
    	getDelegate().remove(crl);
	}

    @Override
    @StartTransaction
	public void removeAllEntries() 
    throws CRLStoreException 
    {
    	getDelegate().removeAllEntries();
	}

    @Override
    @StartTransaction
	public void replace(X509CRL oldCRL, X509CRL newCRL)
	throws CRLStoreException 
	{
    	getDelegate().replace(oldCRL, newCRL);
	}

    @Override
    @StartTransaction
	public long size()
	{
    	return getDelegate().size();
	}
}
