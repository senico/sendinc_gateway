/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.cms.KeyNotFoundException;
import mitm.common.security.keystore.KeyStoreKeyProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMIMEEnvelopedInspectorImplTest
{
    private static final File testDir = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;
    private static KeyStoreKeyProvider keyStoreKeyProvider;
    
    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        keyStoreKeyProvider = new KeyStoreKeyProvider(keyStore, "test");
    }

    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message) 
    throws MessagingException
    {
        // the message should contain the signed from, to and subject
        assertEquals("<test@example.com>", message.getHeader("from", ","));
        assertEquals("<test@example.com>", message.getHeader("to", ","));
        assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }
    
    
    @Test
    public void testEnveloped() 
    throws Exception 
    {
        MimeMessage message = loadMessage("encrypted-validcertificate.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message,  keyStoreKeyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        MimeMessage decrypted = inspector.getContentAsMimeMessage();
        
        assertNotNull(decrypted);
        assertTrue(decrypted.isMimeType("multipart/mixed"));
        
        checkForEmbeddedHeaders(decrypted);
        
        File file = new File(tempDir, "test-encrypted-validcertificate-decrypted.eml");
        
        MailUtils.writeMessage(decrypted, file);
    }
    
    @Test(expected = KeyNotFoundException.class)
    public void testOutlook2010MissingSubjKeyIdNoWorkaround() 
    throws Exception 
    {
        MimeMessage message = loadMessage("outlook2010_cert_missing_subjkeyid.eml");

        KeyStoreKeyProvider keyStoreKeyProvider = new KeyStoreKeyProvider(loadKeyStore(new File(
                "test/resources/testdata/keys/outlook2010_cert_missing_subjkeyid.p12"), ""), "");

        keyStoreKeyProvider.setUseOL2010Workaround(false);
        
        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message, keyStoreKeyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        inspector.getContentAsMimeMessage();
    }
    
    @Test
    public void testOutlook2010MissingSubjKeyIdWorkAround() 
    throws Exception 
    {
        MimeMessage message = loadMessage("outlook2010_cert_missing_subjkeyid.eml");

        KeyStoreKeyProvider keyStoreKeyProvider = new KeyStoreKeyProvider(loadKeyStore(new File(
                "test/resources/testdata/keys/outlook2010_cert_missing_subjkeyid.p12"), ""), "");

        keyStoreKeyProvider.setUseOL2010Workaround(true);
        
        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message, keyStoreKeyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        File file = new File(tempDir, "test-testOutlook2010MissingSubjKeyId-decrypted.eml");
        
        MailUtils.writeMessage(decrypted, file);
        
        assertNotNull(decrypted);
        assertTrue(decrypted.isMimeType("text/plain"));
        assertNull(decrypted.getSubject());
        assertEquals("Created with Outlook 2010 Beta (14.0.4536.1000)", StringUtils.trim(
                (String)decrypted.getContent()));
    }    
}
