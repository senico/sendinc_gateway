/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailUtils;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SyncMode;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import mitm.common.security.provider.MITMProvider;
import mitm.common.security.smime.SMIMEHeader;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMIMEHandlerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;

    private static HibernateSessionSource sessionSource;
    
    private static SessionManager sessionManager;
    
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    
    private static PKISecurityServices pKISecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtHibernate("certificates", sessionManager);
        rootStore = new X509CertStoreExtHibernate("roots", sessionManager);
        crlStore = new X509CRLStoreExtHibernate("crls", sessionManager);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, "test");
        
        pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);
    }

    @Before
    public void setup()
    throws Exception
    {
        sessionSource.getSession().beginTransaction();
                
        certStore.removeAllEntries();
        rootStore.removeAllEntries();
        crlStore.removeAllEntries();
        
        pKISecurityServices.getKeyAndCertStore().sync(SyncMode.ALL);
        
        addTestCAs();
        
        // because X509CertStoreExt uses a statelessSession for getting certificates we need to commit now
        // otherwise the certificates just added are not found
        sessionSource.getSession().getTransaction().commit();

        sessionSource.getSession().beginTransaction();        
    }
    
    private SMIMEHandler createSMIMEHandler()
    {
        SMIMEHandler sMIMEHandler = new SMIMEHandler(pKISecurityServices);
        
        sMIMEHandler.setCertificatesEventListener(createCertificatesEventListener());
        
        return sMIMEHandler;
    }
    
    @After
    public void tearDown()
    {
        sessionSource.getSession().getTransaction().commit();
    }
   
    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }

    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException, CertStoreException 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws CertStoreException
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }        
    }    
    
    private static void addTestCAs() 
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
    }
    
    private static void handleCertificates(Collection<? extends X509Certificate> certificates)
    throws CertificateException
    {
        X509CertStoreExt certStore;
        try {
            // we need to add the certificates in a different transaction otherwise they cannot be found
            // because we use a statelessSession for the certificate iterators of X509CertStoreExtHibernate.
            certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        }
        catch (CertStoreException e) {
            throw new CertificateException(e);
        }

        for (X509Certificate certificate : certificates) {
            try {
                if (!certStore.contains(certificate)) {
                    certStore.addCertificate(certificate);
                }
            }
            catch (CertStoreException e) {
                throw new CertificateException(e);
            }
        }
    }
    
    private static CertificateCollectionEvent createCertificatesEventListener()
    {
        return new CertificateCollectionEvent()
        {
            @Override
            public void certificatesEvent(Collection<? extends X509Certificate> certificates) 
            throws CertificateException 
            {
                SMIMEHandlerTest.handleCertificates(certificates);
            }
        };
    }

    @Test
    public void testOpaqueSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-opaque-validcertificate.eml"));

        SMIMEHandler smimeHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(smimeHandler.handlePart(message), 
                smimeHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testopaquesigned.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);

        assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, 
                SMIMEHeader.Strict.YES));        
    }

    @Test
    public void testOpaqueSignedNonStandardContentType()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, 
                "mail/signed-opaque-non-standard-content-type.eml"));

        SMIMEHandler smimeHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(smimeHandler.handlePart(message), 
                smimeHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testopaquesignednonstandardcontenttype.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, 
                SMIMEHeader.Strict.YES));        
    }
    
    
    @Test
    public void testClearSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-validcertificate.eml"));

        SMIMEHandler smimeHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(smimeHandler.handlePart(message), 
                smimeHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testclearsigned.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);        
    }

    @Test
    public void testClearSignedRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-validcertificate.eml"));
        
        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        sMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage =  new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testclearsignedremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);        
    }

    @Test
    public void testEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-validcertificate.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testencrypted.eml");
        
        MailUtils.writeMessage(newMessage, file);
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("<test@example.com>", header);
        
        assertTrue(sMIMEHandler.isDecrypted());
    }

    @Test
    public void testEncryptedNonStandardContentType()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-non-standard-smime-headers.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testencryptednonstandardcontenttype.eml");

        MailUtils.writeMessage(newMessage, file);
        
        assertFalse(sMIMEHandler.isDecrypted());

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("\"info@example.com\" <info@example.com>", header);
        
        assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }
    
    @Test
    public void testEncryptedHiddenHeaders()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-hidden-headers.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        sMIMEHandler.setProtectedHeaders("subject", "to", "from");

        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testencryptedhiddenheaders.eml");
        
        MailUtils.writeMessage(newMessage, file);
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("<test@example.com>", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("To", ","));
        assertEquals("<test@example.com>", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("Subject", ","));
        assertEquals("normal message with attachment", header);
        
        assertTrue(sMIMEHandler.isDecrypted());
    }

    @Test
    public void testEncryptedHiddenHeadersNoSubjectProtect()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-hidden-headers.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        sMIMEHandler.setProtectedHeaders("to", "from");

        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testencryptedhiddenheaders.eml");
        
        MailUtils.writeMessage(newMessage, file);
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("<test@example.com>", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("To", ","));
        assertEquals("<test@example.com>", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("Subject", ","));
        assertEquals("xxx", header);
        
        assertTrue(sMIMEHandler.isDecrypted());
    }
    
    @Test
    public void testEncryptedNoDecryptionKeyFound()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-no-decryption-key.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testencryptednodecryptionkeyfound.eml");
        
        MailUtils.writeMessage(newMessage, file);
                
        assertFalse(sMIMEHandler.isDecrypted());

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("\"info@example.com\" <info@example.com>", header);
        
        assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }

    @Test
    public void testCompressed()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/compressed.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(sMIMEHandler.handlePart(message), 
                sMIMEHandler, 0);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);

        File file = new File(tempDir, "testcompressed.eml");
        
        MailUtils.writeMessage(newMessage, file);
                
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Compressed-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
        assertEquals("<test@example.com>", header);        
    }

    @Test
    public void testNoSMIME()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/unknown-charset.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = sMIMEHandler.handlePart(message);
        
        assertNull(newMessage);
    }

    @Test
    public void testFakeEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/fake-encrypted.eml"));

        SMIMEHandler sMIMEHandler = createSMIMEHandler();
        
        MimeMessage newMessage = sMIMEHandler.handlePart(message);
        
        assertNull(newMessage);
    }
}
