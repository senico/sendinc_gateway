/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailUtils;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtAutoCommitFactory;
import mitm.common.security.keystore.KeyStoreKeyProvider;
import mitm.common.security.provider.MITMProvider;
import mitm.common.security.smime.SMIMEInspector;
import mitm.common.security.smime.SMIMEInspectorImpl;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMIMEInfoHandlerImplTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;
    private static KeyStoreKeyProvider keyProvider;

    private static HibernateSessionSource sessionSource;
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    private static PKISecurityServices pkiSecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        keyProvider = new KeyStoreKeyProvider(keyStore, "test");
        
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        SessionManager sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        rootStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "roots").create();
        crlStore = new X509CRLStoreExtAutoCommitFactory(sessionSource, "crls").create();
    }

    @Before
    public void setup()
    throws Exception
    {
        certStore.removeAllEntries();
        rootStore.removeAllEntries();
        crlStore.removeAllEntries();
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        pkiSecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);
    }
    
    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }

    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException, CertStoreException 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws CertStoreException
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }        
    }
    
    private static void addTestCAs() 
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
    }
    
    @Test
    public void testClearSigned()
    throws Exception
    {
        addTestCAs();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-validcertificate.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        // add Certificates from CMS blob
        addCertificates(inspector.getSignedInspector().getCertificates(), certStore);
                
        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 0, false);
                
        File file = new File(tempDir, "testclearsigned.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Verified-0-0", ","));
        assertEquals("True", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
    }

    @Test
    public void testClearSignedPathNotFound()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-validcertificate.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 123, false);
        
        File file = new File(tempDir, "testclearsignedpathnotfound.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-ID-0-123", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Verified-0-123", ","));
        assertEquals("True", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Trusted-0-123", ","));
        assertEquals("False", header);
    }
    
    @Test
    public void testClearSignedNoCertificates() 
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-no-certificates.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 0, false);
        
        File file = new File(tempDir, "testclearsignednocertificates.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Verified-0-0", ","));
        assertEquals("False", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Verification-Info-0-0", ","));
        assertEquals("Signing certificate could not be found.", header);
    }
    
    @Test
    public void testClearSignedNoCertificatesButInStore() 
    throws Exception
    {
        addTestCAs();

        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-no-certificates.eml"));

        addCertificates(new File(testBase, "certificates/valid_certificate_mitm_test_ca.cer"), certStore);        
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        SMIMEInfoHandlerImpl handler = new SMIMEInfoHandlerImpl(pkiSecurityServices);        
        
        handler.setAddCertificates(true);
        
        message = handler.handle(message, inspector, 0, false);
        
        File file = new File(tempDir, "testclearsignednocertificatesbutinstore.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Verified-0-0", ","));
        assertEquals("True", header);

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
    }
    
    @Test
    public void testEnveloped() 
    throws Exception
    {
        addTestCAs();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-validcertificate.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);
        
        File file = new File(tempDir, "testenveloped.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Recipient-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/" + 
                "115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);        
    }

    @Test
    public void testEnvelopedMultipleRecipients() 
    throws Exception
    {
        addTestCAs();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypt-15-recipients.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);
        
        File file = new File(tempDir, "testenvelopedmultiplerecipients.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertEquals("3DES, Key size: 168", header);

        final int recipients = 15;
        
        for (int i = 0; i < recipients; i++) 
        {
            header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Recipient-" + i + "-1", 
                    ","));
            assertNotNull(header);
        }

        header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Recipient-" + 
                (recipients + 1) + "-1", ","));
        
        assertNull(header);
    }
    
    @Test
    public void testCompressed() 
    throws Exception
    {
        addTestCAs();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/compressed.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);
        
        File file = new File(tempDir, "testcompressed.eml");
        
        MailUtils.writeMessage(message, file);

        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Compressed-1", ","));
        assertEquals("True", header);
    }
    
    @Test
    public void testMaxRecipients() 
    throws Exception
    {
        addTestCAs();
        
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypt-15-recipients.eml"));
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        SMIMEInfoHandlerImpl handler = new SMIMEInfoHandlerImpl(pkiSecurityServices); 
        
        int maxRecipients = 3;
        
        handler.setMaxRecipients(maxRecipients);
        
        message = handler.handle(message, inspector, 1, false);
        
        File file = new File(tempDir, "testenvelopedmultiplesigners.eml");
        
        MailUtils.writeMessage(message, file);
        
        String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertEquals("3DES, Key size: 168", header);

        for (int i = 0; i < maxRecipients; i++) 
        {
            header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Recipient-" + i + "-1", 
                    ","));
            assertNotNull(header);
        }

        for (int i = maxRecipients; i < 15; i++) 
        {
            header = HeaderUtils.decodeHeaderValue(message.getHeader("X-Djigzo-Info-Encryption-Recipient-" + i + "-1", 
                    ","));
            assertNull(header);
        }
        
        assertNull(header);
    }    
}
