/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.mail.HeaderUtils;
import mitm.common.mail.MailUtils;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.SyncMode;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtAutoCommitFactory;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import mitm.common.security.provider.MITMProvider;
import mitm.common.security.smime.SMIMEHeader;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RecursiveSMIMEHandlerTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    private static final File tempDir = new File("test/tmp");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;

    private static HibernateSessionSource sessionSource;
    
    private static SessionManager sessionManager;
    
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    
    private static PKISecurityServices pKISecurityServices;
    
    private static RecursiveSMIMEHandler recursiveSMIMEHandler;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
                
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtHibernate("certificates", sessionManager);
        rootStore = new X509CertStoreExtHibernate("roots", sessionManager);
        crlStore = new X509CRLStoreExtHibernate("crls", sessionManager);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, keyStore, "test");
        
        pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);
    }

    @Before
    public void setup()
    throws Exception
    {
        sessionSource.getSession().beginTransaction();
                
        recursiveSMIMEHandler = new RecursiveSMIMEHandler(pKISecurityServices);     
        
        recursiveSMIMEHandler.setCertificatesEventListener(createCertificatesEventListener());        
        
        certStore.removeAllEntries();
        rootStore.removeAllEntries();
        crlStore.removeAllEntries();
        
        pKISecurityServices.getKeyAndCertStore().sync(SyncMode.ALL);

        addTestCAs();
        
        // because X509CertStoreExt uses a statelessSession for getting certificates we need to commit now
        // otherwise the certificates just added are not found
        sessionSource.getSession().getTransaction().commit();

        sessionSource.getSession().beginTransaction();
    }

    @After
    public void tearDown()
    {
        sessionSource.getSession().getTransaction().commit();
    }
    
    private static KeyStore loadKeyStore(File file, String password)
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }

    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws CertificateException, NoSuchProviderException, FileNotFoundException, CertStoreException 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws CertStoreException
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate)
            {
                if (!certStore.contains((X509Certificate) certificate))
                {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }        
    }
    
    private static void addTestCAs() 
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);        
    }
    
    private static void handleCertificates(Collection<? extends X509Certificate> certificates)
    throws CertificateException
    {
        X509CertStoreExt certStore;
        
        try {
            // we need to add the certificates in a different transaction otherwise they cannot be found
            // because we use a statelessSession for the certificate iterators of X509CertStoreExtHibernate.
            certStore = new X509CertStoreExtAutoCommitFactory(sessionSource, "certificates").create();
        }
        catch (CertStoreException e) {
            throw new CertificateException(e);
        }

        for (X509Certificate certificate : certificates) {
            try {
                if (!certStore.contains(certificate)) {
                    certStore.addCertificate(certificate);
                }
            }
            catch (CertStoreException e) {
                throw new CertificateException(e);
            }
        }
    }
    
    private static CertificateCollectionEvent createCertificatesEventListener()
    {
        return new CertificateCollectionEvent()
        {
            @Override
            public void certificatesEvent(Collection<? extends X509Certificate> certificates) 
            throws CertificateException 
            {
                RecursiveSMIMEHandlerTest.handleCertificates(certificates);
            }
        };
    }

    @Test
    public void testEmbeddedClearSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/embedded-clear-signed.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testEmbeddedClearSigned.eml");
        
        MailUtils.writeMessage(newMessage, file);
    }
    
    @Test
    public void testSignedKeyUsageNotForSigning()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-signed-key-usage-not-for-signing.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "clear-signed-key-usage-not-for-signing.eml");
        
        MailUtils.writeMessage(newMessage, file);
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/1178C38151374D6C4B29F891B9B4A77/", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Verified-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("False", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-Info-0-0", ","));
        assertTrue(header.startsWith("Signing certificate not trusted. Message: Key usage does not allow digitalSignature or nonRepudiation."));
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }        
    
    @Test
    public void testSignedMissingExtSMIMEKeyUsage()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/clear-sign-missing-smime-ext-key-usage.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "clear-sign-missing-smime-ext-key-usage.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FD035BA042503BCC6CA44680F9F8/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("False", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-Info-0-0", ","));
        assertTrue(header.startsWith("Signing certificate not trusted. Message: Extended key usage does not allow emailProtection."));
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }    
    
    @Test
    public void testSignedEncryptEmptyBody()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed-encrypted-rfc822-empty-body.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencryptemptybody.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }    
    
    @Test
    public void testSignedEncryptEmptyBodyRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed-encrypted-rfc822-empty-body.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencryptemptybodyremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testEncryptedRFC822()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-rfc822.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testencryptedrfc822.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedEncryptedRFC822()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-encrypted-rfc822.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedencryptedrfc822.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("AES128, Key size: 128", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-123", ","));
        assertEquals("456", header);
    }
    
    @Test
    public void testAttachedEncryptedCorrupt()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-corrupt-encrypted.eml"));
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNull(newMessage);
    }
    
    @Test
    public void testEncryptedNonStandardContentType()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypted-non-standard-smime-headers.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testencryptednonstandardcontenttype.eml");
        
        MailUtils.writeMessage(newMessage, file);

        // message cannot be decrypted because there is no private key
        assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }

    @Test
    public void testAttachedEncryptedNonStandardContentType()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-encrypted-non-standard-content-type.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedencryptednonstandardcontenttype.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("AES128, Key size: 128", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedMultipleParts()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed-encrypted-multiple-parts.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedmultipleparts.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(3, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
        
        
        // get the decompressed part
        bp = (MimeBodyPart) mp.getBodyPart(2);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Compressed-0", ","));
        assertEquals("True", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedSignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed-encrypted.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedsignedencrypted.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedSignedEncryptedRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed-encrypted.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedsignedencryptedremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedSignedRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedsignedremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testAttachedSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-signed.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedsigned.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testAttachedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/attached-encrypted.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testattachedencrypted.eml");
        
        MailUtils.writeMessage(newMessage, file);

        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        Multipart mp = (Multipart) newMessage.getContent();
        
        assertEquals(2, mp.getCount());
        
        MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);
        
        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart mp2 = (Multipart) bp.getContent();
        
        assertEquals(2, mp2.getCount());
        
        bp = (MimeBodyPart) mp.getBodyPart(1);
        
        assertTrue(bp.isMimeType("message/rfc822"));
        
        newMessage = (MimeMessage) bp.getContent();
        
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("AES128, Key size: 128", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testEncryptSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypt-signed-validcertificate.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testencryptsigned.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertNull(header);
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }
    
    @Test
    public void testSignedEncryptSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-encrypt-signed-validcertificate.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencryptsigned.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertNull(header);
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);
    }

    @Test
    public void testSignedEncryptSignedRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-encrypt-signed-validcertificate.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencryptsignedremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-0", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-1", ","));
        assertEquals("3DES, Key size: 168", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-2", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-2", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        assertTrue(newMessage.isMimeType("multipart/mixed"));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testEncryptSignedEncryptRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypt-signed-encrypt-validcertificate.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testencryptsignedencryptremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);

        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-2", ","));
        assertEquals("3DES, Key size: 168", header);
        
        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testSignedEncryptRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-encrypt-validcertificate.eml"));

        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencryptremovesignature.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testSignedEncrypt()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/signed-encrypt-validcertificate.eml"));

        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testsignedencrypt.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
        assertEquals("test 1,test 2", header);        
    }
    
    @Test
    public void testMaxRecursion()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(testBase, "mail/encrypt-signed-encrypt-validcertificate.eml"));

        recursiveSMIMEHandler.setMaxRecursion(2);
        recursiveSMIMEHandler.setRemoveSignature(true);
        
        MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);
        
        assertNotNull(newMessage);
        assertNotSame(newMessage, message);
        
        File file = new File(tempDir, "testMaxRecursion.eml");
        
        MailUtils.writeMessage(newMessage, file);

        String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-ID-0-1", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770/", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Signer-Trusted-0-1", ","));
        assertEquals("True", header);
        
        header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Djigzo-Info-Encryption-Algorithm-0", ","));
        assertEquals("3DES, Key size: 168", header);

        assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES));
    }    
}
