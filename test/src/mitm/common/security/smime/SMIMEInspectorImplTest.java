/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.cms.CryptoMessageSyntaxException;
import mitm.common.security.cms.SignerInfo;
import mitm.common.security.keystore.KeyStoreKeyProvider;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMIMEInspectorImplTest
{
    private static final File testDir = new File("test/resources/testdata/mail");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;
    private static KeyStoreKeyProvider keyProvider;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
        
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
        
        keyProvider = new KeyStoreKeyProvider(keyStore, "test");
    }

    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException 
    {        
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
            
            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());
    
            return keyStore;
        }
        catch (NoSuchProviderException e) {
            throw new KeyStoreException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }
        catch (CertificateException e) {
            throw new KeyStoreException(e);
        }
        catch (FileNotFoundException e) {
            throw new KeyStoreException(e);
        }
        catch (IOException e) {
            throw new KeyStoreException(e);
        }
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    @Test
    public void testClearSignedNoCertificates() 
    throws MessagingException, IOException, SMIMEInspectorException, CryptoMessageSyntaxException
    {
        MimeMessage message = loadMessage("signed-no-certificates.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        SMIMESignedInspector signedInspector = inspector.getSignedInspector();
        
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNotNull(signedInspector);
        assertEquals(0, signedInspector.getCertificates().size());
        assertEquals(0, signedInspector.getCRLs().size());
        assertEquals(1, signedInspector.getSigners().size());
        
        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();
        
        assertNotNull(mimeBodyPart);
        assertTrue(mimeBodyPart.isMimeType("text/plain"));
        assertEquals("test", ((String) mimeBodyPart.getContent()).trim());

        message = inspector.getContentAsMimeMessage();
        
        assertNotNull(message);
        assertTrue(message.isMimeType("text/plain"));
        assertEquals("test", ((String) message.getContent()).trim());
    }

    
    @Test
    public void testSMIMECapabilities() 
    throws MessagingException, IOException, SMIMEInspectorException, CryptoMessageSyntaxException
    {
        MimeMessage message = loadMessage("signed-no-certificates.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        SMIMESignedInspector signedInspector = inspector.getSignedInspector();
        
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNotNull(signedInspector);
        
        List<SignerInfo> signers = signedInspector.getSigners(); 
        assertEquals(1, signers.size());
        
        SignerInfo signer = signers.get(0);
        
        List<SMIMECapabilityInfo> capabilities = SMIMECapabilitiesInspector.inspect(signer.getSignedAttributes());
        
        assertEquals(11, capabilities.size());

        assertEquals("AES256",                  capabilities.get(0).toString());
        assertEquals("AES192",                  capabilities.get(1).toString());
        assertEquals("AES128",                  capabilities.get(2).toString());
        assertEquals("3DES",                    capabilities.get(3).toString());
        assertEquals("RC2, 128",                capabilities.get(4).toString());
        assertEquals("1.3.6.1.4.1.188.7.1.1.2", capabilities.get(5).toString()); /* IDEA algorithm */
        assertEquals("CAST5, 128",              capabilities.get(6).toString());
        assertEquals("CAMELLIA256",             capabilities.get(7).toString());
        assertEquals("CAMELLIA192",             capabilities.get(8).toString());
        assertEquals("CAMELLIA128",             capabilities.get(9).toString());
        assertEquals("SEED",                    capabilities.get(10).toString());
    }   
    
    
    @Test
    public void testEnveloped() 
    throws Exception
    {
        MimeMessage message = loadMessage("encrypted-validcertificate.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertNotNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
        
        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();
        
        assertNotNull(mimeBodyPart);
        assertTrue(mimeBodyPart.isMimeType("multipart/mixed"));
    }
    
    @Test
    public void testClearSigned() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("clear-signed-validcertificate.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNotNull(inspector.getSignedInspector());
    }

    @Test
    public void testClearSignedSwapped() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("clear-signed-swapped-parts.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNotNull(inspector.getSignedInspector());
    }

    @Test
    public void testOpaqueSigned() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("signed-opaque-validcertificate.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNotNull(inspector.getSignedInspector());
    }

    @Test
    public void testCompressed() 
    throws Exception
    {
        MimeMessage message = loadMessage("compressed.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.COMPRESSED, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNotNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
        
        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();
        
        assertNotNull(mimeBodyPart);
        assertTrue(mimeBodyPart.isMimeType("multipart/mixed"));
    }

    @Test
    public void testNoSMIME() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("unknown-content-type.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
    }

    @Test
    public void testFakeEncrypted() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("fake-encrypted.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
    }

    @Test
    public void testFakeOpaqueSigned() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("fake-opaque-signed.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
    }

    @Test
    public void testFakeClearSigned() 
    throws MessagingException, IOException, SMIMEInspectorException
    {
        MimeMessage message = loadMessage("fake-clear-signed.eml");
        
        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider, 
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());
        
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        assertNull(inspector.getEnvelopedInspector());
        assertNull(inspector.getCompressedInspector());
        assertNull(inspector.getSignedInspector());
    }
}
