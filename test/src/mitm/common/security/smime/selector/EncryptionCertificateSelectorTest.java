/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime.selector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mitm.common.hibernate.HibernateSessionSource;
import mitm.common.hibernate.SessionManager;
import mitm.common.hibernate.SessionManagerImpl;
import mitm.common.hibernate.StandardHibernateSessionSourceImpl;
import mitm.common.security.KeyAndCertStore;
import mitm.common.security.KeyAndCertStoreImpl;
import mitm.common.security.PKISecurityServices;
import mitm.common.security.bouncycastle.InitializeBouncycastle;
import mitm.common.security.certificate.CertificateUtils;
import mitm.common.security.certificate.X509CertificateInspector;
import mitm.common.security.certstore.X509CertStoreExt;
import mitm.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import mitm.common.security.crl.CRLUtils;
import mitm.common.security.crlstore.X509CRLStoreExt;
import mitm.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import mitm.common.security.provider.MITMProvider;
import mitm.test.TestUtils;

import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncryptionCertificateSelectorTest
{
    private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");
    
    private static final File testBase = new File("test/resources/testdata/");
    
    private static HibernateSessionSource sessionSource;
    private static SessionManager sessionManager; 
    
    private static X509CertStoreExt certStore;
    private static X509CertStoreExt rootStore;
    private static X509CRLStoreExt crlStore;
    
    private static PKISecurityServices pKISecurityServices;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();
                
        sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

        sessionManager = new SessionManagerImpl(sessionSource);
        
        MITMProvider.initialize(sessionManager);
        
        certStore = new X509CertStoreExtHibernate("certificates", sessionManager);
        rootStore = new X509CertStoreExtHibernate("roots", sessionManager);
        crlStore = new X509CRLStoreExtHibernate("crls", sessionManager);
        
        KeyAndCertStore keyAndCertStore = new KeyAndCertStoreImpl(certStore, null, null);
        
        pKISecurityServices = TestUtils.createDefaultPKISecurityServices(keyAndCertStore, 
        		rootStore, crlStore);
    }

    @Before
    public void setup()
    throws Exception
    {
        Session session = sessionSource.newSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            sessionManager.setSession(session);
            
            certStore.removeAllEntries();
            rootStore.removeAllEntries();
            crlStore.removeAllEntries();
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
        finally {
        	sessionManager.setSession(null);
            
            sessionSource.closeSession(session);
        }            
    }

    @After
    public void tearDown()
    {
    }
    
    private static void addCertificates(File file, X509CertStoreExt certStore) 
    throws Exception 
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);
        
        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore) 
    throws Exception
    {
        Session session = sessionSource.newSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            sessionManager.setSession(session);
            
            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate)
                {
                    if (!certStore.contains((X509Certificate) certificate))
                    {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
        finally {
        	sessionManager.setSession(null);

            sessionSource.closeSession(session);
        }
    }

    private static void addCRLs(File file, X509CRLStoreExt crlStore) 
    throws Exception 
    {
        Collection<? extends CRL> crls = CRLUtils.readCRLs(file);
        
        addCRLs(crls, crlStore);
    }

    private static void addCRLs(Collection<? extends CRL> crls, X509CRLStoreExt crlStore) 
    throws Exception
    {
        Session session = sessionSource.newSession();
        
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();

            sessionManager.setSession(session);
            
            for (CRL crl : crls)
            {
                if (crl instanceof X509CRL)
                {
                    if (!crlStore.contains((X509CRL) crl))
                    {
                        crlStore.addCRL((X509CRL) crl);
                    }
                }
            }
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            
            throw e;
        }
        finally {
        	sessionManager.setSession(null);

            sessionSource.closeSession(session);
        }
    }

    @Test
    public void testGetCertificates()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);
                
        /* because we added a new root we want to make sure it is picked up immediately */
        pKISecurityServices.getTrustAnchorBuilder().refresh();
        
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

            Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

            Set<String> serials = new HashSet<String>();
            
            for (X509Certificate certificate : certificates)
            {
                String serial = X509CertificateInspector.getSerialNumberHex(certificate);
                
                serials.add(serial);
            }
            
            assertEquals(13, certificates.size());
            assertEquals(serials.size(), certificates.size());

            assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
            assertTrue(serials.contains("1178C38151374D6C4B29F891B9B4A77")); /* KeyUsageNotForSigning*/
            assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
            assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
            assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
            assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/
            assertTrue(serials.contains("115FCD741088707366E9727452C9770")); /* ValidCertificate*/
            assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
            assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
            assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
            assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
            assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
            assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/

            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
    
    @Test
    public void testGetCertificatesMissingRoot()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/example-self-signed-10.p7b"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);

        /* because we added a new root we want to make sure it is picked up immediately */
        pKISecurityServices.getTrustAnchorBuilder().refresh();
        
        Session session = sessionSource.getSession();
        
        Transaction tx = session.beginTransaction();

        try {
        	sessionManager.setSession(session);
            
            EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

            assertEquals(10, pKISecurityServices.getRootStore().getCertificates(null).size());

            Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");
            
            /* root is missing */
            assertEquals(0, certificates.size());
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
        
    @Test
    public void testGetCertificatesIntermediateRevoked()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);
        addCRLs(new File(testBase, "crls/test-root-ca-revoked.crl"), crlStore);
        
        /* because we added a new root we want to make sure it is picked up immediately */
        pKISecurityServices.getTrustAnchorBuilder().refresh();
        
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

            Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

            /* all certificates should have been revoked because the intermediate has been revoked */
            assertEquals(0, certificates.size());
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
        

    @Test
    public void testGetCertificatesEndEntityRevoked()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);
        addCRLs(new File(testBase, "crls/test-ca.crl"), crlStore);
        
        /* because we added a new root we want to make sure it is picked up immediately */
        pKISecurityServices.getTrustAnchorBuilder().refresh();
        
        Transaction tx = sessionSource.getSession().beginTransaction();

        try {
            EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

            Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

            Set<String> serials = new HashSet<String>();
            
            for (X509Certificate certificate : certificates)
            {
                String serial = X509CertificateInspector.getSerialNumberHex(certificate);
                
                serials.add(serial);
            }
            
            assertEquals(12, certificates.size());
            assertEquals(serials.size(), certificates.size());

            assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
            assertTrue(serials.contains("1178C38151374D6C4B29F891B9B4A77")); /* KeyUsageNotForSigning*/
            assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
            assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
            assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
            assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/
            /*
             * this certificate is revoked
             */ 
            assertFalse(serials.contains("115FCD741088707366E9727452C9770")); /* ValidCertificate*/ 
              
            assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
            assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
            assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
            assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
            assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
            assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/
            
            tx.commit();
        }
        catch(Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }
            
    @Test
    public void testGetCertificatesSpeed()
    throws Exception
    {
        addCertificates(new File(testBase, "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(testBase, "certificates/mitm-test-ca.cer"), certStore);
        addCertificates(new File(testBase, "certificates/testCertificates.p7b"), certStore);
        
        /* because we added a new root we want to make sure it is picked up immediately */
        pKISecurityServices.getTrustAnchorBuilder().refresh();
        
        final int tries = 100;
        
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < tries; i++) 
        {
            Transaction tx = sessionSource.getSession().beginTransaction();
            
            try {
                EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);
            
                Set<X509Certificate> certificates = selector.getMatchingCertificates("test3@example.com");
                
                assertTrue(certificates.size() > 0);
                
                tx.commit();
            }
            catch(Exception e) {
                if (tx != null) {
                    tx.rollback();
                }
                
                throw e;
            }
        }
        
        long diff = System.currentTimeMillis() - start;
        
        double timePerMatch = diff * 0.001 / tries;
        
        System.out.println("Seconds / match: " + timePerMatch);
        
        if (timePerMatch > 0.03) {
            /***************************************************
             * Note: This might fail on slower systems!!
             ***************************************************/
            fail("seconds/match too slow. Note: This might fail on slower systems!!!");
        }
    }
}