/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.security.smime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mitm.common.mail.MailUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMIMEUtilsTest
{
    private static final File testDir = new File("test/resources/testdata/mail");

    @BeforeClass
    public static void setUpBeforeClass() 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }

    @Test
    public void testDissectSigned() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("clear-signed-validcertificate.eml");
        
        MimeMultipart multipart = (MimeMultipart) message.getContent();
        
        BodyPart[] parts = SMIMEUtils.dissectSigned(multipart);
        
        assertEquals(2, parts.length);
        assertNotNull(parts[0]);
        assertNotNull(parts[1]);
        assertTrue(parts[0].isMimeType("multipart/mixed"));
        assertTrue(parts[1].isMimeType("application/pkcs7-signature"));
    }

    @Test
    public void testDissectSignedSwitched() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("clear-signed-validcertificate.eml");
        
        MimeMultipart multipart = (MimeMultipart) message.getContent();

        BodyPart part0 = multipart.getBodyPart(0); 
        BodyPart part1 = multipart.getBodyPart(1); 
        
        assertTrue(part0.isMimeType("multipart/mixed"));
        assertTrue(part1.isMimeType("application/pkcs7-signature"));
        
        MimeMultipart multipartSwitched = new MimeMultipart();

        multipartSwitched.addBodyPart(part1);
        multipartSwitched.addBodyPart(part0);

        part0 = multipartSwitched.getBodyPart(0); 
        part1 = multipartSwitched.getBodyPart(1); 
        
        assertTrue(part0.isMimeType("application/pkcs7-signature"));
        assertTrue(part1.isMimeType("multipart/mixed"));
        
        BodyPart[] parts = SMIMEUtils.dissectSigned(multipartSwitched);
        
        assertEquals(2, parts.length);
        assertNotNull(parts[0]);
        assertNotNull(parts[1]);
        assertTrue(parts[0].isMimeType("multipart/mixed"));
        assertTrue(parts[1].isMimeType("application/pkcs7-signature"));
    }
    
    @Test
    public void testDissectNull() 
    throws MessagingException
    {
        assertNull(SMIMEUtils.dissectSigned(null));
    }

    @Test
    public void testDissectNoMatch() 
    throws MessagingException
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());

        assertNull(SMIMEUtils.dissectSigned(multipart));
    }

    @Test
    public void testDissectOnePart() 
    throws MessagingException
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());

        assertNull(SMIMEUtils.dissectSigned(multipart));
    }

    @Test
    public void testDissectThreeParts() 
    throws MessagingException
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());

        assertNull(SMIMEUtils.dissectSigned(multipart));
    }
}
