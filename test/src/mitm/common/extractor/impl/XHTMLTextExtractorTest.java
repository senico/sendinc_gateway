/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.extractor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.common.extractor.ExtractedPart;
import mitm.common.extractor.TextExtractorContext;
import mitm.common.extractor.TextExtractorEventHandler;
import mitm.common.util.RewindableInputStream;
import mitm.common.util.FileConstants;
import mitm.common.util.LimitReachedException;
import mitm.common.util.SizeUtils;
import mitm.test.TestUtils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Martijn Brinkers
 *
 */
public class XHTMLTextExtractorTest
{
    private static List<ExtractedPart> textParts = new LinkedList<ExtractedPart>();

    private static List<ExtractedPart> attachmentParts = new LinkedList<ExtractedPart>();

    private static List<RewindableInputStream> toClose = new LinkedList<RewindableInputStream>();
    
    private TextExtractorEventHandler handler = new TextExtractorEventHandlerImpl();
    
    private static int tempFileCount;

    private static class TextExtractorEventHandlerImpl implements TextExtractorEventHandler
    {
        @Override
        public void textEvent(ExtractedPart textPart)
        throws IOException
        {
            textParts.add(textPart);
        }

        @Override
        public void attachmentEvent(ExtractedPart attachmentPart)
        throws IOException
        {
            attachmentParts.add(attachmentPart);
        }
    }
    
    private void cleanParts(List<ExtractedPart> parts)
    throws IOException
    {
        for (ExtractedPart part : parts) {
            part.close();
        }
        
        parts.clear();
    }

    private void closeInputStreams(List<RewindableInputStream> streams)
    throws IOException
    {
        for (RewindableInputStream stream : streams) {
            IOUtils.closeQuietly(stream);
        }
        
        streams.clear();
    }
    
    private static RewindableInputStream readDocument(String filename)
    throws FileNotFoundException
    {
        RewindableInputStream stream = new RewindableInputStream(
                new BufferedInputStream(new FileInputStream(new File("test/resources/testdata/documents", filename))),
                SizeUtils.MB * 1);
        
        toClose.add(stream);
        
        return stream;
    }
 
    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        BasicConfigurator.configure();
    }
    
    @Before
    public void before() {
        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");
    }
    
    @After
    public void after()
    throws IOException
    {
        cleanParts(textParts);
        cleanParts(attachmentParts);
        closeInputStreams(toClose);
        
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    @Test
    public void testHTMLBigPreamble()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
        
        TextExtractorContext context = new TextExtractorContextImpl();
        
        context.setName("big-preamble.html");
        
        extractor.extract(readDocument("big-preamble.html"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertEquals("big-preamble.html", part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);

        assertTrue(text.contains("function fillListToGet"));
        assertTrue(text.contains("аренде недвижимости: Аренда квартир и комнат Аренда нежилых"));
    }
    
    @Test
    public void testHTMLUTF8()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
        
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("testHTML_utf8.html"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);

        assertTrue(text.contains("Title : Tilte with UTF-8 chars öäå"));
        assertTrue(text.contains("åäö"));
    }

    @Test
    public void testHTMLWindows1251EncodedNoMetaCharsetEncodingInContext()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
        
        TextExtractorContext context = new TextExtractorContextImpl();
        
        context.setEncoding("windows-1251");
        
        extractor.extract(readDocument("windows-1251-encoded-no-meta-charset.html"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);
        
        assertTrue(text.contains("ИНТЕРНЕТ-МАГАЗИН НОВОГОДНИХ ЕЛОК"));
        assertTrue(text.contains("аренде недвижимости: Аренда квартир и комнат Аренда нежилых"));
    }
    
    /*
     * Same as above but now no Encoding in context. Encoding is auto detected.
     */
    @Test
    public void testHTMLWindows1251EncodedNoMetaCharset()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
        
        assertTrue(extractor.isAutoDetectEncoding());
        
        assertEquals(20, extractor.getAutoDetectConfidence());
        
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("windows-1251-encoded-no-meta-charset.html"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);
        
        assertTrue(text.contains("ИНТЕРНЕТ-МАГАЗИН НОВОГОДНИХ ЕЛОК"));
        assertTrue(text.contains("аренде недвижимости: Аренда квартир и комнат Аренда нежилых"));
    }
    
    @Test
    public void testXML()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
                
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("test.xml"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);

        assertTrue(text.contains("UTF-16BE BOM UTF-16LE BOM UTF-8 BOM TIKA-85: http://www.apache.org/dev/svn-eol-style.txt"));
        assertTrue(text.contains("C = Compressed POSIX tar archive =============="));
        assertTrue(text.contains("Cooltalk Audio"));
    }    
    
    @Test(expected = LimitReachedException.class)
    public void testExceedMaxPart()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, 10);
                
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("test.xml"), context, handler);     
    }

    @Test
    public void testNullFile()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, 10);
                
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("null-file.xml"), context, handler);     

        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());

        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);
        
        assertEquals("", text);
    }
    
    /*
     * Test for a bug in TagSoup (should be fixed with the patched version)
     */
    @Test
    public void testTagSoup()
    throws Exception
    {
        XHTMLTextExtractor extractor = new XHTMLTextExtractor(1, Integer.MAX_VALUE);
        
        assertTrue(extractor.isAutoDetectEncoding());
        
        TextExtractorContext context = new TextExtractorContextImpl();
        
        extractor.extract(readDocument("test-tagsoup.html"), context, handler);     
        
        assertEquals(1, textParts.size());
        assertEquals(0, attachmentParts.size());
        
        ExtractedPart part = textParts.get(0);
        assertNull(part.getContext().getName());

        String text = IOUtils.toString(part.getContent(), CharEncoding.UTF_8);

        assertTrue(text.contains("Honors NIH Contributions"));
        assertTrue(text.contains("Up to Top"));
    }
}
