/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RateCounterImplTest
{
    private static RateCounter rateCounter;
	
    @BeforeClass
    public static void beforeClass()
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
	@Before
	public void before()
	{
		rateCounter = new RateCounterImpl();
		
		rateCounter.start();
	}
	
	@After
	public void after()
	{
		rateCounter.stop();
	}
	
	@Test
	public void testAdd()
	{
		rateCounter.addKey("key", "subKey", 100000);
		
		assertEquals(1, rateCounter.getItemCount("key"));

		rateCounter.addKey("key", "subKey", 100000);

		assertEquals(1, rateCounter.getItemCount("key"));
	}

	@Test
	public void testAddMultiple()
	{
		rateCounter.addKey("key", "subKey1", 100000);
		
		assertEquals(1, rateCounter.getItemCount("key"));

		rateCounter.addKey("key", "subKey2", 100000);

		assertEquals(2, rateCounter.getItemCount("key"));
	}

	@Test
	public void testUnknownKey()
	{
		assertEquals(0, rateCounter.getItemCount("unknown_key"));

		assertEquals(0, ((RateCounterImpl) rateCounter).getKeyMap().size());
	}

	@Test
	public void testLifetimeExceededInGet()
	throws InterruptedException
	{
		rateCounter.addKey("key", "subKey1", 50);
		rateCounter.addKey("key", "subKey2", 50);

		assertEquals(2, rateCounter.getItemCount("key"));
		
		Thread.sleep(100);
		
		assertEquals(0, rateCounter.getItemCount("key"));
		assertEquals(0, ((RateCounterImpl) rateCounter).getKeyMap().size());
	}

	@Test
	public void testLifetimeExceededThread()
	throws InterruptedException
	{
		((RateCounterImpl) rateCounter).setCheckDelay(100);
		
		rateCounter.addKey("key1", "subKey1", 50);
		rateCounter.addKey("key1", "subKey2", 50);
		rateCounter.addKey("key2", "subKey", 50);

		assertEquals(2, ((RateCounterImpl) rateCounter).getKeyMap().size());

		for (int i=0; i < 40; i++)
		{
			Thread.sleep(100);
			
			if (((RateCounterImpl) rateCounter).getKeyMap().size() == 0) {
				break;
			}
		}
		
		assertEquals(0, ((RateCounterImpl) rateCounter).getKeyMap().size());
	}
}
