/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class KeyCacheImplTest
{
    private KeyCacheImpl keyCache;
    
    @BeforeClass
    public static void beforeClass()
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }

    @After
    public void after()
    {
        if (keyCache != null) {
            keyCache.stop();
        }
        
        keyCache = null;
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor()
    {
        keyCache = new KeyCacheImpl(60000, 200, 100, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullKey()
    {
        keyCache = new KeyCacheImpl(1, 50, 100, 1);

        keyCache.add(null);
    }

    @Test
    public void testManyItems()
    throws Exception
    {
        final int low = 10000;
        final int high = 100000;
        
        keyCache = new KeyCacheImpl(60000, low, high, 1);
        
        for (int i = 0; i < 1234567; i++) {
            assertTrue(keyCache.add(Integer.toString(i)));
        }

        assertTrue(keyCache.size() >= low && keyCache.size() <= high);
        
        keyCache.clear();

        assertEquals(0, keyCache.size());
    }

    @Test
    public void testManyItemsEqualLowHigh()
    throws Exception
    {
        keyCache = new KeyCacheImpl(60000, 1000, 1000, 1);
        
        for (int i = 0; i < 100000; i++) {
            assertTrue(keyCache.add(Integer.toString(i)));
        }

        assertEquals(1000, keyCache.size());
    }
    
    @Test
    public void testTooManyItems()
    throws Exception
    {
        keyCache = new KeyCacheImpl(60000, 5, 10, 60000);
        
        for (int i = 0; i < 10; i++) {
            assertTrue(keyCache.add(Integer.toString(i)));
        }
        
        assertEquals(10, keyCache.size());

        assertFalse(keyCache.add("1"));
        
        assertEquals(10, keyCache.size());

        assertTrue(keyCache.add("11"));
        assertEquals(5, keyCache.size());
    }
    
    @Test
    public void testExpire()
    throws Exception
    {
        keyCache = new KeyCacheImpl(1, 50, 100, 1);
        
        keyCache.add("1");
        keyCache.add("2");
        keyCache.add("3");
        keyCache.add("4");
        keyCache.add("5", System.currentTimeMillis() + 5000);
        assertTrue(keyCache.contains("5"));
        
        Thread.sleep(200);

        assertEquals(1, keyCache.size());       
        assertTrue(keyCache.contains("5"));
    }
    
    @Test
    public void testNoExpire()
    {
        keyCache = new KeyCacheImpl(60000, 50, 100, 1);
        
        assertEquals(0, keyCache.size());       
        assertFalse(keyCache.contains("1"));
        assertTrue(keyCache.add("1"));
        assertFalse(keyCache.add("1"));
        assertTrue(keyCache.contains("1"));
        assertEquals(1, keyCache.size());       
        assertFalse(keyCache.contains("2"));
        assertTrue(keyCache.add("2"));
        assertEquals(2, keyCache.size());       
        assertTrue(keyCache.contains("2"));
    }
}
