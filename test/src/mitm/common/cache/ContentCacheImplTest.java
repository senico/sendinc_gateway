/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ContentCacheImplTest
{
    private static ContentCacheImpl cache;
	
    @BeforeClass
    public static void beforeClass()
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
	@Before
	public void before()
	{
		cache = new ContentCacheImpl(DateUtils.MILLIS_PER_MINUTE * 5);
		
		cache.start();
	}
	
	@After
	public void after()
	{
		cache.stop();
	}

	@Test
	public void testAddEntry() 
	throws IOException, CacheException
	{
		FileStreamCacheEntry entry = new FileStreamCacheEntry("body");
		
		assertFalse(entry.isValid());
		
		assertNull(entry.getFile(false));
		
		entry.setObject("test");
		
		String content = "Some content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		assertTrue(entry.isValid());
			
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		entry.writeTo(bos);
		
		String read = new String(bos.toByteArray(), "UTF-8");
		
		assertEquals(content, read);
		
		String key = "key";
		
		cache.addEntry(key, entry);
		
		entry = new FileStreamCacheEntry("other body");
		
		content = "Other content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		cache.addEntry(key, entry);
		
		List<CacheEntry> entries = cache.getAllEntries(key);
		
		assertEquals(2, entries.size());
		
		entry = (FileStreamCacheEntry) entries.get(0);
		assertEquals("body", entry.getId());
		assertEquals("test", entry.getObject());
		assertTrue(entry.getFile(false).exists());
		
		entry = (FileStreamCacheEntry) entries.get(1);
		assertEquals("other body", entry.getId());
		assertEquals(null, entry.getObject());
		assertTrue(entry.getFile(false).exists());
	}

	@Test
	public void testRemoveEntry() 
	throws IOException, CacheException
	{
		FileStreamCacheEntry entry = new FileStreamCacheEntry("body");
		
		assertFalse(entry.isValid());
		
		assertNull(entry.getFile(false));
		
		String content = "Some content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		assertTrue(entry.isValid());
			
		String key = "key";
		
		cache.addEntry(key, entry);
		
		entry = new FileStreamCacheEntry("other body");
		
		content = "Other content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		cache.addEntry(key, entry);

		List<CacheEntry> entries = cache.getAllEntries(key);
		
		assertEquals(2, entries.size());

		entry = (FileStreamCacheEntry) entries.get(0);
		assertEquals("body", entry.getId());
		
		File file = entry.getFile(false);
		assertTrue(file.exists());
		
		cache.removeEntry(key, "body");

		assertFalse(file.exists());
		
		entries = cache.getAllEntries(key);
		
		assertEquals(1, entries.size());
		
		entry = (FileStreamCacheEntry) entries.get(0);
		assertEquals("other body", entry.getId());
		assertEquals(null, entry.getObject());
		assertTrue(entry.getFile(false).exists());
	}

	@Test
	public void testStaleEntries() 
	throws IOException, CacheException, InterruptedException
	{
		cache.setMaxStaleTime(-1);
		
		FileStreamCacheEntry entry = new FileStreamCacheEntry("body");
		
		assertFalse(entry.isValid());
		
		assertNull(entry.getFile(false));
		
		String content = "Some content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		assertTrue(entry.isValid());

		File file1 = entry.getFile(false);
		
		assertTrue(file1.exists());
		
		String key = "key";
		
		cache.addEntry(key, entry);
		
		entry = new FileStreamCacheEntry("other body");
		
		content = "Other content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		File file2 = entry.getFile(false);
		
		assertTrue(file2.exists());
		
		cache.addEntry(key, entry);

		assertNotNull(cache.getEntry(key, entry.getId()));
		
        assertNotNull(cache.getWrapper(key, false));     
		
		cache.kick();		
		
		Thread.sleep(100);
		
		List<CacheEntry> entries = cache.getAllEntries(key);
		
		assertEquals(0, entries.size());
		
		assertFalse(file1.exists());
		assertFalse(file2.exists());
		
        assertNull(cache.getEntry(key, entry.getId()));		
        assertNull(cache.getWrapper(key, false));     
	}
	
	@Test
	public void testGetEntry() 
	throws IOException, CacheException
	{
		FileStreamCacheEntry entry = new FileStreamCacheEntry("body");
		
		assertFalse(entry.isValid());
		
		assertNull(entry.getFile(false));
		
		String content = "Some content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		assertTrue(entry.isValid());
			
		String key = "key";
		
		cache.addEntry(key, entry);
		
		entry = new FileStreamCacheEntry("other body");
		
		content = "Other content";
		
		entry.store(IOUtils.toInputStream(content, "UTF-8"));

		cache.addEntry(key, entry);

		entry = (FileStreamCacheEntry) cache.getEntry(key, "body");
		
		assertNotNull(entry);
		assertEquals("body", entry.getId());

		entry = (FileStreamCacheEntry) cache.getEntry(key, "other body");
		
		assertNotNull(entry);
		assertEquals("other body", entry.getId());

		entry = (FileStreamCacheEntry) cache.getEntry(key, "no entry");
		
		assertNull(entry);
	}
	
    @Test
    public void testRemoveAllEntries() 
    throws IOException, CacheException
    {
        FileStreamCacheEntry entry = new FileStreamCacheEntry("body");
        
        assertFalse(entry.isValid());
        
        assertNull(entry.getFile(false));
        
        String content = "Some content";
        
        entry.store(IOUtils.toInputStream(content, "UTF-8"));

        assertTrue(entry.isValid());
            
        String key = "key";
        
        cache.addEntry(key, entry);
        
        entry = new FileStreamCacheEntry("other body");
        
        content = "Other content";
        
        entry.store(IOUtils.toInputStream(content, "UTF-8"));

        cache.addEntry(key, entry);

        List<CacheEntry> entries = cache.getAllEntries(key);
        
        assertEquals(2, entries.size());

        assertNotNull(cache.getWrapper(key, false));     
        
        cache.removeAllEntries(key);
        
        assertNull(cache.getWrapper(key, false));     
    }	
}
