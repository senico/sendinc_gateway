/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.BeforeClass;
import org.junit.Test;


public class DatabaseActionExecutorImplTest
{
	private static final File hibernateConfig = new File("test/resources/hibernate.cfg.xml");

	private static DatabaseActionExecutor databaseActionExecutor;

	@BeforeClass
	public static void setUpBeforeClass() 
	throws Exception 
	{
		PropertyConfigurator.configure("conf/log4j.properties");

		HibernateSessionSource sessionSource = new StandardHibernateSessionSourceImpl(hibernateConfig);

		SessionManager sessionManager = new SessionManagerImpl(sessionSource);

		databaseActionExecutor = new DatabaseActionExecutorImpl(sessionManager);
	}

	@Test
	public void testRetry0()
	throws Exception
	{
		final MutableInt count = new MutableInt();
		
		final int retries = 0;
		
		try {
			databaseActionExecutor.executeTransaction(
		         new DatabaseAction<Integer>()
		         {
		            @Override
                    public Integer doAction(Session session)
		            throws DatabaseException
		            {	            	
			            count.increment();
	
			            throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
		            }
		        }, retries);
			
			fail();
		}
		catch(ConstraintViolationException e) {
			// ignore
		}
		
		assertEquals(1, count.intValue());
	}

	@Test
	public void testVoidRetry0()
	throws Exception
	{
		final MutableInt count = new MutableInt();
		
		final int retries = 0;
		
		try {
			databaseActionExecutor.executeTransaction(
		         new DatabaseVoidAction()
		         {
		             @Override
		             public void doAction(Session session)
		             throws DatabaseException
		             {	            	
		                 count.increment();

		                 throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
		             }
		        }, retries);
			
			fail();
		}
		catch(ConstraintViolationException e) {
			// ignore
		}
		
		assertEquals(1, count.intValue());
	}
		
	@Test
	public void testRetry()
	throws Exception
	{
		final MutableInt count = new MutableInt();
		
		final int retries = 3;
		
		Integer result = databaseActionExecutor.executeTransaction(
	         new DatabaseAction<Integer>()
	         {
	             @Override
	             public Integer doAction(Session session)
	             throws DatabaseException
	             {	            	
	                 count.increment();

	                 if (count.intValue() < retries) {
	                     throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
	                 }

	                 return count.toInteger();
	             }
	        }, retries);
		
		assertEquals(retries, result.intValue());
	}

	@Test
	public void testRetryEvent()
	throws Exception
	{
		final int retries = 3;
		
		final MutableInt eventCalled = new MutableInt();
		
		DatabaseActionRetryEvent retryEvent = new DatabaseActionRetryEvent()
		{
            @Override
			public void onRetry() {
				eventCalled.increment();
			}
		};

		try {
			databaseActionExecutor.executeTransaction(
		         new DatabaseAction<Integer>()
		         {
		             @Override
		             public Integer doAction(Session session)
		             throws DatabaseException
		             {	            	
		                 throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
		             }
		        }, retries, retryEvent);
			
			fail();
		}
		catch(ConstraintViolationException e) {
			// ignore
		}
		
		assertEquals(retries, eventCalled.intValue());
	}
	
	@Test(expected = ConstraintViolationException.class)
	public void testRetryFail()
	throws Exception
	{
		final int retries = 3;
		
		databaseActionExecutor.executeTransaction(
	         new DatabaseAction<Integer>()
	         {
	             @Override
	             public Integer doAction(Session session)
	             throws DatabaseException
	             {	            	
	                 throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
	             }
	        }, retries);
	}

	@Test
	public void testVoidActionRetry()
	throws Exception
	{
		final MutableInt count = new MutableInt();
		
		final int retries = 3;
		
		databaseActionExecutor.executeTransaction(
	         new DatabaseVoidAction()
	         {
	             @Override
	             public void doAction(Session session)
	             throws DatabaseException
	             {	            	
	                 count.increment();

	                 if (count.intValue() < retries) {
	                     throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
	                 }
	             }
	        }, retries);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testVoidActionRetryFail()
	throws Exception
	{
		final int retries = 3;
		
		databaseActionExecutor.executeTransaction(
	         new DatabaseVoidAction()
	         {
	             @Override
	             public void doAction(Session session)
	             throws DatabaseException
	             {	            	
	                 throw new ConstraintViolationException("Dummy ConstraintViolationException", null, "");
	             }
	        }, retries);
	}
}
