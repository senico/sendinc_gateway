/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.hibernate;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.security.cert.X509Certificate;

import mitm.common.util.MiscStringUtils;
import mitm.test.TestUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.SerializationUtils;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncodedCertificateTest
{
    private final static String SERIALIZED_CERTIFICATE =        
        "rO0ABXNyAChtaXRtLmNvbW1vbi5oaWJlcm5hdGUuRW5jb2RlZENlcnRpZmljYXRlsudlGbW18mYC"+
        "AAJMAA9jZXJ0aWZpY2F0ZVR5cGV0ABJMamF2YS9sYW5nL1N0cmluZztbABJlbmNvZGVkQ2VydGlm"+
        "aWNhdGV0AAJbQnhwdAAFWC41MDl1cgACW0Ks8xf4BghU4AIAAHhwAAADWjCCA1YwggK/oAMCAQIC"+
        "DlLcAAEAArK7dHSUAfR1MA0GCSqGSIb3DQEBBAUAMIG8MQswCQYDVQQGEwJERTEQMA4GA1UECBMH"+
        "SGFtYnVyZzEQMA4GA1UEBxMHSGFtYnVyZzE6MDgGA1UEChMxVEMgVHJ1c3RDZW50ZXIgZm9yIFNl"+
        "Y3VyaXR5IGluIERhdGEgTmV0d29ya3MgR21iSDEiMCAGA1UECxMZVEMgVHJ1c3RDZW50ZXIgQ2xh"+
        "c3MgMSBDQTEpMCcGCSqGSIb3DQEJARYaY2VydGlmaWNhdGVAdHJ1c3RjZW50ZXIuZGUwHhcNMDUw"+
        "OTI0MjAxOTQzWhcNMDYwOTI0MjAxOTQzWjBNMQswCQYDVQQGEwJOTDEZMBcGA1UEAxMQTWFydGlq"+
        "biBCcmlua2VyczEjMCEGCSqGSIb3DQEJARYUbS5icmlua2Vyc0Bwb2JveC5jb20wgZ8wDQYJKoZI"+
        "hvcNAQEBBQADgY0AMIGJAoGBANhdVm1jp1DDyeF/swrUtCxk+q1KkSoJZ2+ElTVVV+EWxljdqd0K"+
        "4Tth68BmjwVlrxhdpmC90tgdU+f/J2jB2Wa5K5UD2XuBMGKNywSMkgCRUHWZwg7OvMQcFyYpakaA"+
        "nzR3lfOoVUJFOKjd4OgoHZaw8qFGhT8wC8kb+/T/39grAgMBAAGjgcgwgcUwDAYDVR0TAQH/BAIw"+
        "ADAOBgNVHQ8BAf8EBAMCBeAwMwYJYIZIAYb4QgEIBCYWJGh0dHA6Ly93d3cudHJ1c3RjZW50ZXIu"+
        "ZGUvZ3VpZGVsaW5lczARBglghkgBhvhCAQEEBAMCBaAwXQYJYIZIAYb4QgEDBFAWTmh0dHBzOi8v"+
        "d3d3LnRydXN0Y2VudGVyLmRlL2NnaS1iaW4vY2hlY2stcmV2LmNnaS81MkRDMDAwMTAwMDJCMkJC"+
        "NzQ3NDk0MDFGNDc1PzANBgkqhkiG9w0BAQQFAAOBgQCLG+rS9ZwNK2cxH3/pUKZISMrXtHRphUq6"+
        "urd+E/dkJfO11utxHjC6yWZdP9ewdY2p3LYQG0s1f4fNl32JoyNiPeSfOidWcheh5PODd+0AO5gI"+
        "Ib/x5YYy+cfJEOyoDkOipm3CnytrG2JkYGd6u/+hAynlyyLvf3OArMKis1nZWw==";        
    
    private static X509Certificate certificate;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        certificate = TestUtils.loadCertificate(new File("test/resources/testdata/certificates/testcertificate.cer"));
    }
    
    
    @Test
    public void testSerialize()
    throws Exception 
    {
        byte[] serialized = SerializationUtils.serialize(new EncodedCertificate(certificate));
        
        EncodedCertificate deserialized = (EncodedCertificate) SerializationUtils.deserialize(serialized);
        
        assertEquals(certificate, deserialized.getCertificate());
    }

    @Test
    public void testDeserialize()
    throws Exception 
    {
        byte[] serialized = Base64.decodeBase64(MiscStringUtils.toAsciiBytes(SERIALIZED_CERTIFICATE));
        
        EncodedCertificate deserialized = (EncodedCertificate) SerializationUtils.deserialize(serialized);
        
        assertEquals(certificate, deserialized.getCertificate());
    }
}
