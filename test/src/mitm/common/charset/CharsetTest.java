/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLClassLoader;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mitm.common.mail.MailUtils;
import mitm.common.util.ClassLoaderUtils;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class CharsetTest
{
    private static final File testDir = new File("test/resources/testdata/mail");
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws IOException
    {
        // add the root to the system classloader to make sure the CharsetAliasProvider
        // is found. This is only required for testing.
        ClassLoaderUtils.addFile(new File("."), (URLClassLoader) CharsetTest.class.getClassLoader());

        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        File mail = new File(testDir, filename);

        MimeMessage message = MailUtils.loadMessage(mail);
        
        return message;
    }
    
    
    /*
     * Test if jutf7.jar can be found. jutf7 provides charset support for some missing charsets like
     * utf-7
     */
    @Test
    public void testUTF7Charset() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("utf7-charset.eml");
        
        String text = (String) message.getContent();
        
        assertNotNull(text);
        assertEquals("test 123", text.trim());
    }

    @Test
    public void testUnknownCharset() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("unknown-charset.eml");
        
        String text = (String) message.getContent();
        
        assertNotNull(text);
        assertTrue(text.trim().startsWith("* PGP"));
    }
    
    @Test
    public void testUnknownCharsetXXX() 
    throws MessagingException, IOException
    {
        MimeMessage message = loadMessage("unknown-charset-xxx.eml");
        
        String text = (String) message.getContent();
        
        assertNotNull(text);
        assertEquals("test", text.trim());
    }
}
