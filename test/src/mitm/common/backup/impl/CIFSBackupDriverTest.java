package mitm.common.backup.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import mitm.common.cifs.SMBFileParameters;
import mitm.common.cifs.StaticSMBFileParameters;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * NOTE: these test cases can only be run when a SMB server is running on the address
 * specified and allows files to be written. Because of this this test is not added 
 * to the test suites.
 * 
 * @author Martijn Brinkers
 *
 */
public class CIFSBackupDriverTest
{
    private final static String SERVER = "192.168.1.36";
    
    private final static String SHARE = "share";

    private final static String FILENAME = "test.txt";
    
    private final static String CONTENT = "test 123";
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws IOException
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }
    
    @Test
    public void testWriteBackup()
    throws Exception
    {
        SMBFileParameters parameters = new StaticSMBFileParameters();
        
        parameters.setServer(SERVER);
        parameters.setShare(SHARE);
        
        CIFSBackupDriver driver = new CIFSBackupDriver(parameters);
        
        InputStream data = IOUtils.toInputStream(CONTENT, "US-ASCII");

        driver.writeBackup(FILENAME, data);
    }
    
    @Test
    public void testIsValid()
    throws Exception
    {
        SMBFileParameters parameters = new StaticSMBFileParameters();
        
        parameters.setServer(SERVER);
        parameters.setShare(SHARE);
        
        CIFSBackupDriver driver = new CIFSBackupDriver(parameters);

        assertTrue(driver.isValid());
    }
}
