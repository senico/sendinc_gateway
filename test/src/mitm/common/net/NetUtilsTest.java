/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.net;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.Test;


public class NetUtilsTest
{
    @Test
    public void testParseQuery()
    throws Exception
    {
        Map<String, String[]> map;
        
        map = NetUtils.parseQuery("errorCode=-4&errorMessage=The+value+of+the+%27ap%27+argument+is+invalid%21");

        assertEquals(2, map.size());
        
        assertEquals("-4", map.get("errorcode")[0]);
        assertEquals("The value of the 'ap' argument is invalid!", map.get("errormessage")[0]);

        map = NetUtils.parseQuery("errorCode=-4&errorMessage=The+value+of+the+%27ap%27+argument+is+invalid%21", false);
        
        assertNull(map.get("errorcode"));
        assertEquals("-4", map.get("errorCode")[0]);
    }
    
    @Test
    public void testParseQuerySpaces()
    throws Exception
    {
        Map<String, String[]> map;
        
        map = NetUtils.parseQuery("errorCode  = -4  & & errorMessage =  The+value+of+the+%27ap%27+argument+is+invalid%21  ");

        assertEquals(2, map.size());
        
        assertEquals("-4", map.get("errorcode")[0]);
        assertEquals("The value of the 'ap' argument is invalid!", map.get("errormessage")[0]);
    }
    
    @Test
    public void testParseQueryNoValue()
    throws Exception
    {
        Map<String, String[]> map;
        
        map = NetUtils.parseQuery("errorCode=&&errorMessage");

        assertEquals(2, map.size());
        
        assertEquals("", map.get("errorcode")[0]);
        assertEquals("", map.get("errormessage")[0]);
    }

    @Test
    public void testParseEmptyQuery()
    throws Exception
    {
        Map<String, String[]> map;
        
        map = NetUtils.parseQuery(null);

        assertEquals(0, map.size());

        map = NetUtils.parseQuery("");

        assertEquals(0, map.size());
    }
    
}
