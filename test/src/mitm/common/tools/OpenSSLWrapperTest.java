/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.tools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import mitm.common.security.SecurityFactory;
import mitm.common.security.SecurityFactoryFactory;
import mitm.common.security.bouncycastle.InitializeBouncycastle;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class OpenSSLWrapperTest
{
    private static final File testDir = new File("test/resources/testdata/mail");
    private static final File tempDir = new File("test/tmp");
    
    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        
        keyStore = loadKeyStore(new File("test/resources/testdata/keys/testCertificates.p12"), "test");
    }
    
    private static KeyStore loadKeyStore(File file, String password) 
    throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, 
    CertificateException, FileNotFoundException, IOException 
    {        
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");
        
        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }
        
    @Test(timeout = 5000)
    public void testDecrypt() 
    throws Exception 
    {
        System.out.println("Starting testDecrypt");
        
        OpenSSLWrapper wrapper = new OpenSSLWrapper();

        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());
        
        PrivateKeyEntry entry = (PrivateKeyEntry) keyStore.getEntry("ValidCertificate", passwd);
        
        wrapper.setPrivateKey(entry.getPrivateKey());
        
        File encryptedFile = new File(testDir, "encrypted-validcertificate.eml");
        
        Process p = wrapper.decrypt(encryptedFile);
        
        InputStream result = p.getInputStream();
        
        String error = IOUtils.toString(p.getErrorStream(), "US-ASCII");
        
        System.out.println(error);
        
        assertEquals("", error.trim());
        
        File outputFile = new File(tempDir, "openssltestdecrypt.eml");
        
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        
        IOUtils.copy(result, outputStream);
        
        outputStream.close();
    }
    
    @Test(timeout = 5000)
    public void testVerify() 
    throws Exception 
    {
        System.out.println("Starting testVerify");
        
        OpenSSLWrapper wrapper = new OpenSSLWrapper();

        File signedFile = new File(testDir, "clear-signed-validcertificate.eml");
        
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
        
        wrapper.setCertificateAuthorities(certificate);
        
        Process p = wrapper.verify(signedFile);
        
        InputStream result = p.getInputStream();
        
        String error = IOUtils.toString(p.getErrorStream(), "US-ASCII");
        
        System.out.println(error);
        
        assertTrue(error.startsWith("Verification successful"));
        
        File outputFile = new File(tempDir, "openssltestverify.eml");
        
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        
        IOUtils.copy(result, outputStream);
        
        outputStream.close();
    }    
}
