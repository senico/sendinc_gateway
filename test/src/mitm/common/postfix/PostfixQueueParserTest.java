/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class PostfixQueueParserTest 
{
	private final static File base = new File("test/resources/testdata");

    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }

    @Test
    public void testParseWithFailure() 
    throws IOException
    {
        String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue-with-failure.txt"));
        
        PostfixQueueParser queueParser = new PostfixQueueParser();
        
        queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);

        assertEquals(2, queueParser.getQueueItems().size());
        
        PostfixQueueItem item = queueParser.getQueueItems().get(0);
        
        assertEquals("7E65E43815", item.getQueueID());
        assertEquals("(host postduif.ic.uva.nl[145.18.40.180]                  said: 451 4.3.0 <christine@m" + 
                "itm.mimesecure.com>... First-time sender tempfailed as anti-spam measure;                 " + 
                " please try again in 3 minutes (in reply to end of DATA command))", item.getFailure());
        assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
        
        item = queueParser.getQueueItems().get(1);
        
        assertEquals("7E65E43815", item.getQueueID());
        assertEquals("(host postduif.ic.uva.nl[145.18.40.180]                  said: 451 4.3.0 <christine" + 
                "@mitm.mimesecure.com>... First-time sender tempfailed as anti-spam measure;             " + 
                "     please try again in 3 minutes (in ()()()reply !@#$%^&*()_+ to end of DATA command))", item.getFailure());
        assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
    }
    
    @Test
    public void testParseMailerDaemonQueue() 
    throws IOException
    {
        String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue-mailer-deamon.txt"));
        
        PostfixQueueParser queueParser = new PostfixQueueParser();
        
        queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);

        assertEquals(1, queueParser.getQueueItems().size());
        
        PostfixQueueItem item = queueParser.getQueueItems().get(0);
        
        assertEquals("61216200B", item.getQueueID());
        assertNull(item.getFailure());
        assertEquals("MAILER-DAEMON", StringUtils.join(item.getRecipients(), ","));
    }
    
	@Test
	public void testParseNonMatchingLine() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue-non-matching-line.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
		
		assertEquals(2, queueParser.getQueueItems().size());
		
		PostfixQueueItem item = queueParser.getQueueItems().get(0);
		
		assertEquals("BB0922C87E7", item.getQueueID());
		assertEquals(PostfixQueueStatus.ACTIVE, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:21:14", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals(null, item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
		
		item = queueParser.getQueueItems().get(1);
		
		assertEquals(null, item.getQueueID());
		assertEquals(null, item.getQueueStatus());
		assertEquals(-1, item.getMessageSize());
		assertEquals(null, item.getArrivalTime());
		assertEquals(null, item.getSender());
		assertEquals("3DFB92C87E7      284 Wed 12:40:49                 connect to example.com[208.77.188.166]: " + 
				"Connection timed out                                          test2@example.com                " + 
				"                          test3@example.com", item.getFailure());
		assertEquals(null, StringUtils.join(item.getRecipients(), ","));
	}
 
	@Test
	public void testQueueLength() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		assertEquals(4, queueParser.getQueueLength(mailq));
	}

	@Test
	public void testQueueLengthEmptyQueue() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue-empty.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		assertEquals(0, queueParser.getQueueLength(mailq));
	}

	@Test
	public void testParse() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
		
		assertEquals(4, queueParser.getQueueItems().size());
		
		PostfixQueueItem item = queueParser.getQueueItems().get(0);
		
		assertEquals("BB0922C87E7", item.getQueueID());
		assertEquals(PostfixQueueStatus.ACTIVE, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:21:14", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals(null, item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
		
		item = queueParser.getQueueItems().get(1);
		
		assertEquals("D97342C87E5", item.getQueueID());
		assertEquals(PostfixQueueStatus.DEFERRED, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:16:50", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
	
		item = queueParser.getQueueItems().get(2);
		
		assertEquals("365732C87E4", item.getQueueID());
		assertEquals(PostfixQueueStatus.HOLD, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:06:37", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
		
		item = queueParser.getQueueItems().get(3);
		
		assertEquals("3DFB92C87E7", item.getQueueID());
		assertEquals(PostfixQueueStatus.DEFERRED, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Wed Aug 20 12:40:49", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test2@example.com,test3@example.com", StringUtils.join(item.getRecipients(), ","));
	}
	
	@Test
	public void testParseMaxItems() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		List<PostfixQueueItem> items = queueParser.parseQueue(mailq, 0, 2);
		
		assertEquals(2, items.size());
		
		PostfixQueueItem item = queueParser.getQueueItems().get(0);
		
		assertEquals("BB0922C87E7", item.getQueueID());
		assertEquals(PostfixQueueStatus.ACTIVE, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:21:14", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals(null, item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
		
		item = queueParser.getQueueItems().get(1);
		
		assertEquals("D97342C87E5", item.getQueueID());
		assertEquals(PostfixQueueStatus.DEFERRED, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:16:50", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
	}
	
	@Test
	public void testParseEmptyQueue() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue-empty.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
		
		assertEquals(0, queueParser.getQueueItems().size());
	}
	
	@Test
	public void testParseSubPart() 
	throws IOException
	{
		String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
		
		PostfixQueueParser queueParser = new PostfixQueueParser();
		
		queueParser.parseQueue(mailq, 2, 2);
		
		assertEquals(2, queueParser.getQueueItems().size());
		
		PostfixQueueItem item = queueParser.getQueueItems().get(0);
		
		assertEquals("365732C87E4", item.getQueueID());
		assertEquals(PostfixQueueStatus.HOLD, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Tue Aug 19 19:06:37", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test@example.com", StringUtils.join(item.getRecipients(), ","));
		
		item = queueParser.getQueueItems().get(1);
		
		assertEquals("3DFB92C87E7", item.getQueueID());
		assertEquals(PostfixQueueStatus.DEFERRED, item.getQueueStatus());
		assertEquals(284, item.getMessageSize());
		assertEquals("Wed Aug 20 12:40:49", item.getArrivalTime());
		assertEquals("martijn@xps", item.getSender());
		assertEquals("(connect to example.com[208.77.188.166]: Connection timed out)", item.getFailure());
		assertEquals("test2@example.com,test3@example.com", StringUtils.join(item.getRecipients(), ","));
	}

	@Test
	public void testQueueItemWithRoundBrackets()
	{
		Matcher matcher= PostfixQueueParser.QUEUE_PATTERN.matcher(
				"7E65E43815 4246 Wed Sep 10 21:55:48 christine@mitm.mimesecure.com (host postduif.ic.uva.nl[145.18.40.180]" + 
				" said: 451 4.3.0 <christine@mitm.mimesecure.com>... First-time sender tempfailed as anti-spam measure;" + 
				" please try again in 3 minutes (in reply to end of DATA command)) test@example.com");
		
		assertTrue(matcher.matches());

		matcher= PostfixQueueParser.QUEUE_PATTERN.matcher(
				"7E65E43815 4246 Wed Sep 10 21:55:48 christine@mitm.mimesecure.com (host postduif.ic.uva.nl[145.18.40.180]" + 
				" said: 451 4.3.0 <christine@mitm.mimesecure.com>... First-time sender tempfailed as anti-spam measure;" + 
				" please try again in 3 minutes (in ()()()reply !@#$%^&*()_+ to end of DATA command)) test@example.com");
		
		assertTrue(matcher.matches());
	}
	
    @Test
    public void testSearchPattern() 
    throws IOException
    {
        String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
        
        PostfixQueueParser queueParser = new PostfixQueueParser(Pattern.compile("martijn@xps"));
        
        queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
        
        assertEquals(4, queueParser.getQueueItems().size());
        
        PostfixQueueItem item = queueParser.getQueueItems().get(0);        
        assertEquals("BB0922C87E7", item.getQueueID());
        
        item = queueParser.getQueueItems().get(1);
        assertEquals("D97342C87E5", item.getQueueID());
    
        item = queueParser.getQueueItems().get(2);
        assertEquals("365732C87E4", item.getQueueID());
        
        item = queueParser.getQueueItems().get(3);
        assertEquals("3DFB92C87E7", item.getQueueID());
        
        queueParser = new PostfixQueueParser(Pattern.compile("test2@example.com"));
        
        queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
        
        assertEquals(1, queueParser.getQueueItems().size());
        
        item = queueParser.getQueueItems().get(0);
        assertEquals("3DFB92C87E7", item.getQueueID());        
    }
    
    @Test
    public void testSearchPatternNoMatch() 
    throws IOException
    {
        String mailq = FileUtils.readFileToString(new File(base, "postfix/postfix-queue.txt"));
        
        PostfixQueueParser queueParser = new PostfixQueueParser(Pattern.compile("NO MATCH"));
        
        queueParser.parseQueue(mailq, 0, Integer.MAX_VALUE);
        
        assertEquals(0, queueParser.getQueueItems().size());
    }       
}
