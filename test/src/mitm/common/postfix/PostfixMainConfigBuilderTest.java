/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import mitm.common.util.CollectionUtils;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class PostfixMainConfigBuilderTest
{
	private final static File baseDir = new File("test/resources/testdata/postfix");
	private final static File tempDir = new File("test/tmp");
	
    @Test
    public void testBuildConfigWithSubdomainParam() 
    throws IOException
    {
        File inputFile = new File(baseDir, "main_subdomain.cf");
        
        FileReader reader = new FileReader(inputFile);
        
        File outputFile = new File(tempDir, "main_subdomain.cf.out");
        
        FileWriter writer = new FileWriter(outputFile);
        
        PostfixMainConfigBuilder builder = new PostfixMainConfigBuilder(reader);
        
        assertTrue(builder.isMatchSubdomains());
        assertEquals("hostname.example.com", builder.getMyHostname().trim());
        assertEquals(2, builder.getMyDestinations().size());
        assertEquals("s1.example.com", new ArrayList<String>(builder.getMyDestinations()).get(0).trim());
        assertEquals("s2.example.com", new ArrayList<String>(builder.getMyDestinations()).get(1).trim());
        assertEquals(2, builder.getMyNetworks().size());
        assertEquals("127.0.0.0/24", new ArrayList<String>(builder.getMyNetworks()).get(0).trim());
        assertEquals("192.168.1.1/32", new ArrayList<String>(builder.getMyNetworks()).get(1).trim());
        assertEquals("relay.example.com", builder.getRelayHost());
        assertTrue(builder.isRelayHostMxLookup());
        /* we need to cast to long to prevent ambigous error */
        assertEquals(2525, (long) builder.getRelayHostPort());
        assertEquals(2, builder.getRelayDomains().size());
        assertEquals("backup.example.com", new ArrayList<String>(builder.getRelayDomains()).get(0).trim());
        assertEquals("backup2.example.com", new ArrayList<String>(builder.getRelayDomains()).get(1).trim());
        /* we need to cast to long to prevent ambigous error */
        assertEquals(123, (long) builder.getBeforeFilterMessageSizeLimit());
        assertEquals(456, (long) builder.getAfterFilterMessageSizeLimit());
        assertEquals(789, (long) builder.getMailboxSizeLimit());
        assertEquals("some helo name", builder.getSMTPHeloName());
        assertEquals("somehost.com", builder.getRelayTransportHost());
        assertTrue(builder.isRelayTransportHostMxLookup());
        assertEquals(2526, (long) builder.getRelayTransportHostPort());
        assertTrue(builder.isRejectUnverifiedRecipient());
        assertEquals("123", builder.getUnverifiedRecipientRejectCode());
                
        builder.setMatchSubdomains(false);
        assertFalse(builder.isMatchSubdomains());
        
        builder.writeConfig(writer);
        
        writer.close();
        
        reader = new FileReader(outputFile);

        builder = new PostfixMainConfigBuilder(reader);
        assertFalse(builder.isMatchSubdomains());
        
        builder.setMatchSubdomains(true);
        assertTrue(builder.isMatchSubdomains());
        
        writer = new FileWriter(outputFile);
        
        builder.writeConfig(writer);
        
        writer.close();
        
        reader = new FileReader(outputFile);

        builder = new PostfixMainConfigBuilder(reader);
        assertTrue(builder.isMatchSubdomains());
    }
	
	@Test
	public void testBuildConfigNoChange() 
	throws IOException
	{
		File inputFile = new File(baseDir, "main.cf");
		
		FileReader reader = new FileReader(inputFile);
		
		File outputFile = new File(tempDir, "main.cf.out");
		
		FileWriter writer = new FileWriter(outputFile);
		
		PostfixMainConfigBuilder builder = new PostfixMainConfigBuilder(reader);
		
		assertEquals("hostname.example.com", builder.getMyHostname().trim());
		assertEquals(2, builder.getMyDestinations().size());
		assertEquals("s1.example.com", new ArrayList<String>(builder.getMyDestinations()).get(0).trim());
		assertEquals("s2.example.com", new ArrayList<String>(builder.getMyDestinations()).get(1).trim());
		assertEquals(2, builder.getMyNetworks().size());
		assertEquals("127.0.0.0/24", new ArrayList<String>(builder.getMyNetworks()).get(0).trim());
		assertEquals("192.168.1.1/32", new ArrayList<String>(builder.getMyNetworks()).get(1).trim());
		assertEquals("relay.example.com", builder.getRelayHost());
		assertTrue(builder.isRelayHostMxLookup());
		/* we need to cast to long to prevent ambigous error */
		assertEquals(2525, (long) builder.getRelayHostPort());
		assertEquals(2, builder.getRelayDomains().size());
		assertEquals("backup.example.com", new ArrayList<String>(builder.getRelayDomains()).get(0).trim());
		assertEquals("backup2.example.com", new ArrayList<String>(builder.getRelayDomains()).get(1).trim());
		/* we need to cast to long to prevent ambigous error */
		assertEquals(123, (long) builder.getBeforeFilterMessageSizeLimit());
		assertEquals(456, (long) builder.getAfterFilterMessageSizeLimit());
		assertEquals(789, (long) builder.getMailboxSizeLimit());
		assertEquals("some helo name", builder.getSMTPHeloName());
		assertEquals("somehost.com", builder.getRelayTransportHost());
		assertTrue(builder.isRelayTransportHostMxLookup());
		assertEquals(2526, (long) builder.getRelayTransportHostPort());
		assertTrue(builder.isRejectUnverifiedRecipient());
		assertEquals("123", builder.getUnverifiedRecipientRejectCode());
				
		builder.writeConfig(writer);
		
		writer.close();
		
		assertTrue(FileUtils.contentEquals(inputFile, outputFile));
	}
	
	@Test
	public void testBuildConfigChangeParams() 
	throws IOException
	{
		File inputFile = new File(baseDir, "main.cf");
		
		FileReader reader = new FileReader(inputFile);
		
		File outputFile = new File(tempDir, "main.cf.out");
		
		FileWriter writer = new FileWriter(outputFile);
		
		PostfixMainConfigBuilder builder = new PostfixMainConfigBuilder(reader);

		builder.setMyHostname("test.example.com");
		builder.setMyDestination(CollectionUtils.asLinkedHashSet(new String[]{"abc", "def"}));
		builder.setMyNetworks(CollectionUtils.asLinkedHashSet(new String[]{"172.0.0.0/8", "192.168.1.1/32"}));
		builder.setRelayHost("somerelay.example.com");
		builder.setRelayHostMxLookup(false);
		builder.setRelayHostPort(987);
		builder.setRelayDomains(CollectionUtils.asLinkedHashSet(new String[]{"relay1", "relay2"}));
		builder.setBeforeFilterMessageSizeLimit(1);
		builder.setAfterFilterMessageSizeLimit(2);
		builder.setMailboxSizeLimit(3);
		builder.setSMTPHeloName("hallo");
		builder.setRelayTransportHost("some relay host");
		builder.setRelayTransportHostMxLookup(false);
		builder.setRelayTransportHostPort(999);
		builder.setRejectUnverifiedRecipient(false);
		builder.setUnverifiedRecipientRejectCode("543");
		
		builder.writeConfig(writer);
		
		writer.close();
		
		reader = new FileReader(outputFile);

		builder = new PostfixMainConfigBuilder(reader);
		
		assertEquals("test.example.com", builder.getMyHostname().trim());
		assertEquals(2, builder.getMyDestinations().size());
		assertEquals("abc", new ArrayList<String>(builder.getMyDestinations()).get(0).trim());
		assertEquals("def", new ArrayList<String>(builder.getMyDestinations()).get(1).trim());
		assertEquals(2, builder.getMyNetworks().size());
		assertEquals("172.0.0.0/8", new ArrayList<String>(builder.getMyNetworks()).get(0).trim());
		assertEquals("192.168.1.1/32", new ArrayList<String>(builder.getMyNetworks()).get(1).trim());
		assertEquals("somerelay.example.com", builder.getRelayHost());
		assertFalse(builder.isRelayHostMxLookup());
		/* we need to cast to long to prevent ambigous error */
		assertEquals(987, (long) builder.getRelayHostPort());
		assertEquals(2, builder.getRelayDomains().size());
		assertEquals("relay1", new ArrayList<String>(builder.getRelayDomains()).get(0).trim());
		assertEquals("relay2", new ArrayList<String>(builder.getRelayDomains()).get(1).trim());
		assertEquals(1, (long) builder.getBeforeFilterMessageSizeLimit());
		assertEquals(2, (long) builder.getAfterFilterMessageSizeLimit());
		assertEquals(3, (long) builder.getMailboxSizeLimit());
		assertEquals("hallo", builder.getSMTPHeloName());
		assertEquals("some relay host", builder.getRelayTransportHost());
		assertFalse(builder.isRelayTransportHostMxLookup());
		/* we need to cast to long to prevent ambigous error */
		assertEquals(999, (long) builder.getRelayTransportHostPort());
		assertFalse(builder.isRejectUnverifiedRecipient());
		assertEquals("543", builder.getUnverifiedRecipientRejectCode());		
	}	
}
