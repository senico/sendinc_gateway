/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

public class PostfixLogParserTest
{
    private static final File testBase = new File("test/resources/testdata/");
    
    private final static void logMatcher(Matcher m)
    {
        for (int i = 0; i <= m.groupCount(); i++)
        {
            System.out.println("i: " + i + ". Group: " + m.group(i));
        }
        
        System.out.println("---------------------");
    }
    
    @BeforeClass
    public static void setUpBeforeClass() 
    throws Exception 
    {
        PropertyConfigurator.configure("conf/log4j.properties");
    }

    /*
     * If an item is removed from Postfix with postsuper -d sometimes the queue reports that the queue is corrupt. This
     * is not a problem but it changes the order of some log items (a remove comes before a final delivery).
     */
    @Test
    public void testQueueCorrupt()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/qmgr_active_corrupt.txt");
    	
    	Reader reader = new FileReader(file);
    	
    	PostfixLogParser parser = new PostfixLogParser();
    	
    	List<PostfixLogItem> items = parser.getLogItems(reader, 13, Integer.MAX_VALUE);

    	assertEquals(1, items.size());
    	assertEquals("8587A56C0AA", items.get(0).getQueueID());
    }    
    
    @Test
    public void testGetLogItems()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser();
        
        List<PostfixLogItem> items = parser.getLogItems(new FileReader(file), 0, Integer.MAX_VALUE);
        
        assertEquals(282, items.size());
        
        LineNumberReader reader = new LineNumberReader(new FileReader(file));
        
        for (PostfixLogItem logItem : items)
        {
            for (String line : logItem.getLines())
            {
                String actual = reader.readLine();
                
                assertNotNull(actual);
                assertNotNull(line);
            }
        }
        
        assertNull(reader.readLine());
    }

    @Test
    public void testGetLogItemsWithStartAndMax()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser();
        
        List<PostfixLogItem> items = parser.getLogItems(new FileReader(file), 7 , 5);
        
        assertEquals(5, items.size());

        assertEquals(1, items.get(0).getLines().size());
        assertEquals("Aug 25 11:18:22 ubuntu postfix/master[6590]: reload configuration /etc/postfix", 
                items.get(0).getLines().get(0));
        assertEquals(5, items.get(1).getLines().size());
        assertEquals("Aug 25 20:00:25 ubuntu postfix/pickup[31727]: B734856C0A9: uid=1000 from=<martijn>", 
                items.get(1).getLines().get(0));
        assertEquals("Aug 25 20:00:25 ubuntu postfix/cleanup[32007]: B734856C0A9: message-id=<20080825180025.B734856C0A9@mail.mimesecure.com>", 
                items.get(1).getLines().get(1));
        assertEquals("Aug 25 20:00:36 ubuntu postfix/qmgr[7118]: B734856C0A9: removed", 
                items.get(1).getLines().get(4));
        
    }
    
    @Test
    public void testLogFileCount()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser();
        
        int count = parser.getLogCount(new FileReader(file));
        
        assertEquals(282, count);
    }
    
    @Test
    public void testLogPattern()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 27 10:29:56 ubuntu postfix/smtpd[22123]: connect from localhost[127.0.0.1]");
        
        assertFalse(m.matches());
    }

    @Test
    public void testLogPattern2()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 26 15:50:08 ubuntu postfix/qmgr[15618]: 8DD6156C0A9: removed");
        
        assertTrue(m.matches());
        
        logMatcher(m);

        assertEquals(4, m.groupCount());
        assertEquals("Aug 26 15:50:08", m.group(1));
        assertEquals("qmgr", m.group(2));
        assertEquals("8DD6156C0A9", m.group(3));
        assertEquals("removed", m.group(4));
    }
    
    @Test
    public void testLogPattern3()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 26 15:50:08 ubuntu postfix/smtp[15793]: 8DD6156C0A9: to=<test" + 
                "@example.com>, relay=mail.mitm.nl[195.20.9.69]:25, delay=2.4, delays=0.01/0.01/0.16/2.2, dsn=2.0.0, status=sent " + 
                "(250 2.0.0 m7QDo4O6031249 Message accepted for delivery)");
        
        assertTrue(m.matches());
        
        logMatcher(m);

        assertEquals(4, m.groupCount());
        assertEquals("Aug 26 15:50:08", m.group(1));
        assertEquals("smtp", m.group(2));
        assertEquals("8DD6156C0A9", m.group(3));
        assertEquals("to=<test@example.com>, relay=mail.mitm.nl[195.20.9.69]:25, delay=2.4, delays=0.01/0.01/0.16/2.2, dsn=2.0.0, " + 
                "status=sent (250 2.0.0 m7QDo4O6031249 Message accepted for delivery)", 
                m.group(4));
    }
    
    @Test
    public void testLogPattern4()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 26 15:47:25 ubuntu postfix/smtpd[15693]: disconnect from localhost[127.0.0.1]");
        
        assertFalse(m.matches());
    }

    @Test
    public void testLogPattern5()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 27 10:37:10 ubuntu postfix/smtpd[22183]: D9D1356C0A9: client=localhost[127.0.0.1]");
        
        assertTrue(m.matches());
        
        logMatcher(m);
        
        assertEquals(4, m.groupCount());
        assertEquals("Aug 27 10:37:10", m.group(1));
        assertEquals("smtpd", m.group(2));
        assertEquals("D9D1356C0A9", m.group(3));
        assertEquals("client=localhost[127.0.0.1]", m.group(4));
    }
    
    @Test
    public void testLogPattern6()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Aug 27 22:57:05 ubuntu postfix/postsuper[3852]: Deleted: 1 message");
        
        assertFalse(m.matches());
    }
    
    @Test
    public void testLogPattern7()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_PATTERN.matcher("Sep 9 09:16:43 ubuntu postfix/smtp[17076]: B169856C0AA: to=<test@example.com>, relay=none, delay=30, delays=0.03/0.01/30/0, dsn=4.4.1, status=deferred (connect to example.com[208.77.188.166]: Connection timed out)");
        
        assertTrue(m.matches());
        
        logMatcher(m);
        
        assertEquals(4, m.groupCount());
        assertEquals("Sep 9 09:16:43", m.group(1));
        assertEquals("smtp", m.group(2));
        assertEquals("B169856C0AA", m.group(3));
        assertEquals("to=<test@example.com>, relay=none, delay=30, delays=0.03/0.01/30/0, dsn=4.4.1, status=deferred (connect to example.com[208.77.188.166]: Connection timed out)", m.group(4));
    }

    @Test
    public void testCorruptQueue()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_CORRUPT_ACTIVE_QUEUE_PATTERN.matcher(
        		"Sep  9 09:20:30 ubuntu postfix/qmgr[7633]: warning: qmgr_active_corrupt: save corrupt file queue active id D807856C0AE: No such file or directory");
        
        assertTrue(m.matches());
        
        logMatcher(m);
        
        assertEquals(4, m.groupCount());
        assertEquals("Sep  9 09:20:30", m.group(1));
        assertEquals("qmgr", m.group(2));
        assertEquals("D807856C0AE", m.group(3));
        assertEquals("No such file or directory", m.group(4));
    }

    @Test
    public void testCorruptQueue2()
    {
        Matcher m = PostfixLogParser.POSTFIX_LOG_CORRUPT_ACTIVE_QUEUE_PATTERN.matcher(
        		"Sep  8 16:10:47 ubuntu postfix/qmgr[7633]: warning: qmgr_active_done_3_generic: remove E6EB456C0AA from active: No such file or directory");
        
        assertTrue(m.matches());
        
        logMatcher(m);
        
        assertEquals(4, m.groupCount());
        assertEquals("Sep  8 16:10:47", m.group(1));
        assertEquals("qmgr", m.group(2));
        assertEquals("E6EB456C0AA", m.group(3));
        assertEquals("from active: No such file or directory", m.group(4));
    }

    @Test
    public void testGetRawLogItems()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser();
        
        List<String> items = parser.getRawLogItems(new FileReader(file), 0, Integer.MAX_VALUE);
        
        assertEquals(450, items.size());
    }

    @Test
    public void testGetRawLogCount()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser();
        
        assertEquals(450, parser.getRawLogCount(new FileReader(file)));
    }

    @Test
    public void testGetRawLogItemsWithPattern()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser(Pattern.compile("B734856C0A9"));
        
        List<String> items = parser.getRawLogItems(new FileReader(file), 0, Integer.MAX_VALUE);
        
        assertEquals(5, items.size());
    }

    @Test
    public void testGetRawLogCountWithPattern()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser(Pattern.compile("B734856C0A9"));
        
        assertEquals(5, parser.getRawLogCount(new FileReader(file)));
    }

    @Test
    public void testGetRawLogItemsWithPatternNoMatch()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser(Pattern.compile("NO_MATCH"));
        
        List<String> items = parser.getRawLogItems(new FileReader(file), 0, Integer.MAX_VALUE);
        
        assertEquals(0, items.size());
    }

    @Test
    public void testGetRawLogCountWithPatternNoMatch()
    throws FileNotFoundException, IOException
    {
        File file = new File(testBase, "postfix/mail.info");
        
        PostfixLogParser parser = new PostfixLogParser(Pattern.compile("NO_MATCH"));
        
        assertEquals(0, parser.getRawLogCount(new FileReader(file)));
    }

//  @Test
//  public void testLargeLogFile()
//  throws FileNotFoundException, IOException
//  {
//	  File file = new File(testBase, "postfix/mail.info.large");
//
//      final int tries = 5;
//      
//      long start = System.currentTimeMillis();
//      
//      for (int i = 0; i < tries; i++) 
//      {
//          PostfixLogParser parser = new PostfixLogParser();
//
//          System.out.println(parser.getLogCount(new FileReader(file)));
//      }
//
//      double timeSpent = (System.currentTimeMillis() - start) * 0.001 / tries;
//      
//      System.out.println("Seconds / check: " + timeSpent);
//  }
}
