/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;
import java.util.LinkedHashMap;

import org.junit.Test;

public class PostfixMainConfigParserTest
{
	@Test
	public void testConfigParser()
	{
		StringReader config = new StringReader("name1 = value\nname2=value\n continue=123\n continue2\n# test\n\naa\n\t  #xx=1\n#b=1x\n123 =");
		
		LinkedHashMap<String, String> values = PostfixMainConfigParser.parse(config);
		
		assertEquals(8, values.size());
		assertEquals("value", values.get("name1"));
		assertEquals("value\n continue=123\n continue2", values.get("name2"));
		assertEquals("# test", values.get("other_0"));
		assertEquals("", values.get("other_1"));
		assertEquals("aa", values.get("other_2"));
		assertEquals("\t  #xx=1", values.get("other_3"));
		assertEquals("#b=1x", values.get("other_4"));
		assertEquals("", values.get("123"));
	}
}
