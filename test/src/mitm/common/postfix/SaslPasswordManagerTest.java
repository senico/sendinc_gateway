/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.postfix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class SaslPasswordManagerTest
{
    @Test
    public void testCreateContentNullTest()
    {
        String content = SaslPasswordManager.createContent(null);
        
        assertEquals("", content);
    }
    
    @Test
    public void testParseLocalhost()
    {
        String input = " localhost : 578  test@gmail.com : sec:ret! ";
        
        List<SaslPassword> passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        SaslPassword password = passwords.get(0);
        assertEquals("localhost", password.getServer());
        assertEquals((Integer) 578, password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test@gmail.com", password.getUsername());
        assertEquals("sec:ret!", password.getPassword());
        
        
        input = " [ localhost  ] test@gmail.com";
        
        passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        password = passwords.get(0);
        assertEquals("localhost", password.getServer());
        assertNull(password.getPort());
        assertFalse(password.isMxLookup());
        assertEquals("test@gmail.com", password.getUsername());
        assertEquals("", password.getPassword());
    }
    
    @Test
    public void testCreateContent()
    {
        String input = " smtp.gmail.com : 578  test@gmail.com : sec:ret! \n\n\nxx\n" + 
        " [ smtp.gmail.com ] :  test@g mail.com :";

        List<SaslPassword> passwords = SaslPasswordManager.parseContent(input);
        
        String content = SaslPasswordManager.createContent(passwords);
        
        assertNotNull(content);
        
        System.out.println(content);
        
        passwords = SaslPasswordManager.parseContent(content);        

        SaslPassword password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertEquals((Integer) 578, password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test@gmail.com", password.getUsername());
        assertEquals("sec:ret!", password.getPassword());
        
        password = passwords.get(1);
        assertEquals("smtp.gmail.com", password.getServer());
        assertNull(password.getPort());
        assertFalse(password.isMxLookup());
        assertEquals("test@g mail.com", password.getUsername());
        assertEquals("", password.getPassword());        
    }
    
    @Test
    public void testParseMultiLine()
    {
        String input = " smtp.gmail.com : 578  test@gmail.com : sec:ret! \n\n\nxx\n" + 
                " [ smtp.gmail.com ] : 25 test@g mail.com :";

        List<SaslPassword> passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(2, passwords.size());
        
        SaslPassword password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertEquals((Integer) 578, password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test@gmail.com", password.getUsername());
        assertEquals("sec:ret!", password.getPassword());
        
        password = passwords.get(1);
        assertEquals("smtp.gmail.com", password.getServer());
        assertEquals((Integer) 25, password.getPort());
        assertFalse(password.isMxLookup());
        assertEquals("test@g mail.com", password.getUsername());
        assertEquals("", password.getPassword());        
    }
    
    @Test
    public void testParseSingleLine()
    {
        String input = " smtp.gmail.com : 578  test@gmail.com : sec:ret! ";
        
        List<SaslPassword> passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        SaslPassword password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertEquals((Integer) 578, password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test@gmail.com", password.getUsername());
        assertEquals("sec:ret!", password.getPassword());
        
        input = " [ smtp.gmail.com ] :  test@g mail.com :";
        
        passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertNull(password.getPort());
        assertFalse(password.isMxLookup());
        assertEquals("test@g mail.com", password.getUsername());
        assertEquals("", password.getPassword());        

        input = "[smtp.gmail.com]:25 test";
        
        passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertEquals((Integer) 25, password.getPort());
        assertFalse(password.isMxLookup());
        assertEquals("test", password.getUsername());
        assertEquals("", password.getPassword());
        
        input = "smtp.gmail.com:test:";
        
        passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        password = passwords.get(0);
        assertEquals("smtp.gmail.com", password.getServer());
        assertNull(password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test", password.getUsername());
        assertEquals("", password.getPassword());
        
        input = "test.x:25 test:";
        
        passwords = SaslPasswordManager.parseContent(input);
        
        assertEquals(1, passwords.size());
        password = passwords.get(0);
        assertEquals("test.x", password.getServer());
        assertEquals((Integer) 25, password.getPort());
        assertTrue(password.isMxLookup());
        assertEquals("test", password.getUsername());
        assertEquals("", password.getPassword());
        
        input = "";
        
        passwords = SaslPasswordManager.parseContent(input);
        assertEquals(0, passwords.size());

        passwords = SaslPasswordManager.parseContent(null);
        assertEquals(0, passwords.size());
    }
}
