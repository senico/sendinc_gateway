/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.common.fetchmail;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;


public class FetchmailConfigBuilderTest
{
    @Test
    public void testFetchmailConfigWriter()
    throws Exception
    {
        String input = "set syslog\n\n### START-AUTO-CONFIG ###\n" +
            "### END-AUTO-CONFIG ###\n\n## test";
        
        FetchmailConfig config = new FetchmailConfig();
        
        config.setPollInterval(5 * 60);
        config.setPostmaster("test\"@example.com");
        config.setCheckCertificate(true);
        
        Poll poll = new Poll();
        poll.setServer("example.com");
        poll.setAuthentication(Authentication.NTLM);
        poll.setFolder("ab\"cde\tf");
        poll.setUsername("&\"&&");
        poll.setIdle(true);
        poll.setProtocol(Protocol.IMAP);

        Poll poll2 = new Poll();
        poll2.setUIDL(false);
        poll2.setPassword("12&3");
        poll2.setPort(123);
        
        config.getPolls().add(poll);
        config.getPolls().add(poll2);
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        FetchmailConfigBuilder.createConfig(config, IOUtils.toInputStream(input, "US-ASCII"), bos);
        
        String result = new String(bos.toByteArray(), "US-ASCII");

        assertTrue(result.contains("sslcertck"));
        assertTrue(result.contains("poll \"example.com\""));
        assertTrue(result.contains("poll \"undefined\""));
        assertTrue(result.contains(" service 123 "));
        assertTrue(result.contains("set syslog"));
        assertTrue(result.contains(" no uidl "));
        assertTrue(result.contains(" no keep"));
        assertTrue(result.contains("password \"12&3\""));
        assertTrue(result.contains("postmaster \"test\\x22@example.com\""));
        assertTrue(result.contains("user \"&\\x22&&\""));
        assertTrue(result.contains("folder \"ab\\x22cdef\""));
        assertTrue(result.contains("## test"));
    }
}
