Content-Type: multipart/mixed; boundary="${boundary}"

--${boundary}
Content-Type: text/plain
Content-Transfer-Encoding: ***

This message contains an encrypted message.

--${boundary}
Content-Type: application/octet-stream; name="smime.p7m"; smime-type=enveloped-data
Content-Transfer-Encoding: ***
Content-Disposition: attachment; filename="x-rimdevicesmime.p7m"
Content-Description: S/MIME Enveloped Message

MIAGCSqGSIb3DQEHA6CAMIACAQAxggETMIIBDwIBADB4MGQxCzAJBgNVBAYTAk5MMQswCQYDVQQI
DAJOSDESMBAGA1UEBwwJQW1zdGVyZGFtMRUwEwYDVQQDDAxNSVRNIFRlc3QgQ0ExHTAbBgkqhkiG
9w0BCQEWDmNhQGV4YW1wbGUuY29tAhABFfzXQQiHBzZulydFLJdwMA0GCSqGSIb3DQEBAQUABIGA
SB0s3K7DbdAUkbBTKlwduPP09IdfFDkQLMbie31bgvF11WH1XsM/whuY2FRIEAGwje4SHoddVNNM
a3GolkUmEZ040YKknOH2R9xjliZIJ5ZXRGYVbnTfdir8bfzNLeDnaRFNvySE97HD9wfwJmZiTJpB
wXbJewP+6rs0N5FmzQowgAYJKoZIhvcNAQcBMBQGCCqGSIb3DQMHBAhk+ONzbXMQwKCABHDAmRZY
ptximws8bYoLUrruJjnFJqL9LsgJWNTb0jsOe+6qV2CBwWKdUkX8f6baxROXk7ocW+EDfuPDk09O
C7nX16ikUcmoc0dk9j+Bi7wvjhHNeiJ5STS702z6UT4bIxIiL7AkpjGXXrjMCAf37CSAAAAAAAAA
AAAAAA==

--${boundary}--
