Subject: The message has been encrypted
From: ${from!"<>"}
<#if to??>
To: <#list to as recipient>${recipient}<#if recipient_has_next>, </#if></#list>
</#if>
<#if cc??>
Cc: <#list cc as ccrecipient>${ccrecipient}<#if ccrecipient_has_next>, </#if></#list>
</#if>
Content-Type: text/plain
Content-Transfer-Encoding: 7bit
Mime-Version: 1.0

The message with Subject:

${subject!""}

has been sent encrypted to the following recipients:

<#-- we need to show the recipients of the source message -->
<#list mail.recipients as recipient>
${recipient}
</#list>
