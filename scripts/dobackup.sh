#!/bin/bash -e

# 
# Script that backups/restores the Djigzo database and files specified in backup_files
#
# Copyright 2008-2011. Martijn Brinkers.
#

SQL_DUMP_FILENAME="djigzo.sql"
VERSION_FILENAME="djigzo-version"
DJIGZO_DATABASE="djigzo"
DJIGZO_DB_USER="djigzo"

THIS_VERSION="3"

GENERAL_EXIT_CODE=100
INCORRECT_USAGE_EXIT_CODE=101
DECRYPTION_FAILED_EXIT_CODE=102
ENCRYPTION_FAILED_EXIT_CODE=103
TEMP_DIR_NOT_SET_EXIT_CODE=104
BACKUP_FILE_NOT_EXIST_EXIT_CODE=105
INCORRECT_BACKUP_FILE_EXIST_EXIT_CODE=106
MKTEMP_FAILED_EXIT_CODE=107
BACKUP_FILE_VERSION_INCORRECT_EXIT_CODE=108

CURRENT_DIR=$(dirname "$0")
TMPDIR=
ACTION=
BACKUP_FILE=
PASSWORD=

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# read the files we need to backup from backup_files
BACKUPFILES=$CURRENT_DIR/backup_files

# add all files you want to backup to this list
FILES_TO_BACKUP=$(cat $BACKUPFILES)

usage()
{
    echo "Usage: (-r | -b | -l) -f file [-p password] args\n" >&2
    exit $INCORRECT_USAGE_EXIT_CODE
}

cleanup()
{
    #echo "cleaning up " $TMPDIR >&2

    if [ ! "$TMPDIR" ]; then
        echo "TMPDIR is not set" >&2

        exit $TEMP_DIR_NOT_SET_EXIT_CODE
    fi

    rm -rf $TMPDIR
}

#
# checks whether the backup file exists
# 
backupExists()
{
    if [ ! -e "$BACKUP_FILE" ]; then
        echo "Backup file does not exist" >&2

        exit $BACKUP_FILE_NOT_EXIST_EXIT_CODE
    fi
}

decryptionFailed()
{
    echo "decryption failed" >&2

    exit $DECRYPTION_FAILED_EXIT_CODE
}

encryptionFailed()
{
    echo "encryption failed" >&2

    exit $ENCRYPTION_FAILED_EXIT_CODE
}

backup()
{
    local TAR=$TMPDIR/backup.tar

    # create an empty tar to which we will add the files
    tar -cf "$TAR" --files-from /dev/null

    # create a version file so we can detect that this is a djigzo backup
    echo "$THIS_VERSION" > $TMPDIR/$VERSION_FILENAME
    tar -C $TMPDIR -rPf "$TAR" "$VERSION_FILENAME"

    # dump the database to a temp file and add it to the tar
    local TMPFILE=$TMPDIR/$SQL_DUMP_FILENAME

    sudo -u postgres pg_dump "$DJIGZO_DATABASE" > $TMPFILE

    tar -C $TMPDIR -rPf "$TAR" "$SQL_DUMP_FILENAME"

    # add the files we want to backup
    for FILE in $FILES_TO_BACKUP; do

        FILENAME=$(echo -n "$FILE" | cut -f 1 -d#)

        # skip lines that start with #        
        if [ -z "$FILENAME" ]; then
            continue
        fi

        if [ -a $FILENAME ]; then
                tar -rPf "$TAR" $FILENAME
        else
                echo "$FILENAME does not exist" >&2
        fi
    done

    local BACKUP=$TAR

    # if a password is specified we will GPG encrypt the tar
    if [ "$PASSWORD" ]; then
        echo $PASSWORD | gpg -q --yes -c --no-tty --batch --passphrase-fd 0 -o "$TAR".gpg "$TAR" 2> /dev/null || encryptionFailed
        BACKUP="$TAR".gpg
    fi

    cp "$BACKUP" "$BACKUP_FILE" 
}

#
# decrypt the BACKUP_FILE if password is set. After decryption BACKUP_FILE will be set
# to the decrypted TAR file
# 
decrypt()
{
    # if a password is specified we assume the file is a GPG encrypted tar
    if [ "$PASSWORD" ]; then
        # decrypt to backup.tar in de temp dir
        echo $PASSWORD | gpg -q -d --no-tty --batch --passphrase-fd 0 -o $TMPDIR/backup.tar "$BACKUP_FILE" 2> /dev/null || decryptionFailed

        BACKUP_FILE=$TMPDIR/backup.tar
    fi
}

invalidBackup()
{
    echo "File is not a valid backup file" >&2

    exit $INCORRECT_BACKUP_FILE_EXIST_EXIT_CODE
}

incorrectVersion()
{
    echo "Backup file is from a newer version of Djigzo than supported. Supported versions <= $THIS_VERSION, backup version = $VERSION" >&2

    exit $BACKUP_FILE_VERSION_INCORRECT_EXIT_CODE
}

#
# checks whether the backup tar contains the version string
# 
isValidBackup()
{
    tar -tPf "$BACKUP_FILE" | grep "^$VERSION_FILENAME\s*\$" >&2 > /dev/null || invalidBackup
    
    # read the backup version 
    VERSION=$(tar --to-stdout -xf "$BACKUP_FILE" "$VERSION_FILENAME" 2> /dev/null);
    
    ALLOW_RESTORE=$(echo $VERSION|awk '{ if ($1 <= '$THIS_VERSION') {print "1"}}')
    
    if [ ! $ALLOW_RESTORE ]; then
        incorrectVersion
    fi    
}

list()
{
    backupExists
    decrypt
    isValidBackup
    
    tar -tPf "$BACKUP_FILE" >&1
}

restart()
{
    # We need to restart the services
    "$CURRENT_DIR"/restart.sh
}

calcPGRestoreExtraParams()
{
    PG_82=8.2
    PG_CURRENT=$(pg_restore --version | cut -d' ' -f 3)

    PG_RESTORE_EXTRA_PARAMS=
    
    # Note: --single-transaction is only supported PostgreSQL >= 8.2    
    PG_RESTORE_EXTRA_PARAMS=$(echo $PG_CURRENT|awk '{ if ($1 >= '$PG_82') {print "--single-transaction"}}')
}

dropTables()
{
    sudo -u postgres psql djigzo < "$CURRENT_DIR"/../conf/djigzo-drop-tables.sql || true
}

restore()
{
    backupExists
    decrypt
    isValidBackup

    # restore the files we need
    for FILE in $FILES_TO_BACKUP; do

        FILENAME=$(echo -n "$FILE" |  cut -f 1 -d#)
        
        # skip lines that start with #        
        if [ -z "$FILENAME" ]; then
            continue
        fi
        
        OWN=$(echo -n "$FILE" |  cut -f 2 -d#)
        MOD=$(echo -n "$FILE" |  cut -f 3 -d#)

        if tar --wildcards -xPof "$BACKUP_FILE" $FILENAME; then
            chown "$OWN" $FILENAME
            chmod "$MOD" $FILENAME
        fi
    done

    # extract the sql file to temp folder and restore the database
    tar -C "$TMPDIR" -xPf "$BACKUP_FILE" "$SQL_DUMP_FILENAME"

    # make sure postgres user can read the dump
    chown postgres:postgres "$TMPDIR"
    chown postgres:postgres "$TMPDIR"/"$SQL_DUMP_FILENAME"

    dropTables
        
    calcPGRestoreExtraParams
    
    if [ $VERSION -eq "1" ]; then
       sudo -u postgres pg_restore $PG_RESTORE_EXTRA_PARAMS -e -d "$DJIGZO_DATABASE" "$TMPDIR"/"$SQL_DUMP_FILENAME"
    else
       sudo -u postgres psql "$DJIGZO_DATABASE" < "$TMPDIR"/"$SQL_DUMP_FILENAME"
    fi
    
    # we import the SQL to restore tables that were not in the imported sql
    sudo -u "$DJIGZO_DB_USER" psql "$DJIGZO_DATABASE" < "$CURRENT_DIR"/../conf/djigzo.sql || true

    # restart all services that need to be restarted
    restart
}

[ $# -ne 0 ] || usage

# create a temp dir
TMPDIR=$(mktemp -d) || exit $MKTEMP_FAILED_EXIT_CODE

# create a trap that will cleanup all temporary resources
trap cleanup ABRT EXIT HUP INT QUIT

while getopts 'rblf:p:' OPTION
do
    case $OPTION in
    r)  ACTION="r"
        ;;
    b)  ACTION="b"
        ;;
    l)  ACTION="l"
        ;;
    f)  BACKUP_FILE="$OPTARG"
        ;;
    p)  PASSWORD="$OPTARG"
        ;;
    ?)  usage
        ;;
    esac
done

shift $(($OPTIND - 1))

[ "$BACKUP_FILE" ] || usage

case $ACTION in
    r)  restore
        ;;
    b)  backup
        ;;
    l)  list
        ;;
esac

exit 0
