#!/bin/bash -e

# 
# Script that copies the standard input to the postifx main.cf config file.
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

log()
{
    logger $1
    echo $1
}

log "copying new main.cf"

cat > /etc/postfix/main.cf

log "main.cf copied"

exit 0