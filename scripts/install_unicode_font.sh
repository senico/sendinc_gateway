#!/bin/bash -e

# 
# Script that extracts the DejaVuSans.ttf fonr from dejavu-fonts-ttf-2.33.tar.bz2 and
# places it in the font directory
#
# Copyright 2011. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# Include djigzo defaults if available
if [ -f /etc/default/djigzo ] ; then
    . /etc/default/djigzo
fi

if [ ! "$DJIGZO_HOME" ]; then
    DJIGZO_HOME=/usr/share/djigzo
fi

FONT_ARCHIVE="dejavu-fonts-ttf-2.33.tar.bz2"
FONT_NAME="DejaVuSans.ttf"
FONT_PATH="dejavu-fonts-ttf-2.33/ttf/"$FONT_NAME
FONTS_DIR=$DJIGZO_HOME/resources/fonts

tar --strip-components=2 -C $FONTS_DIR -xjf $FONTS_DIR/$FONT_ARCHIVE $FONT_PATH

chown djigzo:djigzo $FONTS_DIR/$FONT_NAME

exit 0
