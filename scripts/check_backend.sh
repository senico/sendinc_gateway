#!/bin/bash
#
# Script that checks if the Djigzo backend is running. Returns "ok" if running and "fail" if not running.
# 
# For example, the script be queried over the network with help of xinetd. xinetd config: 
#  
#service djigzo
#{
#        port            = 777
#        disable         = no
#        type            = UNLISTED
#        socket_type     = stream
#        protocol        = tcp
#        user            = djigzo
#        wait            = no
#        instances       = 4
#        server          = /usr/share/djigzo/scripts/check_backend.sh
#        server_args     = running
#        only_from       = 192.168.178.20
#}                                                                               
# 
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# Include djigzo defaults if available
if [ -f /etc/default/djigzo ] ; then
    . /etc/default/djigzo
fi

if [ ! "$DJIGZO_HOME" ]; then
    DJIGZO_HOME=/usr/share/djigzo
fi

usage()
{
    echo "usage: check_backend.sh (running | mpasize <repository> | mtasize | crlsize | certsize | certrequestsize | smssize | usersize)" >&2

    exit 1
}

checkRunning()
{
    running=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -running 2> /dev/null)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ] || [ "$running" != "true" ]; then
      echo "fail"
    else
      echo "ok"
    fi
}

retrieveMPASize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -mpasize "$@" 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveMTASize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -mtasize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveCRLSize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -crlsize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveCertStoreSize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -certsize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveCertReqStoreSize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -certrequestsize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveSMSSize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -smssize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

retrieveUserSize()
{
    result=$(java -cp $DJIGZO_HOME/djigzo.jar mitm.application.djigzo.tools.Monitor -usersize 2>&1)
    
    exit_code=$?
    
    if [ $exit_code -gt 0 ]; then
      echo "Error: $result"
      exit $exit_code;
    else
      echo "$result"
    fi
}

if [ $# -lt 1 ]; then
    usage
fi

COMMAND=$1

shift

case "$COMMAND" in
    running)
        checkRunning $@ 
    ;;
    mpasize)
        retrieveMPASize $@ 
    ;;
    mtasize)
        retrieveMTASize $@ 
    ;;
    crlsize)
        retrieveCRLSize $@ 
    ;;
    certsize)
        retrieveCertStoreSize $@ 
    ;;
    certrequestsize)
        retrieveCertReqStoreSize $@ 
    ;;
    smssize)
        retrieveSMSSize $@ 
    ;;
    usersize)
        retrieveUserSize $@ 
    ;;
    *)
        usage
esac

exit 0
