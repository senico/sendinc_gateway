#!/bin/bash -e

# 
# Script that is used to manage postfix. 
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

usage()
{
    echo "usage: dopostfix (postcat|postfix|postqueu|postsuper) args" >&2

    exit 1
}

COMMAND=$1

shift

case "$COMMAND" in
    postcat)
        sudo postcat $@ 
    ;;
    postfix)
        sudo postfix $@ 
    ;;
    postqueue)
        sudo postqueue $@ 
    ;;
    postsuper)
        sudo postsuper $@ 
    ;;
    *)
        usage
esac

exit 0
