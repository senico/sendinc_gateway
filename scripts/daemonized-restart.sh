#!/bin/bash

# file that restarts Djigzo, the servlet container(s) and reloads postfix.

# Note: this script restarts all Tomcat and Jetty instances it can find. If you do
# not want them to be restarted uncomment the relevant lines

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

DJIGZO="/etc/init.d/djigzo"
DJIGZO_APPLIANCE="/etc/init.d/djigzo-appliance"
POSTFIX="/etc/init.d/postfix"
JETTY6="/etc/init.d/jetty6"
TOMCAT5="/etc/init.d/tomcat5"
TOMCAT55="/etc/init.d/tomcat5.5"
TOMCAT6="/etc/init.d/tomcat6"

DELAY=$1

# do we need to wait a small time before killing djigzo?
# we need this so the soap call from the web application
# can finish without any error
if [ $DELAY ]; then
    sleep $DELAY
fi

if [ -x $DJIGZO ]; then
    sudo $DJIGZO stop
fi

if [ -x $DJIGZO_APPLIANCE ]; then
    sudo $DJIGZO_APPLIANCE restart
fi

# restarting postfix doesn't work when SELinux is enabled because the postfix processes will then be started
# with SELinux type java_t:SystemLow-SystemHigh whereas it should be postfix_master_t
# we will therefore reload postfix
#if [ -x $POSTFIX ]; then
#    sudo $POSTFIX restart
#fi

sudo postfix reload

if [ -x $JETTY6 ]; then
    sudo $JETTY6 restart
fi

if [ -x $TOMCAT5 ]; then
    sudo $TOMCAT5 restart
fi

if [ -x $TOMCAT55 ]; then
    sudo $TOMCAT55 restart
fi

if [ -x $TOMCAT6 ]; then
    sudo $TOMCAT6 restart
fi

if [ -x $DJIGZO ]; then
    sudo $DJIGZO start
fi

exit 0

