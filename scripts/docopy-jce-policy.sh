#!/bin/bash -e

# 
# Script that copies local_policy.jar or US_export_policy.jar.
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# the directory where we will store backups of the policy files
JCE_POLICY_BACKUP_DIR=./lib/jcepolicy

log()
{
    logger $1
    echo $1
}

usage()
{
    log "Usage: (local_policy.jar|US_export_policy.jar) (local_policy.jar|US_export_policy.jar)"
    exit 1
}

[ $# -eq 2 ] || usage

sourcePath=$1

targetPath=$2
targetFilename=${targetPath##/*/}

# only accept local_policy.jar or US_export_policy.jar as the target filename

if [ "$targetFilename" != "local_policy.jar" ] && [ "$targetFilename" != "US_export_policy.jar" ]; then
    log "Only local_policy.jar or US_export_policy.jar is an acceptable target filename."
    exit 1
fi

if [ ! -f "$sourcePath" ]; then
    log "Source file is not a valid file."
    exit 1
fi

if [ ! -f "$targetPath" ]; then
    log "target file is not a valid file."
    exit 1
fi

if ! cp "$sourcePath" "$targetPath"
then
    log "Copy source to target failed."
    exit 1
fi

# create a dir where we will store the policy files 
if [ ! -d "$JCE_POLICY_BACKUP_DIR" ]; then
    mkdir "$JCE_POLICY_BACKUP_DIR"
fi

cp "$sourcePath" "$JCE_POLICY_BACKUP_DIR"/"$targetFilename"

log "jce policy file "$targetFilename" copied."

exit 0
