#!/bin/bash -e

# 
# Script that is used to set postfix sasl passwords for smtp client authentication 
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

SMTP_CLIENT_PASSWD_FILE=/etc/postfix/smtp_client_passwd

usage()
{
    echo "usage: smtp-client-passwd-conf (get|set)" >&2

    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

get()
{
   if [ -f $SMTP_CLIENT_PASSWD_FILE ] ; then
       cat $SMTP_CLIENT_PASSWD_FILE >&1
   fi
}

set()
{
   cat > $SMTP_CLIENT_PASSWD_FILE
   
   chmod 600 $SMTP_CLIENT_PASSWD_FILE
   chown root:root $SMTP_CLIENT_PASSWD_FILE
   
   postmap $SMTP_CLIENT_PASSWD_FILE
}

COMMAND=$1

shift

case "$COMMAND" in
    get)
        get         
    ;;
    set)
        set         
    ;;
    *)
        usage
esac

exit 0
