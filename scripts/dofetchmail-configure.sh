#!/bin/bash -e

# 
# Script that configures Fetchmail by importing an XML file from standard in containing the new Fetchmail 
# configuration. The new configuration is 'merged' with the fetchmail config file /etc/fetchmailrc by 
# replacing everything between ### START-AUTO-CONFIG ### and ### END-AUTO-CONFIG ### 
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# Include djigzo defaults if available
if [ -f /etc/default/djigzo ] ; then
    . /etc/default/djigzo
fi

if [ ! "$DJIGZO_HOME" ]; then
    DJIGZO_HOME=/usr/share/djigzo
fi

FETCHMAIL_CONFIG=/etc/fetchmailrc
FETCHMAIL_RESTART=/etc/init.d/fetchmail
FETCHMAIL_NEW=$(mktemp) || exit 1

FETCHMAIL_CONFIG_JAR=$DJIGZO_HOME/lib/djigzo/fetchmail-config.jar

# check if jar file is owned by root and cannot be written by others
if [ $(stat $FETCHMAIL_CONFIG_JAR --printf=%U:%G) != "root:root" ]; then
        echo "ERROR: $FETCHMAIL_CONFIG_JAR not owned by root"
        exit 1
fi

# only start the jar when mod bits is 644
if [ $(stat $FETCHMAIL_CONFIG_JAR --printf=%a) != "644" ]; then
        echo "ERROR: $FETCHMAIL_CONFIG_JAR mod != 644"
        exit 1
fi

# write the new fetchmail config to temp file
java -jar $FETCHMAIL_CONFIG_JAR $FETCHMAIL_CONFIG > $FETCHMAIL_NEW

# and overwrite the existing fetcmail config with the new file 
mv $FETCHMAIL_NEW /etc/fetchmailrc

# restart fetchmail
$FETCHMAIL_RESTART restart

exit 0
