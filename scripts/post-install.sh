# you should run this after install when you don't use the ant scripts

# create sotflinks to the libs so James can find them 
pushd ../james-2.3.1/apps/james/SAR-INF/lib
find ../../../../../lib -name \*.jar -exec ln -s "{}" ";"
ln -s ../../../../../djigzo.jar
popd

# make scripts executable
find .. -name \*.sh -exec chmod 755 "{}" ";"
chmod 755 djigzo