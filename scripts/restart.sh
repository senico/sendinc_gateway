#!/bin/bash

# this is an intermediate script that calls another script with sudo
# the reason we do this is that we can call this script from a Java
# application without having to use sudo in the Java application 

sudo $(dirname "$0")/dorestart.sh $@

EXIT_CODE=$?

exit $EXIT_CODE 

