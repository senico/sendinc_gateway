#!/bin/bash -e

# 
# Script that restarts all the services that Djigzo depends on. 
#
# Copyright 2008-2010. Martijn Brinkers.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

RESTART_SCRIPT=$(dirname "$0")/daemonized-restart.sh

# we need to start the script that actually restarts the services as a daemon. This script is
# called by Djigzo. When the restart script restarts Djigzo all child processes will be terminated
# starting the restart script as a background daemon makes it a child of the init process and
# it's therefore to terminated when Djizo restarts. The restart script will wait a coulple of
# seconds (4 seconds) before restarting to give the web admin page enough time to open a 
# "delay for restart" page

DELAY=4

# we need to restart the script with nohup to prevent the script from hanging when the parent
# process is killed (the parent process is killed by the restart script). nohup makes sure 
# that std. in/out is redirected  
nohup "$RESTART_SCRIPT" "$DELAY" >/dev/null 2>&1 &

exit 0

