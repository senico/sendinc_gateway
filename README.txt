Copyright 2008-2011, Martijn Brinkers, Djigzo. Djigzo is AGPLv3 licensed.

* This program contains software created by the Exolab Project (http://www.exolab.org/).

* This product includes software developed by the Visigoth Software Society (http://www.visigoths.org/).

* This program contains software created by the DOM4J Project - http://www.dom4j.org

* This product includes software developed by the MX4J project (http://mx4j.sourceforge.net).

* This product includes software developed by the RealityForge Group (http://www.realityforge.org/).

* This product includes icons by FamFamFam (http://www.famfamfam.com/lab/icons/silk/)

