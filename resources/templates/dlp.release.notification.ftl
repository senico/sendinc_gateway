<#if recipients??>
To: <#list recipients as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
In-Reply-To: ${(org.messageID)!""}
Subject: *** The message has been released from quarantine ***
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with id:

${org.id!"unknown"}

and subject:

${qp(org.subject!"")}

has been released from quarantine.

---
Sent with Sendinc Gateway
