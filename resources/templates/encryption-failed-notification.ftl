From: <${from!""}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: The message could not be encrypted.
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with Subject

${qp(subject!"")}

has not been sent to the following recipients because the message could not be encrypted.

<#-- we need to show the recipients of the source message -->
<#list mail.recipients as recipient>
${qp(recipient)}
</#list>

<#if body??>
${qp(body)}
</#if>

---
Sent with Sendinc Gateway
