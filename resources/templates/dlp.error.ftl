From: <${from!""}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: *** DLP error warning ***
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with subject:

${qp(subject!"")}

was quarantined because an error occurred during DLP violation checking.

<#assign quarantineID = mail.getAttribute("djigzo.mailRepositoryID")>

The message has been quarantined under id: ${quarantineID}

<#assign url = .vars["user.dlp.quarantineURL"]!>
<#if url != "">
For more info see ${qp(url)}?id=3D${quarantineID}
</#if>

---
Sent with Sendinc Gateway
