Content-Type: multipart/mixed; boundary="${boundary}"

--${boundary}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable

Hi,

This message contains a password encrypted pdf file. The password for
the pdf can be retrieved by logging into the web portal.

You can read the message by following these steps:

1. click the link below.

2. log in to the site with your password.

3. press 'generate' to generate the password to the pdf file.

4. copy the generated password.

5. open the pdf file, attached to the email you received, and paste the
password in the password box.

<#assign passwordID = passwordContainer.passwordID!>
<#assign baseURL = .vars["user.otpURL"]!>
<#if baseURL != "">
<#assign url=baseURL+'?id='+passwordID+'&pwl='+passwordContainer.passwordLength+'&email='+recipients[0]?url('UTF-8')!>
${qp(url)}
</#if>

The password ID of this email is: ${passwordID}

<#if (from.personal)??>
Best regards,

${qp(from.personal)}
</#if>

---
Sent with Sendinc Gateway

--${boundary}
Content-Disposition: attachment; filename=encrypted_${passwordID}.pdf
Content-Type: application/pdf; name=encrypted_${passwordID}.pdf
Content-Transfer-Encoding: base64
X-Djigzo-Marker: attachment

JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURl
Y29kZT4+CnN0cmVhbQp4nB2KuwrCQBBF+/mKqYWNM2P2bgLLFBEt7AIDFmLnoxNM4+9nIwcOB+6V
TvlHXxZO0hJqzWXcvDz5uuPPf9xY3jQFZXQDF+vbIR68PyurcbxuFeoJ1aQIesAE8KFi9JQrjn6o
yH6PC52CZpp5Bfe6GasKZW5kc3RyZWFtCmVuZG9iagoKMyAwIG9iagoxMTQKZW5kb2JqCgo1IDAg
b2JqCjw8L1R5cGUvRm9udC9TdWJ0eXBlL1R5cGUxL0Jhc2VGb250L1RpbWVzLVJvbWFuCi9FbmNv
ZGluZy9XaW5BbnNpRW5jb2RpbmcKPj4KZW5kb2JqCgo2IDAgb2JqCjw8L0YxIDUgMCBSCj4+CmVu
ZG9iagoKNyAwIG9iago8PC9Gb250IDYgMCBSCi9Qcm9jU2V0Wy9QREYvVGV4dF0KPj4KZW5kb2Jq
CgoxIDAgb2JqCjw8L1R5cGUvUGFnZS9QYXJlbnQgNCAwIFIvUmVzb3VyY2VzIDcgMCBSL01lZGlh
Qm94WzAgMCA2MTIgNzkyXS9Hcm91cDw8L1MvVHJhbnNwYXJlbmN5L0NTL0RldmljZVJHQi9JIHRy
dWU+Pi9Db250ZW50cyAyIDAgUj4+CmVuZG9iagoKNCAwIG9iago8PC9UeXBlL1BhZ2VzCi9SZXNv
dXJjZXMgNyAwIFIKL01lZGlhQm94WyAwIDAgNTk1IDg0MiBdCi9LaWRzWyAxIDAgUiBdCi9Db3Vu
dCAxPj4KZW5kb2JqCgo4IDAgb2JqCjw8L1R5cGUvQ2F0YWxvZy9QYWdlcyA0IDAgUgovT3BlbkFj
dGlvblsxIDAgUiAvWFlaIG51bGwgbnVsbCAwXQo+PgplbmRvYmoKCjkgMCBvYmoKPDwvQXV0aG9y
PEZFRkYwMDZEMDA2MTAwNzIwMDc0MDA2OTAwNkEwMDZFPgovQ3JlYXRvcjxGRUZGMDA1NzAwNzIw
MDY5MDA3NDAwNjUwMDcyPgovUHJvZHVjZXI8RkVGRjAwNEYwMDcwMDA2NTAwNkUwMDRGMDA2NjAw
NjYwMDY5MDA2MzAwNjUwMDJFMDA2RjAwNzIwMDY3MDAyMDAwMzIwMDJFMDAzMz4KL0NyZWF0aW9u
RGF0ZShEOjIwMDgwMzI0MjIyODIzKzAxJzAwJyk+PgplbmRvYmoKCnhyZWYKMCAxMAowMDAwMDAw
MDAwIDY1NTM1IGYgCjAwMDAwMDA0MDEgMDAwMDAgbiAKMDAwMDAwMDAxOSAwMDAwMCBuIAowMDAw
MDAwMjA0IDAwMDAwIG4gCjAwMDAwMDA1NDMgMDAwMDAgbiAKMDAwMDAwMDIyNCAwMDAwMCBuIAow
MDAwMDAwMzE3IDAwMDAwIG4gCjAwMDAwMDAzNDggMDAwMDAgbiAKMDAwMDAwMDY0MSAwMDAwMCBu
IAowMDAwMDAwNzI0IDAwMDAwIG4gCnRyYWlsZXIKPDwvU2l6ZSAxMC9Sb290IDggMCBSCi9JbmZv
IDkgMCBSCi9JRCBbIDwzRTlDMTAyOTVGNTgzMUNDRTdFQzFBREQ2OTFBNTk3Qz4KPDNFOUMxMDI5
NUY1ODMxQ0NFN0VDMUFERDY5MUE1OTdDPiBdCi9Eb2NDaGVja3N1bSAvMjc1QjJEQjg3OTIwNjZE
RTU1NEQwRTExQUI5RjI0NDcKPj4Kc3RhcnR4cmVmCjk1MgolJUVPRgo=


--${boundary}--
