<#if recipients??>
To: <#list recipients as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
In-Reply-To: ${(org.messageID)!""}
Subject: *** The quarantined message expired ***
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with id:

${org.id!"unknown"}

and subject:

${qp(org.subject!"")}

expired and has been deleted from quarantine. The message was not delivered to the intended recipient(s).

---
Sent with Sendinc Gateway
