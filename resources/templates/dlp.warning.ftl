From: <${from!""}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: *** DLP Warning ***
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with subject:

${qp(subject!"")}

Violated the following DLP policies:

<#assign violations = mail.getAttribute("djigzo.policyViolations")>
<#list violations as violation>
${abbreviate(qp((violation)!""), 900)}
</#list> 

---
Sent with Sendinc Gateway
