From: <${from!""}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient_has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: The message has been encrypted
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: quoted-printable
Auto-Submitted: auto-replied

The message with Subject:

${qp(subject!"")}

has been sent encrypted to the following recipients:

<#-- we need to show the recipients of the source message -->
<#list mail.recipients as recipient>
${qp(recipient)}
</#list>

---
Sent with Sendinc Gateway
